// main202.cc is a part of the PYTHIA event generator.
// Copyright (C) 2021 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// This test program is a basic check of Vincia showers for pp > tt at LHC.
// Also illustrates how various components of Vincia can be switched on/off
// in a command file, and measures the run time (eg to compare options
// and/or compare with Pythia).

// Authors: Peter Skands <peter.skands@monash.edu>

// Keywords: Vincia; Dire; top;

#include <time.h>
#include "Pythia8/Pythia.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TSystem.h"
#include "TRandom3.h"

#ifdef HEPMC3
#include "Pythia8Plugins/HepMC3.h"
#endif

#define  MSG_INFO  "\033[0;32m" << std::left << std::setw(40)<< __PRETTY_FUNCTION__ << " INFO " << "\033[0m"
#define  MSG_DEBUG(x) std::cout << "\033[1;90m" << __PRETTY_FUNCTION__ << " DEBUG \033[0;90m" << x << "\033[0m" << std::endl;

using namespace Pythia8;
// -----------------------------------------------------------------------------
// Define some functions
int getFirstMotherofType(int type, Event& event, int i);
std::map<int,int> createListofAncestors(Event& event, int i,bool print=true, bool stopatprocess = false);
std::pair<std::string,std::vector<int>> getMothersType(Event& event, int i );
std::string getStatusType(int code);
std::vector<int> getStringofGluons(Event & event, int b_col, int c_col, bool isb_plus, int first_b_id, int first_c_id);
std::pair<int,std::pair<std::vector<int>,int>>  getColourPartner(const Event & event, int b_id);
int getNonZeroEntries(std::map<int,int> map);
int getbProcessType(Event& event,int);
int getcProcessType(Event& event,int);
int getProcessType(Event& event, int heavy, int partid);
void getShowerStatus(Pythia &pythia,Event &event, int flavour, bool &foundShowerMPIFSR,bool &foundShowerMPIISR,
      bool &foundShowerHardFSR,bool &foundShowerHardISR,bool &foundShowerBeam,std::map<int,int>* gluons = NULL, std::map<int,
      std::vector<int>>* Ptrqqbar = NULL);
bool isCharmonia(int id);
bool isBottomonia(int id);
int  octetFlavour(int id);
vector<int> getDigits(int id);
int getQuarkLineEndpoint(Event &event, int cquark_i);
void getOniaType(Event &event, PartonSystems &partonSystems,  int flavour, int i,bool &isMixed,
                 bool &isMixedLine,bool &isHard,bool &isMPI,bool &iscc,int  &iscc_type,bool &isDecay,
                 int & isHard_partSys, int &iscc_c_partSys, int &iscc_cbar_partSys);

int getHeavyHadronPartSys(Event &event,PartonSystems& partonSystems,int i);
int getPartonSystem(Event& event, PartonSystems& partonSystems, int heavy_i, bool print = false);
std::pair<int,int> getDoublyHeavyHadronPartSys(Event &event,PartonSystems& partonSystems,int i);
std::pair<int,int> getXibcPartSys(Event &event,PartonSystems& partonSystems,int i);


// -----------------------------------------------------------------------------
// b user hook
// -----------------------------------------------------------------------------

class MybUserHooks : public UserHooks {

public:

  // Constructor
  MybUserHooks(const Info* infoPtrIn) {
    infoPtr = infoPtrIn;
  }
  
  //Destructor
  ~MybUserHooks() {}
  
  //to check the event after the hardest interaction
  virtual bool canVetoMPIStep()    {return true;}
  virtual int  numberVetoMPIStep() {return 1;}
  virtual bool doVetoMPIStep(int nMPI, const Event& event)  {
    nMPI      = 1;
    hasHardb  = false;
    pTHat_val = infoPtr->pTHat();
    
    // looking for b quark
    for (int i = 0; i < event.size(); ++i) {
      if(event[i].idAbs() == quarkID) {
        hasHardb = true;
      }
    }

    //do not veto events with b or pT>=pTthreshhold
    if(hasHardb || pTHat_val>=pTHatThreshold){
      return keep;
    } else {
      return reject;
    }
  }

  // Allow a veto for the interleaved evolution in pT
  virtual bool   canVetoPT()   {return true;}
  virtual double scaleVetoPT() {return pTHatThreshold;}
  virtual bool   doVetoPT(int iPos, const Event& event) {
    hasb = false;
    
    // looking for b quark
    for(int i =0; i<event.size(); i++){
      if(event[i].idAbs()== quarkID) {
        hasb = true;
      }
    }

    if (!hasb) {
      return reject;
    } else {
      return keep;
    }
  }

  // virtual bool canVetoAfterHadronization() {return true;}

  // virtual bool doVetoAfterHadronization(const Event& event) {
  //   // looking for Bc
  //   bool hasBc = false;
  //   for (int i = 0; i < event.size(); ++i) {
  //     if(event[i].idAbs() == 511) {
  //       hasBc = true;
  //     }
  //   }
  //   if(hasBc) {
  //     std::cout << "********************" <<std::endl;
  //     std::cout << "***** Found Bs *****" <<std::endl;
  //     std::cout << "********************" <<std::endl;
  //     return keep;
  //   } else {
  //     return reject;
  //   }
  // }  
private:
  // Pointer to the info class
  const  Info* infoPtr;
  const bool reject = true;
  const bool keep   = false;
  double       pTHat_val;
  bool         hasHardb;
  bool         hasb;
  const double pTHatThreshold = 4.0;
  const int    quarkID        = 5;

};

// -----------------------------------------------------------------------------
// c user hook
// -----------------------------------------------------------------------------
class MycUserHooks : public UserHooks {

public:

  // Constructor
  MycUserHooks(const Info* infoPtrIn) {
    infoPtr = infoPtrIn;
  }
  
  //Destructor
  ~MycUserHooks() {}
  
  //to check the event after the hardest interaction
  virtual bool canVetoMPIStep()    {return true;}
  virtual int  numberVetoMPIStep() {return 1;}
  virtual bool doVetoMPIStep(int nMPI, const Event& event)  {
    nMPI      = 1;
    hasHardb  = false;
    pTHat_val = infoPtr->pTHat();
    
    // looking for b quark
    for (int i = 0; i < event.size(); ++i) {
      if(event[i].idAbs() == quarkID) {
        hasHardb = true;
      }
    }

    //do not veto events with b or pT>=pTthreshhold
    if(hasHardb || pTHat_val>=pTHatThreshold){
      return false;
    } else {
      return true;
    }
  }

  // Allow a veto for the interleaved evolution in pT
  virtual bool   canVetoPT()   {return true;}
  virtual double scaleVetoPT() {return pTHatThreshold;}
  virtual bool   doVetoPT(int iPos, const Event& event) {
    hasb = false;
    
    // looking for b quark
    for(int i =0; i<event.size(); i++){
      if(event[i].idAbs()== quarkID) {
        hasb = true;
      }
    }

    if (!hasb) {
      return true;
    } else {
      return false;
    }
  }

private:
  // Pointer to the info class
  const  Info* infoPtr;
  double       pTHat_val;
  bool         hasHardb;
  bool         hasb;
  const double pTHatThreshold = 1.5;
  const int    quarkID        = 4;

};

// -----------------------------------------------------------------------------
// cc user hook
// -----------------------------------------------------------------------------

class MyccUserHooks : public UserHooks {

public:

  MyccUserHooks() { }
    MyccUserHooks(const Info* infoPtrIn, 
                  double pTHatThresholdIn,
                  int quarkIDIn) {
    infoPtr = infoPtrIn;
    pTHatThreshold = pTHatThresholdIn;
    quarkID = quarkIDIn;
  }

  ~MyccUserHooks() {}


  // ===========================================================================
  // Public variables
  // ===========================================================================
  
  const  Info* infoPtr;
  double pTHat_val;
  bool   hasq       = false;
  const bool reject = true;
  const bool keep   = false;
  
  // Options to configure 
  bool         debug          = true;
  const int    npairs         = 2;
  double pTHatThreshold       = 1.5;
  int    quarkID              = 4;

  // ===========================================================================
  // to check the event after the hardest interaction
  // ===========================================================================

  virtual bool canVetoMPIStep()    {return true;}
  virtual int  numberVetoMPIStep() {return 1;}
  virtual bool doVetoMPIStep(int nMPI, const Event& event)  {
    
    bool hasHardq  = false;
    pTHat_val = infoPtr->pTHat();
    
    // looking for b quark
    for (int i = 0; i < event.size(); ++i) {

      if( (event[i].particleDataEntry().nQuarksInCode(quarkID) > 0) || 
          (event[i].particleDataEntry().isOctetHadron() && 
           octetFlavour(event[i].id()) == quarkID)     
          ) {
        hasHardq = true;
      }

    }

    //do not veto events with q or pT>=pTthreshhold
    if(hasHardq || pTHat_val>=pTHatThreshold){
      return keep;
    } else {
      return reject;
    }
  }
  
  // ===========================================================================
  // VETO during pT evolution 
  // ===========================================================================

  virtual bool canVetoPT()  {return true;}

  virtual double scaleVetoPT() {
    return pTHatThreshold;
  }

  virtual bool doVetoPT(int iPos, const Event& event){

    bool hasHardq  = false;


    // looking for c quark (in addition to quarkonia if found)
    for (int i = 0; i < event.size(); ++i) {

      if( (event[i].particleDataEntry().nQuarksInCode(quarkID) > 0) || 
          (event[i].particleDataEntry().isOctetHadron() && 
           octetFlavour(event[i].id()) == quarkID)     
          ) {
        hasHardq = true;
      }

    }

    // veto events without q
    if(hasHardq){
      return keep;
    } else { 
      return reject;
    }




    // std::vector<int> cquarks;
    // std::vector<int> cbarquarks;
    // std::vector<int> quarkonia;

    // for(int i =1; i<event.size(); i++){

    //   // Check for unique quarks 
    //   if( event[i].id()                  == quarkID){
    //     bool not_unique = false;
    //     for(const auto& daug_i: event[i].daughterList()){
    //       if(event[daug_i].id()==quarkID) not_unique = true;
    //     }
    //     if(!not_unique) cquarks.push_back(i);
    //   }

    //   if( event[i].id()                  == -quarkID){
    //     bool not_unique = false;
    //     for(const auto& daug_i: event[i].daughterList()){
    //       if(event[daug_i].id()==-quarkID) not_unique = true;
    //     }
    //     if(!not_unique) cbarquarks.push_back(i);
    //   }

    //   // Find onium in hard/MPI processes
    //   if(  (abs(event[i].status()) == 23 || 
    //         abs(event[i].status()) == 33)){
    //     bool isQuarkonium = false;
        
    //     // Check for normal onium 
    //     if( event[i].particleDataEntry().isOnium() && 
    //         event[i].particleDataEntry().nQuarksInCode(quarkID)>0 ) {
    //       isQuarkonium = true;
    //     }
        
    //     // Check for octet onium
    //     if( event[i].particleDataEntry().isOctetHadron() && 
    //         octetFlavour(event[i].id()) == quarkID   ){
    //       isQuarkonium = true;
    //     }

    //     // Save hard process or MPI onium
    //     if(isQuarkonium) quarkonia.push_back(i);

    //   }
    // }

    // // stop = clock();
    // // std::cout << "    Time PT: " << (double) 1000000*(stop-start)/CLOCKS_PER_SEC << " microseconds " <<std::endl;;
    
    // if(cquarks.size()+cbarquarks.size()+2*quarkonia.size()>=2*npairs) { 
    //   return keep;
    // } else {
    //   return reject;
    // }

  } 
  // ===========================================================================
  // Check how many qq pairs there are
  // ===========================================================================
 
  virtual bool canVetoPartonLevel(){return true;}
  virtual bool doVetoPartonLevel(const Event& event){
    std::vector<int> cquarks;
    std::vector<int> cbarquarks;
    std::vector<int> quarkonia;

    for(int i =1; i<event.size(); i++){

      // Check for unique quarks 
      if( event[i].id()                  == quarkID){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==quarkID) not_unique = true;
        }
        if(!not_unique) cquarks.push_back(i);
      }

      if( event[i].id()                  == -quarkID){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==-quarkID) not_unique = true;
        }
        if(!not_unique) cbarquarks.push_back(i);
      }

      bool isQuarkonium = false;
      
      // Check for normal onium 
      if( event[i].particleDataEntry().isOnium() && 
          event[i].particleDataEntry().nQuarksInCode(quarkID)>0 ) {
        isQuarkonium = true;
      }
      
      // Check for octet onium
      if( event[i].particleDataEntry().isOctetHadron() && 
          octetFlavour(event[i].id()) == quarkID   ){
        isQuarkonium = true;
      }

      // Save hard process or MPI onium
      if(isQuarkonium && 
          (abs(event[i].status()) == 23 || 
           abs(event[i].status()) == 33)  
         ){
        quarkonia.push_back(i);
      }

    }

    if(cquarks.size()+cbarquarks.size()>2*(npairs-1)) { 
      return keep;
    } else if(cquarks.size()+cbarquarks.size()+2*quarkonia.size()>2*(npairs-1)) { 
      return keep;
    } else {
      return reject;
    }

  } 

};


// -----------------------------------------------------------------------------
// bc user hook
// -----------------------------------------------------------------------------

class MybcUserHooks : public UserHooks {

public:


  bool foundbc = false;
  // -----------------------------------------
  // Constructor
  // -----------------------------------------
  MybcUserHooks(const Info* infoPtrIn,
                  bool vetoMPIStepIn,
                  bool vetoPTIn,
                  bool vetoPartonIn,
                  bool vetoMassIn,
                  double pTHatThresholdIn,
                  bool passThroughIn = false) {
    infoPtr        = infoPtrIn;
    vetoMPIStep    = vetoMPIStepIn;
    vetoPT         = vetoPTIn; 
    vetoParton     = vetoPartonIn; 
    vetoMass       = vetoMassIn; 
    pTHatThreshold = pTHatThresholdIn;
    passThrough    = passThroughIn;
  }


  // -----------------------------------------
  // Destructor
  // -----------------------------------------
  ~MybcUserHooks() {}

  // -----------------------------------------
  // Veto MPI for first process (hard process)
  // -----------------------------------------

  //to check the event after the hardest interaction
  virtual bool canVetoMPIStep()    {return vetoMPIStep;}
  virtual int  numberVetoMPIStep() {return 1;}
  virtual bool doVetoMPIStep(int nMPI, const Event& event)  {
    hasHardb  = false;
    hasHardc  = false;
    pTHat_val = infoPtr->pTHat();
    
    // looking for b or c quark
    // We only need to know if there is either of them
    // so break as soon as one is found
    for (int i = 0; i < event.size(); ++i) {
      if(event[i].idAbs() == 5) {
        hasHardb = true;
        break;
      }
      if(event[i].idAbs() == 4) {
        hasHardc = true;
        break;
      }
    }

    //do not veto events with b or c or pT>=pTthreshhold
    if(hasHardb || hasHardc || pTHat_val>=pTHatThreshold){
      decision_vetoMPIStep = keep;
    } else {
      decision_vetoMPIStep = reject;
    }

    // Enact the decision if not in passThrough mode
    if(passThrough){
      return keep;
    } else {
      
      if(decision_vetoMPIStep==reject){
        // Reset variables
        hasHardb  = false;
        hasHardc  = false;
        return reject;
      }else {
        // std::cout << "MPI ===> ";
        // if(hasHardb) std::cout << "\thasHardb ";
        // if(hasHardc) std::cout << "\thasHardc" ;
        // if(pTHat_val>=pTHatThreshold) std::cout << "\tpTHat_val: " << pTHat_val;
        // std::cout << std::endl;

        return keep;
      }
    }
  }

  // -----------------------------------------
  // Veto during pT evolution
  // -----------------------------------------

  virtual bool canVetoPT()  {return vetoPT;}

  virtual double scaleVetoPT() {
    return pTHatThreshold;
  }

  virtual bool doVetoPT(int iPos, const Event& event){

    // std::cout << "doVetoPT: Starting hasHardb: " << hasHardb <<"\thasHardc: " << hasHardc <<"\tpTHat_val: " << pTHat_val << std::endl;
    // If no hard b/c found before 
    // look for b quark but store information about c
    if(!hasHardb){
      for (int i = 0; i < event.size(); ++i) {

        if(event[i].idAbs() == 4) {
          hasHardc = true;
        }

        if(event[i].idAbs() == 5) {
          hasHardb = true;
          break;
        }
      }
    }

    // veto events without b
    if(hasHardb){
      decision_vetoPT = keep;
    } else { 
      decision_vetoPT = reject;
    }


    // Enact the decision if not in passThrough mode
    if(passThrough){
      return keep;
    } else {
      if(decision_vetoPT==reject){
        // Reset variables
        hasHardb  = false;
        hasHardc  = false;
        return reject;
      }else {
        return keep;
      }
    }
  }


  // -----------------------------------------
  // Veto after parton level early 
  // -----------------------------------------
  virtual bool canVetoPartonLevel() {return (vetoParton||vetoMass);}

  virtual bool doVetoPartonLevel(const Event& event){

    if(vetoParton){
      for(int i = 0; i<event.size(); ++i){
        if(event[i].idAbs()==4) hasHardc = true;
        if(event[i].idAbs()==5) hasHardb = true;
        if(hasHardc&&hasHardb) break;
      }

      if(hasHardc && hasHardb){
        decision_vetoParton = keep;
      } else {
        decision_vetoParton = reject;
      }


      if(passThrough){
        // Do nothing
      } else if(vetoMass) {
        // Only want to return a value if it is reject
        // So that kept ones proceed to mass stage
        if(decision_vetoParton == reject) {
          // Reset variables
          hasHardb  = false;
          hasHardc  = false;
          return reject;
        }
      } else {
        // If we're not doing the mass veto return now
        if(decision_vetoParton==reject){
          // Reset variables
          hasHardb  = false;
          hasHardc  = false;
          return reject;
        }else {
          return keep;
        }
      }
    }

    // Work out the mass veto
    std::vector<int> cquarks;
    std::vector<int> cbarquarks;
    std::vector<int> bquarks;
    std::vector<int> bbarquarks;

    for(int i =1; i<event.size(); i++){
      
      if( event[i].id()                  == 4){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==4) not_unique = true;
        }
        if(!not_unique) cquarks.push_back(i);
      
      } else if( event[i].id()                  == -4){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==-4) not_unique = true;
        }
        if(!not_unique) cbarquarks.push_back(i);
      
      } else if( event[i].id()                  == 5){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==5) not_unique = true;
        }
        if(!not_unique) bquarks.push_back(i);
      
      } else if( event[i].id()                  == -5){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==-5) not_unique = true;
        }
        if(!not_unique) bbarquarks.push_back(i);
      }

    }

    double smallest_bc_mass = 999.0;
    for(const auto& bquark: bquarks){
      for(const auto& cbarquark: cbarquarks){

        double b_pe = event[bquark].e();
        double b_px = event[bquark].px();
        double b_py = event[bquark].py();
        double b_pz = event[bquark].pz();
        
        double c_pe = event[cbarquark].e();
        double c_px = event[cbarquark].px();
        double c_py = event[cbarquark].py();
        double c_pz = event[cbarquark].pz();
        
        double b_plus_c_mass =  sqrt(pow2(b_pe+c_pe) - pow2(b_px+c_px) -pow2(b_py+c_py) -pow2(b_pz+c_pz));
        if(b_plus_c_mass<smallest_bc_mass){
          smallest_bc_mass = b_plus_c_mass;
        } 
      
      }
    }

    for(const auto& bbarquark: bbarquarks){
      for(const auto& cquark: cquarks){

        double b_pe = event[bbarquark].e();
        double b_px = event[bbarquark].px();
        double b_py = event[bbarquark].py();
        double b_pz = event[bbarquark].pz();
        
        double c_pe = event[cquark].e();
        double c_px = event[cquark].px();
        double c_py = event[cquark].py();
        double c_pz = event[cquark].pz();
        
        double b_plus_c_mass =  sqrt(pow2(b_pe+c_pe) - pow2(b_px+c_px) -pow2(b_py+c_py) -pow2(b_pz+c_pz));
        if(b_plus_c_mass<smallest_bc_mass){
          smallest_bc_mass = b_plus_c_mass;
        }
      
      }
    }
    
    if(smallest_bc_mass < 10.0){
      decision_vetoMass = keep;
    } else {
      decision_vetoMass = reject;
    }

    // Enact the decision if not in passThrough mode
    if(passThrough){
      return keep;
    } else {
      if(decision_vetoMass==reject){
        // Reset variables
        hasHardb  = false;
        hasHardc  = false;
        return reject;
      }else {
        return keep;
      }
    }

  }

  virtual bool canVetoAfterHadronization() {return true;}

  virtual bool doVetoAfterHadronization(const Event& event) {
    

    // looking for Bc or Bc star 
    // This hook is run before any hadrons are decayed, so the Bcstars won't have decayed
    bool hasBc     = false;
    bool hasBcstar = false;
    for (int i = 0; i < event.size(); ++i) {
      if(event[i].idAbs() == 541)   hasBc     = true;
      if(event[i].idAbs() == 543)   hasBcstar = true;
      if(event[i].idAbs() == 545)   hasBcstar = true;
      if(event[i].idAbs() == 10541) hasBcstar = true;
    }


    // Reset variables 
    hasHardb  = false;
    hasHardc  = false;

    if(hasBc) {
      std::cout << "********************" <<std::endl;
      std::cout << "***** Found Bc *****" <<std::endl;
      std::cout << "********************" <<std::endl;
      return keep;
    } else if(hasBcstar ) {
      std::cout << "********************" <<std::endl;
      std::cout << "*** Found Bcstar ***" <<std::endl;
      std::cout << "********************" <<std::endl;
      return keep;
    } else {
      return reject;
    }
  }  

  bool getDecVetoMPIStep(){return decision_vetoMPIStep;}
  bool getDecVetoPT(){     return decision_vetoPT;}
  bool getDecVetoParton(){ return decision_vetoParton;}
  bool getDecVetoMass(){   return decision_vetoMass;}

  bool        vetoMPIStep  = true; 
  bool        vetoPT       = true; 
  bool        vetoParton   = true; 
  bool        vetoMass     = true; 
  bool        passThrough  = false;

  // Save veto decisions
  bool        decision_vetoMPIStep = false;
  bool        decision_vetoPT      = false;
  bool        decision_vetoParton  = false;
  bool        decision_vetoMass    = false;
// private:
  // Pointer to the info class
  const  Info* infoPtr;
  
  double pTHat_val;
  double pTHatThreshold = 4.;
  
  // These variables must be reset before every 'reject' 
  bool   hasHardb = false;
  bool   hasHardc = false;

  // Definitions
  const bool reject = true;
  const bool keep   = false;
};
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// bbcc user hook
// -----------------------------------------------------------------------------

class MybbccUserHooks : public UserHooks {

public:

  bool foundbc = false;
  // -----------------------------------------
  // Constructor
  // -----------------------------------------
  MybbccUserHooks(const Info* infoPtrIn) {
    infoPtr = infoPtrIn;
  }

  // -----------------------------------------
  // Destructor
  // -----------------------------------------
  ~MybbccUserHooks() {}

  // -----------------------------------------
  // Veto MPI for first process (hard process)
  // -----------------------------------------

  //to check the event after the hardest interaction
  virtual bool canVetoMPIStep()    {return true;}
  virtual int  numberVetoMPIStep() {return 1;}
  virtual bool doVetoMPIStep(int nMPI, const Event& event)  {
    hasHardb  = false;
    hasHardc  = false;
    pTHat_val = infoPtr->pTHat();
    
    // looking for b or c quark
    for (int i = 0; i < event.size(); ++i) {
      
      if( (event[i].particleDataEntry().nQuarksInCode(4) > 0) || 
          (event[i].particleDataEntry().isOctetHadron() && 
           octetFlavour(event[i].id()) == 4)     
          ) {
        hasHardc = true;
      }

      if( (event[i].particleDataEntry().nQuarksInCode(5) > 0) || 
          (event[i].particleDataEntry().isOctetHadron() && 
           octetFlavour(event[i].id()) == 5)     
          ) {
        hasHardb = true;
      }

    }

    //do not veto events with b or c or pT>=pTthreshhold
    if(hasHardb || hasHardc || pTHat_val>=pTHatThreshold){
      return keep;
    } else {
      return reject;
    }
  }

  // -----------------------------------------
  // Veto during pT evolution
  // -----------------------------------------

  virtual bool canVetoPT()  {return true;}

  virtual double scaleVetoPT() {
    return pTHatThreshold;
  }

  virtual bool doVetoPT(int iPos, const Event& event){

    bool hasHardq  = false;

    // looking for b quark
    for (int i = 0; i < event.size(); ++i) {

      if( (event[i].particleDataEntry().nQuarksInCode(5) > 0) || 
          (event[i].particleDataEntry().isOctetHadron() && 
           octetFlavour(event[i].id()) == 5)     
          ) {
        hasHardq = true;
      }

    }

    // veto events without q
    if(hasHardq){
      return keep;
    } else { 
      return reject;
    }

  } 

  // -----------------------------------------
  // Veto after parton level early 
  // -----------------------------------------
  virtual bool canVetoPartonLevel() {return true;}

  virtual bool doVetoPartonLevel(const Event& event){ 
    // Look for charm

    bool temp_foundc = false;
    bool temp_foundb = false;
    for(int i =1; i<event.size(); i++){
      
      if( (event[i].particleDataEntry().nQuarksInCode(4) > 0) || 
          (event[i].particleDataEntry().isOctetHadron() && 
           octetFlavour(event[i].id()) == 4)     
          ) {
        temp_foundc = true;
      }

      if( (event[i].particleDataEntry().nQuarksInCode(5) > 0) || 
          (event[i].particleDataEntry().isOctetHadron() && 
           octetFlavour(event[i].id()) == 5)     
          ) {
        temp_foundb = true;
      }

    }

    if(temp_foundc && temp_foundb){      
      return keep;
    } else {
      return reject;
    }
  }


// private:
  // Pointer to the info class
  const  Info* infoPtr;
  double pTHat_val;
  bool   hasHardb;
  bool   hasHardc;
  bool   hasb;
  const double pTHatThreshold = 4.;
  bool   lookedforb = false;
  bool   foundb     = false;

  const bool reject = true;
  const bool keep   = false;
};
// -----------------------------------------------------------------------------


class MinnisUserHooks : public UserHooks {
public:
  
  // Constructor
  MinnisUserHooks(const Info* infoPtrIn,
                  bool vetoMPIStepIn,
                  bool vetoPTIn,
                  double pTHatThresholdIn,
                  int quarkIDIn,
                  bool passThroughIn = false) {
    infoPtr        = infoPtrIn;
    vetoMPIStep    = vetoMPIStepIn;
    vetoPT         = vetoPTIn; 
    pTHatThreshold = pTHatThresholdIn;
    quarkID        = quarkIDIn;
    passThrough    = passThroughIn;
  }
  
  //Destructor
  ~MinnisUserHooks() {}
  
  //to check the event after the hardest interaction
  virtual bool canVetoMPIStep()    {return vetoMPIStep;}
  virtual int  numberVetoMPIStep() {return 1;}
  virtual bool doVetoMPIStep(int nMPI, const Event& event)  {
    nMPI      = 1;
    hasHardb  = false;
    pTHat_val = infoPtr->pTHat();
    
    // looking for b quark  
    for (int i = 0; i < event.size(); ++i) {
      if( (event[i].particleDataEntry().nQuarksInCode(quarkID) > 0) || 
          (event[i].particleDataEntry().isOctetHadron() && 
           octetFlavour(event[i].id()) == quarkID)     
          ) {
        hasHardb = true;
      }
    }

    //do not veto events with b or pT>=pTthreshhold
    if(hasHardb || pTHat_val>=pTHatThreshold){
      decision_vetoMPIStep = keep;
    } else {      
      decision_vetoMPIStep = reject;
    }

    // Enact the decision if not in passThrough mode
    if(passThrough){
      return keep;
    } else {
      return decision_vetoMPIStep;
    }
  }

  // Allow a veto for the interleaved evolution in pT
  virtual bool   canVetoPT()   {return vetoPT;}
  virtual double scaleVetoPT() {return pTHatThreshold;}
  virtual bool   doVetoPT(int iPos, const Event& event) {
    hasb = false;
    
    // looking for b quark
    for(int i =0; i<event.size(); i++){
      if( (event[i].particleDataEntry().nQuarksInCode(quarkID) > 0) || 
          (event[i].particleDataEntry().isOctetHadron() && 
           octetFlavour(event[i].id()) == quarkID)     
          ) {
        hasb = true;
      }
    }

    // Determine the veto decision
    if (!hasb) {
      decision_vetoPT = reject;
    } else {
      decision_vetoPT = keep;
    }

    // Enact the decsion if not in pass through mode
    if(passThrough){
      return keep;
    } else {
      return decision_vetoPT;
    }

  }

  bool getDecVetoMPIStep(){return decision_vetoMPIStep;}
  bool getDecVetoPT(){     return decision_vetoPT;}
private:

  bool        vetoMPIStep  = true; 
  bool        vetoPT       = true; 
  bool        passThrough  = false;

  // Save veto decisions
  bool        decision_vetoMPIStep = false;
  bool        decision_vetoPT      = false;
  
  const bool reject = true;
  const bool keep   = false;
  // Pointer to the info class
  const  Info* infoPtr;
  double       pTHat_val;
  bool         hasHardb;
  bool         hasb;
  double       pTHatThreshold = 1.0;
  int          quarkID        = 5;
};

// -----------------------------------------------------------------------------




int main(int argc, char *argv[]) {

  //****************************************************************************
  // Generator.
  //****************************************************************************
  
  Pythia pythia;
  
  // map to hold custom settings for use in this executable
  std::map<std::string,std::string> customSettings;

  //****************************************************************************
  // Set options from command line arguments
  //****************************************************************************
  // This bit of code reads the command line and processes them in the order given
  // Both .cmnd files and explicit requirements can be specified
  // Explicit requirements must start with --
  // An example would be:
  // ./main202 main202.cmnd --Main:numberOfEvents 10000 --Vincia:enhanceFacBottom 10
  
  std::cout << "Processing command line:"<<std::endl;
  for(int i = 0; i<argc; i++) std::cout << argv[i] << " ";
  std::cout << std::endl;

  // Loop over arguments
  for(int i = 1; i < argc; i++){
    std::string arg = argv[i];
    
    // If argument begins with '--' try and add it as a pythia setting
    if(arg.find("--") == 0){
      std::string property_name = arg;
      std::string property_value = "";

      // Remove '--' from string
      property_name.erase(0,2);

      // If the next argument doesn't begins with '--' use it as the property value
      if(i+1<argc && std::string(argv[i+1]).find("--") != 0){
        property_value = argv[i+1];

        // Increment i so that this value isn't used again
        i++;
      }
      
      std::cout << "Setting property: '" << property_name <<" = "<< property_value<<"'"<<std::endl;

      // Add to custom settings or pythia settings
      if(property_name.find("TomCustom:") != std::string::npos){
        customSettings[property_name] = property_value;
      } else {
        pythia.readString(Form("%s = %s",property_name.c_str(), property_value.c_str()));
      }
    } else if(arg.find(".cmnd") !=std::string::npos) {
      std::cout << "Reading Config: "<< arg<< std::endl;
      pythia.readFile(arg);

    } else {
      std::cout << "ignoring: "<< arg<< std::endl;
    }
  }

  
  //****************************************************************************
  // Switch on userhooks
  //****************************************************************************

  bool usingbUserHook       = (customSettings["TomCustom:usingbUserHook"     ]==""||customSettings["TomCustom:usingbUserHook"     ]=="off"?false:true);
  bool usingcUserHook       = (customSettings["TomCustom:usingcUserHook"     ]==""||customSettings["TomCustom:usingcUserHook"     ]=="off"?false:true);
  bool usingbcUserHook      = (customSettings["TomCustom:usingbcUserHook"    ]==""||customSettings["TomCustom:usingbcUserHook"    ]=="off"?false:true);
  bool usingccUserHook      = (customSettings["TomCustom:usingccUserHook"    ]==""||customSettings["TomCustom:usingccUserHook"    ]=="off"?false:true);
  bool usingbbccUserHook    = (customSettings["TomCustom:usingbbccUserHook"  ]==""||customSettings["TomCustom:usingbbccUserHook"  ]=="off"?false:true);
  bool usingMinnisUserHook  = (customSettings["TomCustom:usingMinnisUserHook"]==""||customSettings["TomCustom:usingMinnisUserHook"]=="off"?false:true);
  
  bool noVetoPT             = (customSettings["TomCustom:noVetoPT"           ]==""||customSettings["TomCustom:noVetoPT"           ]=="off"?false:true);
  bool noVetoMPIStep        = (customSettings["TomCustom:noVetoMPIStep"      ]==""||customSettings["TomCustom:noVetoMPIStep"      ]=="off"?false:true);
  bool noVetoParton         = (customSettings["TomCustom:noVetoParton"       ]==""||customSettings["TomCustom:noVetoParton"       ]=="off"?false:true);
  bool noVetoMass           = (customSettings["TomCustom:noVetoMass"         ]==""||customSettings["TomCustom:noVetoMass"         ]=="off"?false:true);
  bool passThrough          = (customSettings["TomCustom:passThrough"        ]==""||customSettings["TomCustom:passThrough"        ]=="off"?false:true);

  // Different defaults
  int    quarkID           = 5;
  double VetoPTScale       = 4.00;

  if(usingccUserHook){
    quarkID     = 4;
    VetoPTScale = 1.5;
  } else if(usingMinnisUserHook){

    quarkID     = 5;
    VetoPTScale = 4.00;
  }


  bool changeVetoPTScale    = (customSettings["TomCustom:VetoPTScale"]!="");
  if(changeVetoPTScale){
    VetoPTScale = std::stof(customSettings["TomCustom:VetoPTScale"]);
  }
  
  bool changequarkID    = (customSettings["TomCustom:quarkID"]!="");
  if(changequarkID){
    quarkID = std::stoi(customSettings["TomCustom:quarkID"]);
  }

  // Set up to do a user veto and send it in.
  auto mybUserHooks    = make_shared<MybUserHooks>(   &pythia.info);
  auto mycUserHooks    = make_shared<MycUserHooks>(   &pythia.info);
  auto mybcUserHooks   = make_shared<MybcUserHooks>(  &pythia.info,!noVetoMPIStep,!noVetoPT,!noVetoParton,!noVetoMass,VetoPTScale,passThrough);
  auto myccUserHooks   = make_shared<MyccUserHooks>(  &pythia.info,VetoPTScale,quarkID);
  auto mybbccUserHooks = make_shared<MybbccUserHooks>(&pythia.info);
  auto minnisUserHooks = make_shared<MinnisUserHooks>(&pythia.info,!noVetoMPIStep,!noVetoPT,VetoPTScale,quarkID,passThrough);

  if(usingbUserHook)      pythia.addUserHooksPtr( mybUserHooks);
  if(usingcUserHook)      pythia.addUserHooksPtr( mycUserHooks);
  if(usingccUserHook)     pythia.addUserHooksPtr( myccUserHooks);
  if(usingbbccUserHook)   pythia.addUserHooksPtr( mybbccUserHooks);
  if(usingbcUserHook)     pythia.addUserHooksPtr( mybcUserHooks);
  if(usingMinnisUserHook) pythia.addUserHooksPtr( minnisUserHooks);

  std::cout  << "****************************************************" << std::endl;
  if(noVetoPT)          std::cout  << "**** Using noVetoPT"            << std::endl;
  if(noVetoMPIStep)     std::cout  << "**** Using noVetoMPIStep"       << std::endl;
  if(noVetoParton)      std::cout  << "**** Using noVetoParton"        << std::endl;
  if(noVetoMass)        std::cout  << "**** Using noVetoMass"          << std::endl;
  if(passThrough)       std::cout  << "**** Using pass through mode"   << std::endl;
  if(changeVetoPTScale) std::cout  << "**** Using VetoPTScale "        << VetoPTScale<< std::endl;
  if(changequarkID)     std::cout  << "**** Using quarkID "            << quarkID << std::endl;
  std::cout  << "****************************************************" << std::endl;


  //****************************************************************************
  // Extract settings to be used in the main program.
  //****************************************************************************

  int nEvent         = pythia.settings.mode("Main:numberOfEvents");
  int showerModel    = pythia.settings.mode("PartonShowers:Model");
  bool hadronisation = pythia.settings.flag("HadronLevel:all");
  int  colrec        = pythia.settings.mode("ColourReconnection:mode");
  // double enh_bb      = pythia.settings.parm("Vincia:enhanceFacBottom");
  // double enh_cc      = pythia.settings.parm("Vincia:enhanceFacCharm");
  // double enh_ss      = pythia.settings.parm("Vincia:enhanceFacStrange");
  double pTHatMin    = pythia.settings.parm("PhaseSpace:pTHatMin");
  double pTHatMax    = pythia.settings.parm("PhaseSpace:pTHatMax");
  bool lhe_file      = pythia.settings.mode("Beams:frameType")==4;
  std::string lhe_name = pythia.settings.word("Beams:LHEF");
  
  bool isGenXicc = false;
  if(lhe_name.find("GENXICC")!= std::string::npos) {
    isGenXicc = true;
    std::cout << "Found GENXICC in lhe name" << std::endl;
  }
  bool debug         = (customSettings["TomCustom:Debug"]==""||customSettings["TomCustom:Debug"]=="off"?false:true);
  if(debug) MSG_DEBUG("Using DEBUG") 
  
  Event& event       = pythia.event;

  PartonSystems&   partonSystems = pythia.partonSystems;

  //****************************************************************************
  // Set output filename
  //****************************************************************************
  if(debug) MSG_DEBUG("creating output name")

  TString folder = "output/"; 
  TString name = "main202output"; 

  // Add model to name
  if(pythia.settings.flag("HardQCD:hardbbbar")) name = name + "_hardbbbar";
  if(pythia.settings.flag("HardQCD:gg2gg")    ) name = name + "_gg2gg";
  if(pythia.settings.flag("HardQCD:qg2qg")    ) name = name + "_qg2qg";
  if(pythia.settings.flag("SoftQCD:nonDiffractive")    ) name = name + "_SoftQCD_nd";
  if(lhe_file&&not isGenXicc) name = name + "_BcVegPy";
  if(lhe_file&&    isGenXicc) name = name + "_GenXicc";

  if(usingccUserHook)     name = name + "_UserHook_cc";
  if(usingbbccUserHook)   name = name + "_UserHook_bbcc";
  if(usingcUserHook)      name = name + "_UserHook_c";
  if(usingbcUserHook)     name = name + "_UserHook_bc";
  if(usingbUserHook)      name = name + "_UserHook_b";
  if(usingMinnisUserHook) name = name + "_UserHook_Minni";
  if(noVetoPT)            name = name + "_noVetoPT";
  if(noVetoMPIStep)       name = name + "_noVetoMPI";
  if(noVetoParton)        name = name + "_noVetoParton";
  if(noVetoMass)          name = name + "_noVetoMass";
  if(passThrough)         name = name + "_pass";
  if(changeVetoPTScale)   name = Form("%s_Scale_%f",name.Data(),VetoPTScale); 
  if(changequarkID)       name = Form("%s_Quark_%i",name.Data(),quarkID); 

  // Add nEvents to name
  name = name + Form("_N_%i",nEvent);

  // Add enhancement factor to name
  // if(enh_cc>1.0) {
  //   name = name+ Form("_enh_cc_%f",enh_cc);
  // }
  // if(enh_bb>1.0) {
  //   name = name+ Form("_enh_bb_%f",enh_bb);
  // }
  // if(enh_ss>1.0) {
  //   name = name+ Form("_enh_ss_%f",enh_ss);
  // }

  // if(pythia.settings.flag("Vincia:enhanceInBcRegion")) name = name + "_inBcReg";
  if(!pythia.settings.flag("PartonLevel:MPI")) name = name + "_noMPI";

  int nGluonToQuark = pythia.settings.mode("Vincia:nGluonToQuark");
  if(nGluonToQuark<5) name = name + Form("_GtQ%i",nGluonToQuark);
  
  int nFlavZeroMass = pythia.settings.mode("Vincia:nFlavZeroMass");
  if(nFlavZeroMass!=4) name = name + Form("_nFZMQ%i",nFlavZeroMass);
  
  int partonModel = pythia.settings.mode("PartonShowers:Model");
  if(partonModel!=2) name = name + Form("_PartMod%i",partonModel);
  
  if(colrec==0) name = name + "_noColRec";
  if(colrec==1) name = name + "_withColRec";
  // double  enhanceMPI = pythia.settings.parm("Vincia:enhanceMPI");
  // if(enhanceMPI!=1.0) name = name + Form("_EnhMPI_%.2f",enhanceMPI);

  // Add pThat to name
  if(pTHatMin!= 0.0||pTHatMax!=-1.0) folder = folder + Form("pThatMin_%f_Max_%f/",pTHatMin,pTHatMax);
  gSystem->Exec("mkdir -p "+folder);

  //****************************************************************************
  // Creating output file 
  //****************************************************************************
  if(debug) MSG_DEBUG("creating output file")
  
  // Make root file
  TString filename = folder+name+".root";
  TFile *outputfile = TFile::Open(filename,"recreate");
  cout << "Writing file: " <<filename <<std::endl;

  //****************************************************************************
  // Setup HEPMC writing
  //****************************************************************************

#ifdef HEPMC3
  bool printHepMC = false;

  TString hepmc_filename = filename;
  hepmc_filename.ReplaceAll(".root",".hepmc");

  // Interface for conversion from Pythia8::Event to HepMC one.
  HepMC3::Pythia8ToHepMC3 toHepMC;
  
  // Specify file where HepMC events will be stored.
  HepMC3::WriterAscii ascii_io(hepmc_filename.Data());
  
  // Switch off warnings for parton-level events.
  toHepMC.set_print_inconsistency(false);
  toHepMC.set_free_parton_warnings(false);
#endif


  //****************************************************************************
  // Try to find CPU speed
  //****************************************************************************

  double cpu_power = -99.0;

  // TString cpu_string = gSystem->GetFromPipe("env -i bash -lic \"source /cvmfs/lhcb.cern.ch/lib/LbEnv; lb-dirac dirac-wms-cpu-normalization\"");
  // if(cpu_string.Contains("Estimated CPU power is")){
  //   TObjArray* words = cpu_string.Tokenize(" ");
  //   for(int i = 0; i< words->GetEntries(); i++){  
  //     if(words->At(i)->GetName() == "HS06"){

  //       cpu_power = std::stof(words->At(i-1)->GetName());
  //       std::cout << "Found speed: " << cpu_power << std::endl;
  //     }
  //   }
  // }

  clock_t norm_start, norm_stop;

  norm_start = clock();

  TRandom3* norm_rand = new TRandom3(999);
  int n_shots = 1e8;
  double sum = 0.0;
  for(int i = 0; i< n_shots; i++){
    sum += norm_rand->Rndm();
  }
  norm_stop = clock();
  double norm_time = (double) (norm_stop-norm_start)/CLOCKS_PER_SEC;
  std::cout << " ========================= " <<  std::endl; 
  std::cout << " == TIMING TEST: " <<  norm_time << std::endl; 
  std::cout << " ========================= " <<  std::endl; 

  cpu_power = 1.0/norm_time;


  //****************************************************************************
  // Initialize
  //****************************************************************************
  if(debug) MSG_DEBUG("Initialize")

  if(!pythia.init()) { return EXIT_FAILURE; }

  //****************************************************************************
  // Create trees
  //****************************************************************************
  
  
  int    b_event_nBu;        
  int    b_event_nBu_p;        
  int    b_event_nBu_m;        
  int    b_event_nBd;        
  int    b_event_nBs;        
  int    b_event_nBc;        
  int    b_event_nBc_p;        
  int    b_event_nBc_m;        
  int    b_event_nBds;  
  int    b_event_nLb;  
  int    b_event_nDp;        
  int    b_event_nDp_p;        
  int    b_event_nDp_m;        
  int    b_event_nDz;        
  int    b_event_nDs;  
  int    b_event_nLc;

  int    b_event_nEtac;
  int    b_event_nJpsi;
  int    b_event_nChic2;

  int    b_event_nEtab;
  int    b_event_nUpsilon1S;
  int    b_event_nChib2;

  int    b_event_nSigmabm;
  int    b_event_nSigmab0;
  int    b_event_nSigmabp;
  int    b_event_nXibm;
  int    b_event_nXib0;
  int    b_event_nXiprimebm;
  int    b_event_nXiprimeb0;
  int    b_event_nOmegabm;
  int    b_event_nXibc0;
  int    b_event_nXibc0_p;
  int    b_event_nXibc0_m;
  int    b_event_nXibcp;
  int    b_event_nXibcp_p;
  int    b_event_nXibcp_m;
  int    b_event_nXiprimebc0;
  int    b_event_nXiprimebcp;
  int    b_event_nOmegabc0;
  int    b_event_nOmegabc0_p;
  int    b_event_nOmegabc0_m;
  int    b_event_nOmegaprimebc0;
  int    b_event_nOmegabccp;
  int    b_event_nXibbm;
  int    b_event_nXibb0;
  int    b_event_nOmegabbm;
  int    b_event_nOmegabbc0;
  int    b_event_nOmegabbbm;
  int    b_event_nSigmacpp;
  int    b_event_nSigmacp;
  int    b_event_nSigmac0;
  int    b_event_nXicp;
  int    b_event_nXic0;
  int    b_event_nXiprimecp;
  int    b_event_nXiprimec0;
  int    b_event_nOmegac0;
  int    b_event_nXiccp;
  int    b_event_nXiccp_p;
  int    b_event_nXiccp_m;
  int    b_event_nXiccpp;
  int    b_event_nXiccpp_p;
  int    b_event_nXiccpp_m;
  int    b_event_nOmegacc0;
  int    b_event_nOmegacccpp;


  int    b_event_nXb;
  int    b_event_nXc;

  int    b_event_nbquark;    
  int    b_event_nbquarkbar;     
  int    b_event_ncquark;    
  int    b_event_ncquarkbar;    
  double b_event_weight; 
  double b_event_pTHat;   
  bool   b_event_hasbStatus21;        
  bool   b_event_hasbStatus23;        
  bool   b_event_hasbStatus33;
  bool   b_event_hascStatus21;        
  bool   b_event_hascStatus23;        
  bool   b_event_hascStatus33;
  bool   b_event_hasbOnia;
  bool   b_event_hascOnia;
  bool   b_event_hasbOniaStatus23;
  bool   b_event_hascOniaStatus23;
  bool   b_event_hasbOniaStatus33;
  bool   b_event_hascOniaStatus33;
  int    b_event_hasbOniaStatus33_N;
  int    b_event_hascOniaStatus33_N;
  int    b_event_hasbStatus23_type;        
  int    b_event_hasbStatus33_N;
  int    b_event_hasbStatus33_type;
  int    b_event_hasbStatus33_type2;
  int    b_event_hascStatus23_type;        
  int    b_event_hascStatus33_N;
  int    b_event_hascStatus33_type;
  int    b_event_hascStatus33_type2;
  int    b_event_hasbOniaStatus23_type;
  int    b_event_hascOniaStatus23_type;
  int    b_event_hasbOniaStatus33_type;
  int    b_event_hascOniaStatus33_type;
  int    b_event_hasbOniaStatus33_type2;
  int    b_event_hascOniaStatus33_type2;
  bool   b_event_Jpsi;
  bool   b_event_Jpsi_isHard;
  bool   b_event_Jpsi_isMPI;
  bool   b_event_Jpsi_isDecay;
  bool   b_event_Jpsi_iscc;
  int    b_event_Jpsi_iscc_type;
  int    b_event_Jpsi_isHard_partSys;
  int    b_event_Jpsi_iscc_c_partSys;
  int    b_event_Jpsi_iscc_cbar_partSys;
  bool   b_event_Jpsi_isMixed;
  bool   b_event_Jpsi_isMixedLine;
  bool   b_event_Upsilon1S;
  bool   b_event_Upsilon1S_isHard;
  bool   b_event_Upsilon1S_isMPI;
  bool   b_event_Upsilon1S_isDecay;
  bool   b_event_Upsilon1S_iscc;
  int    b_event_Upsilon1S_iscc_type;
  int    b_event_Upsilon1S_isHard_partSys;
  int    b_event_Upsilon1S_iscc_c_partSys;
  int    b_event_Upsilon1S_iscc_cbar_partSys;
  bool   b_event_Upsilon1S_isMixed;
  bool   b_event_Upsilon1S_isMixedLine;

  double b_event_sigmaGen;
  double b_event_sigmaErr;
  int    b_event_nMPI;
  int    b_event_nMultiplicity;
  int    b_event_nCharged;
  int    b_event_nChargedInLHCb;
  double b_event_timeGen;
  double b_event_timeProc;
  double b_event_CPUPower;


  double b_event_jpsi_px;
  double b_event_jpsi_py;
  double b_event_jpsi_pz;
  double b_event_jpsi_pe;
  int    b_event_jpsi_ID;
  int    b_event_jpsi_c1_partSys;
  int    b_event_jpsi_c2_partSys;
  double b_event_upsilon1s_px;
  double b_event_upsilon1s_py;
  double b_event_upsilon1s_pz;
  double b_event_upsilon1s_pe;
  int    b_event_upsilon1s_ID;
  int    b_event_upsilon1s_b1_partSys;
  int    b_event_upsilon1s_b2_partSys;
  bool   b_event_bc_samePartSys;
  bool   b_event_bc_sysZero;
  int    b_event_bc_b_partSys;
  int    b_event_bc_c_partSys;
  double b_event_bc_px;
  double b_event_bc_py;
  double b_event_bc_pz;
  double b_event_bc_pe;
  int    b_event_bc_id;
  double b_event_bx_px;
  double b_event_bx_py;
  double b_event_bx_pz;
  double b_event_bx_pe;
  int    b_event_bx_id;
  int    b_event_bx_partSys;
  double b_event_dx_px;
  double b_event_dx_py;
  double b_event_dx_pz;
  double b_event_dx_pe;
  int    b_event_dx_id;
  int    b_event_dx_partSys;
  double b_event_dx2_px;
  double b_event_dx2_py;
  double b_event_dx2_pz;
  double b_event_dx2_pe;
  int    b_event_dx2_id;
  int    b_event_dx2_partSys;
  double b_event_bx2_px;
  double b_event_bx2_py;
  double b_event_bx2_pz;
  double b_event_bx2_pe;
  int    b_event_bx2_id;
  int    b_event_bx2_partSys;


  double b_event_Dp_p_px;
  double b_event_Dp_p_py;
  double b_event_Dp_p_pz;
  double b_event_Dp_p_pe;
  int    b_event_Dp_p_id;
  int    b_event_Dp_p_partSys;

  double b_event_Bu_p_px;
  double b_event_Bu_p_py;
  double b_event_Bu_p_pz;
  double b_event_Bu_p_pe;
  int    b_event_Bu_p_id;
  int    b_event_Bu_p_partSys;

  bool foundc;
  bool foundHardc;
  bool foundMPIc;
  bool foundShowerc;

  bool foundShowerHardFSRc;
  bool foundShowerHardISRc;
  bool foundShowerMPIFSRc;
  bool foundShowerMPIISRc;
  bool foundShowerBeamc;

  bool foundb;
  bool foundHardb;
  bool foundMPIb;
  bool foundShowerb;

  bool foundShowerHardFSRb;
  bool foundShowerHardISRb;
  bool foundShowerMPIFSRb;
  bool foundShowerMPIISRb;
  bool foundShowerBeamb;
  
  bool foundVetoLevelc;
  bool foundVetoLevelb;
  bool decMinnivetoMPI;
  bool decMinnivetoPT;

  bool decbcvetoMPI;
  bool decbcvetoPT;
  bool decbcvetoPart;
  bool decbcvetoMass;

  double delta_phi_b;
  double delta_phi_c;

  TTree *outputtree = new TTree("events","");
  outputtree->Branch("nBu"        , &b_event_nBu        ,"nBu/I"       );
  outputtree->Branch("nBu_p"      , &b_event_nBu_p      ,"nBu_p/I"     );
  outputtree->Branch("nBu_m"      , &b_event_nBu_m      ,"nBu_m/I"     );
  outputtree->Branch("nBd"        , &b_event_nBd        ,"nBd/I"       );
  outputtree->Branch("nBs"        , &b_event_nBs        ,"nBs/I"       );
  outputtree->Branch("nBc"        , &b_event_nBc        ,"nBc/I"       );
  outputtree->Branch("nBc_p"      , &b_event_nBc_p      ,"nBc_p/I"     );
  outputtree->Branch("nBc_m"      , &b_event_nBc_m      ,"nBc_m/I"     );
  outputtree->Branch("nBds"       , &b_event_nBds       ,"nBds/I"      );
  outputtree->Branch("nLb"        , &b_event_nLb        ,"nLb/I"       );
  outputtree->Branch("nDp"        , &b_event_nDp        ,"nDp/I"       );
  outputtree->Branch("nDp_p"      , &b_event_nDp_p     ,"nDp_p/I"      );
  outputtree->Branch("nDp_m"      , &b_event_nDp_m     ,"nDp_m/I"      );
  outputtree->Branch("nDz"        , &b_event_nDz        ,"nDz/I"       );
  outputtree->Branch("nDs"        , &b_event_nDs        ,"nDs/I"       );
  outputtree->Branch("nLc"        , &b_event_nLc        ,"nLc/I"       );

  outputtree->Branch("nEtac"      , &b_event_nEtac      , "nEtac/I"      );
  outputtree->Branch("nJpsi"      , &b_event_nJpsi      , "nJpsi/I"      );
  outputtree->Branch("nChic2"     , &b_event_nChic2     , "nChic2/I"     );

  outputtree->Branch("nEtab"      , &b_event_nEtab      , "nEtab/I"      );
  outputtree->Branch("nUpsilon1S" , &b_event_nUpsilon1S , "nUpsilon1S/I" );
  outputtree->Branch("nChib2"     , &b_event_nChib2     , "nChib2/I"     );

  outputtree->Branch("nSigmabm"      , &b_event_nSigmabm       , "nSigmabm/I"       );
  outputtree->Branch("nSigmab0"      , &b_event_nSigmab0       , "nSigmab0/I"       );
  outputtree->Branch("nSigmabp"      , &b_event_nSigmabp       , "nSigmabp/I"       );
  outputtree->Branch("nXibm"         , &b_event_nXibm          , "nXibm/I"          );
  outputtree->Branch("nXib0"         , &b_event_nXib0          , "nXib0/I"          );
  outputtree->Branch("nXiprimebm"    , &b_event_nXiprimebm     , "nXiprimebm/I"     );
  outputtree->Branch("nXiprimeb0"    , &b_event_nXiprimeb0     , "nXiprimeb0/I"     );
  outputtree->Branch("nOmegabm"      , &b_event_nOmegabm       , "nOmegabm/I"       );
  outputtree->Branch("nXibc0"        , &b_event_nXibc0         , "nXibc0/I"         );
  outputtree->Branch("nXibc0_p"      , &b_event_nXibc0_p       , "nXibc0_p/I"       );
  outputtree->Branch("nXibc0_m"      , &b_event_nXibc0_m       , "nXibc0_m/I"       );
  outputtree->Branch("nXibcp"        , &b_event_nXibcp         , "nXibcp/I"         );
  outputtree->Branch("nXibcp_p"      , &b_event_nXibcp_p       , "nXibcp_p/I"       );
  outputtree->Branch("nXibcp_m"      , &b_event_nXibcp_m       , "nXibcp_m/I"       );
  outputtree->Branch("nXiprimebc0"   , &b_event_nXiprimebc0    , "nXiprimebc0/I"    );
  outputtree->Branch("nXiprimebcp"   , &b_event_nXiprimebcp    , "nXiprimebcp/I"    );
  outputtree->Branch("nOmegabc0"     , &b_event_nOmegabc0      , "nOmegabc0/I"      );
  outputtree->Branch("nOmegabc0_p"   , &b_event_nOmegabc0_p    , "nOmegabc0_p/I"    );
  outputtree->Branch("nOmegabc0_m"   , &b_event_nOmegabc0_m    , "nOmegabc0_m/I"    );
  outputtree->Branch("nOmegaprimebc0", &b_event_nOmegaprimebc0 , "nOmegaprimebc0/I" );
  outputtree->Branch("nOmegabccp"    , &b_event_nOmegabccp     , "nOmegabccp/I"     );
  outputtree->Branch("nXibbm"        , &b_event_nXibbm         , "nXibbm/I"         );
  outputtree->Branch("nXibb0"        , &b_event_nXibb0         , "nXibb0/I"         );
  outputtree->Branch("nOmegabbm"     , &b_event_nOmegabbm      , "nOmegabbm/I"      );
  outputtree->Branch("nOmegabbc0"    , &b_event_nOmegabbc0     , "nOmegabbc0/I"     );
  outputtree->Branch("nOmegabbbm"    , &b_event_nOmegabbbm     , "nOmegabbbm/I"     );
  outputtree->Branch("nSigmacpp"     , &b_event_nSigmacpp      , "nSigmacpp/I"      );
  outputtree->Branch("nSigmacp"      , &b_event_nSigmacp       , "nSigmacp/I"       );
  outputtree->Branch("nSigmac0"      , &b_event_nSigmac0       , "nSigmac0/I"       );
  outputtree->Branch("nXicp"         , &b_event_nXicp          , "nXicp/I"          );
  outputtree->Branch("nXic0"         , &b_event_nXic0          , "nXic0/I"          );
  outputtree->Branch("nXiprimecp"    , &b_event_nXiprimecp     , "nXiprimecp/I"     );
  outputtree->Branch("nXiprimec0"    , &b_event_nXiprimec0     , "nXiprimec0/I"     );
  outputtree->Branch("nOmegac0"      , &b_event_nOmegac0       , "nOmegac0/I"       );
  outputtree->Branch("nXiccp"        , &b_event_nXiccp         , "nXiccp/I"         );
  outputtree->Branch("nXiccp_p"      , &b_event_nXiccp_p       , "nXiccp_p/I"       );
  outputtree->Branch("nXiccp_m"      , &b_event_nXiccp_m       , "nXiccp_m/I"       );
  outputtree->Branch("nXiccpp"       , &b_event_nXiccpp        , "nXiccpp/I"        );
  outputtree->Branch("nXiccpp_p"     , &b_event_nXiccpp_p      , "nXiccpp_p/I"      );
  outputtree->Branch("nXiccpp_m"     , &b_event_nXiccpp_m      , "nXiccpp_m/I"      );

  outputtree->Branch("nOmegacc0"     , &b_event_nOmegacc0      , "nOmegacc0/I"      );
  outputtree->Branch("nOmegacccpp"   , &b_event_nOmegacccpp    , "nOmegacccpp/I"    );

  outputtree->Branch("nXb"           , &b_event_nXb            , "nXb/I"            );
  outputtree->Branch("nXc"           , &b_event_nXc            , "nXc/I"            );

  outputtree->Branch("nbquark"    ,        &b_event_nbquark,            "nbquark/I"            );
  outputtree->Branch("nbquarkbar" ,        &b_event_nbquarkbar,         "nbquarkbar/I"         );
  outputtree->Branch("ncquark"    ,        &b_event_ncquark,            "ncquark/I"            );
  outputtree->Branch("ncquarkbar" ,        &b_event_ncquarkbar,         "ncquarkbar/I"         );
  outputtree->Branch("weight"     ,        &b_event_weight,             "weight/D"             );
  outputtree->Branch("pTHat"      ,        &b_event_pTHat,              "pTHat/D"              );
  outputtree->Branch("hasbStatus21",       &b_event_hasbStatus21,       "hasbStatus21/O"       );
  outputtree->Branch("hasbStatus23",       &b_event_hasbStatus23,       "hasbStatus23/O"       );
  outputtree->Branch("hasbStatus33",       &b_event_hasbStatus33,       "hasbStatus33/O"       );
  outputtree->Branch("hascStatus21",       &b_event_hascStatus21,       "hascStatus21/O"       );
  outputtree->Branch("hascStatus23",       &b_event_hascStatus23,       "hascStatus23/O"       );
  outputtree->Branch("hascStatus33",       &b_event_hascStatus33,       "hascStatus33/O"       );
  outputtree->Branch("hascOnia",           &b_event_hascOnia,           "hascOnia/O"           );
  outputtree->Branch("hasbOnia",           &b_event_hasbOnia,           "hasbOnia/O"           );
  outputtree->Branch("hasbOniaStatus23",   &b_event_hasbOniaStatus23,   "hasbOniaStatus23/O"   );
  outputtree->Branch("hascOniaStatus23",   &b_event_hascOniaStatus23,   "hascOniaStatus23/O"   );
  outputtree->Branch("hasbOniaStatus33",   &b_event_hasbOniaStatus33,   "hasbOniaStatus33/O"   );
  outputtree->Branch("hascOniaStatus33",   &b_event_hascOniaStatus33,   "hascOniaStatus33/O"   );
  outputtree->Branch("hasbOniaStatus33_N", &b_event_hasbOniaStatus33_N, "hasbOniaStatus33_N/I" );
  outputtree->Branch("hascOniaStatus33_N", &b_event_hascOniaStatus33_N, "hascOniaStatus33_N/I" );
  outputtree->Branch("hasbStatus23_type",  &b_event_hasbStatus23_type,  "hasbStatus23_type/I"  );
  outputtree->Branch("hasbStatus33_type",  &b_event_hasbStatus33_type,  "hasbStatus33_type/I"  );
  outputtree->Branch("hasbStatus33_N",     &b_event_hasbStatus33_N,     "hasbStatus33_N/I"     );
  outputtree->Branch("hasbStatus33_type2", &b_event_hasbStatus33_type2, "hasbStatus33_type2/I" );
  outputtree->Branch("hascStatus23_type",  &b_event_hascStatus23_type,  "hascStatus23_type/I"  );
  outputtree->Branch("hascStatus33_type",  &b_event_hascStatus33_type,  "hascStatus33_type/I"  );
  outputtree->Branch("hascStatus33_N",     &b_event_hascStatus33_N,     "hascStatus33_N/I"     );
  outputtree->Branch("hascStatus33_type2", &b_event_hascStatus33_type2, "hascStatus33_type2/I" );
  outputtree->Branch("hasbOniaStatus23_type",&b_event_hasbOniaStatus23_type,"hasbOniaStatus23_type/I");
  outputtree->Branch("hascOniaStatus23_type",&b_event_hascOniaStatus23_type,"hascOniaStatus23_type/I");
  outputtree->Branch("hasbOniaStatus33_type",&b_event_hasbOniaStatus33_type,"hasbOniaStatus33_type/I");
  outputtree->Branch("hascOniaStatus33_type",&b_event_hascOniaStatus33_type,"hascOniaStatus33_type/I");
  outputtree->Branch("hasbOniaStatus33_type2",&b_event_hasbOniaStatus33_type2,"hasbOniaStatus33_type2/I");
  outputtree->Branch("hascOniaStatus33_type2",&b_event_hascOniaStatus33_type2,"hascOniaStatus33_type2/I");
  outputtree->Branch("Jpsi",               &b_event_Jpsi,               "Jpsi/O"               );
  outputtree->Branch("Jpsi_isHard",        &b_event_Jpsi_isHard,        "Jpsi_isHard/O"        );
  outputtree->Branch("Jpsi_isMPI",         &b_event_Jpsi_isMPI,         "Jpsi_isMPI/O"         );
  outputtree->Branch("Jpsi_isDecay",       &b_event_Jpsi_isDecay,       "Jpsi_isDecay/O"       );
  outputtree->Branch("Jpsi_iscc",          &b_event_Jpsi_iscc,          "Jpsi_iscc/O"          );
  outputtree->Branch("Jpsi_iscc_type",     &b_event_Jpsi_iscc_type,     "Jpsi_iscc_type/I"     );
  outputtree->Branch("Jpsi_isHard_partSys",    &b_event_Jpsi_isHard_partSys,   "Jpsi_isHard_partSys/I"     );
  outputtree->Branch("Jpsi_iscc_c_partSys",    &b_event_Jpsi_iscc_c_partSys,   "Jpsi_iscc_c_partSys/I"     );
  outputtree->Branch("Jpsi_iscc_cbar_partSys", &b_event_Jpsi_iscc_cbar_partSys,"Jpsi_iscc_cbar_partSys/I"  );

  outputtree->Branch("Jpsi_isMixed",       &b_event_Jpsi_isMixed,       "Jpsi_isMixed/O"       );
  outputtree->Branch("Jpsi_isMixedLine",   &b_event_Jpsi_isMixedLine,   "Jpsi_isMixedLine/O"   );
  outputtree->Branch("Upsilon1S",            &b_event_Upsilon1S,            "Upsilon1S/O"               );
  outputtree->Branch("Upsilon1S_isHard",     &b_event_Upsilon1S_isHard,     "Upsilon1S_isHard/O"        );
  outputtree->Branch("Upsilon1S_isMPI",      &b_event_Upsilon1S_isMPI,      "Upsilon1S_isMPI/O"         );
  outputtree->Branch("Upsilon1S_isDecay",    &b_event_Upsilon1S_isDecay,    "Upsilon1S_isDecay/O"       );
  outputtree->Branch("Upsilon1S_iscc",       &b_event_Upsilon1S_iscc,       "Upsilon1S_iscc/O"          );
  outputtree->Branch("Upsilon1S_iscc_type",  &b_event_Upsilon1S_iscc_type,  "Upsilon1S_iscc_type/I"     );
  outputtree->Branch("Upsilon1S_isHard_partSys",    &b_event_Upsilon1S_isHard_partSys,   "Upsilon1S_isHard_partSys/I"     );
  outputtree->Branch("Upsilon1S_iscc_c_partSys",    &b_event_Upsilon1S_iscc_c_partSys,   "Upsilon1S_iscc_c_partSys/I"     );
  outputtree->Branch("Upsilon1S_iscc_cbar_partSys", &b_event_Upsilon1S_iscc_cbar_partSys,"Upsilon1S_iscc_cbar_partSys/I"  );
  outputtree->Branch("Upsilon1S_isMixed",    &b_event_Upsilon1S_isMixed,    "Upsilon1S_isMixed/O"       );
  outputtree->Branch("Upsilon1S_isMixedLine",&b_event_Upsilon1S_isMixedLine,"Upsilon1S_isMixedLine/O"   );

  outputtree->Branch("sigmaGen"   ,        &b_event_sigmaGen,           "sigmaGen/D"           );
  outputtree->Branch("sigmaErr"   ,        &b_event_sigmaErr,           "sigmaErr/D"           );
  outputtree->Branch("nMPI"       ,        &b_event_nMPI,               "nMPI/I"               );
  outputtree->Branch("nMultiplicity",      &b_event_nMultiplicity,      "nMultiplicity/I"      );
  outputtree->Branch("nCharged",           &b_event_nCharged,           "nCharged/I"           );
  outputtree->Branch("nChargedInLHCb",     &b_event_nChargedInLHCb,     "nChargedInLHCb/I"     );
  outputtree->Branch("timeGen",            &b_event_timeGen,            "timeGen/D"            );
  outputtree->Branch("timeProc",           &b_event_timeProc,           "timeProc/D"           );
  outputtree->Branch("CPUPower",           &b_event_CPUPower,           "CPUPower/D"           );


  outputtree->Branch("jpsi_px"   , &b_event_jpsi_px   ,"jpsi_px/D"   );
  outputtree->Branch("jpsi_py"   , &b_event_jpsi_py   ,"jpsi_py/D"   );
  outputtree->Branch("jpsi_pz"   , &b_event_jpsi_pz   ,"jpsi_pz/D"   );
  outputtree->Branch("jpsi_pe"   , &b_event_jpsi_pe   ,"jpsi_pe/D"   );
  outputtree->Branch("jpsi_ID"   , &b_event_jpsi_ID   ,"jpsi_ID/I"   );
  outputtree->Branch("jpsi_c1_partSys"   , &b_event_jpsi_c1_partSys   ,"jpsi_c1_partSys/I"   );
  outputtree->Branch("jpsi_c2_partSys"   , &b_event_jpsi_c2_partSys   ,"jpsi_c2_partSys/I"   );
  outputtree->Branch("upsilon1s_px"   , &b_event_upsilon1s_px   ,"upsilon1s_px/D"   );
  outputtree->Branch("upsilon1s_py"   , &b_event_upsilon1s_py   ,"upsilon1s_py/D"   );
  outputtree->Branch("upsilon1s_pz"   , &b_event_upsilon1s_pz   ,"upsilon1s_pz/D"   );
  outputtree->Branch("upsilon1s_pe"   , &b_event_upsilon1s_pe   ,"upsilon1s_pe/D"   );
  outputtree->Branch("upsilon1s_ID"   , &b_event_upsilon1s_ID   ,"upsilon1s_ID/I"   );
  outputtree->Branch("upsilon1s_b1_partSys"   , &b_event_upsilon1s_b1_partSys   ,"upsilon1s_b1_partSys/I"   );
  outputtree->Branch("upsilon1s_b2_partSys"   , &b_event_upsilon1s_b2_partSys   ,"upsilon1s_b2_partSys/I"   );

  outputtree->Branch("bc_px"   , &b_event_bc_px   ,"bc_px/D"   );
  outputtree->Branch("bc_py"   , &b_event_bc_py   ,"bc_py/D"   );
  outputtree->Branch("bc_pz"   , &b_event_bc_pz   ,"bc_pz/D"   );
  outputtree->Branch("bc_pe"   , &b_event_bc_pe   ,"bc_pe/D"   );
  outputtree->Branch("bc_id"   , &b_event_bc_id   ,"bc_id/I"   );
  outputtree->Branch("bc_samePartSys" , &b_event_bc_samePartSys ,"bc_samePartSys/O" );
  outputtree->Branch("bc_sysZero"     , &b_event_bc_sysZero     ,"bc_sysZero/O"     );
  outputtree->Branch("bc_b_partSys"   , &b_event_bc_b_partSys   ,"bc_b_partSys/I"   );
  outputtree->Branch("bc_c_partSys"   , &b_event_bc_c_partSys   ,"bc_c_partSys/I"   );

  outputtree->Branch("bx_px"   , &b_event_bx_px   ,"bx_px/D"   );
  outputtree->Branch("bx_py"   , &b_event_bx_py   ,"bx_py/D"   );
  outputtree->Branch("bx_pz"   , &b_event_bx_pz   ,"bx_pz/D"   );
  outputtree->Branch("bx_pe"   , &b_event_bx_pe   ,"bx_pe/D"   );
  outputtree->Branch("bx_id"   , &b_event_bx_id   ,"bx_id/I"   );
  outputtree->Branch("bx_partSys" , &b_event_bx_partSys   ,"bx_partSys/I"   );

  outputtree->Branch("dx_px"   , &b_event_dx_px   ,"dx_px/D"   );
  outputtree->Branch("dx_py"   , &b_event_dx_py   ,"dx_py/D"   );
  outputtree->Branch("dx_pz"   , &b_event_dx_pz   ,"dx_pz/D"   );
  outputtree->Branch("dx_pe"   , &b_event_dx_pe   ,"dx_pe/D"   );
  outputtree->Branch("dx_id"   , &b_event_dx_id   ,"dx_id/I"   );
  outputtree->Branch("dx_partSys" , &b_event_dx_partSys   ,"dx_partSys/I"   );

  outputtree->Branch("dx2_px"  , &b_event_dx2_px  ,"dx2_px/D"  );
  outputtree->Branch("dx2_py"  , &b_event_dx2_py  ,"dx2_py/D"  );
  outputtree->Branch("dx2_pz"  , &b_event_dx2_pz  ,"dx2_pz/D"  );
  outputtree->Branch("dx2_pe"  , &b_event_dx2_pe  ,"dx2_pe/D"  );
  outputtree->Branch("dx2_id"  , &b_event_dx2_id  ,"dx2_id/I"  );
  outputtree->Branch("dx2_partSys", &b_event_dx2_partSys  ,"dx2_partSys/I"  );

  outputtree->Branch("bx2_px"  , &b_event_bx2_px  ,"bx2_px/D"  );
  outputtree->Branch("bx2_py"  , &b_event_bx2_py  ,"bx2_py/D"  );
  outputtree->Branch("bx2_pz"  , &b_event_bx2_pz  ,"bx2_pz/D"  );
  outputtree->Branch("bx2_pe"  , &b_event_bx2_pe  ,"bx2_pe/D"  );
  outputtree->Branch("bx2_id"  , &b_event_bx2_id  ,"bx2_id/I"  );
  outputtree->Branch("bx2_partSys", &b_event_bx2_partSys  ,"bx2_partSys/I"  );



  outputtree->Branch("Dp_p_px"  , &b_event_Dp_p_px  ,"Dp_p_px/D"  );
  outputtree->Branch("Dp_p_py"  , &b_event_Dp_p_py  ,"Dp_p_py/D"  );
  outputtree->Branch("Dp_p_pz"  , &b_event_Dp_p_pz  ,"Dp_p_pz/D"  );
  outputtree->Branch("Dp_p_pe"  , &b_event_Dp_p_pe  ,"Dp_p_pe/D"  );
  outputtree->Branch("Dp_p_id"  , &b_event_Dp_p_id  ,"Dp_p_id/I"  );
  outputtree->Branch("Dp_p_partSys", &b_event_Dp_p_partSys  ,"Dp_p_partSys/I"  );

  outputtree->Branch("Bu_p_px"  , &b_event_Bu_p_px  ,"Bu_p_px/D"  );
  outputtree->Branch("Bu_p_py"  , &b_event_Bu_p_py  ,"Bu_p_py/D"  );
  outputtree->Branch("Bu_p_pz"  , &b_event_Bu_p_pz  ,"Bu_p_pz/D"  );
  outputtree->Branch("Bu_p_pe"  , &b_event_Bu_p_pe  ,"Bu_p_pe/D"  );
  outputtree->Branch("Bu_p_id"  , &b_event_Bu_p_id  ,"Bu_p_id/I"  );
  outputtree->Branch("Bu_p_partSys", &b_event_Bu_p_partSys  ,"Bu_p_partSys/I"  );

  outputtree->Branch("foundc"              , &foundc               , "foundc/O"              );
  outputtree->Branch("foundHardc"          , &foundHardc           , "foundHardc/O"          );
  outputtree->Branch("foundMPIc"           , &foundMPIc            , "foundMPIc/O"           );
  outputtree->Branch("foundShowerc"        , &foundShowerc         , "foundShowerc/O"        );
  outputtree->Branch("foundShowerHardFSRc" , &foundShowerHardFSRc  , "foundShowerHardFSRc/O" );
  outputtree->Branch("foundShowerHardISRc" , &foundShowerHardISRc  , "foundShowerHardISRc/O" );
  outputtree->Branch("foundShowerMPIFSRc"  , &foundShowerMPIFSRc   , "foundShowerMPIFSRc/O"  );
  outputtree->Branch("foundShowerMPIISRc"  , &foundShowerMPIISRc   , "foundShowerMPIISRc/O"  );
  outputtree->Branch("foundShowerBeamc"    , &foundShowerBeamc     , "foundShowerBeamc/O"    );

  outputtree->Branch("foundb"              , &foundb               , "foundb/O"              );
  outputtree->Branch("foundHardb"          , &foundHardb           , "foundHardb/O"          );
  outputtree->Branch("foundMPIb"           , &foundMPIb            , "foundMPIb/O"           );
  outputtree->Branch("foundShowerb"        , &foundShowerb         , "foundShowerb/O"        );
  outputtree->Branch("foundShowerHardFSRb" , &foundShowerHardFSRb  , "foundShowerHardFSRb/O" );
  outputtree->Branch("foundShowerHardISRb" , &foundShowerHardISRb  , "foundShowerHardISRb/O" );
  outputtree->Branch("foundShowerMPIFSRb"  , &foundShowerMPIFSRb   , "foundShowerMPIFSRb/O"  );
  outputtree->Branch("foundShowerMPIISRb"  , &foundShowerMPIISRb   , "foundShowerMPIISRb/O"  );
  outputtree->Branch("foundShowerBeamb"    , &foundShowerBeamb     , "foundShowerBeamb/O"    );

  outputtree->Branch("foundVetoLevelc"               , &foundVetoLevelc                , "foundVetoLevelc/O"               );
  outputtree->Branch("foundVetoLevelb"               , &foundVetoLevelb                , "foundVetoLevelb/O"               );
  outputtree->Branch("decMinnivetoMPI"               , &decMinnivetoMPI                , "decMinnivetoMPI/O"               );
  outputtree->Branch("decMinnivetoPT"                , &decMinnivetoPT                 , "decMinnivetoPT/O"                );

  outputtree->Branch("decbcvetoMPI"               , &decbcvetoMPI                , "decbcvetoMPI/O"               );
  outputtree->Branch("decbcvetoPT"                , &decbcvetoPT                 , "decbcvetoPT/O"                );
  outputtree->Branch("decbcvetoMPI"               , &decbcvetoMPI                , "decbcvetoMPI/O"               );
  outputtree->Branch("decbcvetoPT"                , &decbcvetoPT                 , "decbcvetoPT/O"                );

  outputtree->Branch("delta_phi_b"    , &delta_phi_b     , "delta_phi_b/D"    );
  outputtree->Branch("delta_phi_c"    , &delta_phi_c     , "delta_phi_c/D"    );

  outputtree->Branch("delta_phi_b"    , &delta_phi_b     , "delta_phi_b/D"    );
  outputtree->Branch("delta_phi_c"    , &delta_phi_c     , "delta_phi_c/D"    );

  double b_event_bcvegpy_bc_px;
  double b_event_bcvegpy_bc_py;
  double b_event_bcvegpy_bc_pz;
  double b_event_bcvegpy_bc_pe;
  double b_event_bcvegpy_b_px;
  double b_event_bcvegpy_b_py;
  double b_event_bcvegpy_b_pz;
  double b_event_bcvegpy_b_pe;
  double b_event_bcvegpy_c_px;
  double b_event_bcvegpy_c_py;
  double b_event_bcvegpy_c_pz;
  double b_event_bcvegpy_c_pe;

  if(lhe_file){
    outputtree->Branch("bcvegpy_bc_px"    , &b_event_bcvegpy_bc_px     , "bcvegpy_bc_px/D"    );
    outputtree->Branch("bcvegpy_bc_py"    , &b_event_bcvegpy_bc_py     , "bcvegpy_bc_py/D"    );
    outputtree->Branch("bcvegpy_bc_pz"    , &b_event_bcvegpy_bc_pz     , "bcvegpy_bc_pz/D"    );
    outputtree->Branch("bcvegpy_bc_pe"    , &b_event_bcvegpy_bc_pe     , "bcvegpy_bc_pe/D"    );
   
    outputtree->Branch("bcvegpy_b_px"    , &b_event_bcvegpy_b_px     , "bcvegpy_b_px/D"    );
    outputtree->Branch("bcvegpy_b_py"    , &b_event_bcvegpy_b_py     , "bcvegpy_b_py/D"    );
    outputtree->Branch("bcvegpy_b_pz"    , &b_event_bcvegpy_b_pz     , "bcvegpy_b_pz/D"    );
    outputtree->Branch("bcvegpy_b_pe"    , &b_event_bcvegpy_b_pe     , "bcvegpy_b_pe/D"    );
   
    outputtree->Branch("bcvegpy_c_px"    , &b_event_bcvegpy_c_px     , "bcvegpy_c_px/D"    );
    outputtree->Branch("bcvegpy_c_py"    , &b_event_bcvegpy_c_py     , "bcvegpy_c_py/D"    );
    outputtree->Branch("bcvegpy_c_pz"    , &b_event_bcvegpy_c_pz     , "bcvegpy_c_pz/D"    );
    outputtree->Branch("bcvegpy_c_pe"    , &b_event_bcvegpy_c_pe     , "bcvegpy_c_pe/D"    );
  }
  double b_parts_eta;        
  double b_parts_pT;        
  double b_parts_id;        
  double b_parts_event;        
  double b_parts_particle;        
  double b_parts_isFinal;         
  double b_parts_b_plus_c_mass;           
  double b_parts_b_plus_c_mass_all;           
  double b_parts_bc_sideband;
  bool   b_parts_con_to_c;        
  bool   b_parts_isBc;        
  bool   b_parts_isBcstar;        
  double b_parts_weight;        
  bool   b_parts_iscolconnected;        
  bool   b_parts_fromBcstar;        
  bool   b_parts_isMPI;              
  int    b_parts_status;              

  // TTree *outputparticles = new TTree("parts","");
  // outputparticles->Branch("eta"                 , &b_parts_eta               , "eta/D"               );
  // outputparticles->Branch("pT"                  , &b_parts_pT                , "pT/D"                );
  // outputparticles->Branch("id"                  , &b_parts_id                , "id/D"                );
  // outputparticles->Branch("event"               , &b_parts_event             , "event/D"             );
  // outputparticles->Branch("particle"            , &b_parts_particle          , "particle/D"          );
  // outputparticles->Branch("isFinal"             , &b_parts_isFinal           , "isFinal/D"           );
  // outputparticles->Branch("weight"              , &b_parts_weight            , "weight/D"            );
  // outputparticles->Branch("b_plus_c_mass"       , &b_parts_b_plus_c_mass     , "b_plus_c_mass/D"     );
  // outputparticles->Branch("b_plus_c_mass_all"   , &b_parts_b_plus_c_mass_all , "b_plus_c_mass_all/D" );
  // outputparticles->Branch("bc_sideband"         , &b_parts_bc_sideband       , "bc_sideband/D"       );
  // outputparticles->Branch("con_to_c"            , &b_parts_con_to_c          , "con_to_c/O"          );
  // outputparticles->Branch("isBc"                , &b_parts_isBc              , "isBc/O"              );
  // outputparticles->Branch("isBcstar"            , &b_parts_isBcstar          , "isBcstar/O"          );
  // outputparticles->Branch("iscolconnected"      , &b_parts_iscolconnected    , "iscolconnected/O"    );
  // outputparticles->Branch("fromBcstar"          , &b_parts_fromBcstar        , "fromBcstar/O"        );
  // outputparticles->Branch("isMPI"               , &b_parts_isMPI             , "isMPI/O"             );
  // outputparticles->Branch("status"              , &b_parts_status            , "status/I"            );
  // outputparticles->Branch("hasbStatus21"        , &b_event_hasbStatus21      , "hasbStatus21/O"      );
  // outputparticles->Branch("hasbStatus23"        , &b_event_hasbStatus23      , "hasbStatus23/O"      );
  // outputparticles->Branch("hasbStatus33"        , &b_event_hasbStatus33      , "hasbStatus33/O"      );
  // outputparticles->Branch("hascStatus21"        , &b_event_hascStatus21      , "hascStatus21/O"      );
  // outputparticles->Branch("hascStatus23"        , &b_event_hascStatus23      , "hascStatus23/O"      );
  // outputparticles->Branch("hascStatus33"        , &b_event_hascStatus33      , "hascStatus33/O"      );
  // outputparticles->Branch("hasbStatus23_type"   , &b_event_hasbStatus23_type , "hasbStatus23_type/I" );
  // outputparticles->Branch("hasbStatus33_type"   , &b_event_hasbStatus33_type , "hasbStatus33_type/I" );
  // outputparticles->Branch("hasbStatus33_type2"  , &b_event_hasbStatus33_type2, "hasbStatus33_type2/I");
  // outputparticles->Branch("hasbStatus33_N"      , &b_event_hasbStatus33_N    , "hasbStatus33_N/I"    );
  // outputparticles->Branch("hascStatus23_type"   , &b_event_hascStatus23_type , "hascStatus23_type/I" );
  // outputparticles->Branch("hascStatus33_type"   , &b_event_hascStatus33_type , "hascStatus33_type/I" );
  // outputparticles->Branch("hascStatus33_type2"  , &b_event_hascStatus33_type2, "hascStatus33_type2/I");
  // outputparticles->Branch("hascStatus33_N"      , &b_event_hascStatus33_N    , "hascStatus33_N/I"    );
  //****************************************************************************
  // Measure the cpu runtime.
  //****************************************************************************

  clock_t start, stop;
  double t = 0.0;
  double t_total_gen = 0.0;
  double t_total_proc = 0.0;
  start = clock();


  //****************************************************************************
  // Begin event loop. Generate event. Abort if error.
  //****************************************************************************
  if(debug) MSG_DEBUG("Begin Event loop")

  // Allow for possibility of a few faulty events.
  int nAbort = 10000;
  int iAbort = 0;

  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {
    if(debug) MSG_DEBUG("Event " << iEvent)

    clock_t start_event_proc, stop_event_proc;
    clock_t start_event_gen, stop_event_gen;
    start_event_gen = clock();

    if(debug) MSG_DEBUG("Call pythia.next()")
    
    while(!pythia.next()){
      // cout << " next is false!" << std::endl;
      if(pythia.info.atEndOfFile()){
        cout << " at end of lhe file! exiting while loop" << std::endl;
        break;
      }
    }

    if(pythia.info.atEndOfFile()){
      cout << " at end of lhe file! exiting for loop" << std::endl;
      break;
    }

    // cout << " continuing..." << std::endl;

    // if (!pythia.next()) {
    //   cout << " Event generation aborted prematurely, owing to error!\n";
    //   cout<< "Event number was : "<<iEvent<<endl;
    //   // First few failures write off as "acceptable" errors, then quit.
    //   if (++iAbort < nAbort) {
    //     cout<< "    "<<iAbort <<"/"<<nAbort << " continue..." <<endl;
    //     --iEvent;
    //     continue;
    //   } else {
    //     cout<< "    "<<iAbort <<"/"<<nAbort << " break..." <<endl;
    //     break;
    //   }

    // }
    if(debug) MSG_DEBUG("Done pythia.next()")

    stop_event_gen   = clock();
    start_event_proc = clock();

    // Check for weights
    double weight = pythia.info.weight();

    // =========================================================================
    // Fill HEPMC event
    // =========================================================================
    if(debug) MSG_DEBUG("Fill HEPMC3 event")

#ifdef HEPMC3    
    // Construct new empty HepMC event.
    HepMC3::GenEvent hepmcevt;
    // Set event weight
    hepmcevt.weights().push_back(weight);
    // Fill HepMC event
    toHepMC.fill_next_event( pythia, &hepmcevt );
    
    if(printHepMC) ascii_io.write_event(hepmcevt);
#endif

    // =========================================================================
    // First, loop over the particles in event to determine what kind of event it is 
    // =========================================================================
    if(debug) MSG_DEBUG("Determine what type of event it is")

    std::vector<int> status_33_particles;
    

    b_event_hasbStatus21      = false;
    b_event_hasbStatus23      = false;
    b_event_hasbStatus33      = false;
    b_event_hascStatus21      = false;
    b_event_hascStatus23      = false;
    b_event_hascStatus33      = false;
    
    b_event_hascOnia          = false;
    b_event_hasbOnia          = false;
    b_event_hasbOniaStatus23  = false;
    b_event_hascOniaStatus23  = false;
    b_event_hasbOniaStatus33  = false;
    b_event_hascOniaStatus33  = false;
    b_event_hasbOniaStatus33_N= 0;
    b_event_hascOniaStatus33_N= 0;

    b_event_hasbStatus23_type = 0;
    b_event_hascStatus23_type = 0;
    b_event_hasbStatus33_type = 0;
    b_event_hascStatus33_type = 0;
    b_event_hasbStatus33_type2 = 0;
    b_event_hascStatus33_type2 = 0;
    b_event_hasbStatus33_N     = 0;
    b_event_hascStatus33_N     = 0;
    b_event_hasbOniaStatus23_type= 0;
    b_event_hascOniaStatus23_type= 0;
    b_event_hasbOniaStatus33_type= 0;
    b_event_hascOniaStatus33_type= 0;
    b_event_hasbOniaStatus33_type2= 0;
    b_event_hascOniaStatus33_type2= 0;

    b_event_Jpsi         = false;
    b_event_Jpsi_isHard  = false;
    b_event_Jpsi_isMPI   = false;
    b_event_Jpsi_iscc    = false;
    b_event_Jpsi_iscc_type = 0;
    b_event_Jpsi_isHard_partSys    = -9999;
    b_event_Jpsi_iscc_c_partSys    = -9999;
    b_event_Jpsi_iscc_cbar_partSys = -9999;
    b_event_Jpsi_isDecay = false;
    b_event_Jpsi_isMixed = false;
    b_event_Jpsi_isMixedLine = false;

    b_event_Upsilon1S         = false;
    b_event_Upsilon1S_isHard  = false;
    b_event_Upsilon1S_isMPI   = false;
    b_event_Upsilon1S_iscc    = false;
    b_event_Upsilon1S_iscc_type = 0;
    b_event_Upsilon1S_isHard_partSys    = -9999;
    b_event_Upsilon1S_iscc_c_partSys    = -9999;
    b_event_Upsilon1S_iscc_cbar_partSys = -9999;
    b_event_Upsilon1S_isDecay = false;
    b_event_Upsilon1S_isMixed = false;
    b_event_Upsilon1S_isMixedLine = false;

    foundc                    = false;
    foundHardc                = false;
    foundMPIc                 = false;
    foundShowerc              = false;

    foundShowerHardFSRc       = false;
    foundShowerHardISRc       = false;
    foundShowerMPIFSRc        = false;
    foundShowerMPIISRc        = false;
    foundShowerBeamc          = false;


    foundb                    = false;
    foundHardb                = false;
    foundMPIb                 = false;
    foundShowerb              = false;

    foundShowerHardFSRb       = false;
    foundShowerHardISRb       = false;
    foundShowerMPIFSRb        = false;
    foundShowerMPIISRb        = false;
    foundShowerBeamb          = false;

    decMinnivetoMPI = minnisUserHooks->getDecVetoMPIStep();
    decMinnivetoPT  = minnisUserHooks->getDecVetoPT();

    decbcvetoMPI   = mybcUserHooks->getDecVetoMPIStep();
    decbcvetoPT    = mybcUserHooks->getDecVetoPT();
    decbcvetoPart  = mybcUserHooks->getDecVetoMass();
    decbcvetoMass  = mybcUserHooks->getDecVetoParton();

    delta_phi_b = -999;
    delta_phi_c = -999;

    int temp_23charmonium_id  = 0;
    int temp_23bottomonium_id = 0;

    if(debug) MSG_DEBUG("Begin Particle loop")
    for (int i=1; i<event.size(); ++i) {

      if (abs(event[i].status())==21 && event[i].idAbs()==5)  b_event_hasbStatus21 = true;
      if (abs(event[i].status())==23 && event[i].idAbs()==5)  b_event_hasbStatus23 = true;
      if (abs(event[i].status())==33 && event[i].idAbs()==5)  b_event_hasbStatus33 = true;
      if (abs(event[i].status())==21 && event[i].idAbs()==4)  b_event_hascStatus21 = true;
      if (abs(event[i].status())==23 && event[i].idAbs()==4)  b_event_hascStatus23 = true;
      if (abs(event[i].status())==33 && event[i].idAbs()==4)  b_event_hascStatus33 = true;
      
      bool containsCharmonium = false;
      
      // Check for normal onium 
      if( event[i].particleDataEntry().isOnium() && 
          event[i].particleDataEntry().nQuarksInCode(4)>0 ) {
        containsCharmonium = true;
      }
      
      // Check for octet onium
      if( event[i].particleDataEntry().isOctetHadron() && 
          octetFlavour(event[i].id()) == 4   ){
        containsCharmonium = true;
      }      
      bool containsBottomonium = false;
      
      // Check for normal onium 
      if( event[i].particleDataEntry().isOnium() && 
          event[i].particleDataEntry().nQuarksInCode(5)>0 ) {
        containsBottomonium = true;
      }
      
      // Check for octet onium
      if( event[i].particleDataEntry().isOctetHadron() && 
          octetFlavour(event[i].id()) == 5   ){
        containsBottomonium = true;
      }

      if (containsCharmonium)   b_event_hascOnia = true;
      if (containsBottomonium)  b_event_hasbOnia = true;

      if (abs(event[i].status())==23 && containsCharmonium){
        b_event_hascOniaStatus23 = true;
        temp_23charmonium_id = event[i].idAbs();
      }

      if (abs(event[i].status())==33 && containsCharmonium){
        b_event_hascOniaStatus33 = true;
      }
      
      if (abs(event[i].status())==23 && containsBottomonium){
        b_event_hasbOniaStatus23 = true;
        temp_23bottomonium_id = event[i].idAbs();
      }

      if (abs(event[i].status())==33 && containsBottomonium){
        b_event_hasbOniaStatus33 = true;
      }

      // Store all of the MPI particles
      if(abs(event[i].status())==33) status_33_particles.push_back(i);
      
      // Are there heavy quarks
      if(event[i].idAbs()==5) foundb = true;
      if(event[i].idAbs()==4) foundc = true;

      if(event[i].idAbs()==5 && (abs(event[i].status())==21||abs(event[i].status())==23) ) foundHardb = true;
      if(event[i].idAbs()==4 && (abs(event[i].status())==21||abs(event[i].status())==23) ) foundHardc = true;

      if(event[i].idAbs()==5 && (abs(event[i].status())==31||abs(event[i].status())==33) ) foundMPIb = true;
      if(event[i].idAbs()==4 && (abs(event[i].status())==31||abs(event[i].status())==33) ) foundMPIc = true;
    }

    // =========================================================================
    // Get hard process type 
    // =========================================================================
    if(debug) MSG_DEBUG("Get hard process type")
 
    if(b_event_hasbStatus23) b_event_hasbStatus23_type = getProcessType(event,5,5);
    if(b_event_hascStatus23) b_event_hascStatus23_type = getProcessType(event,4,5);
    
    if(b_event_hasbOniaStatus23) b_event_hasbOniaStatus23_type = getProcessType(event,temp_23bottomonium_id,5);
    if(b_event_hascOniaStatus23) b_event_hascOniaStatus23_type = getProcessType(event,temp_23charmonium_id, 5);

    // =========================================================================
    // Determine info about which MPI processes are in the event
    // =========================================================================
    if(debug) MSG_DEBUG("Get MPI process type")

    if(status_33_particles.size()%2==0){
      int count_MPI_c = 0;
      int count_MPI_b = 0;
      int count_MPI_cOnia = 0;
      int count_MPI_bOnia = 0;
      for(int j = 0; j<status_33_particles.size()/2; j++){

        int id_j   = status_33_particles[2*j];
        int id_jp1 = status_33_particles[2*j+1];

        if( (event[id_j].idAbs()  ==4 ||
             event[id_jp1].idAbs()==4)   ){
          if(count_MPI_c==0){
            b_event_hascStatus33_type  = getProcessType(event,4,id_j);
          } else if(count_MPI_c==1){
            b_event_hascStatus33_type2 = getProcessType(event,4,id_j);
          }
          count_MPI_c++;
        }
        b_event_hascStatus33_N = count_MPI_c;

        if( (event[id_j].idAbs()  ==5 ||
             event[id_jp1].idAbs()==5)   ){
          if(count_MPI_b==0){
            b_event_hasbStatus33_type  = getProcessType(event,5,id_j);
          } else if(count_MPI_b==1){
            b_event_hasbStatus33_type2 = getProcessType(event,5,id_j);
          }
          count_MPI_b++;
        }
        b_event_hasbStatus33_N = count_MPI_b;

        
        // Work out if there are charmonium 
        bool containsCharmonium_j    = false;
        bool containsCharmonium_jp1  = false;
        bool containsBottomonium_j   = false;
        bool containsBottomonium_jp1 = false;
      
        // ------------------ Check for normal onium ----------------------------

        if( event[id_j].particleDataEntry().isOnium() && 
            event[id_j].particleDataEntry().nQuarksInCode(4)>0 ) {
          containsCharmonium_j = true;
        }
        if( event[id_jp1].particleDataEntry().isOnium() && 
            event[id_jp1].particleDataEntry().nQuarksInCode(4)>0 ) {
          containsCharmonium_jp1 = true;
        } 
        if( event[id_j].particleDataEntry().isOnium() && 
            event[id_j].particleDataEntry().nQuarksInCode(5)>0 ) {
          containsBottomonium_j = true;
        }    
        if( event[id_jp1].particleDataEntry().isOnium() && 
            event[id_jp1].particleDataEntry().nQuarksInCode(5)>0 ) {
          containsBottomonium_jp1 = true;
        }

        // ------------------ Check for octet onium ----------------------------
        if( event[id_j].particleDataEntry().isOctetHadron() && 
            octetFlavour(event[id_j].id()) == 4   ){
          containsCharmonium_j = true;
        }      
        if( event[id_j].particleDataEntry().isOctetHadron() && 
            octetFlavour(event[id_j].id()) == 5   ){
          containsBottomonium_j = true;
        }
        if( event[id_jp1].particleDataEntry().isOctetHadron() && 
            octetFlavour(event[id_jp1].id()) == 4   ){
          containsCharmonium_jp1 = true;
        } 
        if( event[id_jp1].particleDataEntry().isOctetHadron() && 
            octetFlavour(event[id_jp1].id()) == 5   ){
          containsBottomonium_jp1 = true;
        }

        // --------------------------- write count  ----------------------------
        if( containsCharmonium_j || containsCharmonium_jp1 ) {
          if(count_MPI_cOnia==0){
            b_event_hascOniaStatus33_type  = getProcessType(event,(containsCharmonium_j?event[id_j].id():event[id_jp1].id()),id_j);
          } else if(count_MPI_cOnia==1){
            b_event_hascOniaStatus33_type2 = getProcessType(event,(containsCharmonium_j?event[id_j].id():event[id_jp1].id()),id_j);
          }
          count_MPI_cOnia++;
        }
        b_event_hascOniaStatus33_N = count_MPI_cOnia;


        if( containsBottomonium_j || containsBottomonium_jp1 ) {
          if(count_MPI_bOnia==0){
            b_event_hasbOniaStatus33_type  = getProcessType(event,(containsBottomonium_j?event[id_j].id():event[id_jp1].id()),id_j);
          } else if(count_MPI_bOnia==1){
            b_event_hasbOniaStatus33_type2 = getProcessType(event,(containsBottomonium_j?event[id_j].id():event[id_jp1].id()),id_j);
          }
          count_MPI_bOnia++;
        }
        b_event_hasbOniaStatus33_N = count_MPI_bOnia;

      }
    } else {
      std::cout << "ERROR Odd number of MPI particles!" <<std::endl;
    }
    

   
    // =========================================================================
    // Get shower type 
    // =========================================================================
    if(debug) MSG_DEBUG("Get shower process type")

    // If there is a charm quark, but no hard or mpi charm, it must
    // be from the shower
    foundShowerc = (foundc && !(foundHardc || foundMPIc));
    foundShowerb = (foundb && !(foundHardb || foundMPIb));
    std::map<int,int> gluonsb;
    std::map<int,int> gluonsc;
    std::map<int,std::vector<int>> gluonsc_ccbar;
    std::map<int,std::vector<int>> gluonsb_bbbar;
    if(foundShowerb) {

      getShowerStatus(pythia, event, 5, 
                      foundShowerMPIFSRb, foundShowerMPIISRb, 
                      foundShowerHardFSRb, foundShowerHardISRb, 
                      foundShowerBeamb, &gluonsb, &gluonsb_bbbar);      
    }
    
    if(foundShowerc){

      getShowerStatus(pythia, event, 4, 
                      foundShowerMPIFSRc, foundShowerMPIISRc, 
                      foundShowerHardFSRc, foundShowerHardISRc, 
                      foundShowerBeamc, &gluonsc, &gluonsc_ccbar);               
    } 


    // bool printc = true;
    // foundVetoLevelc = mycUserHooks->foundVetoLevel;
    // if(printc && usingcUserHook && foundc && !foundVetoLevelc ){
    //   std::cout << "==== Beginning === " << std::endl;  
    //   std::cout << "Veto status: " << mycUserHooks->foundVetoLevel  << std::endl;
    //   std::cout << "Event at the time the veto determines there is no charm: iPos: " <<  mycUserHooks->temp_iPos << std::endl;
    //   mycUserHooks->temp_event.list();
    //   std::cout << "Event after all generation steps:" << std::endl;
    //   pythia.event.list();
    //   std::cout << "At veto iPos: " <<  mycUserHooks->temp_iPos << std::endl;

    //   std::cout << "Found c :";
    //   std::cout << "\tHard c " << foundHardc;
    //   std::cout << "\tMPI c " << foundMPIc;
    //   std::cout << "\tShower c  " << foundShowerc;
    //   std::cout << "\tShower Hard FSR c  " << foundShowerHardFSRc;
    //   std::cout << "\tShower MPI FSR c  " << foundShowerMPIFSRc;
    //   std::cout << "\tShower Hard ISR c  " << foundShowerHardISRc;
    //   std::cout << "\tShower MPI ISR c  " << foundShowerMPIISRc;
    //   std::cout << std::endl;
    //   std::cout << "Gluons c: " << gluonsc.size() << std::endl;
    //   for(const auto& gluon: gluonsc){
    //     std::cout << "\tgluon i  "<< gluon.first<< std::endl;
    //     std::cout << "\tgluon id "<< event[gluon.first].id()<< std::endl;
    //     std::cout << "\tpt: " << event[gluon.first].pT()<< std::endl;
    //     std::cout << "\tpx: " << event[gluon.first].px()<< std::endl;
    //     std::cout << "\tpy: " << event[gluon.first].py()<< std::endl;
    //     std::cout << "\tpz: " << event[gluon.first].pz()<< std::endl;
    //     std::cout << "\te:  " << event[gluon.first].e()<< std::endl;
    //     std::cout << std::endl;

    //     std::cout << "\tNumber of c quarks:"<< gluonsc_ccbar[gluon.first].size()<< std::endl;
    //     double tot_px = 0;
    //     double tot_py = 0;
    //     double tot_pz = 0;
    //     double tot_pe = 0;
    //     for(int ic = 0; ic < gluonsc_ccbar[gluon.first].size(); ic++){
    //       std::cout << "\t\tcharm i  "<< gluonsc_ccbar[gluon.first][ic]<< std::endl;
    //       std::cout << "\t\tcharm id "<< event[gluonsc_ccbar[gluon.first][ic]].id()<< std::endl;
    //       std::cout << "\t\tpt: " << event[gluonsc_ccbar[gluon.first][ic]].pT()<< std::endl;
    //       std::cout << "\t\tpx: " << event[gluonsc_ccbar[gluon.first][ic]].px()<< std::endl;
    //       std::cout << "\t\tpy: " << event[gluonsc_ccbar[gluon.first][ic]].py()<< std::endl;
    //       std::cout << "\t\tpz: " << event[gluonsc_ccbar[gluon.first][ic]].pz()<< std::endl;
    //       std::cout << "\t\te:  " << event[gluonsc_ccbar[gluon.first][ic]].e()<< std::endl;
    //       tot_px +=event[gluonsc_ccbar[gluon.first][ic]].px();
    //       tot_py +=event[gluonsc_ccbar[gluon.first][ic]].py();
    //       tot_pz +=event[gluonsc_ccbar[gluon.first][ic]].pz();
    //       tot_pe +=event[gluonsc_ccbar[gluon.first][ic]].e();
    //       std::cout << std::endl;
    //     }
    //     std::cout << "\t Mass: " << sqrt(tot_pe*tot_pe - tot_px*tot_px -tot_py*tot_py -tot_pz*tot_pz )<<  std::endl;

    //   }
    //   std::cout << "==== End === " << std::endl;
    //   std::cout << std::endl;
    // }

    // bool printb = true;
    // foundVetoLevelb = mybUserHooks->foundVetoLevel;
    // if(printb && usingbUserHook && foundb && !foundVetoLevelb ){
    //   std::cout << "==== Beginning === " << std::endl;
    //   std::cout << "Veto status: " << mybUserHooks->foundVetoLevel  << std::endl;      
    //   std::cout << "Event at the time the veto determines there is no charm: iPos: " <<  mybUserHooks->temp_iPos << std::endl;
    //   mybUserHooks->temp_event.list();
    //   std::cout << "Event:" << std::endl;
    //   std::cout << "Event after all generation steps:" << std::endl;

    //   pythia.event.list();
    //   std::cout << "At veto level: " <<  mybUserHooks->temp_iPos << std::endl;

    //   std::cout << "Found b :";
    //   std::cout << "\tHard b " << foundHardb;
    //   std::cout << "\tMPI b " << foundMPIb;
    //   std::cout << "\tShower b  " << foundShowerb;
    //   std::cout << "\tShower Hard FSR b  " << foundShowerHardFSRb;
    //   std::cout << "\tShower MPI FSR b  " << foundShowerMPIFSRb;
    //   std::cout << "\tShower Hard ISR b  " << foundShowerHardISRb;
    //   std::cout << "\tShower MPI ISR b  " << foundShowerMPIISRb;
    //   std::cout << std::endl;
    //   std::cout << "Gluons b: " << gluonsb.size() << std::endl;
    //   for(const auto& gluon: gluonsb){
    //     std::cout << "\tgluon i  "<< gluon.first<< std::endl;
    //     std::cout << "\tgluon id "<< event[gluon.first].id()<< std::endl;
    //     std::cout << "\tpt: " << event[gluon.first].pT()<< std::endl;
    //     std::cout << "\tpx: " << event[gluon.first].px()<< std::endl;
    //     std::cout << "\tpy: " << event[gluon.first].py()<< std::endl;
    //     std::cout << "\tpz: " << event[gluon.first].pz()<< std::endl;
    //     std::cout << "\te:  " << event[gluon.first].e()<< std::endl;
    //     std::cout << std::endl;
    //   }

    //   std::cout << "pTHat: " << pythia.info.pTHat() << std::endl;

    //   int nMPI = pythia.info.nMPI();
    //   std::cout << "nMPI: " << nMPI << std::endl;

    //   for(int j = 0; j<nMPI; j++){
    //     std::cout << "\tpT: " << pythia.info.pTMPI(j) << std::endl;
    //   }

    //   int count_MPI = 0;
    //   for(int i=1; i<event.size(); ++i){
    //     if(event[i].status()==-21){
    //       std::cout << i;
    //       std::cout << "\t"   << event[i].nameWithStatus();
    //       std::cout << " + "  << event[i+1].nameWithStatus();
    //       std::cout << " -> " << event[i+2].nameWithStatus();
    //       std::cout << " + "  << event[i+3].nameWithStatus();
    //       std::cout << "\tpT: " << pythia.info.pTMPI(count_MPI);
    //       std::cout << std::endl;

    //       i +=3;
    //     }
    //     if(event[i].status()==-31){
    //       count_MPI++;
    //       std::cout << i;
    //       std::cout << "\t"   << event[i].nameWithStatus();
    //       std::cout << " + "  << event[i+1].nameWithStatus();
    //       std::cout << " -> " << event[i+2].nameWithStatus();
    //       std::cout << " + "  << event[i+3].nameWithStatus();
    //       std::cout << "\tpT: " << pythia.info.pTMPI(count_MPI);

    //       std::cout << std::endl;

    //       i +=3;
    //     }
    //   }

    //   std::cout << "==== End === " << std::endl;
    //   std::cout << std::endl;

    // }

    // =========================================================================
    // Print the event if there is a hard b and c quark  
    // =========================================================================

    // if(b_event_hasbStatus23 && b_event_hascStatus23) {
    //   std::cout << "INFO Found event with hard c and b quark:\t"<<std::endl;
    //   pythia.event.list();
    // }

    // =========================================================================
    // Now loop over particles again, saving useful info
    // =========================================================================

    int nBs    = 0;
    int nBd    = 0;
    int nBu    = 0;
    int nBu_p  = 0;
    int nBu_m  = 0;
    int nBc    = 0;
    int nBc_p  = 0;
    int nBc_m  = 0;
    int nBds   = 0;
    int nLb    = 0;
    int nDp    = 0;
    int nDp_p  = 0;
    int nDp_m  = 0;
    int nDz    = 0;
    int nDs    = 0;
    int nLc    = 0;

    int nEtac         = 0;
    int nJpsi         = 0;
    int nChic2        = 0;

    int nEtab         = 0;
    int nUpsilon1S    = 0;
    int nChib2        = 0;

    int nSigmabm       = 0;      
    int nSigmab0       = 0;      
    int nSigmabp       = 0;      
    int nXibm          = 0;   
    int nXib0          = 0;   
    int nXiprimebm     = 0;        
    int nXiprimeb0     = 0;        
    int nOmegabm       = 0;      
    int nXibc0         = 0;    
    int nXibc0_p       = 0;    
    int nXibc0_m       = 0;    
    int nXibcp         = 0; 
    int nXibcp_p       = 0;    
    int nXibcp_m       = 0;    
    int nXiprimebc0    = 0;         
    int nXiprimebcp    = 0;         
    int nOmegabc0      = 0; 
    int nOmegabc0_p    = 0; 
    int nOmegabc0_m    = 0; 
    int nOmegaprimebc0 = 0;            
    int nOmegabccp     = 0;        
    int nXibbm         = 0;    
    int nXibb0         = 0;    
    int nOmegabbm      = 0;       
    int nOmegabbc0     = 0;        
    int nOmegabbbm     = 0;        
    int nSigmacpp      = 0;       
    int nSigmacp       = 0;      
    int nSigmac0       = 0;      
    int nXicp          = 0;   
    int nXic0          = 0;   
    int nXiprimecp     = 0;        
    int nXiprimec0     = 0;        
    int nOmegac0       = 0;      
    int nXiccp         = 0;    
    int nXiccp_p       = 0;    
    int nXiccp_m       = 0;    
    int nXiccpp        = 0;     
    int nXiccpp_p      = 0;     
    int nXiccpp_m      = 0;     
    int nOmegacc0      = 0;       
    int nOmegacccpp    = 0;         
    
    int nXb            = 0;         
    int nXc            = 0;         

    int nFinal         = 0;
    int nCharged       = 0;
    int nChargedInLHCb = 0;

    std::map<int,int> unique_bp_quarks;
    std::map<int,int> unique_bm_quarks;
    std::map<int,int> unique_cp_quarks;
    std::map<int,int> unique_cm_quarks;

    b_event_jpsi_px = 0.0; 
    b_event_jpsi_py = 0.0; 
    b_event_jpsi_pz = 0.0; 
    b_event_jpsi_pe = 0.0;
    b_event_jpsi_ID = -999;
    b_event_jpsi_c1_partSys = -9999;
    b_event_jpsi_c2_partSys = -9999;
    b_event_upsilon1s_px = 0.0; 
    b_event_upsilon1s_py = 0.0; 
    b_event_upsilon1s_pz = 0.0; 
    b_event_upsilon1s_pe = 0.0;
    b_event_upsilon1s_ID = -999;
    b_event_upsilon1s_b1_partSys = -9999;
    b_event_upsilon1s_b2_partSys = -9999;
    b_event_bc_samePartSys = false; 
    b_event_bc_sysZero = false;
    b_event_bc_b_partSys = -9999; 
    b_event_bc_c_partSys = -9999; 
    b_event_bc_px = 0.0; 
    b_event_bc_py = 0.0; 
    b_event_bc_pz = 0.0; 
    b_event_bc_pe = 0.0;
    b_event_bx_px = 0.0; 
    b_event_bx_py = 0.0; 
    b_event_bx_pz = 0.0; 
    b_event_bx_pe = 0.0;
    b_event_dx_px = 0.0; 
    b_event_dx_py = 0.0; 
    b_event_dx_pz = 0.0; 
    b_event_dx_pe = 0.0;
    b_event_dx2_px = 0.0; 
    b_event_dx2_py = 0.0; 
    b_event_dx2_pz = 0.0; 
    b_event_dx2_pe = 0.0;
    b_event_bx2_px = 0.0; 
    b_event_bx2_py = 0.0; 
    b_event_bx2_pz = 0.0; 
    b_event_bx2_pe = 0.0;

    b_event_bx_id  = -999;
    b_event_bc_id  = -999;
    b_event_dx_id  = -999;
    b_event_bx2_id = -999;
    b_event_dx2_id = -999;

    b_event_bx_partSys  = -9999;
    b_event_dx_partSys  = -9999;
    b_event_bx2_partSys = -9999;
    b_event_dx2_partSys = -9999;

    b_event_Dp_p_px = 0.0; 
    b_event_Dp_p_py = 0.0; 
    b_event_Dp_p_pz = 0.0; 
    b_event_Dp_p_pe = 0.0;
    b_event_Dp_p_id = -999;
    b_event_Dp_p_partSys = -9999;

    b_event_Bu_p_px = 0.0; 
    b_event_Bu_p_py = 0.0; 
    b_event_Bu_p_pz = 0.0; 
    b_event_Bu_p_pe = 0.0;
    b_event_Bu_p_id = -999;
    b_event_Bu_p_partSys = -9999;

    double temp_jpsi_px =0.0, temp_jpsi_py =0.0, temp_jpsi_pz =0.0, temp_jpsi_pe =0.0;
    double temp_upsilon1s_px =0.0, temp_upsilon1s_py =0.0, temp_upsilon1s_pz =0.0, temp_upsilon1s_pe =0.0;
    double temp_Xiccp_px =0.0, temp_Xiccp_py =0.0, temp_Xiccp_pz =0.0, temp_Xiccp_pe =0.0;
    double temp_Xiccpp_px =0.0, temp_Xiccpp_py =0.0, temp_Xiccpp_pz =0.0, temp_Xiccpp_pe =0.0;
    double temp_Omegacc0_px =0.0, temp_Omegacc0_py =0.0, temp_Omegacc0_pz =0.0, temp_Omegacc0_pe =0.0;
    double temp_Xibcp_px =0.0, temp_Xibcp_py =0.0, temp_Xibcp_pz =0.0, temp_Xibcp_pe =0.0;
    double temp_Xibc0_px =0.0, temp_Xibc0_py =0.0, temp_Xibc0_pz =0.0, temp_Xibc0_pe =0.0;

    int temp_jpsi_ID;
    int temp_upsilon1s_ID;
    int temp_Xiccp_ID;
    int temp_Xiccpp_ID;
    int temp_Omegacc0_ID;
    int temp_Xibcp_ID;
    int temp_Xibc0_ID;

    int temp_Xiccp_c1_partSys    = -99;
    int temp_Xiccp_c2_partSys    = -99;
    int temp_Xiccpp_c1_partSys   = -99;
    int temp_Xiccpp_c2_partSys   = -99;
    int temp_Omegacc0_c1_partSys = -99;
    int temp_Omegacc0_c2_partSys = -99;

    int temp_Xibc0_b_partSys = -99;
    int temp_Xibc0_c_partSys = -99;
    int temp_Xibcp_b_partSys = -99;
    int temp_Xibcp_c_partSys = -99;



    double temp_bc_px = 0.0, temp_bc_py = 0.0, temp_bc_pz = 0.0, temp_bc_pe = 0.0;
    double temp_bx_px = 0.0, temp_bx_py = 0.0, temp_bx_pz = 0.0, temp_bx_pe = 0.0;
    double temp_dx_px = 0.0, temp_dx_py = 0.0, temp_dx_pz = 0.0, temp_dx_pe = 0.0;

    double temp_Dp_p_px = 0.0, temp_Dp_p_py = 0.0, temp_Dp_p_pz = 0.0, temp_Dp_p_pe = 0.0; int temp_Dp_p_partSys = 0; int temp_Dp_p_id  = 0;
    double temp_Bu_p_px = 0.0, temp_Bu_p_py = 0.0, temp_Bu_p_pz = 0.0, temp_Bu_p_pe = 0.0; int temp_Bu_p_partSys = 0; int temp_Bu_p_id  = 0;

    int temp_dx_partSys = 0, temp_bx_partSys = 0, temp_bc_partSys = 0;

    double temp_dx1_px = 0.0, temp_dx1_py = 0.0, temp_dx1_pz = 0.0, temp_dx1_pe = 0.0;
    double temp_dx2_px = 0.0, temp_dx2_py = 0.0, temp_dx2_pz = 0.0, temp_dx2_pe = 0.0;
    double temp_bx1_px = 0.0, temp_bx1_py = 0.0, temp_bx1_pz = 0.0, temp_bx1_pe = 0.0;
    double temp_bx2_px = 0.0, temp_bx2_py = 0.0, temp_bx2_pz = 0.0, temp_bx2_pe = 0.0;

    int temp_dx1_partSys = 0, temp_bx1_partSys = 0;
    int temp_dx2_partSys = 0, temp_bx2_partSys = 0;

    int temp_bc_id  = 0;
    int temp_dx_id  = 0;
    int temp_dx1_id = 0;
    int temp_dx2_id = 0;
    int temp_bx1_id = 0;
    int temp_bx_id  = 0;
    int temp_bx2_id = 0;
    if(debug) MSG_DEBUG("Loop Particles 2")

    for (int i=1; i<event.size(); ++i) {

      // std::cout << "Mother size: " << event[i].motherList().size();
      // std::cout << "\tMother1:         " <<event[event[i].mother1()].nameWithStatus()<< "\t " << event[i].mother1();
      // std::cout << "\tMother2:         " <<event[event[i].mother2()].nameWithStatus()<< "\t " << event[i].mother2();
      // std::cout << std::endl;
      
      b_parts_b_plus_c_mass     = 0.0;
      b_parts_b_plus_c_mass_all = 0.0;
      b_parts_bc_sideband       = 0.0;
      b_parts_con_to_c          = false;
      b_parts_isBc              = false;
      b_parts_isBcstar          = false;
      b_parts_iscolconnected    = false;
      b_parts_fromBcstar        = false;
      b_parts_isMPI             = false;
      b_parts_status            = -999;
      
      // =======================================================================
      // Fill some information if a Bc is generated
      // =======================================================================

      if(lhe_file && not isGenXicc){
        if(i==5 && event[i].idAbs() == 541){
          b_event_bcvegpy_bc_px = event[i].px(); 
          b_event_bcvegpy_bc_py = event[i].py(); 
          b_event_bcvegpy_bc_pz = event[i].pz(); 
          b_event_bcvegpy_bc_pe = event[i].e();
        } else if((i==6 || i==7) && event[i].idAbs() == 4){
          b_event_bcvegpy_c_px = event[i].px(); 
          b_event_bcvegpy_c_py = event[i].py(); 
          b_event_bcvegpy_c_pz = event[i].pz(); 
          b_event_bcvegpy_c_pe = event[i].e();
        } else if((i==6 || i==7) && event[i].idAbs() == 5){
          b_event_bcvegpy_b_px = event[i].px(); 
          b_event_bcvegpy_b_py = event[i].py(); 
          b_event_bcvegpy_b_pz = event[i].pz(); 
          b_event_bcvegpy_b_pe = event[i].e();
        } 
      } else if(lhe_file && isGenXicc){
        if(i==5 && event[i].idAbs() == 4422){
          b_event_bcvegpy_bc_px = event[i].px(); 
          b_event_bcvegpy_bc_py = event[i].py(); 
          b_event_bcvegpy_bc_pz = event[i].pz(); 
          b_event_bcvegpy_bc_pe = event[i].e();
        } else if((i==6 ) && event[i].idAbs() == 4){
          b_event_bcvegpy_c_px = event[i].px(); 
          b_event_bcvegpy_c_py = event[i].py(); 
          b_event_bcvegpy_c_pz = event[i].pz(); 
          b_event_bcvegpy_c_pe = event[i].e();
        } else if((i==7) && event[i].idAbs() == 4){
          b_event_bcvegpy_b_px = event[i].px(); 
          b_event_bcvegpy_b_py = event[i].py(); 
          b_event_bcvegpy_b_pz = event[i].pz(); 
          b_event_bcvegpy_b_pe = event[i].e();
        } 
      }

      if(event[i].idAbs() == 541 && !lhe_file){
        bool printBcinfo = false;

        if(debug) MSG_DEBUG("Found Bc")
        
        // Get Mother b and c quarks 
        if(debug) MSG_DEBUG("getFirstMotherofType")

        int first_b_id = getFirstMotherofType(5,event,i);
        if(debug) MSG_DEBUG("getFirstMotherofType")

        int first_c_id = getFirstMotherofType(4,event,i);
        double b_pe = event[first_b_id].e();
        double b_px = event[first_b_id].px();
        double b_py = event[first_b_id].py();
        double b_pz = event[first_b_id].pz();
        
        double c_pe = event[first_c_id].e();
        double c_px = event[first_c_id].px();
        double c_py = event[first_c_id].py();
        double c_pz = event[first_c_id].pz();


        int first_b_id_mother1 = event[first_b_id].mother1();
        int first_b_id_mother2 = event[first_b_id].mother2();
        int first_c_id_mother1 = event[first_c_id].mother1();
        int first_c_id_mother2 = event[first_c_id].mother2();

        b_event_bc_b_partSys = partonSystems.getSystemOf(first_b_id_mother1);
        b_event_bc_c_partSys = partonSystems.getSystemOf(first_c_id_mother1);

        b_event_bc_b_partSys = getPartonSystem(event,partonSystems,first_b_id);
        b_event_bc_c_partSys = getPartonSystem(event,partonSystems,first_c_id);
        if(printBcinfo) std::cout << "SYSTEM mother 1 b: " <<  b_event_bc_b_partSys<< "\t"<<  std::endl;
        if(printBcinfo) std::cout << "SYSTEM mother 1 c: " <<  b_event_bc_c_partSys << "\t"<<  std::endl;

        if(b_event_bc_b_partSys==b_event_bc_c_partSys){
          b_event_bc_samePartSys = true;
          if(b_event_bc_b_partSys==0) b_event_bc_sysZero = true;
        }

        b_parts_b_plus_c_mass =  sqrt(pow2(b_pe+c_pe) - pow2(b_px+c_px) -pow2(b_py+c_py) -pow2(b_pz+c_pz));
        
        if(event[first_b_id].id()>0){
          b_parts_iscolconnected = (event[first_b_id].col()  == event[first_c_id].acol());
        } else {
          b_parts_iscolconnected = (event[first_b_id].acol() == event[first_c_id].col() );
        }
        b_parts_fromBcstar = (event[event[i].mother1()].idAbs()==543);

        
        if(printBcinfo) std::cout << "Generated Bc! event: "<<iEvent <<std::endl;
        if(printBcinfo) std::cout << "Mass:             " << event[i].m0() <<std::endl;
        if(printBcinfo) std::cout << "First b:          " <<first_b_id << "\t " << event[first_b_id].nameWithStatus() <<std::endl;
        if(printBcinfo) std::cout << "First c:          " <<first_c_id << "\t " << event[first_c_id].nameWithStatus() <<std::endl;
        if(printBcinfo) std::cout << "b col:            " << event[first_b_id].col()  << std::endl;
        if(printBcinfo) std::cout << "b acol:           " << event[first_b_id].acol() << std::endl;
        if(printBcinfo) std::cout << "c col:            " << event[first_c_id].col()  << std::endl;
        if(printBcinfo) std::cout << "c acol:           " << event[first_c_id].acol() << std::endl;
        if(printBcinfo) std::cout << "b_plus_c_mass:    " << b_parts_b_plus_c_mass <<std::endl;
      
        if(b_parts_iscolconnected){
          if(printBcinfo) std::cout << "--> is colour connected" << std::endl;
        } else {
          if(printBcinfo) std::cout << "--> NOT colour connected" << std::endl;
        }

        

        if(!b_parts_iscolconnected){  
          bool isb_plus = (event[first_b_id].id()>0);
          int b_col = 0;
          int c_col = 0;
          if(isb_plus){
             b_col = event[first_b_id].col();
             c_col = event[first_c_id].acol();
           } else {
             b_col = event[first_b_id].acol();
             c_col = event[first_c_id].col();
           }

          if(printBcinfo) std::cout << "Looking for colour: " << (isb_plus?b_col:c_col)<<std::endl;
          if(printBcinfo) std::cout << "    Want to reach:  " << (isb_plus?c_col:b_col)<<std::endl;
          
          std::vector<int> gluons = getStringofGluons(event, b_col,c_col,isb_plus,first_b_id,first_c_id);


          double bc_pe_total = event[first_b_id].e()  + event[first_c_id].e();
          double bc_px_total = event[first_b_id].px() + event[first_c_id].px();
          double bc_py_total = event[first_b_id].py() + event[first_c_id].py();
          double bc_pz_total = event[first_b_id].pz() + event[first_c_id].pz();
          
          for(const auto& gluon: gluons){
            if(printBcinfo) std::cout << "Gluon: ";
            if(printBcinfo) std::cout << "\t" << event[gluon].e(); 
            if(printBcinfo) std::cout << "\t" << event[gluon].px(); 
            if(printBcinfo) std::cout << "\t" << event[gluon].py(); 
            if(printBcinfo) std::cout << "\t" << event[gluon].pz(); 
            if(printBcinfo) std::cout << std::endl; 
            bc_pe_total += event[gluon].e(); 
            bc_px_total += event[gluon].px(); 
            bc_py_total += event[gluon].py(); 
            bc_pz_total += event[gluon].pz(); 
          }
          b_parts_b_plus_c_mass_all = sqrt(pow2(bc_pe_total)-pow2(bc_px_total)-pow2(bc_py_total)-pow2(bc_pz_total));

          if(printBcinfo) std::cout << "b_b_plus_c_mass:     " << b_parts_b_plus_c_mass     << std::endl;
          if(printBcinfo) std::cout << "b_b_plus_c_mass_all: " << b_parts_b_plus_c_mass_all << std::endl;
          if(printBcinfo) std::cout << std::endl;

        }

        
        if(printBcinfo) std::cout << "Mother1:         " <<event[event[i].mother1()].nameWithStatus()                                    << "\t " << event[i].mother1()                                    <<std::endl;
        if(printBcinfo) std::cout << "    Mother1:     " <<event[event[event[i].mother1()].mother1()].nameWithStatus()                   << "\t " << event[event[i].mother1()].mother1()                   <<std::endl;
        if(printBcinfo) std::cout << "        Mother1: " <<event[event[event[event[i].mother1()].mother1()].mother1()].nameWithStatus()  << "\t " << event[event[event[i].mother1()].mother1()].mother1()  <<std::endl;
        if(printBcinfo) std::cout << "        Mother2: " <<event[event[event[event[i].mother1()].mother1()].mother2()].nameWithStatus()  << "\t " << event[event[event[i].mother1()].mother1()].mother2()  <<std::endl;
        if(printBcinfo) std::cout << "    Mother2:     " <<event[event[event[i].mother1()].mother2()].nameWithStatus()                   << "\t " << event[event[i].mother1()].mother2()                   <<std::endl;
        if(printBcinfo) std::cout << "        Mother1: " <<event[event[event[event[i].mother1()].mother2()].mother1()].nameWithStatus()  << "\t " << event[event[event[i].mother1()].mother2()].mother1()  <<std::endl;
        if(printBcinfo) std::cout << "        Mother2: " <<event[event[event[event[i].mother1()].mother2()].mother2()].nameWithStatus()  << "\t " << event[event[event[i].mother1()].mother2()].mother2()  <<std::endl;
        if(printBcinfo) std::cout << "Mother2:         " <<event[event[i].mother2()].nameWithStatus()                                    << "\t " << event[i].mother2()                                    <<std::endl;
        if(printBcinfo) std::cout << "    Mother1:     " <<event[event[event[i].mother2()].mother1()].nameWithStatus()                   << "\t " << event[event[i].mother2()].mother1()                   <<std::endl;
        if(printBcinfo) std::cout << "        Mother1: " <<event[event[event[event[i].mother2()].mother1()].mother1()].nameWithStatus()  << "\t " << event[event[event[i].mother2()].mother1()].mother1()  <<std::endl;
        if(printBcinfo) std::cout << "        Mother2: " <<event[event[event[event[i].mother2()].mother1()].mother2()].nameWithStatus()  << "\t " << event[event[event[i].mother2()].mother1()].mother2()  <<std::endl;
        if(printBcinfo) std::cout << "    Mother2:     " <<event[event[event[i].mother2()].mother2()].nameWithStatus()                   << "\t " << event[event[i].mother2()].mother2()                   <<std::endl;
        if(printBcinfo) std::cout << "        Mother1: " <<event[event[event[event[i].mother2()].mother2()].mother1()].nameWithStatus()  << "\t " << event[event[event[i].mother2()].mother2()].mother1()  <<std::endl;
        if(printBcinfo) std::cout << "        Mother2: " <<event[event[event[event[i].mother2()].mother2()].mother2()].nameWithStatus()  << "\t " << event[event[event[i].mother2()].mother2()].mother2()  <<std::endl;
        
        
        std::map<int,int> ancs_b = createListofAncestors(event,first_b_id,false);
        std::map<int,int> ancs_c = createListofAncestors(event,first_c_id,false);

        for(const auto& anc:ancs_c){
          if(abs(event[anc.first].status()) > 30 && abs(event[anc.first].status()) < 40 ){
            b_parts_isMPI = true;
          } 
        }
        for(const auto& anc:ancs_b){
          if(abs(event[anc.first].status()) > 30 && abs(event[anc.first].status()) < 40 ){
            b_parts_isMPI = true;
          } 
        }

        // pythia.event.list();
      } // End of Bc specific part


      // =======================================================================
      // Count B hadrons  
      // =======================================================================

      if      (event[i].idAbs() == 531)  ++nBs;
      else if (event[i].idAbs() == 541 &&event[i].isFinal())  ++nBc;
      else if (event[i].idAbs() == 521)  ++nBu;
      else if (event[i].idAbs() == 511)  ++nBd;
      else if (event[i].idAbs() == 513)  ++nBds;
      else if (event[i].idAbs() == 5122) ++nLb;

      else if (event[i].idAbs() == 411)  ++nDp;
      else if (event[i].idAbs() == 421)  ++nDz;
      else if (event[i].idAbs() == 431)  ++nDs;
      else if (event[i].idAbs() == 4122) ++nLc;


      else if (event[i].idAbs() ==  441)  ++nEtac;
      else if (event[i].idAbs() ==  443 &&event[i].isFinal())  ++nJpsi;
      else if (event[i].idAbs() ==  445)  ++nChic2;

      else if (event[i].idAbs() ==  551)  ++nEtab;
      else if (event[i].idAbs() ==  553 && event[i].isFinal())  ++nUpsilon1S;
      else if (event[i].idAbs() ==  555)  ++nChib2;

      else if (event[i].idAbs() == 5112) ++nSigmabm;
      else if (event[i].idAbs() == 5212) ++nSigmab0;
      else if (event[i].idAbs() == 5222) ++nSigmabp;
      else if (event[i].idAbs() == 5132) ++nXibm;
      else if (event[i].idAbs() == 5232) ++nXib0;
      else if (event[i].idAbs() == 5312) ++nXiprimebm;
      else if (event[i].idAbs() == 5322) ++nXiprimeb0;
      else if (event[i].idAbs() == 5332) ++nOmegabm;

      else if (event[i].idAbs() == 5142 &&event[i].isFinal()) ++nXibc0;
      else if (event[i].idAbs() == 5242 &&event[i].isFinal()) ++nXibcp;
      else if (event[i].idAbs() == 5412) ++nXiprimebc0;
      else if (event[i].idAbs() == 5422) ++nXiprimebcp;
      else if (event[i].idAbs() == 5342) ++nOmegabc0;
      else if (event[i].idAbs() == 5432) ++nOmegaprimebc0;
      else if (event[i].idAbs() == 5442) ++nOmegabccp;
      else if (event[i].idAbs() == 5512) ++nXibbm;
      else if (event[i].idAbs() == 5522) ++nXibb0;
      else if (event[i].idAbs() == 5532) ++nOmegabbm;
      else if (event[i].idAbs() == 5542) ++nOmegabbc0;
      else if (event[i].idAbs() == 5554) ++nOmegabbbm;

      else if (event[i].idAbs() == 4222) ++nSigmacpp;
      else if (event[i].idAbs() == 4212) ++nSigmacp;
      else if (event[i].idAbs() == 4112) ++nSigmac0;
      else if (event[i].idAbs() == 4232) ++nXicp;
      else if (event[i].idAbs() == 4132) ++nXic0;
      else if (event[i].idAbs() == 4322) ++nXiprimecp;
      else if (event[i].idAbs() == 4312) ++nXiprimec0;
      else if (event[i].idAbs() == 4332) ++nOmegac0;

      else if (event[i].idAbs() == 4412 &&event[i].isFinal()) ++nXiccp;
      else if (event[i].idAbs() == 4422 &&event[i].isFinal()) ++nXiccpp;
      else if (event[i].idAbs() == 4432) ++nOmegacc0;
      else if (event[i].idAbs() == 4444) ++nOmegacccpp;



      // Charge split 
      if      (event[i].id()    == 521)  ++nBu_p;
      else if (event[i].id()    ==-521)  ++nBu_m;
      else if (event[i].id()    == 541 &&event[i].isFinal())  ++nBc_p;
      else if (event[i].id()    ==-541 &&event[i].isFinal())  ++nBc_m;
      else if (event[i].id()    == 411)  ++nDp_p;
      else if (event[i].id()    ==-411)  ++nDp_m;
      else if (event[i].id()    == 4412 &&event[i].isFinal()) ++nXiccp_p;
      else if (event[i].id()    ==-4412 &&event[i].isFinal()) ++nXiccp_m;
      else if (event[i].id()    == 4422 &&event[i].isFinal()) ++nXiccpp_p;
      else if (event[i].id()    ==-4422 &&event[i].isFinal()) ++nXiccpp_m;
      else if (event[i].id()    == 5142 &&event[i].isFinal()) ++nXibc0_p;
      else if (event[i].id()    ==-5142 &&event[i].isFinal()) ++nXibc0_m;
      else if (event[i].id()    == 5242 &&event[i].isFinal()) ++nXibcp_p;
      else if (event[i].id()    ==-5242 &&event[i].isFinal()) ++nXibcp_m;
      else if (event[i].id()    == 5342) ++nOmegabc0_p;
      else if (event[i].id()    ==-5342) ++nOmegabc0_m;

      nXb = nBs+nBu+nBd+nLb+
            nSigmabm+nSigmab0+nSigmabp+
            nXibm+nXib0+
            nXiprimebm+nXiprimeb0+
            nOmegabm;

      nXc = nDs+nDp+nDz+nLc+
            nSigmacpp+nSigmacp+nSigmac0+
            nXicp+nXic0+
            nXiprimecp+nXiprimec0+
            nOmegac0;

      if (event[i].isFinal())  ++nFinal;
      if (event[i].isFinal()&&event[i].isCharged())  ++nCharged;
      if (event[i].isFinal()    &&
          event[i].isCharged()  &&
          (event[i].eta() > 2.0 && event[i].eta()< 4.5))  ++nChargedInLHCb;
          


      // =======================================================================
      // Cache 4-momenta  
      // =======================================================================
      // Bc
      if(event[i].idAbs() == 541){
        temp_bc_px = event[i].px();
        temp_bc_py = event[i].py();
        temp_bc_pz = event[i].pz();
        temp_bc_pe = event[i].e();
        temp_bc_id = event[i].id();
      }      
      // Jpsi
      if(event[i].idAbs() == 443){
        temp_jpsi_px = event[i].px();
        temp_jpsi_py = event[i].py();
        temp_jpsi_pz = event[i].pz();
        temp_jpsi_pe = event[i].e();
        temp_jpsi_ID = event[i].idAbs();
      }     
      // Upsilon1S
      if(event[i].idAbs() == 553){
        temp_upsilon1s_px = event[i].px();
        temp_upsilon1s_py = event[i].py();
        temp_upsilon1s_pz = event[i].pz();
        temp_upsilon1s_pe = event[i].e();
        temp_upsilon1s_ID = event[i].id();
      } 

      // Xiccp
      if(event[i].idAbs() == 4412){
        temp_Xiccp_px = event[i].px();
        temp_Xiccp_py = event[i].py();
        temp_Xiccp_pz = event[i].pz();
        temp_Xiccp_pe = event[i].e();
        temp_Xiccp_ID = event[i].id();
        std::pair<int,int> temp_Xiccp_partSys = getDoublyHeavyHadronPartSys(event,partonSystems,i);
        temp_Xiccp_c1_partSys = temp_Xiccp_partSys.first;
        temp_Xiccp_c2_partSys = temp_Xiccp_partSys.second;
      }    
      // Xiccpp
      if(event[i].idAbs() == 4422){
        temp_Xiccpp_px = event[i].px();
        temp_Xiccpp_py = event[i].py();
        temp_Xiccpp_pz = event[i].pz();
        temp_Xiccpp_pe = event[i].e();
        temp_Xiccpp_ID = event[i].id();
        std::pair<int,int> temp_Xiccpp_partSys = getDoublyHeavyHadronPartSys(event,partonSystems,i);
        temp_Xiccpp_c1_partSys = temp_Xiccpp_partSys.first;
        temp_Xiccpp_c2_partSys = temp_Xiccpp_partSys.second;
      }    

      // Omegacc0
      if(event[i].idAbs() == 4432){
        temp_Omegacc0_px = event[i].px();
        temp_Omegacc0_py = event[i].py();
        temp_Omegacc0_pz = event[i].pz();
        temp_Omegacc0_pe = event[i].e();
        temp_Omegacc0_ID = event[i].id();
        std::pair<int,int> temp_Omegacc0_partSys = getDoublyHeavyHadronPartSys(event,partonSystems,i);
        temp_Omegacc0_c1_partSys = temp_Omegacc0_partSys.first;
        temp_Omegacc0_c2_partSys = temp_Omegacc0_partSys.second;
      }

      // Xibc0
      if(event[i].idAbs() == 5142){
        temp_Xibc0_px = event[i].px();
        temp_Xibc0_py = event[i].py();
        temp_Xibc0_pz = event[i].pz();
        temp_Xibc0_pe = event[i].e();
        temp_Xibc0_ID = event[i].id();
        std::pair<int,int> temp_Xibc0_partSys = getXibcPartSys(event,partonSystems,i);
        temp_Xibc0_b_partSys = temp_Xibc0_partSys.first;
        temp_Xibc0_c_partSys = temp_Xibc0_partSys.second;
      }    
      // Xibcp
      if(event[i].idAbs() == 5242){
        temp_Xibcp_px = event[i].px();
        temp_Xibcp_py = event[i].py();
        temp_Xibcp_pz = event[i].pz();
        temp_Xibcp_pe = event[i].e();
        temp_Xibcp_ID = event[i].id();
        std::pair<int,int> temp_Xibcp_partSys = getXibcPartSys(event,partonSystems,i);
        temp_Xibcp_b_partSys = temp_Xibcp_partSys.first;
        temp_Xibcp_c_partSys = temp_Xibcp_partSys.second;
      }

      // Bx
      if(event[i].idAbs() == 511  || // B0         (d bbar)
         event[i].idAbs() == 521  || // B+         (u bbar)
         event[i].idAbs() == 531  || // Bs0        (s bbar)
         event[i].idAbs() == 5122 || // Lambda_b0  (u d b)
         event[i].idAbs() == 5112 || // Sigma_b-   (d d b)
         event[i].idAbs() == 5212 || // Sigma_b0   (u d b)
         event[i].idAbs() == 5222 || // Sigma_b+   (u u b)
         event[i].idAbs() == 5132 || // Xi_b-      (d s b)
         event[i].idAbs() == 5232 || // Xi_b_0     (u s b)
         event[i].idAbs() == 5312 || // Xiprime_b- (s d b)
         event[i].idAbs() == 5322 || // Xiprime_b0 (u s b)
         event[i].idAbs() == 5332    // Omega_b-   (s s b)
            ){
        temp_bx_px = event[i].px();
        temp_bx_py = event[i].py();
        temp_bx_pz = event[i].pz();
        temp_bx_pe = event[i].e();
        temp_bx_id = event[i].id();
        temp_bx_partSys = getHeavyHadronPartSys(event,partonSystems,i);
      
      }

      // Dx
      if( event[i].idAbs() == 411  ||  // D+          (c dbar)
          event[i].idAbs() == 421  ||  // D0          (c ubar)
          event[i].idAbs() == 431  ||  // Ds+         (c sbar)
          event[i].idAbs() == 4122 ||  // Lambda_c+   (u d c)
          event[i].idAbs() == 4222 ||  // Sigma_c++   (u u c)
          event[i].idAbs() == 4212 ||  // Sigma_c+    (u d c)
          event[i].idAbs() == 4112 ||  // Sigma_c0    (d d c)
          event[i].idAbs() == 4232 ||  // Xi_c+       (u s c)
          event[i].idAbs() == 4132 ||  // Xi_c0       (d s c)
          event[i].idAbs() == 4322 ||  // Xiprime_c+  (s u c)
          event[i].idAbs() == 4312 ||  // Xiprime_c0  (s d c)
          event[i].idAbs() == 4332){   // Omega_c0    (s s c)
        temp_dx_px = event[i].px();
        temp_dx_py = event[i].py();
        temp_dx_pz = event[i].pz();
        temp_dx_pe = event[i].e();
        temp_dx_id = event[i].id();
        temp_dx_partSys = getHeavyHadronPartSys(event,partonSystems,i);
      }

      // Bup 
      if(event[i].id() == 521){
        temp_Bu_p_px      = temp_bx_px;
        temp_Bu_p_py      = temp_bx_py;
        temp_Bu_p_pz      = temp_bx_pz;
        temp_Bu_p_pe      = temp_bx_pe;
        temp_Bu_p_id      = temp_bx_id;
        temp_Bu_p_partSys = temp_bx_partSys;
      }
      
      // Dpp 
      if(event[i].id() == 411){
        temp_Dp_p_px      = temp_dx_px;
        temp_Dp_p_py      = temp_dx_py;
        temp_Dp_p_pz      = temp_dx_pz;
        temp_Dp_p_pe      = temp_dx_pe;
        temp_Dp_p_id      = temp_dx_id;
        temp_Dp_p_partSys = temp_dx_partSys;
      }

      // Cache first D meson momentum
      if(nXc == 1 &&(
          event[i].idAbs() == 411  ||  // D+          (c dbar)
          event[i].idAbs() == 421  ||  // D0          (c ubar)
          event[i].idAbs() == 431  ||  // Ds+         (c sbar)
          event[i].idAbs() == 4122 ||  // Lambda_c+   (u d c)
          event[i].idAbs() == 4222 ||  // Sigma_c++   (u u c)
          event[i].idAbs() == 4212 ||  // Sigma_c+    (u d c)
          event[i].idAbs() == 4112 ||  // Sigma_c0    (d d c)
          event[i].idAbs() == 4232 ||  // Xi_c+       (u s c)
          event[i].idAbs() == 4132 ||  // Xi_c0       (d s c)
          event[i].idAbs() == 4322 ||  // Xiprime_c+  (s u c)
          event[i].idAbs() == 4312 ||  // Xiprime_c0  (s d c)
          event[i].idAbs() == 4332)){  // Omega_c0    (s s c)
        temp_dx1_px = event[i].px();
        temp_dx1_py = event[i].py();
        temp_dx1_pz = event[i].pz();
        temp_dx1_pe = event[i].e();
        temp_dx1_id = event[i].id();
        temp_dx1_partSys = temp_dx_partSys;

      }      
      // Cache second D meson momentum
      if(nXc == 2 &&(
          event[i].idAbs() == 411  ||  // D+          (c dbar)
          event[i].idAbs() == 421  ||  // D0          (c ubar)
          event[i].idAbs() == 431  ||  // Ds+         (c sbar)
          event[i].idAbs() == 4122 ||  // Lambda_c+   (u d c)
          event[i].idAbs() == 4222 ||  // Sigma_c++   (u u c)
          event[i].idAbs() == 4212 ||  // Sigma_c+    (u d c)
          event[i].idAbs() == 4112 ||  // Sigma_c0    (d d c)
          event[i].idAbs() == 4232 ||  // Xi_c+       (u s c)
          event[i].idAbs() == 4132 ||  // Xi_c0       (d s c)
          event[i].idAbs() == 4322 ||  // Xiprime_c+  (s u c)
          event[i].idAbs() == 4312 ||  // Xiprime_c0  (s d c)
          event[i].idAbs() == 4332)){  // Omega_c0    (s s c)
        temp_dx2_px = event[i].px();
        temp_dx2_py = event[i].py();
        temp_dx2_pz = event[i].pz();
        temp_dx2_pe = event[i].e();
        temp_dx2_id = event[i].id();
        temp_dx2_partSys = temp_dx_partSys;

      }

      // Cache first B meson momentum
      if(nXb == 1 &&(
         event[i].idAbs() == 511  || // B0         (d bbar)
         event[i].idAbs() == 521  || // B+         (u bbar)
         event[i].idAbs() == 531  || // Bs0        (s bbar)
         event[i].idAbs() == 5122 || // Lambda_b0  (u d b)
         event[i].idAbs() == 5112 || // Sigma_b-   (d d b)
         event[i].idAbs() == 5212 || // Sigma_b0   (u d b)
         event[i].idAbs() == 5222 || // Sigma_b+   (u u b)
         event[i].idAbs() == 5132 || // Xi_b-      (d s b)
         event[i].idAbs() == 5232 || // Xi_b_0     (u s b)
         event[i].idAbs() == 5312 || // Xiprime_b- (s d b)
         event[i].idAbs() == 5322 || // Xiprime_b0 (u s b)
         event[i].idAbs() == 5332    // Omega_b-   (s s b) 
         ) ){
        temp_bx1_px = event[i].px();
        temp_bx1_py = event[i].py();
        temp_bx1_pz = event[i].pz();
        temp_bx1_pe = event[i].e();
        temp_bx1_id = event[i].id();
        temp_bx1_partSys = temp_bx_partSys;

      }      
      // Cache second B meson momentum
      if(nXb == 2 && (
         event[i].idAbs() == 511  || // B0         (d bbar)
         event[i].idAbs() == 521  || // B+         (u bbar)
         event[i].idAbs() == 531  || // Bs0        (s bbar)
         event[i].idAbs() == 5122 || // Lambda_b0  (u d b)
         event[i].idAbs() == 5112 || // Sigma_b-   (d d b)
         event[i].idAbs() == 5212 || // Sigma_b0   (u d b)
         event[i].idAbs() == 5222 || // Sigma_b+   (u u b)
         event[i].idAbs() == 5132 || // Xi_b-      (d s b)
         event[i].idAbs() == 5232 || // Xi_b_0     (u s b)
         event[i].idAbs() == 5312 || // Xiprime_b- (s d b)
         event[i].idAbs() == 5322 || // Xiprime_b0 (u s b)
         event[i].idAbs() == 5332    // Omega_b-   (s s b) 
         ) ){
        temp_bx2_px = event[i].px();
        temp_bx2_py = event[i].py();
        temp_bx2_pz = event[i].pz();
        temp_bx2_pe = event[i].e();
        temp_bx2_id = event[i].id();
        temp_bx2_partSys = temp_bx_partSys;
      }
      // =======================================================================
      // Checks for b and c quarks 
      // Only interested in ones at the end their family tree
      // i.e. the daughters have different ids to them --> hadronisation
      // =======================================================================
      bool has_unique_bp_quark = false;
      bool has_unique_bm_quark = false;
      bool has_unique_cp_quark = false;
      bool has_unique_cm_quark = false;

      if( event[i].id()                  == 5){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==5) not_unique = true;
        }
        if(!not_unique) has_unique_bp_quark = true;
      }

      if( event[i].id()                  == -5){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==-5) not_unique = true;
        }
        if(!not_unique) has_unique_bm_quark = true;
      }

      if( event[i].id()                  == 4){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==4) not_unique = true;
        }
        if(!not_unique) has_unique_cp_quark = true;
      }

      if( event[i].id()                  == -4){
        bool not_unique = false;
        for(const auto& daug_i: event[i].daughterList()){
          if(event[daug_i].id()==-4) not_unique = true;
        }
        if(!not_unique) has_unique_cm_quark = true;
      }

      // =======================================================================
      // If particle is a b or bbar, see what it is colour connected to 
      // =======================================================================

      if(has_unique_bp_quark || has_unique_bm_quark){
        // check whether daug is Bc
        if(event[event[i].daughter1()].idAbs()==541 ||
           event[event[i].daughter2()].idAbs()==541) b_parts_isBc     = true;
        if(event[event[i].daughter1()].idAbs()==543 ||
           event[event[i].daughter2()].idAbs()==543) b_parts_isBcstar = true;

        // Get partner at other end of colour string
        std::pair<int,std::pair<std::vector<int>,int>> partner = getColourPartner(event,i);
        int partner_id          = partner.first;
        int partner_status      = partner.second.second;
        std::vector<int> gluons = partner.second.first;

        // If it is colour connected to a charm quark calculate mass
        int sign = (has_unique_bp_quark?-1:1);
        if(event[partner_id].id() == sign*4 && partner_status==0) { 

          // std::cout << "Found " << i; 
          // std::cout << "\t" << event[i].nameWithStatus(); 
          // std::cout << "\t" << partner_id; 
          // std::cout << "\t" << event[partner_id].nameWithStatus();
          // std::cout << "\tstatus: "<< partner_status; 
          // std::cout << "\tgluons: " << gluons.size(); 
          b_parts_con_to_c = true;

          double bc_pe_total = event[i].e()  + event[partner_id].e();
          double bc_px_total = event[i].px() + event[partner_id].px();
          double bc_py_total = event[i].py() + event[partner_id].py();
          double bc_pz_total = event[i].pz() + event[partner_id].pz();
          
          for(const auto& gluon: gluons){ 
            bc_pe_total += event[gluon].e(); 
            bc_px_total += event[gluon].px(); 
            bc_py_total += event[gluon].py(); 
            bc_pz_total += event[gluon].pz(); 
          }
          b_parts_bc_sideband = sqrt(pow2(bc_pe_total)-pow2(bc_px_total)-pow2(bc_py_total)-pow2(bc_pz_total));
          // std::cout << "\tb_bc_sideband: " << b_parts_bc_sideband; 
          // std::cout << std::endl;
        }
      }
      // =======================================================================
      // use maps to keep track of which particles in event are of each type
      // =======================================================================

      if (has_unique_bp_quark) unique_bp_quarks[i]++;
      if (has_unique_bm_quark) unique_bm_quarks[i]++;
      if (has_unique_cp_quark) unique_cp_quarks[i]++;
      if (has_unique_cm_quark) unique_cm_quarks[i]++;
    
      // =======================================================================
      // Count how many of the b and c quarks come from hard process (23) or MPI (33)
      // =======================================================================
      
      if (has_unique_bp_quark ||
          has_unique_bm_quark ||
          has_unique_cp_quark ||
          has_unique_cm_quark){
        std::map<int,int> ancs = createListofAncestors(event,i,false);
        bool found23 = false; 
        bool found33 = false; 
        // for (const auto& anc: ancs) {
        //   if(abs(event[anc.first].status())==23) found23 = true;
        //   if(abs(event[anc.first].status())==33) found33 = true;
        // }
        

        if(abs(event[event[i].iTopCopy()].status()   )==33) found33 = true;
        if(abs(event[event[i].iTopCopyId()].status() )==23) found23 = true;

        if(found33) b_parts_isMPI = true;
        
      }


      // =======================================================================
      // Get more info about quarkonia 
      // =======================================================================    

      // if(event[i].idAbs() == 443){
      //   std::cout << "============ Found J/psi " << i  << " is Final? " << event[i].isFinal() << std::endl;
      // }
      if(event[i].idAbs() == 443 && event[i].isFinal()){
        b_event_Jpsi = true;
        getOniaType(  event, partonSystems, 4 ,i ,
                      b_event_Jpsi_isMixed,
                      b_event_Jpsi_isMixedLine,
                      b_event_Jpsi_isHard,
                      b_event_Jpsi_isMPI,
                      b_event_Jpsi_iscc,
                      b_event_Jpsi_iscc_type,
                      b_event_Jpsi_isDecay,
                      b_event_Jpsi_isHard_partSys, 
                      b_event_Jpsi_iscc_c_partSys, 
                      b_event_Jpsi_iscc_cbar_partSys); 
        // for(int ipartsys = 0; ipartsys < partonSystems.sizeSys(); ipartsys++){
        //   int sys_size =  partonSystems.sizeOut(ipartsys);
        //   std::cout << "System: " << ipartsys << "\t" <<sys_size << std::endl;
          
        //   for(int ioutpart = 0; ioutpart< sys_size; ioutpart++){
        //     int parton_id = partonSystems.getOut(ipartsys, ioutpart);
        //     std::cout <<  "\t["<< parton_id << "," <<event[parton_id].nameWithStatus() <<"]";
        //   }
        //   std::cout << std::endl;
        // }
        
        if(b_event_Jpsi_isHard||b_event_Jpsi_isMPI){

          int first_system_id = event[i].mother1();
          int first_system    = partonSystems.getSystemOf(first_system_id);
          if(first_system==-1){
            first_system_id = event[event[i].mother1()].mother1();
            first_system    = partonSystems.getSystemOf(first_system_id);
          }
          // std::cout << "SYSTEM mother: " <<  first_system<< "\t" << first_system_id<<  std::endl;
          // std::cout << "b_event_Jpsi_isHard_partSys " <<  b_event_Jpsi_isHard_partSys<<  std::endl;
        }
      }

      // if(event[i].idAbs() == 553){
      //   std::cout << "============ Found Upsilon(1S) " << i  << " is Final? " << event[i].isFinal() << std::endl;
      // }
      if(event[i].idAbs() == 553 && event[i].isFinal()){
        b_event_Upsilon1S = true;
        getOniaType(  event,partonSystems, 5 ,i ,
                      b_event_Upsilon1S_isMixed,
                      b_event_Upsilon1S_isMixedLine,
                      b_event_Upsilon1S_isHard,
                      b_event_Upsilon1S_isMPI,
                      b_event_Upsilon1S_iscc,
                      b_event_Upsilon1S_iscc_type,
                      b_event_Upsilon1S_isDecay,
                      b_event_Upsilon1S_isHard_partSys, 
                      b_event_Upsilon1S_iscc_c_partSys, 
                      b_event_Upsilon1S_iscc_cbar_partSys);  
      }

      // =======================================================================
      // Fill particle tree for certain particle types 
      // =======================================================================

      bool writeOutParticle = false;

      // Save B and D mesons
      if( event[i].isHadron()    &&
          abs(event[i].id())>400 &&
          abs(event[i].id())<600 ) writeOutParticle = true;

      // Save b and c baryons  
      if( event[i].isHadron()    &&
          abs(event[i].id())>4000 &&
          abs(event[i].id())<6000 ) writeOutParticle = true;

      if(has_unique_bp_quark) writeOutParticle = true;
      if(has_unique_bm_quark) writeOutParticle = true;
      if(has_unique_cp_quark) writeOutParticle = true;
      if(has_unique_cm_quark) writeOutParticle = true;

      // Save TTree
      if(writeOutParticle){
        b_parts_eta          = event[i].eta();        
        b_parts_pT           = event[i].pT();        
        b_parts_id           = event[i].id();
        b_parts_event        = iEvent; 
        b_parts_particle     = i; 
        b_parts_weight       = weight; 
        b_parts_isFinal      = event[i].isFinal();
        b_parts_status       = event[i].status();

        // if(customSettings["TomCustom:OnlySaveBc"]=="on"){
        //   if(abs(b_parts_id)==541) outputparticles->Fill();
        
        // } else if(customSettings["TomCustom:OnlySaveborc"]=="on"){
        //   if(foundb ||foundc) outputparticles->Fill();
            
        // } else if(customSettings["TomCustom:OnlySavebandc"]=="on"){
        //   if(foundb && foundc) outputparticles->Fill();
        
        // } else {
        //   outputparticles->Fill();
        // }
      }

    } // End of loop over particles 

    // =========================================================================
    // Look for events with one Bc, one Bx and one Dx meson   
    // =========================================================================
    if(nJpsi==1){
      b_event_jpsi_px = temp_jpsi_px; 
      b_event_jpsi_py = temp_jpsi_py; 
      b_event_jpsi_pz = temp_jpsi_pz; 
      b_event_jpsi_pe = temp_jpsi_pe;
      b_event_jpsi_ID = temp_jpsi_ID;
    }

    if(nUpsilon1S==1){
      b_event_upsilon1s_px = temp_upsilon1s_px; 
      b_event_upsilon1s_py = temp_upsilon1s_py; 
      b_event_upsilon1s_pz = temp_upsilon1s_pz; 
      b_event_upsilon1s_pe = temp_upsilon1s_pe;
      b_event_upsilon1s_ID = temp_upsilon1s_ID;
    }


    if(nXibc0==1){
      b_event_upsilon1s_px = temp_Xibc0_px; 
      b_event_upsilon1s_py = temp_Xibc0_py; 
      b_event_upsilon1s_pz = temp_Xibc0_pz; 
      b_event_upsilon1s_pe = temp_Xibc0_pe;
      b_event_upsilon1s_ID = temp_Xibc0_ID;
      b_event_upsilon1s_b1_partSys = temp_Xibc0_b_partSys;
      b_event_upsilon1s_b2_partSys = temp_Xibc0_c_partSys;
    }

    if(nXibcp==1){
      b_event_upsilon1s_px = temp_Xibcp_px; 
      b_event_upsilon1s_py = temp_Xibcp_py; 
      b_event_upsilon1s_pz = temp_Xibcp_pz; 
      b_event_upsilon1s_pe = temp_Xibcp_pe;
      b_event_upsilon1s_ID = temp_Xibcp_ID;
      b_event_upsilon1s_b1_partSys = temp_Xibcp_b_partSys;
      b_event_upsilon1s_b2_partSys = temp_Xibcp_c_partSys;
    }

    if(nXiccp==1){
      b_event_jpsi_px = temp_Xiccp_px; 
      b_event_jpsi_py = temp_Xiccp_py; 
      b_event_jpsi_pz = temp_Xiccp_pz; 
      b_event_jpsi_pe = temp_Xiccp_pe;
      b_event_jpsi_ID = temp_Xiccp_ID;
      b_event_jpsi_c1_partSys = temp_Xiccp_c1_partSys;
      b_event_jpsi_c2_partSys = temp_Xiccp_c2_partSys;
    }

    if(nXiccpp==1){
      b_event_jpsi_px = temp_Xiccpp_px; 
      b_event_jpsi_py = temp_Xiccpp_py; 
      b_event_jpsi_pz = temp_Xiccpp_pz; 
      b_event_jpsi_pe = temp_Xiccpp_pe;
      b_event_jpsi_ID = temp_Xiccpp_ID;
      b_event_jpsi_c1_partSys = temp_Xiccpp_c1_partSys;
      b_event_jpsi_c2_partSys = temp_Xiccpp_c2_partSys;
    }

    if(nOmegacc0==1){
      b_event_jpsi_px = temp_Omegacc0_px; 
      b_event_jpsi_py = temp_Omegacc0_py; 
      b_event_jpsi_pz = temp_Omegacc0_pz; 
      b_event_jpsi_pe = temp_Omegacc0_pe;
      b_event_jpsi_ID = temp_Omegacc0_ID;
      b_event_jpsi_c1_partSys = temp_Omegacc0_c1_partSys;
      b_event_jpsi_c2_partSys = temp_Omegacc0_c2_partSys;
    }

    if(nBc==1){
      b_event_bc_px = temp_bc_px; 
      b_event_bc_py = temp_bc_py; 
      b_event_bc_pz = temp_bc_pz; 
      b_event_bc_pe = temp_bc_pe;
      b_event_bc_id = temp_bc_id;
    }

    if(nXb == 1 ){ 
      b_event_bx_px = temp_bx_px; 
      b_event_bx_py = temp_bx_py; 
      b_event_bx_pz = temp_bx_pz; 
      b_event_bx_pe = temp_bx_pe;
      b_event_bx_id = temp_bx_id;
      b_event_bx_partSys = temp_bx_partSys;
    }

    if(nXc == 1 ){ 
      b_event_dx_px = temp_dx_px; 
      b_event_dx_py = temp_dx_py; 
      b_event_dx_pz = temp_dx_pz; 
      b_event_dx_pe = temp_dx_pe;
      b_event_dx_id = temp_dx_id;
      b_event_dx_partSys = temp_dx_partSys;
    }

    if( nDp_p>0) {
      b_event_Dp_p_px = temp_Dp_p_px; 
      b_event_Dp_p_py = temp_Dp_p_py; 
      b_event_Dp_p_pz = temp_Dp_p_pz; 
      b_event_Dp_p_pe = temp_Dp_p_pe;
      b_event_Dp_p_id = temp_Dp_p_id;
      b_event_Dp_p_partSys = temp_Dp_p_partSys;
    }
    if( nBu_p>0) {
      b_event_Bu_p_px = temp_Bu_p_px; 
      b_event_Bu_p_py = temp_Bu_p_py; 
      b_event_Bu_p_pz = temp_Bu_p_pz; 
      b_event_Bu_p_pe = temp_Bu_p_pe;
      b_event_Bu_p_id = temp_Bu_p_id;
      b_event_Bu_p_partSys = temp_Bu_p_partSys;
    }
    // =========================================================================
    // Look for events with two Dx mesons   
    // =========================================================================    
    if(nXc == 2 ){ 
      
      // If dx1 contains c it comes first, otherwise second
      // For events with c cbar this means dx1 = c and dx2 = cbar 
      // For events with c c or cbar cbar it shouldn't matter?

      if( temp_dx1_id == 411  ||  // D+          (c dbar)
          temp_dx1_id == 421  ||  // D0          (c ubar)
          temp_dx1_id == 431  ||  // Ds+         (c sbar)
          temp_dx1_id == 4122 ||  // Lambda_c+   (u d c)
          temp_dx1_id == 4222 ||  // Sigma_c++   (u u c)
          temp_dx1_id == 4212 ||  // Sigma_c+    (u d c)
          temp_dx1_id == 4112 ||  // Sigma_c0    (d d c)
          temp_dx1_id == 4232 ||  // Xi_c+       (u s c)
          temp_dx1_id == 4132 ||  // Xi_c0       (d s c)
          temp_dx1_id == 4322 ||  // Xiprime_c+  (s u c)
          temp_dx1_id == 4312 ||  // Xiprime_c0  (s d c)
          temp_dx1_id == 4332){   // Omega_c0    (s s c)
        b_event_dx_px = temp_dx1_px; 
        b_event_dx_py = temp_dx1_py; 
        b_event_dx_pz = temp_dx1_pz; 
        b_event_dx_pe = temp_dx1_pe;
        b_event_dx_id = temp_dx1_id;
        b_event_dx_partSys = temp_dx1_partSys;

        b_event_dx2_px = temp_dx2_px; 
        b_event_dx2_py = temp_dx2_py; 
        b_event_dx2_pz = temp_dx2_pz; 
        b_event_dx2_pe = temp_dx2_pe;
        b_event_dx2_id = temp_dx2_id;
        b_event_dx2_partSys = temp_dx2_partSys;

      } else {
        b_event_dx_px = temp_dx2_px; 
        b_event_dx_py = temp_dx2_py; 
        b_event_dx_pz = temp_dx2_pz; 
        b_event_dx_pe = temp_dx2_pe;
        b_event_dx_id = temp_dx2_id;
        b_event_dx_partSys = temp_dx2_partSys;

        b_event_dx2_px = temp_dx1_px; 
        b_event_dx2_py = temp_dx1_py; 
        b_event_dx2_pz = temp_dx1_pz; 
        b_event_dx2_pe = temp_dx1_pe;
        b_event_dx2_id = temp_dx1_id;
        b_event_dx2_partSys = temp_dx1_partSys;

      }

    }


    // =========================================================================
    // Look for events with two Bx mesons   
    // =========================================================================    
    if(nXb == 2 ){ 

      if(temp_bx1_id == -511 || // B0         (d bbar)
         temp_bx1_id == -521 || // B+         (u bbar)
         temp_bx1_id == -531 || // Bs0        (s bbar)
         temp_bx1_id == 5122 || // Lambda_b0  (u d b)
         temp_bx1_id == 5112 || // Sigma_b-   (d d b)
         temp_bx1_id == 5212 || // Sigma_b0   (u d b)
         temp_bx1_id == 5222 || // Sigma_b+   (u u b)
         temp_bx1_id == 5132 || // Xi_b-      (d s b)
         temp_bx1_id == 5232 || // Xi_b_0     (u s b)
         temp_bx1_id == 5312 || // Xiprime_b- (s d b)
         temp_bx1_id == 5322 || // Xiprime_b0 (u s b)
         temp_bx1_id == 5332    // Omega_b-   (s s b) 
         ){
        b_event_bx_px = temp_bx1_px; 
        b_event_bx_py = temp_bx1_py; 
        b_event_bx_pz = temp_bx1_pz; 
        b_event_bx_pe = temp_bx1_pe;
        b_event_bx_id = temp_bx1_id;
        b_event_bx_partSys = temp_bx1_partSys;

        b_event_bx2_px = temp_bx2_px; 
        b_event_bx2_py = temp_bx2_py; 
        b_event_bx2_pz = temp_bx2_pz; 
        b_event_bx2_pe = temp_bx2_pe;
        b_event_bx2_id = temp_bx2_id;
        b_event_bx2_partSys = temp_bx2_partSys;

      } else {
        b_event_bx_px = temp_bx2_px; 
        b_event_bx_py = temp_bx2_py; 
        b_event_bx_pz = temp_bx2_pz; 
        b_event_bx_pe = temp_bx2_pe;
        b_event_bx_id = temp_bx2_id;
        b_event_bx_partSys = temp_bx2_partSys;

        b_event_bx2_px = temp_bx1_px; 
        b_event_bx2_py = temp_bx1_py; 
        b_event_bx2_pz = temp_bx1_pz; 
        b_event_bx2_pe = temp_bx1_pe;
        b_event_bx2_id = temp_bx1_id;
        b_event_bx2_partSys = temp_bx1_partSys;
      }

    }

    // =========================================================================
    // Inspect 4 momenta 
    // =========================================================================
      
    if(    unique_bp_quarks.size()==1 
        && unique_bm_quarks.size()==1  
        ){
      int bp_i = 0;
      int bm_i = 0;
      for(const auto& unique_bp_quark:unique_bp_quarks) bp_i = unique_bp_quark.first;
      for(const auto& unique_bm_quark:unique_bm_quarks) bm_i = unique_bm_quark.first;
      delta_phi_b = acos((event[bp_i].px()*event[bm_i].px() + event[bp_i].py()*event[bm_i].py())/(event[bp_i].pT()*event[bm_i].pT()));
    }  
    if(    unique_cp_quarks.size()==1 
        && unique_cm_quarks.size()==1  
        ){
      int cp_i = 0;
      int cm_i = 0;
      for(const auto& unique_cp_quark:unique_cp_quarks) cp_i = unique_cp_quark.first;
      for(const auto& unique_cm_quark:unique_cm_quarks) cm_i = unique_cm_quark.first;
      delta_phi_c = acos((event[cp_i].px()*event[cm_i].px() + event[cp_i].py()*event[cm_i].py())/(event[cp_i].pT()*event[cm_i].pT()));
    }



    if(    unique_bp_quarks.size()==1 
        && unique_bm_quarks.size()==1 
        && unique_cp_quarks.size()==1 
        && unique_cm_quarks.size()==1 
        ){
      int bp_i = 0;
      int bm_i = 0;
      int cp_i = 0;
      int cm_i = 0;
      for(const auto& unique_bp_quark:unique_bp_quarks) bp_i = unique_bp_quark.first;
      for(const auto& unique_bm_quark:unique_bm_quarks) bm_i = unique_bm_quark.first;
      for(const auto& unique_cp_quark:unique_cp_quarks) cp_i = unique_cp_quark.first;
      for(const auto& unique_cm_quark:unique_cm_quarks) cm_i = unique_cm_quark.first;

      // std::cout << "Found b and c" << std::endl;
      // std::cout << "Veto foundbc: " << mybcUserHooks->foundbc << std::endl;
      // std::cout << "Found c :";
      // std::cout << "\tHard c " << foundHardc;
      // std::cout << "\tMPI c " << foundMPIc;
      // std::cout << "\tShower c  " << foundShowerc;
      // std::cout << std::endl;
      
      // std::cout << "Found b :";
      // std::cout << "\tHard b " << foundHardb;
      // std::cout << "\tMPI b " << foundMPIb;
      // std::cout << "\tShower b  " << foundShowerb;
      // std::cout << std::endl;

      // std::cout << "\tbp pT:  \t"<< event[bp_i].pT();
      // std::cout << "\tbp eta: \t"<< event[bp_i].eta();
      // std::cout << "\tbp phi: \t"<< event[bp_i].phi();
      // std::cout << std::endl;

      // std::cout << "\tbm pT:  \t"<< event[bm_i].pT();
      // std::cout << "\tbm eta: \t"<< event[bm_i].eta();
      // std::cout << "\tbm phi: \t"<< event[bm_i].phi();
      // std::cout << std::endl;
      
      // std::cout << "\tcp pT:  \t"<< event[cp_i].pT();
      // std::cout << "\tcp eta: \t"<< event[cp_i].eta();
      // std::cout << "\tcp phi: \t"<< event[cp_i].phi();
      // std::cout << std::endl;
      
      // std::cout << "\tcm pT:  \t"<< event[cm_i].pT();
      // std::cout << "\tcm eta: \t"<< event[cm_i].eta();
      // std::cout << "\tcm phi: \t"<< event[cm_i].phi();
      // std::cout << std::endl;

      double delta_phi_b_temp = acos((event[bp_i].px()*event[bm_i].px() + event[bp_i].py()*event[bm_i].py())/(event[bp_i].pT()*event[bm_i].pT()));
      double delta_phi_c_temp = acos((event[cp_i].px()*event[cm_i].px() + event[cp_i].py()*event[cm_i].py())/(event[cp_i].pT()*event[cm_i].pT()));
      // std::cout << "\tb delta phi: \t"<< delta_phi_b_temp;
      // std::cout << std::endl;
      // std::cout << "\tc delta phi: \t"<< delta_phi_c_temp;
      // std::cout << std::endl;
      // std::cout << std::endl;

    }

    // =========================================================================
    // Fill event tree 
    // =========================================================================
    
    b_event_nBs           =  nBs;        
    b_event_nBu           =  nBu;        
    b_event_nBu_p         =  nBu_p;        
    b_event_nBu_m         =  nBu_m;        
    b_event_nBd           =  nBd;        
    b_event_nBc           =  nBc;        
    b_event_nBc_p         =  nBc_p;        
    b_event_nBc_m         =  nBc_m;        
    b_event_nBds          =  nBds;         
    b_event_nLb           =  nLb;         
    b_event_nDz           =  nDz;        
    b_event_nDp           =  nDp;        
    b_event_nDp_p         =  nDp_p;        
    b_event_nDp_m         =  nDp_m;        
    b_event_nDs           =  nDs;       
    b_event_nLc           =  nLc;   

    b_event_nEtac         = nEtac;
    b_event_nJpsi         = nJpsi;
    b_event_nChic2        = nChic2;

    b_event_nEtab         = nEtab;
    b_event_nUpsilon1S    = nUpsilon1S;
    b_event_nChib2        = nChib2;

    b_event_nSigmabm       = nSigmabm;
    b_event_nSigmab0       = nSigmab0;
    b_event_nSigmabp       = nSigmabp;
    b_event_nXibm          = nXibm;
    b_event_nXib0          = nXib0;
    b_event_nXiprimebm     = nXiprimebm;
    b_event_nXiprimeb0     = nXiprimeb0;
    b_event_nOmegabm       = nOmegabm;
    b_event_nXibc0         = nXibc0;
    b_event_nXibc0_p       = nXibc0_p;
    b_event_nXibc0_m       = nXibc0_m;
    b_event_nXibcp         = nXibcp;
    b_event_nXibcp_p       = nXibcp_p;
    b_event_nXibcp_m       = nXibcp_m;
    b_event_nXiprimebc0    = nXiprimebc0;
    b_event_nXiprimebcp    = nXiprimebcp;
    b_event_nOmegabc0      = nOmegabc0;
    b_event_nOmegabc0_p    = nOmegabc0_p;
    b_event_nOmegabc0_m    = nOmegabc0_m;
    b_event_nOmegaprimebc0 = nOmegaprimebc0;
    b_event_nOmegabccp     = nOmegabccp;
    b_event_nXibbm         = nXibbm;
    b_event_nXibb0         = nXibb0;
    b_event_nOmegabbm      = nOmegabbm;
    b_event_nOmegabbc0     = nOmegabbc0;
    b_event_nOmegabbbm     = nOmegabbbm;
    b_event_nSigmacpp      = nSigmacpp;
    b_event_nSigmacp       = nSigmacp;
    b_event_nXicp          = nXicp;
    b_event_nXic0          = nXic0;
    b_event_nXiprimecp     = nXiprimecp;
    b_event_nXiprimec0     = nXiprimec0;
    b_event_nOmegac0       = nOmegac0;
    b_event_nXiccp         = nXiccp;
    b_event_nXiccp_p       = nXiccp_p;
    b_event_nXiccp_m       = nXiccp_m;
    b_event_nXiccpp        = nXiccpp;
    b_event_nXiccpp_p      = nXiccpp_p;
    b_event_nXiccpp_m      = nXiccpp_m;
    b_event_nOmegacc0      = nOmegacc0;
    b_event_nOmegacccpp    = nOmegacccpp;

    b_event_nXb           = nXb;
    b_event_nXc           = nXc;

    b_event_nbquark       =  getNonZeroEntries(unique_bp_quarks);    
    b_event_nbquarkbar    =  getNonZeroEntries(unique_bm_quarks);
    b_event_ncquark       =  getNonZeroEntries(unique_cp_quarks);    
    b_event_ncquarkbar    =  getNonZeroEntries(unique_cm_quarks); 
    b_event_weight        =  weight; 
    b_event_pTHat         =  pythia.info.pTHat();
    b_event_sigmaGen      =  pythia.info.sigmaGen();
    b_event_sigmaErr      =  pythia.info.sigmaErr();
    b_event_nMPI          =  pythia.info.nMPI();

    b_event_nMultiplicity  = nFinal;
    b_event_nCharged       = nCharged;
    b_event_nChargedInLHCb = nChargedInLHCb;

    // =========================================================================
    // Print information about how long the generation and processing took
    // =========================================================================

    stop_event_proc = clock(); // Stop timer
    double t_event_proc = (double) (stop_event_proc-start_event_proc)/CLOCKS_PER_SEC;
    double t_event_gen  = (double) (stop_event_gen- start_event_gen )/CLOCKS_PER_SEC;

    t_total_gen  += t_event_gen; 
    t_total_proc += t_event_proc; 

    b_event_timeGen = t_event_gen;
    b_event_timeProc = t_event_proc;
    b_event_CPUPower = cpu_power;


    if(iEvent % pythia.settings.mode("Next:numberCount")==0){
      std::cout << "Event: " << iEvent;
      std::cout << "\t Generation: " << 1000*t_event_gen << " ms";
      std::cout << "\t Processing: " << 1000*t_event_proc<< " ms";
      std::cout << "\t n(b+bbar): " << b_event_nbquark+b_event_nbquarkbar;
      std::cout << "\t n(c+cbar): " << b_event_ncquark+b_event_ncquarkbar;
      std::cout << "\t n(Bc): " << b_event_nBc;
      std::cout << std::endl;
    }

    // =========================================================================
    // Fill tree
    // =========================================================================
    // std::cout << "Number of MPI: " << b_event_nMPI << std::endl;
    // if(foundb ||foundc) 
    if(customSettings["TomCustom:OnlySaveBc"]=="on"){
      if(nBc>0) outputtree->Fill();
    
    } else if(customSettings["TomCustom:OnlySaveborc"]=="on"){
      if(foundb ||foundc) outputtree->Fill();
        
    } else if(customSettings["TomCustom:OnlySavebandc"]=="on"){
      if(foundb && foundc) outputtree->Fill();
    
    } else {
      outputtree->Fill();
    }




  }


  // outputparticles->Write();
  outputtree->Write();
  outputfile->Close();
  
  // End of event loop. Determine run time.
  stop = clock(); // Stop timer
  t = (double) (stop-start)/CLOCKS_PER_SEC;

  // Statistics. Histograms.
  pythia.stat();

  cout << "Cross section: " << b_event_sigmaGen*1000 << " +/- " << b_event_sigmaErr*1000 << " microbarns"<<std::endl;
  // ---------------------------------------------------------------------------
  // Print runtime
  cout << "\n";
  cout << "|----------------------------------------|" << endl;
  cout << "| CPU Runtime = " << t << " sec" << endl;
  cout << "| Generation  = " << t_total_gen << " sec" << endl;
  cout << "| Processing  = " << t_total_proc << " sec" << endl;
  cout << "|----------------------------------------|" << "\n" << endl;
  cout << "| CPU/event   = " << 1000*t/nEvent << " msec" << endl;
  cout << "| Gen/event   = " << 1000*t_total_gen/nEvent << " msec" << endl;
  cout << "| Proc/event  = " << 1000*t_total_proc/nEvent << " msec" << endl;
  cout << "|----------------------------------------|" << "\n" << endl;

  cout << "Written file: " <<filename <<std::endl;
  return 0;
}


int getProcessType(Event& event, int heavy, int partid){

  int absheavy = abs(heavy);
  int type = 0;

  int part1 = partid;
  int part2 = partid+1;

  // First check if both particles are not the requested type
  if( (event[part1].idAbs()!=absheavy && event[part2].idAbs()!=absheavy)){
    type = -1;
  } else if(event[part1].idAbs()==absheavy && event[part2].idAbs()==absheavy){
    
    if(event[part1].id()==event[part2].id()){
      type = -absheavy;
    } else {
      type =  absheavy;
    }
  
  } else if(event[part1].idAbs()==absheavy && event[part2].idAbs()!=absheavy){
    type = event[part2].idAbs();
  
  } else if(event[part1].idAbs()!=absheavy && event[part2].idAbs()==absheavy){
    type = event[part1].idAbs();
  }
  return type;
}

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------
int getbProcessType(Event& event, int partid){
  // Determine what type of process lead to the hard b 
  int type = 0;
  bool print = false;

  int part1 = partid;
  int part2 = partid+1;

  if( (event[part1].idAbs()==5 && event[part2].idAbs()==21) ||
      (event[part2].idAbs()==5 && event[part1].idAbs()==21) ){
    if(print) std::cout << "b g process" << std::endl;
  type = 21;
  
  }else if( (event[part1].idAbs()==5 && event[part2].idAbs()==1) ||
            (event[part2].idAbs()==5 && event[part1].idAbs()==1) ){
    if(print) std::cout << "b d process" << std::endl;
    type = 1;
  
  }else if( (event[part1].idAbs()==5 && event[part2].idAbs()==2) ||
            (event[part2].idAbs()==5 && event[part1].idAbs()==2) ){
    if(print) std::cout << "b u process" << std::endl;
    type = 2;
  
  }else if( (event[part1].idAbs()==5 && event[part2].idAbs()==3) ||
            (event[part2].idAbs()==5 && event[part1].idAbs()==3) ){
    if(print) std::cout << "b s process" << std::endl;
    type = 3;
  
  }else if( (event[part1].idAbs()==5 && event[part2].idAbs()==4) ||
            (event[part2].idAbs()==5 && event[part1].idAbs()==4) ){
    if(print) std::cout << "b c process" << std::endl;
    type = 4;
  
  }else if( (event[part1].idAbs()==5 && event[part2].idAbs()==5)){
    if(print) std::cout << "b bbar " << std::endl;
    type = 5;
  
  }else if( (event[part1].idAbs()!=5 && event[part2].idAbs()!=5)){
    if(print) std::cout << "no outgoing b or bbar  " << std::endl;
    type = -1;
  
  } else {
    if(print) std::cout << "Other? i: " << event[part1].id()  <<" i+1: " << event[part2].id()<< std::endl;
    // pythia.event.list();
    type = 99;
  }
  return type;
}

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------
int getcProcessType(Event& event, int partid){
  int type = 0;
  bool print = false;

  int part1 = partid;
  int part2 = partid+1;
  // Determine what type of process lead to the hard b 
  if( (event[part1].idAbs()==4 && event[part2].idAbs()==21) ||
      (event[part2].idAbs()==4 && event[part1].idAbs()==21) ){
    if(print) std::cout << "c g process" << std::endl;
  type = 21;

  }else if( (event[part1].idAbs()==4 && event[part2].idAbs()==1) ||
            (event[part2].idAbs()==4 && event[part1].idAbs()==1) ){
    if(print) std::cout << "c d process" << std::endl;
    type = 1;

  }else if( (event[part1].idAbs()==4 && event[part2].idAbs()==2) ||
            (event[part2].idAbs()==4 && event[part1].idAbs()==2) ){
    if(print) std::cout << "c u process" << std::endl;
    type = 2;

  }else if( (event[part1].idAbs()==4 && event[part2].idAbs()==3) ||
            (event[part2].idAbs()==4 && event[part1].idAbs()==3) ){
    if(print) std::cout << "c s process" << std::endl;
    type = 3;

  }else if( (event[part1].idAbs()==4 && event[part2].idAbs()==5) ||
            (event[part2].idAbs()==4 && event[part1].idAbs()==5) ){
    if(print) std::cout << "c b process" << std::endl;
    type = 5;

  }else if( (event[part1].idAbs()==4 && event[part2].idAbs()==4)){
    if(print) std::cout << "c cbar " << std::endl;
    type = 5;

  }else if( (event[part1].idAbs()!=4 && event[part2].idAbs()!=4)){
    if(print) std::cout << "no outgoing c or cbar  " << std::endl;
    type = -1;

  } else {
    if(print) std::cout << "Other? i: " << event[part1].id()  <<" i+1: " << event[part2].id()<< std::endl;
    // pythia.event.list();
    type = 99;
  }
  return type;
}
// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------
int getNonZeroEntries(std::map<int,int> map){
  int count = 0;
  for(const auto& entry: map){
    if(entry.second>0){
      count++;
    }
  }
  return count;
}


// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------
int getFirstMotherofType(int type, Event& event, int i){
  std::vector<int> ids; 
  ids.push_back(event[i].mother1());
  ids.push_back(event[i].mother2());

  bool isDone = false;
  int max_tries = 1000;
  int count = 0;
  while(!isDone){
    std::vector<int> new_ids; 
    for(const auto & id: ids){
      if(abs(event[id].id())==type) {
        return id;
      } else {
        if(id != 0 && id != 1 && id != 2){
          new_ids.push_back(event[id].mother1());
          new_ids.push_back(event[id].mother2());
        }
      }
    }

    if(new_ids.size()==0) isDone = true;
    ids.clear();
    ids = new_ids;
    new_ids.clear();
    if(isDone) break;
  }
  return 0;
}

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------
std::map<int,int> createListofAncestors(Event& event, int i, bool print, bool stopatprocess){
  std::map<int,int> anc;
  std::vector<int> ids; 

  // Get the type of process of the mother
  std::pair<std::string,std::vector<int>> mothers = getMothersType(event,i);
  
  if(print) std::cout << "------"<<  std::endl;
  if(print) std::cout << "i: " << i ;
  if(print) std::cout << "\t" << mothers.first; 
  if(print) std::cout << "\t" << event[i].nameWithStatus(); 
  if(print) std::cout << "\t" << event[i].id(); 
  if(print) std::cout << "\t" << event[i].status();
  if(print) std::cout << "\t" << getStatusType(event[i].status());
  if(print) std::cout << std::endl;
  if(print) std::cout << std::endl;
  

  // Add the mothers ids to the list of ids to check
  // When using the option stopatprocess no mothers ids are added
  // if the particle has status 23 or 33
  if( !(stopatprocess && (abs(event[i].status())==33 || abs(event[i].status())==23 ) ) ){
    for(const auto& mother: mothers.second){
      ids.push_back(mother);
      anc[mother]++;
    }
  }


  bool isDone   = false;
  int max_tries = 1000;
  int count     = 0;
  while(!isDone && count<max_tries){
    count++;
    std::vector<int> new_ids; 
    
    // Loop over the mother ids and find mothers in turn
    for(const auto & id: ids){

      anc[event[id].mother1()]++;
      anc[event[id].mother2()]++;

        std::pair<std::string,std::vector<int>> mothers = getMothersType(event,id);

        if(print) std::cout << "i: " << id ;
        if(print) std::cout << "\t" << mothers.first; 
        if(print) std::cout << "\t" << event[id].nameWithStatus(); 
        if(print) std::cout << "\t" << event[id].id();
        if(print) std::cout << "\t" << event[id].status();
        if(print) std::cout << "\t" << getStatusType(event[id].status());
        if(print) std::cout << std::endl;
        
        // Put the mothers into vector 
        if( !(stopatprocess && (abs(event[id].status())==33 || abs(event[id].status())==23 )) ){
          for(const auto& thing: mothers.second){
            new_ids.push_back(thing);
          }
        }
      
    }

    if(print) std::cout << std::endl;
    if(new_ids.size()==0) isDone = true;
    ids.clear();
    ids = new_ids;
    new_ids.clear();
  }
  if(print) std::cout << "------"<<  std::endl;

  return anc;
}
// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------

std::pair<std::string,std::vector<int>> getMothersType(Event& event, int i){
  std::vector<int> mothers;
  std::string name;
  int mother1 = event[i].mother1();
  int mother2 = event[i].mother2();
  if(i==5 || i==6){
    name = "hard process";
  } else if (i==3 || i == 4){
    name = "initial particles";
  } else if(mother1==0 && mother2==0){
    name = "system";
  } else if(mother1 > 0 && mother2 == mother1) {
    name = "recoil";
    mothers.push_back(mother1);
  } else if(mother1 > 0 && mother2 == 0) {
    name = "normal";
    mothers.push_back(mother1);
  } else if(mother1 == 0 && mother2 > 0) {
    name = "inverted";
    mothers.push_back(mother2);
  } else {
    name = "two mothers";
    mothers.push_back(mother1);
    mothers.push_back(mother2);
  }

  return std::make_pair(name,mothers);
}

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------

std::string getStatusType(int code){
  
  if(       abs(code)>10 && abs(code) < 20 ){
    return "beam particles";
  } else if(abs(code)>20 && abs(code) < 30 ){
    return "hardest particles";
  } else if(abs(code)>30 && abs(code) < 40 ){
    return "subprocess";
  } else if(abs(code)>40 && abs(code) < 50 ){
    return "ISR";
  } else if(abs(code)>50 && abs(code) < 60 ){
    return "FSR";
  } else if(abs(code)>60 && abs(code) < 70 ){
    return "beam remnant";
  } else if(abs(code)>70 && abs(code) < 80 ){
    return "hadronisation partons";
  } else if(abs(code)>80 && abs(code) < 90 ){
    return "hadronisation hadron";
  } else if(abs(code)>90 && abs(code) < 100 ){
    return "decay process";
  } else if(abs(code)>100 && abs(code) < 110 ){
    return "R-hadron";
  } else if(abs(code)>120 && abs(code) < 130 ){
    return "special hadron";
  } else {
    return "other";
  }
}

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------

std::vector<int> getStringofGluons(Event & event, int b_col, int c_col, bool isb_plus, int first_b_id, int first_c_id){
  
  // vector of ints to return
  std:vector<int> gluons;

  // Start with the positively charged quark
  // i.e. which ever had col()
  int current_colour = (isb_plus?b_col:c_col);  
  int target_colour  = (isb_plus?c_col:b_col);  
  
  bool matches_target_col = false;
  bool broken_string      = false;
  
  int max_tries = 1000;
  int count = 0;
  while(!matches_target_col && !broken_string && count<max_tries){ 
    count++;
    
    bool found_matching_colour = false;
    // Loop through event and look for corresponding acol()
    for (int j=1; j<event.size(); ++j) {
      int event_col = event[j].acol();
      
      // If the particle matches the current colour
      if( event_col == current_colour ){
        found_matching_colour = true;
        std::cout << "\tFound: "    << j << "\t" << event[j].nameWithStatus();
        std::cout << "\tcol: "    << event[j].col();
        std::cout << "\tacol: "   << event[j].acol();
        std::cout << "\t daug1: " << event[j].daughter1();
        std::cout << "\t daug2: " << event[j].daughter2();
        std::cout << "\t b daug: "<< event[first_b_id].daughter1();
        std::cout << std::endl;
        
        // If daugter is equal to daughter of the b/c meson
        if( event[j].daughter1() == event[first_b_id].daughter1() ){
          int opposite_col = event[j].col();
          std::cout << "Matching opposite color: " << opposite_col<< std::endl;
          gluons.push_back(j);
          
          if(opposite_col == target_colour) {
            matches_target_col = true;
          } else {
            current_colour = opposite_col;
          }
        }


      }

    }
    if(!found_matching_colour) broken_string = true;
  }

  return gluons;
}

// -----------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------

std::pair<int,std::pair<std::vector<int>,int>> getColourPartner(const Event & event, int b_id){
  
  bool isb_plus = (event[b_id].id()>0);
  
  // Start with the positively charged quark
  // i.e. which ever had col()
  int current_colour    = (isb_plus?event[b_id].col():event[b_id].acol());   
  int current_colour_op = (isb_plus?event[b_id].acol():event[b_id].col());   
  int current_particle = b_id;
  
  std::vector<int> gluons;
  int status = 0;
  // std::cout << "Current Particle: " << b_id << "\t" << event[b_id].nameWithStatus();
  // std::cout << "\tcurrent_colour: "    << current_colour;
  // std::cout << "\tcurrent_colour_op: " << current_colour_op;
  // std::cout << "\t daug1: " << event[b_id].daughter1() << " "<< event[event[b_id].daughter1()].nameWithStatus();
  // std::cout << "\t daug2: " << event[b_id].daughter2() << " "<< event[event[b_id].daughter2()].nameWithStatus();
  // std::cout << std::endl;

  bool found_endpoint     = false;
  bool broken_string      = false;
  
  int count = 0;
  int max_count = 500;
  while(!found_endpoint && !broken_string && count<max_count){ 
    count++;
    // Loop through event and find particles with the same colour
    // where the particle doesn't have daughters of the same type 
    // i.e. want to find particles just before hadronisation 
    std::vector<int> matching_particles;
    for (int j=1; j<event.size(); ++j){
      int event_col    = (isb_plus?event[j].acol():event[j].col()); 
      
      if( event_col == current_colour ) {
        
        bool matching_daug = false;
        if(event[j].id() ==  event[event[j].daughter1()].id() ||
           event[j].id() ==  event[event[j].daughter2()].id()) matching_daug = true;

        if(!matching_daug) matching_particles.push_back(j);
      }
    }

    // if(matching_particles.size()>1) std::cout << "Found " << matching_particles.size() << " matching particles " << std::endl; 

    // Print out matching particles
    for(const auto & part : matching_particles){
      int event_col    = (isb_plus?event[part].acol():event[part].col());   
      int event_col_op = (isb_plus?event[part].col():event[part].acol());

      bool matching_daug = false;
      if(event[part].id() ==  event[event[part].daughter1()].id() ||
         event[part].id() ==  event[event[part].daughter2()].id()) matching_daug = true; 

      // std::cout << "\t "    << part << "\t" << event[part].nameWithStatus();
      // std::cout << "\tevent_col: "    << event_col;
      // std::cout << "\taevent_col_op: "   << event_col_op;
      // std::cout << "\t daug1: " << event[part].daughter1() << "\t"<< event[event[part].daughter1()].nameWithStatus();
      // std::cout << "\t daug2: " << event[part].daughter2() << "\t"<< event[event[part].daughter2()].nameWithStatus();
      // std::cout << "\t Matching?: " << matching_daug;
      // std::cout << std::endl;   
    }

    // If there's only one particle that matches
    if(matching_particles.size()==1){
      int j = matching_particles[0];
      int event_col_op = (isb_plus?event[j].col():event[j].acol());
      
      current_particle = j;

      if(event_col_op == 0) {
        found_endpoint = true;
      } else {
        current_colour = event_col_op;
        gluons.push_back(j);
      }

    }

    // If there's more than one particle that matches
    // Pick first for now
    if(matching_particles.size()>1){
      int j = matching_particles[0];
      int event_col_op = (isb_plus?event[j].col():event[j].acol());
      
      current_particle = j;
      status += 1;
      if(event_col_op == 0) {
        found_endpoint = true;
      } else {
        current_colour = event_col_op;
        gluons.push_back(j);

      }
    }

    if(matching_particles.size()==0) {
      broken_string = true;
      status = -1;
    }
  }
  if(count==max_count) {
    // std::cout << "ERROR reached max_count" << std::endl;
    status = -2;
    gluons.clear();
  }

  return std::make_pair(current_particle,std::make_pair(gluons,status));

}

// -----------------------------------------------------------------------------
// Get Shower status
// -----------------------------------------------------------------------------


void getShowerStatus( Pythia& pythia,
                      Event &event, 
                      int flavour, 
                      bool &foundShowerMPIFSR,
                      bool &foundShowerMPIISR,
                      bool &foundShowerHardFSR,
                      bool &foundShowerHardISR,
                      bool &foundShowerBeam,
                      std::map<int,int>* Ptrgluons,
                      std::map<int,std::vector<int>>* Ptrqqbar
                      ){


  // Loop through event and collect all top ancestor quarks
  std::map<int,int> quarks;
  for (int i=1; i<event.size(); ++i) {
    if(event[i].idAbs()==flavour) {
      quarks[event[i].iTopCopyId()]++;
    }
  }
  
  // Determine the mothers of all ancestor quarks
  // std::map<int,int> gluons = *Ptrgluons;
  std::map<int,int> beam_rem;
  std::map<int,int> others;
  for(const auto& quark : quarks){
      int i = quark.first;

      if(       event[event[i].mother1()].idAbs()==21 ||
                event[i].mother1()==1 ||
                event[i].mother1()==2
        ) {
        (*Ptrgluons)[event[i].mother1()]++;
        (*Ptrqqbar)[event[i].mother1()].push_back(i);
      } else {
        others[event[i].mother1()]++;
      }
  }

  // Investigate where the gluons came from
  for(const auto& gluon: *Ptrgluons){
    int i = gluon.first;
    
    // Status of gluon 
    bool eventis23 = abs(event[i].status()) == 23;
    bool eventis33 = abs(event[i].status()) == 33;

    // Status of gluon ancestors
    bool ancis23 = false;
    bool ancis33 = false;
    std::map<int,int> ancs = createListofAncestors(event,i,false);
    for(const auto& anc:ancs){
      if(abs(event[anc.first].status())==23) ancis23 = true;
      if(abs(event[anc.first].status())==33) ancis33 = true;
    }

    // Status of gluon daughters
    std::vector<int> daughters = event[i].daughterListRecursive();

    bool daughteris33 = false; 
    bool daughteris23 = false; 
    for(const auto& daughter:daughters){
      if(abs(event[daughter].status())==23) daughteris23 = true;
      if(abs(event[daughter].status())==33) daughteris33 = true;
    }

    // Decide what statuses it gets 
    if(eventis23 || ancis23 ) foundShowerHardFSR = true;
    if(eventis33 || ancis33)  foundShowerMPIFSR  = true;
    if(daughteris23)          foundShowerHardISR = true;
    if(daughteris33)          foundShowerMPIISR  = true;

    if(!foundShowerHardFSR &&
       !foundShowerHardISR &&
       !foundShowerMPIFSR  &&
       !foundShowerMPIISR  
       ) {
      foundShowerBeam = true;
    }
  }

  if(others.size()>0){
    // pythia.event.list();
    std::cout << "Found not gluon" << std::endl;
    for(const auto& other: others){
      std::cout << "Particle: "<<other.first<< std::endl;
    } 
  } 

}

// ===========================================================================
// Check for charmonia 
// ===========================================================================

bool isCharmonia(int id){
  switch(abs(id)) {
    case 441:
      return true;
    case 10441:
      return true;
    case 100441:
      return true;
    case 443:
      return true;
    case 10443:
      return true;
    case 20443:
      return true;
    case 100443:
      return true;
    case 30443:
      return true;
    case 9000443:
      return true;
    case 9010443:
      return true;
    case 9020443:
      return true;
    case 445:
      return true;
    case 100445:
      return true;
    // ==== Add other J/psi states 
    case 9940003:
      return true; 
    case 9941003:
      return true; 
    case 9942003:
      return true; 
    default:
      return false;
  }
}
// ===========================================================================
// Check for bottomonia 
// ===========================================================================

bool isBottomonia(int id){
  switch(abs(id)) {
    case 551:
      return true;
    case 10551:
      return true;
    case 100551:
      return true;
    case 110551:
      return true;
    case 200551:
      return true;
    case 210551:
      return true;
    case 553:
      return true;
    case 10553:
      return true;
    case 20553:
      return true;
    case 30553:
      return true;
    case 100553:
      return true;
    case 110553:
      return true;
    case 120553:
      return true;
    case 130553:
      return true;
    case 200553:
      return true;
    case 210553:
      return true;
    case 220553:
      return true;
    case 300553:
      return true;
    case 9000553:
      return true;
    case 9010553:
      return true;
    case 555:
      return true;
    case 10555:
      return true;
    case 20555:
      return true;
    case 100555:
      return true;
    case 110555:
      return true;
    case 120555:
      return true;
    case 200555:
      return true;
    case 557:
      return true;
    case 100557:
      return true;
    default:
      return false;
  }
}

int octetFlavour(int id){
  return abs(id - 9900000)/10000;
}

vector<int> getDigits(int id){
    int mod1(10), mod2(1);
  vector<int> digits;
  while (digits.size() < 7) {
    digits.push_back((id%mod1 - id%mod2) / mod2);
    mod1 *= 10;
    mod2 *= 10;
  }
  return digits;
}

// -----------------------------------------------------------------------------
int getQuarkLineEndpoint(Event &event, int cquark_i){
  bool print = false;
  bool foundTop = false;
  
  // Keep track of processes
  bool foundHard = false;
  bool foundMPI  = false;

  // Get the id of the c or cbar
  int cquark_id = event[cquark_i].id();

  while(!foundTop){
    if(print) std::cout << "c quark start: cquark_i: " << cquark_i  << "\t"<<  event[cquark_i].nameWithStatus() << " status: " << event[cquark_i].status();

    if(event[cquark_i].status() == -33 ||
       event[cquark_i].status() == -31 ) foundMPI = true;
    if(event[cquark_i].status() == -23 ||
       event[cquark_i].status() == -21 ) foundHard = true;

    bool foundSameMother = false;  
    if(print) std::cout << "\tMothers: ";
    for(const auto& mother : event[cquark_i].motherList()  ) {
      if(print) std::cout << " " << event[mother].nameWithStatus();
      if(event[mother].id() == cquark_id) {
        foundSameMother = true;
        cquark_i = mother;
      }
    }
    if(print) std::cout << std::endl;

    // If the mothers don't contain the same flavour quark, we've reached the top
    if(!foundSameMother){
      foundTop = true;
    }
  }

  // Now we've reached the top of a chain, swap to the cbar and go down as far as we can
  bool foundBottom  = false;
  int cquarkbar_id = -cquark_id;
  int cquarkbar_i = -99999;

  // Get daughters of mother1 to find the cbar
  if(print) std::cout << "Daughters of mother 1: ";
  for(const auto& daug_i: event[event[cquark_i].mother1()].daughterList()){
    if(print) std::cout << event[daug_i].nameWithStatus() << " ";
    if(event[daug_i].id()==cquarkbar_id) cquarkbar_i = daug_i;
  }
  if(print) std::cout << std::endl;

  // If there's some problem
  if(cquarkbar_i ==-99999){
    std::cout << "ERROR Can't find cquarkbar_i " << std::endl;
    return cquarkbar_i;
  } else {
    if(print) std::cout << "Using cquarkbar_i " << cquarkbar_i << " " << event[cquarkbar_i].status() << std::endl;
  }

  while(!foundBottom){
    if(print) std::cout << "cbar quark start: cquarkbar_i: " << cquarkbar_i  << "\t"<<  event[cquarkbar_i].nameWithStatus() << " status: " << event[cquarkbar_i].status();

    if(event[cquarkbar_i].status() == -33 ||
       event[cquarkbar_i].status() == -31 ) foundMPI = true;
    if(event[cquarkbar_i].status() == -23 ||
       event[cquarkbar_i].status() == -21 ) foundHard = true;

    bool foundSameDaughter = false;  
    if(print) std::cout << "\tDaughters: ";
    for(const auto& daughter : event[cquarkbar_i].daughterList()  ) {
      if(print) std::cout << " " << event[daughter].nameWithStatus();
      if(event[daughter].id() == cquarkbar_id) {
        foundSameDaughter = true;
        cquarkbar_i = daughter;
      }
    }
    if(print) std::cout << std::endl;

    // If the mothers don't contain the same flavour quark, we've reached the top
    if(!foundSameDaughter){
      foundBottom = true;
    }

  }

  if(print) std::cout << "Highest point: " << event[event[cquark_i].mother1()].nameWithStatus();
  if(print) std::cout << "\t  " << event[event[cquark_i].mother1()].status();
  if(print) std::cout << std::endl;
  if(print) std::cout << "Processes " << (foundHard?"Hard":" ");
  if(print) std::cout << "\t  " << (foundMPI? "MPI":" ");
  if(print) std::cout << std::endl;

  return cquarkbar_i;
}

// -----------------------------------------------------------------------------
void getOniaType(
  Event &event, 
  PartonSystems &partonSystems, 
  int flavour, 
  int i,
  bool &isMixed,
  bool &isMixedLine,
  bool &isHard,
  bool &isMPI,
  bool &iscc,
  int  &iscc_type,
  bool &isDecay,
  int  &isHard_partSys,
  int  &iscc_c_partSys,
  int  &iscc_cbar_partSys
  ){
  bool print = false;

  isMixed     = false;
  isMixedLine = false;
  isHard      = false;
  isMPI       = false;
  iscc        = false;
  iscc_type   = 0;
  isDecay     = false;
  
  int current_id = i;
  int onia_id = event[i].id();
  int partner_id = 0;

  std::string onia_name = event[i].nameWithStatus();

  if(print) std::cout << "==*=*=*=*=*= Final particle : " << i << " " << onia_name<< std::endl;
  for(const auto& daug_i: event[i].daughterList()){
    std::cout << "    " << event[daug_i].nameWithStatus();
  }
  if(print) std::cout << std::endl;
  if(print) std::cout << " mother1: " << event[event[i].mother1()].nameWithStatus() << std::endl;
  if(print) std::cout << " mother2: " << event[event[i].mother2()].nameWithStatus() << std::endl;


  // Work out what type of event we have
  if(       abs(event[i].status()) == 23){
    isHard = true;
    if(abs(event[i-1].status()) == 23) partner_id = i-1;
    if(abs(event[i+1].status()) == 23) partner_id = i+1;
  } else if(abs(event[i].status()) == 33){
    isMPI  = true;
    if(abs(event[i-1].status()) == 33) partner_id = i-1;
    if(abs(event[i+1].status()) == 33) partner_id = i+1;

  } else if(  (abs(event[event[i].mother1()].idAbs()) == flavour) && 
              (abs(event[event[i].mother2()].idAbs()) == flavour)){
    iscc = true;
  } else {
    isDecay = true;
    std::map<int,int> ancs    = createListofAncestors(event,i,false,true);
    
    // Loop until we find what particle it came from
    bool isTopDecay = false;
    while(! isTopDecay){
      std::pair<std::string,std::vector<int>> mothers = getMothersType(event,current_id);
      bool isSingleParent = (mothers.second.size()==1);
      bool isStatus23     = abs(event[current_id].status())==23;
      bool isStatus33     = abs(event[current_id].status())==33;
      if(isStatus33 || isStatus23) isTopDecay = true;
      if(!isSingleParent) {
        isTopDecay = true;
      } else {
        current_id = mothers.second[0];
      }
    }
    if(print) std::cout << "  Top decay: " << current_id << "  " << event[current_id].nameWithStatus()  << "  " << event[current_id].status() << std::endl;
    if(print) std::cout << "    isAbs: " << event[current_id].idAbs()  << std::endl;
    
    // If the top decay was also a J/psi it was not a decay but just recoil etc.
    if(event[current_id].idAbs() == onia_id) isDecay = false;
    
    // Also count octet states as not decays
    vector<int> digits = getDigits(event[current_id].id());
    int new_id = 100000*digits[2] + 
                  10000*digits[1] +
                   1000*0         +
                    100*digits[4] +
                     10*digits[4] +
                      1*digits[0];

    if(new_id == onia_id) isDecay = false;


    if(       abs(event[current_id].status()) == 23){
      isHard = true;
      if(abs(event[current_id-1].status()) == 23) partner_id = current_id-1;
      if(abs(event[current_id+1].status()) == 23) partner_id = current_id+1;
    } else if(abs(event[current_id].status()) == 33){
      isMPI  = true;
      if(abs(event[current_id-1].status()) == 33) partner_id = current_id-1;
      if(abs(event[current_id+1].status()) == 33) partner_id = current_id+1;
    } else if(  (abs(event[event[current_id].mother1()].idAbs()) == flavour) && 
                (abs(event[event[current_id].mother2()].idAbs()) == flavour)){
      iscc = true;
    } else {

    }
  }

  if(print) std::cout << "  isHard  "  << isHard <<  std::endl;
  if(print) std::cout << "  isMPI   "  << isMPI  << std::endl;
  if(print) std::cout << "  iscc    "   << iscc  << std::endl;
  if(print) std::cout << "  isDecay "   << isDecay  << std::endl;

  // if(iscc) pythia.event.list();

  if(       isHard){
    if(print) std::cout << "Found hard " << onia_name << std::endl;
    isMixed = false;
    isMixedLine = false;
    if(print) std::cout <<  "    "<<event[current_id].nameWithStatus() << " " << event[partner_id].nameWithStatus()<<   std::endl;
    isHard_partSys = getPartonSystem(event,partonSystems,i);

  } else if(isMPI){
    if(print) std::cout << "Found MPI " << onia_name  << std::endl;
    isMixed = false;
    isMixedLine = false;
    if(print) std::cout <<  "    "<<event[current_id].nameWithStatus() << " " << event[partner_id].nameWithStatus()<<   std::endl;
    isHard_partSys = getPartonSystem(event,partonSystems,i);

  } else if(iscc){
    if(print) std::cout << "Found "<<onia_name<<" from q-qbar"  << std::endl;
    if(print) std::cout << "  **** c i: " << i << " current_id: " << current_id  << std::endl;
    
    // Loop over the ancestors and check whether the c and cbar came from hard/MPI processes 
    std::map<int,int> ancs_c    = createListofAncestors(event,event[current_id].mother1(),print,true);
    std::vector<int> process_ids_c;
    for (const auto& anc: ancs_c) {
      if(abs(event[anc.first].status())==23) process_ids_c.push_back(anc.first);
      if(abs(event[anc.first].status())==33) process_ids_c.push_back(anc.first);
    }
    iscc_c_partSys = getPartonSystem(event,partonSystems,event[current_id].mother1());

    if(print) std::cout << "  **** c-bar"  << std::endl;
    std::map<int,int> ancs_cbar = createListofAncestors(event,event[current_id].mother2(),print,true);
    std::vector<int> process_ids_cbar;
    for (const auto& anc: ancs_cbar) {
      if(abs(event[anc.first].status())==23) process_ids_cbar.push_back(anc.first);
      if(abs(event[anc.first].status())==33) process_ids_cbar.push_back(anc.first);
    }

    iscc_cbar_partSys = getPartonSystem(event,partonSystems,event[current_id].mother2());

    // Print out hard/MPI processes 
    if(print) std::cout << "  ==== print q hard processes..."  << std::endl;

    for(const auto& process_id_c : process_ids_c){
      if(event[process_id_c].status() == event[process_id_c-1].status()){
      if(print) std::cout <<  "    "<<event[process_id_c].nameWithStatus() << " " << event[process_id_c-1].nameWithStatus()<<   std::endl;

      } else if(event[process_id_c].status() == event[process_id_c+1].status()) {
      if(print) std::cout <<  "    "<<event[process_id_c].nameWithStatus() << " " << event[process_id_c+1].nameWithStatus()<<   std::endl;

      }
    }
    
    if(print) std::cout << "  ==== print qbar hard processes..."  << std::endl;

    for(const auto& process_id_cbar : process_ids_cbar){
      if(event[process_id_cbar].status() == event[process_id_cbar-1].status()){
      if(print) std::cout <<  "    "<<event[process_id_cbar].nameWithStatus() << " " << event[process_id_cbar-1].nameWithStatus()<<   std::endl;

      } else if(event[process_id_cbar].status() == event[process_id_cbar+1].status()) {
      if(print) std::cout <<  "    "<<event[process_id_cbar].nameWithStatus() << " " << event[process_id_cbar+1].nameWithStatus()<<   std::endl;

      }
    }


    // Check whether the c and cbar came from the same or different processes
    bool sameProcess = false;
    for(const auto& id_c: process_ids_c){
      for(const auto& id_cbar: process_ids_cbar){
        if(id_c == id_cbar) {
          sameProcess = true;
          if(abs(event[id_c].status())==23) iscc_type = 1; 
          if(abs(event[id_c].status())==33) iscc_type = 2; 
        }
      }
    }

    if(sameProcess){
      isMixed = false;
    } else {
      isMixed = true;
      
      // Try to work out what processes produced the c and cbar quarks
      if(process_ids_c.size()==0 && process_ids_cbar.size()==0){
        iscc_type = 10;
      

      } else if(process_ids_c.size()==0){
        iscc_type = 10000;
        for(const auto& id_c: process_ids_c){
          if(abs(event[id_c].status())==23) iscc_type += 1; 
          if(abs(event[id_c].status())==33) iscc_type += 10; 
        }
        for(const auto& id_c: process_ids_cbar){
          if(abs(event[id_c].status())==23) iscc_type += 100; 
          if(abs(event[id_c].status())==33) iscc_type += 1000; 
        }

      }else if(process_ids_cbar.size()==0){
        iscc_type = 20000;
        for(const auto& id_c: process_ids_c){
          if(abs(event[id_c].status())==23) iscc_type += 1; 
          if(abs(event[id_c].status())==33) iscc_type += 10; 
        }
        for(const auto& id_c: process_ids_cbar){
          if(abs(event[id_c].status())==23) iscc_type += 100; 
          if(abs(event[id_c].status())==33) iscc_type += 1000; 
        }
      
      } else {
        iscc_type = 30000;
        for(const auto& id_c: process_ids_c){
          if(abs(event[id_c].status())==23) iscc_type += 1; 
          if(abs(event[id_c].status())==33) iscc_type += 10; 
        }
        for(const auto& id_c: process_ids_cbar){
          if(abs(event[id_c].status())==23) iscc_type += 100; 
          if(abs(event[id_c].status())==33) iscc_type += 1000; 
        }
      }
    }
    if(print) std::cout << "    isMixed:   " << isMixed  << std::endl;
    if(print) std::cout << "    iscc_type: " << iscc_type  << std::endl;

    // Try tracing the quark line up until we get to something that is not the same flavour quark 
    int func_cbar_i = getQuarkLineEndpoint(event,event[current_id].mother1());
    int func_c_i    = getQuarkLineEndpoint(event,event[current_id].mother2());

    if(func_cbar_i == event[current_id].mother2()){
      if(print) std::cout << "***** Found same cbar as in "<<onia_name<<", therefore NOT MIXED" << std::endl;
      isMixedLine = false;
    } else {
      if(print) std::cout << "***** Found different cbar, MIXED" << std::endl;
      isMixedLine = true;
    }

    if(print) std::cout << "mother1, endpoint: " << event[current_id].mother1() << ", " << func_cbar_i << std::endl;
    if(print) std::cout << "mother2, endpoint: " << event[current_id].mother2() << ", " << func_c_i << std::endl;

  } else {
    if(print) std::cout << "  Not sure what type"     << std::endl;
    if(print) std::cout << "  isHard  "   << isHard   << std::endl;
    if(print) std::cout << "  isMPI   "   << isMPI    << std::endl;
    if(print) std::cout << "  iscc    "   << iscc     << std::endl;
    if(print) std::cout << "  isDecay "   << isDecay  << std::endl;
  }     

  if(print) std::cout << "    final isMixed: " << isMixed  << std::endl;
  // return current_id;
  if(print) std::cout << "==*=*=*=*=*= End  " << std::endl;
}

// -----------------------------------------------------------------------------

int getHeavyHadronPartSys(Event &event,PartonSystems& partonSystems,int i){

  bool print  = false;

  int heavy_id = event[i].id();
  int heavy_flavour = -999;

  if( heavy_id == 411  ||  // D+          (c dbar)
      heavy_id == 421  ||  // D0          (c ubar)
      heavy_id == 431  ||  // Ds+         (c sbar)
      heavy_id == 4122 ||  // Lambda_c+   (u d c)
      heavy_id == 4222 ||  // Sigma_c++   (u u c)
      heavy_id == 4212 ||  // Sigma_c+    (u d c)
      heavy_id == 4112 ||  // Sigma_c0    (d d c)
      heavy_id == 4232 ||  // Xi_c+       (u s c)
      heavy_id == 4132 ||  // Xi_c0       (d s c)
      heavy_id == 4322 ||  // Xiprime_c+  (s u c)
      heavy_id == 4312 ||  // Xiprime_c0  (s d c)
      heavy_id == 4332){   // Omega_c0    (s s c)
    heavy_flavour = 4;
  } else if(
      heavy_id == -411  ||  // D+          (c dbar)
      heavy_id == -421  ||  // D0          (c ubar)
      heavy_id == -431  ||  // Ds+         (c sbar)
      heavy_id == -4122 ||  // Lambda_c+   (u d c)
      heavy_id == -4222 ||  // Sigma_c++   (u u c)
      heavy_id == -4212 ||  // Sigma_c+    (u d c)
      heavy_id == -4112 ||  // Sigma_c0    (d d c)
      heavy_id == -4232 ||  // Xi_c+       (u s c)
      heavy_id == -4132 ||  // Xi_c0       (d s c)
      heavy_id == -4322 ||  // Xiprime_c+  (s u c)
      heavy_id == -4312 ||  // Xiprime_c0  (s d c)
      heavy_id == -4332){   // Omega_c0    (s s c)){
    heavy_flavour = -4;
  } else if(
      heavy_id == -511 || // B0         (d bbar)
      heavy_id == -521 || // B+         (u bbar)
      heavy_id == -531 || // Bs0        (s bbar)
      heavy_id == 5122 || // Lambda_b0  (u d b)
      heavy_id == 5112 || // Sigma_b-   (d d b)
      heavy_id == 5212 || // Sigma_b0   (u d b)
      heavy_id == 5222 || // Sigma_b+   (u u b)
      heavy_id == 5132 || // Xi_b-      (d s b)
      heavy_id == 5232 || // Xi_b_0     (u s b)
      heavy_id == 5312 || // Xiprime_b- (s d b)
      heavy_id == 5322 || // Xiprime_b0 (u s b)
      heavy_id == 5332    // Omega_b-   (s s b) 
         ){
    heavy_flavour = 5;
  } else if(
      heavy_id ==   511 || // B0         (d bbar)
      heavy_id ==   521 || // B+         (u bbar)
      heavy_id ==   531 || // Bs0        (s bbar)
      heavy_id == -5122 || // Lambda_b0  (u d b)
      heavy_id == -5112 || // Sigma_b-   (d d b)
      heavy_id == -5212 || // Sigma_b0   (u d b)
      heavy_id == -5222 || // Sigma_b+   (u u b)
      heavy_id == -5132 || // Xi_b-      (d s b)
      heavy_id == -5232 || // Xi_b_0     (u s b)
      heavy_id == -5312 || // Xiprime_b- (s d b)
      heavy_id == -5322 || // Xiprime_b0 (u s b)
      heavy_id == -5332    // Omega_b-   (s s b) 
         ){
    heavy_flavour = -5;
  } else {
    std::cout << "ERROR: Can't find heavy flavour" << std::endl;
    return -999; 
  }
  if(print) std::cout << "Looking for " << heavy_flavour << std::endl;

  // ---- New method 

  
  // Loop over the mothers of the hadron and look for any charm quark (of the right sign) or charm diquarks
  // Check the parton system of each
  bool isDone = false;
  std::vector<int> ids = event[i].motherList();

  std::vector<int> foundpartonSystemsVec;
  int count = 0;
  while(!isDone){
    std::vector<int> new_ids;

    for(const auto& mother : ids  ) {
      if(event[mother].id()   == heavy_flavour ||
        (event[mother].idAbs()!= abs(heavy_flavour) && event[mother].particleDataEntry().nQuarksInCode(abs(heavy_flavour))>0)){
        int parton_sys = partonSystems.getSystemOf(mother);
        if(print) std::cout << "----> " << count << " i: " << mother << "  " << event[mother].nameWithStatus() << "  " << parton_sys<< std::endl;
        
        // If we find the parton system record it
        // Otherwise keep checking the mothers
        if(parton_sys!=-1){
          if (std::find(foundpartonSystemsVec.begin(), 
                        foundpartonSystemsVec.end(), 
                        parton_sys) == foundpartonSystemsVec.end()) {
            foundpartonSystemsVec.push_back(parton_sys);
          }

        } else {        
          for(const auto& new_id : event[mother].motherList()){
            new_ids.push_back(new_id);
          }
        }
      } 
    }

    if(new_ids.size()==0) isDone = true;
    ids.clear();
    ids = new_ids;
    new_ids.clear();
    count++;
  }

  if(foundpartonSystemsVec.size()==1){
    if(print) std::cout << "Found 1 parton system: " << foundpartonSystemsVec[0] << std::endl;
    return foundpartonSystemsVec[0];
  } else if(foundpartonSystemsVec.size()==2){
    return foundpartonSystemsVec[0];
    if(print) std::cout << "Found 2 parton systems: " << foundpartonSystemsVec[0] << ", "<< foundpartonSystemsVec[1] << std::endl;
  } else if(foundpartonSystemsVec.size()==0){
    if(print) std::cout << "Found 0 parton system " << std::endl;
    // return -999;  
  } else {
    if(print) std::cout << "Found "<< foundpartonSystemsVec.size()<<" parton systems: " << foundpartonSystemsVec[0] << ", "<< foundpartonSystemsVec[1] << std::endl;
    return foundpartonSystemsVec[0];
  }




  // Existing method
  if(print) std::cout << "    Attempt 1 X: " << event[i].nameWithStatus() << "\t mothers: ";
  bool foundheavy = false;
  int  foundheavy_i = -99;
  
  // Loop over mother looking for the one with heavy_flavour
  for(const auto& mother : event[i].motherList()  ) {
    if(print) std::cout << event[mother].nameWithStatus() << " "; 
    if(event[mother].id()==heavy_flavour&&!foundheavy){
      foundheavy = true;
      foundheavy_i = mother;
    }
  } 
  if(print) std::cout << std::endl;

  int particle = event[i].mother1();
  while(!foundheavy){
    if(print) std::cout << "    Attempt 2 X: " << event[i].nameWithStatus() << "\t mothers: ";
    for(const auto& mother : event[particle].motherList()  ) {
      if(print) std::cout << event[mother].nameWithStatus() << " "; 
      if(event[mother].id()==heavy_flavour&&!foundheavy){
        foundheavy = true;
        foundheavy_i = mother;
      }
    } 
    if(print) std::cout << std::endl;
    particle = event[particle].mother1();
    if(particle == 0 || particle == 1 || particle == 2){
      break;
    }
  }

  // This can happen if heacy hadron is from a decay e.g. psi(3770)->DD 
  if(foundheavy_i==-99){
    // std::cout << "ERROR: Can't find heavy quark from heavy hadron" << std::endl;
    return -9; 
  }
  return getPartonSystem(event,partonSystems,foundheavy_i,print);

}

// -----------------------------------------------------------------------------

int getPartonSystem(Event& event, PartonSystems& partonSystems, int heavy_i, bool print){

  int heavy_flavour = event[heavy_i].id();
  // if(abs(heavy_flavour)!=4 && abs(heavy_flavour) != 5) print = true;
  bool foundPartSys = false;
  int parton_sys = partonSystems.getSystemOf(heavy_i);

  // Loop until we find a parton system
  if(print) std::cout <<"  => Charm:   " << event[heavy_i].nameWithStatus(); 
  
  while(!foundPartSys){
    if(parton_sys==-1){
      heavy_i   = event[heavy_i].mother1();
      parton_sys = partonSystems.getSystemOf(heavy_i);
      if(print) std::cout <<"\tPartSys: "               << parton_sys;
      if(print) std::cout <<" (i: "               << heavy_i <<") ";
      if(print) std::cout <<" "                         << event[heavy_i].nameWithStatus();

    } else {
      foundPartSys = true;
    }
    // Stop if we get to the top
    if(heavy_i==1||heavy_i == 2||heavy_i == 0){
      foundPartSys = true;
    }
  }
  if(print) std::cout << std::endl; 

  if(parton_sys != -1){
    return parton_sys;
  }

  // Return if not heavy quark, i.e. onia
  if(abs(heavy_flavour)!=4 && abs(heavy_flavour)!= 5) return parton_sys;

  if(print) std::cout << "===> Found heavy_i " << heavy_i << " with partonSys " << parton_sys << std::endl;
  if(print) std::cout << std::endl;


  std::vector<int> unpaired_q_systems;
  std::vector<int> unpaired_qbar_systems;

  for(int ipartsys = 0; ipartsys < partonSystems.sizeSys(); ipartsys++){
    
    int n_requested_flavour     = 0;
    int n_requested_flavour_bar = 0;
    int sys_size =  partonSystems.sizeOut(ipartsys);
    if(print) std::cout << "System: " << ipartsys << "\t" <<sys_size << std::endl;

    for(int ioutpart = 0; ioutpart< sys_size; ioutpart++){
      int parton_id = partonSystems.getOut(ipartsys, ioutpart);
      
      if(event[parton_id].id()== heavy_flavour) n_requested_flavour++;
      if(event[parton_id].id()==-heavy_flavour) n_requested_flavour_bar++;
      
      if(event[parton_id].idAbs()==4 || event[parton_id].idAbs()==5){
        if(print) std::cout <<  "\t"<< parton_id << " " <<event[parton_id].nameWithStatus() << std::endl;
      }
    }
    if(print) std::cout << std::endl;

    // Loop for parton systems with an odd number of heavy quarks
    if((n_requested_flavour+n_requested_flavour_bar)%2==1){
      // One extra q quark
      if(n_requested_flavour==n_requested_flavour_bar+1) unpaired_q_systems.push_back(ipartsys);

      // One extra qbar quark 
      if(n_requested_flavour_bar==n_requested_flavour+1) unpaired_qbar_systems.push_back(ipartsys);
    }
  }

  // Only interested in systems with an unpaired quark of opposite flavour
  if(unpaired_qbar_systems.size()==1){
    if(print) std::cout << "One unpaired system: "<< unpaired_qbar_systems[0] << std::endl;
    parton_sys = unpaired_qbar_systems[0];
  } else {
    if(print) std::cout << "N unpaired: " <<unpaired_qbar_systems.size() << std::endl; 
  }
  if(print) std::cout << "===> Return " << parton_sys << std::endl;

  return parton_sys;
}
// -----------------------------------------------------------------------------

std::pair<int,int> getXibcPartSys(Event &event,PartonSystems& partonSystems,int i){
  bool print = true;
  if(print) std::cout << "Mother list for " << event[i].idAbs() << " " << std::endl;
  int heavy_flavour1 = 4;
  int heavy_flavour2 = 5;
  int heavy_charge = (event[i].id()>0?+1:-1);
  
  // Loop over the mothers of the hadron and look for any charm quark (of the right sign) or charm diquarks
  // Check the parton system of each
  bool isDone = false;
  std::vector<int> ids = event[i].motherList();

  std::vector<int> foundcpartonSystemsVec;

  // First look for charm parton system
  int count = 0;
  while(!isDone){
    std::vector<int> new_ids;

    for(const auto& mother : ids  ) {
      if(event[mother].id()   == heavy_flavour1*heavy_charge ||
        (event[mother].idAbs()!= heavy_flavour1 && event[mother].particleDataEntry().nQuarksInCode(heavy_flavour1)>0)){
        int parton_sys = partonSystems.getSystemOf(mother);
        if(print) std::cout << "----> "<< count<< " i: " << mother << "  " << event[mother].nameWithStatus() << "  " << parton_sys<< std::endl;

        // If we've found the parton system, store it, otherwise keep checking mothers
        if(parton_sys!=-1){
          if (std::find(foundcpartonSystemsVec.begin(), 
                        foundcpartonSystemsVec.end(), 
                        parton_sys) == foundcpartonSystemsVec.end()) {
            foundcpartonSystemsVec.push_back(parton_sys);
          }

        } else {        
          for(const auto& new_id : event[mother].motherList()){
            new_ids.push_back(new_id);
          }
        }
      } 
    }

    if(new_ids.size()==0) isDone = true;
    ids.clear();
    ids = new_ids;
    new_ids.clear();
    count++;

  }

  int c_id=-999, b_id=-999;
  if(foundcpartonSystemsVec.size()==1){
    if(print) std::cout << "Found 1 charm parton system: " << foundcpartonSystemsVec[0] << std::endl;
    c_id = foundcpartonSystemsVec[0];
  } else if(foundcpartonSystemsVec.size()==0){
    if(print) std::cout << "Found 0 charm parton system: " << std::endl;
    c_id = -1;
  } else {
    if(print) std::cout << "Found "<< foundcpartonSystemsVec.size()<<" charm parton systems: " << foundcpartonSystemsVec[0] << ", "<< foundcpartonSystemsVec[1] << std::endl;
    c_id = foundcpartonSystemsVec[0];
  }
  
  // Now look for b quarks 
  count = 0;
  isDone = false;

  ids = event[i].motherList();
  std::vector<int> foundbpartonSystemsVec;
  while(!isDone){
    std::vector<int> new_ids;

    for(const auto& mother : ids  ) {
      if(event[mother].id()   == heavy_flavour2*heavy_charge ||
        (event[mother].idAbs()!= heavy_flavour2 && event[mother].particleDataEntry().nQuarksInCode(heavy_flavour2)>0)){
        int parton_sys = partonSystems.getSystemOf(mother);
        if(print) std::cout << "----> "<< count<< " i: " << mother << "  " << event[mother].nameWithStatus() << "  " << parton_sys<< std::endl;

        // If we've found the parton system, store it, otherwise keep checking mothers
        if(parton_sys!=-1){
          if (std::find(foundbpartonSystemsVec.begin(), 
                        foundbpartonSystemsVec.end(), 
                        parton_sys) == foundbpartonSystemsVec.end()) {
            foundbpartonSystemsVec.push_back(parton_sys);
          }

        } else {        
          for(const auto& new_id : event[mother].motherList()){
            new_ids.push_back(new_id);
          }
        }
      } 
    }

    if(new_ids.size()==0) isDone = true;
    ids.clear();
    ids = new_ids;
    new_ids.clear();
    count++;

  }

  if(foundbpartonSystemsVec.size()==1){
    if(print) std::cout << "Found 1 beauty parton system: " << foundbpartonSystemsVec[0] << std::endl;
    b_id = foundbpartonSystemsVec[0];
  } else if(foundbpartonSystemsVec.size()==0){
    if(print) std::cout << "Found 0 beauty parton system: " << std::endl;
    b_id = -1;
  } else {
    if(print) std::cout << "Found "<< foundbpartonSystemsVec.size()<<" beauty parton systems: " << foundbpartonSystemsVec[0] << ", "<< foundbpartonSystemsVec[1] << std::endl;
    b_id = foundbpartonSystemsVec[0];
  }


  return std::make_pair(b_id,c_id);
}

// -----------------------------------------------------------------------------

std::pair<int,int> getDoublyHeavyHadronPartSys(Event &event,PartonSystems& partonSystems,int i){
  bool print = false;
  if(print) std::cout << "Mother list for " << event[i].idAbs() << " " << std::endl;
  int heavy_flavour = 4;
  int heavy_charge = (event[i].id()>0?+1:-1);
  
  // Loop over the mothers of the hadron and look for any charm quark (of the right sign) or charm diquarks
  // Check the parton system of each
  bool isDone = false;
  std::vector<int> ids = event[i].motherList();

  std::map<int,int> foundpartonSystemsMap;
  std::vector<int> foundpartonSystemsVec;

  int count = 0;
  while(!isDone){
    std::vector<int> new_ids;

    for(const auto& mother : ids  ) {
      if(event[mother].id()   == heavy_flavour*heavy_charge ||
        (event[mother].idAbs()!= heavy_flavour && event[mother].particleDataEntry().nQuarksInCode(heavy_flavour)>0)){
        int parton_sys = partonSystems.getSystemOf(mother);
        if(print) std::cout << "----> "<< count<< " i: " << mother << "  " << event[mother].nameWithStatus() << "  " << parton_sys<< std::endl;

        // If we've found the parton system, store it, otherwise keep checking mothers
        if(parton_sys!=-1) {
          foundpartonSystemsMap[parton_sys]++;
        } else {        
          for(const auto& new_id : event[mother].motherList()){
            new_ids.push_back(new_id);
          }
        }
      } 
    }

    if(new_ids.size()==0) isDone = true;
    ids.clear();
    ids = new_ids;
    new_ids.clear();
    count++;

  }

  // Just get the keys out of the map
  for(const auto& foundpartonSystemsPair: foundpartonSystemsMap ){
    foundpartonSystemsVec.push_back(foundpartonSystemsPair.first);
  }

  if(foundpartonSystemsVec.size()==1){
    if(print) std::cout << "Found 1 parton system: " << foundpartonSystemsVec[0] << std::endl;
    return std::make_pair(foundpartonSystemsVec[0],foundpartonSystemsVec[0]);
  } else if(foundpartonSystemsVec.size()==2){
    if(print) std::cout << "Found 2 parton systems: " << foundpartonSystemsVec[0] << ", "<< foundpartonSystemsVec[1] << std::endl;
    return std::make_pair(foundpartonSystemsVec[0],foundpartonSystemsVec[1]);
  } else if(foundpartonSystemsVec.size()==0){
    if(print) std::cout << "Found 0 parton system: " << std::endl;
    return std::make_pair(-1,-1);
  } else {
    if(print) std::cout << "Found "<< foundpartonSystemsVec.size()<<" parton systems: " << foundpartonSystemsVec[0] << ", "<< foundpartonSystemsVec[1] << std::endl;
    return std::make_pair(foundpartonSystemsVec[0],foundpartonSystemsVec[1]);
  }


  std::vector<int> charm_mothers;
  std::vector<int> parton_systems;
  std::vector<int> to_check;
  for(const auto& mother : event[i].motherList()  ) {
    int n_heavy = event[mother].particleDataEntry().nQuarksInCode(heavy_flavour);

    if(n_heavy==1 && event[mother].idAbs()==heavy_flavour){
      int parton_sys = getPartonSystem(event,partonSystems,mother,true);
      parton_systems.push_back(parton_sys);

      std::cout << "** Normal Charm: "<< event[mother].nameWithStatus();
      std::cout << ",\t" << event[mother].idAbs();
      std::cout << ",\ti: " << mother;
      std::cout << "\tparton system: " << parton_sys <<std::endl; 

    } else if(n_heavy==1 && event[mother].idAbs()!=heavy_flavour){
      
      int parton_sys     = getPartonSystem(event,partonSystems,mother,false);
      std::cout << "** diquark       "<< event[mother].nameWithStatus();
      std::cout << ",\t" << event[mother].idAbs();
      std::cout << ",\ti: " << mother;
      std::cout << "\tparton system: " << parton_sys <<std::endl; 

      int top_mother1_i = event[event[mother].iTopCopy()].mother1();
      int top_mother2_i = event[event[mother].iTopCopy()].mother2();

      int parton_sys_top_mother;
      if(       event[top_mother1_i].idAbs() == heavy_flavour){
        parton_sys_top_mother = getPartonSystem(event,partonSystems,top_mother1_i,false);
      } else if(event[top_mother2_i].idAbs() == heavy_flavour){
        parton_sys_top_mother = getPartonSystem(event,partonSystems,top_mother2_i,false);
      } else {
        std::cout << "ERROR no heavy mother of diquark" << std::endl;
        parton_sys_top_mother = -99999;
      }
      parton_systems.push_back(parton_sys_top_mother);
      
      int parton_sys_top = getPartonSystem(event,partonSystems,event[mother].iTopCopy(),false);

      std::cout << "    ** diquark Top   "<< event[event[mother].iTopCopy()].nameWithStatus();
      std::cout << ",\t" << event[event[mother].iTopCopy()].idAbs();
      std::cout << ",\ti: " << event[mother].iTopCopy();
      std::cout << "\tparton system: " << parton_sys <<std::endl; 

      int parton_sys_top1 = getPartonSystem(event,partonSystems,event[event[mother].iTopCopy()].mother1(),true);
      int parton_sys_top2 = getPartonSystem(event,partonSystems,event[event[mother].iTopCopy()].mother2(),true);

      std::cout << "    ** diquark Top    mother1: " << event[event[event[mother].iTopCopy()].mother1()].nameWithStatus() << " " <<parton_sys_top1 <<std::endl; 
      std::cout << "    ** diquark Top    mother2: " << event[event[event[mother].iTopCopy()].mother2()].nameWithStatus() << " " <<parton_sys_top2 <<std::endl; 


    } else if(n_heavy==2){
      int parton_sys = getPartonSystem(event,partonSystems,mother,true);

      std::cout << "    "<< event[mother].nameWithStatus();
      std::cout << "\ti "<< mother;
      std::cout << ",\t" << event[mother].idAbs();
      std::cout << "\tparton system: " << parton_sys<<std::endl;

      for(const auto& gdmother : event[event[mother].iTopCopy()].motherList()  ) {
        parton_sys = getPartonSystem(event,partonSystems,gdmother,true);
        parton_systems.push_back(parton_sys);

        std::cout << "        "<< event[gdmother].nameWithStatus();
        std::cout << "\ti "<< gdmother;
        std::cout << ",\t" << event[gdmother].idAbs();
        std::cout << "\tparton system: " << parton_sys<<std::endl;
      }
    }
  } 

  std::cout << std::endl;

  int parton_0 = -99;
  int parton_1 = -99;
  std::cout <<"Found " << parton_systems.size()<<" parton systems " << std::endl;
  for(int j = 0; j < parton_systems.size(); j++){
    std::cout << "    i: " <<parton_systems[j] <<std::endl;  
    if(j==0) parton_0 = parton_systems[j];  
    if(j==1) parton_1 = parton_systems[j];  
  }
  
  if(parton_systems.size()>2&&parton_0==parton_1){
    parton_1 = parton_systems[2];
  }

  // Print out 
  for(int ipartsys = 0; ipartsys < partonSystems.sizeSys(); ipartsys++){
    
    int sys_size =  partonSystems.sizeOut(ipartsys);
    std::cout << "System: " << ipartsys << "\t" <<sys_size << std::endl;

    for(int ioutpart = 0; ioutpart< sys_size; ioutpart++){
      int parton_id = partonSystems.getOut(ipartsys, ioutpart);
      
      
      if(event[parton_id].particleDataEntry().nQuarksInCode(heavy_flavour)>0){
        std::cout <<  "\t"<< parton_id << " " <<event[parton_id].nameWithStatus() << std::endl;
      }
    }
    std::cout << std::endl;
  }

  return std::make_pair(parton_0,parton_1);

}
