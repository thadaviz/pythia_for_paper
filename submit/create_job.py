import os

pythia_build_dir = "/afs/cern.ch/user/t/thadaviz/Pythia/Paper_Pythia/"

remaketar = True

# If false it will use this lfn rather than creating a new one
uploadedlfn = "/lhcb/user/t/thadaviz/GangaFiles_07.01_Friday_06_May_2022/pythia.tgz"

if remaketar:
    # -----------------------------------------------#
    ## make tar of pythia directory 
    # -----------------------------------------------#

    print('Make tgz of code directory')

    ## locations we want to include in the tar file 
    tar_locations  = 'releases/include/ '
    tar_locations += 'releases/lib/ '
    tar_locations += 'releases/bin/ '
    tar_locations += 'releases/share/ '
    tar_locations += 'pythia_for_paper/main202 '
    tar_locations += 'pythia_for_paper/Makefile '
    tar_locations += 'pythia_for_paper/Makefile.inc '
    tar_locations += 'pythia_for_paper/*.cmnd '

    ## temporary location we'll keep the tar file
    tar_file_loc = "/tmp/$USER/pythia.tgz"

    ## create tar file
    os.system(f"cd {pythia_build_dir}; tar czf {tar_file_loc} {tar_locations}")

    # -----------------------------------------------#
    ## Upload tar file to dirac 
    # -----------------------------------------------#

    print('Upload tar file')

    input_tar = DiracFile(tar_file_loc)
    input_tar.put()
else:
    input_tar = DiracFile("LFN:"+uploadedlfn)

# -----------------------------------------------#
## Create arguments 
# -----------------------------------------------#

## How many subjobs we want to run
num_subjobs = 1
first_seed = 50000

## Any options to be applied to all subjobs
# args_for_all_jobs = [  
#                         "mainSoftQCDnonDiffractive.cmnd",
#                         "--Main:numberOfEvents","1000",

#                         "--TomCustom:usingMinnisUserHook", "on",
#                         # "--TomCustom:noVetoPT", "on",
#                         # "--TomCustom:noVetoMPIStep", "on",
#                         "--TomCustom:VetoPTScale", "5.0",

#                         # "--TomCustom:passThrough", "on",
#                         # "--PartonLevel:MPI", "off",
#                     ]

# comment = "SoftQCD_UH_5.0"


# args_for_all_jobs = [  
#                         "mainSoftQCDnonDiffractive.cmnd",
#                         # "mainColourReconnections.cmnd",
#                         "--Main:numberOfEvents","200",

#                         "--TomCustom:usingbcUserHook", "on",
#                         # "--TomCustom:noVetoMass", "on",       ## No4
#                         # "--TomCustom:noVetoParton", "on",     ## No3
#                         # "--TomCustom:noVetoPT", "on",         ## No2
#                         # "--TomCustom:noVetoMPIStep", "on",    ## No1
#                         "--TomCustom:VetoPTScale", "4.00",
#                         "--PartonShowers:Model" , "1",

#                         # "--TomCustom:passThrough", "on",
#                         # "--PartonLevel:MPI", "off",
#                     ]

# # # comment = "SoftQCD_UH_bb_noPT_4.00"
# # # # comment = "SoftQCD_bbcc"

comment = "SoftQCD_Bc_withPartonSyst"

args_for_all_jobs = [  
                        ## ----------- options ---------------
                        "mainSoftQCDnonDiffractive.cmnd",
                        # "mainColourReconnections.cmnd",
                        
                        # ## ----------- cccc ---------------
                        # "--Main:numberOfEvents","40000",
                        # "--TomCustom:usingccUserHook", "on",
                        # "--TomCustom:quarkID","4", 
                        # "--TomCustom:VetoPTScale","1.50",
                        
                        # ## ----------- bbbb ---------------
                        # "--Main:numberOfEvents","40000",
                        # "--TomCustom:usingccUserHook", "on",
                        # "--TomCustom:quarkID","5", 
                        # "--TomCustom:VetoPTScale","4.00",
                        
                        # ## ----------- bbcc ---------------
                        # "--Main:numberOfEvents","20000",
                        # "--TomCustom:usingbbccUserHook", "on",

                        ## ----------- cc ---------------
                        # "--Main:numberOfEvents","100000",
                        # "--TomCustom:usingMinnisUserHook", "on",
                        # "--TomCustom:quarkID","4", 
                        # "--TomCustom:VetoPTScale","1.50",

                        # "--TomCustom:passThrough","on",

                        # ## ----------- bb ---------------
                        # "--Main:numberOfEvents","100000",
                        # "--TomCustom:usingMinnisUserHook", "on",
                        # "--TomCustom:VetoPTScale","4.00",

                        # ## ----------- Bc ---------------
                        "--Main:numberOfEvents","5",
                        "--TomCustom:usingbcUserHook", "on",
                        "--TomCustom:VetoPTScale","4.00",


                        "--PartonShowers:Model" , "1",
                        # "--PartonLevel:MPI", "off",
                    ]

# comment = "SoftQCD_cc_passthrough"

## Make a list of lists.
## Each list are the arguments for one job
## Must start random seeds at 1 as a seed of 0 gives a different sequence each time 
arguments = []
for i in range(1,num_subjobs+1):
    seed = first_seed + i
    args_for_job = args_for_all_jobs + ["--Random:setSeed","on","--Random:seed",f"{seed}"]
    arguments += [args_for_job]

# -----------------------------------------------#
## Create job 
# -----------------------------------------------#

print('Create job')

j = Job()
j.name = "Pythia"
j.comment = comment
j.application = Executable()
j.application.platform = 'x86_64-centos7-gcc9-opt'

## Assuming you're in the same directory as the wrapper
## This might need updating to your file structure
j.application.exe = File(pythia_build_dir+'pythia_for_paper/submit/pythia_job_wrapper.sh')
j.inputfiles = [input_tar]
j.outputfiles = [DiracFile("*.root")]
j.backend = Dirac()
# j.backend.settings['BannedSites'] = ['LCG.CPPM.fr',]
j.splitter=ArgSplitter(args=arguments)
queues.add(j.submit)
