#!/bin/bash 

export START_DIR=$PWD

## untar the pythia directory
tar -xzf pythia.tgz                       

echo 'Source enviroment'
## source root enviroment
# source  /cvmfs/sft.cern.ch/lcg/views/LCG_97python3/x86_64-centos7-gcc9-opt/setup.sh
source  /cvmfs/sft.cern.ch/lcg/views/LCG_104_LHCB_7/x86_64-el9-gcc12-opt/setup.sh
which root

echo 'Set env variables'
## point environment variables towards our installation of pythia
export PYTHIA8DATA=$PWD/releases/share/Pythia8/xmldoc/
export PYTHIA8=$PWD/releases/

echo 'ls -l'
ls -l

echo 'ls -l pythia_for_paper'
ls -l pythia_for_paper

echo 'ls -l releases'
ls -l releases

echo 'ls -l releases'
ls -l releases

## Run my example
cd pythia_for_paper

echo 'ldd main202'
ldd main202

echo 'ls ../releases/lib/libpythia8.so'
ls ../releases/lib/libpythia8.so

## Run program 
echo ./main202 $@
./main202 $@

# echo make clean
# make clean

# echo make main202
# make main202

# ## Run program 
# echo ./main202 $@
# ./main202 $@

## Move output to main directory
## probably need a better way of doing this
echo mv output/*.root $START_DIR
mv output/*.root $START_DIR

echo "Done!"
