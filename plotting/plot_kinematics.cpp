#include "tools.h"

void plot_kinematics(){

    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;


    double BcVegPy_crosssec_val = 113830880.77856530      * nanobarn; // nb
    double BcVegPy_crosssec_err =      4772.3452537976009 * nanobarn; // nb
    
    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bc_N_255_noMPI.root");
    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bc_N_20000.root");
    // TFile* pythia_file = TFile::Open("../output/main202output_SoftQCD_nd_UserHook_bc_N_10000_PartMod1.root");

    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bc_Scale_4.000000_N_194000_PartMod1.root");
    TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bc_Scale_4.000000_N_99200_PartMod1.root");

    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bc_Scale_4.000000_N_98000_noMPI_PartMod1.root");

    TTree* pythia_tree = (TTree*) pythia_file->Get("events");


    pythia_tree->SetAlias("bc_pt" , "sqrt(bc_px*bc_px+bc_py*bc_py)");
    pythia_tree->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_bx_dx","acos((bx_px*dx_px + bx_py*dx_py)/(bx_pt*dx_pt))");
    pythia_tree->SetAlias("angle_bc_dx","acos((bc_px*dx_px + bc_py*dx_py)/(bc_pt*dx_pt))");
    pythia_tree->SetAlias("angle_bc_bx","acos((bc_px*bx_px + bc_py*bx_py)/(bc_pt*bx_pt))");


    pythia_tree->SetAlias("bc_p",  "sqrt(bc_px*bc_px+bc_py*bc_py+bc_pz*bc_pz)");
    pythia_tree->SetAlias("bc_pt", "sqrt(bc_px*bc_px+bc_py*bc_py)");
    pythia_tree->SetAlias("bc_eta","(bc_pz>0?-1:1)*acosh(bc_p/bc_pt)");
    pythia_tree->SetAlias("bc_y","0.5*log( (bc_pe+bc_pz) / (bc_pe-bc_pz) )");

    pythia_tree->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree->SetAlias("delta_eta_bx_dx","bx_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_bc_dx","bc_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_bc_bx","bc_eta-bx_eta");

    pythia_tree->SetAlias("delta_y_bx_dx","bx_y-dx_y");
    pythia_tree->SetAlias("delta_y_bc_dx","bc_y-dx_y");
    pythia_tree->SetAlias("delta_y_bc_bx","bc_y-bx_y");

    pythia_tree->SetAlias("delta_R_bx_dx","sqrt(delta_eta_bx_dx*delta_eta_bx_dx+angle_bx_dx*angle_bx_dx)");
    pythia_tree->SetAlias("delta_R_bc_dx","sqrt(delta_eta_bc_dx*delta_eta_bc_dx+angle_bc_dx*angle_bc_dx)");
    pythia_tree->SetAlias("delta_R_bc_bx","sqrt(delta_eta_bc_bx*delta_eta_bc_bx+angle_bc_bx*angle_bc_bx)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia No MPI simulation
    // -------------------------------------------------------------------------

    TFile* pythia_nompi_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bc_Scale_4.000000_N_98000_noMPI_PartMod1.root");
    TTree* pythia_tree_nompi = (TTree*) pythia_nompi_file->Get("events");


    pythia_tree_nompi->SetAlias("bc_pt" , "sqrt(bc_px*bc_px+bc_py*bc_py)");
    pythia_tree_nompi->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree_nompi->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree_nompi->SetAlias("angle_bx_dx","acos((bx_px*dx_px + bx_py*dx_py)/(bx_pt*dx_pt))");
    pythia_tree_nompi->SetAlias("angle_bc_dx","acos((bc_px*dx_px + bc_py*dx_py)/(bc_pt*dx_pt))");
    pythia_tree_nompi->SetAlias("angle_bc_bx","acos((bc_px*bx_px + bc_py*bx_py)/(bc_pt*bx_pt))");


    pythia_tree_nompi->SetAlias("bc_p",  "sqrt(bc_px*bc_px+bc_py*bc_py+bc_pz*bc_pz)");
    pythia_tree_nompi->SetAlias("bc_pt", "sqrt(bc_px*bc_px+bc_py*bc_py)");
    pythia_tree_nompi->SetAlias("bc_eta","(bc_pz>0?-1:1)*acosh(bc_p/bc_pt)");
    pythia_tree_nompi->SetAlias("bc_y","0.5*log( (bc_pe+bc_pz) / (bc_pe-bc_pz) )");

    pythia_tree_nompi->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree_nompi->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree_nompi->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree_nompi->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    pythia_tree_nompi->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree_nompi->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_nompi->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree_nompi->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree_nompi->SetAlias("delta_eta_bx_dx","bx_eta-dx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_bc_dx","bc_eta-dx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_bc_bx","bc_eta-bx_eta");

    pythia_tree_nompi->SetAlias("delta_y_bx_dx","bx_y-dx_y");
    pythia_tree_nompi->SetAlias("delta_y_bc_dx","bc_y-dx_y");
    pythia_tree_nompi->SetAlias("delta_y_bc_bx","bc_y-bx_y");

    pythia_tree_nompi->SetAlias("delta_R_bx_dx","sqrt(delta_eta_bx_dx*delta_eta_bx_dx+angle_bx_dx*angle_bx_dx)");
    pythia_tree_nompi->SetAlias("delta_R_bc_dx","sqrt(delta_eta_bc_dx*delta_eta_bc_dx+angle_bc_dx*angle_bc_dx)");
    pythia_tree_nompi->SetAlias("delta_R_bc_bx","sqrt(delta_eta_bc_bx*delta_eta_bc_bx+angle_bc_bx*angle_bc_bx)");

    double sigmaGen_pythia_nompi;
    double sigmaErr_pythia_nompi;
    pythia_tree_nompi->SetBranchAddress("sigmaGen",&sigmaGen_pythia_nompi);
    pythia_tree_nompi->SetBranchAddress("sigmaErr",&sigmaErr_pythia_nompi);
    
    int n_entries_pythia_nompi = pythia_tree_nompi->GetEntries();
    // -------------------------------------------------------------------------
    // Get BcVegPy simulation
    // -------------------------------------------------------------------------

    TFile* bcvegpy_file = TFile::Open("../output/main202output_BcVegPy_N_10000_PartMod1.root");
    TTree* bcvegpy_tree = (TTree*) bcvegpy_file->Get("events");


    double sigmaGen_bcvegpy;
    double sigmaErr_bcvegpy;
    bcvegpy_tree->SetBranchAddress("sigmaGen",&sigmaGen_bcvegpy);
    bcvegpy_tree->SetBranchAddress("sigmaErr",&sigmaErr_bcvegpy);
    

    bcvegpy_tree->SetAlias("bc_pt" , "sqrt(bc_px*bc_px+bc_py*bc_py)");
    bcvegpy_tree->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");
    bcvegpy_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    bcvegpy_tree->SetAlias("angle_bx_dx","acos((bx_px*dx_px + bx_py*dx_py)/(bx_pt*dx_pt))");
    bcvegpy_tree->SetAlias("angle_bc_dx","acos((bc_px*dx_px + bc_py*dx_py)/(bc_pt*dx_pt))");
    bcvegpy_tree->SetAlias("angle_bc_bx","acos((bc_px*bx_px + bc_py*bx_py)/(bc_pt*bx_pt))");

    bcvegpy_tree->SetAlias("bc_p",  "sqrt(bc_px*bc_px+bc_py*bc_py+bc_pz*bc_pz)");
    bcvegpy_tree->SetAlias("bc_pt", "sqrt(bc_px*bc_px+bc_py*bc_py)");
    bcvegpy_tree->SetAlias("bc_eta","(bc_pz>0?-1:1)*acosh(bc_p/bc_pt)");
    bcvegpy_tree->SetAlias("bc_y","0.5*log( (bc_pe+bc_pz) / (bc_pe-bc_pz) )");

    bcvegpy_tree->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    bcvegpy_tree->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    bcvegpy_tree->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    bcvegpy_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    bcvegpy_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    bcvegpy_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    bcvegpy_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    bcvegpy_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    bcvegpy_tree->SetAlias("delta_eta_bx_dx","bx_eta-dx_eta");
    bcvegpy_tree->SetAlias("delta_eta_bc_dx","bc_eta-dx_eta");
    bcvegpy_tree->SetAlias("delta_eta_bc_bx","bc_eta-bx_eta");

    bcvegpy_tree->SetAlias("delta_y_bx_dx","bx_y-dx_y");
    bcvegpy_tree->SetAlias("delta_y_bc_dx","bc_y-dx_y");
    bcvegpy_tree->SetAlias("delta_y_bc_bx","bc_y-bx_y");



    bcvegpy_tree->SetAlias("delta_R_bx_dx","sqrt(delta_eta_bx_dx*delta_eta_bx_dx+angle_bx_dx*angle_bx_dx)");
    bcvegpy_tree->SetAlias("delta_R_bc_dx","sqrt(delta_eta_bc_dx*delta_eta_bc_dx+angle_bc_dx*angle_bc_dx)");
    bcvegpy_tree->SetAlias("delta_R_bc_bx","sqrt(delta_eta_bc_bx*delta_eta_bc_bx+angle_bc_bx*angle_bc_bx)");
    
    int n_entries_bcvegpy = bcvegpy_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    pythia_tree_nompi->GetEntry(pythia_tree_nompi->GetEntries()-1);
    bcvegpy_tree->GetEntry(bcvegpy_tree->GetEntries()-1);

    std::cout << "Cross section BcVegPy:       " << sigmaGen_bcvegpy*microbarn << " +/- " << sigmaErr_bcvegpy*microbarn << " microbarns"; 
    std::cout << "\t" << bcvegpy_tree->GetEntries("nBc>0") << "/" << bcvegpy_tree->GetEntries("")<<std::endl;
    
    std::cout << "Cross section Pythia:        " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nBc>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
    
    std::cout << "Cross section Pythia NoMPI:  " << sigmaGen_pythia_nompi*microbarn  << " +/- " << sigmaErr_pythia_nompi*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree_nompi->GetEntries("nBc>0")<< "/" << pythia_tree_nompi->GetEntries("")<< std::endl;
    
    // std::cout << "Cross section BcVegPy:  " << BcVegPy_crosssec_val  << " +/- " << BcVegPy_crosssec_err  << std::endl;


    double scalefactor_bcvegpy = 1e-3*sigmaGen_bcvegpy*microbarn/n_entries_bcvegpy;
    double scalefactor_pythia  =      sigmaGen_pythia*microbarn/n_entries_pythia;
    double scalefactor_pythia_nompi  =sigmaGen_pythia_nompi*microbarn/n_entries_pythia_nompi;
    
    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    TCut cuts_bcvegpy = "nbquark+nbquarkbar==1&&ncquark+ncquarkbar==1&&bx_pt!=0&&dx_pt!=0&&bc_pt!=0";
    TCut cuts_pythia  = "nbquark+nbquarkbar==2&&ncquark+ncquarkbar==2&&bx_pt!=0&&dx_pt!=0&&bc_pt!=0";
    
    // TCut cuts_all = "nBc==1&& (bc_y)>2 && (bc_y)<5 && (bx_y)>2 && (bx_y)<5 && (dx_y)>2 && (dx_y)<5";
    // TCut cuts_all = "nBc==1&& (bc_eta)>2 && (bc_eta)<5 && (bx_eta)>2 && (bx_eta)<5 && (dx_eta)>2 && (dx_eta)<5";
    // TCut cuts_all = "bc_eta>2 && bc_eta<5";
    TCut cuts_all = "nBc==1";
    

    // -------------------------------------------------------------------------
    // Plot 1D kinematics integrated over all eta 
    // -------------------------------------------------------------------------
    // TCut temp_cut = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5) && foundHardc&&hascStatus23&&(hascStatus23_type!=4)";
    // pT
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia  = new TH1D("h_pt_pythia", "h_pt_pythia", 50,0,20);
    TH1D* h_pt_pythia_nompi  = new TH1D("h_pt_pythia_nompi", "h_pt_pythia_nompi", 50,0,20);
    TH1D* h_pt_bcvegpy = new TH1D("h_pt_bcvegpy","h_pt_bcvegpy",50,0,20);

    pythia_tree_nompi->Draw("bc_pt>>h_pt_pythia_nompi",cuts_all);
    pythia_tree->Draw("bc_pt>>h_pt_pythia",cuts_all);
    bcvegpy_tree->Draw("bc_pt>>h_pt_bcvegpy",cuts_all);

    h_pt_bcvegpy->Scale(scalefactor_bcvegpy);
    h_pt_pythia->Scale(scalefactor_pythia);
    h_pt_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_pt_pythia->SetLineColor(kRed);
    h_pt_pythia_nompi->SetLineColor(kBlue);
    h_pt_bcvegpy->SetLineColor(kGreen+2);

    h_pt_pythia->SetMaximum(1.05*max({h_pt_pythia->GetMaximum(),
                                      h_pt_bcvegpy->GetMaximum(),
                                      h_pt_pythia_nompi->GetMaximum()}));
    
    double h_pt_pythia_bin_width = h_pt_pythia->GetXaxis()->GetBinWidth(2);
    h_pt_pythia->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_pythia_bin_width));
    h_pt_pythia->Draw("hist");
    h_pt_pythia_nompi->Draw("hist same");
    h_pt_bcvegpy->Draw("same hist");

    TLegend* leg_pt = new TLegend(0.6,0.6,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,"Pythia With MPI","l");
    leg_pt->AddEntry(h_pt_pythia_nompi,"Pythia No MPI","l");
    leg_pt->AddEntry(h_pt_bcvegpy,"BcVegPy 2.2","l");
    leg_pt->SetTextFont(132);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics/c_pt.pdf");
    

    // multiplicity
    TCanvas* c_multiplicity = new TCanvas("c_multiplicity","c_multiplicity");
    TH1D* h_multiplicity_pythia  = new TH1D("h_multiplicity_pythia", "h_multiplicity_pythia", 50,0,1000);
    TH1D* h_multiplicity_pythia_nompi  = new TH1D("h_multiplicity_pythia_nompi", "h_multiplicity_pythia_nompi", 50,0,1000);
    TH1D* h_multiplicity_bcvegpy = new TH1D("h_multiplicity_bcvegpy","h_multiplicity_bcvegpy",50,0,1000);

    pythia_tree_nompi->Draw("nMultiplicity>>h_multiplicity_pythia_nompi",cuts_all);
    pythia_tree->Draw("nMultiplicity>>h_multiplicity_pythia",cuts_all);
    bcvegpy_tree->Draw("nMultiplicity>>h_multiplicity_bcvegpy",cuts_all);

    h_multiplicity_bcvegpy->Scale(scalefactor_bcvegpy);
    h_multiplicity_pythia->Scale(scalefactor_pythia);
    h_multiplicity_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_multiplicity_pythia->SetLineColor(kRed);
    h_multiplicity_pythia_nompi->SetLineColor(kBlue);
    h_multiplicity_bcvegpy->SetLineColor(kGreen+2);

    h_multiplicity_pythia->SetMaximum(1.05*max({h_multiplicity_pythia->GetMaximum(),
                                      h_multiplicity_bcvegpy->GetMaximum(),
                                      h_multiplicity_pythia_nompi->GetMaximum()}));
    
    double h_multiplicity_pythia_bin_width = h_multiplicity_pythia->GetXaxis()->GetBinWidth(2);
    h_multiplicity_pythia->SetTitle(Form(";N_{Particles}; d#sigma/dN [%.1f #mub]",h_multiplicity_pythia_bin_width));
    h_multiplicity_pythia->Draw("hist");
    h_multiplicity_pythia_nompi->Draw("hist same");
    h_multiplicity_bcvegpy->Draw("same hist");

    TLegend* leg_multiplicity = new TLegend(0.6,0.6,0.9,0.9);
    leg_multiplicity->AddEntry(h_multiplicity_pythia,"Pythia With MPI","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_nompi,"Pythia No MPI","l");
    leg_multiplicity->AddEntry(h_multiplicity_bcvegpy,"BcVegPy 2.2","l");
    leg_multiplicity->SetTextFont(132);
    leg_multiplicity->SetFillStyle(0);
    leg_multiplicity->Draw();
    c_multiplicity->Print("plots_kinematics/c_multiplicity.pdf");


    TCanvas* c_pt_ratio = new TCanvas("c_pt_ratio","c_pt_ratio");
    TH1D* h_pt_ratio  = new TH1D("h_pt_ratio", "h_pt_ratio", 50,0,20);
    h_pt_pythia->Sumw2();
    h_pt_bcvegpy->Sumw2();
    h_pt_ratio->Divide(h_pt_pythia,h_pt_bcvegpy);
    h_pt_ratio->SetTitle(";#it{p}_{T} [GeV/#it{c}]; Ratio Pythia/BcVegPy");
    // h_pt_ratio->SetMaximum(8.0);
    h_pt_ratio->SetMinimum(0.0);
    h_pt_ratio->Draw();

    c_pt_ratio->Print("plots_kinematics/c_pt_ratio.pdf");

    // eta
    TCanvas* c_eta = new TCanvas("c_eta","c_eta");
    TH1D* h_eta_pythia  = new TH1D("h_eta_pythia", "h_eta_pythia", 50,-10,10);
    TH1D* h_eta_pythia_nompi  = new TH1D("h_eta_pythia_nompi", "h_eta_pythia_nompi", 50,-10,10);
    TH1D* h_eta_bcvegpy = new TH1D("h_eta_bcvegpy","h_eta_bcvegpy",50,-10,10);

    pythia_tree->Draw("bc_eta>>h_eta_pythia",cuts_all);
    pythia_tree_nompi->Draw("bc_eta>>h_eta_pythia_nompi",cuts_all);
    bcvegpy_tree->Draw("bc_eta>>h_eta_bcvegpy",cuts_all);

    h_eta_bcvegpy->Scale(scalefactor_bcvegpy);
    h_eta_pythia->Scale(scalefactor_pythia);
    h_eta_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_eta_pythia->SetLineColor(kRed);
    h_eta_pythia_nompi->SetLineColor(kBlue);
    h_eta_bcvegpy->SetLineColor(kGreen+2);
    h_eta_pythia->SetMaximum(1.05*max({h_eta_pythia->GetMaximum(),
                                       h_eta_bcvegpy->GetMaximum(),
                                       h_eta_pythia_nompi->GetMaximum()}));

    double h_eta_pythia_bin_width = h_eta_pythia->GetXaxis()->GetBinWidth(2);
    h_eta_pythia->SetTitle(Form(";#eta; d#sigma/d#eta [%.1f #mub]",h_eta_pythia_bin_width));  
    h_eta_pythia->Draw("hist");
    h_eta_pythia_nompi->Draw("same hist");
    h_eta_bcvegpy->Draw("same hist");


    TLegend* leg_eta = new TLegend(0.7,0.7,0.9,0.9);
    leg_eta->AddEntry(h_eta_pythia,"Pythia With MPI","l");
    leg_eta->AddEntry(h_eta_pythia_nompi,"Pythia No MPI","l");
    leg_eta->AddEntry(h_eta_bcvegpy,"BcVegPy 2.2","l");
    leg_eta->SetTextFont(132);
    leg_eta->Draw();
    c_eta->Print("plots_kinematics/c_eta.pdf");

    // Rapidity
    TCanvas* c_y = new TCanvas("c_y","c_y");
    TH1D* h_y_pythia  = new TH1D("h_y_pythia", "h_y_pythia", 50,-10,10);
    TH1D* h_y_pythia_nompi  = new TH1D("h_y_pythia_nompi", "h_y_pythia_nompi", 50,-10,10);
    TH1D* h_y_bcvegpy = new TH1D("h_y_bcvegpy","h_y_bcvegpy",50,-10,10);

    pythia_tree->Draw("bc_y>>h_y_pythia");
    pythia_tree_nompi->Draw("bc_y>>h_y_pythia_nompi");
    bcvegpy_tree->Draw("bc_y>>h_y_bcvegpy");

    h_y_bcvegpy->Scale(scalefactor_bcvegpy);
    h_y_pythia->Scale(scalefactor_pythia);
    h_y_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_y_pythia->SetLineColor(kRed);
    h_y_pythia_nompi->SetLineColor(kBlue);
    h_y_bcvegpy->SetLineColor(kGreen+2);

    h_y_pythia->SetMaximum(1.05*max({h_y_pythia->GetMaximum(),
                                     h_y_bcvegpy->GetMaximum(),
                                     h_y_pythia_nompi->GetMaximum()
                                      }));

    double h_y_pythia_bin_width = h_y_pythia->GetXaxis()->GetBinWidth(2);
    h_y_pythia->SetTitle(Form(";#it{y}; d#sigma/d#it{y} [%.1f #mub]",h_y_pythia_bin_width));
    h_y_pythia->Draw("hist");
    h_y_pythia_nompi->Draw("same hist");
    h_y_bcvegpy->Draw("same hist");


    TLegend* leg_y = new TLegend(0.65,0.6,0.95,0.9);
    leg_y->AddEntry(h_y_pythia,"Pythia With MPI","l");
    leg_y->AddEntry(h_y_pythia_nompi,"Pythia No MPI","l");
    leg_y->AddEntry(h_y_bcvegpy,"BcVegPy 2.2","l");
    leg_y->SetTextFont(132);
    leg_y->SetFillStyle(0);
    leg_y->Draw();

    c_y->Print("plots_kinematics/c_y.pdf");


    // -------------------------------------------------------------------------
    // Plot 2D pT vs. eta 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_eta_pythia = new TCanvas("c_2D_pt_eta_pythia","c_2D_pt_eta_pythia");
    TH2D* h_2D_pt_eta_pythia         = new TH2D("h_2D_pt_eta_pythia",  "h_2D_pt_eta_pythia",  50,-10,10,50,0,20);
    TH2D* h_2D_pt_eta_pythia_nompi   = new TH2D("h_2D_pt_eta_pythia_nompi",  "h_2D_pt_eta_pythia_nompi",  50,-10,10,50,0,20);
    TH2D* h_2D_pt_eta_bcvegpy        = new TH2D("h_2D_pt_eta_bcvegpy", "h_2D_pt_eta_bcvegpy", 50,-10,10,50,0,20);

    pythia_tree->Draw("bc_pt:bc_eta>>h_2D_pt_eta_pythia",cuts_all+ "nBc==1");
    pythia_tree_nompi->Draw("bc_pt:bc_eta>>h_2D_pt_eta_pythia_nompi",cuts_all+ "nBc==1");
    bcvegpy_tree->Draw("bc_pt:bc_eta>>h_2D_pt_eta_bcvegpy",cuts_all+ "nBc==1");

    h_2D_pt_eta_bcvegpy->Scale(scalefactor_bcvegpy);
    h_2D_pt_eta_pythia->Scale(scalefactor_pythia);
    h_2D_pt_eta_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    double maximum_2D_pt_eta = max({
                                        h_2D_pt_eta_bcvegpy->GetMaximum(),
                                        h_2D_pt_eta_pythia->GetMaximum(),
                                        h_2D_pt_eta_pythia_nompi->GetMaximum()
                                        });

    h_2D_pt_eta_pythia->SetMaximum(maximum_2D_pt_eta);
    h_2D_pt_eta_pythia_nompi->SetMaximum(maximum_2D_pt_eta);
    h_2D_pt_eta_bcvegpy->SetMaximum(maximum_2D_pt_eta);

    h_2D_pt_eta_pythia->SetTitle("; #eta;#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_eta_pythia->Draw("colz");
    gPad->SetLogz();
    
    c_2D_pt_eta_pythia->Print("plots_kinematics/c_2D_pt_eta_pythia.pdf");
    

    TCanvas* c_2D_pt_eta_pythia_nompi = new TCanvas("c_2D_pt_eta_pythia_nompi","c_2D_pt_eta_pythia_nompi");

    h_2D_pt_eta_pythia_nompi->SetTitle("; #eta;#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_eta_pythia_nompi->Draw("colz");
    gPad->SetLogz();
    
    c_2D_pt_eta_pythia_nompi->Print("plots_kinematics/c_2D_pt_eta_pythia_nompi.pdf");
    

    TCanvas* c_2D_pt_eta_bcvegpy = new TCanvas("c_2D_pt_eta_bcvegpy","c_2D_pt_eta_bcvegpy");

    h_2D_pt_eta_bcvegpy->SetTitle("; #eta;#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_eta_bcvegpy->Draw("colz");
    gPad->SetLogz();
    
    c_2D_pt_eta_bcvegpy->Print("plots_kinematics/c_2D_pt_eta_bcvegpy.pdf");


    // -------------------------------------------------------------------------
    // Plot 2D pT vs. y 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_y_pythia = new TCanvas("c_2D_pt_y_pythia","c_2D_pt_y_pythia");
    TH2D* h_2D_pt_y_pythia   = new TH2D("h_2D_pt_y_pythia",  "h_2D_pt_y_pythia",  50,-10,10,50,0,20);
    TH2D* h_2D_pt_y_pythia_nompi   = new TH2D("h_2D_pt_y_pythia_nompi",  "h_2D_pt_y_pythia_nompi",  50,-10,10,50,0,20);
    TH2D* h_2D_pt_y_bcvegpy  = new TH2D("h_2D_pt_y_bcvegpy", "h_2D_pt_y_bcvegpy", 50,-10,10,50,0,20);

    pythia_tree->Draw("bc_pt:bc_y>>h_2D_pt_y_pythia",cuts_all+ "nBc==1");
    pythia_tree_nompi->Draw("bc_pt:bc_y>>h_2D_pt_y_pythia_nompi",cuts_all+ "nBc==1");
    bcvegpy_tree->Draw("bc_pt:bc_y>>h_2D_pt_y_bcvegpy",cuts_all+ "nBc==1");

    h_2D_pt_y_bcvegpy->Scale(scalefactor_bcvegpy);
    h_2D_pt_y_pythia->Scale(scalefactor_pythia);
    h_2D_pt_y_pythia_nompi->Scale(scalefactor_pythia_nompi);

    double maximum_2D_pt_y = max( {
                                    h_2D_pt_y_bcvegpy->GetMaximum(),
                                    h_2D_pt_y_pythia->GetMaximum(),
                                    h_2D_pt_y_pythia_nompi->GetMaximum(),
                                });

    h_2D_pt_y_pythia->SetMaximum(maximum_2D_pt_y);
    h_2D_pt_y_pythia_nompi->SetMaximum(maximum_2D_pt_y);
    h_2D_pt_y_bcvegpy->SetMaximum(maximum_2D_pt_y);

    h_2D_pt_y_pythia->SetTitle(";#it{y};#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_pythia->Draw("colz");
    gPad->SetLogz();
    
    c_2D_pt_y_pythia->Print("plots_kinematics/c_2D_pt_y_pythia.pdf");
    

    TCanvas* c_2D_pt_y_pythia_nompi = new TCanvas("c_2D_pt_y_pythia_nompi","c_2D_pt_y_pythia_nompi");

    h_2D_pt_y_pythia_nompi->SetTitle(";#it{y};#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_pythia_nompi->Draw("colz");
    gPad->SetLogz();
    
    c_2D_pt_y_pythia_nompi->Print("plots_kinematics/c_2D_pt_y_pythia_nompi.pdf");
    

    TCanvas* c_2D_pt_y_bcvegpy = new TCanvas("c_2D_pt_y_bcvegpy","c_2D_pt_y_bcvegpy");

    h_2D_pt_y_bcvegpy->SetTitle(";#it{y};#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_bcvegpy->Draw("colz");
    gPad->SetLogz();
    
    c_2D_pt_y_bcvegpy->Print("plots_kinematics/c_2D_pt_y_bcvegpy.pdf");
    // -------------------------------------------------------------------------
    // Plot 1D angles integrated over all eta 
    // -------------------------------------------------------------------------
    SetLHCbStyle("oneD");

    // angles
    TCanvas* c_delta_phi_bc_bx = new TCanvas("c_delta_phi_bc_bx","c_delta_phi_bc_bx");
    TH1D* h_delta_phi_bc_bx_pythia  = new TH1D("h_delta_phi_bc_bx_pythia", "h_delta_phi_bc_bx_pythia", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_bx_pythia_nompi  = new TH1D("h_delta_phi_bc_bx_pythia_nompi", "h_delta_phi_bc_bx_pythia_nompi", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_bx_bcvegpy = new TH1D("h_delta_phi_bc_bx_bcvegpy","h_delta_phi_bc_bx_bcvegpy",50,0,TMath::Pi());

    pythia_tree->Draw("angle_bc_bx>>h_delta_phi_bc_bx_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("angle_bc_bx>>h_delta_phi_bc_bx_pythia_nompi",  cuts_pythia +cuts_all);
    bcvegpy_tree->Draw("angle_bc_bx>>h_delta_phi_bc_bx_bcvegpy",cuts_bcvegpy+cuts_all);

    h_delta_phi_bc_bx_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_phi_bc_bx_pythia->Scale(scalefactor_pythia);
    h_delta_phi_bc_bx_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_delta_phi_bc_bx_bcvegpy->Scale(1.0/h_delta_phi_bc_bx_bcvegpy->Integral());
    h_delta_phi_bc_bx_pythia->Scale(1.0/h_delta_phi_bc_bx_pythia->Integral());
    h_delta_phi_bc_bx_pythia_nompi->Scale(1.0/h_delta_phi_bc_bx_pythia_nompi->Integral());

    h_delta_phi_bc_bx_pythia->SetLineColor(kRed);
    h_delta_phi_bc_bx_pythia_nompi->SetLineColor(kBlue);
    h_delta_phi_bc_bx_bcvegpy->SetLineColor(kGreen+2);

    h_delta_phi_bc_bx_bcvegpy->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; Normalised");
    h_delta_phi_bc_bx_pythia->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; Normalised");
    h_delta_phi_bc_bx_pythia_nompi->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; Normalised");
    h_delta_phi_bc_bx_bcvegpy->SetMinimum(0.0);
    h_delta_phi_bc_bx_pythia->SetMinimum(0.0);
    h_delta_phi_bc_bx_pythia_nompi->SetMinimum(0.0);
    

    h_delta_phi_bc_bx_pythia->SetMaximum(1.05*max({h_delta_phi_bc_bx_pythia->GetMaximum(),
                                                   h_delta_phi_bc_bx_pythia_nompi->GetMaximum(),
                                                   h_delta_phi_bc_bx_bcvegpy->GetMaximum()}));

    h_delta_phi_bc_bx_pythia->Draw("hist");
    h_delta_phi_bc_bx_pythia_nompi->Draw("hist same");
    h_delta_phi_bc_bx_bcvegpy->Draw("hist same");
    


    TLegend* leg_delta_phi_bc_bx = new TLegend(0.2,0.6,0.5,0.9);
    leg_delta_phi_bc_bx->AddEntry(h_delta_phi_bc_bx_pythia,"Pythia With MPI","l");
    leg_delta_phi_bc_bx->AddEntry(h_delta_phi_bc_bx_pythia_nompi,"Pythia No MPI","l");
    leg_delta_phi_bc_bx->AddEntry(h_delta_phi_bc_bx_bcvegpy,"BcVegPy 2.2","l");
    leg_delta_phi_bc_bx->SetTextFont(132);
    leg_delta_phi_bc_bx->Draw();

    c_delta_phi_bc_bx->Print("plots_kinematics/c_delta_phi_bc_bx.pdf");


    // angles
    TCanvas* c_delta_phi_bc_dx = new TCanvas("c_delta_phi_bc_dx","c_delta_phi_bc_dx");
    TH1D* h_delta_phi_bc_dx_pythia  = new TH1D("h_delta_phi_bc_dx_pythia", "h_delta_phi_bc_dx_pythia", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_dx_pythia_nompi  = new TH1D("h_delta_phi_bc_dx_pythia_nompi", "h_delta_phi_bc_dx_pythia_nompi", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_dx_bcvegpy = new TH1D("h_delta_phi_bc_dx_bcvegpy","h_delta_phi_bc_dx_bcvegpy",50,0,TMath::Pi());

    pythia_tree->Draw("angle_bc_dx>>h_delta_phi_bc_dx_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("angle_bc_dx>>h_delta_phi_bc_dx_pythia_nompi",  cuts_pythia +cuts_all);
    bcvegpy_tree->Draw("angle_bc_dx>>h_delta_phi_bc_dx_bcvegpy",cuts_bcvegpy+cuts_all);

    h_delta_phi_bc_dx_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_phi_bc_dx_pythia->Scale(scalefactor_pythia);
    h_delta_phi_bc_dx_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_delta_phi_bc_dx_bcvegpy->Scale(1.0/h_delta_phi_bc_dx_bcvegpy->Integral());
    h_delta_phi_bc_dx_pythia->Scale(1.0/h_delta_phi_bc_dx_pythia->Integral());
    h_delta_phi_bc_dx_pythia_nompi->Scale(1.0/h_delta_phi_bc_dx_pythia_nompi->Integral());

    h_delta_phi_bc_dx_pythia->SetLineColor(kRed);
    h_delta_phi_bc_dx_pythia_nompi->SetLineColor(kBlue);
    h_delta_phi_bc_dx_bcvegpy->SetLineColor(kGreen+2);

    h_delta_phi_bc_dx_pythia->SetTitle(";#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bc_dx_pythia->SetMinimum(0.0);

    h_delta_phi_bc_dx_pythia->SetMaximum(1.05*max({h_delta_phi_bc_dx_pythia->GetMaximum(),
                                                   h_delta_phi_bc_dx_pythia_nompi->GetMaximum(),
                                                   h_delta_phi_bc_dx_bcvegpy->GetMaximum()}));

    h_delta_phi_bc_dx_pythia->Draw("hist");
    h_delta_phi_bc_dx_pythia_nompi->Draw("hist same");
    h_delta_phi_bc_dx_bcvegpy->Draw("hist same");


    TLegend* leg_delta_phi_bc_dx = new TLegend(0.6,0.2,0.9,0.5);
    leg_delta_phi_bc_dx->AddEntry(h_delta_phi_bc_dx_pythia,"Pythia With MPI","l");
    leg_delta_phi_bc_dx->AddEntry(h_delta_phi_bc_dx_pythia_nompi,"Pythia No MPI","l");
    leg_delta_phi_bc_dx->AddEntry(h_delta_phi_bc_dx_bcvegpy,"BcVegPy 2.2","l");
    leg_delta_phi_bc_dx->SetTextFont(132);
    leg_delta_phi_bc_dx->Draw();
    c_delta_phi_bc_dx->Print("plots_kinematics/c_delta_phi_bc_dx.pdf");

    //////

    // angles
    TCanvas* c_delta_phi_bx_dx = new TCanvas("c_delta_phi_bx_dx","c_delta_phi_bx_dx");
    TH1D* h_delta_phi_bx_dx_pythia  = new TH1D("h_delta_phi_bx_dx_pythia", "h_delta_phi_bx_dx_pythia", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bx_dx_pythia_nompi  = new TH1D("h_delta_phi_bx_dx_pythia_nompi", "h_delta_phi_bx_dx_pythia_nompi", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bx_dx_bcvegpy = new TH1D("h_delta_phi_bx_dx_bcvegpy","h_delta_phi_bx_dx_bcvegpy",50,0,TMath::Pi());

    pythia_tree->Draw("angle_bx_dx>>h_delta_phi_bx_dx_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("angle_bx_dx>>h_delta_phi_bx_dx_pythia_nompi",  cuts_pythia +cuts_all);
    bcvegpy_tree->Draw("angle_bx_dx>>h_delta_phi_bx_dx_bcvegpy",cuts_bcvegpy+cuts_all);

    h_delta_phi_bx_dx_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_phi_bx_dx_pythia->Scale(scalefactor_pythia);
    h_delta_phi_bx_dx_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_delta_phi_bx_dx_bcvegpy->Scale(1.0/h_delta_phi_bx_dx_bcvegpy->Integral());
    h_delta_phi_bx_dx_pythia->Scale(1.0/h_delta_phi_bx_dx_pythia->Integral());
    h_delta_phi_bx_dx_pythia_nompi->Scale(1.0/h_delta_phi_bx_dx_pythia_nompi->Integral());

    h_delta_phi_bx_dx_pythia->SetLineColor(kRed);
    h_delta_phi_bx_dx_pythia_nompi->SetLineColor(kBlue);
    h_delta_phi_bx_dx_bcvegpy->SetLineColor(kGreen+2);

    h_delta_phi_bx_dx_pythia->SetTitle(";#Delta#phi(X_{b}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bx_dx_pythia->SetMinimum(0.0);

    h_delta_phi_bx_dx_pythia->SetMaximum(1.05*max({h_delta_phi_bx_dx_pythia->GetMaximum(),
                                                   h_delta_phi_bx_dx_pythia_nompi->GetMaximum(),
                                                   h_delta_phi_bx_dx_bcvegpy->GetMaximum()}));

    h_delta_phi_bx_dx_pythia->Draw("hist");
    h_delta_phi_bx_dx_pythia_nompi->Draw("hist same");
    h_delta_phi_bx_dx_bcvegpy->Draw("hist same");


    TLegend* leg_delta_phi_bx_dx = new TLegend(0.6,0.2,0.9,0.5);
    leg_delta_phi_bx_dx->AddEntry(h_delta_phi_bx_dx_pythia,"Pythia With MPI","l");
    leg_delta_phi_bx_dx->AddEntry(h_delta_phi_bx_dx_pythia_nompi,"Pythia No MPI","l");
    leg_delta_phi_bx_dx->AddEntry(h_delta_phi_bx_dx_bcvegpy,"BcVegPy 2.2","l");
    leg_delta_phi_bx_dx->SetTextFont(132);
    leg_delta_phi_bx_dx->Draw();
    c_delta_phi_bx_dx->Print("plots_kinematics/c_delta_phi_bx_dx.pdf");

    // Delta y
    TCanvas* c_delta_y_bx_dx = new TCanvas("c_delta_y_bx_dx","c_delta_y_bx_dx");
    TH1D* h_delta_y_bx_dx_pythia  = new TH1D("h_delta_y_bx_dx_pythia", "h_delta_y_bx_dx_pythia", 50,0.0,5);
    TH1D* h_delta_y_bx_dx_pythia_nompi  = new TH1D("h_delta_y_bx_dx_pythia_nompi", "h_delta_y_bx_dx_pythia_nompi", 50,0.0,5);
    TH1D* h_delta_y_bx_dx_bcvegpy = new TH1D("h_delta_y_bx_dx_bcvegpy","h_delta_y_bx_dx_bcvegpy",50,0.0,5);

    pythia_tree->Draw("abs(delta_y_bx_dx)>>h_delta_y_bx_dx_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("abs(delta_y_bx_dx)>>h_delta_y_bx_dx_pythia_nompi",  cuts_pythia +cuts_all);
    bcvegpy_tree->Draw("abs(delta_y_bx_dx)>>h_delta_y_bx_dx_bcvegpy",cuts_bcvegpy+cuts_all);

    h_delta_y_bx_dx_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_y_bx_dx_pythia->Scale(scalefactor_pythia);
    h_delta_y_bx_dx_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_delta_y_bx_dx_bcvegpy->Scale(1.0/h_delta_y_bx_dx_bcvegpy->Integral());
    h_delta_y_bx_dx_pythia->Scale(1.0/h_delta_y_bx_dx_pythia->Integral());
    h_delta_y_bx_dx_pythia_nompi->Scale(1.0/h_delta_y_bx_dx_pythia_nompi->Integral());

    h_delta_y_bx_dx_pythia->SetLineColor(kRed);
    h_delta_y_bx_dx_pythia_nompi->SetLineColor(kBlue);
    h_delta_y_bx_dx_bcvegpy->SetLineColor(kGreen+2);

    h_delta_y_bx_dx_pythia->SetTitle(";#Delta#it{y}(X_{b}, X_{#bar{c}}); Normalised");
    h_delta_y_bx_dx_pythia->SetMinimum(0.0);

    h_delta_y_bx_dx_pythia->SetMaximum(1.05*max({h_delta_y_bx_dx_pythia->GetMaximum(),
                                                 h_delta_y_bx_dx_pythia_nompi->GetMaximum(),
                                                 h_delta_y_bx_dx_bcvegpy->GetMaximum()}));

    h_delta_y_bx_dx_pythia->Draw("hist");
    h_delta_y_bx_dx_pythia_nompi->Draw("hist same");
    h_delta_y_bx_dx_bcvegpy->Draw("hist same");


    TLegend* leg_delta_y_bx_dx = new TLegend(0.6,0.6,0.9,0.9);
    leg_delta_y_bx_dx->AddEntry(h_delta_y_bx_dx_pythia,"Pythia With MPI","l");
    leg_delta_y_bx_dx->AddEntry(h_delta_y_bx_dx_pythia_nompi,"Pythia No MPI","l");
    leg_delta_y_bx_dx->AddEntry(h_delta_y_bx_dx_bcvegpy,"BcVegPy 2.2","l");
    leg_delta_y_bx_dx->SetTextFont(132);
    leg_delta_y_bx_dx->Draw();
    c_delta_y_bx_dx->Print("plots_kinematics/c_delta_y_bx_dx.pdf");



    // Delta y
    TCanvas* c_delta_y_bc_dx = new TCanvas("c_delta_y_bc_dx","c_delta_y_bc_dx");
    TH1D* h_delta_y_bc_dx_pythia  = new TH1D("h_delta_y_bc_dx_pythia", "h_delta_y_bc_dx_pythia", 50,0.0,5);
    TH1D* h_delta_y_bc_dx_pythia_nompi  = new TH1D("h_delta_y_bc_dx_pythia_nompi", "h_delta_y_bc_dx_pythia_nompi", 50,0.0,5);
    TH1D* h_delta_y_bc_dx_bcvegpy = new TH1D("h_delta_y_bc_dx_bcvegpy","h_delta_y_bc_dx_bcvegpy",50,0.0,5);

    pythia_tree->Draw("abs(delta_y_bc_dx)>>h_delta_y_bc_dx_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("abs(delta_y_bc_dx)>>h_delta_y_bc_dx_pythia_nompi",  cuts_pythia +cuts_all);
    bcvegpy_tree->Draw("abs(delta_y_bc_dx)>>h_delta_y_bc_dx_bcvegpy",cuts_bcvegpy+cuts_all);

    h_delta_y_bc_dx_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_y_bc_dx_pythia->Scale(scalefactor_pythia);
    h_delta_y_bc_dx_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_delta_y_bc_dx_bcvegpy->Scale(1.0/h_delta_y_bc_dx_bcvegpy->Integral());
    h_delta_y_bc_dx_pythia->Scale(1.0/h_delta_y_bc_dx_pythia->Integral());
    h_delta_y_bc_dx_pythia_nompi->Scale(1.0/h_delta_y_bc_dx_pythia_nompi->Integral());

    h_delta_y_bc_dx_pythia->SetLineColor(kRed);
    h_delta_y_bc_dx_pythia_nompi->SetLineColor(kBlue);
    h_delta_y_bc_dx_bcvegpy->SetLineColor(kGreen+2);

    h_delta_y_bc_dx_pythia->SetTitle(";#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}); Normalised");
    h_delta_y_bc_dx_pythia->SetMinimum(0.0);

    h_delta_y_bc_dx_pythia->SetMaximum(1.05*max({h_delta_y_bc_dx_pythia->GetMaximum(),
                                                 h_delta_y_bc_dx_pythia_nompi->GetMaximum(),
                                                 h_delta_y_bc_dx_bcvegpy->GetMaximum()}));

    h_delta_y_bc_dx_pythia->Draw("hist");
    h_delta_y_bc_dx_pythia_nompi->Draw("hist same");
    h_delta_y_bc_dx_bcvegpy->Draw("hist same");


    TLegend* leg_delta_y_bc_dx = new TLegend(0.6,0.6,0.9,0.9);
    leg_delta_y_bc_dx->AddEntry(h_delta_y_bc_dx_pythia,"Pythia With MPI","l");
    leg_delta_y_bc_dx->AddEntry(h_delta_y_bc_dx_pythia_nompi,"Pythia No MPI","l");
    leg_delta_y_bc_dx->AddEntry(h_delta_y_bc_dx_bcvegpy,"BcVegPy 2.2","l");
    leg_delta_y_bc_dx->SetTextFont(132);
    leg_delta_y_bc_dx->Draw();
    c_delta_y_bc_dx->Print("plots_kinematics/c_delta_y_bc_dx.pdf");



    // Delta y
    TCanvas* c_delta_y_bc_bx = new TCanvas("c_delta_y_bc_bx","c_delta_y_bc_bx");
    TH1D* h_delta_y_bc_bx_pythia  = new TH1D("h_delta_y_bc_bx_pythia", "h_delta_y_bc_bx_pythia", 50,0.0,5);
    TH1D* h_delta_y_bc_bx_pythia_nompi  = new TH1D("h_delta_y_bc_bx_pythia_nompi", "h_delta_y_bc_bx_pythia_nompi", 50,0.0,5);
    TH1D* h_delta_y_bc_bx_bcvegpy = new TH1D("h_delta_y_bc_bx_bcvegpy","h_delta_y_bc_bx_bcvegpy",50,0.0,5);

    pythia_tree->Draw("abs(delta_y_bc_bx)>>h_delta_y_bc_bx_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("abs(delta_y_bc_bx)>>h_delta_y_bc_bx_pythia_nompi",  cuts_pythia +cuts_all);
    bcvegpy_tree->Draw("abs(delta_y_bc_bx)>>h_delta_y_bc_bx_bcvegpy",cuts_bcvegpy+cuts_all);

    h_delta_y_bc_bx_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_y_bc_bx_pythia->Scale(scalefactor_pythia);
    h_delta_y_bc_bx_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_delta_y_bc_bx_bcvegpy->Scale(1.0/h_delta_y_bc_bx_bcvegpy->Integral());
    h_delta_y_bc_bx_pythia->Scale(1.0/h_delta_y_bc_bx_pythia->Integral());
    h_delta_y_bc_bx_pythia_nompi->Scale(1.0/h_delta_y_bc_bx_pythia_nompi->Integral());

    h_delta_y_bc_bx_pythia->SetLineColor(kRed);
    h_delta_y_bc_bx_pythia_nompi->SetLineColor(kBlue);
    h_delta_y_bc_bx_bcvegpy->SetLineColor(kGreen+2);

    h_delta_y_bc_bx_pythia->SetTitle(";#Delta#it{y}(B_{c}^{+}, X_{b}); Normalised");
    h_delta_y_bc_bx_pythia->SetMinimum(0.0);

    h_delta_y_bc_bx_pythia->SetMaximum(1.05*max({h_delta_y_bc_bx_pythia->GetMaximum(),
                                                 h_delta_y_bc_bx_pythia_nompi->GetMaximum(),
                                                 h_delta_y_bc_bx_bcvegpy->GetMaximum()}));

    h_delta_y_bc_bx_pythia->Draw("hist");
    h_delta_y_bc_bx_pythia_nompi->Draw("hist same");
    h_delta_y_bc_bx_bcvegpy->Draw("hist same");


    TLegend* leg_delta_y_bc_bx = new TLegend(0.6,0.6,0.9,0.9);
    leg_delta_y_bc_bx->AddEntry(h_delta_y_bc_bx_pythia,"Pythia With MPI","l");
    leg_delta_y_bc_bx->AddEntry(h_delta_y_bc_bx_pythia_nompi,"Pythia No MPI","l");
    leg_delta_y_bc_bx->AddEntry(h_delta_y_bc_bx_bcvegpy,"BcVegPy 2.2","l");
    leg_delta_y_bc_bx->SetTextFont(132);
    leg_delta_y_bc_bx->Draw();
    c_delta_y_bc_bx->Print("plots_kinematics/c_delta_y_bc_bx.pdf");
    // -------------------------------------------------------------------------
    // Plot 2D angles integrated over all eta 
    // -------------------------------------------------------------------------


    SetLHCbStyle("cont");

    // angles
    TCanvas* c_delta_phi_pythia = new TCanvas("c_delta_phi_pythia","c_delta_phi_pythia");
    TH2D* h_delta_phi_pythia  = new TH2D("h_delta_phi_pythia", "h_delta_phi_pythia", 20,0,TMath::Pi(),20,0,TMath::Pi());
    TH2D* h_delta_phi_pythia_nompi  = new TH2D("h_delta_phi_pythia_nompi", "h_delta_phi_pythia_nompi", 20,0,TMath::Pi(),20,0,TMath::Pi());
    TH2D* h_delta_phi_bcvegpy = new TH2D("h_delta_phi_bcvegpy","h_delta_phi_bcvegpy",20,0,TMath::Pi(),20,0,TMath::Pi());

    pythia_tree->Draw("angle_bc_dx:angle_bc_bx>>h_delta_phi_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("angle_bc_dx:angle_bc_bx>>h_delta_phi_pythia_nompi",  cuts_pythia +cuts_all);
    bcvegpy_tree->Draw("angle_bc_dx:angle_bc_bx>>h_delta_phi_bcvegpy",cuts_bcvegpy+cuts_all);

    h_delta_phi_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_phi_pythia->Scale(scalefactor_pythia);
    h_delta_phi_pythia_nompi->Scale(scalefactor_pythia_nompi);


    double maximum_2D_delta_phi = max({
                                        h_delta_phi_pythia->GetMaximum(),
                                        h_delta_phi_pythia_nompi->GetMaximum(),
                                        h_delta_phi_bcvegpy->GetMaximum()
                                        });
    double minimum_2D_delta_phi = min({
                                        h_delta_phi_pythia->GetMinimum(),
                                        h_delta_phi_pythia_nompi->GetMinimum(),
                                        h_delta_phi_bcvegpy->GetMinimum()
                                        });


    h_delta_phi_pythia->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; #Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]");
    h_delta_phi_pythia->Draw("colz");
    // h_delta_phi_pythia->SetMaximum(maximum_2D_delta_phi);
    // h_delta_phi_pythia->SetMinimum(minimum_2D_delta_phi);
    h_delta_phi_pythia->SetMinimum(0.0);
    // gPad->SetLogz();
    c_delta_phi_pythia->Print("plots_kinematics/c_delta_phi_pythia.pdf");
    
    TCanvas* c_delta_phi_pythia_nompi = new TCanvas("c_delta_phi_pythia_nompi","c_delta_phi_pythia_nompi");
    h_delta_phi_pythia_nompi->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; #Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]");
    h_delta_phi_pythia_nompi->Draw("colz");
    // h_delta_phi_pythia_nompi->SetMaximum(maximum_2D_delta_phi);
    // h_delta_phi_pythia_nompi->SetMinimum(minimum_2D_delta_phi);
    h_delta_phi_pythia_nompi->SetMinimum(0.0);
    // gPad->SetLogz();

    c_delta_phi_pythia_nompi->Print("plots_kinematics/c_delta_phi_pythia_nompi.pdf");
    
    TCanvas* c_delta_phi_bcvegpy = new TCanvas("c_delta_phi_bcvegpy","c_delta_phi_bcvegpy");
    h_delta_phi_bcvegpy->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; #Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]");
    h_delta_phi_bcvegpy->Draw("colz");
    // h_delta_phi_bcvegpy->SetMaximum(maximum_2D_delta_phi);
    // h_delta_phi_bcvegpy->SetMinimum(minimum_2D_delta_phi);
    h_delta_phi_bcvegpy->SetMinimum(0.0);
    // gPad->SetLogz();

    c_delta_phi_bcvegpy->Print("plots_kinematics/c_delta_phi_bcvegpy.pdf");

    // eta
    TCanvas* c_delta_eta_pythia = new TCanvas("c_delta_eta_pythia","c_delta_eta_pythia");
    TH2D* h_delta_eta_pythia  = new TH2D("h_delta_eta_pythia", "h_delta_eta_pythia", 20,-5.0,5.0,20,-5.0,5.0);
    TH2D* h_delta_eta_bcvegpy = new TH2D("h_delta_eta_bcvegpy","h_delta_eta_bcvegpy",20,-5.0,5.0,20,-5.0,5.0);

    pythia_tree->Draw("delta_eta_bc_dx:delta_eta_bc_bx>>h_delta_eta_pythia",  cuts_pythia +cuts_all);
    bcvegpy_tree->Draw("delta_eta_bc_dx:delta_eta_bc_bx>>h_delta_eta_bcvegpy",cuts_bcvegpy+cuts_all);

    h_delta_eta_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_eta_pythia->Scale(scalefactor_pythia);

    h_delta_eta_pythia->SetTitle(";#Delta#eta(B_{c}^{+}, X_{b}) [rad]; #Delta#eta(B_{c}^{+}, X_{#bar{c}}) [rad]");
    h_delta_eta_pythia->Draw("colz");
    c_delta_eta_pythia->Print("plots_kinematics/c_delta_eta_pythia.pdf");
    
    TCanvas* c_delta_eta_bcvegpy = new TCanvas("c_delta_eta_bcvegpy","c_delta_eta_bcvegpy");
    h_delta_eta_bcvegpy->SetTitle(";#Delta#eta(B_{c}^{+}, X_{b}) [rad]; #Delta#eta(B_{c}^{+}, X_{#bar{c}}) [rad]");
    h_delta_eta_bcvegpy->Draw("colz");
    c_delta_eta_bcvegpy->Print("plots_kinematics/c_delta_eta_bcvegpy.pdf");

    // Delta R
    TCanvas* c_delta_R_pythia = new TCanvas("c_delta_R_pythia","c_delta_R_pythia");
    TH2D* h_delta_R_pythia  = new TH2D("h_delta_R_pythia", "h_delta_R_pythia", 20,0.0,10.0,20,0.0,10.0);
    TH2D* h_delta_R_bcvegpy = new TH2D("h_delta_R_bcvegpy","h_delta_R_bcvegpy",20,0.0,10.0,20,0.0,10.0);

    pythia_tree->Draw("delta_R_bc_dx:delta_R_bc_bx>>h_delta_R_pythia",  cuts_pythia +cuts_all);
    bcvegpy_tree->Draw("delta_R_bc_dx:delta_R_bc_bx>>h_delta_R_bcvegpy",cuts_bcvegpy+cuts_all);

    h_delta_R_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_R_pythia->Scale(scalefactor_pythia);

    h_delta_R_pythia->SetTitle(";#DeltaR(B_{c}^{+}, X_{b}) [rad]; #DeltaR(B_{c}^{+}, X_{#bar{c}}) [rad]");
    h_delta_R_pythia->Draw("colz");
    c_delta_R_pythia->Print("plots_kinematics/c_delta_R_pythia.pdf");
    
    TCanvas* c_delta_R_bcvegpy = new TCanvas("c_delta_R_bcvegpy","c_delta_R_bcvegpy");
    h_delta_R_bcvegpy->SetTitle(";#DeltaR(B_{c}^{+}, X_{b}) [rad]; #DeltaR(B_{c}^{+}, X_{#bar{c}}) [rad]");
    h_delta_R_bcvegpy->Draw("colz");
    c_delta_R_bcvegpy->Print("plots_kinematics/c_delta_R_bcvegpy.pdf");


    // ================================
    // All combinations 
    // ================================

    TH1D* h_1d_angle_bc_bx_withMPI  = new TH1D("h_1d_angle_bc_bx_withMPI", "h_1d_angle_bc_bx_withMPI", 10,0,TMath::Pi());
    TH1D* h_1d_angle_bc_bx_noMPI    = new TH1D("h_1d_angle_bc_bx_noMPI",   "h_1d_angle_bc_bx_noMPI",   10,0,TMath::Pi());

    h_1d_angle_bc_bx_withMPI->SetLineColor(kRed);
    h_1d_angle_bc_bx_noMPI->SetLineColor(kGreen+2);
    TH1D* h_1d_angle_bc_dx_withMPI  = new TH1D("h_1d_angle_bc_dx_withMPI", "h_1d_angle_bc_dx_withMPI", 10,0,TMath::Pi());
    TH1D* h_1d_angle_bc_dx_noMPI    = new TH1D("h_1d_angle_bc_dx_noMPI",   "h_1d_angle_bc_dx_noMPI",   10,0,TMath::Pi());
    h_1d_angle_bc_dx_withMPI->SetLineColor(kRed);
    h_1d_angle_bc_dx_noMPI->SetLineColor(kGreen+2);

    // std::map<std::string,TCut> b_cuts;
    // std::map<std::string,TCut> c_cuts;

    // b_cuts["Hard_b_bb"] = "foundHardb&&hasbStatus23&&hasbStatus23_type==5";
    // b_cuts["Hard_b_bx"] = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5)";
    // b_cuts["MPI_b_bb"]  = "foundMPIb&&hasbStatus33&&hasbStatus33_type==5";
    // b_cuts["MPI_b_bx"]  = "foundMPIb&&hasbStatus33&&(hasbStatus33_type!=5)";
    // b_cuts["Shower_b"]  = "foundShowerb";


    // c_cuts["Hard_c_cc"] = "foundHardc&&hascStatus23&&hascStatus23_type==4";
    // c_cuts["Hard_c_cx"] = "foundHardc&&hascStatus23&&(hascStatus23_type!=4)";
    // c_cuts["MPI_c_cc"]  = "foundMPIc&&hascStatus33&&hascStatus33_type==4";
    // c_cuts["MPI_c_cx"]  = "foundMPIc&&hascStatus33&&(hascStatus33_type!=4)";
    // c_cuts["Shower_c"]  = "foundShowerc";

    // TH2D* h_2d_withMPI     = new TH2D("h_2d_withMPI",    "", 10,0,TMath::Pi(),10,0,TMath::Pi());
    // TH2D* h_2d_noMPI_pp    = new TH2D("h_2d_noMPI_pp",   "", 10,0,TMath::Pi(),10,0,TMath::Pi());
    // TH2D* h_2d_noMPI_fe    = new TH2D("h_2d_noMPI_fe",   "", 10,0,TMath::Pi(),10,0,TMath::Pi());

    // TH1D* h_1d_pT_withMPI     = new TH1D("h_1d_pT_withMPI",    "", 50,0,20);
    // TH1D* h_1d_pT_noMPI_pp    = new TH1D("h_1d_pT_noMPI_pp",   "", 50,0,20);
    // TH1D* h_1d_pT_noMPI_fe    = new TH1D("h_1d_pT_noMPI_fe",   "", 50,0,20);
    
    // TH2D* h_2d_pT_y_withMPI     = new TH2D("h_2d_pT_y_withMPI",    "", 50,-10,10,50,0,20);
    // TH2D* h_2d_pT_y_noMPI_pp    = new TH2D("h_2d_pT_y_noMPI_pp",   "", 50,-10,10,50,0,20);
    // TH2D* h_2d_pT_y_noMPI_fe    = new TH2D("h_2d_pT_y_noMPI_fe",   "", 50,-10,10,50,0,20);


    // TH1D* h_1d_delta_y_bx_dx_withMPI     = new TH1D("h_1d_delta_y_bx_dx_withMPI",    "", 50,0,8);
    // TH1D* h_1d_delta_y_bx_dx_noMPI_pp    = new TH1D("h_1d_delta_y_bx_dx_noMPI_pp",   "", 50,0,8);
    // TH1D* h_1d_delta_y_bx_dx_noMPI_fe    = new TH1D("h_1d_delta_y_bx_dx_noMPI_fe",   "", 50,0,8);

    
    // for(const auto& b_cut: b_cuts){
    //     for(const auto& c_cut: c_cuts){
    //         std::string b_name = b_cut.first; 
    //         std::string c_name = c_cut.first; 


    //         TH2D* h_2d  = new TH2D(Form("h_2d_%s_%s",b_name.c_str(),c_name.c_str()), "", 10,0,TMath::Pi(),10,0,TMath::Pi());
    //         TH1D* h_1d_pT_cats  = new TH1D(Form("h_1d_pT_cats_%s_%s",b_name.c_str(),c_name.c_str()), "", 50,0,20);

    //         TCanvas* can_all = new TCanvas(Form("c_2d_%s_%s",b_name.c_str(),c_name.c_str()),
    //                                        Form("c_2d_%s_%s",b_name.c_str(),c_name.c_str()));

    //         TCut temp_cuts = cuts_pythia+b_cut.second+c_cut.second+cuts_all;
    //         // TCut temp_cuts = cuts_pythia+b_cut.second+c_cut.second;
    //         int n = pythia_tree->Draw(Form("angle_bc_dx:angle_bc_bx>>h_2d_%s_%s",b_name.c_str(),c_name.c_str()),temp_cuts,"colz");
            
    //         pythia_tree->Draw(Form("bc_pt>>h_1d_pT_cats_%s_%s",b_name.c_str(),c_name.c_str()),temp_cuts,"colz");

    //         std::string cat_name = "";
    //         if( (b_name == "Hard_b_bb" && c_name == "Shower_c"  )||
    //             (b_name == "MPI_b_bb"  && c_name == "Shower_c"  )||
    //             (b_name == "Shower_b"  && c_name == "Hard_c_cc" )||
    //             (b_name == "Shower_b"  && c_name == "MPI_c_cc"  )||
    //             (b_name == "Shower_b"  && c_name == "Shower_c"  )
    //             ){
                
    //             pythia_tree->Draw("angle_bc_dx:angle_bc_bx>>+h_2d_noMPI_pp", temp_cuts,"colz");
    //             pythia_tree->Draw("bc_pt>>+h_1d_pT_noMPI_pp", temp_cuts,"colz");
    //             pythia_tree->Draw("abs(delta_y_bx_dx)>>+h_1d_delta_y_bx_dx_noMPI_pp", temp_cuts,"colz");
    //             pythia_tree->Draw("bc_pt:bc_y>>+h_2d_pT_y_noMPI_pp", temp_cuts,"colz");
    //             cat_name = "noMPI_pp";

    //         } else if( 
    //             (b_name == "Hard_b_bx" && c_name == "Shower_c"  )||
    //             (b_name == "MPI_b_bx"  && c_name == "Shower_c"  )||
    //             (b_name == "Shower_b"  && c_name == "Hard_c_cx" )||
    //             (b_name == "Shower_b"  && c_name == "MPI_c_cx"  )||
    //             (b_name == "Hard_b_bx" && c_name == "Hard_c_cx"  )
    //             ){
    //             pythia_tree->Draw("angle_bc_dx:angle_bc_bx>>+h_2d_noMPI_fe",temp_cuts,"colz");
    //             pythia_tree->Draw("bc_pt>>+h_1d_pT_noMPI_fe",temp_cuts,"colz");
    //             pythia_tree->Draw("abs(delta_y_bx_dx)>>+h_1d_delta_y_bx_dx_noMPI_fe",temp_cuts,"colz");
    //             pythia_tree->Draw("bc_pt:bc_y>>+h_2d_pT_y_noMPI_fe",temp_cuts,"colz");
    //             cat_name = "noMPI_fe";

    //         } else {
    //             pythia_tree->Draw("angle_bc_dx:angle_bc_bx>>+h_2d_withMPI",temp_cuts,"colz");
    //             pythia_tree->Draw("bc_pt>>+h_1d_pT_withMPI",temp_cuts,"colz");
    //             pythia_tree->Draw("abs(delta_y_bx_dx)>>+h_1d_delta_y_bx_dx_withMPI",temp_cuts,"colz");
    //             pythia_tree->Draw("bc_pt:bc_y>>+h_2d_pT_y_withMPI",temp_cuts,"colz");
    //             cat_name = "withMPI";
    //         }

    //         std::cout << b_name <<"\t" << c_name << "\t" << n << "\t" << cat_name<<std::endl;
    //         h_2d->SetMinimum(0.0);
    //         h_2d->Draw("colz");
    //         h_2d->SetTitle(";#Delta#phi(B_{c}^{+},X_{b});#Delta#phi(B_{c}^{+},X_{#bar{c}})");
    //         can_all->Print(Form("plots_kinematics/c_2d_%s_%s.pdf",b_name.c_str(),c_name.c_str()));

    //         // //
    //         TCanvas* can_pt = new TCanvas(Form("c_1d_pT_cats_%s_%s",b_name.c_str(),c_name.c_str()),
    //                                       Form("c_1d_pT_cats_%s_%s",b_name.c_str(),c_name.c_str()));
    //         h_1d_pT_cats->SetMinimum(0.0);
    //         h_1d_pT_cats->Scale(scalefactor_pythia);
    //         h_1d_pT_cats->SetLineColor(kRed);
    //         h_1d_pT_cats->Draw("hist");

    //         h_1d_pT_cats->SetTitle(";#it{p}_{T} [GeV/#it{c}]; d#sigma/dp_{T} [2.5 #mub GeV^{-1}#it{c}]");
    //         can_pt->Print(Form("plots_kinematics/c_1d_pT_cats_%s_%s.pdf",b_name.c_str(),c_name.c_str()));


        
    //     }
    // }

    // TCanvas* c_2d_withMPI = new TCanvas("c_2d_withMPI","c_2d_withMPI");
    // h_2d_withMPI->SetMinimum(0.0);
    // h_2d_withMPI->Draw("colz");
    // h_2d_withMPI->SetTitle(";#Delta#phi(B_{c}^{+},X_{b});#Delta#phi(B_{c}^{+},X_{#bar{c}})");
    // c_2d_withMPI->Print("plots_kinematics/c_2d_withMPI.pdf");
    

    // TCanvas* c_2d_noMPI_pp = new TCanvas("c_2d_noMPI_pp","c_2d_noMPI_pp");
    // h_2d_noMPI_pp->SetMinimum(0.0);
    // h_2d_noMPI_pp->Draw("colz");
    // h_2d_noMPI_pp->SetTitle(";#Delta#phi(B_{c}^{+},X_{b});#Delta#phi(B_{c}^{+},X_{#bar{c}})");
    // c_2d_noMPI_pp->Print("plots_kinematics/c_2d_noMPI_pp.pdf");
    
    

    // TCanvas* c_2d_noMPI_fe = new TCanvas("c_2d_noMPI_fe","c_2d_noMPI_fe");
    // h_2d_noMPI_fe->SetMinimum(0.0);
    // h_2d_noMPI_fe->Draw("colz");
    // h_2d_noMPI_fe->SetTitle(";#Delta#phi(B_{c}^{+},X_{b});#Delta#phi(B_{c}^{+},X_{#bar{c}})");
    // c_2d_noMPI_fe->Print("plots_kinematics/c_2d_noMPI_fe.pdf");


    // // -------------------------------------------------------------------------

    // TCanvas* c_1d_pT_withMPI = new TCanvas("c_1d_pT_withMPI","c_1d_pT_withMPI");
    // h_1d_pT_withMPI->SetMinimum(0.0);
    // h_1d_pT_withMPI->Draw("colz");
    // h_1d_pT_withMPI->SetTitle(";#it{p}_{T} [GeV/#it{c}];Entries");
    // c_1d_pT_withMPI->Print("plots_kinematics/c_1d_pT_withMPI.pdf");
    
    // TCanvas* c_1d_pT_noMPI_pp = new TCanvas("c_1d_pT_noMPI_pp","c_1d_pT_noMPI_pp");
    // h_1d_pT_noMPI_pp->SetMinimum(0.0);
    // h_1d_pT_noMPI_pp->Draw("colz");
    // h_1d_pT_noMPI_pp->SetTitle(";#it{p}_{T} [GeV/#it{c}];Entries");
    // c_1d_pT_noMPI_pp->Print("plots_kinematics/c_1d_pT_noMPI_pp.pdf");

    // TCanvas* c_1d_pT_noMPI_fe = new TCanvas("c_1d_pT_noMPI_fe","c_1d_pT_noMPI_fe");
    // h_1d_pT_noMPI_fe->SetMinimum(0.0);
    // h_1d_pT_noMPI_fe->Draw("colz");
    // h_1d_pT_noMPI_fe->SetTitle(";#it{p}_{T} [GeV/#it{c}];Entries");
    // c_1d_pT_noMPI_fe->Print("plots_kinematics/c_1d_pT_noMPI_fe.pdf");

    // // -------------------------------------------------------------------------

    // TCanvas* c_1d_delta_y_bx_dx_withMPI = new TCanvas("c_1d_delta_y_bx_dx_withMPI","c_1d_delta_y_bx_dx_withMPI");
    // h_1d_delta_y_bx_dx_withMPI->SetMinimum(0.0);
    // h_1d_delta_y_bx_dx_withMPI->Draw("colz");
    // h_1d_delta_y_bx_dx_withMPI->SetTitle(";#Delta#it{y}(X_{b},X_{#bar{c}});Entries");
    // c_1d_delta_y_bx_dx_withMPI->Print("plots_kinematics/c_1d_delta_y_bx_dx_withMPI.pdf");
    
    // TCanvas* c_1d_delta_y_bx_dx_noMPI_pp = new TCanvas("c_1d_delta_y_bx_dx_noMPI_pp","c_1d_delta_y_bx_dx_noMPI_pp");
    // h_1d_delta_y_bx_dx_noMPI_pp->SetMinimum(0.0);
    // h_1d_delta_y_bx_dx_noMPI_pp->Draw("colz");
    // h_1d_delta_y_bx_dx_noMPI_pp->SetTitle(";#Delta#it{y}(X_{b},X_{#bar{c}});Entries");
    // c_1d_delta_y_bx_dx_noMPI_pp->Print("plots_kinematics/c_1d_delta_y_bx_dx_noMPI_pp.pdf");

    // TCanvas* c_1d_delta_y_bx_dx_noMPI_fe = new TCanvas("c_1d_delta_y_bx_dx_noMPI_fe","c_1d_delta_y_bx_dx_noMPI_fe");
    // h_1d_delta_y_bx_dx_noMPI_fe->SetMinimum(0.0);
    // h_1d_delta_y_bx_dx_noMPI_fe->Draw("colz");
    // h_1d_delta_y_bx_dx_noMPI_fe->SetTitle(";#Delta#it{y}(X_{b},X_{#bar{c}});Entries");
    // c_1d_delta_y_bx_dx_noMPI_fe->Print("plots_kinematics/c_1d_delta_y_bx_dx_noMPI_fe.pdf");

    // // -------------------------------------------------------------------------
    // h_2d_pT_y_withMPI->Scale(scalefactor_pythia);
    // h_2d_pT_y_noMPI_pp->Scale(scalefactor_pythia);
    // h_2d_pT_y_noMPI_fe->Scale(scalefactor_pythia);

    // double maximum_2D_pt_y_MPI_split = max( {h_2d_pT_y_withMPI->GetMaximum(),
    //                                         h_2d_pT_y_noMPI_pp->GetMaximum(),
    //                                         h_2d_pT_y_noMPI_fe->GetMaximum()});

    // h_2d_pT_y_withMPI->SetMaximum(maximum_2D_pt_y_MPI_split);
    // h_2d_pT_y_noMPI_pp->SetMaximum(maximum_2D_pt_y_MPI_split);
    // h_2d_pT_y_noMPI_fe->SetMaximum(maximum_2D_pt_y_MPI_split);


    // TCanvas* c_2d_pT_y_withMPI = new TCanvas("c_2d_pT_y_withMPI","c_2d_pT_y_withMPI");
    // h_2d_pT_y_withMPI->SetMinimum(0.0);
    // gPad->SetLogz();
    // h_2d_pT_y_withMPI->Draw("colz");
    // h_2d_pT_y_withMPI->SetTitle(";#it{y};#it{p}_{T} [GeV/#it{c}]");
    // c_2d_pT_y_withMPI->Print("plots_kinematics/c_2d_pT_y_withMPI.pdf");
    

    // TCanvas* c_2d_pT_y_noMPI_pp = new TCanvas("c_2d_pT_y_noMPI_pp","c_2d_pT_y_noMPI_pp");
    // h_2d_pT_y_noMPI_pp->SetMinimum(0.0);
    // gPad->SetLogz();
    // h_2d_pT_y_noMPI_pp->Draw("colz");
    // h_2d_pT_y_noMPI_pp->SetTitle(";#it{y};#it{p}_{T} [GeV/#it{c}]");
    // c_2d_pT_y_noMPI_pp->Print("plots_kinematics/c_2d_pT_y_noMPI_pp.pdf");
    
    

    // TCanvas* c_2d_pT_y_noMPI_fe = new TCanvas("c_2d_pT_y_noMPI_fe","c_2d_pT_y_noMPI_fe");
    // h_2d_pT_y_noMPI_fe->SetMinimum(0.0);
    // gPad->SetLogz();
    // h_2d_pT_y_noMPI_fe->Draw("colz");
    // h_2d_pT_y_noMPI_fe->SetTitle(";#it{y};#it{p}_{T} [GeV/#it{c}]");
    // c_2d_pT_y_noMPI_fe->Print("plots_kinematics/c_2d_pT_y_noMPI_fe.pdf");


    // Loop over Bc

   // Loop over the events and categorise them
    bool foundHardc; 
    bool hascStatus23; 
    bool hascStatus33; 
    int hascStatus23_type;
    int hascStatus33_type;
    int hascStatus33_type2;
    int hascStatus33_N;
    bool foundHardb; 
    bool hasbStatus23; 
    bool hasbStatus33; 
    int hasbStatus23_type;
    int hasbStatus33_type;
    int hasbStatus33_type2;
    int hasbStatus33_N;
    int nBc;
    int nXc;
    int nXb;
    int nMultiplicity;
    int nMPI;

    pythia_tree->SetBranchAddress("foundHardc",        &foundHardc); 
    pythia_tree->SetBranchAddress("hascStatus23",      &hascStatus23); 
    pythia_tree->SetBranchAddress("hascStatus33",      &hascStatus33); 
    pythia_tree->SetBranchAddress("hascStatus23_type", &hascStatus23_type );
    pythia_tree->SetBranchAddress("hascStatus33_type", &hascStatus33_type );
    pythia_tree->SetBranchAddress("hascStatus33_type2",&hascStatus33_type2 );
    pythia_tree->SetBranchAddress("hascStatus33_N",    &hascStatus33_N );
    pythia_tree->SetBranchAddress("foundHardc",        &foundHardc); 
    pythia_tree->SetBranchAddress("hasbStatus23",      &hasbStatus23); 
    pythia_tree->SetBranchAddress("hasbStatus33",      &hasbStatus33); 
    pythia_tree->SetBranchAddress("hasbStatus23_type", &hasbStatus23_type );
    pythia_tree->SetBranchAddress("hasbStatus33_type", &hasbStatus33_type );
    pythia_tree->SetBranchAddress("hasbStatus33_type2",&hasbStatus33_type2 );
    pythia_tree->SetBranchAddress("hasbStatus33_N",    &hasbStatus33_N );
    pythia_tree->SetBranchAddress("nBc",               &nBc );
    pythia_tree->SetBranchAddress("nXc",               &nXc );
    pythia_tree->SetBranchAddress("nXb",               &nXb );
    pythia_tree->SetBranchAddress("nMultiplicity",     &nMultiplicity );
    pythia_tree->SetBranchAddress("nMPI",              &nMPI );

    // Add kinematic distributions
    double bc_pe,  bc_px,  bc_py,  bc_pz;
    double dx_pe,  dx_px,  dx_py,  dx_pz;
    double bx_pe,  bx_px,  bx_py,  bx_pz;

    pythia_tree->SetBranchAddress("bc_px",&bc_px); 
    pythia_tree->SetBranchAddress("bc_py",&bc_py); 
    pythia_tree->SetBranchAddress("bc_pz",&bc_pz); 
    pythia_tree->SetBranchAddress("bc_pe",&bc_pe); 
    pythia_tree->SetBranchAddress("dx_px",&dx_px); 
    pythia_tree->SetBranchAddress("dx_py",&dx_py); 
    pythia_tree->SetBranchAddress("dx_pz",&dx_pz); 
    pythia_tree->SetBranchAddress("dx_pe",&dx_pe);
    pythia_tree->SetBranchAddress("bx_px",&bx_px); 
    pythia_tree->SetBranchAddress("bx_py",&bx_py); 
    pythia_tree->SetBranchAddress("bx_pz",&bx_pz); 
    pythia_tree->SetBranchAddress("bx_pe",&bx_pe); 

    std::vector<std::string> all_processes; 
    std::map<std::string,int> n_processes; 
    std::map<std::string,TH2D*> h_processes_delta_phi_2d; 
    std::map<std::string,TH2D*> h_processes_delta_eta_2d;

    std::map<std::string,TH1D*> h_processes_delta_y_bc_dx; 
    std::map<std::string,TH1D*> h_processes_delta_y_bx_dx; 
    std::map<std::string,TH1D*> h_processes_delta_y_bc_bx; 
    std::map<std::string,TH2D*> h_processes_delta_y_2d; 

    std::map<std::string,TH1D*> h_processes_delta_phi_bc_dx; 
    std::map<std::string,TH1D*> h_processes_delta_phi_bx_dx; 
    std::map<std::string,TH1D*> h_processes_delta_phi_bc_bx; 

    std::map<std::string,TH1D*> h_processes_nMPI; 
    std::map<std::string,TH1D*> h_processes_nMultiplicity; 


    std::map<std::string,TH2D*> h_processes_pt_eta_2d; 

    int n_bins     = 10;
    int n_bins_kin = 10;

    for(int i = 0; i< n_entries_pythia; i++){
        pythia_tree->GetEntry(i);
        if(nBc==1&&nXc==1&&nXb==1){

            double bc_pt = sqrt(bc_px*bc_px+bc_py*bc_py);
            double bx_pt = sqrt(bx_px*bx_px+bx_py*bx_py);
            double dx_pt = sqrt(dx_px*dx_px+dx_py*dx_py ); 

            double angle_bx_dx = acos((bx_px*dx_px + bx_py*dx_py)/(bx_pt*dx_pt));
            double angle_bc_dx = acos((bc_px*dx_px + bc_py*dx_py)/(bc_pt*dx_pt));
            double angle_bc_bx = acos((bc_px*bx_px + bc_py*bx_py)/(bc_pt*bx_pt));
    
            double bc_p   = sqrt(bc_px*bc_px+bc_py*bc_py+bc_pz*bc_pz);
            double bx_p   = sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz);
            double dx_p   = sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz);
            
            double bc_eta = (bc_pz>0.0?-1.0:1.0)*acosh(bc_p/bc_pt);
            double bx_eta = (bx_pz>0.0?-1.0:1.0)*acosh(bx_p/bx_pt);
            double dx_eta = (dx_pz>0.0?-1.0:1.0)*acosh(dx_p/dx_pt);

            double delta_eta_bc_bx = bc_eta - bx_eta;
            double delta_eta_bc_dx = bc_eta - dx_eta;
            double delta_eta_bx_dx = bx_eta - dx_eta;

            double bc_y = 0.5*log( (bc_pe+bc_pz) / (bc_pe-bc_pz) );
            double bx_y = 0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) );
            double dx_y = 0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) );

            double delta_y_bc_bx = bc_y - bx_y;
            double delta_y_bc_dx = bc_y - dx_y;
            double delta_y_bx_dx = bx_y - dx_y;

            std::vector<std::string> processes;
            std::string name;

            int n_c_accounted_for = hascStatus23 + hascStatus33_N;
            int n_b_accounted_for = hasbStatus23 + hasbStatus33_N;

            if(hasbStatus23){
                name = "Hard";
                if(hasbStatus23_type==5){
                    name += "_bb";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);
            }

            if(hasbStatus33_N == 1){
                name = "MPI";
                if(hasbStatus33_type==5){
                    name += "_bb";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);
            }

            if(hascStatus23){
                name = "Hard";
                if(hascStatus23_type==4){
                    name += "_cc";
                } else {
                    name += "_cx";
                }
                processes.push_back(name);
            }

            if(hascStatus33_N == 1){
                name = "MPI";
                if(hascStatus33_type==4){
                    name += "_cc";
                } else {
                    name += "_cx";
                }
                processes.push_back(name);
            }


            if(n_c_accounted_for==0){
                name =  "Showerc";
                processes.push_back(name);
            }

            if(n_b_accounted_for==0){
                name =  "Showerb";
                processes.push_back(name);
            }

            std::string cat_name;
            if(n_c_accounted_for+n_b_accounted_for<3 && processes.size()==2){
                cat_name = processes[0]+"_"+ processes[1];
            } else if(n_c_accounted_for+n_b_accounted_for >=3){
                cat_name = "Bad_3";
            } else {
                cat_name = "Bad_Other";
            }

            if(h_processes_delta_phi_2d.count(cat_name)==0){
                all_processes.push_back(cat_name);

                h_processes_delta_phi_2d[cat_name] = new TH2D(Form("h_%s_delta_phi_2d",cat_name.c_str()), 
                                                              Form("h_%s_delta_phi_2d;#Delta#phi(B_{c}^{+}, X_{b}) [rad];#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]",cat_name.c_str()),
                                                              n_bins,0,TMath::Pi(),
                                                              n_bins,0,TMath::Pi() );
                h_processes_pt_eta_2d[cat_name] = new TH2D(Form("h_%s_pt_eta_2d",cat_name.c_str()), 
                                                        Form("h_%s_pt_eta_2d;#eta;p_{T} [GeV/c^2]",cat_name.c_str()),
                                                        n_bins_kin,-10.0,10.0,
                                                        n_bins_kin,0,15.0 );
                h_processes_delta_eta_2d[cat_name] = new TH2D(Form("h_%s_delta_eta_2d",cat_name.c_str()), 
                                                        Form("h_%s_delta_eta_2d;#Delta#eta(B_{c}^{+}, X_{b});#Delta#eta(B_{c}^{+}, X_{#bar{c}})",cat_name.c_str()),
                                                        20,0,5.0,
                                                        20,0,5.0 );
                h_processes_delta_y_2d[cat_name] = new TH2D(Form("h_%s_delta_y_2d",cat_name.c_str()), 
                                                        Form("h_%s_delta_y_2d;#Delta#it{y}(B_{c}^{+}, X_{b});#Delta#it{y}(B_{c}^{+}, X_{#bar{c}})",cat_name.c_str()),
                                                        20,0,5.0,
                                                        20,0,5.0 );


                h_processes_delta_y_bc_dx[cat_name] = new TH1D(Form("h_%s_delta_y_bc_dx",cat_name.c_str()), 
                                                        Form("h_%s_delta_y_bc_dx;#Delta#it{y}(B_{c}^{+}, X_{#bar{c}});Entries",cat_name.c_str()),
                                                        50,0,5.0 );
                h_processes_delta_y_bx_dx[cat_name] = new TH1D(Form("h_%s_delta_y_bx_dx",cat_name.c_str()), 
                                                        Form("h_%s_delta_y_bx_dx;#Delta#it{y}(X_{b}, X_{#bar{c}});Entries",cat_name.c_str()),
                                                        50,0,5.0 );
                h_processes_delta_y_bc_bx[cat_name] = new TH1D(Form("h_%s_delta_y_bc_bx",cat_name.c_str()), 
                                                        Form("h_%s_delta_y_bc_bx;#Delta#it{y}(B_{c}^{+}, X_{b});Entries",cat_name.c_str()),
                                                        50,0,5.0 );


                h_processes_delta_phi_bc_bx[cat_name] = new TH1D(Form("h_%s_delta_phi_bc_bx",cat_name.c_str()), 
                                                                 Form("h_%s_delta_phi_bc_bx;#Delta#phi(B_{c}^{+}, X_{b}) [rad];Entries",cat_name.c_str()),
                                                                 50,0,TMath::Pi());

                h_processes_delta_phi_bc_dx[cat_name] = new TH1D(Form("h_%s_delta_phi_bc_dx",cat_name.c_str()), 
                                                                 Form("h_%s_delta_phi_bc_dx;#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad];Entries",cat_name.c_str()),
                                                                 50,0,TMath::Pi());

                h_processes_delta_phi_bx_dx[cat_name] = new TH1D(Form("h_%s_delta_phi_bx_dx",cat_name.c_str()), 
                                                                 Form("h_%s_delta_phi_bx_dx;#Delta#phi(X_{b}, X_{#bar{c}}) [rad];Entries",cat_name.c_str()),
                                                                 50,0,TMath::Pi());



                h_processes_nMPI[cat_name] = new TH1D(Form("h_%s_nMPI",cat_name.c_str()), 
                                                                 Form("h_%s_nMPI;nMPI;Entries",cat_name.c_str()),
                                                                 30,0,30);
                h_processes_nMultiplicity[cat_name] = new TH1D(Form("h_%s_nMultiplicity",cat_name.c_str()), 
                                                                 Form("h_%s_nMultiplicity;nMultiplicity;Entries",cat_name.c_str()),
                                                                 20,0,1000);

            }
            
            h_processes_delta_phi_2d[   cat_name]->Fill(angle_bc_bx,angle_bc_dx);
            h_processes_pt_eta_2d[      cat_name]->Fill(bc_eta,bc_pt);
            h_processes_delta_eta_2d[   cat_name]->Fill(delta_eta_bc_bx,delta_eta_bc_dx);
            h_processes_delta_y_2d[     cat_name]->Fill(abs(delta_y_bc_bx),abs(delta_y_bc_dx));

            h_processes_delta_y_bc_dx[  cat_name]->Fill(delta_y_bc_dx);
            h_processes_delta_y_bx_dx[  cat_name]->Fill(delta_y_bx_dx);
            h_processes_delta_y_bc_bx[  cat_name]->Fill(delta_y_bc_bx);
            h_processes_delta_phi_bc_dx[cat_name]->Fill(angle_bc_dx);
            h_processes_delta_phi_bx_dx[cat_name]->Fill(angle_bx_dx);
            h_processes_delta_phi_bc_bx[cat_name]->Fill(angle_bc_bx);

            h_processes_nMultiplicity[cat_name]->Fill(nMultiplicity);
            h_processes_nMPI[cat_name]->Fill(nMPI);

            n_processes[cat_name]++;
        }
    }

    for(const auto & n_proc: n_processes){
        std::cout << std::setw(25) << n_proc.first  << " | ";
        std::cout << std::setw(10) << n_proc.second << " | ";
        std::cout << std::endl;
    }   

    // Add plots together
    std::map<std::string,std::string> getCat;

    getCat["Hard_bb_MPI_cc"]   = "MPI"; 
    getCat["Hard_bb_MPI_cx"]   = "MPI"; 
    getCat["Hard_bx_Hard_cx"]  = "MPI"; 
    getCat["Hard_bx_MPI_cc"]   = "MPI"; 
    getCat["Hard_bx_MPI_cx"]   = "MPI"; 
    getCat["MPI_bb_Hard_cc"]   = "MPI"; 
    getCat["MPI_bb_Hard_cx"]   = "MPI"; 
    getCat["MPI_bb_MPI_cc"]    = "MPI"; 
    getCat["MPI_bb_MPI_cx"]    = "MPI"; 
    getCat["MPI_bx_Hard_cc"]   = "MPI"; 
    getCat["MPI_bx_Hard_cx"]   = "MPI"; 
    getCat["MPI_bx_MPI_cc"]    = "MPI"; 
    getCat["MPI_bx_MPI_cx"]    = "MPI"; 
    
    getCat["Hard_bb_Showerc"]  = "SPS_pp"; 
    getCat["Hard_cc_Showerb"]  = "SPS_pp"; 
    getCat["MPI_bb_Showerc"]   = "SPS_pp"; 
    getCat["MPI_cc_Showerb"]   = "SPS_pp"; 

    getCat["Hard_bx_Showerc"]  = "SPS_fe"; 
    getCat["Hard_cx_Showerb"]  = "SPS_fe"; 
    getCat["MPI_bx_Showerc"]   = "SPS_fe";
    getCat["MPI_cx_Showerb"]   = "SPS_fe";

    getCat["Showerc_Showerb"]  = "SPS_pp"; 

    // -------------------------------------------------------------------------
    // Save plots for individual categories
    // -------------------------------------------------------------------------

    // Draw angles    
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes_delta_phi_2d){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s",name.c_str()),Form("can_%s",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics/can_%s.pdf",name.c_str()));

    }
    // Draw delta eta 
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes_delta_eta_2d){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s",name.c_str()),Form("can_%s",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics/can_%s.pdf",name.c_str()));

    }
    // Draw delta eta 
    SetLHCbStyle("oneD");
    for(const auto & h_proc: h_processes_delta_y_bx_dx){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s",name.c_str()),Form("can_%s",name.c_str()));
        h_proc.second->Draw();
        can->Print(Form("plots_kinematics/can_%s.pdf",name.c_str()));

    }

    // Draw pt/eta
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes_pt_eta_2d){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s",name.c_str()),Form("can_%s",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics/can_%s.pdf",name.c_str()));

    }

    // -------------------------------------------------------------------------
    // Make combined categories 
    // -------------------------------------------------------------------------

    // -------------------------------------------------------------------------
    TH1D* h_processes_delta_phi_bc_dx_SPS_pp = new TH1D("h_processes_delta_phi_bc_dx_SPS_pp", 
                                     "h_processes_delta_phi_bc_dx_SPS_pp;#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                     50,0,TMath::Pi());
    TH1D* h_processes_delta_phi_bc_dx_SPS_fe = new TH1D("h_processes_delta_phi_bc_dx_SPS_fe", 
                                    "h_processes_delta_phi_bc_dx_SPS_fe;#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                    50,0,TMath::Pi());
    TH1D* h_processes_delta_phi_bc_dx_SPS = new TH1D("h_processes_delta_phi_bc_dx_SPS", 
                                    "h_processes_delta_phi_bc_dx_SPS;#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                    50,0,TMath::Pi());

    TH1D* h_processes_delta_phi_bc_dx_MPI = new TH1D("h_processes_delta_phi_bc_dx_MPI", 
                                    "h_processes_delta_phi_bc_dx_MPI;#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                    50,0,TMath::Pi());

    // -------------------------------------------------------------------------
    TH1D* h_processes_delta_phi_bc_bx_SPS_pp = new TH1D("h_processes_delta_phi_bc_bx_SPS_pp", 
                                     "h_processes_delta_phi_bc_bx_SPS_pp;#Delta#phi(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                     50,0,TMath::Pi());
    TH1D* h_processes_delta_phi_bc_bx_SPS_fe = new TH1D("h_processes_delta_phi_bc_bx_SPS_fe", 
                                    "h_processes_delta_phi_bc_bx_SPS_fe;#Delta#phi(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                    50,0,TMath::Pi());
    TH1D* h_processes_delta_phi_bc_bx_SPS = new TH1D("h_processes_delta_phi_bc_bx_SPS", 
                                    "h_processes_delta_phi_bc_bx_SPS;#Delta#phi(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                    50,0,TMath::Pi());

    TH1D* h_processes_delta_phi_bc_bx_MPI = new TH1D("h_processes_delta_phi_bc_bx_MPI", 
                                    "h_processes_delta_phi_bc_bx_MPI;#Delta#phi(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                    50,0,TMath::Pi());
    // -------------------------------------------------------------------------
    TH1D* h_processes_delta_phi_bx_dx_SPS_pp = new TH1D("h_processes_delta_phi_bx_dx_SPS_pp", 
                                     "h_processes_delta_phi_bx_dx_SPS_pp;#Delta#phi(X_{b}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                     50,0,TMath::Pi());
    TH1D* h_processes_delta_phi_bx_dx_SPS_fe = new TH1D("h_processes_delta_phi_bx_dx_SPS_fe", 
                                    "h_processes_delta_phi_bx_dx_SPS_fe;#Delta#phi(X_{b}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                    50,0,TMath::Pi());
    TH1D* h_processes_delta_phi_bx_dx_SPS = new TH1D("h_processes_delta_phi_bx_dx_SPS", 
                                    "h_processes_delta_phi_bx_dx_SPS;#Delta#phi(X_{b}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                    50,0,TMath::Pi());

    TH1D* h_processes_delta_phi_bx_dx_MPI = new TH1D("h_processes_delta_phi_bx_dx_MPI", 
                                    "h_processes_delta_phi_bx_dx_MPI;#Delta#phi(X_{b}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [#mub rad^{-1}]",
                                    50,0,TMath::Pi());
    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------

    TH1D* h_processes_delta_y_bc_dx_SPS_pp = new TH1D("h_processes_delta_y_bc_dx_SPS_pp", 
                                     "h_processes_delta_y_bc_dx_SPS_pp;#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#eta [#mub]",
                                     50,0,5.0 );
    TH1D* h_processes_delta_y_bc_dx_SPS_fe = new TH1D("h_processes_delta_y_bc_dx_SPS_fe", 
                                    "h_processes_delta_y_bc_dx_SPS_fe;#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#eta [#mub]",
                                    50,0,5.0 );
    TH1D* h_processes_delta_y_bc_dx_SPS = new TH1D("h_processes_delta_y_bc_dx_SPS", 
                                    "h_processes_delta_y_bc_dx_SPS;#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#eta [#mub]",
                                    50,0,5.0 );

    TH1D* h_processes_delta_y_bc_dx_MPI = new TH1D("h_processes_delta_y_bc_dx_MPI", 
                                    "h_processes_delta_y_bc_dx_MPI;#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#eta [#mub]",
                                    50,0,5.0 );
    // -------------------------------------------------------------------------
    TH1D* h_processes_delta_y_bc_bx_SPS_pp = new TH1D("h_processes_delta_y_bc_bx_SPS_pp", 
                                     "h_processes_delta_y_bc_bx_SPS_pp;#Delta#it{y}(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#eta [#mub]",
                                     50,0,5.0 );
    TH1D* h_processes_delta_y_bc_bx_SPS_fe = new TH1D("h_processes_delta_y_bc_bx_SPS_fe", 
                                    "h_processes_delta_y_bc_bx_SPS_fe;#Delta#it{y}(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#eta [#mub]",
                                    50,0,5.0 );
    TH1D* h_processes_delta_y_bc_bx_SPS = new TH1D("h_processes_delta_y_bc_bx_SPS", 
                                    "h_processes_delta_y_bc_bx_SPS;#Delta#it{y}(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#eta [#mub]",
                                    50,0,5.0 );

    TH1D* h_processes_delta_y_bc_bx_MPI = new TH1D("h_processes_delta_y_bc_bx_MPI", 
                                    "h_processes_delta_y_bc_bx_MPI;#Delta#it{y}(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#eta [#mub]",
                                    50,0,5.0 );
    // -------------------------------------------------------------------------
    TH1D* h_processes_delta_y_bx_dx_SPS_pp = new TH1D("h_processes_delta_y_bx_dx_SPS_pp", 
                                     "h_processes_delta_y_bx_dx_SPS_pp;#Delta#it{y}(X_{b},X_{#bar{c}}) [rad];d#sigma/d#Delta#eta [#mub]",
                                     50,0,5.0 );
    TH1D* h_processes_delta_y_bx_dx_SPS_fe = new TH1D("h_processes_delta_y_bx_dx_SPS_fe", 
                                    "h_processes_delta_y_bx_dx_SPS_fe;#Delta#it{y}(X_{b},X_{#bar{c}}) [rad];d#sigma/d#Delta#eta [#mub]",
                                    50,0,5.0 );
    TH1D* h_processes_delta_y_bx_dx_SPS = new TH1D("h_processes_delta_y_bx_dx_SPS", 
                                    "h_processes_delta_y_bx_dx_SPS;#Delta#it{y}(X_{b},X_{#bar{c}}) [rad];d#sigma/d#Delta#eta [#mub]",
                                    50,0,5.0 );

    TH1D* h_processes_delta_y_bx_dx_MPI = new TH1D("h_processes_delta_y_bx_dx_MPI", 
                                    "h_processes_delta_y_bx_dx_MPI;#Delta#it{y}(X_{b},X_{#bar{c}}) [rad];d#sigma/d#Delta#eta [#mub]",
                                    50,0,5.0 );
    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------
    TH2D* h_processes_delta_y_2d_SPS_pp = new TH2D("h_processes_delta_y_2d_SPS_pp", 
                                    "h_processes_delta_y_2d_SPS_pp;#Delta#it{y}(B_{c}^{+}, X_{b}) [rad];#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    20,0,5.0,
                                    20,0,5.0 );
    TH2D* h_processes_delta_y_2d_SPS_fe = new TH2D("h_processes_delta_y_2d_SPS_fe", 
                                    "h_processes_delta_y_2d_SPS_fe;#Delta#it{y}(B_{c}^{+}, X_{b}) [rad];#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    20,0,5.0,
                                    20,0,5.0 );
    TH2D* h_processes_delta_y_2d_SPS = new TH2D("h_processes_delta_y_2d_SPS", 
                                    "h_processes_delta_y_2d_SPS;#Delta#it{y}(B_{c}^{+}, X_{b}) [rad];#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    20,0,5.0,
                                    20,0,5.0 );

    TH2D* h_processes_delta_y_2d_MPI = new TH2D("h_processes_delta_y_2d_MPI", 
                                    "h_processes_delta_y_2d_MPI;#Delta#it{y}(B_{c}^{+}, X_{b}) [rad];#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    20,0,5.0,
                                    20,0,5.0 );
    // -------------------------------------------------------------------------
    TH2D* h_processes_delta_phi_2d_SPS_pp = new TH2D("h_processes_delta_phi_2d_SPS_pp", 
                                    "h_processes_delta_phi_2d_SPS_pp;#Delta#phi(B_{c}^{+}, X_{b}) [rad];#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );
    TH2D* h_processes_delta_phi_2d_SPS_fe = new TH2D("h_processes_delta_phi_2d_SPS_fe", 
                                    "h_processes_delta_phi_2d_SPS_fe;#Delta#phi(B_{c}^{+}, X_{b}) [rad];#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );
    TH2D* h_processes_delta_phi_2d_SPS = new TH2D("h_processes_delta_phi_2d_SPS", 
                                    "h_processes_delta_phi_2d_SPS;#Delta#phi(B_{c}^{+}, X_{b}) [rad];#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_delta_phi_2d_MPI = new TH2D("h_processes_delta_phi_2d_MPI", 
                                    "h_processes_delta_phi_2d_MPI;#Delta#phi(B_{c}^{+}, X_{b}) [rad];#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );
    // -------------------------------------------------------------------------
    TH2D* h_processes_pt_eta_2d_SPS_pp = new TH2D("h_processes_pt_eta_2d_SPS_pp", 
                                    "h_processes_pt_eta_2d_SPS_pp;#eta;p_{T} [GeV/c^2]",
                                    n_bins_kin,-10.0,10.0,
                                    n_bins_kin,0,15.0 );
    TH2D* h_processes_pt_eta_2d_SPS_fe = new TH2D("h_processes_pt_eta_2d_SPS_fe", 
                                    "h_processes_pt_eta_2d_SPS_fe;#eta;p_{T} [GeV/c^2]",
                                    n_bins_kin,-10.0,10.0,
                                    n_bins_kin,0,15.0 );
    TH2D* h_processes_pt_eta_2d_SPS = new TH2D("h_processes_pt_eta_2d_SPS", 
                                    "h_processes_pt_eta_2d_SPS;#eta;p_{T} [GeV/c^2]",
                                    n_bins_kin,-10.0,10.0,
                                    n_bins_kin,0,15.0 );

    TH2D* h_processes_pt_eta_2d_MPI = new TH2D("h_processes_pt_eta_2d_MPI", 
                                    "h_processes_pt_eta_2d_MPI;#eta;p_{T} [GeV/c^2]",
                                    n_bins_kin,-10.0,10.0,
                                    n_bins_kin,0,15.0 );
    // -------------------------------------------------------------------------
    TH2D* h_processes_delta_eta_2d_SPS_pp = new TH2D("h_processes_delta_eta_2d_SPS_pp", 
                                    "h_processes_delta_eta_2d_SPS_pp;#Delta#eta(B_{c}^{+}, X_{b});#Delta#eta(B_{c}^{+}, X_{#bar{c}})",
                                    20,0,5.0,
                                    20,0,5.0 );
    TH2D* h_processes_delta_eta_2d_SPS_fe = new TH2D("h_processes_delta_eta_2d_SPS_fe", 
                                    "h_processes_delta_eta_2d_SPS_fe;#Delta#eta(B_{c}^{+}, X_{b});#Delta#eta(B_{c}^{+}, X_{#bar{c}})",
                                    20,0,5.0,
                                    20,0,5.0 );
    TH2D* h_processes_delta_eta_2d_SPS = new TH2D("h_processes_delta_eta_2d_SPS", 
                                    "h_processes_delta_eta_2d_SPS;#Delta#eta(B_{c}^{+}, X_{b});#Delta#eta(B_{c}^{+}, X_{#bar{c}})",
                                    20,0,5.0,
                                    20,0,5.0 );

    TH2D* h_processes_delta_eta_2d_MPI = new TH2D("h_processes_delta_eta_2d_MPI", 
                                    "h_processes_delta_eta_2d_MPI;#Delta#eta(B_{c}^{+}, X_{b});#Delta#eta(B_{c}^{+}, X_{#bar{c}})",
                                    20,0,5.0,
                                    20,0,5.0 );
    // -------------------------------------------------------------------------


    // -------------------------------------------------------------------------
    TH1D* h_processes_nMPI_SPS_pp = new TH1D("h_processes_nMPI_SPS_pp", 
                                     "h_processes_nMPI_SPS_pp;N_{MPI};d#sigma/dN [#mub]",
                                     30,0,30);
    TH1D* h_processes_nMPI_SPS_fe = new TH1D("h_processes_nMPI_SPS_fe", 
                                    "h_processes_nMPI_SPS_fe;N_{MPI};d#sigma/dN [#mub]",
                                    30,0,30);
    TH1D* h_processes_nMPI_SPS = new TH1D("h_processes_nMPI_SPS", 
                                    "h_processes_nMPI_SPS;N_{MPI};d#sigma/dN [#mub]",
                                    30,0,30);

    TH1D* h_processes_nMPI_MPI = new TH1D("h_processes_nMPI_MPI", 
                                    "h_processes_nMPI_MPI;N_{MPI};d#sigma/dN [#mub]",
                                    30,0,30);


    // -------------------------------------------------------------------------
    TH1D* h_processes_nMultiplicity_SPS_pp = new TH1D("h_processes_nMultiplicity_SPS_pp", 
                                     "h_processes_nMultiplicity_SPS_pp;N_{Particles};d#sigma/dN [#mub]",
                                     20,0,1000);
    TH1D* h_processes_nMultiplicity_SPS_fe = new TH1D("h_processes_nMultiplicity_SPS_fe", 
                                    "h_processes_nMultiplicity_SPS_fe;N_{Particles};d#sigma/dN [#mub]",
                                    20,0,1000);
    TH1D* h_processes_nMultiplicity_SPS = new TH1D("h_processes_nMultiplicity_SPS", 
                                    "h_processes_nMultiplicity_SPS;N_{Particles};d#sigma/dN [#mub]",
                                    20,0,1000);

    TH1D* h_processes_nMultiplicity_MPI = new TH1D("h_processes_nMultiplicity_MPI", 
                                    "h_processes_nMultiplicity_MPI;N_{Particles};d#sigma/dN [#mub]",
                                    20,0,1000);


    for(int i = 0; i< all_processes.size(); i++){
        std::string cat_name = all_processes[i];
        if(getCat.count(cat_name)){
            std::cout << "Sorting " << cat_name << " into " << getCat[cat_name] << std::endl;
            
            if(getCat[cat_name] == "SPS_pp"){
                h_processes_delta_phi_2d_SPS_pp->Add(    h_processes_delta_phi_2d_SPS_pp,    h_processes_delta_phi_2d[cat_name]);
                h_processes_delta_eta_2d_SPS_pp->Add(    h_processes_delta_eta_2d_SPS_pp,    h_processes_delta_eta_2d[cat_name]);
                h_processes_delta_y_2d_SPS_pp->Add(      h_processes_delta_y_2d_SPS_pp,      h_processes_delta_y_2d[cat_name]);
                h_processes_pt_eta_2d_SPS_pp->Add(       h_processes_pt_eta_2d_SPS_pp,       h_processes_pt_eta_2d[cat_name]);
                
                h_processes_delta_phi_bx_dx_SPS_pp->Add( h_processes_delta_phi_bx_dx_SPS_pp, h_processes_delta_phi_bx_dx[cat_name]);
                h_processes_delta_phi_bc_bx_SPS_pp->Add( h_processes_delta_phi_bc_bx_SPS_pp, h_processes_delta_phi_bc_bx[cat_name]);
                h_processes_delta_phi_bc_dx_SPS_pp->Add( h_processes_delta_phi_bc_dx_SPS_pp, h_processes_delta_phi_bc_dx[cat_name]);
                
                h_processes_delta_y_bx_dx_SPS_pp->Add(   h_processes_delta_y_bx_dx_SPS_pp,   h_processes_delta_y_bx_dx[cat_name]);
                h_processes_delta_y_bc_bx_SPS_pp->Add(   h_processes_delta_y_bc_bx_SPS_pp,   h_processes_delta_y_bc_bx[cat_name]);
                h_processes_delta_y_bc_dx_SPS_pp->Add(   h_processes_delta_y_bc_dx_SPS_pp,   h_processes_delta_y_bc_dx[cat_name]);

                h_processes_nMPI_SPS_pp->Add(            h_processes_nMPI_SPS_pp,             h_processes_nMPI[cat_name]);
                h_processes_nMultiplicity_SPS_pp->Add(   h_processes_nMultiplicity_SPS_pp,    h_processes_nMultiplicity[cat_name]);


                h_processes_delta_phi_2d_SPS->Add(       h_processes_delta_phi_2d_SPS,       h_processes_delta_phi_2d[cat_name]);
                h_processes_delta_eta_2d_SPS->Add(       h_processes_delta_eta_2d_SPS,       h_processes_delta_eta_2d[cat_name]);
                h_processes_delta_y_2d_SPS->Add(         h_processes_delta_y_2d_SPS,         h_processes_delta_y_2d[cat_name]);
                h_processes_pt_eta_2d_SPS->Add(          h_processes_pt_eta_2d_SPS,          h_processes_pt_eta_2d[cat_name]);
                
                h_processes_delta_phi_bx_dx_SPS->Add(    h_processes_delta_phi_bx_dx_SPS,    h_processes_delta_phi_bx_dx[cat_name]);
                h_processes_delta_phi_bc_bx_SPS->Add(    h_processes_delta_phi_bc_bx_SPS,    h_processes_delta_phi_bc_bx[cat_name]);
                h_processes_delta_phi_bc_dx_SPS->Add(    h_processes_delta_phi_bc_dx_SPS,    h_processes_delta_phi_bc_dx[cat_name]);
                
                h_processes_delta_y_bx_dx_SPS->Add(      h_processes_delta_y_bx_dx_SPS,      h_processes_delta_y_bx_dx[cat_name]);
                h_processes_delta_y_bc_bx_SPS->Add(      h_processes_delta_y_bc_bx_SPS,      h_processes_delta_y_bc_bx[cat_name]);
                h_processes_delta_y_bc_dx_SPS->Add(      h_processes_delta_y_bc_dx_SPS,      h_processes_delta_y_bc_dx[cat_name]);

                h_processes_nMPI_SPS->Add(               h_processes_nMPI_SPS,               h_processes_nMPI[cat_name]);
                h_processes_nMultiplicity_SPS->Add(      h_processes_nMultiplicity_SPS,      h_processes_nMultiplicity[cat_name]);

            } else if(getCat[cat_name]== "SPS_fe"){
                h_processes_delta_phi_2d_SPS_fe->Add(    h_processes_delta_phi_2d_SPS_fe,    h_processes_delta_phi_2d[cat_name]);
                h_processes_delta_eta_2d_SPS_fe->Add(    h_processes_delta_eta_2d_SPS_fe,    h_processes_delta_eta_2d[cat_name]);
                h_processes_delta_y_2d_SPS_fe->Add(      h_processes_delta_y_2d_SPS_fe,      h_processes_delta_y_2d[cat_name]);
                h_processes_pt_eta_2d_SPS_fe->Add(       h_processes_pt_eta_2d_SPS_fe,       h_processes_pt_eta_2d[cat_name]);
                
                h_processes_delta_phi_bx_dx_SPS_fe->Add( h_processes_delta_phi_bx_dx_SPS_fe, h_processes_delta_phi_bx_dx[cat_name]);
                h_processes_delta_phi_bc_bx_SPS_fe->Add( h_processes_delta_phi_bc_bx_SPS_fe, h_processes_delta_phi_bc_bx[cat_name]);
                h_processes_delta_phi_bc_dx_SPS_fe->Add( h_processes_delta_phi_bc_dx_SPS_fe, h_processes_delta_phi_bc_dx[cat_name]);
                
                h_processes_delta_y_bx_dx_SPS_fe->Add(   h_processes_delta_y_bx_dx_SPS_fe,   h_processes_delta_y_bx_dx[cat_name]);
                h_processes_delta_y_bc_bx_SPS_fe->Add(   h_processes_delta_y_bc_bx_SPS_fe,   h_processes_delta_y_bc_bx[cat_name]);
                h_processes_delta_y_bc_dx_SPS_fe->Add(   h_processes_delta_y_bc_dx_SPS_fe,   h_processes_delta_y_bc_dx[cat_name]);

                h_processes_nMPI_SPS_fe->Add(            h_processes_nMPI_SPS_fe,            h_processes_nMPI[cat_name]);
                h_processes_nMultiplicity_SPS_fe->Add(   h_processes_nMultiplicity_SPS_fe,   h_processes_nMultiplicity[cat_name]);


                h_processes_delta_phi_2d_SPS->Add(       h_processes_delta_phi_2d_SPS,       h_processes_delta_phi_2d[cat_name]);
                h_processes_delta_eta_2d_SPS->Add(       h_processes_delta_eta_2d_SPS,       h_processes_delta_eta_2d[cat_name]);
                h_processes_delta_y_2d_SPS->Add(         h_processes_delta_y_2d_SPS,         h_processes_delta_y_2d[cat_name]);
                h_processes_pt_eta_2d_SPS->Add(          h_processes_pt_eta_2d_SPS,          h_processes_pt_eta_2d[cat_name]);
                
                h_processes_delta_phi_bx_dx_SPS->Add(    h_processes_delta_phi_bx_dx_SPS,    h_processes_delta_phi_bx_dx[cat_name]);
                h_processes_delta_phi_bc_bx_SPS->Add(    h_processes_delta_phi_bc_bx_SPS,    h_processes_delta_phi_bc_bx[cat_name]);
                h_processes_delta_phi_bc_dx_SPS->Add(    h_processes_delta_phi_bc_dx_SPS,    h_processes_delta_phi_bc_dx[cat_name]);
                
                h_processes_delta_y_bx_dx_SPS->Add(      h_processes_delta_y_bx_dx_SPS,      h_processes_delta_y_bx_dx[cat_name]);
                h_processes_delta_y_bc_bx_SPS->Add(      h_processes_delta_y_bc_bx_SPS,      h_processes_delta_y_bc_bx[cat_name]);
                h_processes_delta_y_bc_dx_SPS->Add(      h_processes_delta_y_bc_dx_SPS,      h_processes_delta_y_bc_dx[cat_name]);

                h_processes_nMPI_SPS->Add(               h_processes_nMPI_SPS,               h_processes_nMPI[cat_name]);
                h_processes_nMultiplicity_SPS->Add(      h_processes_nMultiplicity_SPS,      h_processes_nMultiplicity[cat_name]);

            
            }else if(getCat[cat_name]== "MPI"){
                h_processes_delta_phi_2d_MPI->Add(    h_processes_delta_phi_2d_MPI,    h_processes_delta_phi_2d[cat_name]);
                h_processes_delta_eta_2d_MPI->Add(    h_processes_delta_eta_2d_MPI,    h_processes_delta_eta_2d[cat_name]);
                h_processes_delta_y_2d_MPI->Add(      h_processes_delta_y_2d_MPI,      h_processes_delta_y_2d[cat_name]);
                h_processes_pt_eta_2d_MPI->Add(       h_processes_pt_eta_2d_MPI,       h_processes_pt_eta_2d[cat_name]);
                
                h_processes_delta_phi_bx_dx_MPI->Add( h_processes_delta_phi_bx_dx_MPI, h_processes_delta_phi_bx_dx[cat_name]);
                h_processes_delta_phi_bc_bx_MPI->Add( h_processes_delta_phi_bc_bx_MPI, h_processes_delta_phi_bc_bx[cat_name]);
                h_processes_delta_phi_bc_dx_MPI->Add( h_processes_delta_phi_bc_dx_MPI, h_processes_delta_phi_bc_dx[cat_name]);
                
                h_processes_delta_y_bx_dx_MPI->Add(   h_processes_delta_y_bx_dx_MPI,   h_processes_delta_y_bx_dx[cat_name]);
                h_processes_delta_y_bc_bx_MPI->Add(   h_processes_delta_y_bc_bx_MPI,   h_processes_delta_y_bc_bx[cat_name]);
                h_processes_delta_y_bc_dx_MPI->Add(   h_processes_delta_y_bc_dx_MPI,   h_processes_delta_y_bc_dx[cat_name]);

                h_processes_nMPI_MPI->Add(            h_processes_nMPI_MPI,            h_processes_nMPI[cat_name]);
                h_processes_nMultiplicity_MPI->Add(   h_processes_nMultiplicity_MPI,   h_processes_nMultiplicity[cat_name]);

            }
        } else {
            std::cout << "Cat not SPS or MPI " << cat_name << std::endl;
        }  
    }


    // -------------------------------------------------------------------------
    // Plot 2d distributions
    // -------------------------------------------------------------------------
    SetLHCbStyle("square");


    h_processes_delta_phi_2d_SPS_pp->SetMinimum(0);
    h_processes_delta_phi_2d_SPS_fe->SetMinimum(0);
    h_processes_delta_phi_2d_MPI->SetMinimum(0);


    TCanvas* can_delta_phi_2d_SPS = new TCanvas("can_delta_phi_2d_SPS","can_delta_phi_2d_SPS",550,500);
    h_processes_delta_phi_2d_SPS->Draw("colz");
    can_delta_phi_2d_SPS->Print("plots_kinematics/can_delta_phi_2d_SPS.pdf");
    
    TCanvas* can_delta_phi_2d_SPS_pp = new TCanvas("can_delta_phi_2d_SPS_pp","can_delta_phi_2d_SPS_pp",550,500);
    h_processes_delta_phi_2d_SPS_pp->Draw("colz");
    can_delta_phi_2d_SPS_pp->Print("plots_kinematics/can_delta_phi_2d_SPS_pp.pdf");

    TCanvas* can_delta_phi_2d_SPS_fe = new TCanvas("can_delta_phi_2d_SPS_fe","can_delta_phi_2d_SPS_fe",550,500);
    h_processes_delta_phi_2d_SPS_fe->Draw("colz");
    can_delta_phi_2d_SPS_fe->Print("plots_kinematics/can_delta_phi_2d_SPS_fe.pdf");
    
    TCanvas* can_delta_phi_2d_MPI = new TCanvas("can_delta_phi_2d_MPI","can_delta_phi_2d_MPI",550,500);
    h_processes_delta_phi_2d_MPI->Draw("colz");
    can_delta_phi_2d_MPI->Print("plots_kinematics/can_delta_phi_2d_MPI.pdf");

    // -------------------------------------------------------------------------

    h_processes_delta_y_2d_SPS_pp->SetMinimum(0);
    h_processes_delta_y_2d_SPS_fe->SetMinimum(0);
    h_processes_delta_y_2d_MPI->SetMinimum(0);

    TCanvas* can_delta_y_2d_SPS = new TCanvas("can_delta_y_2d_SPS","can_delta_y_2d_SPS");
    h_processes_delta_y_2d_SPS->Draw("colz");
    can_delta_y_2d_SPS->Print("plots_kinematics/can_delta_y_2d_SPS.pdf");
    
    TCanvas* can_delta_y_2d_SPS_pp = new TCanvas("can_delta_y_2d_SPS_pp","can_delta_y_2d_SPS_pp");
    h_processes_delta_y_2d_SPS_pp->Draw("colz");
    can_delta_y_2d_SPS_pp->Print("plots_kinematics/can_delta_y_2d_SPS_pp.pdf");

    TCanvas* can_delta_y_2d_SPS_fe = new TCanvas("can_delta_y_2d_SPS_fe","can_delta_y_2d_SPS_fe");
    h_processes_delta_y_2d_SPS_fe->Draw("colz");
    can_delta_y_2d_SPS_fe->Print("plots_kinematics/can_delta_y_2d_SPS_fe.pdf");
    
    TCanvas* can_delta_y_2d_MPI = new TCanvas("can_delta_y_2d_MPI","can_delta_y_2d_MPI");
    h_processes_delta_y_2d_MPI->Draw("colz");
    can_delta_y_2d_MPI->Print("plots_kinematics/can_delta_y_2d_MPI.pdf");

    // -------------------------------------------------------------------------

    h_processes_pt_eta_2d_SPS_pp->SetMinimum(0);
    h_processes_pt_eta_2d_SPS_fe->SetMinimum(0);
    h_processes_pt_eta_2d_MPI->SetMinimum(0);

    TCanvas* can_pt_eta_2d_SPS = new TCanvas("can_pt_eta_2d_SPS","can_pt_eta_2d_SPS");
    h_processes_pt_eta_2d_SPS->Draw("colz");
    can_pt_eta_2d_SPS->Print("plots_kinematics/can_pt_eta_2d_SPS.pdf");
    
    TCanvas* can_pt_eta_2d_SPS_pp = new TCanvas("can_pt_eta_2d_SPS_pp","can_pt_eta_2d_SPS_pp");
    h_processes_pt_eta_2d_SPS_pp->Draw("colz");
    can_pt_eta_2d_SPS_pp->Print("plots_kinematics/can_pt_eta_2d_SPS_pp.pdf");

    TCanvas* can_pt_eta_2d_SPS_fe = new TCanvas("can_pt_eta_2d_SPS_fe","can_pt_eta_2d_SPS_fe");
    h_processes_pt_eta_2d_SPS_fe->Draw("colz");
    can_pt_eta_2d_SPS_fe->Print("plots_kinematics/can_pt_eta_2d_SPS_fe.pdf");
    
    TCanvas* can_pt_eta_2d_MPI = new TCanvas("can_pt_eta_2d_MPI","can_pt_eta_2d_MPI");
    h_processes_pt_eta_2d_MPI->Draw("colz");
    can_pt_eta_2d_MPI->Print("plots_kinematics/can_pt_eta_2d_MPI.pdf");

    // -------------------------------------------------------------------------

    h_processes_delta_eta_2d_SPS_pp->SetMinimum(0);
    h_processes_delta_eta_2d_SPS_fe->SetMinimum(0);
    h_processes_delta_eta_2d_MPI->SetMinimum(0);

    TCanvas* can_delta_eta_2d_SPS = new TCanvas("can_delta_eta_2d_SPS","can_delta_eta_2d_SPS");
    h_processes_delta_eta_2d_SPS->Draw("colz");
    can_delta_eta_2d_SPS->Print("plots_kinematics/can_delta_eta_2d_SPS.pdf");
    
    TCanvas* can_delta_eta_2d_SPS_pp = new TCanvas("can_delta_eta_2d_SPS_pp","can_delta_eta_2d_SPS_pp");
    h_processes_delta_eta_2d_SPS_pp->Draw("colz");
    can_delta_eta_2d_SPS_pp->Print("plots_kinematics/can_delta_eta_2d_SPS_pp.pdf");

    TCanvas* can_delta_eta_2d_SPS_fe = new TCanvas("can_delta_eta_2d_SPS_fe","can_delta_eta_2d_SPS_fe");
    h_processes_delta_eta_2d_SPS_fe->Draw("colz");
    can_delta_eta_2d_SPS_fe->Print("plots_kinematics/can_delta_eta_2d_SPS_fe.pdf");
    
    TCanvas* can_delta_eta_2d_MPI = new TCanvas("can_delta_eta_2d_MPI","can_delta_eta_2d_MPI");
    h_processes_delta_eta_2d_MPI->Draw("colz");
    can_delta_eta_2d_MPI->Print("plots_kinematics/can_delta_eta_2d_MPI.pdf");


    // -------------------------------------------------------------------------
    // Plot 1d distributions
    // -------------------------------------------------------------------------
    SetLHCbStyle("oneD");    

    h_processes_delta_phi_bc_dx_SPS_pp->Scale(scalefactor_pythia);
    h_processes_delta_phi_bc_dx_SPS_fe->Scale(scalefactor_pythia);
    h_processes_delta_phi_bc_dx_MPI->Scale(scalefactor_pythia);
    h_processes_delta_phi_bc_bx_SPS_pp->Scale(scalefactor_pythia);
    h_processes_delta_phi_bc_bx_SPS_fe->Scale(scalefactor_pythia);
    h_processes_delta_phi_bc_bx_MPI->Scale(scalefactor_pythia);
    h_processes_delta_phi_bx_dx_SPS_pp->Scale(scalefactor_pythia);
    h_processes_delta_phi_bx_dx_SPS_fe->Scale(scalefactor_pythia);
    h_processes_delta_phi_bx_dx_MPI->Scale(scalefactor_pythia);
    
    h_processes_delta_y_bc_dx_SPS_pp->Scale(scalefactor_pythia);
    h_processes_delta_y_bc_dx_SPS_fe->Scale(scalefactor_pythia);
    h_processes_delta_y_bc_dx_MPI->Scale(scalefactor_pythia);
    h_processes_delta_y_bc_bx_SPS_pp->Scale(scalefactor_pythia);
    h_processes_delta_y_bc_bx_SPS_fe->Scale(scalefactor_pythia);
    h_processes_delta_y_bc_bx_MPI->Scale(scalefactor_pythia);
    h_processes_delta_y_bx_dx_SPS_pp->Scale(scalefactor_pythia);
    h_processes_delta_y_bx_dx_SPS_fe->Scale(scalefactor_pythia);
    h_processes_delta_y_bx_dx_MPI->Scale(scalefactor_pythia);

    TCanvas* can_delta_phi_bc_dx_all = new TCanvas("can_delta_phi_bc_dx_all","can_delta_phi_bc_dx_all");
    h_processes_delta_phi_bc_dx_SPS_pp->Draw("hist ][");
    h_processes_delta_phi_bc_dx_SPS_pp->SetMinimum(0.0);
    h_processes_delta_phi_bc_dx_SPS_pp->SetMaximum(1.1* max({h_processes_delta_phi_bc_dx_SPS_pp->GetMaximum(),
                                                           h_processes_delta_phi_bc_dx_SPS_fe->GetMaximum(),
                                                           h_processes_delta_phi_bc_dx_MPI->GetMaximum()}));
    double h_processes_delta_phi_bc_dx_SPS_pp_bin_width = h_processes_delta_phi_bc_dx_SPS_pp->GetXaxis()->GetBinWidth(2); 
    h_processes_delta_phi_bc_dx_SPS_pp->SetTitle(Form(";#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [%.2f mb rad^{-1}]",h_processes_delta_phi_bc_dx_SPS_pp_bin_width));
    h_processes_delta_phi_bc_dx_SPS_pp->SetLineColor(kRed);
    h_processes_delta_phi_bc_dx_SPS_fe->Draw("hist same ][");
    h_processes_delta_phi_bc_dx_SPS_fe->SetLineColor(kRed);
    h_processes_delta_phi_bc_dx_SPS_fe->SetLineStyle(kDashed);

    h_processes_delta_phi_bc_dx_MPI->Draw("hist same ][");
    h_processes_delta_phi_bc_dx_MPI->SetLineColor(kGreen+2);

    TLegend* leg_processes_delta_phi_bc_dx = new TLegend(0.5,0.7,0.9,0.9);
    leg_processes_delta_phi_bc_dx->AddEntry(h_processes_delta_phi_bc_dx_SPS_pp,"SPS Pair Creation","l");
    leg_processes_delta_phi_bc_dx->AddEntry(h_processes_delta_phi_bc_dx_SPS_fe,"SPS Flavour excitation","l");
    leg_processes_delta_phi_bc_dx->AddEntry(h_processes_delta_phi_bc_dx_MPI,   "DPS","l");
    leg_processes_delta_phi_bc_dx->SetTextFont(132);
    leg_processes_delta_phi_bc_dx->SetFillStyle(0);
    leg_processes_delta_phi_bc_dx->Draw();

    can_delta_phi_bc_dx_all->Print("plots_kinematics/can_delta_phi_bc_dx_all.pdf");

    TCanvas* can_delta_phi_bc_bx_all = new TCanvas("can_delta_phi_bc_bx_all","can_delta_phi_bc_bx_all");
    h_processes_delta_phi_bc_bx_SPS_pp->Draw("hist ][");
    h_processes_delta_phi_bc_bx_SPS_pp->SetMinimum(0.0);
    h_processes_delta_phi_bc_bx_SPS_pp->SetMaximum(1.1* max({h_processes_delta_phi_bc_bx_SPS_pp->GetMaximum(),
                                                           h_processes_delta_phi_bc_bx_SPS_fe->GetMaximum(),
                                                           h_processes_delta_phi_bc_bx_MPI->GetMaximum()}));
    double h_processes_delta_phi_bc_bx_SPS_pp_bin_width = h_processes_delta_phi_bc_bx_SPS_pp->GetXaxis()->GetBinWidth(2); 
    h_processes_delta_phi_bc_bx_SPS_pp->SetTitle(Form(";#Delta#phi(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#phi [%.2f mb rad^{-1}]",h_processes_delta_phi_bc_bx_SPS_pp_bin_width));
    
    h_processes_delta_phi_bc_bx_SPS_pp->SetLineColor(kRed);
    h_processes_delta_phi_bc_bx_SPS_fe->Draw("hist same ][");
    h_processes_delta_phi_bc_bx_SPS_fe->SetLineColor(kRed);
    h_processes_delta_phi_bc_bx_SPS_fe->SetLineStyle(kDashed);

    h_processes_delta_phi_bc_bx_MPI->Draw("hist same ][");
    h_processes_delta_phi_bc_bx_MPI->SetLineColor(kGreen+2);

    TLegend* leg_processes_delta_phi_bc_bx = new TLegend(0.2,0.7,0.6,0.9);
    leg_processes_delta_phi_bc_bx->AddEntry(h_processes_delta_phi_bc_bx_SPS_pp,"SPS Pair Creation","l");
    leg_processes_delta_phi_bc_bx->AddEntry(h_processes_delta_phi_bc_bx_SPS_fe,"SPS Flavour excitation","l");
    leg_processes_delta_phi_bc_bx->AddEntry(h_processes_delta_phi_bc_bx_MPI,   "DPS","l");
    leg_processes_delta_phi_bc_bx->SetTextFont(132);
    leg_processes_delta_phi_bc_bx->SetFillStyle(0);
    leg_processes_delta_phi_bc_bx->Draw();
    can_delta_phi_bc_bx_all->Print("plots_kinematics/can_delta_phi_bc_bx_all.pdf");

    TCanvas* can_delta_phi_bx_dx_all = new TCanvas("can_delta_phi_bx_dx_all","can_delta_phi_bx_dx_all");
    h_processes_delta_phi_bx_dx_SPS_pp->Draw("hist ][");
    h_processes_delta_phi_bx_dx_SPS_pp->SetMinimum(0.0);
    h_processes_delta_phi_bx_dx_SPS_pp->SetMaximum(1.1* max({h_processes_delta_phi_bx_dx_SPS_pp->GetMaximum(),
                                                           h_processes_delta_phi_bx_dx_SPS_fe->GetMaximum(),
                                                           h_processes_delta_phi_bx_dx_MPI->GetMaximum()}));
    double h_processes_delta_phi_bx_dx_SPS_pp_bin_width = h_processes_delta_phi_bx_dx_SPS_pp->GetXaxis()->GetBinWidth(2); 
    h_processes_delta_phi_bx_dx_SPS_pp->SetTitle(Form(";#Delta#phi(X_{b}, X_{#bar{c}}) [rad];d#sigma/d#Delta#phi [%.2f mb rad^{-1}]",h_processes_delta_phi_bx_dx_SPS_pp_bin_width));
    
    h_processes_delta_phi_bx_dx_SPS_pp->SetLineColor(kRed);
    h_processes_delta_phi_bx_dx_SPS_fe->Draw("hist same ][");
    h_processes_delta_phi_bx_dx_SPS_fe->SetLineColor(kRed);
    h_processes_delta_phi_bx_dx_SPS_fe->SetLineStyle(kDashed);

    h_processes_delta_phi_bx_dx_MPI->Draw("hist same ][");
    h_processes_delta_phi_bx_dx_MPI->SetLineColor(kGreen+2);

    TLegend* leg_processes_delta_phi_bx_dx = new TLegend(0.5,0.2,0.9,0.4);
    leg_processes_delta_phi_bx_dx->AddEntry(h_processes_delta_phi_bx_dx_SPS_pp,"SPS Pair Creation","l");
    leg_processes_delta_phi_bx_dx->AddEntry(h_processes_delta_phi_bx_dx_SPS_fe,"SPS Flavour excitation","l");
    leg_processes_delta_phi_bx_dx->AddEntry(h_processes_delta_phi_bx_dx_MPI,   "DPS","l");
    leg_processes_delta_phi_bx_dx->SetTextFont(132);
    leg_processes_delta_phi_bx_dx->SetFillStyle(0);
    leg_processes_delta_phi_bx_dx->Draw();

    can_delta_phi_bx_dx_all->Print("plots_kinematics/can_delta_phi_bx_dx_all.pdf");


    // -------------------------------------------------------------------------

    TCanvas* can_delta_y_bc_dx_all = new TCanvas("can_delta_y_bc_dx_all","can_delta_y_bc_dx_all");
    h_processes_delta_y_bc_dx_SPS_pp->Draw("hist ][");
    h_processes_delta_y_bc_dx_SPS_pp->SetMinimum(0.0);
    h_processes_delta_y_bc_dx_SPS_pp->SetMaximum(1.1* max({h_processes_delta_y_bc_dx_SPS_pp->GetMaximum(),
                                                           h_processes_delta_y_bc_dx_SPS_fe->GetMaximum(),
                                                           h_processes_delta_y_bc_dx_MPI->GetMaximum()}));
    double h_processes_delta_y_bc_dx_SPS_pp_bin_width = h_processes_delta_y_bc_dx_SPS_pp->GetXaxis()->GetBinWidth(2); 
    h_processes_delta_y_bc_dx_SPS_pp->SetTitle(Form(";#Delta#it{y}(B_{c}^{+}, X_{#bar{c}}) [rad];d#sigma/d#Delta#it{y} [%.2f mb]",h_processes_delta_y_bc_dx_SPS_pp_bin_width));
    h_processes_delta_y_bc_dx_SPS_pp->SetLineColor(kRed);
    h_processes_delta_y_bc_dx_SPS_fe->Draw("hist same ][");
    h_processes_delta_y_bc_dx_SPS_fe->SetLineColor(kRed);
    h_processes_delta_y_bc_dx_SPS_fe->SetLineStyle(kDashed);

    h_processes_delta_y_bc_dx_MPI->Draw("hist same ][");
    h_processes_delta_y_bc_dx_MPI->SetLineColor(kGreen+2);

    TLegend* leg_processes_delta_y_bc_dx = new TLegend(0.5,0.7,0.9,0.9);
    leg_processes_delta_y_bc_dx->AddEntry(h_processes_delta_y_bc_dx_SPS_pp,"SPS Pair Creation","l");
    leg_processes_delta_y_bc_dx->AddEntry(h_processes_delta_y_bc_dx_SPS_fe,"SPS Flavour excitation","l");
    leg_processes_delta_y_bc_dx->AddEntry(h_processes_delta_y_bc_dx_MPI,   "DPS","l");
    leg_processes_delta_y_bc_dx->SetTextFont(132);
    leg_processes_delta_y_bc_dx->SetFillStyle(0);
    leg_processes_delta_y_bc_dx->Draw();

    can_delta_y_bc_dx_all->Print("plots_kinematics/can_delta_y_bc_dx_all.pdf");

    TCanvas* can_delta_y_bc_bx_all = new TCanvas("can_delta_y_bc_bx_all","can_delta_y_bc_bx_all");
    h_processes_delta_y_bc_bx_SPS_pp->Draw("hist ][");
    h_processes_delta_y_bc_bx_SPS_pp->SetMinimum(0.0);
    h_processes_delta_y_bc_bx_SPS_pp->SetMaximum(1.1* max({h_processes_delta_y_bc_bx_SPS_pp->GetMaximum(),
                                                           h_processes_delta_y_bc_bx_SPS_fe->GetMaximum(),
                                                           h_processes_delta_y_bc_bx_MPI->GetMaximum()}));
    double h_processes_delta_y_bc_bx_SPS_pp_bin_width = h_processes_delta_y_bc_bx_SPS_pp->GetXaxis()->GetBinWidth(2); 
    h_processes_delta_y_bc_bx_SPS_pp->SetTitle(Form(";#Delta#it{y}(B_{c}^{+}, X_{b}) [rad];d#sigma/d#Delta#it{y} [%.2f mb]",h_processes_delta_y_bc_bx_SPS_pp_bin_width));
    h_processes_delta_y_bc_bx_SPS_pp->SetLineColor(kRed);
    h_processes_delta_y_bc_bx_SPS_fe->Draw("hist same ][");
    h_processes_delta_y_bc_bx_SPS_fe->SetLineColor(kRed);
    h_processes_delta_y_bc_bx_SPS_fe->SetLineStyle(kDashed);

    h_processes_delta_y_bc_bx_MPI->Draw("hist same ][");
    h_processes_delta_y_bc_bx_MPI->SetLineColor(kGreen+2);

    TLegend* leg_processes_delta_y_bc_bx = new TLegend(0.5,0.7,0.9,0.9);
    leg_processes_delta_y_bc_bx->AddEntry(h_processes_delta_y_bc_bx_SPS_pp,"SPS Pair Creation","l");
    leg_processes_delta_y_bc_bx->AddEntry(h_processes_delta_y_bc_bx_SPS_fe,"SPS Flavour excitation","l");
    leg_processes_delta_y_bc_bx->AddEntry(h_processes_delta_y_bc_bx_MPI,   "DPS","l");
    leg_processes_delta_y_bc_bx->SetTextFont(132);
    leg_processes_delta_y_bc_bx->SetFillStyle(0);
    leg_processes_delta_y_bc_bx->Draw();

    can_delta_y_bc_bx_all->Print("plots_kinematics/can_delta_y_bc_bx_all.pdf");

    TCanvas* can_delta_y_bx_dx_all = new TCanvas("can_delta_y_bx_dx_all","can_delta_y_bx_dx_all");
    h_processes_delta_y_bx_dx_SPS_pp->Draw("hist ][");
    h_processes_delta_y_bx_dx_SPS_pp->SetMinimum(0.0);
    h_processes_delta_y_bx_dx_SPS_pp->SetMaximum(1.1* max({h_processes_delta_y_bx_dx_SPS_pp->GetMaximum(),
                                                           h_processes_delta_y_bx_dx_SPS_fe->GetMaximum(),
                                                           h_processes_delta_y_bx_dx_MPI->GetMaximum()}));
    double h_processes_delta_y_bx_dx_SPS_pp_bin_width = h_processes_delta_y_bx_dx_SPS_pp->GetXaxis()->GetBinWidth(2); 
    h_processes_delta_y_bx_dx_SPS_pp->SetTitle(Form(";#Delta#it{y}( X_{b}, X_{#bar{c}}) [rad];d#sigma/d#Delta#it{y} [%.2f mb]",h_processes_delta_y_bx_dx_SPS_pp_bin_width));
    h_processes_delta_y_bx_dx_SPS_pp->SetLineColor(kRed);
    h_processes_delta_y_bx_dx_SPS_fe->Draw("hist same ][");
    h_processes_delta_y_bx_dx_SPS_fe->SetLineColor(kRed);
    h_processes_delta_y_bx_dx_SPS_fe->SetLineStyle(kDashed);

    h_processes_delta_y_bx_dx_MPI->Draw("hist same ][");
    h_processes_delta_y_bx_dx_MPI->SetLineColor(kGreen+2);

    TLegend* leg_processes_delta_y_bx_dx = new TLegend(0.5,0.7,0.9,0.9);
    leg_processes_delta_y_bx_dx->AddEntry(h_processes_delta_y_bx_dx_SPS_pp,"SPS Pair Creation","l");
    leg_processes_delta_y_bx_dx->AddEntry(h_processes_delta_y_bx_dx_SPS_fe,"SPS Flavour excitation","l");
    leg_processes_delta_y_bx_dx->AddEntry(h_processes_delta_y_bx_dx_MPI,   "DPS","l");
    leg_processes_delta_y_bx_dx->SetTextFont(132);
    leg_processes_delta_y_bx_dx->SetFillStyle(0);
    leg_processes_delta_y_bx_dx->Draw();

    can_delta_y_bx_dx_all->Print("plots_kinematics/can_delta_y_bx_dx_all.pdf");

    // Multiplicity 

    TCanvas* can_nMPI_all = new TCanvas("can_nMPI_all","can_nMPI_all");
    h_processes_nMPI_SPS_pp->Draw("hist ][");
    h_processes_nMPI_SPS_pp->SetMinimum(0.0);
    h_processes_nMPI_SPS_pp->SetMaximum(1.1* max({h_processes_nMPI_SPS_pp->GetMaximum(),
                                                           h_processes_nMPI_SPS_fe->GetMaximum(),
                                                           h_processes_nMPI_MPI->GetMaximum()}));
    double h_processes_nMPI_SPS_pp_bin_width = h_processes_nMPI_SPS_pp->GetXaxis()->GetBinWidth(2); 
    h_processes_nMPI_SPS_pp->SetTitle(Form(";N_{MPI};d#sigma/dN [%.2f mb]",h_processes_nMPI_SPS_pp_bin_width));
    h_processes_nMPI_SPS_pp->SetLineColor(kRed);
    h_processes_nMPI_SPS_fe->Draw("hist same ][");
    h_processes_nMPI_SPS_fe->SetLineColor(kRed);
    h_processes_nMPI_SPS_fe->SetLineStyle(kDashed);

    h_processes_nMPI_MPI->Draw("hist same ][");
    h_processes_nMPI_MPI->SetLineColor(kGreen+2);

    TLegend* leg_processes_nMPI = new TLegend(0.5,0.7,0.9,0.9);
    leg_processes_nMPI->AddEntry(h_processes_nMPI_SPS_pp,"SPS Pair Creation","l");
    leg_processes_nMPI->AddEntry(h_processes_nMPI_SPS_fe,"SPS Flavour excitation","l");
    leg_processes_nMPI->AddEntry(h_processes_nMPI_MPI,   "DPS","l");
    leg_processes_nMPI->SetTextFont(132);
    leg_processes_nMPI->SetFillStyle(0);
    leg_processes_nMPI->Draw();

    can_nMPI_all->Print("plots_kinematics/can_nMPI_all.pdf");

    TCanvas* can_nMultiplicity_all = new TCanvas("can_nMultiplicity_all","can_nMultiplicity_all");
    h_processes_nMultiplicity_SPS_pp->Draw("hist ][");
    h_processes_nMultiplicity_SPS_pp->SetMinimum(0.0);
    h_processes_nMultiplicity_SPS_pp->SetMaximum(1.1* max({h_processes_nMultiplicity_SPS_pp->GetMaximum(),
                                                           h_processes_nMultiplicity_SPS_fe->GetMaximum(),
                                                           h_processes_nMultiplicity_MPI->GetMaximum()}));
    double h_processes_nMultiplicity_SPS_pp_bin_width = h_processes_nMultiplicity_SPS_pp->GetXaxis()->GetBinWidth(2); 
    h_processes_nMultiplicity_SPS_pp->SetTitle(Form(";N_{Particles};d#sigma/dN [%.2f mb]",h_processes_nMultiplicity_SPS_pp_bin_width));
    h_processes_nMultiplicity_SPS_pp->SetLineColor(kRed);
    h_processes_nMultiplicity_SPS_fe->Draw("hist same ][");
    h_processes_nMultiplicity_SPS_fe->SetLineColor(kRed);
    h_processes_nMultiplicity_SPS_fe->SetLineStyle(kDashed);

    h_processes_nMultiplicity_MPI->Draw("hist same ][");
    h_processes_nMultiplicity_MPI->SetLineColor(kGreen+2);

    TLegend* leg_processes_nMultiplicity = new TLegend(0.5,0.7,0.9,0.9);
    leg_processes_nMultiplicity->AddEntry(h_processes_nMultiplicity_SPS_pp,"SPS Pair Creation","l");
    leg_processes_nMultiplicity->AddEntry(h_processes_nMultiplicity_SPS_fe,"SPS Flavour excitation","l");
    leg_processes_nMultiplicity->AddEntry(h_processes_nMultiplicity_MPI,   "DPS","l");
    leg_processes_nMultiplicity->SetTextFont(132);
    leg_processes_nMultiplicity->SetFillStyle(0);
    leg_processes_nMultiplicity->Draw();

    can_nMultiplicity_all->Print("plots_kinematics/can_nMultiplicity_all.pdf");
    
    TCanvas* can_nMultiplicity_all_ratio = new TCanvas("can_nMultiplicity_all_ratio","can_nMultiplicity_all_ratio");
    TH1D* h_processes_nMultiplicity_ratio = new TH1D("h_processes_nMultiplicity_ratio", 
                                     "h_processes_nMultiplicity_ratio;N_{Particles};d#sigma/dN [#mub]",
                                     20,0,1000);
    h_processes_nMultiplicity_ratio->Divide(h_processes_nMultiplicity_MPI,h_processes_nMultiplicity_SPS_pp);

    h_processes_nMultiplicity_ratio->Draw();   
    can_nMultiplicity_all_ratio->Print("plots_kinematics/can_nMultiplicity_all_ratio.pdf");

}