#include "tools.h"

void plot_kinematics_Bc_to_Bu(){
    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;


    double BcVegPy_crosssec_val = 113830880.77856530      * nanobarn; // nb
    double BcVegPy_crosssec_err =      4772.3452537976009 * nanobarn; // nb
    
    double theory_Br_Bc2JpsiPi = 0.33*1e-2;
    double theory_Br_Bu2JpsiK  = 0.106*1e-2;

    std::string filename_pythia_all      = "../output/main202output_SoftQCD_nd_N_500000_PartMod1.root";

    std::string filename_pythia_Bc       = "../output/from_lxplus/Complete_paper_samples/Sample_Bc/main202output_SoftQCD_nd_UserHook_bc_Scale_4.000000_N_250000_PartMod1_noColRec.root";
    // std::string filename_pythia_Bc_nompi = "../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bc_Scale_4.000000_N_52000_noMPI_PartMod1.root";

    std::string filename_bcvegpy         = "../output/from_lxplus/Complete_paper_samples/Sample_Bc_BcVegPy/main202output_BcVegPy_N_100000_PartMod1_old_b_mass.root";
    // std::string filename_bcvegpy         = "../output/from_lxplus/Complete_paper_samples/Sample_Bc_BcVegPy/main202output_BcVegPy_N_100000_PartMod1_noColRec.root";
    std::string filename_pythia_Bu       = "../output/from_lxplus/Complete_paper_samples/Sample_bb/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_10000000_PartMod1_noColRec.root";
    // std::string filename_pythia_Bu_nompi = "../output/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_50000_noMPI_PartMod1.root";
    
    // -------------------------------------------------------------------------
    // Fig cuts
    // -------------------------------------------------------------------------

    // TCut Bc_fid = "bc_y   >  2.0 && bc_y   <  4.5";
    // TCut Bu_fid = "Bu_p_y >  2.0 && Bu_p_y <  4.5";

    // TCut Bc_fid = "bc_y   >  2.0 && bc_y   <  2.9";
    // TCut Bu_fid = "Bu_p_y >  2.0 && Bu_p_y <  2.9";

    // TCut Bc_fid = "bc_pt   > 10 && bc_pt   < 20";
    // TCut Bu_fid = "Bu_p_pt > 10 && Bu_p_pt < 20";
    
    TCut Bc_fid = "";
    TCut Bu_fid = "";

    TCut cuts_Bc_all    = "nBc_p>=1";
    TCut cuts_Bu_all    = "nBu_p>=1";

    TCut cuts_DPS = "bc_b_partSys!=bc_c_partSys";
    TCut cuts_SPS = "bc_b_partSys==bc_c_partSys";

    TCut cuts_SPS_hard = "bc_b_partSys==0";

    TCut hassXbXc = "nXc==1 && nXb==1";
    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file_all = TFile::Open(filename_pythia_all.c_str());
    TTree* pythia_tree_all = (TTree*) pythia_file_all->Get("events");


    pythia_tree_all->SetAlias("bc_pt" , "sqrt(bc_px*bc_px+bc_py*bc_py)");
    pythia_tree_all->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree_all->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree_all->SetAlias("angle_bx_dx","acos((bx_px*dx_px + bx_py*dx_py)/(bx_pt*dx_pt))");
    pythia_tree_all->SetAlias("angle_bc_dx","acos((bc_px*dx_px + bc_py*dx_py)/(bc_pt*dx_pt))");
    pythia_tree_all->SetAlias("angle_bc_bx","acos((bc_px*bx_px + bc_py*bx_py)/(bc_pt*bx_pt))");


    pythia_tree_all->SetAlias("bc_p",  "sqrt(bc_px*bc_px+bc_py*bc_py+bc_pz*bc_pz)");
    pythia_tree_all->SetAlias("bc_pt", "sqrt(bc_px*bc_px+bc_py*bc_py)");
    pythia_tree_all->SetAlias("bc_eta","(bc_pz>0?-1:1)*acosh(bc_p/bc_pt)");
    pythia_tree_all->SetAlias("bc_y","0.5*log( (bc_pe+bc_pz) / (bc_pe-bc_pz) )");

    pythia_tree_all->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree_all->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree_all->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree_all->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    pythia_tree_all->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree_all->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_all->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree_all->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree_all->SetAlias("delta_eta_bx_dx","bx_eta-dx_eta");
    pythia_tree_all->SetAlias("delta_eta_bc_dx","bc_eta-dx_eta");
    pythia_tree_all->SetAlias("delta_eta_bc_bx","bc_eta-bx_eta");

    pythia_tree_all->SetAlias("delta_y_bx_dx","bx_y-dx_y");
    pythia_tree_all->SetAlias("delta_y_bc_dx","bc_y-dx_y");
    pythia_tree_all->SetAlias("delta_y_bc_bx","bc_y-bx_y");

    pythia_tree_all->SetAlias("delta_R_bx_dx","sqrt(delta_eta_bx_dx*delta_eta_bx_dx+angle_bx_dx*angle_bx_dx)");
    pythia_tree_all->SetAlias("delta_R_bc_dx","sqrt(delta_eta_bc_dx*delta_eta_bc_dx+angle_bc_dx*angle_bc_dx)");
    pythia_tree_all->SetAlias("delta_R_bc_bx","sqrt(delta_eta_bc_bx*delta_eta_bc_bx+angle_bc_bx*angle_bc_bx)");

    double sigmaGen_pythia_all;
    double sigmaErr_pythia_all;
    pythia_tree_all->SetBranchAddress("sigmaGen",&sigmaGen_pythia_all);
    pythia_tree_all->SetBranchAddress("sigmaErr",&sigmaErr_pythia_all);
    
    int n_entries_pythia_all = pythia_tree_all->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open(filename_pythia_Bc.c_str());
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");


    pythia_tree->SetAlias("bc_pt" , "sqrt(bc_px*bc_px+bc_py*bc_py)");
    pythia_tree->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_bx_dx","acos((bx_px*dx_px + bx_py*dx_py)/(bx_pt*dx_pt))");
    pythia_tree->SetAlias("angle_bc_dx","acos((bc_px*dx_px + bc_py*dx_py)/(bc_pt*dx_pt))");
    pythia_tree->SetAlias("angle_bc_bx","acos((bc_px*bx_px + bc_py*bx_py)/(bc_pt*bx_pt))");


    pythia_tree->SetAlias("bc_p",  "sqrt(bc_px*bc_px+bc_py*bc_py+bc_pz*bc_pz)");
    pythia_tree->SetAlias("bc_pt", "sqrt(bc_px*bc_px+bc_py*bc_py)");
    pythia_tree->SetAlias("bc_eta","(bc_pz>0?-1:1)*acosh(bc_p/bc_pt)");
    pythia_tree->SetAlias("bc_y","0.5*log( (bc_pe+bc_pz) / (bc_pe-bc_pz) )");

    pythia_tree->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree->SetAlias("delta_eta_bx_dx","bx_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_bc_dx","bc_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_bc_bx","bc_eta-bx_eta");

    pythia_tree->SetAlias("delta_y_bx_dx","bx_y-dx_y");
    pythia_tree->SetAlias("delta_y_bc_dx","bc_y-dx_y");
    pythia_tree->SetAlias("delta_y_bc_bx","bc_y-bx_y");

    pythia_tree->SetAlias("delta_R_bx_dx","sqrt(delta_eta_bx_dx*delta_eta_bx_dx+angle_bx_dx*angle_bx_dx)");
    pythia_tree->SetAlias("delta_R_bc_dx","sqrt(delta_eta_bc_dx*delta_eta_bc_dx+angle_bc_dx*angle_bc_dx)");
    pythia_tree->SetAlias("delta_R_bc_bx","sqrt(delta_eta_bc_bx*delta_eta_bc_bx+angle_bc_bx*angle_bc_bx)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // // -------------------------------------------------------------------------
    // // Get Pythia No MPI simulation
    // // -------------------------------------------------------------------------

    // TFile* pythia_nompi_file = TFile::Open(filename_pythia_Bc_nompi.c_str());
    // TTree* pythia_tree_nompi = (TTree*) pythia_nompi_file->Get("events");


    // pythia_tree_nompi->SetAlias("bc_pt" , "sqrt(bc_px*bc_px+bc_py*bc_py)");
    // pythia_tree_nompi->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");
    // pythia_tree_nompi->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    // pythia_tree_nompi->SetAlias("angle_bx_dx","acos((bx_px*dx_px + bx_py*dx_py)/(bx_pt*dx_pt))");
    // pythia_tree_nompi->SetAlias("angle_bc_dx","acos((bc_px*dx_px + bc_py*dx_py)/(bc_pt*dx_pt))");
    // pythia_tree_nompi->SetAlias("angle_bc_bx","acos((bc_px*bx_px + bc_py*bx_py)/(bc_pt*bx_pt))");


    // pythia_tree_nompi->SetAlias("bc_p",  "sqrt(bc_px*bc_px+bc_py*bc_py+bc_pz*bc_pz)");
    // pythia_tree_nompi->SetAlias("bc_pt", "sqrt(bc_px*bc_px+bc_py*bc_py)");
    // pythia_tree_nompi->SetAlias("bc_eta","(bc_pz>0?-1:1)*acosh(bc_p/bc_pt)");
    // pythia_tree_nompi->SetAlias("bc_y","0.5*log( (bc_pe+bc_pz) / (bc_pe-bc_pz) )");

    // pythia_tree_nompi->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    // pythia_tree_nompi->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    // pythia_tree_nompi->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    // pythia_tree_nompi->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    // pythia_tree_nompi->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    // pythia_tree_nompi->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    // pythia_tree_nompi->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    // pythia_tree_nompi->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    // pythia_tree_nompi->SetAlias("delta_eta_bx_dx","bx_eta-dx_eta");
    // pythia_tree_nompi->SetAlias("delta_eta_bc_dx","bc_eta-dx_eta");
    // pythia_tree_nompi->SetAlias("delta_eta_bc_bx","bc_eta-bx_eta");

    // pythia_tree_nompi->SetAlias("delta_y_bx_dx","bx_y-dx_y");
    // pythia_tree_nompi->SetAlias("delta_y_bc_dx","bc_y-dx_y");
    // pythia_tree_nompi->SetAlias("delta_y_bc_bx","bc_y-bx_y");

    // pythia_tree_nompi->SetAlias("delta_R_bx_dx","sqrt(delta_eta_bx_dx*delta_eta_bx_dx+angle_bx_dx*angle_bx_dx)");
    // pythia_tree_nompi->SetAlias("delta_R_bc_dx","sqrt(delta_eta_bc_dx*delta_eta_bc_dx+angle_bc_dx*angle_bc_dx)");
    // pythia_tree_nompi->SetAlias("delta_R_bc_bx","sqrt(delta_eta_bc_bx*delta_eta_bc_bx+angle_bc_bx*angle_bc_bx)");

    // double sigmaGen_pythia_nompi;
    // double sigmaErr_pythia_nompi;
    // pythia_tree_nompi->SetBranchAddress("sigmaGen",&sigmaGen_pythia_nompi);
    // pythia_tree_nompi->SetBranchAddress("sigmaErr",&sigmaErr_pythia_nompi);
    
    // int n_entries_pythia_nompi = pythia_tree_nompi->GetEntries();
    // -------------------------------------------------------------------------
    // Get BcVegPy simulation
    // -------------------------------------------------------------------------

    TFile* bcvegpy_file = TFile::Open(filename_bcvegpy.c_str());
    TTree* bcvegpy_tree = (TTree*) bcvegpy_file->Get("events");


    double sigmaGen_bcvegpy;
    double sigmaErr_bcvegpy;
    bcvegpy_tree->SetBranchAddress("sigmaGen",&sigmaGen_bcvegpy);
    bcvegpy_tree->SetBranchAddress("sigmaErr",&sigmaErr_bcvegpy);
    

    bcvegpy_tree->SetAlias("bc_pt" , "sqrt(bc_px*bc_px+bc_py*bc_py)");
    bcvegpy_tree->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");
    bcvegpy_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    bcvegpy_tree->SetAlias("angle_bx_dx","acos((bx_px*dx_px + bx_py*dx_py)/(bx_pt*dx_pt))");
    bcvegpy_tree->SetAlias("angle_bc_dx","acos((bc_px*dx_px + bc_py*dx_py)/(bc_pt*dx_pt))");
    bcvegpy_tree->SetAlias("angle_bc_bx","acos((bc_px*bx_px + bc_py*bx_py)/(bc_pt*bx_pt))");

    bcvegpy_tree->SetAlias("bc_p",  "sqrt(bc_px*bc_px+bc_py*bc_py+bc_pz*bc_pz)");
    bcvegpy_tree->SetAlias("bc_pt", "sqrt(bc_px*bc_px+bc_py*bc_py)");
    bcvegpy_tree->SetAlias("bc_eta","(bc_pz>0?-1:1)*acosh(bc_p/bc_pt)");
    bcvegpy_tree->SetAlias("bc_y","0.5*log( (bc_pe+bc_pz) / (bc_pe-bc_pz) )");

    bcvegpy_tree->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    bcvegpy_tree->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    bcvegpy_tree->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    bcvegpy_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    bcvegpy_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    bcvegpy_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    bcvegpy_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    bcvegpy_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    bcvegpy_tree->SetAlias("delta_eta_bx_dx","bx_eta-dx_eta");
    bcvegpy_tree->SetAlias("delta_eta_bc_dx","bc_eta-dx_eta");
    bcvegpy_tree->SetAlias("delta_eta_bc_bx","bc_eta-bx_eta");

    bcvegpy_tree->SetAlias("delta_y_bx_dx","bx_y-dx_y");
    bcvegpy_tree->SetAlias("delta_y_bc_dx","bc_y-dx_y");
    bcvegpy_tree->SetAlias("delta_y_bc_bx","bc_y-bx_y");



    bcvegpy_tree->SetAlias("delta_R_bx_dx","sqrt(delta_eta_bx_dx*delta_eta_bx_dx+angle_bx_dx*angle_bx_dx)");
    bcvegpy_tree->SetAlias("delta_R_bc_dx","sqrt(delta_eta_bc_dx*delta_eta_bc_dx+angle_bc_dx*angle_bc_dx)");
    bcvegpy_tree->SetAlias("delta_R_bc_bx","sqrt(delta_eta_bc_bx*delta_eta_bc_bx+angle_bc_bx*angle_bc_bx)");
    
    int n_entries_bcvegpy = bcvegpy_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    pythia_tree_all->GetEntry(pythia_tree_all->GetEntries()-1);
    // pythia_tree_nompi->GetEntry(pythia_tree_nompi->GetEntries()-1);
    bcvegpy_tree->GetEntry(bcvegpy_tree->GetEntries()-1);

    std::cout << "Cross section BcVegPy:       " << sigmaGen_bcvegpy*microbarn << " +/- " << sigmaErr_bcvegpy*microbarn << " microbarns"; 
    std::cout << "\t" << bcvegpy_tree->GetEntries("nBc>0") << "/" << bcvegpy_tree->GetEntries("")<<std::endl;
    
    std::cout << "Cross section Pythia:        " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nBc>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
    
    // std::cout << "Cross section Pythia NoMPI:  " << sigmaGen_pythia_nompi*microbarn  << " +/- " << sigmaErr_pythia_nompi*microbarn << " microbarns";
    // std::cout << "\t" << pythia_tree_nompi->GetEntries("nBc>0")<< "/" << pythia_tree_nompi->GetEntries("")<< std::endl;
        
    std::cout << "Cross section Pythia all:  " << sigmaGen_pythia_all*microbarn  << " +/- " << sigmaErr_pythia_all*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree_all->GetEntries("nBc>0")<< "/" << pythia_tree_all->GetEntries("")<< std::endl;
    
    // std::cout << "Cross section BcVegPy:  " << BcVegPy_crosssec_val  << " +/- " << BcVegPy_crosssec_err  << std::endl;


    double scalefactor_bcvegpy = 1e-3*sigmaGen_bcvegpy*microbarn/n_entries_bcvegpy;
    double scalefactor_pythia  =      sigmaGen_pythia*microbarn/n_entries_pythia;
    // double scalefactor_pythia_nompi  =sigmaGen_pythia_nompi*microbarn/n_entries_pythia_nompi;
    double scalefactor_pythia_all    =sigmaGen_pythia_all*microbarn/n_entries_pythia_all;
    

    
    // -------------------------------------------------------------------------
    // Get Bu files
    // -------------------------------------------------------------------------

    TFile* pythia_Bu_file = TFile::Open(filename_pythia_Bu.c_str());
    TTree* pythia_Bu_tree = (TTree*)pythia_Bu_file->Get("events");

    double sigmaGen_pythia_Bu;
    double sigmaErr_pythia_Bu;
    pythia_Bu_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia_Bu);
    pythia_Bu_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia_Bu);

    pythia_Bu_tree->SetAlias("bx_pt" ,  "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_Bu_tree->SetAlias("bx2_pt" , "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    pythia_Bu_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");
    pythia_Bu_tree->SetAlias("bx2_y","0.5*log( (bx2_pe+bx2_pz) / (bx2_pe-bx2_pz) )");

    pythia_Bu_tree->SetAlias("angle_bx_bx2","acos((bx_px*bx2_px + bx_py*bx2_py)/(bx_pt*bx2_pt))");

    pythia_Bu_tree->SetAlias("Bu_p_p",  "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py+Bu_p_pz*Bu_p_pz)");
    pythia_Bu_tree->SetAlias("Bu_p_pt", "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py)");
    pythia_Bu_tree->SetAlias("Bu_p_eta","(Bu_p_pz>0?-1:1)*acosh(Bu_p_p/Bu_p_pt)");
    pythia_Bu_tree->SetAlias("Bu_p_y","0.5*log( (Bu_p_pe+Bu_p_pz) / (Bu_p_pe-Bu_p_pz) )");



    int n_entries_pythia_Bu = pythia_Bu_tree->GetEntries();
    
    // -------------------------------------------------------------------------
    // Get Bu files
    // -------------------------------------------------------------------------

    // TFile* pythia_Bu_file_nompi = TFile::Open(filename_pythia_Bu_nompi.c_str());
    // TTree* pythia_Bu_tree_nompi = (TTree*)pythia_Bu_file_nompi->Get("events");

    // double sigmaGen_pythia_Bu_nompi;
    // double sigmaErr_pythia_Bu_nompi;
    // pythia_Bu_tree_nompi->SetBranchAddress("sigmaGen",&sigmaGen_pythia_Bu_nompi);
    // pythia_Bu_tree_nompi->SetBranchAddress("sigmaErr",&sigmaErr_pythia_Bu_nompi);

    // pythia_Bu_tree_nompi->SetAlias("bx_pt" ,  "sqrt(bx_px*bx_px+bx_py*bx_py)");
    // pythia_Bu_tree_nompi->SetAlias("bx2_pt" , "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    // pythia_Bu_tree_nompi->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");
    // pythia_Bu_tree_nompi->SetAlias("bx2_y","0.5*log( (bx2_pe+bx2_pz) / (bx2_pe-bx2_pz) )");

    // pythia_Bu_tree_nompi->SetAlias("angle_bx_bx2","acos((bx_px*bx2_px + bx_py*bx2_py)/(bx_pt*bx2_pt))");

    // int n_entries_pythia_Bu_nompi = pythia_Bu_tree_nompi->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_Bu_tree->GetEntry(pythia_Bu_tree->GetEntries()-1);
    // pythia_Bu_tree_nompi->GetEntry(pythia_Bu_tree_nompi->GetEntries()-1);

    std::cout << "Cross section Pythia Bu:        " << sigmaGen_pythia_Bu*microbarn  << " +/- " << sigmaErr_pythia_Bu*microbarn << " microbarns";
    std::cout << "\t" << pythia_Bu_tree->GetEntries("nBu>0")<< "/" << pythia_Bu_tree->GetEntries("")<< std::endl;

    // std::cout << "Cross section Pythia Bu No MPI: " << sigmaGen_pythia_Bu_nompi*microbarn  << " +/- " << sigmaErr_pythia_Bu_nompi*microbarn << " microbarns";
    // std::cout << "\t" << pythia_Bu_tree_nompi->GetEntries("nBu>0")<< "/" << pythia_Bu_tree_nompi->GetEntries("")<< std::endl;

    double scalefactor_pythia_Bu  =      sigmaGen_pythia_Bu*microbarn/n_entries_pythia_Bu;
    // double scalefactor_pythia_Bu_nompi  =      sigmaGen_pythia_Bu_nompi*microbarn/n_entries_pythia_Bu_nompi;



    // -------------------------------------------------------------------------
    // Plot pT of Bc and Bu
    // -------------------------------------------------------------------------
    TCanvas* c_pt_all = new TCanvas("c_pt_all","c_pt_all");
    TH1D* h_pt_pythia        = new TH1D("h_pt_pythia", "h_pt_pythia", 20,0,20);
    TH1D* h_pt_bcvegpy       = new TH1D("h_pt_bcvegpy","h_pt_bcvegpy",20,0,20);

    TH1D* h_pt_pythia_Bu        = new TH1D("h_pt_pythia_Bu", "h_pt_pythia_Bu", 20,0,20);


    pythia_tree->Draw(      "bc_pt>>h_pt_pythia",      cuts_Bc_all+Bc_fid);
    bcvegpy_tree->Draw(     "bc_pt>>h_pt_bcvegpy",     cuts_Bc_all+Bc_fid);

    pythia_Bu_tree->Draw(      "Bu_p_pt>>h_pt_pythia_Bu",      cuts_Bu_all+Bu_fid);

    h_pt_bcvegpy->Scale(scalefactor_bcvegpy);
    h_pt_pythia->Scale(scalefactor_pythia);

    h_pt_pythia_Bu->Scale(scalefactor_pythia_Bu);

    // Black -> With MPI
    h_pt_pythia->SetLineColor(kBlack);

    h_pt_pythia_Bu->SetLineColor(kBlack);
    h_pt_pythia_Bu->SetLineStyle(kDashed);


    // Green -> BcVegPy
    h_pt_bcvegpy->SetLineColor(kGreen+2);


    h_pt_pythia->SetMaximum(1.05*max({h_pt_pythia->GetMaximum(),
                                      h_pt_bcvegpy->GetMaximum(),
                                      h_pt_pythia_Bu->GetMaximum()
                                      }));
    
    double h_pt_pythia_bin_width = h_pt_pythia->GetXaxis()->GetBinWidth(2);
    h_pt_pythia->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_pythia_bin_width));
    h_pt_pythia->Draw("hist");
    h_pt_bcvegpy->Draw("same hist");
    h_pt_pythia_Bu->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_pt = new TLegend(0.6,0.6,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia_Bu,"Bu Pythia With MPI","l");
    leg_pt->AddEntry(h_pt_pythia,"Bc Pythia With MPI","l");

    leg_pt->AddEntry(h_pt_bcvegpy,"BcVegPy","l");
    leg_pt->SetTextFont(132);
    leg_pt->Draw();
    c_pt_all->Print("plots_kinematics_Bc_to_Bu/c_pt_all.pdf");


    // -------------------------------------------------------------------------
    // Plot theory
    // -------------------------------------------------------------------------


    TCanvas* c_pt_theory = new TCanvas("c_pt_theory","c_pt_theory");

    // ---- Make theory comparision
    TH1D* h_pt_theory_Bc = (TH1D*) h_pt_bcvegpy->Clone("h_pt_theory_Bc");
    TH1D* h_pt_theory_Bu = new TH1D( "h_pt_theory_Bu","h_pt_theory_Bu",20,0,20);
    h_pt_theory_Bu->SetBinContent(   1      ,   5.2085e+06);
    h_pt_theory_Bu->SetBinContent(   2      ,   1.2663e+07);
    h_pt_theory_Bu->SetBinContent(   3      ,   1.5310e+07);
    h_pt_theory_Bu->SetBinContent(   4      ,   1.4486e+07);
    h_pt_theory_Bu->SetBinContent(   5      ,   1.2042e+07);
    h_pt_theory_Bu->SetBinContent(   6      ,   9.3091e+06);
    h_pt_theory_Bu->SetBinContent(   7      ,   6.9358e+06);
    h_pt_theory_Bu->SetBinContent(   8      ,   5.0900e+06);
    h_pt_theory_Bu->SetBinContent(   9      ,   3.7264e+06);
    h_pt_theory_Bu->SetBinContent(   10     ,   2.7404e+06);
    h_pt_theory_Bu->SetBinContent(   11     ,   2.0315e+06);
    h_pt_theory_Bu->SetBinContent(   12     ,   1.5206e+06);
    h_pt_theory_Bu->SetBinContent(   13     ,   1.1502e+06);
    h_pt_theory_Bu->SetBinContent(   14     ,   8.7866e+05);
    h_pt_theory_Bu->SetBinContent(   15     ,   6.7812e+05);
    h_pt_theory_Bu->SetBinContent(   16     ,   5.2835e+05);
    h_pt_theory_Bu->SetBinContent(   17     ,   4.1523e+05);
    h_pt_theory_Bu->SetBinContent(   18     ,   3.2906e+05);
    h_pt_theory_Bu->SetBinContent(   19     ,   2.6286e+05);
    h_pt_theory_Bu->SetBinContent(   20     ,   2.1153e+05);

    double total_fonll_sigma = h_pt_theory_Bu->Integral();
    double total_Bc_sigma    = h_pt_theory_Bc->Integral();

    h_pt_theory_Bu->Scale(1.0/total_fonll_sigma);
    h_pt_theory_Bc->Scale(1.0/total_Bc_sigma);
    TH1D* h_pt_theory_ratio = new TH1D( "h_pt_theory_ratio","h_pt_theory_ratio",20,0,20);

    double Bu_fragmentation_fraction = 0.35; // ??? 

    double frac_in_acc = bcvegpy_tree->GetEntries("nBc_p>0&&bc_y>2.0&&bc_y<4.5")/(double)bcvegpy_tree->GetEntries("nBc_p>0");
    double crosssection_Bcp_all = 1e-3*sigmaGen_bcvegpy*microbarn;
    // double crosssection_Bu = 93.720;
    // double crosssection_Bu = 86.6/2.0;
    double crosssection_Bupm_inacc = 86.6; // Measured B^\pm cross section 
    double crosssection_Bup_inacc  = crosssection_Bupm_inacc / (2 - Bu_fragmentation_fraction); 
    double crosssection_Bcp_inacc  = crosssection_Bcp_all*frac_in_acc;

    std::cout << "Frac in acc:             " << frac_in_acc << std::endl;
    std::cout << "crosssection_Bcp_all:    " << crosssection_Bcp_all << std::endl;
    std::cout << "crosssection_Bcp_inacc:  " << crosssection_Bcp_inacc << std::endl;
    std::cout << "crosssection_Bupm_inacc: " << crosssection_Bupm_inacc << std::endl;
    std::cout << "crosssection_Bup_inacc:  " << crosssection_Bup_inacc  << std::endl;
    
    double frac_Bp_inacc  = pythia_Bu_tree->GetEntries("nBu_p>0&&Bu_p_y>2&&Bu_p_y<4.5")/(double)pythia_Bu_tree->GetEntries();
    double frac_Bp        = pythia_Bu_tree->GetEntries("nBu_p>0")/(double)pythia_Bu_tree->GetEntries();
    double frac_Bpm       = pythia_Bu_tree->GetEntries("nBu>0")/(double)pythia_Bu_tree->GetEntries();
    


    std::cout << "Frac in acc Bu:     " << frac_Bp << std::endl;
    std::cout << "Frac in acc Bu:     " << frac_Bpm << std::endl;
    
    

    std::cout << "crosssection Bu+ in acc (pythia): "  <<  frac_Bp_inacc*sigmaGen_pythia_Bu*microbarn   << " +/- "  << frac_Bp_inacc*sigmaErr_pythia_Bu*microbarn << " microbarns" << std::endl;
    std::cout << "crosssection Bu+        (pythia): "  <<  frac_Bp      *sigmaGen_pythia_Bu*microbarn   << " +/- "  << frac_Bp      *sigmaErr_pythia_Bu*microbarn << " microbarns" << std::endl;
    std::cout << "crosssection Bu+-       (pythia): "  <<  frac_Bpm     *sigmaGen_pythia_Bu*microbarn   << " +/- " << frac_Bpm      *sigmaErr_pythia_Bu*microbarn << " microbarns" << std::endl;


    h_pt_theory_Bu->Draw();

    h_pt_theory_ratio->Sumw2();
    h_pt_theory_ratio->Divide(h_pt_theory_Bc,h_pt_theory_Bu);
    
    // h_pt_theory_ratio->Scale(100*theory_Br_Bc2JpsiPi/theory_Br_Bu2JpsiK*(frac_in_acc*crosssection_Bcp_all/(crosssection_Bu*Bu_fragmentation_fraction)) );
    // h_pt_theory_ratio->Scale(100*theory_Br_Bc2JpsiPi/theory_Br_Bu2JpsiK*(frac_in_acc*crosssection_Bcp_all/(crosssection_Bu)) );
    h_pt_theory_ratio->Scale(100*theory_Br_Bc2JpsiPi/theory_Br_Bu2JpsiK*(crosssection_Bcp_inacc/(crosssection_Bup_inacc)) );
    h_pt_theory_ratio->Draw();
    h_pt_theory_ratio->SetMaximum(1.8);
    h_pt_theory_ratio->SetMinimum(0.0);
    h_pt_theory_ratio->SetTitle(";#it{p}_{T} [GeV/#it{c}];R(#it{p}_{T}) (%)");

    c_pt_theory->Print("plots_kinematics_Bc_to_Bu/c_pt_theory.pdf");
    
    // -------------------------------------------------------------------------
    // Compare FONLL to pythia in acceptance just B+
    // -------------------------------------------------------------------------


    TCanvas* c_pt_xsec_theory_inacc = new TCanvas("c_pt_xsec_theory_inacc","c_pt_xsec_theory_inacc");
    TH1D* h_pt_theory_xsec_inacc_Bu = new TH1D( "h_pt_theory_xsec_inacc_Bu","h_pt_theory_xsec_inacc_Bu",20,0,20);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   1      ,   5.2085e+06*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   2      ,   1.2663e+07*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   3      ,   1.5310e+07*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   4      ,   1.4486e+07*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   5      ,   1.2042e+07*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   6      ,   9.3091e+06*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   7      ,   6.9358e+06*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   8      ,   5.0900e+06*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   9      ,   3.7264e+06*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   10     ,   2.7404e+06*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   11     ,   2.0315e+06*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   12     ,   1.5206e+06*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   13     ,   1.1502e+06*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   14     ,   8.7866e+05*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   15     ,   6.7812e+05*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   16     ,   5.2835e+05*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   17     ,   4.1523e+05*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   18     ,   3.2906e+05*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   19     ,   2.6286e+05*1e-6);
    h_pt_theory_xsec_inacc_Bu->SetBinContent(   20     ,   2.1153e+05*1e-6);

    TH1D* h_pt_pythia_xsec_inacc_Bu  = new TH1D("h_pt_pythia_xsec_inacc_Bu", "h_pt_pythia_xsec_inacc_Bu", 20,0,20);

    pythia_Bu_tree->Draw("Bu_p_pt>>h_pt_pythia_xsec_inacc_Bu",cuts_Bu_all+ "Bu_p_y >  2.0 && Bu_p_y <  4.5");
    h_pt_pythia_xsec_inacc_Bu->Scale(scalefactor_pythia_Bu);

    double h_pt_theory_xsec_inacc_Bu_bin_width = h_pt_theory_xsec_inacc_Bu->GetXaxis()->GetBinWidth(2);
    h_pt_theory_xsec_inacc_Bu->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma(B^{+})/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_theory_xsec_inacc_Bu_bin_width));
    h_pt_theory_xsec_inacc_Bu->Scale(Bu_fragmentation_fraction);
    h_pt_theory_xsec_inacc_Bu->SetMaximum(1.05*max({h_pt_theory_xsec_inacc_Bu->GetMaximum(),
                                              h_pt_pythia_xsec_inacc_Bu->GetMaximum()
                                  }));

    
    // h_pt_theory_xsec_inacc_Bu->Scale(2.0);
    // h_pt_pythia_xsec_inacc_Bu->Scale(2.0);


    h_pt_theory_xsec_inacc_Bu->SetMinimum(3e-3);
    h_pt_theory_xsec_inacc_Bu->SetMaximum(40);

    h_pt_theory_xsec_inacc_Bu->Draw("hist");
    h_pt_pythia_xsec_inacc_Bu->Draw("same");
    gPad->SetLogy();
    TLegend* leg_pt_theory_xsec = new TLegend(0.6,0.2,0.8,0.4);
    leg_pt_theory_xsec->AddEntry(h_pt_theory_xsec_inacc_Bu,"Pythia","pe");
    leg_pt_theory_xsec->AddEntry(h_pt_pythia_xsec_inacc_Bu,"FonLL","l");
    leg_pt_theory_xsec->SetTextFont(132);
    leg_pt_theory_xsec->SetFillStyle(0);
    leg_pt_theory_xsec->Draw();

    c_pt_xsec_theory_inacc->Print("plots_kinematics_Bc_to_Bu/c_pt_xsec_theory_inacc.pdf");

    // -------------------------------------------------------------------------
    // Compare FONLL to Pythia everywhere B+- 
    // -------------------------------------------------------------------------


    TCanvas* c_pt_xsec_theory_allacc = new TCanvas("c_pt_xsec_theory_allacc","c_pt_xsec_theory_allacc");
    TH1D* h_pt_theory_xsec_allacc_Bu = new TH1D( "h_pt_theory_xsec_allacc_Bu","h_pt_theory_xsec_allacc_Bu",20,0,20);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   1      ,   2.4668e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   2      ,   6.0098e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   3      ,   7.2972e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   4      ,   6.9546e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   5      ,   5.8390e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   6      ,   4.5662e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   7      ,   3.4445e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   8      ,   2.5607e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   9      ,   1.8995e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   10     ,   1.4155e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   11     ,   1.0631e+07*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   12     ,   8.0622e+06*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   13     ,   6.1775e+06*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   14     ,   4.7796e+06*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   15     ,   3.7362e+06*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   16     ,   2.9480e+06*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   17     ,   2.3458e+06*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   18     ,   1.8821e+06*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   19     ,   1.5221e+06*1e-6);
    h_pt_theory_xsec_allacc_Bu->SetBinContent(   20     ,   1.2399e+06*1e-6);


    TH1D* h_pt_pythia_xsec_allacc_Bu  = new TH1D("h_pt_pythia_xsec_allacc_Bu", "h_pt_pythia_xsec_allacc_Bu", 20,0,20);

    pythia_Bu_tree->Draw("Bu_p_pt>>h_pt_pythia_xsec_allacc_Bu","nBu_p>0");
    h_pt_pythia_xsec_allacc_Bu->Scale(scalefactor_pythia_Bu);

    double h_pt_theory_xsec_allacc_Bu_bin_width = h_pt_theory_xsec_allacc_Bu->GetXaxis()->GetBinWidth(2);
    h_pt_theory_xsec_allacc_Bu->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma(B^{#pm})/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_theory_xsec_allacc_Bu_bin_width));
    
    h_pt_theory_xsec_allacc_Bu->Scale(2.0);
    h_pt_theory_xsec_allacc_Bu->Scale(Bu_fragmentation_fraction);
    
    h_pt_theory_xsec_allacc_Bu->SetMaximum(1.05*max({h_pt_theory_xsec_allacc_Bu->GetMaximum(),
                                              h_pt_pythia_xsec_allacc_Bu->GetMaximum()
                                  }));

    


    h_pt_theory_xsec_allacc_Bu->SetMinimum(3e-3);
    h_pt_theory_xsec_allacc_Bu->SetMaximum(100);

    h_pt_theory_xsec_allacc_Bu->Draw("hist");
    h_pt_pythia_xsec_allacc_Bu->Draw("same");
    gPad->SetLogy();
    TLegend* leg_pt_theory_xsec_allacc = new TLegend(0.6,0.2,0.8,0.4);
    leg_pt_theory_xsec_allacc->AddEntry(h_pt_theory_xsec_allacc_Bu,"Pythia","pe");
    leg_pt_theory_xsec_allacc->AddEntry(h_pt_pythia_xsec_allacc_Bu,"FonLL","l");
    leg_pt_theory_xsec_allacc->SetTextFont(132);
    leg_pt_theory_xsec_allacc->SetFillStyle(0);
    leg_pt_theory_xsec_allacc->Draw();

    c_pt_xsec_theory_allacc->Print("plots_kinematics_Bc_to_Bu/c_pt_xsec_theory_allacc.pdf");




    // -------------------------------------------------------------------------
    // Plot pT of just Bc
    // -------------------------------------------------------------------------

    TCanvas* c_pt_Bc = new TCanvas("c_pt_Bc","c_pt_Bc");
    TH1D* h_pt_Bc_pythia_all  = new TH1D("h_pt_Bc_pythia_all", "h_pt_Bc_pythia_all", 50,0,20);
    TH1D* h_pt_Bc_pythia_SPS  = new TH1D("h_pt_Bc_pythia_SPS", "h_pt_Bc_pythia_SPS", 50,0,20);
    TH1D* h_pt_Bc_pythia_DPS  = new TH1D("h_pt_Bc_pythia_DPS", "h_pt_Bc_pythia_DPS", 50,0,20);
    TH1D* h_pt_Bc_bcvegpy     = new TH1D("h_pt_Bc_bcvegpy",    "h_pt_Bc_bcvegpy"  ,  50,0,20);

    pythia_tree->Draw("bc_pt>>h_pt_Bc_pythia_all",cuts_Bc_all+Bc_fid);
    pythia_tree->Draw("bc_pt>>h_pt_Bc_pythia_DPS",cuts_Bc_all+Bc_fid+cuts_DPS);
    pythia_tree->Draw("bc_pt>>h_pt_Bc_pythia_SPS",cuts_Bc_all+Bc_fid+cuts_SPS);
    bcvegpy_tree->Draw("bc_pt>>h_pt_Bc_bcvegpy",  cuts_Bc_all+Bc_fid);

    h_pt_Bc_bcvegpy->Scale(scalefactor_bcvegpy);
    h_pt_Bc_pythia_all->Scale(scalefactor_pythia);
    h_pt_Bc_pythia_DPS->Scale(scalefactor_pythia);
    h_pt_Bc_pythia_SPS->Scale(scalefactor_pythia);

    // Black -> All
    h_pt_Bc_pythia_all->SetLineColor(kBlack);

    // Red -> just SPS
    h_pt_Bc_pythia_SPS->SetLineColor(kRed);

    // Blue -> just DPS
    h_pt_Bc_pythia_DPS->SetLineColor(kBlue);

    // Green -> BcVegPy
    h_pt_Bc_bcvegpy->SetLineColor(kGreen+2);

    // h_pt_Bc_bcvegpy->Scale(1.0/h_pt_Bc_bcvegpy->Integral());
    // h_pt_Bc_pythia_all->Scale(1.0/h_pt_Bc_pythia_all->Integral());
    // h_pt_Bc_pythia_DPS->Scale(1.0/h_pt_Bc_pythia_DPS->Integral());
    // h_pt_Bc_pythia_SPS->Scale(1.0/h_pt_Bc_pythia_SPS->Integral());

    h_pt_Bc_pythia_all->SetMaximum(1.05*max({h_pt_Bc_pythia_all->GetMaximum(),
                                      h_pt_Bc_bcvegpy->GetMaximum(),
                                      h_pt_Bc_pythia_SPS->GetMaximum(),
                                      h_pt_Bc_pythia_DPS->GetMaximum()
                                      }));
    
    double h_pt_Bc_pythia_all_bin_width = h_pt_Bc_pythia_all->GetXaxis()->GetBinWidth(2);
    h_pt_Bc_pythia_all->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma(B^{+}_{c})/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_Bc_pythia_all_bin_width));
    h_pt_Bc_pythia_all->Draw("hist");
    h_pt_Bc_pythia_SPS->Draw("hist same");
    h_pt_Bc_pythia_DPS->Draw("hist same");
    h_pt_Bc_bcvegpy->Draw("same hist");
    // gPad->SetLogy();

    TLegend* leg_pt_Bc = new TLegend(0.55,0.6,0.93,0.9);
    leg_pt_Bc->AddEntry(h_pt_Bc_pythia_all,         "Pythia - All","l");
    leg_pt_Bc->AddEntry(h_pt_Bc_pythia_SPS,         "Pythia - Just SPS","l");
    leg_pt_Bc->AddEntry(h_pt_Bc_pythia_DPS,         "Pythia - Just DPS","l");
    leg_pt_Bc->AddEntry(h_pt_Bc_bcvegpy,            "BcVegPy","l");
    leg_pt_Bc->SetTextFont(132);
    leg_pt_Bc->SetFillStyle(0);
    leg_pt_Bc->Draw();
    c_pt_Bc->Print("plots_kinematics_Bc_to_Bu/c_pt_Bc.pdf");



    // -------------------------------------------------------------------------
    // Plot pT ratio for DPS vs. SPS
    // -------------------------------------------------------------------------
    TCanvas* c_pt_DPSfrac = new TCanvas("c_pt_DPSfrac","c_pt_DPSfrac");
    
    TH1D* h_pt_Bc_DPSfrac_pythia_DPS  = new TH1D("h_pt_Bc_DPSfrac_pythia_DPS", "h_pt_Bc_DPSfrac_pythia_DPS", 20,0,20);
    TH1D* h_pt_Bc_DPSfrac_pythia_all  = new TH1D("h_pt_Bc_DPSfrac_pythia_all", "h_pt_Bc_DPSfrac_pythia_all", 20,0,20);
    TH1D* h_pt_pythia_DPSfrac         = new TH1D("h_pt_pythia_DPSfrac",        "h_pt_pythia_DPSfrac",        20,0,20);

    pythia_tree->Draw(   "bc_pt>>h_pt_Bc_DPSfrac_pythia_all",cuts_Bc_all+Bc_fid);
    pythia_tree->Draw(   "bc_pt>>h_pt_Bc_DPSfrac_pythia_DPS",cuts_Bc_all+Bc_fid+cuts_DPS);
    
    h_pt_Bc_DPSfrac_pythia_all->Scale(scalefactor_pythia);
    h_pt_Bc_DPSfrac_pythia_DPS->Scale(scalefactor_pythia);

    h_pt_pythia_DPSfrac->Divide(      h_pt_Bc_DPSfrac_pythia_DPS,h_pt_Bc_DPSfrac_pythia_all);
    h_pt_pythia_DPSfrac->Scale(100);

    h_pt_pythia_DPSfrac->SetTitle(";#it{p}_{T} [GeV/#it{c}];f_{DPS} (%)");
    h_pt_pythia_DPSfrac->SetMinimum(0);
    h_pt_pythia_DPSfrac->SetMaximum(100);
    h_pt_pythia_DPSfrac->Draw("");

    c_pt_DPSfrac->Print("plots_kinematics_Bc_to_Bu/c_pt_DPSfrac.pdf");

    // -------------------------------------------------------------------------
    // Plot y ratio for DPS vs. SPS
    // -------------------------------------------------------------------------
    TCanvas* c_y_DPSfrac = new TCanvas("c_y_DPSfrac","c_y_DPSfrac");
    
    TH1D* h_y_Bc_DPSfrac_pythia_DPS  = new TH1D("h_y_Bc_DPSfrac_pythia_DPS", "h_y_Bc_DPSfrac_pythia_DPS", 20,-5.0,5.0);
    TH1D* h_y_Bc_DPSfrac_pythia_all  = new TH1D("h_y_Bc_DPSfrac_pythia_all", "h_y_Bc_DPSfrac_pythia_all", 20,-5.0,5.0);
    TH1D* h_y_pythia_DPSfrac         = new TH1D("h_y_pythia_DPSfrac",        "h_y_pythia_DPSfrac",        20,-5.0,5.0);

    pythia_tree->Draw(   "bc_y>>h_y_Bc_DPSfrac_pythia_all",cuts_Bc_all+Bc_fid);
    pythia_tree->Draw(   "bc_y>>h_y_Bc_DPSfrac_pythia_DPS",cuts_Bc_all+Bc_fid+cuts_DPS);
    
    h_y_Bc_DPSfrac_pythia_all->Scale(scalefactor_pythia);
    h_y_Bc_DPSfrac_pythia_DPS->Scale(scalefactor_pythia);

    h_y_pythia_DPSfrac->Divide(      h_y_Bc_DPSfrac_pythia_DPS,h_y_Bc_DPSfrac_pythia_all);
    h_y_pythia_DPSfrac->Scale(100);

    h_y_pythia_DPSfrac->SetTitle(";#it{y};f_{DPS} (%)");
    h_y_pythia_DPSfrac->SetMinimum(0);
    h_y_pythia_DPSfrac->SetMaximum(100);
    h_y_pythia_DPSfrac->Draw("");

    c_y_DPSfrac->Print("plots_kinematics_Bc_to_Bu/c_y_DPSfrac.pdf");
    // -------------------------------------------------------------------------
    // Plot pT ratio
    // -------------------------------------------------------------------------

    TCanvas* c_pt_ratio = new TCanvas("c_pt_ratio","c_pt_ratio");


    TH1D* h_pt_Bc_forratio_pythia_all  = new TH1D("h_pt_Bc_forratio_pythia_all", "h_pt_Bc_forratio_pythia_all", 20,0,20);
    TH1D* h_pt_Bc_forratio_pythia_SPS  = new TH1D("h_pt_Bc_forratio_pythia_SPS", "h_pt_Bc_forratio_pythia_SPS", 20,0,20);
    TH1D* h_pt_Bc_forratio_pythia_DPS  = new TH1D("h_pt_Bc_forratio_pythia_DPS", "h_pt_Bc_forratio_pythia_DPS", 20,0,20);
    TH1D* h_pt_Bc_forratio_bcvegpy     = new TH1D("h_pt_Bc_forratio_bcvegpy",    "h_pt_Bc_forratio_bcvegpy"  ,  20,0,20);
    TH1D* h_pt_Bu_forratio_pythia      = new TH1D("h_pt_Bu_forratio_pythia",     "h_pt_Bu_forratio_pythia"  ,   20,0,20);

    TH1D* h_pt_pythia_all_ratio        = new TH1D("h_pt_pythia_all_ratio",       "h_pt_pythia_all_ratio",       20,0,20);
    TH1D* h_pt_pythia_DPS_ratio        = new TH1D("h_pt_pythia_DPS_ratio",       "h_pt_pythia_DPS_ratio",       20,0,20);
    TH1D* h_pt_pythia_SPS_ratio        = new TH1D("h_pt_pythia_SPS_ratio",       "h_pt_pythia_SPS_ratio",       20,0,20);
    TH1D* h_pt_bcvegpy_ratio           = new TH1D("h_pt_bcvegpy_ratio",          "h_pt_bcvegpy_ratio",          20,0,20);

    pythia_tree->Draw(   "bc_pt>>h_pt_Bc_forratio_pythia_all",cuts_Bc_all+Bc_fid);
    pythia_tree->Draw(   "bc_pt>>h_pt_Bc_forratio_pythia_DPS",cuts_Bc_all+Bc_fid+cuts_DPS);
    pythia_tree->Draw(   "bc_pt>>h_pt_Bc_forratio_pythia_SPS",cuts_Bc_all+Bc_fid+cuts_SPS);
    bcvegpy_tree->Draw(  "bc_pt>>h_pt_Bc_forratio_bcvegpy",   cuts_Bc_all+Bc_fid);
    pythia_Bu_tree->Draw("Bu_p_pt>>h_pt_Bu_forratio_pythia",  cuts_Bu_all+Bu_fid);

    h_pt_Bc_forratio_bcvegpy->Scale(   scalefactor_bcvegpy);
    h_pt_Bc_forratio_pythia_all->Scale(scalefactor_pythia);
    h_pt_Bc_forratio_pythia_DPS->Scale(scalefactor_pythia);
    h_pt_Bc_forratio_pythia_SPS->Scale(scalefactor_pythia);
    h_pt_Bu_forratio_pythia->Scale(    scalefactor_pythia_Bu);

    h_pt_pythia_all_ratio->Divide(      h_pt_Bc_forratio_pythia_all,h_pt_Bu_forratio_pythia);
    h_pt_pythia_DPS_ratio->Divide(      h_pt_Bc_forratio_pythia_DPS,h_pt_Bu_forratio_pythia);
    h_pt_pythia_SPS_ratio->Divide(      h_pt_Bc_forratio_pythia_SPS,h_pt_Bu_forratio_pythia);
    h_pt_bcvegpy_ratio->Divide(         h_pt_Bc_forratio_bcvegpy,   h_pt_Bu_forratio_pythia);

    h_pt_pythia_DPS_ratio->SetLineColor(kBlue);
    h_pt_pythia_DPS_ratio->SetMarkerColor(kBlue);

    h_pt_pythia_SPS_ratio->SetLineColor(kRed);
    h_pt_pythia_SPS_ratio->SetMarkerColor(kRed);

    h_pt_bcvegpy_ratio->SetMarkerColor(kGreen+2);
    h_pt_bcvegpy_ratio->SetLineColor(kGreen+2);

    h_pt_pythia_all_ratio->Scale(100*theory_Br_Bc2JpsiPi/theory_Br_Bu2JpsiK);
    h_pt_pythia_DPS_ratio->Scale(100*theory_Br_Bc2JpsiPi/theory_Br_Bu2JpsiK);
    h_pt_pythia_SPS_ratio->Scale(100*theory_Br_Bc2JpsiPi/theory_Br_Bu2JpsiK);
    h_pt_bcvegpy_ratio->Scale(   100*theory_Br_Bc2JpsiPi/theory_Br_Bu2JpsiK);


    // h_pt_pythia_all_ratio->Scale(1e3);
    // h_pt_pythia_DPS_ratio->Scale(1e3);
    // h_pt_pythia_SPS_ratio->Scale(1e3);
    // h_pt_bcvegpy_ratio->Scale(   1e3);

    
    h_pt_pythia_all_ratio->SetMaximum(1.05*max({h_pt_pythia_all_ratio->GetMaximum(),
                                                  h_pt_pythia_DPS_ratio->GetMaximum(),
                                                  h_pt_pythia_SPS_ratio->GetMaximum(),
                                                  h_pt_bcvegpy_ratio->GetMaximum()
                                      }));
    h_pt_pythia_all_ratio->SetTitle(";#it{p}_{T} [GeV/#it{c}];R(#it{p}_{T}) (%)");
    // h_pt_pythia_all_ratio->SetTitle(";#it{p}_{T} [GeV/#it{c}];f_{c}/f_{u} (x10^{-3})");
    h_pt_pythia_all_ratio->SetMinimum(0);
    h_pt_pythia_all_ratio->SetMaximum(1.8);
    h_pt_pythia_all_ratio->Draw("][hist");
    h_pt_pythia_DPS_ratio->Draw("][hist same");
    h_pt_pythia_SPS_ratio->Draw("][hist same");
    h_pt_bcvegpy_ratio->Draw("][hist same");
    h_pt_theory_ratio->Draw("same");


    TLegend* leg_pt_ratio = new TLegend(0.2,0.6,0.6,0.9);
    leg_pt_ratio->AddEntry(h_pt_pythia_all_ratio,"Pythia - All","l");
    leg_pt_ratio->AddEntry(h_pt_pythia_DPS_ratio,"Pythia - Just DPS","l");
    leg_pt_ratio->AddEntry(h_pt_pythia_SPS_ratio,"Pythia - Just SPS","l");
    leg_pt_ratio->AddEntry(h_pt_bcvegpy_ratio,"BcVegPy+Pythia","l");
    leg_pt_ratio->AddEntry(h_pt_theory_ratio,"BcVegPy theory","pe");
    leg_pt_ratio->SetTextFont(132);
    leg_pt_ratio->SetFillStyle(0);
    leg_pt_ratio->Draw();

    c_pt_ratio->Print("plots_kinematics_Bc_to_Bu/c_pt_ratio.pdf");


    // -------------------------------------------------------------------------
    // Plot fc/fu == sigma(Bc+)/sigma(B+) 
    // -------------------------------------------------------------------------

    TCanvas* c_pt_fc_fu = new TCanvas("c_pt_fc_fu","c_pt_fc_fu");

    TH1D* h_pt_Bu_fc_fu_theory_ratio = new TH1D("h_pt_Bu_fc_fu_theory_ratio","h_pt_Bu_fc_fu_theory_ratio",20,0,20);
    h_pt_Bu_fc_fu_theory_ratio->Divide(h_pt_theory_Bc,h_pt_theory_Bu);

    h_pt_Bu_fc_fu_theory_ratio->Scale((crosssection_Bcp_inacc/(crosssection_Bup_inacc)));

    TH1D* h_pt_Bc_fc_fu_pythia_all  = new TH1D("h_pt_Bc_fc_fu_pythia_all", "h_pt_Bc_fc_fu_pythia_all", 20,0,20);
    TH1D* h_pt_Bc_fc_fu_pythia_SPS  = new TH1D("h_pt_Bc_fc_fu_pythia_SPS", "h_pt_Bc_fc_fu_pythia_SPS", 20,0,20);
    TH1D* h_pt_Bc_fc_fu_pythia_DPS  = new TH1D("h_pt_Bc_fc_fu_pythia_DPS", "h_pt_Bc_fc_fu_pythia_DPS", 20,0,20);
    TH1D* h_pt_Bc_fc_fu_bcvegpy     = new TH1D("h_pt_Bc_fc_fu_bcvegpy",    "h_pt_Bc_fc_fu_bcvegpy"  ,  20,0,20);
    TH1D* h_pt_Bu_fc_fu_pythia      = new TH1D("h_pt_Bu_fc_fu_pythia",     "h_pt_Bu_fc_fu_pythia"  ,   20,0,20);

    TH1D* h_pt_fc_fu_pythia_all_ratio        = new TH1D("h_pt_fc_fu_pythia_all_ratio",       "h_pt_fc_fu_pythia_all_ratio",       20,0,20);
    TH1D* h_pt_fc_fu_pythia_DPS_ratio        = new TH1D("h_pt_fc_fu_pythia_DPS_ratio",       "h_pt_fc_fu_pythia_DPS_ratio",       20,0,20);
    TH1D* h_pt_fc_fu_pythia_SPS_ratio        = new TH1D("h_pt_fc_fu_pythia_SPS_ratio",       "h_pt_fc_fu_pythia_SPS_ratio",       20,0,20);
    TH1D* h_pt_fc_fu_bcvegpy_ratio           = new TH1D("h_pt_fc_fu_bcvegpy_ratio",          "h_pt_fc_fu_bcvegpy_ratio",          20,0,20);

    pythia_tree->Draw(   "bc_pt>>h_pt_Bc_fc_fu_pythia_all",cuts_Bc_all+Bc_fid);
    pythia_tree->Draw(   "bc_pt>>h_pt_Bc_fc_fu_pythia_DPS",cuts_Bc_all+Bc_fid+cuts_DPS);
    pythia_tree->Draw(   "bc_pt>>h_pt_Bc_fc_fu_pythia_SPS",cuts_Bc_all+Bc_fid+cuts_SPS);
    bcvegpy_tree->Draw(  "bc_pt>>h_pt_Bc_fc_fu_bcvegpy",   cuts_Bc_all+Bc_fid);
    pythia_Bu_tree->Draw("Bu_p_pt>>h_pt_Bu_fc_fu_pythia",  cuts_Bu_all+Bu_fid);

    h_pt_Bc_fc_fu_bcvegpy->Scale(   scalefactor_bcvegpy);
    h_pt_Bc_fc_fu_pythia_all->Scale(scalefactor_pythia);
    h_pt_Bc_fc_fu_pythia_DPS->Scale(scalefactor_pythia);
    h_pt_Bc_fc_fu_pythia_SPS->Scale(scalefactor_pythia);
    h_pt_Bu_fc_fu_pythia->Scale(    scalefactor_pythia_Bu);

    h_pt_fc_fu_pythia_all_ratio->Divide(      h_pt_Bc_fc_fu_pythia_all,h_pt_Bu_fc_fu_pythia);
    h_pt_fc_fu_pythia_DPS_ratio->Divide(      h_pt_Bc_fc_fu_pythia_DPS,h_pt_Bu_fc_fu_pythia);
    h_pt_fc_fu_pythia_SPS_ratio->Divide(      h_pt_Bc_fc_fu_pythia_SPS,h_pt_Bu_fc_fu_pythia);
    h_pt_fc_fu_bcvegpy_ratio->Divide(         h_pt_Bc_fc_fu_bcvegpy,   h_pt_Bu_fc_fu_pythia);

    h_pt_fc_fu_pythia_DPS_ratio->SetLineColor(kBlue);
    h_pt_fc_fu_pythia_DPS_ratio->SetMarkerColor(kBlue);

    h_pt_fc_fu_pythia_SPS_ratio->SetLineColor(kRed);
    h_pt_fc_fu_pythia_SPS_ratio->SetMarkerColor(kRed);

    h_pt_fc_fu_bcvegpy_ratio->SetMarkerColor(kGreen+2);
    h_pt_fc_fu_bcvegpy_ratio->SetLineColor(kGreen+2);

    h_pt_fc_fu_pythia_all_ratio->Scale(1000);
    h_pt_fc_fu_pythia_DPS_ratio->Scale(1000);
    h_pt_fc_fu_pythia_SPS_ratio->Scale(1000);
    h_pt_fc_fu_bcvegpy_ratio->Scale(   1000);
    h_pt_Bu_fc_fu_theory_ratio->Scale( 1000);


    
    h_pt_fc_fu_pythia_all_ratio->SetMaximum(1.05*max({h_pt_fc_fu_pythia_all_ratio->GetMaximum(),
                                                  h_pt_fc_fu_pythia_DPS_ratio->GetMaximum(),
                                                  h_pt_fc_fu_pythia_SPS_ratio->GetMaximum(),
                                                  h_pt_fc_fu_bcvegpy_ratio->GetMaximum()
                                      }));
    // h_pt_fc_fu_pythia_all_ratio->SetTitle(";#it{p}_{T} [GeV/#it{c}];R(#it{p}_{T}) (%)");
    h_pt_fc_fu_pythia_all_ratio->SetTitle(";#it{p}_{T} [GeV/#it{c}];f_{c}/f_{u} (#times10^{-3})");
    h_pt_fc_fu_pythia_all_ratio->SetMinimum(0);
    h_pt_fc_fu_pythia_all_ratio->SetMaximum(16.0);
    h_pt_fc_fu_pythia_all_ratio->Draw("][hist");
    h_pt_fc_fu_pythia_DPS_ratio->Draw("][hist same");
    h_pt_fc_fu_pythia_SPS_ratio->Draw("][hist same");
    h_pt_fc_fu_bcvegpy_ratio->Draw("][hist same");
    h_pt_Bu_fc_fu_theory_ratio->Draw("same");


    TF1* f_lhcb_fc_fupfd_jpsimunu_low = new TF1("f_lhcb_fc_fupfd_jpsimunu_low","(1.95/1.4)*2*([0]+[1]*(x-7.2))",0,20);
    f_lhcb_fc_fupfd_jpsimunu_low->SetParameter(0, 4.13);  
    f_lhcb_fc_fupfd_jpsimunu_low->SetParameter(1,-9.7e-2);
    f_lhcb_fc_fupfd_jpsimunu_low->SetLineStyle(kDotted);
    f_lhcb_fc_fupfd_jpsimunu_low->SetLineColor(kMagenta);
    f_lhcb_fc_fupfd_jpsimunu_low->Draw("same");
    
    TF1* f_lhcb_fc_fupfd_jpsimunu_mid = new TF1("f_lhcb_fc_fupfd_jpsimunu_mid","(1.95/1.95)*2*([0]+[1]*(x-7.2))",0,20);
    f_lhcb_fc_fupfd_jpsimunu_mid->SetParameter(0, 4.13);  
    f_lhcb_fc_fupfd_jpsimunu_mid->SetParameter(1,-9.7e-2);
    f_lhcb_fc_fupfd_jpsimunu_mid->SetLineColor(kMagenta);
    f_lhcb_fc_fupfd_jpsimunu_mid->Draw("same");
    
    TF1* f_lhcb_fc_fupfd_jpsimunu_high = new TF1("f_lhcb_fc_fupfd_jpsimunu_high","(1.95/7.5)*2*([0]+[1]*(x-7.2))",0,20);
    f_lhcb_fc_fupfd_jpsimunu_high->SetParameter(0, 4.13);  
    f_lhcb_fc_fupfd_jpsimunu_high->SetParameter(1,-9.7e-2);
    f_lhcb_fc_fupfd_jpsimunu_high->SetLineStyle(kDashed);
    f_lhcb_fc_fupfd_jpsimunu_high->SetLineColor(kMagenta);
    f_lhcb_fc_fupfd_jpsimunu_high->Draw("same");

    TF1* f_lhcb_fc_fupfd_jpsipi_low = new TF1("f_lhcb_fc_fupfd_jpsipi_low","10*0.683*1.02/0.64",0,20);
    f_lhcb_fc_fupfd_jpsipi_low->SetParameter(0, 4.13);  
    f_lhcb_fc_fupfd_jpsipi_low->SetParameter(1,-9.7e-2);
    f_lhcb_fc_fupfd_jpsipi_low->SetLineColor(kOrange);
    f_lhcb_fc_fupfd_jpsipi_low->Draw("same");
    
    TF1* f_lhcb_fc_fupfd_jpsipi_high = new TF1("f_lhcb_fc_fupfd_jpsipi_high","10*0.683*1.02/3.3",0,20);
    f_lhcb_fc_fupfd_jpsipi_high->SetParameter(0, 4.13);  
    f_lhcb_fc_fupfd_jpsipi_high->SetParameter(1,-9.7e-2);
    f_lhcb_fc_fupfd_jpsipi_high->SetLineStyle(kDashed);
    f_lhcb_fc_fupfd_jpsipi_high->SetLineColor(kOrange);
    f_lhcb_fc_fupfd_jpsipi_high->Draw("same");

    TLegend* leg_pt_fc_fu_ratio = new TLegend(0.18,0.34,0.66,0.72);
    leg_pt_fc_fu_ratio->AddEntry(h_pt_fc_fu_pythia_all_ratio,"Pythia - All","l");
    leg_pt_fc_fu_ratio->AddEntry(h_pt_fc_fu_pythia_DPS_ratio,"Pythia - Just DPS","l");
    leg_pt_fc_fu_ratio->AddEntry(h_pt_fc_fu_pythia_SPS_ratio,"Pythia - Just SPS","l");
    leg_pt_fc_fu_ratio->AddEntry(h_pt_bcvegpy_ratio,         "BcVegPy+Pythia","l");
    leg_pt_fc_fu_ratio->AddEntry(h_pt_Bu_fc_fu_theory_ratio, "BcVegPy+FONLL theory","pe");
    leg_pt_fc_fu_ratio->AddEntry(f_lhcb_fc_fupfd_jpsimunu_low, "PAPER-2019-033 + B(B_{c}^{+}#rightarrowJ/#psi#mu#nu) = 1.4%","l");
    leg_pt_fc_fu_ratio->AddEntry(f_lhcb_fc_fupfd_jpsimunu_mid, "PAPER-2019-033 + B(B_{c}^{+}#rightarrowJ/#psi#mu#nu) = 1.95%","l");
    leg_pt_fc_fu_ratio->AddEntry(f_lhcb_fc_fupfd_jpsimunu_high,"PAPER-2019-033 + B(B_{c}^{+}#rightarrowJ/#psi#mu#nu) = 7.5%","l");
    leg_pt_fc_fu_ratio->AddEntry(f_lhcb_fc_fupfd_jpsipi_low,   "PAPER-2014-050 + B(B_{c}^{+}#rightarrowJ/#psi#pi)   = 0.064% (AVG)","l");
    leg_pt_fc_fu_ratio->AddEntry(f_lhcb_fc_fupfd_jpsipi_high,  "PAPER-2014-050 + B(B_{c}^{+}#rightarrowJ/#psi#pi)   = 0.33% (AVG)","l");
    leg_pt_fc_fu_ratio->SetTextFont(132);
    leg_pt_fc_fu_ratio->SetFillStyle(0);
    leg_pt_fc_fu_ratio->Draw();

    c_pt_fc_fu->Print("plots_kinematics_Bc_to_Bu/c_pt_fc_fu.pdf");

    // -------------------------------------------------------------------------
    // Plot 2D pT vs. y 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_y_pythia_DPS = new TCanvas("c_2D_pt_y_pythia_DPS","c_2D_pt_y_pythia_DPS");
    TH2D* h_2D_pt_y_pythia_DPS   = new TH2D("h_2D_pt_y_pythia_DPS",  "h_2D_pt_y_pythia_DPS",  50,-10,10,50,0,20);
    TH2D* h_2D_pt_y_pythia_SPS   = new TH2D("h_2D_pt_y_pythia_SPS",  "h_2D_pt_y_pythia_SPS",  50,-10,10,50,0,20);
    TH2D* h_2D_pt_y_bcvegpy      = new TH2D("h_2D_pt_y_bcvegpy",     "h_2D_pt_y_bcvegpy",     50,-10,10,50,0,20);

    pythia_tree->Draw("bc_pt:bc_y>>h_2D_pt_y_pythia_DPS",cuts_Bc_all+cuts_DPS);
    pythia_tree->Draw("bc_pt:bc_y>>h_2D_pt_y_pythia_SPS",cuts_Bc_all+cuts_SPS);
    bcvegpy_tree->Draw("bc_pt:bc_y>>h_2D_pt_y_bcvegpy",  cuts_Bc_all);

    h_2D_pt_y_bcvegpy->Scale(scalefactor_bcvegpy);
    h_2D_pt_y_pythia_DPS->Scale(scalefactor_pythia);
    h_2D_pt_y_pythia_SPS->Scale(scalefactor_pythia);

    double maximum_2D_pt_y = max( {
                                    h_2D_pt_y_bcvegpy->GetMaximum(),
                                    h_2D_pt_y_pythia_DPS->GetMaximum(),
                                    h_2D_pt_y_pythia_SPS->GetMaximum(),
                                });

    h_2D_pt_y_pythia_DPS->SetMaximum(maximum_2D_pt_y);
    h_2D_pt_y_pythia_SPS->SetMaximum(maximum_2D_pt_y);
    h_2D_pt_y_bcvegpy->SetMaximum(maximum_2D_pt_y);

    h_2D_pt_y_pythia_DPS->SetTitle(";#it{y};#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_pythia_DPS->Draw("colz");
    gPad->SetLogz();
    
    c_2D_pt_y_pythia_DPS->Print("plots_kinematics/c_2D_pt_y_pythia_DPS.pdf");
    

    TCanvas* c_2D_pt_y_pythia_SPS = new TCanvas("c_2D_pt_y_pythia_SPS","c_2D_pt_y_pythia_SPS");

    h_2D_pt_y_pythia_SPS->SetTitle(";#it{y};#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_pythia_SPS->Draw("colz");
    gPad->SetLogz();
    
    c_2D_pt_y_pythia_SPS->Print("plots_kinematics/c_2D_pt_y_pythia_SPS.pdf");
    

    TCanvas* c_2D_pt_y_bcvegpy = new TCanvas("c_2D_pt_y_bcvegpy","c_2D_pt_y_bcvegpy");

    h_2D_pt_y_bcvegpy->SetTitle(";#it{y};#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_bcvegpy->Draw("colz");
    gPad->SetLogz();
    
    c_2D_pt_y_bcvegpy->Print("plots_kinematics/c_2D_pt_y_bcvegpy.pdf");
    
    SetLHCbStyle("oneD");
    // -------------------------------------------------------------------------
    // Plot y of just Bc
    // -------------------------------------------------------------------------

    TCanvas* c_y_Bc = new TCanvas("c_y_Bc","c_y_Bc");
    TH1D* h_y_Bc_pythia_all  = new TH1D("h_y_Bc_pythia_all", "h_y_Bc_pythia_all", 50,-7.5,12.0);
    TH1D* h_y_Bc_pythia_SPS  = new TH1D("h_y_Bc_pythia_SPS", "h_y_Bc_pythia_SPS", 50,-7.5,12.0);
    TH1D* h_y_Bc_pythia_DPS  = new TH1D("h_y_Bc_pythia_DPS", "h_y_Bc_pythia_DPS", 50,-7.5,12.0);
    TH1D* h_y_Bc_bcvegpy     = new TH1D("h_y_Bc_bcvegpy",    "h_y_Bc_bcvegpy"  ,  50,-7.5,12.0);

    pythia_tree->Draw("bc_y>>h_y_Bc_pythia_all",cuts_Bc_all);
    pythia_tree->Draw("bc_y>>h_y_Bc_pythia_DPS",cuts_Bc_all+cuts_DPS);
    pythia_tree->Draw("bc_y>>h_y_Bc_pythia_SPS",cuts_Bc_all+cuts_SPS);
    bcvegpy_tree->Draw("bc_y>>h_y_Bc_bcvegpy",  cuts_Bc_all);


    h_y_Bc_bcvegpy->Scale(scalefactor_bcvegpy);
    h_y_Bc_pythia_all->Scale(scalefactor_pythia);
    h_y_Bc_pythia_DPS->Scale(scalefactor_pythia);
    h_y_Bc_pythia_SPS->Scale(scalefactor_pythia);

    // Black -> All
    h_y_Bc_pythia_all->SetLineColor(kBlack);

    // Red -> just SPS
    h_y_Bc_pythia_SPS->SetLineColor(kRed);

    // Blue -> just DPS
    h_y_Bc_pythia_DPS->SetLineColor(kBlue);

    // Green -> BcVegPy
    h_y_Bc_bcvegpy->SetLineColor(kGreen+2);


    h_y_Bc_pythia_all->SetMaximum(1.05*max({h_y_Bc_pythia_all->GetMaximum(),
                                      h_y_Bc_bcvegpy->GetMaximum(),
                                      h_y_Bc_pythia_SPS->GetMaximum(),
                                      h_y_Bc_pythia_DPS->GetMaximum()
                                      }));
    
    double h_y_Bc_pythia_all_bin_width = h_y_Bc_pythia_all->GetXaxis()->GetBinWidth(2);
    h_y_Bc_pythia_all->SetTitle(Form(";#it{y}; d#sigma(B^{+}_{c})/d#it{y} [%.1f #mub]",h_y_Bc_pythia_all_bin_width));
    h_y_Bc_pythia_all->Draw("hist");
    h_y_Bc_pythia_SPS->Draw("hist same");
    h_y_Bc_pythia_DPS->Draw("hist same");
    h_y_Bc_bcvegpy->Draw("same hist");
    // gPad->SetLogy();

    TLegend* leg_y_Bc = new TLegend(0.55,0.6,0.93,0.9);
    leg_y_Bc->AddEntry(h_y_Bc_pythia_all,         "Pythia - All","l");
    leg_y_Bc->AddEntry(h_y_Bc_pythia_SPS,         "Pythia - Just SPS","l");
    leg_y_Bc->AddEntry(h_y_Bc_pythia_DPS,         "Pythia - Just DPS","l");
    leg_y_Bc->AddEntry(h_y_Bc_bcvegpy,            "BcVegPy","l");
    leg_y_Bc->SetTextFont(132);
    leg_y_Bc->SetFillStyle(0);
    leg_y_Bc->Draw();
    c_y_Bc->Print("plots_kinematics_Bc_to_Bu/c_y_Bc.pdf");



    // -------------------------------------------------------------------------
    // Plot multiplicity in MPI file
    // -------------------------------------------------------------------------
    TCanvas* c_multiplicity_cats = new TCanvas("c_multiplicity_cats","c_multiplicity_cats");
    TH1D* h_multiplicity_cats_pythia  = new TH1D("h_multiplicity_cats_pythia", "h_multiplicity_cats_pythia", 16,0,80);
    TH1D* h_multiplicity_cats_bcvegpy = new TH1D("h_multiplicity_cats_bcvegpy","h_multiplicity_cats_bcvegpy",16,0,80);

    TH1D* h_multiplicity_cats_pythia_DPS  = new TH1D("h_multiplicity_cats_pythia_DPS", "h_multiplicity_cats_pythia_DPS", 16,0,80);
    TH1D* h_multiplicity_cats_pythia_SPS  = new TH1D("h_multiplicity_cats_pythia_SPS", "h_multiplicity_cats_pythia_SPS", 16,0,80);

    TH1D* h_multiplicity_cats_pythia_Bu  = new TH1D("h_multiplicity_cats_pythia_Bu", "h_multiplicity_cats_pythia_Bu", 16,0,80);


    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_cats_pythia",  cuts_Bc_all+Bc_fid);
    bcvegpy_tree->Draw("nChargedInLHCb>>h_multiplicity_cats_bcvegpy",cuts_Bc_all+Bc_fid);

    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_cats_pythia_DPS",  cuts_Bc_all+Bc_fid+cuts_DPS);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_cats_pythia_SPS",  cuts_Bc_all+Bc_fid+cuts_SPS);

    pythia_Bu_tree->Draw("nChargedInLHCb>>h_multiplicity_cats_pythia_Bu",cuts_Bu_all+Bu_fid);



    h_multiplicity_cats_bcvegpy->Scale(scalefactor_bcvegpy);
    h_multiplicity_cats_pythia->Scale(scalefactor_pythia);

    h_multiplicity_cats_pythia_SPS->Scale(scalefactor_pythia);
    h_multiplicity_cats_pythia_DPS->Scale(scalefactor_pythia);

    h_multiplicity_cats_pythia_Bu->Scale(scalefactor_pythia_Bu);

    // Black -> With MPI
    h_multiplicity_cats_pythia->SetLineColor(kBlack);

    h_multiplicity_cats_pythia_Bu->SetLineColor(kBlack);
    // h_multiplicity_cats_pythia_Bu->SetLineStyle(kDashed);

    // Blue -> DPS
    h_multiplicity_cats_pythia_DPS->SetLineColor(kBlue);
    // h_multiplicity_cats_pythia_DPS->SetLineStyle(kDashed);
    
    // Red -> SPS
    h_multiplicity_cats_pythia_SPS->SetLineColor(kRed);
    // h_multiplicity_cats_pythia_SPS->SetLineStyle(kDashed);

    // Green -> BcVegPy
    h_multiplicity_cats_bcvegpy->SetLineColor(kGreen+2);

    h_multiplicity_cats_pythia->SetMaximum(1.05*max({h_multiplicity_cats_pythia->GetMaximum(),
                                      h_multiplicity_cats_pythia_DPS->GetMaximum(),
                                      h_multiplicity_cats_pythia_SPS->GetMaximum(),
                                      h_multiplicity_cats_bcvegpy->GetMaximum()
                                      // ,
                                      // h_multiplicity_cats_pythia_Bu->GetMaximum()
                                      }));
    
    double h_multiplicity_cats_pythia_bin_width = h_multiplicity_cats_pythia->GetXaxis()->GetBinWidth(2);
    h_multiplicity_cats_pythia->SetTitle(Form(";N_{Charged}^{2.0<#eta<4.5}; d#sigma/dN [%.1f #mub]",h_multiplicity_cats_pythia_bin_width));
    h_multiplicity_cats_pythia->Draw("hist");
    h_multiplicity_cats_pythia_DPS->Draw("same hist");
    h_multiplicity_cats_pythia_SPS->Draw("same hist");
    h_multiplicity_cats_bcvegpy->Draw("same hist");
    // h_multiplicity_cats_pythia_Bu->Draw("same hist");
    // gPad->SetLogy();
    h_multiplicity_cats_pythia->SetMaximum(0.13);


    std::cout << "Mean Pythia:     " << h_multiplicity_cats_pythia->GetMean() << std::endl;
    std::cout << "Mean Pythia DPS: " << h_multiplicity_cats_pythia_DPS->GetMean() << std::endl;
    std::cout << "Mean Pythia SPS: " << h_multiplicity_cats_pythia_SPS->GetMean() << std::endl;
    std::cout << "Mean BcVegPy:    " << h_multiplicity_cats_bcvegpy->GetMean() << std::endl;
    TLegend* leg_multiplicity_cats = new TLegend(0.19,0.56,0.59,0.9);
    // leg_multiplicity_cats->AddEntry(h_multiplicity_cats_pythia_Bu, "Bu Pythia","l");
    leg_multiplicity_cats->AddEntry(h_multiplicity_cats_pythia,    "Pythia - All","l");
    leg_multiplicity_cats->AddEntry(h_multiplicity_cats_pythia_DPS,"Pythia - Just DPS","l");
    leg_multiplicity_cats->AddEntry(h_multiplicity_cats_pythia_SPS,"Pythia - Just SPS","l");
    leg_multiplicity_cats->AddEntry(h_multiplicity_cats_bcvegpy,   "BcVegPy","l");
    leg_multiplicity_cats->SetTextFont(132);
    leg_multiplicity_cats->SetFillStyle(0);
    leg_multiplicity_cats->Draw();
    c_multiplicity_cats->Print("plots_kinematics_Bc_to_Bu/c_multiplicity_cats.pdf");

    TCanvas* c_multiplicity_cats_ratio = new TCanvas("c_multiplicity_cats_ratio","c_multiplicity_cats_ratio");

    TH1D* h_multiplicity_cats_pythia_ratio  = new TH1D("h_multiplicity_cats_pythia_ratio", "h_multiplicity_cats_pythia_ratio", 16,0,80);
    TH1D* h_multiplicity_cats_bcvegpy_ratio = new TH1D("h_multiplicity_cats_bcvegpy_ratio","h_multiplicity_cats_bcvegpy_ratio",16,0,80);

    TH1D* h_multiplicity_cats_pythia_DPS_ratio  = new TH1D("h_multiplicity_cats_pythia_DPS_ratio", "h_multiplicity_cats_pythia_DPS_ratio", 16,0,80);
    TH1D* h_multiplicity_cats_pythia_SPS_ratio  = new TH1D("h_multiplicity_cats_pythia_SPS_ratio", "h_multiplicity_cats_pythia_ratio", 16,0,80);

    h_multiplicity_cats_pythia_ratio->Divide(    h_multiplicity_cats_pythia,    h_multiplicity_cats_pythia_Bu);
    h_multiplicity_cats_pythia_DPS_ratio->Divide(h_multiplicity_cats_pythia_DPS,h_multiplicity_cats_pythia_Bu);
    h_multiplicity_cats_pythia_SPS_ratio->Divide(h_multiplicity_cats_pythia_SPS,h_multiplicity_cats_pythia_Bu);
    h_multiplicity_cats_bcvegpy_ratio->Divide(   h_multiplicity_cats_bcvegpy,   h_multiplicity_cats_pythia_Bu);


    h_multiplicity_cats_pythia_DPS_ratio->SetLineColor(kBlue);
    h_multiplicity_cats_pythia_DPS_ratio->SetMarkerColor(kBlue);
    h_multiplicity_cats_pythia_SPS_ratio->SetLineColor(kRed);
    h_multiplicity_cats_pythia_SPS_ratio->SetMarkerColor(kRed);
    h_multiplicity_cats_bcvegpy_ratio->SetMarkerColor(kGreen+2);
    h_multiplicity_cats_bcvegpy_ratio->SetLineColor(kGreen+2);


    h_multiplicity_cats_pythia_ratio->Scale(100);
    h_multiplicity_cats_pythia_DPS_ratio->Scale(100);
    h_multiplicity_cats_pythia_SPS_ratio->Scale(100);
    h_multiplicity_cats_bcvegpy_ratio->Scale(100);
    
    h_multiplicity_cats_pythia_ratio->SetMaximum(1.05*max({
                                                           h_multiplicity_cats_pythia_ratio->GetMaximum(),
                                                           h_multiplicity_cats_pythia_DPS_ratio->GetMaximum(),
                                                           h_multiplicity_cats_pythia_SPS_ratio->GetMaximum(),
                                                           h_multiplicity_cats_bcvegpy_ratio->GetMaximum()
                                      }));
    h_multiplicity_cats_pythia_ratio->SetTitle(";N_{Charged}^{2.0<#eta<4.5};#scale[0.6]{#frac{d#sigma(B_{c}^{+})}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_multiplicity_cats_pythia_ratio->SetMinimum(0);
    h_multiplicity_cats_pythia_ratio->SetMaximum(0.5);
    h_multiplicity_cats_pythia_ratio->Draw();
    h_multiplicity_cats_pythia_DPS_ratio->Draw("same");
    h_multiplicity_cats_pythia_SPS_ratio->Draw("same");
    h_multiplicity_cats_bcvegpy_ratio->Draw("same");


    TLegend* leg_multiplicity_cats_ratio = new TLegend(0.2,0.5,0.5,0.9);
    leg_multiplicity_cats_ratio->AddEntry(h_multiplicity_cats_pythia_ratio,"Pythia - All","pe");
    leg_multiplicity_cats_ratio->AddEntry(h_multiplicity_cats_pythia_DPS_ratio,"Pythia - Just DPS","pe");
    leg_multiplicity_cats_ratio->AddEntry(h_multiplicity_cats_pythia_SPS_ratio,"Pythia - Just SPS","pe");
    leg_multiplicity_cats_ratio->AddEntry(h_multiplicity_cats_bcvegpy_ratio,"BcVegPy","pe");
    leg_multiplicity_cats_ratio->SetTextFont(132);
    leg_multiplicity_cats_ratio->SetFillStyle(0);
    leg_multiplicity_cats_ratio->Draw();

    c_multiplicity_cats_ratio->Print("plots_kinematics_Bc_to_Bu/c_multiplicity_cats_ratio.pdf");



    // -------------------------------------------------------------------------
    // Plot nMPI in MPI file
    // -------------------------------------------------------------------------
    TCanvas* c_nMPI_cats = new TCanvas("c_nMPI_cats","c_nMPI_cats");
    TH1D* h_nMPI_cats_pythia  = new TH1D("h_nMPI_cats_pythia", "h_nMPI_cats_pythia", 13,0,26);
    TH1D* h_nMPI_cats_bcvegpy = new TH1D("h_nMPI_cats_bcvegpy","h_nMPI_cats_bcvegpy",13,0,26);

    TH1D* h_nMPI_cats_pythia_DPS  = new TH1D("h_nMPI_cats_pythia_DPS", "h_nMPI_cats_pythia_DPS", 13,0,26);
    TH1D* h_nMPI_cats_pythia_SPS  = new TH1D("h_nMPI_cats_pythia_SPS", "h_nMPI_cats_pythia_SPS", 13,0,26);

    TH1D* h_nMPI_cats_pythia_Bu  = new TH1D("h_nMPI_cats_pythia_Bu", "h_nMPI_cats_pythia_Bu", 13,0,26);


    pythia_tree->Draw("nMPI>>h_nMPI_cats_pythia",  cuts_Bc_all+Bc_fid);
    bcvegpy_tree->Draw("nMPI>>h_nMPI_cats_bcvegpy",cuts_Bc_all+Bc_fid);

    pythia_tree->Draw("nMPI>>h_nMPI_cats_pythia_DPS",  cuts_Bc_all+Bc_fid+cuts_DPS);
    pythia_tree->Draw("nMPI>>h_nMPI_cats_pythia_SPS",  cuts_Bc_all+Bc_fid+cuts_SPS);

    pythia_Bu_tree->Draw("nMPI>>h_nMPI_cats_pythia_Bu",cuts_Bu_all+Bu_fid);



    h_nMPI_cats_bcvegpy->Scale(scalefactor_bcvegpy);
    h_nMPI_cats_pythia->Scale(scalefactor_pythia);

    h_nMPI_cats_pythia_SPS->Scale(scalefactor_pythia);
    h_nMPI_cats_pythia_DPS->Scale(scalefactor_pythia);

    h_nMPI_cats_pythia_Bu->Scale(scalefactor_pythia_Bu);

    // Black -> With MPI
    h_nMPI_cats_pythia->SetLineColor(kBlack);

    h_nMPI_cats_pythia_Bu->SetLineColor(kBlack);
    h_nMPI_cats_pythia_Bu->SetLineStyle(kDashed);

    // Blue -> DPS
    h_nMPI_cats_pythia_DPS->SetLineColor(kBlue);
    h_nMPI_cats_pythia_DPS->SetLineStyle(kDashed);
    
    // Red -> SPS
    h_nMPI_cats_pythia_SPS->SetLineColor(kRed);
    h_nMPI_cats_pythia_SPS->SetLineStyle(kDashed);

    // Green -> BcVegPy
    h_nMPI_cats_bcvegpy->SetLineColor(kGreen+2);

    h_nMPI_cats_pythia->SetMaximum(1.05*max({h_nMPI_cats_pythia->GetMaximum(),
                                      h_nMPI_cats_pythia_DPS->GetMaximum(),
                                      h_nMPI_cats_pythia_SPS->GetMaximum(),
                                      h_nMPI_cats_bcvegpy->GetMaximum(),
                                      h_nMPI_cats_pythia_Bu->GetMaximum()
                                      }));
    
    double h_nMPI_cats_pythia_bin_width = h_nMPI_cats_pythia->GetXaxis()->GetBinWidth(2);
    h_nMPI_cats_pythia->SetTitle(Form(";N_{Charged}^{2.0<#eta<4.5}; d#sigma/dN [%.1f #mub]",h_nMPI_cats_pythia_bin_width));
    h_nMPI_cats_pythia->Draw("hist");
    h_nMPI_cats_pythia_DPS->Draw("hist");
    h_nMPI_cats_pythia_SPS->Draw("hist");
    h_nMPI_cats_bcvegpy->Draw("same hist");
    h_nMPI_cats_pythia_Bu->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_nMPI_cats = new TLegend(0.6,0.6,0.9,0.9);
    leg_nMPI_cats->AddEntry(h_nMPI_cats_pythia_Bu,"Bu Pythia With MPI","l");
    leg_nMPI_cats->AddEntry(h_nMPI_cats_pythia,"Bc Pythia With MPI","l");
    leg_nMPI_cats->AddEntry(h_nMPI_cats_pythia_DPS,"Bc DPS Pythia With MPI","l");
    leg_nMPI_cats->AddEntry(h_nMPI_cats_pythia_SPS,"Bc SPS Pythia With MPI","l");
    leg_nMPI_cats->AddEntry(h_nMPI_cats_bcvegpy,"BcVegPy","l");
    leg_nMPI_cats->SetTextFont(132);
    leg_nMPI_cats->SetFillStyle(0);
    leg_nMPI_cats->Draw();
    c_nMPI_cats->Print("plots_kinematics_Bc_to_Bu/c_nMPI_cats.pdf");

    TCanvas* c_nMPI_cats_ratio = new TCanvas("c_nMPI_cats_ratio","c_nMPI_cats_ratio");

    TH1D* h_nMPI_cats_pythia_ratio  = new TH1D("h_nMPI_cats_pythia_ratio", "h_nMPI_cats_pythia_ratio", 13,0,26);
    TH1D* h_nMPI_cats_bcvegpy_ratio = new TH1D("h_nMPI_cats_bcvegpy_ratio","h_nMPI_cats_bcvegpy_ratio",13,0,26);

    TH1D* h_nMPI_cats_pythia_DPS_ratio  = new TH1D("h_nMPI_cats_pythia_DPS_ratio", "h_nMPI_cats_pythia_DPS_ratio", 13,0,26);
    TH1D* h_nMPI_cats_pythia_SPS_ratio  = new TH1D("h_nMPI_cats_pythia_SPS_ratio", "h_nMPI_cats_pythia_ratio", 13,0,26);

    h_nMPI_cats_pythia_ratio->Divide(    h_nMPI_cats_pythia,    h_nMPI_cats_pythia_Bu);
    h_nMPI_cats_pythia_DPS_ratio->Divide(h_nMPI_cats_pythia_DPS,h_nMPI_cats_pythia_Bu);
    h_nMPI_cats_pythia_SPS_ratio->Divide(h_nMPI_cats_pythia_SPS,h_nMPI_cats_pythia_Bu);
    h_nMPI_cats_bcvegpy_ratio->Divide(   h_nMPI_cats_bcvegpy,   h_nMPI_cats_pythia_Bu);


    h_nMPI_cats_pythia_DPS_ratio->SetLineColor(kBlue);
    h_nMPI_cats_pythia_DPS_ratio->SetMarkerColor(kBlue);
    h_nMPI_cats_pythia_SPS_ratio->SetLineColor(kRed);
    h_nMPI_cats_pythia_SPS_ratio->SetMarkerColor(kRed);
    h_nMPI_cats_bcvegpy_ratio->SetMarkerColor(kGreen+2);
    h_nMPI_cats_bcvegpy_ratio->SetLineColor(kGreen+2);


    h_nMPI_cats_pythia_ratio->Scale(100);
    h_nMPI_cats_pythia_DPS_ratio->Scale(100);
    h_nMPI_cats_pythia_SPS_ratio->Scale(100);
    h_nMPI_cats_bcvegpy_ratio->Scale(100);
    
    h_nMPI_cats_pythia_ratio->SetMaximum(1.05*max({
                                                           h_nMPI_cats_pythia_ratio->GetMaximum(),
                                                           h_nMPI_cats_pythia_DPS_ratio->GetMaximum(),
                                                           h_nMPI_cats_pythia_SPS_ratio->GetMaximum(),
                                                           h_nMPI_cats_bcvegpy_ratio->GetMaximum()
                                      }));
    h_nMPI_cats_pythia_ratio->SetTitle(";N_{MPI};#scale[0.6]{#frac{d#sigma(B_{c}^{+})}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_nMPI_cats_pythia_ratio->SetMinimum(0);
    h_nMPI_cats_pythia_ratio->SetMaximum(0.8);
    h_nMPI_cats_pythia_ratio->Draw();
    h_nMPI_cats_pythia_DPS_ratio->Draw("same");
    h_nMPI_cats_pythia_SPS_ratio->Draw("same");
    h_nMPI_cats_bcvegpy_ratio->Draw("same");


    TLegend* leg_nMPI_cats_ratio = new TLegend(0.2,0.5,0.5,0.9);
    leg_nMPI_cats_ratio->AddEntry(h_nMPI_cats_pythia_ratio,"Pythia - All","pe");
    leg_nMPI_cats_ratio->AddEntry(h_nMPI_cats_pythia_DPS_ratio,"Pythia - Just DPS","pe");
    leg_nMPI_cats_ratio->AddEntry(h_nMPI_cats_pythia_SPS_ratio,"Pythia - Just SPS","pe");
    leg_nMPI_cats_ratio->AddEntry(h_nMPI_cats_bcvegpy_ratio,"BcVegPy","pe");
    leg_nMPI_cats_ratio->SetTextFont(132);
    leg_nMPI_cats_ratio->SetFillStyle(0);
    leg_nMPI_cats_ratio->Draw();

    c_nMPI_cats_ratio->Print("plots_kinematics_Bc_to_Bu/c_nMPI_cats_ratio.pdf");




    // -------------------------------------------------------------------------
    // Plot 2D plots 
    // -------------------------------------------------------------------------

    // -------------------------------------------------------------------------
    // Plot 1D angles integrated over all eta 
    // -------------------------------------------------------------------------
    SetLHCbStyle("oneD");

    // angles
    TCanvas* c_delta_phi_bc_bx = new TCanvas("c_delta_phi_bc_bx","c_delta_phi_bc_bx");
    TH1D* h_delta_phi_bc_bx_pythia_all  = new TH1D("h_delta_phi_bc_bx_pythia_all", "h_delta_phi_bc_bx_pythia_all", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_bx_pythia_SPS  = new TH1D("h_delta_phi_bc_bx_pythia_SPS", "h_delta_phi_bc_bx_pythia_SPS", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_bx_pythia_DPS  = new TH1D("h_delta_phi_bc_bx_pythia_DPS", "h_delta_phi_bc_bx_pythia_DPS", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_bx_bcvegpy = new TH1D("h_delta_phi_bc_bx_bcvegpy","h_delta_phi_bc_bx_bcvegpy",50,0,TMath::Pi());

    pythia_tree->Draw( "angle_bc_bx>>h_delta_phi_bc_bx_pythia_all",  cuts_Bc_all+hassXbXc);
    pythia_tree->Draw( "angle_bc_bx>>h_delta_phi_bc_bx_pythia_DPS",  cuts_Bc_all+hassXbXc+cuts_DPS);
    pythia_tree->Draw( "angle_bc_bx>>h_delta_phi_bc_bx_pythia_SPS",  cuts_Bc_all+hassXbXc+cuts_SPS);
    bcvegpy_tree->Draw("angle_bc_bx>>h_delta_phi_bc_bx_bcvegpy",     cuts_Bc_all+hassXbXc);

    h_delta_phi_bc_bx_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_phi_bc_bx_pythia_all->Scale(scalefactor_pythia);
    h_delta_phi_bc_bx_pythia_SPS->Scale(scalefactor_pythia);
    h_delta_phi_bc_bx_pythia_DPS->Scale(scalefactor_pythia);

    // h_delta_phi_bc_bx_bcvegpy->Scale(1.0/h_delta_phi_bc_bx_bcvegpy->Integral());
    // h_delta_phi_bc_bx_pythia_all->Scale(1.0/h_delta_phi_bc_bx_pythia_all->Integral());
    // h_delta_phi_bc_bx_pythia_SPS->Scale(1.0/h_delta_phi_bc_bx_pythia_SPS->Integral());
    // h_delta_phi_bc_bx_pythia_DPS->Scale(1.0/h_delta_phi_bc_bx_pythia_DPS->Integral());

    h_delta_phi_bc_bx_pythia_all->SetLineColor(kBlack);
    h_delta_phi_bc_bx_pythia_SPS->SetLineColor(kBlue);
    h_delta_phi_bc_bx_pythia_DPS->SetLineColor(kRed);
    h_delta_phi_bc_bx_bcvegpy->SetLineColor(kGreen+2);

    h_delta_phi_bc_bx_bcvegpy->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; Normalised");
    h_delta_phi_bc_bx_pythia_all->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; Normalised");
    h_delta_phi_bc_bx_pythia_SPS->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; Normalised");
    h_delta_phi_bc_bx_pythia_DPS->SetTitle(";#Delta#phi(B_{c}^{+}, X_{b}) [rad]; Normalised");
    h_delta_phi_bc_bx_bcvegpy->SetMinimum(0.0);
    h_delta_phi_bc_bx_pythia_all->SetMinimum(0.0);
    h_delta_phi_bc_bx_pythia_SPS->SetMinimum(0.0);
    h_delta_phi_bc_bx_pythia_DPS->SetMinimum(0.0);
    

    h_delta_phi_bc_bx_pythia_all->SetMaximum(1.05*max({h_delta_phi_bc_bx_pythia_all->GetMaximum(),
                                                   h_delta_phi_bc_bx_pythia_SPS->GetMaximum(),
                                                   h_delta_phi_bc_bx_pythia_DPS->GetMaximum(),
                                                   h_delta_phi_bc_bx_bcvegpy->GetMaximum()}));

    h_delta_phi_bc_bx_pythia_all->Draw("hist ][");
    h_delta_phi_bc_bx_pythia_SPS->Draw("hist ][ same");
    h_delta_phi_bc_bx_pythia_DPS->Draw("hist ][ same");
    h_delta_phi_bc_bx_bcvegpy->Draw("hist ][ same");
    

    TLegend* leg_delta_phi_bc_bx = new TLegend(0.2,0.6,0.5,0.9);
    leg_delta_phi_bc_bx->AddEntry(h_delta_phi_bc_bx_pythia_all,"Pythia - All","l");
    leg_delta_phi_bc_bx->AddEntry(h_delta_phi_bc_bx_pythia_SPS,"Pythia - Just SPS","l");
    leg_delta_phi_bc_bx->AddEntry(h_delta_phi_bc_bx_pythia_DPS,"Pythia - Just DPS","l");
    leg_delta_phi_bc_bx->AddEntry(h_delta_phi_bc_bx_bcvegpy,"BcVegPy","l");
    leg_delta_phi_bc_bx->SetTextFont(132);
    leg_delta_phi_bc_bx->Draw();

    c_delta_phi_bc_bx->Print("plots_kinematics_Bc_to_Bu/c_delta_phi_bc_bx.pdf");


    // angles
    TCanvas* c_delta_phi_bc_dx = new TCanvas("c_delta_phi_bc_dx","c_delta_phi_bc_dx");
    TH1D* h_delta_phi_bc_dx_pythia_all  = new TH1D("h_delta_phi_bc_dx_pythia_all", "h_delta_phi_bc_dx_pythia_all", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_dx_pythia_SPS  = new TH1D("h_delta_phi_bc_dx_pythia_SPS", "h_delta_phi_bc_dx_pythia_SPS", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_dx_pythia_DPS  = new TH1D("h_delta_phi_bc_dx_pythia_DPS", "h_delta_phi_bc_dx_pythia_DPS", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bc_dx_bcvegpy = new TH1D("h_delta_phi_bc_dx_bcvegpy","h_delta_phi_bc_dx_bcvegpy",50,0,TMath::Pi());

    pythia_tree->Draw( "angle_bc_dx>>h_delta_phi_bc_dx_pythia_all",  cuts_Bc_all+hassXbXc);
    pythia_tree->Draw( "angle_bc_dx>>h_delta_phi_bc_dx_pythia_DPS",  cuts_Bc_all+hassXbXc+cuts_DPS);
    pythia_tree->Draw( "angle_bc_dx>>h_delta_phi_bc_dx_pythia_SPS",  cuts_Bc_all+hassXbXc+cuts_SPS);
    bcvegpy_tree->Draw("angle_bc_dx>>h_delta_phi_bc_dx_bcvegpy",     cuts_Bc_all+hassXbXc);

    h_delta_phi_bc_dx_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_phi_bc_dx_pythia_all->Scale(scalefactor_pythia);
    h_delta_phi_bc_dx_pythia_SPS->Scale(scalefactor_pythia);
    h_delta_phi_bc_dx_pythia_DPS->Scale(scalefactor_pythia);

    h_delta_phi_bc_dx_bcvegpy->Scale(1.0/h_delta_phi_bc_dx_bcvegpy->Integral());
    h_delta_phi_bc_dx_pythia_all->Scale(1.0/h_delta_phi_bc_dx_pythia_all->Integral());
    h_delta_phi_bc_dx_pythia_SPS->Scale(1.0/h_delta_phi_bc_dx_pythia_SPS->Integral());
    h_delta_phi_bc_dx_pythia_DPS->Scale(1.0/h_delta_phi_bc_dx_pythia_DPS->Integral());

    h_delta_phi_bc_dx_pythia_all->SetLineColor(kBlack);
    h_delta_phi_bc_dx_pythia_SPS->SetLineColor(kBlue);
    h_delta_phi_bc_dx_pythia_DPS->SetLineColor(kRed);
    h_delta_phi_bc_dx_bcvegpy->SetLineColor(kGreen+2);

    h_delta_phi_bc_dx_bcvegpy->SetTitle(";#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bc_dx_pythia_all->SetTitle(";#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bc_dx_pythia_SPS->SetTitle(";#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bc_dx_pythia_DPS->SetTitle(";#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bc_dx_bcvegpy->SetMinimum(0.0);
    h_delta_phi_bc_dx_pythia_all->SetMinimum(0.0);
    h_delta_phi_bc_dx_pythia_SPS->SetMinimum(0.0);
    h_delta_phi_bc_dx_pythia_DPS->SetMinimum(0.0);
    

    h_delta_phi_bc_dx_pythia_all->SetMaximum(1.05*max({h_delta_phi_bc_dx_pythia_all->GetMaximum(),
                                                   h_delta_phi_bc_dx_pythia_SPS->GetMaximum(),
                                                   h_delta_phi_bc_dx_pythia_DPS->GetMaximum(),
                                                   h_delta_phi_bc_dx_bcvegpy->GetMaximum()}));

    h_delta_phi_bc_dx_pythia_all->Draw("hist ][");
    h_delta_phi_bc_dx_pythia_SPS->Draw("hist ][ same");
    h_delta_phi_bc_dx_pythia_DPS->Draw("hist ][ same");
    h_delta_phi_bc_dx_bcvegpy->Draw("hist ][ same");
    

    TLegend* leg_delta_phi_bc_dx = new TLegend(0.2,0.2,0.5,0.5);
    leg_delta_phi_bc_dx->AddEntry(h_delta_phi_bc_dx_pythia_all,"Pythia - All","l");
    leg_delta_phi_bc_dx->AddEntry(h_delta_phi_bc_dx_pythia_SPS,"Pythia - Just SPS","l");
    leg_delta_phi_bc_dx->AddEntry(h_delta_phi_bc_dx_pythia_DPS,"Pythia - Just DPS","l");
    leg_delta_phi_bc_dx->AddEntry(h_delta_phi_bc_dx_bcvegpy,"BcVegPy","l");
    leg_delta_phi_bc_dx->SetTextFont(132);
    leg_delta_phi_bc_dx->Draw();

    c_delta_phi_bc_dx->Print("plots_kinematics_Bc_to_Bu/c_delta_phi_bc_dx.pdf");



    // angles
    TCanvas* c_delta_phi_bx_dx = new TCanvas("c_delta_phi_bx_dx","c_delta_phi_bx_dx");
    TH1D* h_delta_phi_bx_dx_pythia_all  = new TH1D("h_delta_phi_bx_dx_pythia_all", "h_delta_phi_bx_dx_pythia_all", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bx_dx_pythia_SPS  = new TH1D("h_delta_phi_bx_dx_pythia_SPS", "h_delta_phi_bx_dx_pythia_SPS", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bx_dx_pythia_DPS  = new TH1D("h_delta_phi_bx_dx_pythia_DPS", "h_delta_phi_bx_dx_pythia_DPS", 50,0,TMath::Pi());
    TH1D* h_delta_phi_bx_dx_bcvegpy = new TH1D("h_delta_phi_bx_dx_bcvegpy","h_delta_phi_bx_dx_bcvegpy",50,0,TMath::Pi());

    pythia_tree->Draw( "angle_bx_dx>>h_delta_phi_bx_dx_pythia_all",  cuts_Bc_all+Bc_fid+hassXbXc);
    pythia_tree->Draw( "angle_bx_dx>>h_delta_phi_bx_dx_pythia_DPS",  cuts_Bc_all+Bc_fid+hassXbXc+cuts_DPS);
    pythia_tree->Draw( "angle_bx_dx>>h_delta_phi_bx_dx_pythia_SPS",  cuts_Bc_all+Bc_fid+hassXbXc+cuts_SPS);
    bcvegpy_tree->Draw("angle_bx_dx>>h_delta_phi_bx_dx_bcvegpy",     cuts_Bc_all+Bc_fid+hassXbXc);

    h_delta_phi_bx_dx_bcvegpy->Scale(scalefactor_bcvegpy);
    h_delta_phi_bx_dx_pythia_all->Scale(scalefactor_pythia);
    h_delta_phi_bx_dx_pythia_SPS->Scale(scalefactor_pythia);
    h_delta_phi_bx_dx_pythia_DPS->Scale(scalefactor_pythia);

    h_delta_phi_bx_dx_bcvegpy->Scale(1.0/h_delta_phi_bx_dx_bcvegpy->Integral());
    h_delta_phi_bx_dx_pythia_all->Scale(1.0/h_delta_phi_bx_dx_pythia_all->Integral());
    h_delta_phi_bx_dx_pythia_SPS->Scale(1.0/h_delta_phi_bx_dx_pythia_SPS->Integral());
    h_delta_phi_bx_dx_pythia_DPS->Scale(1.0/h_delta_phi_bx_dx_pythia_DPS->Integral());

    h_delta_phi_bx_dx_pythia_all->SetLineColor(kBlack);
    h_delta_phi_bx_dx_pythia_SPS->SetLineColor(kBlue);
    h_delta_phi_bx_dx_pythia_DPS->SetLineColor(kRed);
    h_delta_phi_bx_dx_bcvegpy->SetLineColor(kGreen+2);

    h_delta_phi_bx_dx_bcvegpy->SetTitle(";#Delta#phi(X_{b}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bx_dx_pythia_all->SetTitle(";#Delta#phi(X_{b}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bx_dx_pythia_SPS->SetTitle(";#Delta#phi(X_{b}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bx_dx_pythia_DPS->SetTitle(";#Delta#phi(X_{b}, X_{#bar{c}}) [rad]; Normalised");
    h_delta_phi_bx_dx_bcvegpy->SetMinimum(0.0);
    h_delta_phi_bx_dx_pythia_all->SetMinimum(0.0);
    h_delta_phi_bx_dx_pythia_SPS->SetMinimum(0.0);
    h_delta_phi_bx_dx_pythia_DPS->SetMinimum(0.0);
    

    h_delta_phi_bx_dx_pythia_all->SetMaximum(1.05*max({h_delta_phi_bx_dx_pythia_all->GetMaximum(),
                                                   h_delta_phi_bx_dx_pythia_SPS->GetMaximum(),
                                                   h_delta_phi_bx_dx_pythia_DPS->GetMaximum(),
                                                   h_delta_phi_bx_dx_bcvegpy->GetMaximum()}));

    h_delta_phi_bx_dx_pythia_all->Draw("hist ][");
    h_delta_phi_bx_dx_pythia_SPS->Draw("hist ][ same");
    h_delta_phi_bx_dx_pythia_DPS->Draw("hist ][ same");
    h_delta_phi_bx_dx_bcvegpy->Draw("hist ][ same");
    

    TLegend* leg_delta_phi_bx_dx = new TLegend(0.2,0.6,0.5,0.9);
    leg_delta_phi_bx_dx->AddEntry(h_delta_phi_bx_dx_pythia_all,"Pythia - All","l");
    leg_delta_phi_bx_dx->AddEntry(h_delta_phi_bx_dx_pythia_SPS,"Pythia - Just SPS","l");
    leg_delta_phi_bx_dx->AddEntry(h_delta_phi_bx_dx_pythia_DPS,"Pythia - Just DPS","l");
    leg_delta_phi_bx_dx->AddEntry(h_delta_phi_bx_dx_bcvegpy,"BcVegPy","l");
    leg_delta_phi_bx_dx->SetTextFont(132);
    leg_delta_phi_bx_dx->SetFillStyle(0);
    leg_delta_phi_bx_dx->Draw();

    c_delta_phi_bx_dx->Print("plots_kinematics_Bc_to_Bu/c_delta_phi_bx_dx.pdf");

    // // angles
    SetLHCbStyle("cont");
    int n_bins = 10;

    TH2D* h_processes_delta_phi_2d_pythia_all = new TH2D("h_processes_delta_phi_2d_pythia_all", 
                                    "h_processes_delta_phi_2d_pythia_all;#Delta#phi(B_{c}^{+}, X_{b}) [rad];#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );
    TH2D* h_processes_delta_phi_2d_pythia_SPS = new TH2D("h_processes_delta_phi_2d_pythia_SPS", 
                                    "h_processes_delta_phi_2d_pythia_SPS;#Delta#phi(B_{c}^{+}, X_{b}) [rad];#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_delta_phi_2d_pythia_DPS = new TH2D("h_processes_delta_phi_2d_pythia_DPS", 
                                    "h_processes_delta_phi_2d_pythia_DPS;#Delta#phi(B_{c}^{+}, X_{b}) [rad];#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_delta_phi_2d_bcvegpy = new TH2D("h_processes_delta_phi_2d_bcvegpy", 
                                    "h_processes_delta_phi_2d_bcvegpy;#Delta#phi(B_{c}^{+}, X_{b}) [rad];#Delta#phi(B_{c}^{+}, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    // h_processes_delta_phi_2d_bcvegpy->Scale(scalefactor_bcvegpy);
    // h_processes_delta_phi_2d_pythia_all->Scale(scalefactor_pythia);
    // h_processes_delta_phi_2d_pythia_SPS->Scale(scalefactor_pythia);
    // h_processes_delta_phi_2d_pythia_DPS->Scale(scalefactor_pythia);

    TCanvas* can_delta_phi_2d_pythia_all = new TCanvas("can_delta_phi_2d_pythia_all","can_delta_phi_2d_pythia_all",550,500);

    pythia_tree->Draw( "angle_bc_dx:angle_bc_bx>>h_processes_delta_phi_2d_pythia_all",  cuts_Bc_all+Bc_fid+hassXbXc);
    pythia_tree->Draw( "angle_bc_dx:angle_bc_bx>>h_processes_delta_phi_2d_pythia_DPS",  cuts_Bc_all+Bc_fid+hassXbXc+cuts_DPS);
    pythia_tree->Draw( "angle_bc_dx:angle_bc_bx>>h_processes_delta_phi_2d_pythia_SPS",  cuts_Bc_all+Bc_fid+hassXbXc+cuts_SPS);
    bcvegpy_tree->Draw("angle_bc_dx:angle_bc_bx>>h_processes_delta_phi_2d_bcvegpy",     cuts_Bc_all+Bc_fid+hassXbXc);

    h_processes_delta_phi_2d_pythia_all->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_DPS->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_SPS->SetMinimum(0);
    h_processes_delta_phi_2d_bcvegpy->SetMinimum(0);
    
    h_processes_delta_phi_2d_pythia_all->Draw("colz");
    can_delta_phi_2d_pythia_all->Print("plots_kinematics_Bc_to_Bu/can_delta_phi_2d_pythia_all.pdf");

    TCanvas* can_delta_phi_2d_pythia_SPS = new TCanvas("can_delta_phi_2d_pythia_SPS","can_delta_phi_2d_pythia_SPS",550,500);
    h_processes_delta_phi_2d_pythia_SPS->Draw("colz");
    can_delta_phi_2d_pythia_SPS->Print("plots_kinematics_Bc_to_Bu/can_delta_phi_2d_pythia_SPS.pdf");
    
    TCanvas* can_delta_phi_2d_pythia_DPS = new TCanvas("can_delta_phi_2d_pythia_DPS","can_delta_phi_2d_pythia_DPS",550,500);
    h_processes_delta_phi_2d_pythia_DPS->Draw("colz");
    can_delta_phi_2d_pythia_DPS->Print("plots_kinematics_Bc_to_Bu/can_delta_phi_2d_pythia_DPS.pdf");
    
    TCanvas* can_delta_phi_2d_bcvegpy = new TCanvas("can_delta_phi_2d_bcvegpy","can_delta_phi_2d_bcvegpy",550,500);
    h_processes_delta_phi_2d_bcvegpy->Draw("colz");
    can_delta_phi_2d_bcvegpy->Print("plots_kinematics_Bc_to_Bu/can_delta_phi_2d_bcvegpy.pdf");

}

