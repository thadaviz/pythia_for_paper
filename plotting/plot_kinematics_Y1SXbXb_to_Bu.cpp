#include "tools.h"

void plot_kinematics_Y1SXbXb_to_Bu(){
    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    std::string filename_pythia_bbbb     = "../output/from_lxplus/Complete_paper_samples/Sample_bbbb/main202output_SoftQCD_nd_UserHook_cc_Scale_4.000000_Quark_5_N_13600000_PartMod1_noColRec.root";
    std::string filename_pythia_bb       = "../output/from_lxplus/Complete_paper_samples/Sample_bb/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_10000000_PartMod1_noColRec.root";


    // -------------------------------------------------------------------------
    // Fig cuts
    // -------------------------------------------------------------------------

    // TCut Upsilon1S_fid = "upsilon1s_y > 2.0 && upsilon1s_y < 4.5";
    TCut Upsilon1S_fid = "";

    // TCut Bu_fid = "Bu_p_y > 2.0 && Bu_p_y < 4.5";
    TCut Bu_fid = "";

    TCut cuts_Y1SXcXc_all    = "nUpsilon1S>0 && nXb==2";
    TCut cuts_Dz_all         = "nBu_p>=1";


    TCut cut_DPS_Mix  = " Upsilon1S_isMixedLine && Upsilon1S_iscc && Upsilon1S_iscc_c_partSys!=Upsilon1S_iscc_cbar_partSys ";
    TCut cut_SPS_Mix  = " Upsilon1S_isMixedLine && Upsilon1S_iscc && Upsilon1S_iscc_c_partSys==Upsilon1S_iscc_cbar_partSys";
  
    TCut cut_DPS_Sep  = "!Upsilon1S_isMixedLine && (((Upsilon1S_isHard||Upsilon1S_isMPI)&&Upsilon1S_isHard_partSys!=bx_partSys)||(Upsilon1S_iscc && Upsilon1S_iscc_c_partSys!=Upsilon1S_iscc_cbar_partSys&&Upsilon1S_iscc_c_partSys!=bx_partSys))"; 
    TCut cut_SPS_Sep  = "!Upsilon1S_isMixedLine && (((Upsilon1S_isHard||Upsilon1S_isMPI)&&Upsilon1S_isHard_partSys==bx_partSys)||(Upsilon1S_iscc && Upsilon1S_iscc_c_partSys==Upsilon1S_iscc_cbar_partSys&&Upsilon1S_iscc_c_partSys==bx_partSys))";

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file_all = TFile::Open(filename_pythia_bb.c_str());
    TTree* pythia_tree_all = (TTree*) pythia_file_all->Get("events");

    pythia_tree_all->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_all->SetAlias("bx2_pt" , "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    pythia_tree_all->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");


    pythia_tree_all->SetAlias("angle_bx2_bx","acos((bx2_px*bx_px + bx2_py*bx_py)/(bx2_pt*bx_pt))");
    pythia_tree_all->SetAlias("angle_upsilon1s_bx","acos((upsilon1s_px*bx_px + upsilon1s_py*bx_py)/(upsilon1s_pt*bx_pt))");
    pythia_tree_all->SetAlias("angle_upsilon1s_bx2","acos((upsilon1s_px*bx2_px + upsilon1s_py*bx2_py)/(upsilon1s_pt*bx2_pt))");


    pythia_tree_all->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree_all->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_all->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree_all->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree_all->SetAlias("bx2_p",  "sqrt(bx2_px*bx2_px+bx2_py*bx2_py+bx2_pz*bx2_pz)");
    pythia_tree_all->SetAlias("bx2_pt", "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    pythia_tree_all->SetAlias("bx2_eta","(bx2_pz>0?-1:1)*acosh(bx2_p/bx2_pt)");
    pythia_tree_all->SetAlias("bx2_y","0.5*log( (bx2_pe+bx2_pz) / (bx2_pe-bx2_pz) )");


    pythia_tree_all->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree_all->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree_all->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree_all->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    pythia_tree_all->SetAlias("Bu_p_p",  "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py+Bu_p_pz*Bu_p_pz)");
    pythia_tree_all->SetAlias("Bu_p_pt", "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py)");
    pythia_tree_all->SetAlias("Bu_p_eta","(Bu_p_pz>0?-1:1)*acosh(Bu_p_p/Bu_p_pt)");
    pythia_tree_all->SetAlias("Bu_p_y","0.5*log( (Bu_p_pe+Bu_p_pz) / (Bu_p_pe-Bu_p_pz) )");

    pythia_tree_all->SetAlias("delta_eta_bx2_bx","bx2_eta-bx_eta");
    pythia_tree_all->SetAlias("delta_eta_upsilon1s_bx","upsilon1s_eta-bx_eta");
    pythia_tree_all->SetAlias("delta_eta_upsilon1s_bx2","upsilon1s_eta-bx2_eta");

    pythia_tree_all->SetAlias("delta_y_bx2_bx","bx2_y-bx_y");
    pythia_tree_all->SetAlias("delta_y_upsilon1s_bx","upsilon1s_y-bx_y");
    pythia_tree_all->SetAlias("delta_y_upsilon1s_bx2","upsilon1s_y-bx2_y");

    pythia_tree_all->SetAlias("delta_R_bx2_bx","sqrt(delta_eta_bx2_bx*delta_eta_bx2_bx+angle_bx2_bx*angle_bx2_bx)");
    pythia_tree_all->SetAlias("delta_R_upsilon1s_bx","sqrt(delta_eta_upsilon1s_bx*delta_eta_upsilon1s_bx+angle_upsilon1s_bx*angle_upsilon1s_bx)");
    pythia_tree_all->SetAlias("delta_R_upsilon1s_bx2","sqrt(delta_eta_upsilon1s_bx2*delta_eta_upsilon1s_bx2+angle_upsilon1s_bx2*angle_upsilon1s_bx2)");

    double sigmaGen_pythia_all;
    double sigmaErr_pythia_all;
    pythia_tree_all->SetBranchAddress("sigmaGen",&sigmaGen_pythia_all);
    pythia_tree_all->SetBranchAddress("sigmaErr",&sigmaErr_pythia_all);
    
    int n_entries_pythia_all = pythia_tree_all->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open(filename_pythia_bbbb.c_str());
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");

    pythia_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("bx2_pt" , "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    pythia_tree->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");


    pythia_tree->SetAlias("angle_bx2_bx","acos((bx2_px*bx_px + bx2_py*bx_py)/(bx2_pt*bx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_bx","acos((upsilon1s_px*bx_px + upsilon1s_py*bx_py)/(upsilon1s_pt*bx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_bx2","acos((upsilon1s_px*bx2_px + upsilon1s_py*bx2_py)/(upsilon1s_pt*bx2_pt))");


    pythia_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree->SetAlias("bx2_p",  "sqrt(bx2_px*bx2_px+bx2_py*bx2_py+bx2_pz*bx2_pz)");
    pythia_tree->SetAlias("bx2_pt", "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    pythia_tree->SetAlias("bx2_eta","(bx2_pz>0?-1:1)*acosh(bx2_p/bx2_pt)");
    pythia_tree->SetAlias("bx2_y","0.5*log( (bx2_pe+bx2_pz) / (bx2_pe-bx2_pz) )");


    pythia_tree->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");

    pythia_tree->SetAlias("Bu_p_p",  "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py+Bu_p_pz*Bu_p_pz)");
    pythia_tree->SetAlias("Bu_p_pt", "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py)");
    pythia_tree->SetAlias("Bu_p_eta","(Bu_p_pz>0?-1:1)*acosh(Bu_p_p/Bu_p_pt)");
    pythia_tree->SetAlias("Bu_p_y","0.5*log( (Bu_p_pe+Bu_p_pz) / (Bu_p_pe-Bu_p_pz) )");

    pythia_tree->SetAlias("delta_eta_bx2_bx","bx2_eta-bx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_bx","upsilon1s_eta-bx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_bx2","upsilon1s_eta-bx2_eta");

    pythia_tree->SetAlias("delta_y_bx2_bx","bx2_y-bx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_bx","upsilon1s_y-bx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_bx2","upsilon1s_y-bx2_y");

    pythia_tree->SetAlias("delta_R_bx2_bx","sqrt(delta_eta_bx2_bx*delta_eta_bx2_bx+angle_bx2_bx*angle_bx2_bx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_bx","sqrt(delta_eta_upsilon1s_bx*delta_eta_upsilon1s_bx+angle_upsilon1s_bx*angle_upsilon1s_bx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_bx2","sqrt(delta_eta_upsilon1s_bx2*delta_eta_upsilon1s_bx2+angle_upsilon1s_bx2*angle_upsilon1s_bx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    pythia_tree_all->GetEntry(pythia_tree_all->GetEntries()-1);
 
    std::cout << "Cross section Pythia:        " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nUpsilon1S>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
   
    std::cout << "Cross section Pythia all:  " << sigmaGen_pythia_all*microbarn  << " +/- " << sigmaErr_pythia_all*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree_all->GetEntries("nUpsilon1S>0")<< "/" << pythia_tree_all->GetEntries("")<< std::endl;
    

    double scalefactor_pythia     = sigmaGen_pythia*microbarn/n_entries_pythia;
    double scalefactor_pythia_all = sigmaGen_pythia_all*microbarn/n_entries_pythia_all;


    // // -------------------------------------------------------------------------
    // // Plot multiplicity
    // // -------------------------------------------------------------------------
    // TCanvas* c_multiplicity = new TCanvas("c_multiplicity","c_multiplicity");
    // TH1D* h_multiplicity_pythia  = new TH1D("h_multiplicity_pythia", "h_multiplicity_pythia", 20,0,100);
    // TH1D* h_multiplicity_pythia_DPS  = new TH1D("h_multiplicity_pythia_DPS", "h_multiplicity_pythia_DPS", 20,0,100);
    // TH1D* h_multiplicity_pythia_SPS  = new TH1D("h_multiplicity_pythia_SPS", "h_multiplicity_pythia_SPS", 20,0,100);

    // TH1D* h_multiplicity_pythia_Dz  = new TH1D("h_multiplicity_pythia_Dz", "h_multiplicity_pythia_Dz", 20,0,100);

    // pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia",cuts_Y1SXcXc_all+Upsilon1S_fid);
    // pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_DPS",cuts_Y1SXcXc_all+Upsilon1S_fid+cuts_DPS);
    // pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_SPS",cuts_Y1SXcXc_all+Upsilon1S_fid+cuts_SPS);
    // pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_Dz",cuts_Dz_all+Bu_fid);

    // h_multiplicity_pythia->Scale(scalefactor_pythia);
    // h_multiplicity_pythia_DPS->Scale(scalefactor_pythia);
    // h_multiplicity_pythia_SPS->Scale(scalefactor_pythia);
    // h_multiplicity_pythia_Dz->Scale(scalefactor_pythia);

    // // Black -> With MPI
    // h_multiplicity_pythia->SetLineColor(kBlack);

    // h_multiplicity_pythia_Dz->SetLineColor(kBlack);
    // h_multiplicity_pythia_Dz->SetLineStyle(kDashed);

    // h_multiplicity_pythia_DPS->SetLineColor(kRed);
    // h_multiplicity_pythia_SPS->SetLineColor(kBlue);

    // h_multiplicity_pythia->SetMaximum(1.05*max({h_multiplicity_pythia->GetMaximum(),
    //                                             h_multiplicity_pythia_Dz->GetMaximum(),
    //                                             h_multiplicity_pythia_DPS->GetMaximum(),
    //                                             h_multiplicity_pythia_SPS->GetMaximum()
    //                                   }));
    
    // double h_multiplicity_pythia_bin_width = h_multiplicity_pythia->GetXaxis()->GetBinWidth(2);
    // h_multiplicity_pythia->SetTitle(Form(";N_{Charged}^{2.0<#eta<4.5}; d#sigma/dN [%.1f #mub]",h_multiplicity_pythia_bin_width));
    // h_multiplicity_pythia->Draw("hist");
    // h_multiplicity_pythia_Dz->Draw("same hist");
    // h_multiplicity_pythia_DPS->Draw("same hist");
    // h_multiplicity_pythia_SPS->Draw("same hist");
    // gPad->SetLogy();

    // TLegend* leg_multiplicity = new TLegend(0.6,0.6,0.9,0.9);
    // leg_multiplicity->AddEntry(h_multiplicity_pythia_Dz,"B^{+} Pythia With MPI","l");
    // leg_multiplicity->AddEntry(h_multiplicity_pythia,"#Upsilon(1S) Pythia With MPI","l");
    // leg_multiplicity->AddEntry(h_multiplicity_pythia_SPS,"#Upsilon(1S) DPS Pythia With MPI","l");
    // leg_multiplicity->AddEntry(h_multiplicity_pythia_DPS,"#Upsilon(1S) SPS Pythia With MPI","l");
    // leg_multiplicity->SetTextFont(132);
    // leg_multiplicity->SetFillStyle(0);
    // leg_multiplicity->Draw();
    // c_multiplicity->Print("plots_kinematics_Y1SXbXb_to_Bu/c_multiplicity.pdf");

    // TCanvas* c_multiplicity_ratio = new TCanvas("c_multiplicity_ratio","c_multiplicity_ratio");

    // TH1D* h_multiplicity_pythia_ratio  = new TH1D("h_multiplicity_pythia_ratio", "h_multiplicity_pythia_ratio", 20,0,100);
    // TH1D* h_multiplicity_pythia_SPS_ratio  = new TH1D("h_multiplicity_pythia_SPS_ratio", "h_multiplicity_pythia_SPS_ratio", 20,0,100);
    // TH1D* h_multiplicity_pythia_DPS_ratio  = new TH1D("h_multiplicity_pythia_DPS_ratio", "h_multiplicity_pythia_DPS_ratio", 20,0,100);

    // h_multiplicity_pythia_ratio->Divide(h_multiplicity_pythia,h_multiplicity_pythia_Dz);
    // h_multiplicity_pythia_DPS_ratio->Divide(h_multiplicity_pythia_DPS,h_multiplicity_pythia_Dz);
    // h_multiplicity_pythia_SPS_ratio->Divide(h_multiplicity_pythia_SPS,h_multiplicity_pythia_Dz);


    // h_multiplicity_pythia_ratio->Scale(100);
    // h_multiplicity_pythia_DPS_ratio->Scale(100);
    // h_multiplicity_pythia_SPS_ratio->Scale(100);
    
    //         // Black -> With MPI
    // h_multiplicity_pythia_ratio->SetLineColor(kBlack);


    // h_multiplicity_pythia_DPS_ratio->SetLineColor(kRed);
    // h_multiplicity_pythia_DPS_ratio->SetMarkerColor(kRed);
    // h_multiplicity_pythia_SPS_ratio->SetLineColor(kBlue);
    // h_multiplicity_pythia_SPS_ratio->SetMarkerColor(kBlue);

    // h_multiplicity_pythia_ratio->SetMaximum(1.05*max({
    //                                               h_multiplicity_pythia_ratio->GetMaximum(),
    //                                               h_multiplicity_pythia_DPS_ratio->GetMaximum(),
    //                                               h_multiplicity_pythia_SPS_ratio->GetMaximum()
    //                                   }));
    // h_multiplicity_pythia_ratio->SetTitle(";N_{Charged}^{2.0<#eta<4.5};#scale[0.6]{#frac{d#sigma(#Upsilon(1S))}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    // h_multiplicity_pythia_ratio->SetMinimum(0);
    // h_multiplicity_pythia_ratio->Draw();
    // h_multiplicity_pythia_DPS_ratio->Draw("same");
    // h_multiplicity_pythia_SPS_ratio->Draw("same");


    // TLegend* leg_multiplicity_ratio = new TLegend(0.5,0.4,0.8,0.7);
    // leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_ratio,"Pythia With MPI","pe");
    // leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_DPS_ratio,"DPS Pythia With MPI","pe");
    // leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_SPS_ratio,"SPS Pythia With MPI","pe");
    // leg_multiplicity_ratio->SetTextFont(132);
    // leg_multiplicity_ratio->SetFillStyle(0);
    // leg_multiplicity_ratio->Draw();

    // c_multiplicity_ratio->Print("plots_kinematics_Y1SXbXb_to_Bu/c_multiplicity_ratio.pdf");

    // -------------------------------------------------------------------------
    // Plot multiplicity
    // -------------------------------------------------------------------------
    TCanvas* c_nMPI = new TCanvas("c_nMPI","c_nMPI");
    TH1D* h_nMPI_pythia  = new TH1D("h_nMPI_pythia", "h_nMPI_pythia", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Mix  = new TH1D("h_nMPI_pythia_DPS_Mix", "h_nMPI_pythia_DPS_Mix", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Mix  = new TH1D("h_nMPI_pythia_SPS_Mix", "h_nMPI_pythia_SPS_Mix", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Sep  = new TH1D("h_nMPI_pythia_DPS_Sep", "h_nMPI_pythia_DPS_Sep", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Sep  = new TH1D("h_nMPI_pythia_SPS_Sep", "h_nMPI_pythia_SPS_Sep", 13,0,26);

    TH1D* h_nMPI_pythia_Dz  = new TH1D("h_nMPI_pythia_Dz", "h_nMPI_pythia_Dz", 13,0,26);

    pythia_tree->Draw("nMPI>>h_nMPI_pythia",cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_DPS_Mix",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Mix);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_SPS_Mix",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Mix);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_DPS_Sep",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Sep);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_SPS_Sep",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Sep);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia",cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree_all->Draw("nMPI>>h_nMPI_pythia_Dz",cuts_Dz_all+Bu_fid);

    h_nMPI_pythia->Scale(scalefactor_pythia);
    h_nMPI_pythia_DPS_Mix->Scale(scalefactor_pythia);
    h_nMPI_pythia_SPS_Mix->Scale(scalefactor_pythia);
    h_nMPI_pythia_DPS_Sep->Scale(scalefactor_pythia);
    h_nMPI_pythia_SPS_Sep->Scale(scalefactor_pythia);
    h_nMPI_pythia_Dz->Scale(scalefactor_pythia_all);


    // Black -> With MPI
    h_nMPI_pythia->SetLineColor(kBlack);

    h_nMPI_pythia_Dz->SetLineColor(kBlack);
    h_nMPI_pythia_Dz->SetLineStyle(kDashed);


    h_nMPI_pythia_DPS_Mix->SetLineColor(kBlue);
    h_nMPI_pythia_SPS_Mix->SetLineColor(kRed);

    h_nMPI_pythia_DPS_Sep->SetLineColor(kBlue-9);
    h_nMPI_pythia_SPS_Sep->SetLineColor(kRed-9);

    h_nMPI_pythia_DPS_Sep->SetLineStyle(kDotted);
    h_nMPI_pythia_SPS_Sep->SetLineStyle(kDotted);

    h_nMPI_pythia->SetMaximum(1.05*max({h_nMPI_pythia->GetMaximum(),
                                      h_nMPI_pythia_Dz->GetMaximum(),
                                      h_nMPI_pythia_DPS_Mix->GetMaximum(),
                                      h_nMPI_pythia_SPS_Mix->GetMaximum(),
                                      h_nMPI_pythia_DPS_Sep->GetMaximum(),
                                      h_nMPI_pythia_SPS_Sep->GetMaximum()
                                      }));
    
    double h_nMPI_pythia_bin_width = h_nMPI_pythia->GetXaxis()->GetBinWidth(2);
    h_nMPI_pythia->SetTitle(Form(";N_{MPI}; d#sigma/dN [%.1f #mub]",h_nMPI_pythia_bin_width));
    h_nMPI_pythia->Draw("hist");
    h_nMPI_pythia_Dz->Draw("same hist");
    h_nMPI_pythia_DPS_Mix->Draw("same hist");
    h_nMPI_pythia_SPS_Mix->Draw("same hist");
    h_nMPI_pythia_DPS_Sep->Draw("same hist");
    h_nMPI_pythia_SPS_Sep->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_nMPI = new TLegend(0.6,0.6,0.9,0.9);
    leg_nMPI->AddEntry(h_nMPI_pythia_Dz,     "B^{+} Pythia","l");
    leg_nMPI->AddEntry(h_nMPI_pythia,        "#Upsilon(1S)X_{b}X_{#bar{b}} Pythia","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_DPS_Mix,"#Upsilon(1S)X_{b}X_{#bar{b}} DPS Mixed","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_DPS_Sep,"#Upsilon(1S)X_{b}X_{#bar{b}} DPS Unmixed","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_SPS_Mix,"#Upsilon(1S)X_{b}X_{#bar{b}} SPS Mixed","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_SPS_Sep,"#Upsilon(1S)X_{b}X_{#bar{b}} SPS Unmixed","l");

    leg_nMPI->SetTextFont(132);
    leg_nMPI->SetFillStyle(0);
    leg_nMPI->Draw();
    c_nMPI->Print("plots_kinematics_Y1SXbXb_to_Bu/c_nMPI.pdf");

    TCanvas* c_nMPI_ratio = new TCanvas("c_nMPI_ratio","c_nMPI_ratio");

    TH1D* h_nMPI_pythia_ratio  = new TH1D("h_nMPI_pythia_ratio", "h_nMPI_pythia_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Mix_ratio  = new TH1D("h_nMPI_pythia_DPS_Mix_ratio", "h_nMPI_pythia_DPS_Mix_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Mix_ratio  = new TH1D("h_nMPI_pythia_SPS_Mix_ratio", "h_nMPI_pythia_SPS_Mix_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Sep_ratio  = new TH1D("h_nMPI_pythia_DPS_Sep_ratio", "h_nMPI_pythia_DPS_Sep_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Sep_ratio  = new TH1D("h_nMPI_pythia_SPS_Sep_ratio", "h_nMPI_pythia_SPS_Sep_ratio", 13,0,26);

    h_nMPI_pythia_ratio->Divide(h_nMPI_pythia,h_nMPI_pythia_Dz);
    h_nMPI_pythia_DPS_Mix_ratio->Divide(h_nMPI_pythia_DPS_Mix,h_nMPI_pythia_Dz);
    h_nMPI_pythia_SPS_Mix_ratio->Divide(h_nMPI_pythia_SPS_Mix,h_nMPI_pythia_Dz);
    h_nMPI_pythia_DPS_Sep_ratio->Divide(h_nMPI_pythia_DPS_Sep,h_nMPI_pythia_Dz);
    h_nMPI_pythia_SPS_Sep_ratio->Divide(h_nMPI_pythia_SPS_Sep,h_nMPI_pythia_Dz);


    h_nMPI_pythia_ratio->Scale(100);
    h_nMPI_pythia_DPS_Mix_ratio->Scale(100);
    h_nMPI_pythia_SPS_Mix_ratio->Scale(100);
    h_nMPI_pythia_DPS_Sep_ratio->Scale(100);
    h_nMPI_pythia_SPS_Sep_ratio->Scale(100);
    
        // Black -> With MPI
    h_nMPI_pythia_ratio->SetLineColor(kBlack);


    h_nMPI_pythia_DPS_Mix_ratio->SetLineColor(kBlue);
    h_nMPI_pythia_DPS_Mix_ratio->SetMarkerColor(kBlue);

    h_nMPI_pythia_SPS_Mix_ratio->SetLineColor(kRed);
    h_nMPI_pythia_SPS_Mix_ratio->SetMarkerColor(kRed);

    h_nMPI_pythia_DPS_Sep_ratio->SetLineColor(kBlue-9);
    h_nMPI_pythia_DPS_Sep_ratio->SetMarkerColor(kBlue-9);

    h_nMPI_pythia_SPS_Sep_ratio->SetLineColor(kRed-9);
    h_nMPI_pythia_SPS_Sep_ratio->SetMarkerColor(kRed-9);
    h_nMPI_pythia_DPS_Sep_ratio->SetLineStyle(kDotted);
    h_nMPI_pythia_SPS_Sep_ratio->SetLineStyle(kDotted);

    h_nMPI_pythia_ratio->SetMaximum(1.05*max({h_nMPI_pythia_ratio->GetMaximum(),
                                              h_nMPI_pythia_DPS_Mix_ratio->GetMaximum(),
                                              h_nMPI_pythia_SPS_Mix_ratio->GetMaximum(),
                                              h_nMPI_pythia_DPS_Sep_ratio->GetMaximum(),
                                              h_nMPI_pythia_SPS_Sep_ratio->GetMaximum()
                                      }));
    h_nMPI_pythia_ratio->SetTitle(";N_{MPI};#scale[0.6]{#frac{d#sigma(#Upsilon(1S)X_{b}X_{#bar{b}})}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_nMPI_pythia_ratio->SetMinimum(0);
    h_nMPI_pythia_ratio->SetMaximum(0.09);
    h_nMPI_pythia_ratio->Draw();
    h_nMPI_pythia_DPS_Mix_ratio->Draw("same");
    h_nMPI_pythia_SPS_Mix_ratio->Draw("same");
    h_nMPI_pythia_DPS_Sep_ratio->Draw("same");
    h_nMPI_pythia_SPS_Sep_ratio->Draw("same");


    TLegend* leg_nMPI_ratio = new TLegend(0.13,0.55,0.72,0.93);
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_ratio,        "Pythia - All","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_DPS_Mix_ratio,"Pythia - Just DPS Mixed",   "pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_DPS_Sep_ratio,"Pythia - Just DPS Unmixed","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_SPS_Mix_ratio,"Pythia - Just SPS Mixed",   "pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_SPS_Sep_ratio,"Pythia - Just SPS Unmixed","pe");
    leg_nMPI_ratio->SetTextFont(132);
    leg_nMPI_ratio->SetFillStyle(0);
    leg_nMPI_ratio->Draw();
    c_nMPI_ratio->Print("plots_kinematics_Y1SXbXb_to_Bu/c_nMPI_ratio.pdf");



    // -------------------------------------------------------------------------
    // Plot multiplicity
    // -------------------------------------------------------------------------
    TCanvas* c_multiplicity = new TCanvas("c_multiplicity","c_multiplicity");
    TH1D* h_multiplicity_pythia  = new TH1D("h_multiplicity_pythia", "h_multiplicity_pythia", 20,0,80);
    TH1D* h_multiplicity_pythia_DPS_Mix  = new TH1D("h_multiplicity_pythia_DPS_Mix", "h_multiplicity_pythia_DPS_Mix", 20,0,80);
    TH1D* h_multiplicity_pythia_SPS_Mix  = new TH1D("h_multiplicity_pythia_SPS_Mix", "h_multiplicity_pythia_SPS_Mix", 20,0,80);
    TH1D* h_multiplicity_pythia_DPS_Sep  = new TH1D("h_multiplicity_pythia_DPS_Sep", "h_multiplicity_pythia_DPS_Sep", 20,0,80);
    TH1D* h_multiplicity_pythia_SPS_Sep  = new TH1D("h_multiplicity_pythia_SPS_Sep", "h_multiplicity_pythia_SPS_Sep", 20,0,80);

    TH1D* h_multiplicity_pythia_Dz  = new TH1D("h_multiplicity_pythia_Dz", "h_multiplicity_pythia_Dz", 20,0,80);

    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia",cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_DPS_Mix",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Mix);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_SPS_Mix",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Mix);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_DPS_Sep",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Sep);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_SPS_Sep",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Sep);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia",cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree_all->Draw("nChargedInLHCb>>h_multiplicity_pythia_Dz",cuts_Dz_all+Bu_fid);

    h_multiplicity_pythia->Scale(scalefactor_pythia);
    h_multiplicity_pythia_DPS_Mix->Scale(scalefactor_pythia);
    h_multiplicity_pythia_SPS_Mix->Scale(scalefactor_pythia);
    h_multiplicity_pythia_DPS_Sep->Scale(scalefactor_pythia);
    h_multiplicity_pythia_SPS_Sep->Scale(scalefactor_pythia);
    h_multiplicity_pythia_Dz->Scale(scalefactor_pythia_all);


    // Black -> With MPI
    h_multiplicity_pythia->SetLineColor(kBlack);

    h_multiplicity_pythia_Dz->SetLineColor(kBlack);
    h_multiplicity_pythia_Dz->SetLineStyle(kDashed);


    h_multiplicity_pythia_DPS_Mix->SetLineColor(kBlue);
    h_multiplicity_pythia_SPS_Mix->SetLineColor(kRed);

    h_multiplicity_pythia_DPS_Sep->SetLineColor(kBlue-9);
    h_multiplicity_pythia_SPS_Sep->SetLineColor(kRed-9);

    h_multiplicity_pythia_DPS_Sep->SetLineStyle(kDotted);
    h_multiplicity_pythia_SPS_Sep->SetLineStyle(kDotted);

    h_multiplicity_pythia->SetMaximum(1.05*max({h_multiplicity_pythia->GetMaximum(),
                                      h_multiplicity_pythia_Dz->GetMaximum(),
                                      h_multiplicity_pythia_DPS_Mix->GetMaximum(),
                                      h_multiplicity_pythia_SPS_Mix->GetMaximum(),
                                      h_multiplicity_pythia_DPS_Sep->GetMaximum(),
                                      h_multiplicity_pythia_SPS_Sep->GetMaximum()
                                      }));
    
    double h_multiplicity_pythia_bin_width = h_multiplicity_pythia->GetXaxis()->GetBinWidth(2);
    h_multiplicity_pythia->SetTitle(Form(";N_{Charged}^{2.0<#eta<4.5}; d#sigma/dN [%.1f #mub]",h_multiplicity_pythia_bin_width));
    h_multiplicity_pythia->Draw("hist");
    h_multiplicity_pythia_Dz->Draw("same hist");
    h_multiplicity_pythia_DPS_Mix->Draw("same hist");
    h_multiplicity_pythia_SPS_Mix->Draw("same hist");
    h_multiplicity_pythia_DPS_Sep->Draw("same hist");
    h_multiplicity_pythia_SPS_Sep->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_multiplicity = new TLegend(0.6,0.6,0.9,0.9);
    leg_multiplicity->AddEntry(h_multiplicity_pythia_Dz,     "B^{+} Pythia","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia,        "#Upsilon(1S)X_{b}X_{#bar{b}} Pythia","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_DPS_Mix,"#Upsilon(1S)X_{b}X_{#bar{b}} DPS Mixed","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_DPS_Sep,"#Upsilon(1S)X_{b}X_{#bar{b}} DPS Unmixed","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_SPS_Mix,"#Upsilon(1S)X_{b}X_{#bar{b}} SPS Mixed","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_SPS_Sep,"#Upsilon(1S)X_{b}X_{#bar{b}} SPS Unmixed","l");

    leg_multiplicity->SetTextFont(132);
    leg_multiplicity->SetFillStyle(0);
    leg_multiplicity->Draw();
    c_multiplicity->Print("plots_kinematics_Y1SXbXb_to_Bu/c_multiplicity.pdf");

    TCanvas* c_multiplicity_ratio = new TCanvas("c_multiplicity_ratio","c_multiplicity_ratio");

    TH1D* h_multiplicity_pythia_ratio  = new TH1D("h_multiplicity_pythia_ratio", "h_multiplicity_pythia_ratio", 20,0,80);
    TH1D* h_multiplicity_pythia_DPS_Mix_ratio  = new TH1D("h_multiplicity_pythia_DPS_Mix_ratio", "h_multiplicity_pythia_DPS_Mix_ratio", 20,0,80);
    TH1D* h_multiplicity_pythia_SPS_Mix_ratio  = new TH1D("h_multiplicity_pythia_SPS_Mix_ratio", "h_multiplicity_pythia_SPS_Mix_ratio", 20,0,80);
    TH1D* h_multiplicity_pythia_DPS_Sep_ratio  = new TH1D("h_multiplicity_pythia_DPS_Sep_ratio", "h_multiplicity_pythia_DPS_Sep_ratio", 20,0,80);
    TH1D* h_multiplicity_pythia_SPS_Sep_ratio  = new TH1D("h_multiplicity_pythia_SPS_Sep_ratio", "h_multiplicity_pythia_SPS_Sep_ratio", 20,0,80);

    h_multiplicity_pythia_ratio->Divide(h_multiplicity_pythia,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_DPS_Mix_ratio->Divide(h_multiplicity_pythia_DPS_Mix,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_SPS_Mix_ratio->Divide(h_multiplicity_pythia_SPS_Mix,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_DPS_Sep_ratio->Divide(h_multiplicity_pythia_DPS_Sep,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_SPS_Sep_ratio->Divide(h_multiplicity_pythia_SPS_Sep,h_multiplicity_pythia_Dz);


    h_multiplicity_pythia_ratio->Scale(100);
    h_multiplicity_pythia_DPS_Mix_ratio->Scale(100);
    h_multiplicity_pythia_SPS_Mix_ratio->Scale(100);
    h_multiplicity_pythia_DPS_Sep_ratio->Scale(100);
    h_multiplicity_pythia_SPS_Sep_ratio->Scale(100);
    
        // Black -> With MPI
    h_multiplicity_pythia_ratio->SetLineColor(kBlack);


    h_multiplicity_pythia_DPS_Mix_ratio->SetLineColor(kBlue);
    h_multiplicity_pythia_DPS_Mix_ratio->SetMarkerColor(kBlue);

    h_multiplicity_pythia_SPS_Mix_ratio->SetLineColor(kRed);
    h_multiplicity_pythia_SPS_Mix_ratio->SetMarkerColor(kRed);

    h_multiplicity_pythia_DPS_Sep_ratio->SetLineColor(kBlue-9);
    h_multiplicity_pythia_DPS_Sep_ratio->SetMarkerColor(kBlue-9);

    h_multiplicity_pythia_SPS_Sep_ratio->SetLineColor(kRed-9);
    h_multiplicity_pythia_SPS_Sep_ratio->SetMarkerColor(kRed-9);
    h_multiplicity_pythia_DPS_Sep_ratio->SetLineStyle(kDotted);
    h_multiplicity_pythia_SPS_Sep_ratio->SetLineStyle(kDotted);

    h_multiplicity_pythia_ratio->SetMaximum(1.05*max({h_multiplicity_pythia_ratio->GetMaximum(),
                                              h_multiplicity_pythia_DPS_Mix_ratio->GetMaximum(),
                                              h_multiplicity_pythia_SPS_Mix_ratio->GetMaximum(),
                                              h_multiplicity_pythia_DPS_Sep_ratio->GetMaximum(),
                                              h_multiplicity_pythia_SPS_Sep_ratio->GetMaximum()
                                      }));
    h_multiplicity_pythia_ratio->SetTitle(";N_{Charged}^{2.0<#eta<4.5};#scale[0.6]{#frac{d#sigma(#Upsilon(1S)X_{b}X_{#bar{b}})}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_multiplicity_pythia_ratio->SetMinimum(0);
    h_multiplicity_pythia_ratio->SetMaximum(0.07);
    h_multiplicity_pythia_ratio->Draw();
    h_multiplicity_pythia_DPS_Mix_ratio->Draw("same");
    h_multiplicity_pythia_SPS_Mix_ratio->Draw("same");
    h_multiplicity_pythia_DPS_Sep_ratio->Draw("same");
    h_multiplicity_pythia_SPS_Sep_ratio->Draw("same");


    TLegend* leg_multiplicity_ratio = new TLegend(0.13,0.55,0.72,0.93);
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_ratio,        "Pythia - All","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_DPS_Mix_ratio,"Pythia - Just DPS Mixed",   "pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_DPS_Sep_ratio,"Pythia - Just DPS Unmixed","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_SPS_Mix_ratio,"Pythia - Just SPS Mixed",   "pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_SPS_Sep_ratio,"Pythia - Just SPS Unmixed","pe");
    leg_multiplicity_ratio->SetTextFont(132);
    leg_multiplicity_ratio->SetFillStyle(0);
    leg_multiplicity_ratio->Draw();
    c_multiplicity_ratio->Print("plots_kinematics_Y1SXbXb_to_Bu/c_multiplicity_ratio.pdf");

    // // angles
    SetLHCbStyle("square");
    int n_bins = 10;

    TH2D* h_processes_delta_phi_2d_pythia_all = new TH2D("h_processes_delta_phi_2d_pythia_all", 
                                    "h_processes_delta_phi_2d_pythia_all;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );
    TH2D* h_processes_delta_phi_2d_pythia_SPS_Mix = new TH2D("h_processes_delta_phi_2d_pythia_SPS_Mix", 
                                    "h_processes_delta_phi_2d_pythia_SPS_Mix;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_delta_phi_2d_pythia_DPS_Mix = new TH2D("h_processes_delta_phi_2d_pythia_DPS_Mix", 
                                    "h_processes_delta_phi_2d_pythia_DPS_Mix;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );
    TH2D* h_processes_delta_phi_2d_pythia_SPS_Sep = new TH2D("h_processes_delta_phi_2d_pythia_SPS_Sep", 
                                    "h_processes_delta_phi_2d_pythia_SPS_Sep;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_delta_phi_2d_pythia_DPS_Sep = new TH2D("h_processes_delta_phi_2d_pythia_DPS_Sep", 
                                    "h_processes_delta_phi_2d_pythia_DPS_Sep;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );



    TCanvas* can_delta_phi_2d_pythia_all = new TCanvas("can_delta_phi_2d_pythia_all","can_delta_phi_2d_pythia_all",550,500);

    pythia_tree->Draw( "angle_upsilon1s_bx:angle_upsilon1s_bx2>>h_processes_delta_phi_2d_pythia_all",      cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree->Draw( "angle_upsilon1s_bx:angle_upsilon1s_bx2>>h_processes_delta_phi_2d_pythia_DPS_Mix",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Mix);
    pythia_tree->Draw( "angle_upsilon1s_bx:angle_upsilon1s_bx2>>h_processes_delta_phi_2d_pythia_SPS_Mix",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Mix);
    pythia_tree->Draw( "angle_upsilon1s_bx:angle_upsilon1s_bx2>>h_processes_delta_phi_2d_pythia_DPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Sep);
    pythia_tree->Draw( "angle_upsilon1s_bx:angle_upsilon1s_bx2>>h_processes_delta_phi_2d_pythia_SPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Sep);

    h_processes_delta_phi_2d_pythia_all->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_DPS_Mix->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_SPS_Mix->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_DPS_Sep->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_SPS_Sep->SetMinimum(0);
    
    h_processes_delta_phi_2d_pythia_all->Draw("colz");
    can_delta_phi_2d_pythia_all->Print("plots_kinematics_Y1SXbXb_to_Bu/can_delta_phi_2d_pythia_all.pdf");

    TCanvas* can_delta_phi_2d_pythia_SPS_Mix = new TCanvas("can_delta_phi_2d_pythia_SPS_Mix","can_delta_phi_2d_pythia_SPS_Mix",550,500);
    h_processes_delta_phi_2d_pythia_SPS_Mix->Draw("colz");
    can_delta_phi_2d_pythia_SPS_Mix->Print("plots_kinematics_Y1SXbXb_to_Bu/can_delta_phi_2d_pythia_SPS_Mix.pdf");
    
    TCanvas* can_delta_phi_2d_pythia_DPS_Mix = new TCanvas("can_delta_phi_2d_pythia_DPS_Mix","can_delta_phi_2d_pythia_DPS_Mix",550,500);
    h_processes_delta_phi_2d_pythia_DPS_Mix->Draw("colz");
    can_delta_phi_2d_pythia_DPS_Mix->Print("plots_kinematics_Y1SXbXb_to_Bu/can_delta_phi_2d_pythia_DPS_Mix.pdf");

    TCanvas* can_delta_phi_2d_pythia_SPS_Sep = new TCanvas("can_delta_phi_2d_pythia_SPS_Sep","can_delta_phi_2d_pythia_SPS_Sep",550,500);
    h_processes_delta_phi_2d_pythia_SPS_Sep->Draw("colz");
    can_delta_phi_2d_pythia_SPS_Sep->Print("plots_kinematics_Y1SXbXb_to_Bu/can_delta_phi_2d_pythia_SPS_Sep.pdf");
    
    TCanvas* can_delta_phi_2d_pythia_DPS_Sep = new TCanvas("can_delta_phi_2d_pythia_DPS_Sep","can_delta_phi_2d_pythia_DPS_Sep",550,500);
    h_processes_delta_phi_2d_pythia_DPS_Sep->Draw("colz");
    can_delta_phi_2d_pythia_DPS_Sep->Print("plots_kinematics_Y1SXbXb_to_Bu/can_delta_phi_2d_pythia_DPS_Sep.pdf");


    // -------------------------------------------------------------------------

    TH2D* h_processes_delta_eta_2d_pythia_all = new TH2D("h_processes_delta_eta_2d_pythia_all", 
                                    "h_processes_delta_eta_2d_pythia_all;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    20,0,10.0,
                                    20,0,10.0 );
    TH2D* h_processes_delta_eta_2d_pythia_SPS_Mix = new TH2D("h_processes_delta_eta_2d_pythia_SPS_Mix", 
                                    "h_processes_delta_eta_2d_pythia_SPS_Mix;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    20,0,10.0,
                                    20,0,10.0 );

    TH2D* h_processes_delta_eta_2d_pythia_DPS_Mix = new TH2D("h_processes_delta_eta_2d_pythia_DPS_Mix", 
                                    "h_processes_delta_eta_2d_pythia_DPS_Mix;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    20,0,10.0,
                                    20,0,10.0 );
    TH2D* h_processes_delta_eta_2d_pythia_SPS_Sep = new TH2D("h_processes_delta_eta_2d_pythia_SPS_Sep", 
                                    "h_processes_delta_eta_2d_pythia_SPS_Sep;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    20,0,10.0,
                                    20,0,10.0 );

    TH2D* h_processes_delta_eta_2d_pythia_DPS_Sep = new TH2D("h_processes_delta_eta_2d_pythia_DPS_Sep", 
                                    "h_processes_delta_eta_2d_pythia_DPS_Sep;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{b}}) [rad]",
                                    20,0,10.0,
                                    20,0,10.0 );



    TCanvas* can_delta_eta_2d_pythia_all = new TCanvas("can_delta_eta_2d_pythia_all","can_delta_eta_2d_pythia_all",550,500);

    pythia_tree->Draw( "delta_eta_upsilon1s_bx:delta_eta_upsilon1s_bx2>>h_processes_delta_eta_2d_pythia_all",      cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree->Draw( "delta_eta_upsilon1s_bx:delta_eta_upsilon1s_bx2>>h_processes_delta_eta_2d_pythia_DPS_Mix",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Mix);
    pythia_tree->Draw( "delta_eta_upsilon1s_bx:delta_eta_upsilon1s_bx2>>h_processes_delta_eta_2d_pythia_SPS_Mix",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Mix);
    pythia_tree->Draw( "delta_eta_upsilon1s_bx:delta_eta_upsilon1s_bx2>>h_processes_delta_eta_2d_pythia_DPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Sep);
    pythia_tree->Draw( "delta_eta_upsilon1s_bx:delta_eta_upsilon1s_bx2>>h_processes_delta_eta_2d_pythia_SPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Sep);

    h_processes_delta_eta_2d_pythia_all->SetMinimum(0);
    h_processes_delta_eta_2d_pythia_DPS_Mix->SetMinimum(0);
    h_processes_delta_eta_2d_pythia_SPS_Mix->SetMinimum(0);
    h_processes_delta_eta_2d_pythia_DPS_Sep->SetMinimum(0);
    h_processes_delta_eta_2d_pythia_SPS_Sep->SetMinimum(0);
    
    h_processes_delta_eta_2d_pythia_all->Draw("colz");

    TCanvas* can_delta_eta_2d_pythia_SPS_Mix = new TCanvas("can_delta_eta_2d_pythia_SPS_Mix","can_delta_eta_2d_pythia_SPS_Mix",550,500);
    h_processes_delta_eta_2d_pythia_SPS_Mix->Draw("colz");
    
    TCanvas* can_delta_eta_2d_pythia_DPS_Mix = new TCanvas("can_delta_eta_2d_pythia_DPS_Mix","can_delta_eta_2d_pythia_DPS_Mix",550,500);
    h_processes_delta_eta_2d_pythia_DPS_Mix->Draw("colz");

    TCanvas* can_delta_eta_2d_pythia_SPS_Sep = new TCanvas("can_delta_eta_2d_pythia_SPS_Sep","can_delta_eta_2d_pythia_SPS_Sep",550,500);
    h_processes_delta_eta_2d_pythia_SPS_Sep->Draw("colz");
    
    TCanvas* can_delta_eta_2d_pythia_DPS_Sep = new TCanvas("can_delta_eta_2d_pythia_DPS_Sep","can_delta_eta_2d_pythia_DPS_Sep",550,500);
    h_processes_delta_eta_2d_pythia_DPS_Sep->Draw("colz");


    // -------------------------------------------------------------------------

    TH2D* h_processes_pt_y_2d_pythia_all = new TH2D("h_processes_pt_y_2d_pythia_all", 
                                    "h_processes_pt_y_2d_pythia_all;#it{y};#it{p}_{T} [GeV/#it{c}^{}]",
                                    20,-10.0,10.0,
                                    20,0.0,20.0 );
    TH2D* h_processes_pt_y_2d_pythia_SPS_Mix = new TH2D("h_processes_pt_y_2d_pythia_SPS_Mix", 
                                    "h_processes_pt_y_2d_pythia_SPS_Mix;#it{y};#it{p}_{T} [GeV/#it{c}^{}]",
                                    20,-10.0,10.0,
                                    20,0.0,20.0 );

    TH2D* h_processes_pt_y_2d_pythia_DPS_Mix = new TH2D("h_processes_pt_y_2d_pythia_DPS_Mix", 
                                    "h_processes_pt_y_2d_pythia_DPS_Mix;#it{y};#it{p}_{T} [GeV/#it{c}^{}]",
                                    20,-10.0,10.0,
                                    20,0.0,20.0 );
    TH2D* h_processes_pt_y_2d_pythia_SPS_Sep = new TH2D("h_processes_pt_y_2d_pythia_SPS_Sep", 
                                    "h_processes_pt_y_2d_pythia_SPS_Sep;#it{y};#it{p}_{T} [GeV/#it{c}^{}]",
                                    20,-10.0,10.0,
                                    20,0.0,20.0 );

    TH2D* h_processes_pt_y_2d_pythia_DPS_Sep = new TH2D("h_processes_pt_y_2d_pythia_DPS_Sep", 
                                    "h_processes_pt_y_2d_pythia_DPS_Sep;#it{y};#it{p}_{T} [GeV/#it{c}^{}]",
                                    20,-10.0,10.0,
                                    20,0.0,20.0 );



    TCanvas* can_pt_y_2d_pythia_all = new TCanvas("can_pt_y_2d_pythia_all","can_pt_y_2d_pythia_all",550,500);

    pythia_tree->Draw( "upsilon1s_pt:upsilon1s_y>>h_processes_pt_y_2d_pythia_all",      cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree->Draw( "upsilon1s_pt:upsilon1s_y>>h_processes_pt_y_2d_pythia_DPS_Mix",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Mix);
    pythia_tree->Draw( "upsilon1s_pt:upsilon1s_y>>h_processes_pt_y_2d_pythia_SPS_Mix",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Mix);
    pythia_tree->Draw( "upsilon1s_pt:upsilon1s_y>>h_processes_pt_y_2d_pythia_DPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Sep);
    pythia_tree->Draw( "upsilon1s_pt:upsilon1s_y>>h_processes_pt_y_2d_pythia_SPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Sep);

    h_processes_pt_y_2d_pythia_all->SetMinimum(0);
    h_processes_pt_y_2d_pythia_DPS_Mix->SetMinimum(0);
    h_processes_pt_y_2d_pythia_SPS_Mix->SetMinimum(0);
    h_processes_pt_y_2d_pythia_DPS_Sep->SetMinimum(0);
    h_processes_pt_y_2d_pythia_SPS_Sep->SetMinimum(0);
    
    h_processes_pt_y_2d_pythia_all->Draw("colz");

    TCanvas* can_pt_y_2d_pythia_SPS_Mix = new TCanvas("can_pt_y_2d_pythia_SPS_Mix","can_pt_y_2d_pythia_SPS_Mix",550,500);
    h_processes_pt_y_2d_pythia_SPS_Mix->Draw("colz");
    
    TCanvas* can_pt_y_2d_pythia_DPS_Mix = new TCanvas("can_pt_y_2d_pythia_DPS_Mix","can_pt_y_2d_pythia_DPS_Mix",550,500);
    h_processes_pt_y_2d_pythia_DPS_Mix->Draw("colz");

    TCanvas* can_pt_y_2d_pythia_SPS_Sep = new TCanvas("can_pt_y_2d_pythia_SPS_Sep","can_pt_y_2d_pythia_SPS_Sep",550,500);
    h_processes_pt_y_2d_pythia_SPS_Sep->Draw("colz");
    
    TCanvas* can_pt_y_2d_pythia_DPS_Sep = new TCanvas("can_pt_y_2d_pythia_DPS_Sep","can_pt_y_2d_pythia_DPS_Sep",550,500);
    h_processes_pt_y_2d_pythia_DPS_Sep->Draw("colz");
    // -------------------------------------------------------------------------
    TH1D* h_processes_delta_R_2d_pythia_all = new TH1D("h_processes_delta_R_2d_pythia_all", 
                                    "h_processes_delta_R_2d_pythia_all;#DeltaR(#Upsilon(1S), X_{b}) [rad];Arbitary units",
                                    20,0,8);
    TH1D* h_processes_delta_R_2d_pythia_SPS_Mix = new TH1D("h_processes_delta_R_2d_pythia_SPS_Mix", 
                                    "h_processes_delta_R_2d_pythia_SPS_Mix;#DeltaR(#Upsilon(1S), X_{b}) [rad];Arbitary units",
                                    20,0,8);

    TH1D* h_processes_delta_R_2d_pythia_DPS_Mix = new TH1D("h_processes_delta_R_2d_pythia_DPS_Mix", 
                                    "h_processes_delta_R_2d_pythia_DPS_Mix;#DeltaR(#Upsilon(1S), X_{b}) [rad];Arbitary units",
                                    20,0,8);
    TH1D* h_processes_delta_R_2d_pythia_SPS_Sep = new TH1D("h_processes_delta_R_2d_pythia_SPS_Sep", 
                                    "h_processes_delta_R_2d_pythia_SPS_Sep;#DeltaR(#Upsilon(1S), X_{b}) [rad];Arbitary units",
                                    20,0,8);

    TH1D* h_processes_delta_R_2d_pythia_DPS_Sep = new TH1D("h_processes_delta_R_2d_pythia_DPS_Sep", 
                                    "h_processes_delta_R_2d_pythia_DPS_Sep;#DeltaR(#Upsilon(1S), X_{b}) [rad];Arbitary units",
                                    20,0,8);



    TCanvas* can_delta_R_2d_pythia_all = new TCanvas("can_delta_R_2d_pythia_all","can_delta_R_2d_pythia_all");

    pythia_tree->Draw( "delta_R_bx2_bx>>h_processes_delta_R_2d_pythia_all",      cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree->Draw( "delta_R_bx2_bx>>h_processes_delta_R_2d_pythia_DPS_Mix",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Mix);
    pythia_tree->Draw( "delta_R_bx2_bx>>h_processes_delta_R_2d_pythia_SPS_Mix",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Mix);
    pythia_tree->Draw( "delta_R_bx2_bx>>h_processes_delta_R_2d_pythia_DPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Sep);
    pythia_tree->Draw( "delta_R_bx2_bx>>h_processes_delta_R_2d_pythia_SPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Sep);

    h_processes_delta_R_2d_pythia_all->SetMinimum(0);
    h_processes_delta_R_2d_pythia_DPS_Mix->SetMinimum(0);
    h_processes_delta_R_2d_pythia_SPS_Mix->SetMinimum(0);
    h_processes_delta_R_2d_pythia_DPS_Sep->SetMinimum(0);
    h_processes_delta_R_2d_pythia_SPS_Sep->SetMinimum(0);

    h_processes_delta_R_2d_pythia_DPS_Mix->SetLineColor(kBlue);
    h_processes_delta_R_2d_pythia_SPS_Mix->SetLineColor(kRed);
    h_processes_delta_R_2d_pythia_DPS_Sep->SetLineColor(kBlue-9);
    h_processes_delta_R_2d_pythia_SPS_Sep->SetLineColor(kRed-9);


    h_processes_delta_R_2d_pythia_DPS_Mix->Scale(1.0/h_processes_delta_R_2d_pythia_DPS_Mix->Integral());
    h_processes_delta_R_2d_pythia_SPS_Mix->Scale(1.0/h_processes_delta_R_2d_pythia_SPS_Mix->Integral());
    h_processes_delta_R_2d_pythia_DPS_Sep->Scale(1.0/h_processes_delta_R_2d_pythia_DPS_Sep->Integral());
    h_processes_delta_R_2d_pythia_SPS_Sep->Scale(1.0/h_processes_delta_R_2d_pythia_SPS_Sep->Integral());
    
    // h_processes_delta_R_2d_pythia_all->Draw("");
    // can_delta_R_2d_pythia_all->Print("plots_kinematics_Y1SXbXb_to_Bu/can_delta_R_2d_pythia_all.pdf");

    h_processes_delta_R_2d_pythia_SPS_Mix->Draw("hist ");
    h_processes_delta_R_2d_pythia_DPS_Mix->Draw("hist same");
    h_processes_delta_R_2d_pythia_SPS_Sep->Draw("hist same");
    h_processes_delta_R_2d_pythia_DPS_Sep->Draw("hist same");

}

