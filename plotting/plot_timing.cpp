#include "tools.h"

std::pair<int,double> getTimePerbb(std::string);
std::pair<int,double> getTimePercc(std::string);
std::pair<int,double> getTimePerBc(std::string);
std::pair<std::pair<int,int>,int> getPassThroughbb(std::string);
std::pair<std::pair<int,int>,int> getPassThroughcc(std::string);
void getTimePerBHadron(std::string);
void getTimePerDHadron(std::string);
double median(std::vector<double> &v);
double getRelEffError(int pass, int total);
double getAbsEffError(int pass, int total);

void plot_timing(){

    SetLHCbStyle("oneD");

    std::pair<int,double> time_UH_bb_noPT_noMPI = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_noVetoMPI_Scale_4.000000_N_1000000_PartMod1.root");
    std::pair<int,double> time_UH_cc_noPT_noMPI = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_noVetoMPI_Scale_4.000000_N_1000000_PartMod1.root");

    std::pair<int,double> time_UH_noPT_3_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_3.000000_N_200000_PartMod1.root");
    std::pair<int,double> time_UH_noPT_3_5 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_3.500000_N_200000_PartMod1.root");
    std::pair<int,double> time_UH_noPT_4_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_4.000000_N_200000_PartMod1.root");
    std::pair<int,double> time_UH_noPT_4_5 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_4.500000_N_200000_PartMod1.root");
    std::pair<int,double> time_UH_noPT_5_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_5.000000_N_200000_PartMod1.root");

    std::pair<int,double> time_UH_3_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_3.000000_N_20000_PartMod1.root");
    std::pair<int,double> time_UH_3_5 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_3.500000_N_20000_PartMod1.root");
    std::pair<int,double> time_UH_4_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_20000_PartMod1.root");
    std::pair<int,double> time_UH_4_5 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.500000_N_20000_PartMod1.root");
    std::pair<int,double> time_UH_5_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_5.000000_N_20000_PartMod1.root");

    std::pair<int,double> time_UH_cc_noPT_1_00 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_1.000000_Quark_4_N_200000_PartMod1.root");
    std::pair<int,double> time_UH_cc_noPT_1_25 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_1.250000_Quark_4_N_200000_PartMod1.root");
    std::pair<int,double> time_UH_cc_noPT_1_50 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_1.500000_Quark_4_N_200000_PartMod1.root");
    std::pair<int,double> time_UH_cc_noPT_1_75 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_1.750000_Quark_4_N_200000_PartMod1.root");
    std::pair<int,double> time_UH_cc_noPT_2_00 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_2.000000_Quark_4_N_200000_PartMod1.root");

    std::pair<int,double> time_UH_cc_1_00 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_1.000000_Quark_4_N_20000_PartMod1.root");
    std::pair<int,double> time_UH_cc_1_25 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_1.250000_Quark_4_N_20000_PartMod1.root");
    std::pair<int,double> time_UH_cc_1_50 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_1.500000_Quark_4_N_20000_PartMod1.root");
    std::pair<int,double> time_UH_cc_1_75 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_1.750000_Quark_4_N_20000_PartMod1.root");
    std::pair<int,double> time_UH_cc_2_00 = getTimePercc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_2.000000_Quark_4_N_20000_PartMod1.root");
  
    // Vincia shower   
    std::pair<int,double> time_vincia_UH_bb_noPT_noMPI = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_noVetoPT_noVetoMPI_Scale_4.000000_N_200000.root");

    std::pair<int,double> time_vincia_UH_3_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_3.000000_N_2000.root");
    std::pair<int,double> time_vincia_UH_3_5 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_3.500000_N_2000.root");
    std::pair<int,double> time_vincia_UH_4_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_2000.root");
    std::pair<int,double> time_vincia_UH_4_5 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.500000_N_2000.root");
    std::pair<int,double> time_vincia_UH_5_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_5.000000_N_2000.root");

    std::pair<int,double> time_vincia_UH_noPT_3_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_3.000000_N_200000.root");
    std::pair<int,double> time_vincia_UH_noPT_3_5 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_3.500000_N_200000.root");
    std::pair<int,double> time_vincia_UH_noPT_4_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_4.000000_N_200000.root");
    std::pair<int,double> time_vincia_UH_noPT_4_5 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_4.500000_N_200000.root");
    std::pair<int,double> time_vincia_UH_noPT_5_0 = getTimePerbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_Scale_5.000000_N_200000.root");


    std::cout << "None n_bb: " << time_UH_bb_noPT_noMPI.first << "\ttime: " << time_UH_bb_noPT_noMPI.second << "\ttimeperb: " << time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first << std::endl;
    std::cout << "None n_cc: " << time_UH_cc_noPT_noMPI.first << "\ttime: " << time_UH_cc_noPT_noMPI.second << "\ttimeperc: " << time_UH_cc_noPT_noMPI.second/time_UH_cc_noPT_noMPI.first << std::endl;
    std::cout << std::endl;

    std::cout <<" -------- UserHook bb no PT " <<  std::endl;
    std::cout << "3.0 n_bb: " << time_UH_noPT_3_0.first << "\ttime: " << time_UH_noPT_3_0.second << "\ttimeperb: " << time_UH_noPT_3_0.second/time_UH_noPT_3_0.first << std::endl;
    std::cout << "3.5 n_bb: " << time_UH_noPT_3_5.first << "\ttime: " << time_UH_noPT_3_5.second << "\ttimeperb: " << time_UH_noPT_3_5.second/time_UH_noPT_3_5.first << std::endl;
    std::cout << "4.0 n_bb: " << time_UH_noPT_4_0.first << "\ttime: " << time_UH_noPT_4_0.second << "\ttimeperb: " << time_UH_noPT_4_0.second/time_UH_noPT_4_0.first << std::endl;
    std::cout << "4.5 n_bb: " << time_UH_noPT_4_5.first << "\ttime: " << time_UH_noPT_4_5.second << "\ttimeperb: " << time_UH_noPT_4_5.second/time_UH_noPT_4_5.first << std::endl;
    std::cout << "5.0 n_bb: " << time_UH_noPT_5_0.first << "\ttime: " << time_UH_noPT_5_0.second << "\ttimeperb: " << time_UH_noPT_5_0.second/time_UH_noPT_5_0.first << std::endl;
    std::cout << std::endl;

    std::cout <<" -------- UserHook bb " <<  std::endl;
    std::cout << "3.0 n_bb: " << time_UH_3_0.first << "\ttime: " << time_UH_3_0.second << "\ttimeperb: " << time_UH_3_0.second/time_UH_3_0.first << std::endl;
    std::cout << "3.5 n_bb: " << time_UH_3_5.first << "\ttime: " << time_UH_3_5.second << "\ttimeperb: " << time_UH_3_5.second/time_UH_3_5.first << std::endl;
    std::cout << "4.0 n_bb: " << time_UH_4_0.first << "\ttime: " << time_UH_4_0.second << "\ttimeperb: " << time_UH_4_0.second/time_UH_4_0.first << std::endl;
    std::cout << "4.5 n_bb: " << time_UH_4_5.first << "\ttime: " << time_UH_4_5.second << "\ttimeperb: " << time_UH_4_5.second/time_UH_4_5.first << std::endl;
    std::cout << "5.0 n_bb: " << time_UH_5_0.first << "\ttime: " << time_UH_5_0.second << "\ttimeperb: " << time_UH_5_0.second/time_UH_5_0.first << std::endl;
    std::cout << std::endl;

    std::cout <<" -------- UserHook cc no PT" <<  std::endl;
    std::cout << "1.00 n_cc: " << time_UH_cc_noPT_1_00.first << "\ttime: " << time_UH_cc_noPT_1_00.second << "\ttimepercc: " << time_UH_cc_noPT_1_00.second/time_UH_cc_noPT_1_00.first << std::endl;
    std::cout << "1.25 n_cc: " << time_UH_cc_noPT_1_25.first << "\ttime: " << time_UH_cc_noPT_1_25.second << "\ttimepercc: " << time_UH_cc_noPT_1_25.second/time_UH_cc_noPT_1_25.first << std::endl;
    std::cout << "1.50 n_cc: " << time_UH_cc_noPT_1_50.first << "\ttime: " << time_UH_cc_noPT_1_50.second << "\ttimepercc: " << time_UH_cc_noPT_1_50.second/time_UH_cc_noPT_1_50.first << std::endl;
    std::cout << "1.75 n_cc: " << time_UH_cc_noPT_1_75.first << "\ttime: " << time_UH_cc_noPT_1_75.second << "\ttimepercc: " << time_UH_cc_noPT_1_75.second/time_UH_cc_noPT_1_75.first << std::endl;
    std::cout << "2.00 n_cc: " << time_UH_cc_noPT_2_00.first << "\ttime: " << time_UH_cc_noPT_2_00.second << "\ttimepercc: " << time_UH_cc_noPT_2_00.second/time_UH_cc_noPT_2_00.first << std::endl;
    std::cout << std::endl;
    
    std::cout <<" -------- UserHook cc " <<  std::endl;
    std::cout << "1.00 n_cc: " << time_UH_cc_1_00.first << "\ttime: " << time_UH_cc_1_00.second << "\ttimepercc: " << time_UH_cc_1_00.second/time_UH_cc_1_00.first << std::endl;
    std::cout << "1.25 n_cc: " << time_UH_cc_1_25.first << "\ttime: " << time_UH_cc_1_25.second << "\ttimepercc: " << time_UH_cc_1_25.second/time_UH_cc_1_25.first << std::endl;
    std::cout << "1.50 n_cc: " << time_UH_cc_1_50.first << "\ttime: " << time_UH_cc_1_50.second << "\ttimepercc: " << time_UH_cc_1_50.second/time_UH_cc_1_50.first << std::endl;
    std::cout << "1.75 n_cc: " << time_UH_cc_1_75.first << "\ttime: " << time_UH_cc_1_75.second << "\ttimepercc: " << time_UH_cc_1_75.second/time_UH_cc_1_75.first << std::endl;
    std::cout << "2.00 n_cc: " << time_UH_cc_2_00.first << "\ttime: " << time_UH_cc_2_00.second << "\ttimepercc: " << time_UH_cc_2_00.second/time_UH_cc_2_00.first << std::endl;
    std::cout << std::endl;

    std::cout <<" -------- Pass through bb"<< std::endl;

    std::pair<std::pair<int,int>,int> n_3_0 = getPassThroughbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_3.000000_N_1000000_PartMod1.root");
    std::pair<std::pair<int,int>,int> n_3_5 = getPassThroughbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_3.500000_N_1000000_PartMod1.root");
    std::pair<std::pair<int,int>,int> n_4_0 = getPassThroughbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_4.000000_N_1000000_PartMod1.root");
    std::pair<std::pair<int,int>,int> n_4_5 = getPassThroughbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_4.500000_N_1000000_PartMod1.root");
    std::pair<std::pair<int,int>,int> n_5_0 = getPassThroughbb("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_5.000000_N_1000000_PartMod1.root");


    std::pair<std::pair<int,int>,int> n_vincia_3_0 = getPassThroughbb("../output/from_lxplus/Timing/Clean/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_3.000000_N_1000000.root");
    std::pair<std::pair<int,int>,int> n_vincia_3_5 = getPassThroughbb("../output/from_lxplus/Timing/Clean/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_3.500000_N_1000000.root");
    std::pair<std::pair<int,int>,int> n_vincia_4_0 = getPassThroughbb("../output/from_lxplus/Timing/Clean/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_4.000000_N_1000000.root");
    std::pair<std::pair<int,int>,int> n_vincia_4_5 = getPassThroughbb("../output/from_lxplus/Timing/Clean/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_4.500000_N_1000000.root");
    std::pair<std::pair<int,int>,int> n_vincia_5_0 = getPassThroughbb("../output/from_lxplus/Timing/Clean/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_5.000000_N_1000000.root");

    std::cout << "3.0 all: " << n_3_0.first.first << "\tfail MPI: " << n_3_0.first.second << "\tfailPT: " << n_3_0.second << std::endl;
    std::cout << "3.5 all: " << n_3_5.first.first << "\tfail MPI: " << n_3_5.first.second << "\tfailPT: " << n_3_5.second << std::endl;
    std::cout << "4.0 all: " << n_4_0.first.first << "\tfail MPI: " << n_4_0.first.second << "\tfailPT: " << n_4_0.second << std::endl;
    std::cout << "4.5 all: " << n_4_5.first.first << "\tfail MPI: " << n_4_5.first.second << "\tfailPT: " << n_4_5.second << std::endl;
    std::cout << "5.0 all: " << n_5_0.first.first << "\tfail MPI: " << n_5_0.first.second << "\tfailPT: " << n_5_0.second << std::endl;
    std::cout << std::endl;

    std::cout <<" -------- Pass through cc"<< std::endl;

    // std::pair<std::pair<int,int>,int> n_cc_1_00 = getPassThroughcc("../output/from_lxplus/Timing/Clean/Charm/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_1.000000_N_900000.root");
    // std::pair<std::pair<int,int>,int> n_cc_1_25 = getPassThroughcc("../output/from_lxplus/Timing/Clean/Charm/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_1.250000_N_900000.root");
    // std::pair<std::pair<int,int>,int> n_cc_1_50 = getPassThroughcc("../output/from_lxplus/Timing/Clean/Charm/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_1.500000_N_900000.root");
    // std::pair<std::pair<int,int>,int> n_cc_1_75 = getPassThroughcc("../output/from_lxplus/Timing/Clean/Charm/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_1.750000_N_900000.root");
    // std::pair<std::pair<int,int>,int> n_cc_2_00 = getPassThroughcc("../output/from_lxplus/Timing/Clean/Charm/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_2.000000_N_1000000.root");

    std::pair<std::pair<int,int>,int> n_cc_1_00 = getPassThroughcc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_1.000000_Quark_4_N_1000000_PartMod1.root");
    std::pair<std::pair<int,int>,int> n_cc_1_25 = getPassThroughcc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_1.250000_Quark_4_N_1000000_PartMod1.root");
    std::pair<std::pair<int,int>,int> n_cc_1_50 = getPassThroughcc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_1.500000_Quark_4_N_1000000_PartMod1.root");
    std::pair<std::pair<int,int>,int> n_cc_1_75 = getPassThroughcc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_1.750000_Quark_4_N_1000000_PartMod1.root");
    std::pair<std::pair<int,int>,int> n_cc_2_00 = getPassThroughcc("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_2.000000_Quark_4_N_1000000_PartMod1.root");

    std::cout << "1.00 all: " << n_cc_1_00.first.first << "\tfail MPI: " << n_cc_1_00.first.second << "\tfailPT: " << n_cc_1_00.second << std::endl;
    std::cout << "1.25 all: " << n_cc_1_25.first.first << "\tfail MPI: " << n_cc_1_25.first.second << "\tfailPT: " << n_cc_1_25.second << std::endl;
    std::cout << "1.50 all: " << n_cc_1_50.first.first << "\tfail MPI: " << n_cc_1_50.first.second << "\tfailPT: " << n_cc_1_50.second << std::endl;
    std::cout << "1.75 all: " << n_cc_1_75.first.first << "\tfail MPI: " << n_cc_1_75.first.second << "\tfailPT: " << n_cc_1_75.second << std::endl;
    std::cout << "2.00 all: " << n_cc_2_00.first.first << "\tfail MPI: " << n_cc_2_00.first.second << "\tfailPT: " << n_cc_2_00.second << std::endl;
    std::cout << std::endl;



    // -------------------------------------------------------------------------
    // Bc times
    // -------------------------------------------------------------------------
    
    std::cout <<" -------- Bc times "<< std::endl;
 
    std::pair<int,double> Bc_4Steps = getTimePerBc("../output/Timing/main202output_SoftQCD_nd_UserHook_bc_Scale_4.000000_N_100_PartMod1.root");
    std::pair<int,double> Bc_3Steps = getTimePerBc("../output/Timing/main202output_SoftQCD_nd_UserHook_bc_noVetoMPI_Scale_4.000000_N_100_PartMod1.root");
    std::pair<int,double> Bc_2Steps = getTimePerBc("../output/Timing/main202output_SoftQCD_nd_UserHook_bc_noVetoPT_noVetoMPI_Scale_4.000000_N_100_PartMod1.root");
    std::pair<int,double> Bc_1Steps = getTimePerBc("../output/Timing/main202output_SoftQCD_nd_UserHook_bc_noVetoPT_noVetoMPI_noVetoParton_Scale_4.000000_N_100_PartMod1.root");
    std::pair<int,double> Bc_0Steps = getTimePerBc("../output/Timing/main202output_SoftQCD_nd_UserHook_bc_noVetoPT_noVetoMPI_noVetoParton_noVetoMass_Scale_4.000000_N_100_PartMod1.root");
    
    
    // std::pair<int,double> Bc_4Steps = getTimePerBc("../output/from_lxplus/Timing/main202output_SoftQCD_nd_UserHook_bc_Scale_4.000000_N_1000_noMPI_PartMod1.root");
    // std::pair<int,double> Bc_3Steps = getTimePerBc("../output/from_lxplus/Timing/main202output_SoftQCD_nd_UserHook_bc_noVetoMPI_Scale_4.000000_N_1000_noMPI_PartMod1.root");
    // std::pair<int,double> Bc_2Steps = getTimePerBc("../output/from_lxplus/Timing/main202output_SoftQCD_nd_UserHook_bc_noVetoPT_noVetoMPI_Scale_4.000000_N_1000_noMPI_PartMod1.root");
    // std::pair<int,double> Bc_1Steps = getTimePerBc("../output/from_lxplus/Timing/main202output_SoftQCD_nd_UserHook_bc_noVetoPT_noVetoMPI_noVetoParton_Scale_4.000000_N_1000_noMPI_PartMod1.root");
    // std::pair<int,double> Bc_0Steps = getTimePerBc("../output/from_lxplus/Timing/main202output_SoftQCD_nd_UserHook_bc_noVetoPT_noVetoMPI_noVetoParton_noVetoMass_Scale_4.000000_N_1000_noMPI_PartMod1.root");
    
    std::cout << "4 Steps : " << Bc_4Steps.first << "\ttime: " << Bc_4Steps.second << "\ttimeperBc: " << (Bc_4Steps.second/Bc_4Steps.first) << " seconds"<<  std::endl;
    std::cout << "3 Steps : " << Bc_3Steps.first << "\ttime: " << Bc_3Steps.second << "\ttimeperBc: " << (Bc_3Steps.second/Bc_3Steps.first) << " seconds"<<  std::endl;
    std::cout << "2 Steps : " << Bc_2Steps.first << "\ttime: " << Bc_2Steps.second << "\ttimeperBc: " << (Bc_2Steps.second/Bc_2Steps.first) << " seconds"<<  std::endl;
    std::cout << "1 Steps : " << Bc_1Steps.first << "\ttime: " << Bc_1Steps.second << "\ttimeperBc: " << (Bc_1Steps.second/Bc_1Steps.first) << " seconds"<<  std::endl;
    std::cout << "0 Steps : " << Bc_0Steps.first << "\ttime: " << Bc_0Steps.second << "\ttimeperBc: " << (Bc_0Steps.second/Bc_0Steps.first) << " seconds"<<  std::endl;
    std::cout << std::endl;

    // -------------------------------------------------------------------------
    // Graphs
    // -------------------------------------------------------------------------

    TCanvas* c_bb = new TCanvas("c_bb","c_bb");
    // ---- Create TGraphs -----
    TGraph* g_bb_UH = new TGraph();
    g_bb_UH->AddPoint(3.0,time_UH_3_0.second/time_UH_3_0.first);
    g_bb_UH->AddPoint(3.5,time_UH_3_5.second/time_UH_3_5.first);
    g_bb_UH->AddPoint(4.0,time_UH_4_0.second/time_UH_4_0.first);
    g_bb_UH->AddPoint(4.5,time_UH_4_5.second/time_UH_4_5.first);
    g_bb_UH->AddPoint(5.0,time_UH_5_0.second/time_UH_5_0.first);
    
    TGraph* g_bb_UH_noPT = new TGraph();
    g_bb_UH_noPT->AddPoint(3.0,time_UH_noPT_3_0.second/time_UH_noPT_3_0.first);
    g_bb_UH_noPT->AddPoint(3.5,time_UH_noPT_3_5.second/time_UH_noPT_3_5.first);
    g_bb_UH_noPT->AddPoint(4.0,time_UH_noPT_4_0.second/time_UH_noPT_4_0.first);
    g_bb_UH_noPT->AddPoint(4.5,time_UH_noPT_4_5.second/time_UH_noPT_4_5.first);
    g_bb_UH_noPT->AddPoint(5.0,time_UH_noPT_5_0.second/time_UH_noPT_5_0.first);
    g_bb_UH_noPT->SetLineColor(kRed);
    g_bb_UH_noPT->SetMarkerColor(kRed);

    g_bb_UH_noPT->SetTitle(";#hat{#it{p}}_{T} threshold [GeV/#it{c}];Time per b#bar{b} /Seconds");
    g_bb_UH_noPT->SetMinimum(0);
    g_bb_UH_noPT->SetMaximum(1.05*time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first);
    g_bb_UH_noPT->Draw("APL");
    g_bb_UH->Draw("PL same");


    TLegend* leg = new TLegend(0.2,0.4,0.7,0.6);
    leg->AddEntry(g_bb_UH_noPT,"Process veto","l");
    leg->AddEntry(g_bb_UH, "Process veto + Evolution veto","l");
    
    leg->Draw();
    leg->SetTextFont(132);
    leg->SetFillStyle(0);

    TLine* line = new TLine(2.8,time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first,5.2,time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first);
    line->SetLineColor(kGreen+2);
    line->Draw();
    c_bb->Print("plots_timing/c_bb.pdf");

    // -------------------------------------------------------------------------
    // Speed up bb 
    // -------------------------------------------------------------------------
    TCanvas* c_bb_speedup = new TCanvas("c_bb_speedup","c_bb_speedup");
    
    double benchmark_bb        = time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first;
    double benchmark_bb_vincia = time_vincia_UH_bb_noPT_noMPI.second/time_vincia_UH_bb_noPT_noMPI.first;

    // ---- Create TGraphs -----
    TGraph* g_bb_su_UH = new TGraph();
    g_bb_su_UH->AddPoint(3.0,benchmark_bb/(time_UH_3_0.second/time_UH_3_0.first));
    g_bb_su_UH->AddPoint(3.5,benchmark_bb/(time_UH_3_5.second/time_UH_3_5.first));
    g_bb_su_UH->AddPoint(4.0,benchmark_bb/(time_UH_4_0.second/time_UH_4_0.first));
    g_bb_su_UH->AddPoint(4.5,benchmark_bb/(time_UH_4_5.second/time_UH_4_5.first));
    g_bb_su_UH->AddPoint(5.0,benchmark_bb/(time_UH_5_0.second/time_UH_5_0.first));
    
    TGraph* g_bb_su_UH_noPT = new TGraph();
    g_bb_su_UH_noPT->AddPoint(3.0,(benchmark_bb)/(time_UH_noPT_3_0.second/time_UH_noPT_3_0.first));
    g_bb_su_UH_noPT->AddPoint(3.5,(benchmark_bb)/(time_UH_noPT_3_5.second/time_UH_noPT_3_5.first));
    g_bb_su_UH_noPT->AddPoint(4.0,(benchmark_bb)/(time_UH_noPT_4_0.second/time_UH_noPT_4_0.first));
    g_bb_su_UH_noPT->AddPoint(4.5,(benchmark_bb)/(time_UH_noPT_4_5.second/time_UH_noPT_4_5.first));
    g_bb_su_UH_noPT->AddPoint(5.0,(benchmark_bb)/(time_UH_noPT_5_0.second/time_UH_noPT_5_0.first));
    g_bb_su_UH_noPT->SetLineColor(kRed);
    g_bb_su_UH_noPT->SetMarkerColor(kRed);


    TGraph* g_bb_su_vin_UH = new TGraph();
    g_bb_su_vin_UH->AddPoint(3.0,benchmark_bb_vincia/(time_vincia_UH_3_0.second/time_vincia_UH_3_0.first));
    g_bb_su_vin_UH->AddPoint(3.5,benchmark_bb_vincia/(time_vincia_UH_3_5.second/time_vincia_UH_3_5.first));
    g_bb_su_vin_UH->AddPoint(4.0,benchmark_bb_vincia/(time_vincia_UH_4_0.second/time_vincia_UH_4_0.first));
    g_bb_su_vin_UH->AddPoint(4.5,benchmark_bb_vincia/(time_vincia_UH_4_5.second/time_vincia_UH_4_5.first));
    g_bb_su_vin_UH->AddPoint(5.0,benchmark_bb_vincia/(time_vincia_UH_5_0.second/time_vincia_UH_5_0.first));
    g_bb_su_vin_UH->SetLineStyle(kDashed);
    
    TGraph* g_bb_su_vin_UH_noPT = new TGraph();
    g_bb_su_vin_UH_noPT->AddPoint(3.0,(benchmark_bb_vincia)/(time_vincia_UH_noPT_3_0.second/time_vincia_UH_noPT_3_0.first));
    g_bb_su_vin_UH_noPT->AddPoint(3.5,(benchmark_bb_vincia)/(time_vincia_UH_noPT_3_5.second/time_vincia_UH_noPT_3_5.first));
    g_bb_su_vin_UH_noPT->AddPoint(4.0,(benchmark_bb_vincia)/(time_vincia_UH_noPT_4_0.second/time_vincia_UH_noPT_4_0.first));
    g_bb_su_vin_UH_noPT->AddPoint(4.5,(benchmark_bb_vincia)/(time_vincia_UH_noPT_4_5.second/time_vincia_UH_noPT_4_5.first));
    g_bb_su_vin_UH_noPT->AddPoint(5.0,(benchmark_bb_vincia)/(time_vincia_UH_noPT_5_0.second/time_vincia_UH_noPT_5_0.first));
    g_bb_su_vin_UH_noPT->SetLineColor(kRed);
    g_bb_su_vin_UH_noPT->SetMarkerColor(kRed);
    g_bb_su_vin_UH_noPT->SetLineStyle(kDashed);

    g_bb_su_vin_UH->SetTitle(";#hat{#it{p}}_{T} threshold [GeV/#it{c}];Relative speed up");
    g_bb_su_vin_UH->SetMinimum(0);
    g_bb_su_vin_UH->SetMaximum(50);
    // g_bb_su_UH_noPT->SetMaximum(1.05*time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first);
    g_bb_su_vin_UH->Draw("APL");
    g_bb_su_vin_UH_noPT->Draw("PL same");

    g_bb_su_UH->Draw("PL same");
    g_bb_su_UH_noPT->Draw("PL same");


    g_bb_su_UH->SetTitle(";#hat{#it{p}}_{T} threshold [GeV/#it{c}];Relative speed up");
    g_bb_su_UH->SetMinimum(0);
    g_bb_su_UH->SetMaximum(20);
    // g_bb_su_UH_noPT->SetMaximum(1.05*time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first);
    g_bb_su_UH->Draw("APL");
    g_bb_su_UH_noPT->Draw("PL same");

    TLegend* leg_su = new TLegend(0.2,0.75,0.90,0.92);
    // leg_su->AddEntry(g_bb_su_vin_UH,     "#splitline{Vincia Shower: Process veto +}{Evolution veto}","l");
    leg_su->AddEntry(g_bb_su_UH,         "Process veto + Evolution veto","l");
    // leg_su->AddEntry(g_bb_su_vin_UH_noPT,"Vincia Shower: Process veto","l");
    leg_su->AddEntry(g_bb_su_UH_noPT,    "Process veto","l");
    
    leg_su->Draw();
    leg_su->SetTextFont(132);
    leg_su->SetFillStyle(0);
    // gPad->SetGrid();
    c_bb_speedup->Print("plots_timing/c_bb_speedup.pdf");
    // -------------------------------------------------------------------------
    // Speed up cc 
    // -------------------------------------------------------------------------
    TCanvas* c_cc_speedup = new TCanvas("c_cc_speedup","c_cc_speedup");
    
    double benchmark_cc = time_UH_cc_noPT_noMPI.second/time_UH_cc_noPT_noMPI.first;

    // ---- Create TGraphs -----
    TGraph* g_cc_su_UH = new TGraph();
    g_cc_su_UH->AddPoint(1.00,benchmark_cc/(time_UH_cc_1_00.second/time_UH_cc_1_00.first));
    g_cc_su_UH->AddPoint(1.25,benchmark_cc/(time_UH_cc_1_25.second/time_UH_cc_1_25.first));
    g_cc_su_UH->AddPoint(1.50,benchmark_cc/(time_UH_cc_1_50.second/time_UH_cc_1_50.first));
    g_cc_su_UH->AddPoint(1.75,benchmark_cc/(time_UH_cc_1_75.second/time_UH_cc_1_75.first));
    g_cc_su_UH->AddPoint(2.00,benchmark_cc/(time_UH_cc_2_00.second/time_UH_cc_2_00.first));
    
    TGraph* g_cc_su_UH_noPT = new TGraph();
    g_cc_su_UH_noPT->AddPoint(1.00,(benchmark_cc)/(time_UH_cc_noPT_1_00.second/time_UH_cc_noPT_1_00.first));
    g_cc_su_UH_noPT->AddPoint(1.25,(benchmark_cc)/(time_UH_cc_noPT_1_25.second/time_UH_cc_noPT_1_25.first));
    g_cc_su_UH_noPT->AddPoint(1.50,(benchmark_cc)/(time_UH_cc_noPT_1_50.second/time_UH_cc_noPT_1_50.first));
    g_cc_su_UH_noPT->AddPoint(1.75,(benchmark_cc)/(time_UH_cc_noPT_1_75.second/time_UH_cc_noPT_1_75.first));
    g_cc_su_UH_noPT->AddPoint(2.00,(benchmark_cc)/(time_UH_cc_noPT_2_00.second/time_UH_cc_noPT_2_00.first));
    g_cc_su_UH_noPT->SetLineColor(kRed);
    g_cc_su_UH_noPT->SetMarkerColor(kRed);

    g_cc_su_UH->SetTitle(";#hat{#it{p}}_{T} threshold [GeV/#it{c}];Relative speed up");
    g_cc_su_UH->SetMinimum(0.8);
    // g_cc_su_UH_noPT->SetMaximum(1.05*time_UH_cc_noPT_noMPI.second/time_UH_cc_noPT_noMPI.first);
    g_cc_su_UH->Draw("APL");
    g_cc_su_UH_noPT->Draw("PL same");


    TLegend* leg_su_cc = new TLegend(0.2,0.35,0.9,0.55);
    leg_su_cc->AddEntry(g_cc_su_UH_noPT,"Process veto","l");
    leg_su_cc->AddEntry(g_cc_su_UH, "Process veto + Evolution veto","l");
    
    leg_su_cc->Draw();
    leg_su_cc->SetTextFont(132);
    leg_su_cc->SetFillStyle(0);
    gPad->SetGrid();
    c_cc_speedup->Print("plots_timing/c_cc_speedup.pdf");
    
    // -------------------------------------------------------------------------
    // Speed up cc 
    // -------------------------------------------------------------------------

    TCanvas* c_cc = new TCanvas("c_cc","c_cc");
    // ---- Create TGraphs -----
    TGraph* g_cc_UH = new TGraph();
    g_cc_UH->AddPoint(1.00,time_UH_cc_1_00.second/time_UH_cc_1_00.first);
    g_cc_UH->AddPoint(1.25,time_UH_cc_1_25.second/time_UH_cc_1_25.first);
    g_cc_UH->AddPoint(1.50,time_UH_cc_1_50.second/time_UH_cc_1_50.first);
    g_cc_UH->AddPoint(1.75,time_UH_cc_1_75.second/time_UH_cc_1_75.first);
    g_cc_UH->AddPoint(2.00,time_UH_cc_2_00.second/time_UH_cc_2_00.first);
    
    TGraph* g_cc_UH_noPT = new TGraph();
    g_cc_UH_noPT->AddPoint(1.00,time_UH_cc_noPT_1_00.second/time_UH_cc_noPT_1_00.first);
    g_cc_UH_noPT->AddPoint(1.25,time_UH_cc_noPT_1_25.second/time_UH_cc_noPT_1_25.first);
    g_cc_UH_noPT->AddPoint(1.50,time_UH_cc_noPT_1_50.second/time_UH_cc_noPT_1_50.first);
    g_cc_UH_noPT->AddPoint(1.75,time_UH_cc_noPT_1_75.second/time_UH_cc_noPT_1_75.first);
    g_cc_UH_noPT->AddPoint(2.00,time_UH_cc_noPT_2_00.second/time_UH_cc_noPT_2_00.first);
    g_cc_UH_noPT->SetLineColor(kRed);
    g_cc_UH_noPT->SetMarkerColor(kRed);

    g_cc_UH->SetTitle(";#hat{#it{p}}_{T} threshold [GeV/#it{c}];Time per c#bar{c} /Seconds");
    g_cc_UH->SetMinimum(0);
    g_cc_UH->SetMaximum(1.05*time_UH_cc_noPT_noMPI.second/time_UH_cc_noPT_noMPI.first);
    g_cc_UH->Draw("APL");
    g_cc_UH_noPT->Draw("PL same");


    TLegend* leg_cc = new TLegend(0.2,0.4,0.7,0.6);
    leg_cc->AddEntry(g_cc_UH_noPT,"Process veto","l");
    leg_cc->AddEntry(g_cc_UH, "Process veto + Evolution veto","l");
    
    leg_cc->Draw();
    leg_cc->SetTextFont(132);
    leg_cc->SetFillStyle(0);

    TLine* line_cc = new TLine(0.9,time_UH_cc_noPT_noMPI.second/time_UH_cc_noPT_noMPI.first,2.1,time_UH_cc_noPT_noMPI.second/time_UH_cc_noPT_noMPI.first);
    line_cc->SetLineColor(kGreen+2);
    line_cc->Draw();
    c_cc->Print("plots_timing/c_cc.pdf");


    // -------------------------------------------------------------------------
    // Graphs
    // -------------------------------------------------------------------------
    TCanvas* c_missed = new TCanvas("c_missed","c_missed");

    TGraph* g_bb_missed_MPI = new TGraph();
    g_bb_missed_MPI->AddPoint(3.0,100*n_3_0.first.second/n_3_0.first.first);
    g_bb_missed_MPI->AddPoint(3.5,100*n_3_5.first.second/n_3_5.first.first);
    g_bb_missed_MPI->AddPoint(4.0,100*n_4_0.first.second/n_4_0.first.first);
    g_bb_missed_MPI->AddPoint(4.5,100*n_4_5.first.second/n_4_5.first.first);
    g_bb_missed_MPI->AddPoint(5.0,100*n_5_0.first.second/n_5_0.first.first);

    TGraph* g_bb_missed_PT = new TGraph();
    g_bb_missed_PT->AddPoint(3.0,100*n_3_0.second/n_3_0.first.first);
    g_bb_missed_PT->AddPoint(3.5,100*n_3_5.second/n_3_5.first.first);
    g_bb_missed_PT->AddPoint(4.0,100*n_4_0.second/n_4_0.first.first);
    g_bb_missed_PT->AddPoint(4.5,100*n_4_5.second/n_4_5.first.first);
    g_bb_missed_PT->AddPoint(5.0,100*n_5_0.second/n_5_0.first.first);
    g_bb_missed_MPI->SetLineColor(kRed);
    g_bb_missed_MPI->SetMarkerColor(kRed);

    TGraph* g_bb_missed_vin_MPI = new TGraph();
    g_bb_missed_vin_MPI->AddPoint(3.0,100*n_vincia_3_0.first.second/n_vincia_3_0.first.first);
    g_bb_missed_vin_MPI->AddPoint(3.5,100*n_vincia_3_5.first.second/n_vincia_3_5.first.first);
    g_bb_missed_vin_MPI->AddPoint(4.0,100*n_vincia_4_0.first.second/n_vincia_4_0.first.first);
    g_bb_missed_vin_MPI->AddPoint(4.5,100*n_vincia_4_5.first.second/n_vincia_4_5.first.first);
    g_bb_missed_vin_MPI->AddPoint(5.0,100*n_vincia_5_0.first.second/n_vincia_5_0.first.first);
    g_bb_missed_vin_MPI->SetLineColor(kRed);
    g_bb_missed_vin_MPI->SetMarkerColor(kRed);
    g_bb_missed_vin_MPI->SetLineStyle(kDashed);

    TGraph* g_bb_missed_vin_PT = new TGraph();
    g_bb_missed_vin_PT->AddPoint(3.0,100*n_vincia_3_0.second/n_vincia_3_0.first.first);
    g_bb_missed_vin_PT->AddPoint(3.5,100*n_vincia_3_5.second/n_vincia_3_5.first.first);
    g_bb_missed_vin_PT->AddPoint(4.0,100*n_vincia_4_0.second/n_vincia_4_0.first.first);
    g_bb_missed_vin_PT->AddPoint(4.5,100*n_vincia_4_5.second/n_vincia_4_5.first.first);
    g_bb_missed_vin_PT->AddPoint(5.0,100*n_vincia_5_0.second/n_vincia_5_0.first.first);
    g_bb_missed_vin_PT->SetLineStyle(kDashed);

    g_bb_missed_PT->SetTitle(";#hat{#it{p}}_{T} threshold [GeV/#it{c}];Fraction of b#bar{b} missed (%)");
    g_bb_missed_PT->SetMinimum(0);
    g_bb_missed_PT->SetMaximum(25);
    // g_bb_missed_PT->SetMaximum(1.05*time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first);
    g_bb_missed_PT->Draw("APL");
    g_bb_missed_MPI->Draw("PL same");
    // g_bb_missed_vin_PT->Draw("PL same");
    // g_bb_missed_vin_MPI->Draw("PL same");



    TLegend* leg_missed = new TLegend(0.2,0.75,0.96,0.92);
    // leg_missed->AddEntry(g_bb_missed_vin_PT, "#splitline{Vincia Shower: Process veto +}{Evolution veto}","l");
    leg_missed->AddEntry(g_bb_missed_PT,     "Process veto + Evolution veto","l");
    // leg_missed->AddEntry(g_bb_missed_vin_MPI,"Vincia Shower: Process veto","l");
    leg_missed->AddEntry(g_bb_missed_MPI,    "Process veto","l");
    
    leg_missed->Draw();
    leg_missed->SetTextFont(132);
    leg_missed->SetFillStyle(0);

    c_missed->Print("plots_timing/c_missed.pdf");

    // -------------------------------------------------------------------------
    // Graphs
    // -------------------------------------------------------------------------
    TCanvas* c_missed_cc = new TCanvas("c_missed_cc","c_missed_cc");

    TGraph* g_cc_missed_MPI = new TGraph();
    g_cc_missed_MPI->AddPoint(1.00,100*n_cc_1_00.first.second/n_cc_1_00.first.first);
    g_cc_missed_MPI->AddPoint(1.25,100*n_cc_1_25.first.second/n_cc_1_25.first.first);
    g_cc_missed_MPI->AddPoint(1.50,100*n_cc_1_50.first.second/n_cc_1_50.first.first);
    g_cc_missed_MPI->AddPoint(1.75,100*n_cc_1_75.first.second/n_cc_1_75.first.first);
    g_cc_missed_MPI->AddPoint(2.00,100*n_cc_2_00.first.second/n_cc_2_00.first.first);

    TGraph* g_cc_missed_PT = new TGraph();
    g_cc_missed_PT->AddPoint(1.00,100*n_cc_1_00.second/n_cc_1_00.first.first);
    g_cc_missed_PT->AddPoint(1.25,100*n_cc_1_25.second/n_cc_1_25.first.first);
    g_cc_missed_PT->AddPoint(1.50,100*n_cc_1_50.second/n_cc_1_50.first.first);
    g_cc_missed_PT->AddPoint(1.75,100*n_cc_1_75.second/n_cc_1_75.first.first);
    g_cc_missed_PT->AddPoint(2.00,100*n_cc_2_00.second/n_cc_2_00.first.first);
    g_cc_missed_MPI->SetLineColor(kRed);
    g_cc_missed_MPI->SetMarkerColor(kRed);

    g_cc_missed_PT->SetTitle(";#hat{#it{p}}_{T} threshold [GeV/#it{c}];Fraction of c#bar{c} missed (%)");
    g_cc_missed_PT->SetMinimum(0);
    // g_cc_missed_PT->SetMaximum(1.05*time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first);
    g_cc_missed_PT->Draw("APL");
    g_cc_missed_MPI->Draw("PL same");
    gPad->SetGrid();
    TLegend* leg_missed_cc = new TLegend(0.2,0.6,0.7,0.8);
    leg_missed_cc->AddEntry(g_cc_missed_MPI,"Process veto","l");
    leg_missed_cc->AddEntry(g_cc_missed_PT, "Process veto + Evolution veto","l");
    
    leg_missed_cc->Draw();
    leg_missed_cc->SetTextFont(132);
    leg_missed_cc->SetFillStyle(0);

    c_missed_cc->Print("plots_timing/c_missed_cc.pdf");

    // -------------------------------------------------------------------------
    // Plot Bc
    // -------------------------------------------------------------------------

    TCanvas* c_Bc = new TCanvas("c_Bc","c_Bc");
    // ---- Create TGraphs -----
    TGraph* g_Bc = new TGraph();
    g_Bc->AddPoint(4.0,(Bc_4Steps.second/Bc_4Steps.first)/60.0);
    g_Bc->AddPoint(3.0,(Bc_3Steps.second/Bc_3Steps.first)/60.0);
    g_Bc->AddPoint(2.0,(Bc_2Steps.second/Bc_2Steps.first)/60.0);
    g_Bc->AddPoint(1.0,(Bc_1Steps.second/Bc_1Steps.first)/60.0);
    g_Bc->AddPoint(0.0,(Bc_0Steps.second/Bc_0Steps.first)/60.0);
    // g_Bc->AddPoint(4.0,(time_Bc_4Steps)/60.0);
    // g_Bc->AddPoint(3.0,(time_Bc_3Steps)/60.0);
    // g_Bc->AddPoint(2.0,(time_Bc_2Steps)/60.0);
    // g_Bc->AddPoint(1.0,(time_Bc_1Steps)/60.0);
    // g_Bc->AddPoint(0.0,(time_Bc_0Steps)/60.0);

    g_Bc->SetTitle(";UserHook Level;Time per B_{c}^{+} /Minutes");
    gPad->SetLogy();
    // g_Bc->SetMinimum(1.0);
    // g_Bc->SetMaximum(100000.0);
    // g_Bc->SetMaximum(1.05*time_UH_bb_noPT_noMPI.second/time_UH_bb_noPT_noMPI.first);
    g_Bc->Draw("APL");


    // TLegend* leg_Bc = new TLegend(0.2,0.4,0.7,0.6);
    // leg_Bc->AddEntry(g_bb_UH_noPT,"Process veto","l");
    // leg_Bc->AddEntry(g_bb_UH, "Process veto + Evolution veto","l");
    
    // leg_Bc->Draw();
    // leg_Bc->SetTextFont(132);

    c_Bc->Print("plots_timing/c_Bc.pdf");

    // -------------------------------------------------------------------------
    // Speed up Bc 
    // -------------------------------------------------------------------------
    TCanvas* c_Bc_speedup = new TCanvas("c_Bc_speedup","c_Bc_speedup");
    
    double benchmark_Bc = Bc_0Steps.second/Bc_0Steps.first;

    TH1D* h_temp = new TH1D("h_temp","h_temp",5,0,5);
    // h_temp->GetXaxis()->SetBinLabel(1,"No UH");
    // h_temp->GetXaxis()->SetBinLabel(2,"Mass UH");
    // h_temp->GetXaxis()->SetBinLabel(3,"Mass + Parton UH");
    // h_temp->GetXaxis()->SetBinLabel(4,"Mass + Parton+ Evol. UH");
    // h_temp->GetXaxis()->SetBinLabel(5,"Mass + Parton+ Evol.+ Proc. UH");
    h_temp->GetXaxis()->SetBinLabel(1,"");
    h_temp->GetXaxis()->SetBinLabel(2,"");
    h_temp->GetXaxis()->SetBinLabel(3,"");
    h_temp->GetXaxis()->SetBinLabel(4,"");
    h_temp->GetXaxis()->SetBinLabel(5,"");
    h_temp->GetXaxis()->SetLabelSize(15);
    h_temp->SetMaximum(15.0);
    h_temp->SetTitle(";;Relative speed up");
    h_temp->SetFillColor(kRed);
    h_temp->SetBarWidth(0.5);
    h_temp->SetBarOffset(0.25);

    h_temp->SetBinContent(1,benchmark_Bc/(Bc_4Steps.second/Bc_4Steps.first));
    h_temp->SetBinContent(2,benchmark_Bc/(Bc_3Steps.second/Bc_3Steps.first));
    h_temp->SetBinContent(3,benchmark_Bc/(Bc_2Steps.second/Bc_2Steps.first));
    h_temp->SetBinContent(4,benchmark_Bc/(Bc_1Steps.second/Bc_1Steps.first));
    h_temp->SetBinContent(5,benchmark_Bc/(Bc_0Steps.second/Bc_0Steps.first));
    h_temp->Draw("hBar");


    TText *text_1 = new TText(1.5,4.4,"No UserHook");
    text_1->SetTextSize(20);
    // text_1->SetTextAngle(90);
    text_1->Draw();
    TText *text_2 = new TText(1.5,3.4,"Mass UserHook");
    text_2->SetTextSize(20);
    // text_2->SetTextAngle(90);
    text_2->Draw();

    TText *text_3 = new TText(1.5,2.4,"Mass + Parton-level UserHooks");
    text_3->SetTextSize(20);
    // text_3->SetTextAngle(90);
    text_3->Draw();

    TText *text_4_part1 = new TText(0.5,1.55,"Mass + Parton-level +");
    text_4_part1->SetTextSize(20);
    // text_4_part1->SetTextAngle(90);
    text_4_part1->SetTextColor(kWhite);
    text_4_part1->Draw();
    TText *text_4_part2 = new TText(0.5,1.35,"Evolution UserHooks");
    text_4_part2->SetTextSize(20);
    // text_4_part2->SetTextAngle(90);
    text_4_part2->SetTextColor(kWhite);
    text_4_part2->Draw();

    TText *text_5_part1 = new TText(0.5,0.55,"Mass + Parton-level +");
    text_5_part1->SetTextSize(20);
    // text_5_part1->SetTextAngle(90);
    text_5_part1->SetTextColor(kWhite);
    text_5_part1->Draw();
    TText *text_5_part2 = new TText(0.5,0.35,"Evolution + Process-level UserHooks");
    text_5_part2->SetTextSize(20);
    // text_5_part2->SetTextAngle(90);
    text_5_part2->SetTextColor(kWhite);
    text_5_part2->Draw();

    // // ---- Create TGraphs -----
    // TGraph* g_Bc_su_UH = new TGraph();
    // g_Bc_su_UH->AddPoint(4.5,benchmark_Bc/(Bc_4Steps.second/Bc_4Steps.first));
    // g_Bc_su_UH->AddPoint(3.5,benchmark_Bc/(Bc_3Steps.second/Bc_3Steps.first));
    // g_Bc_su_UH->AddPoint(2.5,benchmark_Bc/(Bc_2Steps.second/Bc_2Steps.first));
    // g_Bc_su_UH->AddPoint(1.5,benchmark_Bc/(Bc_1Steps.second/Bc_1Steps.first));
    // g_Bc_su_UH->AddPoint(0.5,benchmark_Bc/(Bc_0Steps.second/Bc_0Steps.first));


    // g_Bc_su_UH->SetTitle(";UserHook Level;Relative speed up");
    // g_Bc_su_UH->SetMinimum(0.0);
    // // gPad->SetLogy();
    // // g_Bc_su_UH_noPT->SetMaximum(1.05*time_UH_Bc_noPT_noMPI.second/time_UH_Bc_noPT_noMPI.first);
    // g_Bc_su_UH->Draw("PL");
    // gPad->SetGrid();
    gPad->RedrawAxis();
    c_Bc_speedup->Print("plots_timing/c_Bc_speedup.pdf");

    // -------------------------------------------------------------------------
    // Make table 
    // -------------------------------------------------------------------------
    std::cout <<" -------- Tables B Hadron"<< std::endl;
    // getTimePerBHadron("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_noVetoMPI_Scale_4.000000_N_1000000_PartMod1.root");
    // getTimePerBHadron("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_20000_PartMod1.root");

    getTimePerBHadron("../output/from_lxplus/Complete_paper_samples/Timing_singlyheavy/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_noVetoMPI_Scale_1.500000_Quark_4_N_4000000_PartMod1_noColRec.root");
    getTimePerBHadron("../output/from_lxplus/Complete_paper_samples/Timing_singlyheavy/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_Quark_5_N_100000_PartMod1_noColRec.root");
    std::cout << std::endl;

    std::cout <<" -------- Tables D Hadron"<< std::endl;
    // getTimePerDHadron("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_noVetoMPI_Scale_4.000000_N_1000000_PartMod1.root");
    // getTimePerDHadron("../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_Scale_1.500000_Quark_4_N_20000_PartMod1.root");
    
    getTimePerDHadron("../output/from_lxplus/Complete_paper_samples/Timing_singlyheavy/main202output_SoftQCD_nd_UserHook_Minni_noVetoPT_noVetoMPI_Scale_1.500000_Quark_4_N_4000000_PartMod1_noColRec.root");
    getTimePerDHadron("../output/from_lxplus/Complete_paper_samples/Timing_singlyheavy/main202output_SoftQCD_nd_UserHook_Minni_Scale_1.500000_Quark_4_N_100000_PartMod1_noColRec.root");
    std::cout << std::endl;

}


// -----------------------------------------------------------------------------
// Get the time to make a bb pair
// -----------------------------------------------------------------------------

std::pair<int,double> getTimePerbb(std::string name){
    
    TFile* file = TFile::Open(name.c_str());
    if(!file){
        std::cout << "Can't fild file: " << name << std::endl;
        exit(1);
    }
    
    TTree* events = (TTree*)file->Get("events");
    if(!events){
        std::cout << "Can't fild tree: events in file " <<name << std::endl;
        exit(1);
    }

    double n_bb = events->GetEntries("foundb || hasbOnia");

    double timeGen;
    bool foundb;
    bool hasbOnia;
    events->SetBranchAddress("timeGen",&timeGen);
    events->SetBranchAddress("foundb",&foundb);
    events->SetBranchAddress("hasbOnia",&hasbOnia);

    double total_time = 0.0;
    std::vector<double> times;
    double temp_time = 0.0;
    for(int i =0; i< events->GetEntries(); i++){
        events->GetEntry(i);
        total_time += timeGen;
        
        temp_time += timeGen;
        if(foundb || hasbOnia){
            times.push_back(temp_time);
            temp_time = 0.0;
        }
    }
    
    double med = median(times);
    std::cout << "Found n_bb: " << n_bb << "\ttimes: " << times.size()  << "\tmedian "<< med << "\tmean "<< total_time/n_bb<< std::endl;
    file->Close();

    return std::make_pair(n_bb,total_time);
}


// -----------------------------------------------------------------------------
// Get the time to make a cc pair
// -----------------------------------------------------------------------------

std::pair<int,double> getTimePercc(std::string name){
    
    TFile* file = TFile::Open(name.c_str());
    if(!file){
        std::cout << "Can't fild file: " << name << std::endl;
        exit(1);
    }
    
    TTree* events = (TTree*)file->Get("events");
    if(!events){
        std::cout << "Can't fild tree: events in file " <<name << std::endl;
        exit(1);
    }

    double n_cc = events->GetEntries("foundc || hascOnia");

    double timeGen;
    bool foundc;
    bool hascOnia;
    events->SetBranchAddress("timeGen",&timeGen);
    events->SetBranchAddress("foundc",&foundc);
    events->SetBranchAddress("hascOnia",&hascOnia);

    double total_time = 0.0;
    std::vector<double> times;
    double temp_time = 0.0;
    for(int i =0; i< events->GetEntries(); i++){
        events->GetEntry(i);
        total_time += timeGen;
        
        temp_time += timeGen;
        if(foundc || hascOnia){
            times.push_back(temp_time);
            temp_time = 0.0;
        }
    }
    
    double med = median(times);
    std::cout << "Found n_cc: " << n_cc << "\ttimes: " << times.size()  << "\tmedian "<< med << "\tmean "<< total_time/n_cc<< std::endl;
    file->Close();

    return std::make_pair(n_cc,total_time);
}


// -----------------------------------------------------------------------------
// Get the time for different hadrons
// -----------------------------------------------------------------------------

void getTimePerBHadron(std::string name){
    
    TFile* file = TFile::Open(name.c_str());
    if(!file){
        std::cout << "Can't fild file: " << name << std::endl;
        exit(1);
    }
    
    TTree* events = (TTree*)file->Get("events");
    if(!events){
        std::cout << "Can't fild tree: events in file " <<name << std::endl;
        exit(1);
    }

    bool useMinutes = false;
   
    std::string units = "\tseconds"; 
    double factor = 1.0;
    if(useMinutes){
        units = "\tminutes";
        factor = 60.0;
    }
    double n_bb         = events->GetEntries("foundb || hasbOnia");
    double n_nBu        = events->GetEntries("nBu>0");
    double n_nBd        = events->GetEntries("nBd>0");
    double n_nBs        = events->GetEntries("nBs>0");
    double n_nLb        = events->GetEntries("nLb>0");

    double n_nSigmabm   = events->GetEntries("nSigmabm>0");
    double n_nSigmab0   = events->GetEntries("nSigmab0>0");
    double n_nSigmabp   = events->GetEntries("nSigmabp>0");
    double n_nXibm      = events->GetEntries("nXibm>0");
    double n_nXib0      = events->GetEntries("nXib0>0");
    double n_nXiprimebm = events->GetEntries("nXiprimebm>0");
    double n_nXiprimeb0 = events->GetEntries("nXiprimeb0>0");
    double n_nOmegabm   = events->GetEntries("nOmegabm>0");

    double timeGen;
    bool foundb;
    bool hasbOnia;
    events->SetBranchAddress("timeGen",&timeGen);
    events->SetBranchAddress("foundb",&foundb);
    events->SetBranchAddress("hasbOnia",&hasbOnia);

    double total_time = 0.0;
    std::vector<double> times;
    double temp_time = 0.0;
    for(int i =0; i< events->GetEntries(); i++){
        events->GetEntry(i);
        total_time += timeGen;
        
        temp_time += timeGen;
        if(foundb || hasbOnia){
            times.push_back(temp_time);
            temp_time = 0.0;
        }
    }
   

    // double med = median(times);
    std::cout << "Found n_bb: " << n_bb << "\ttimes: " << times.size() << "\tmean "<< total_time/n_bb;
    std::cout <<  "\tLimit: " << (total_time/n_bb)* (n_bb/1.0) / factor << std::endl;
    
    double t_gen = total_time/(n_bb*factor);

    double eff_nBu         = (n_nBu/n_bb);
    double eff_nBd         = (n_nBd/n_bb);
    double eff_nBs         = (n_nBs/n_bb);
    double eff_nLb         = (n_nLb/n_bb);
    double eff_nSigmabm    = (n_nSigmabm/n_bb);
    double eff_nSigmab0    = (n_nSigmab0/n_bb);
    double eff_nSigmabp    = (n_nSigmabp/n_bb);
    double eff_nXibm       = (n_nXibm/n_bb);
    double eff_nXib0       = (n_nXib0/n_bb);
    double eff_nXiprimebm  = (n_nXiprimebm/n_bb);
    double eff_nXiprimeb0  = (n_nXiprimeb0/n_bb);
    double eff_nOmegabm    = (n_nOmegabm/n_bb);

    std::cout << "Time Bu        " << t_gen/eff_nBu        << "\t+/- " <<t_gen/eff_nBu        *getRelEffError(n_nBu        ,n_bb) <<units << " \t("<<n_nBu        << ") " <<100*getRelEffError(n_nBu        ,n_bb) <<" %" <<  std::endl;         
    std::cout << "Time Bd        " << t_gen/eff_nBd        << "\t+/- " <<t_gen/eff_nBd        *getRelEffError(n_nBd        ,n_bb) <<units << " \t("<<n_nBd        << ") " <<100*getRelEffError(n_nBd        ,n_bb) <<" %" <<  std::endl;         
    std::cout << "Time Bs        " << t_gen/eff_nBs        << "\t+/- " <<t_gen/eff_nBs        *getRelEffError(n_nBs        ,n_bb) <<units << " \t("<<n_nBs        << ") " <<100*getRelEffError(n_nBs        ,n_bb) <<" %" <<  std::endl;         
    std::cout << "Time Lb        " << t_gen/eff_nLb        << "\t+/- " <<t_gen/eff_nLb        *getRelEffError(n_nLb        ,n_bb) <<units << " \t("<<n_nLb        << ") " <<100*getRelEffError(n_nLb        ,n_bb) <<" %" <<  std::endl;         
    std::cout << "Time Sigmabm   " << t_gen/eff_nSigmabm   << "\t+/- " <<t_gen/eff_nSigmabm   *getRelEffError(n_nSigmabm   ,n_bb) <<units << " \t("<<n_nSigmabm   << ") " <<100*getRelEffError(n_nSigmabm   ,n_bb) <<" %" <<  std::endl;    
    std::cout << "Time Sigmab0   " << t_gen/eff_nSigmab0   << "\t+/- " <<t_gen/eff_nSigmab0   *getRelEffError(n_nSigmab0   ,n_bb) <<units << " \t("<<n_nSigmab0   << ") " <<100*getRelEffError(n_nSigmab0   ,n_bb) <<" %" <<  std::endl;    
    std::cout << "Time Sigmabp   " << t_gen/eff_nSigmabp   << "\t+/- " <<t_gen/eff_nSigmabp   *getRelEffError(n_nSigmabp   ,n_bb) <<units << " \t("<<n_nSigmabp   << ") " <<100*getRelEffError(n_nSigmabp   ,n_bb) <<" %" <<  std::endl;    
    std::cout << "Time Xibm      " << t_gen/eff_nXibm      << "\t+/- " <<t_gen/eff_nXibm      *getRelEffError(n_nXibm      ,n_bb) <<units << " \t("<<n_nXibm      << ") " <<100*getRelEffError(n_nXibm      ,n_bb) <<" %" <<  std::endl;       
    std::cout << "Time Xib0      " << t_gen/eff_nXib0      << "\t+/- " <<t_gen/eff_nXib0      *getRelEffError(n_nXib0      ,n_bb) <<units << " \t("<<n_nXib0      << ") " <<100*getRelEffError(n_nXib0      ,n_bb) <<" %" <<  std::endl;       
    std::cout << "Time Xiprimebm " << t_gen/eff_nXiprimebm << "\t+/- " <<t_gen/eff_nXiprimebm *getRelEffError(n_nXiprimebm ,n_bb) <<units << " \t("<<n_nXiprimebm << ") " <<100*getRelEffError(n_nXiprimebm ,n_bb) <<" %" <<  std::endl;  
    std::cout << "Time Xiprimeb0 " << t_gen/eff_nXiprimeb0 << "\t+/- " <<t_gen/eff_nXiprimeb0 *getRelEffError(n_nXiprimeb0 ,n_bb) <<units << " \t("<<n_nXiprimeb0 << ") " <<100*getRelEffError(n_nXiprimeb0 ,n_bb) <<" %" <<  std::endl;  
    std::cout << "Time Omegabm   " << t_gen/eff_nOmegabm   << "\t+/- " <<t_gen/eff_nOmegabm   *getRelEffError(n_nOmegabm   ,n_bb) <<units << " \t("<<n_nOmegabm   << ") " <<100*getRelEffError(n_nOmegabm   ,n_bb) <<" %" <<  std::endl;    

    file->Close();

    // return std::make_pair(n_bb,total_time);
}


// -----------------------------------------------------------------------------
// Get the time for different hadrons
// -----------------------------------------------------------------------------

void getTimePerDHadron(std::string name){
    
    TFile* file = TFile::Open(name.c_str());
    if(!file){
        std::cout << "Can't fild file: " << name << std::endl;
        exit(1);
    }
    
    TTree* events = (TTree*)file->Get("events");
    if(!events){
        std::cout << "Can't fild tree: events in file " <<name << std::endl;
        exit(1);
    }
    bool useMinutes = false;
   
    std::string units = "\tseconds"; 
    double factor = 1.0;
    if(useMinutes){
        units = "\tminutes";
        factor = 60.0;
    }
    double n_cc         = events->GetEntries("foundc || hascOnia");
    double n_nDz        = events->GetEntries("nDz>0");
    double n_nDp        = events->GetEntries("nDp>0");
    double n_nDs        = events->GetEntries("nDs>0");
    double n_nLc        = events->GetEntries("nLc>0");

    double n_nSigmacpp  = events->GetEntries("nSigmacpp>0");
    double n_nSigmacp   = events->GetEntries("nSigmacp>0");
    double n_nXicp      = events->GetEntries("nXicp>0");
    double n_nXic0      = events->GetEntries("nXic0>0");
    double n_nXiprimecp = events->GetEntries("nXiprimecp>0");
    double n_nXiprimec0 = events->GetEntries("nXiprimec0>0");
    double n_nOmegac0   = events->GetEntries("nOmegac0>0");

    double timeGen;
    bool foundc;
    bool hascOnia;
    events->SetBranchAddress("timeGen",&timeGen);
    events->SetBranchAddress("foundc",&foundc);
    events->SetBranchAddress("hascOnia",&hascOnia);

    double total_time = 0.0;
    std::vector<double> times;
    double temp_time = 0.0;
    for(int i =0; i< events->GetEntries(); i++){
        events->GetEntry(i);
        total_time += timeGen;
        
        temp_time += timeGen;
        if(foundc || hascOnia){
            times.push_back(temp_time);
            temp_time = 0.0;
        }
    }
   

    // double med = median(times);
    std::cout << "Found n_cc: " << n_cc << "\ttimes: " << times.size() << "\tmean "<< total_time/n_cc;
    std::cout <<  "\tLimit: " << (total_time/n_cc)* (n_cc/1.0) / factor << std::endl;   
    
    double t_gen = total_time/(n_cc*factor);

    double eff_nDz         = n_nDz/n_cc; 
    double eff_nDp         = n_nDp/n_cc; 
    double eff_nDs         = n_nDs/n_cc; 
    double eff_nLc         = n_nLc/n_cc; 
    double eff_nSigmacpp   = n_nSigmacpp/n_cc; 
    double eff_nSigmacp    = n_nSigmacp/n_cc; 
    double eff_nXicp       = n_nXicp/n_cc; 
    double eff_nXic0       = n_nXic0/n_cc; 
    double eff_nXiprimecp  = n_nXiprimecp/n_cc; 
    double eff_nXiprimec0  = n_nXiprimec0/n_cc; 
    double eff_nOmegac0    = n_nOmegac0/n_cc; 

    std::cout << "Time Dz        " << t_gen/eff_nDz          << "\t+/- "<<t_gen/eff_nDz         *getRelEffError(n_nDz        ,n_cc) << units <<" \t(" << n_nDz        << ") " <<100*getRelEffError(n_nDz        ,n_cc)<< "%"<<std::endl;
    std::cout << "Time Dp        " << t_gen/eff_nDp          << "\t+/- "<<t_gen/eff_nDp         *getRelEffError(n_nDp        ,n_cc) << units <<" \t(" << n_nDp        << ") " <<100*getRelEffError(n_nDp        ,n_cc)<< "%"<<std::endl;
    std::cout << "Time Ds        " << t_gen/eff_nDs          << "\t+/- "<<t_gen/eff_nDs         *getRelEffError(n_nDs        ,n_cc) << units <<" \t(" << n_nDs        << ") " <<100*getRelEffError(n_nDs        ,n_cc)<< "%"<<std::endl;
    std::cout << "Time Lc        " << t_gen/eff_nLc          << "\t+/- "<<t_gen/eff_nLc         *getRelEffError(n_nLc        ,n_cc) << units <<" \t(" << n_nLc        << ") " <<100*getRelEffError(n_nLc        ,n_cc)<< "%"<<std::endl;

    std::cout << "Time Sigmacpp  " << t_gen/eff_nSigmacpp    << "\t+/- "<<t_gen/eff_nSigmacpp   *getRelEffError(n_nSigmacpp  ,n_cc) << units <<" \t(" << n_nSigmacpp  << ") " <<100*getRelEffError(n_nSigmacpp  ,n_cc)<< "%"<<std::endl;
    std::cout << "Time Sigmacp   " << t_gen/eff_nSigmacp     << "\t+/- "<<t_gen/eff_nSigmacp    *getRelEffError(n_nSigmacp   ,n_cc) << units <<" \t(" << n_nSigmacp   << ") " <<100*getRelEffError(n_nSigmacp   ,n_cc)<< "%"<<std::endl;
    std::cout << "Time Xicp      " << t_gen/eff_nXicp        << "\t+/- "<<t_gen/eff_nXicp       *getRelEffError(n_nXicp      ,n_cc) << units <<" \t(" << n_nXicp      << ") " <<100*getRelEffError(n_nXicp      ,n_cc)<< "%"<<std::endl;
    std::cout << "Time Xic0      " << t_gen/eff_nXic0        << "\t+/- "<<t_gen/eff_nXic0       *getRelEffError(n_nXic0      ,n_cc) << units <<" \t(" << n_nXic0      << ") " <<100*getRelEffError(n_nXic0      ,n_cc)<< "%"<<std::endl;
    std::cout << "Time Xiprimecp " << t_gen/eff_nXiprimecp   << "\t+/- "<<t_gen/eff_nXiprimecp  *getRelEffError(n_nXiprimecp ,n_cc) << units <<" \t(" << n_nXiprimecp << ") " <<100*getRelEffError(n_nXiprimecp ,n_cc)<< "%"<<std::endl;
    std::cout << "Time Xiprimec0 " << t_gen/eff_nXiprimec0   << "\t+/- "<<t_gen/eff_nXiprimec0  *getRelEffError(n_nXiprimec0 ,n_cc) << units <<" \t(" << n_nXiprimec0 << ") " <<100*getRelEffError(n_nXiprimec0 ,n_cc)<< "%"<<std::endl;
    std::cout << "Time Omegac0   " << t_gen/eff_nOmegac0     << "\t+/- "<<t_gen/eff_nOmegac0    *getRelEffError(n_nOmegac0   ,n_cc) << units <<" \t(" << n_nOmegac0   << ") " <<100*getRelEffError(n_nOmegac0   ,n_cc)<< "%"<<std::endl;

    file->Close();

    // return std::make_pair(n_bb,total_time);
}

double getRelEffError(int pass, int total){
    double eff = (double)pass/(double(total));

    double abs_err = sqrt(eff*(1-eff)/total);
    return abs_err/eff;
}
double getAbsEffError(int pass, int total){
    double eff = (double)pass/(double(total));

    double abs_err = sqrt(eff*(1-eff)/total);
    return abs_err;
}
// -----------------------------------------------------------------------------
// Get the fractions that don't get picked up bb
// -----------------------------------------------------------------------------


std::pair<std::pair<int,int>,int> getPassThroughbb(std::string name){
    
    TFile* file = TFile::Open(name.c_str());
    if(!file){
        std::cout << "Can't fild file: " << name << std::endl;
        exit(1);
    }
    
    TTree* events = (TTree*)file->Get("events");
    if(!events){
        std::cout << "Can't fild tree: events in file " <<name << std::endl;
        exit(1);
    }

    double n_bb_all     = events->GetEntries("(foundb || hasbOnia)");
    double n_bb_failMPI = n_bb_all - events->GetEntries("(foundb || hasbOnia) && !decMinnivetoMPI");
    double n_bb_failPT  = n_bb_all - events->GetEntries("(foundb || hasbOnia) && !decMinnivetoMPI && !decMinnivetoPT");

    file->Close();

    return std::make_pair(std::make_pair(n_bb_all,n_bb_failMPI),n_bb_failPT);
}

// -----------------------------------------------------------------------------
// Get the fractions that don't get picked up cc
// -----------------------------------------------------------------------------


std::pair<std::pair<int,int>,int> getPassThroughcc(std::string name){
    
    TFile* file = TFile::Open(name.c_str());
    if(!file){
        std::cout << "Can't fild file: " << name << std::endl;
        exit(1);
    }
    
    TTree* events = (TTree*)file->Get("events");
    if(!events){
        std::cout << "Can't fild tree: events in file " <<name << std::endl;
        exit(1);
    }

    double n_cc_all     = events->GetEntries("(foundc || hascOnia)");
    double n_cc_failMPI = n_cc_all - events->GetEntries("(foundc || hascOnia) && !decMinnivetoMPI");
    double n_cc_failPT  = n_cc_all - events->GetEntries("(foundc || hascOnia) && !decMinnivetoMPI && !decMinnivetoPT");

    file->Close();

    return std::make_pair(std::make_pair(n_cc_all,n_cc_failMPI),n_cc_failPT);
}
// -----------------------------------------------------------------------------
// Get the time per Bc
// -----------------------------------------------------------------------------


std::pair<int,double> getTimePerBc(std::string name){
    
    TFile* file = TFile::Open(name.c_str());
    if(!file){
        std::cout << "Can't fild file: " << name << std::endl;
        exit(1);
    }
    
    TTree* events = (TTree*)file->Get("events");
    if(!events){
        std::cout << "Can't fild tree: events in file " <<name << std::endl;
        exit(1);
    }

    double n_Bc = events->GetEntries("nBc>0");

    double timeGen;
    int nBc;
    events->SetBranchAddress("timeGen",&timeGen);
    events->SetBranchAddress("nBc",&nBc);

    double total_time = 0.0;
    std::vector<double> times;
    double temp_time = 0.0;
    for(int i =0; i< events->GetEntries(); i++){
        events->GetEntry(i);
        total_time += timeGen;
        
        temp_time += timeGen;
        if(nBc>0){
            times.push_back(temp_time);
            temp_time = 0.0;
        }
    }
    
    double med = median(times);
    std::cout << "Found n_Bc: " << n_Bc << "\ttimes: " << times.size()  << "\tmedian "<< med << "\tmean "<< total_time/n_Bc<< std::endl;
    file->Close();

    return std::make_pair(n_Bc,total_time);
}

// -----------------------------------------------------------------------------
// Calculate median of vector
// -----------------------------------------------------------------------------
    
double median(std::vector<double> &v){
    size_t n = v.size() / 2;
    std::nth_element(v.begin(), v.begin()+n, v.end());
    return v[n];
}

