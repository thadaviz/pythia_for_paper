#include "tools.h"

void plot_kinematics_Xibc(){

    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;



    std::string pythia_name       = "../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bbcc_N_2000000_PartMod1_WithColRec.root";
    std::string pythia_name_nompi = "../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bbcc_N_2000000_noMPI_PartMod1_WithColRec.root";
    std::string genxicc_name      = "../output/GenXicc/main202output_GenXibc_N_10000_PartMod1.root";

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open(pythia_name.c_str());
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");

    pythia_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");


    pythia_tree->SetAlias("angle_dx_dx","acos((dx_px*bx_px + dx_py*bx_py)/(dx_pt*bx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*bx_px + upsilon1s_py*bx_py)/(upsilon1s_pt*bx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_bx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");


    pythia_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    pythia_tree->SetAlias("delta_eta_dx_bx","dx_eta-bx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_bx","upsilon1s_eta-bx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");

    pythia_tree->SetAlias("delta_y_dx_bx","dx_y-bx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_bx","upsilon1s_y-bx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");

    pythia_tree->SetAlias("delta_R_dx_bx",       "sqrt(delta_eta_dx_bx*delta_eta_dx_bx+angle_dx_bx*angle_dx_bx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_bx","sqrt(delta_eta_upsilon1s_bx*delta_eta_upsilon1s_bx+angle_upsilon1s_bx*angle_upsilon1s_bx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file_nompi = TFile::Open(pythia_name_nompi.c_str());
    TTree* pythia_tree_nompi = (TTree*) pythia_file_nompi->Get("events");

    pythia_tree_nompi->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_nompi->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_nompi->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");


    pythia_tree_nompi->SetAlias("angle_dx_dx","acos((dx_px*bx_px + dx_py*bx_py)/(dx_pt*bx_pt))");
    pythia_tree_nompi->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*bx_px + upsilon1s_py*bx_py)/(upsilon1s_pt*bx_pt))");
    pythia_tree_nompi->SetAlias("angle_upsilon1s_bx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");


    pythia_tree_nompi->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree_nompi->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_nompi->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree_nompi->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree_nompi->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree_nompi->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_nompi->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree_nompi->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree_nompi->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree_nompi->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree_nompi->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree_nompi->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    pythia_tree_nompi->SetAlias("delta_eta_dx_bx","dx_eta-bx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_upsilon1s_bx","upsilon1s_eta-bx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");

    pythia_tree_nompi->SetAlias("delta_y_dx_bx","dx_y-bx_y");
    pythia_tree_nompi->SetAlias("delta_y_upsilon1s_bx","upsilon1s_y-bx_y");
    pythia_tree_nompi->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");

    pythia_tree_nompi->SetAlias("delta_R_dx_bx",       "sqrt(delta_eta_dx_bx*delta_eta_dx_bx+angle_dx_bx*angle_dx_bx)");
    pythia_tree_nompi->SetAlias("delta_R_upsilon1s_bx","sqrt(delta_eta_upsilon1s_bx*delta_eta_upsilon1s_bx+angle_upsilon1s_bx*angle_upsilon1s_bx)");
    pythia_tree_nompi->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");

    double sigmaGen_pythia_nompi;
    double sigmaErr_pythia_nompi;
    pythia_tree_nompi->SetBranchAddress("sigmaGen",&sigmaGen_pythia_nompi);
    pythia_tree_nompi->SetBranchAddress("sigmaErr",&sigmaErr_pythia_nompi);
    
    int n_entries_pythia_nompi = pythia_tree_nompi->GetEntries();
    // -------------------------------------------------------------------------
    // Get GenXicc simulation
    // -------------------------------------------------------------------------

    TFile* genxicc_file = TFile::Open(genxicc_name.c_str());
    TTree* genxicc_tree = (TTree*) genxicc_file->Get("events");
    genxicc_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    genxicc_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");
    genxicc_tree->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");


    genxicc_tree->SetAlias("angle_dx_dx","acos((dx_px*bx_px + dx_py*bx_py)/(dx_pt*bx_pt))");
    genxicc_tree->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*bx_px + upsilon1s_py*bx_py)/(upsilon1s_pt*bx_pt))");
    genxicc_tree->SetAlias("angle_upsilon1s_bx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");


    genxicc_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    genxicc_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    genxicc_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    genxicc_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    genxicc_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    genxicc_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    genxicc_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    genxicc_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    genxicc_tree->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    genxicc_tree->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    genxicc_tree->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    genxicc_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    genxicc_tree->SetAlias("delta_eta_dx_bx","dx_eta-bx_eta");
    genxicc_tree->SetAlias("delta_eta_upsilon1s_bx","upsilon1s_eta-bx_eta");
    genxicc_tree->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");

    genxicc_tree->SetAlias("delta_y_dx_bx","dx_y-bx_y");
    genxicc_tree->SetAlias("delta_y_upsilon1s_bx","upsilon1s_y-bx_y");
    genxicc_tree->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");

    genxicc_tree->SetAlias("delta_R_dx_bx",       "sqrt(delta_eta_dx_bx*delta_eta_dx_bx+angle_dx_bx*angle_dx_bx)");
    genxicc_tree->SetAlias("delta_R_upsilon1s_bx","sqrt(delta_eta_upsilon1s_bx*delta_eta_upsilon1s_bx+angle_upsilon1s_bx*angle_upsilon1s_bx)");
    genxicc_tree->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");

    double sigmaGen_genxicc;
    double sigmaErr_genxicc;
    genxicc_tree->SetBranchAddress("sigmaGen",&sigmaGen_genxicc);
    genxicc_tree->SetBranchAddress("sigmaErr",&sigmaErr_genxicc);

    int n_entries_genxicc = genxicc_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    pythia_tree_nompi->GetEntry(pythia_tree_nompi->GetEntries()-1);
    genxicc_tree->GetEntry(genxicc_tree->GetEntries()-1);

    
    double withtarget_pythia        = pythia_tree->GetEntries("nXibcp>0");
    double withtarget_pythia_nompi  = pythia_tree_nompi->GetEntries("nXibcp>0");
    double withtarget_genxicc       = genxicc_tree->GetEntries("nXibcp>0");

    double corrected_sigmaGen_genxicc       = sigmaGen_genxicc       * (withtarget_genxicc/      n_entries_genxicc);
    double corrected_sigmaGen_pythia        = sigmaGen_pythia        * (withtarget_pythia/       n_entries_pythia);
    double corrected_sigmaGen_pythia_nompi  = sigmaGen_pythia_nompi  * (withtarget_pythia_nompi/ n_entries_pythia_nompi);
    double corrected_sigmaErr_genxicc       = sigmaErr_genxicc       * (withtarget_genxicc/      n_entries_genxicc);
    double corrected_sigmaErr_pythia        = sigmaErr_pythia        * (withtarget_pythia/       n_entries_pythia);
    double corrected_sigmaErr_pythia_nompi  = sigmaErr_pythia_nompi  * (withtarget_pythia_nompi/ n_entries_pythia_nompi);


    std::cout << "Cross section GenXicc: " << sigmaGen_genxicc*microbarn << " +/- " << sigmaErr_genxicc*microbarn << " microbarns"; 
    std::cout << "\t" << genxicc_tree->GetEntries("nXibcp>0") << "/" << genxicc_tree->GetEntries("")<<std::endl;
    
    std::cout << "Cross section Pythia:  " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nXibcp>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
    
    std::cout << "Cross section Pythia No MPI:  " << sigmaGen_pythia_nompi*microbarn  << " +/- " << sigmaErr_pythia_nompi*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree_nompi->GetEntries("nXibcp>0")<< "/" << pythia_tree_nompi->GetEntries("")<< std::endl;


    std::cout << "Corrected Cross section GenXicc:        " << corrected_sigmaGen_genxicc*microbarn << " +/- " << corrected_sigmaErr_genxicc*microbarn << " microbarns"<<std::endl;
    std::cout << "Corrected Cross section Pythia:         " << corrected_sigmaGen_pythia*microbarn  << " +/- " << corrected_sigmaErr_pythia*microbarn << " microbarns"<< std::endl;
    std::cout << "Corrected Cross section Pythia no mpi:  " << corrected_sigmaGen_pythia_nompi*microbarn  << " +/- " << corrected_sigmaErr_pythia_nompi*microbarn << " microbarns"<< std::endl;
    

    double scalefactor_genxicc = 1e-2*corrected_sigmaGen_genxicc*microbarn/withtarget_genxicc;
    double scalefactor_pythia  =      corrected_sigmaGen_pythia*microbarn/withtarget_pythia;
    double scalefactor_pythia_nompi  =      corrected_sigmaGen_pythia_nompi*microbarn/withtarget_pythia_nompi;
    
    std::cout << "scalefactor_genxicc " << scalefactor_genxicc<<std::endl;
    std::cout << "scalefactor_pythia " << scalefactor_pythia<<std::endl;
    std::cout << "scalefactor_pythia_nompi " << scalefactor_pythia_nompi<<std::endl;

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    
    // TCut cuts_all = "isFinal&&abs(id)==5242";
    
    TCut cuts_genxicc      = "nXb==1&&nXc==1&&bx_pt!=0&&dx_pt!=0&&upsilon1s_pt!=0&&nXibcp==1";
    TCut cuts_pythia       = "nXb==1&&nXc==1&&bx_pt!=0&&dx_pt!=0&&upsilon1s_pt!=0&&nXibcp==1";
    TCut cuts_pythia_nompi = "nXb==1&&nXc==1&&bx_pt!=0&&dx_pt!=0&&upsilon1s_pt!=0&&nXibcp==1";
    

    // -------------------------------------------------------------------------
    // Plot 1D kinematics integrated over all eta 
    // -------------------------------------------------------------------------
    // TCut temp_cut = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5) && foundHardc&&hascStatus23&&(hascStatus23_type!=4)";
    // pT
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia  = new TH1D("h_pt_pythia", "h_pt_pythia", 20,0,20);
    TH1D* h_pt_pythia_nompi  = new TH1D("h_pt_pythia_nompi", "h_pt_pythia_nompi", 20,0,20);
    TH1D* h_pt_genxicc = new TH1D("h_pt_genxicc","h_pt_genxicc",20,0,20);

    pythia_tree->Draw("upsilon1s_pt>>h_pt_pythia",cuts_pythia);
    pythia_tree_nompi->Draw("upsilon1s_pt>>h_pt_pythia_nompi",cuts_pythia_nompi);
    genxicc_tree->Draw("upsilon1s_pt>>h_pt_genxicc",cuts_genxicc);

    h_pt_genxicc->Scale(scalefactor_genxicc);
    h_pt_pythia->Scale(scalefactor_pythia);
    h_pt_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_pt_pythia->SetLineColor(kRed);
    h_pt_pythia_nompi->SetLineColor(kBlue);
    h_pt_genxicc->SetLineColor(kGreen+2);
    h_pt_pythia->SetMaximum(1.05*max({   
                                        h_pt_pythia->GetMaximum(),
                                        h_pt_pythia_nompi->GetMaximum(),
                                        h_pt_genxicc->GetMaximum()
                                        }));

    double h_pt_pythia_bin_width = h_pt_pythia->GetXaxis()->GetBinWidth(2);
    h_pt_pythia->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_pythia_bin_width));
    h_pt_pythia->Draw("hist");
    h_pt_pythia_nompi->Draw("same hist");
    h_pt_genxicc->Draw("same hist");

    TLegend* leg_pt = new TLegend(0.6,0.6 ,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,"Pythia With MPI","l");
    leg_pt->AddEntry(h_pt_pythia_nompi,"Pythia No MPI","l");
    leg_pt->AddEntry(h_pt_genxicc,"GenXicc 2.1","l");
    leg_pt->SetTextFont(132);
    leg_pt->SetFillStyle(0);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics_Xibc/c_pt.pdf");


    // eta
    TCanvas* c_eta = new TCanvas("c_eta","c_eta");
    TH1D* h_eta_pythia  = new TH1D("h_eta_pythia", "h_eta_pythia", 20,-10,10);
    TH1D* h_eta_pythia_nompi  = new TH1D("h_eta_pythia_nompi", "h_eta_pythia_nompi", 20,-10,10);
    TH1D* h_eta_genxicc = new TH1D("h_eta_genxicc","h_eta_genxicc",20,-10,10);

    pythia_tree->Draw("upsilon1s_eta>>h_eta_pythia",cuts_pythia);
    pythia_tree_nompi->Draw("upsilon1s_eta>>h_eta_pythia_nompi",cuts_pythia_nompi);
    genxicc_tree->Draw("upsilon1s_eta>>h_eta_genxicc",cuts_genxicc);

    h_eta_genxicc->Scale(scalefactor_genxicc);
    h_eta_pythia->Scale(scalefactor_pythia);
    h_eta_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_eta_pythia->SetLineColor(kRed);
    h_eta_pythia_nompi->SetLineColor(kBlue);
    h_eta_genxicc->SetLineColor(kGreen+2);
    h_eta_pythia->SetMaximum(1.05*max({   
                                        h_eta_pythia->GetMaximum(),
                                        h_eta_pythia_nompi->GetMaximum(),
                                        h_eta_genxicc->GetMaximum()
                                        }));

    double h_eta_pythia_bin_width = h_eta_pythia->GetXaxis()->GetBinWidth(2);
    h_eta_pythia->SetTitle(Form(";#eta [GeV/#it{c}]; d#sigma/d#eta [%.1f #mub]",h_eta_pythia_bin_width));
    h_eta_pythia->Draw("hist");
    h_eta_pythia_nompi->Draw("same hist");
    h_eta_genxicc->Draw("same hist");


    TLegend* leg_eta = new TLegend(0.6,0.6,0.9,0.9);
    leg_eta->AddEntry(h_eta_pythia,"Pythia With MPI","l");
    leg_eta->AddEntry(h_eta_pythia_nompi,"Pythia No MPI","l");
    leg_eta->AddEntry(h_eta_genxicc,"GenXicc 2.1","l");
    leg_eta->SetTextFont(132);
    leg_eta->SetFillStyle(0);
    leg_eta->Draw();
    c_eta->Print("plots_kinematics_Xibc/c_eta.pdf");

    // Rapidity
    TCanvas* c_y = new TCanvas("c_y","c_y");
    TH1D* h_y_pythia        = new TH1D("h_y_pythia",       "h_y_pythia",       50,-10,10);
    TH1D* h_y_pythia_nompi  = new TH1D("h_y_pythia_nompi", "h_y_pythia_nompi", 50,-10,10);
    TH1D* h_y_genxicc       = new TH1D("h_y_genxicc",      "h_y_genxicc",      50,-10,10);

    pythia_tree->Draw("upsilon1s_y>>h_y_pythia",cuts_pythia);
    pythia_tree_nompi->Draw("upsilon1s_y>>h_y_pythia_nompi",cuts_pythia_nompi);
    genxicc_tree->Draw("upsilon1s_y>>h_y_genxicc",cuts_genxicc);

    h_y_genxicc->Scale(scalefactor_genxicc);
    h_y_pythia->Scale(scalefactor_pythia);
    h_y_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_y_pythia->SetLineColor(kRed);
    h_y_pythia_nompi->SetLineColor(kBlue);
    h_y_genxicc->SetLineColor(kGreen+2);
    h_y_pythia->SetMaximum(1.05*max({   
                                        h_y_pythia->GetMaximum(),
                                        h_y_pythia_nompi->GetMaximum(),
                                        h_y_genxicc->GetMaximum()
                                        }));
    std::cout << "Maximum: " << h_y_pythia->GetMaximum() << std::endl;
    double h_y_pythia_bin_width = h_y_pythia->GetXaxis()->GetBinWidth(2);
    h_y_pythia->SetTitle(Form(";#it{y}; d#sigma/d#it{y} [%.1f #mub]",h_y_pythia_bin_width));
    h_y_pythia->Draw("hist");
    h_y_pythia_nompi->Draw("same hist");
    h_y_genxicc->Draw("same hist");


    TLegend* leg_y = new TLegend(0.6,0.6,0.9,0.9);
    leg_y->AddEntry(h_y_pythia,"Pythia With MPI","l");
    leg_y->AddEntry(h_y_pythia_nompi,"Pythia No MPI","l");
    leg_y->AddEntry(h_y_genxicc,"GenXicc 2.1","l");
    leg_y->SetTextFont(132);
    leg_y->SetFillStyle(0);
    leg_y->Draw();

    c_y->Print("plots_kinematics_Xibc/c_y.pdf");

    // -------------------------------------------------------------------------
    // Plot 2D pT vs. eta 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_eta_pythia = new TCanvas("c_2D_pt_eta_pythia","c_2D_pt_eta_pythia");
    TH2D* h_2D_pt_eta_pythia         = new TH2D("h_2D_pt_eta_pythia",        "h_2D_pt_eta_pythia",        20,-10,10,20,0,20);
    TH2D* h_2D_pt_eta_pythia_nompi   = new TH2D("h_2D_pt_eta_pythia_nompi",  "h_2D_pt_eta_pythia_nompi",  20,-10,10,20,0,20);
    TH2D* h_2D_pt_eta_genxicc        = new TH2D("h_2D_pt_eta_genxicc",       "h_2D_pt_eta_genxicc",       20,-10,10,20,0,20);

    pythia_tree->Draw("upsilon1s_pt:upsilon1s_eta>>h_2D_pt_eta_pythia",cuts_pythia);
    pythia_tree_nompi->Draw("upsilon1s_pt:upsilon1s_eta>>h_2D_pt_eta_pythia_nompi",cuts_pythia_nompi);
    genxicc_tree->Draw("upsilon1s_pt:upsilon1s_eta>>h_2D_pt_eta_genxicc",cuts_genxicc);

    h_2D_pt_eta_genxicc->Scale(scalefactor_genxicc);
    h_2D_pt_eta_pythia->Scale(scalefactor_pythia);
    h_2D_pt_eta_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_2D_pt_eta_pythia->SetTitle("; #eta;#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_eta_pythia->Draw("colz");
    c_2D_pt_eta_pythia->Print("plots_kinematics_Xibc/c_2D_pt_eta_pythia.pdf");
    
    TCanvas* c_2D_pt_eta_pythia_nompi = new TCanvas("c_2D_pt_eta_pythia_nompi","c_2D_pt_eta_pythia_nompi");
    h_2D_pt_eta_pythia_nompi->SetTitle("; #eta;#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_eta_pythia_nompi->Draw("colz");
    c_2D_pt_eta_pythia_nompi->Print("plots_kinematics_Xibc/c_2D_pt_eta_pythia_nompi.pdf");

    TCanvas* c_2D_pt_eta_genxicc = new TCanvas("c_2D_pt_eta_genxicc","c_2D_pt_eta_genxicc");
    h_2D_pt_eta_genxicc->SetTitle("; #eta;#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_eta_genxicc->Draw("colz");
    c_2D_pt_eta_genxicc->Print("plots_kinematics_Xibc/c_2D_pt_eta_genxicc.pdf");


    // -------------------------------------------------------------------------
    // Plot 2D pT vs. y 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_y_pythia = new TCanvas("c_2D_pt_y_pythia","c_2D_pt_y_pythia");
    TH2D* h_2D_pt_y_pythia         = new TH2D("h_2D_pt_y_pythia",        "h_2D_pt_y_pythia",        20,-10,10,20,0,20);
    TH2D* h_2D_pt_y_pythia_nompi   = new TH2D("h_2D_pt_y_pythia_nompi",  "h_2D_pt_y_pythia_nompi",  20,-10,10,20,0,20);
    TH2D* h_2D_pt_y_genxicc        = new TH2D("h_2D_pt_y_genxicc",       "h_2D_pt_y_genxicc",       20,-10,10,20,0,20);

    pythia_tree->Draw("upsilon1s_pt:upsilon1s_y>>h_2D_pt_y_pythia",cuts_pythia);
    pythia_tree_nompi->Draw("upsilon1s_pt:upsilon1s_y>>h_2D_pt_y_pythia_nompi",cuts_pythia_nompi);
    genxicc_tree->Draw("upsilon1s_pt:upsilon1s_y>>h_2D_pt_y_genxicc",cuts_genxicc);

    h_2D_pt_y_genxicc->Scale(scalefactor_genxicc);
    h_2D_pt_y_pythia->Scale(scalefactor_pythia);
    h_2D_pt_y_pythia_nompi->Scale(scalefactor_pythia_nompi);

    double max_2D_pt_y_ = 1.05*max({
        h_2D_pt_y_genxicc->GetMaximum(),
        h_2D_pt_y_pythia->GetMaximum(),
        h_2D_pt_y_pythia_nompi->GetMaximum()
    });
    h_2D_pt_y_pythia->SetMaximum(max_2D_pt_y_);
    h_2D_pt_y_pythia_nompi->SetMaximum(max_2D_pt_y_);
    h_2D_pt_y_genxicc->SetMaximum(max_2D_pt_y_);

    h_2D_pt_y_pythia->SetTitle("; #it{y};#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_pythia->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_pythia->Print("plots_kinematics_Xibc/c_2D_pt_y_pythia.pdf");
    
    TCanvas* c_2D_pt_y_pythia_nompi = new TCanvas("c_2D_pt_y_pythia_nompi","c_2D_pt_y_pythia_nompi");
    h_2D_pt_y_pythia_nompi->SetTitle("; #it{y};#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_pythia_nompi->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_pythia_nompi->Print("plots_kinematics_Xibc/c_2D_pt_y_pythia_nompi.pdf");
  

    TCanvas* c_2D_pt_y_genxicc = new TCanvas("c_2D_pt_y_genxicc","c_2D_pt_y_genxicc");
    h_2D_pt_y_genxicc->SetTitle("; #it{y};#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_genxicc->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_genxicc->Print("plots_kinematics_Xibc/c_2D_pt_y_genxicc.pdf");


}