#include "tools.h"

void plot_kinematics_Xicc_compare_productions(){

    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_400000.root");
    TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_9900000_PartMod1.root");
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");

    pythia_tree->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    pythia_tree->SetAlias("angle_jpsi_bx","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");


    pythia_tree->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    pythia_tree->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    pythia_tree->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    pythia_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_jpsi_bx","jpsi_eta-dx2_eta");

    pythia_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    pythia_tree->SetAlias("delta_y_jpsi_bx","jpsi_y-dx2_y");

    pythia_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    pythia_tree->SetAlias("delta_R_jpsi_bx","sqrt(delta_eta_jpsi_bx*delta_eta_jpsi_bx+angle_jpsi_bx*angle_jpsi_bx)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();
    // -------------------------------------------------------------------------
    // Get Pythia nompi simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file_nompi = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_10000000_noMPI_PartMod1.root");
    TTree* pythia_tree_nompi = (TTree*) pythia_file_nompi->Get("events");

    pythia_tree_nompi->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree_nompi->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_nompi->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree_nompi->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree_nompi->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    pythia_tree_nompi->SetAlias("angle_jpsi_bx","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");


    pythia_tree_nompi->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    pythia_tree_nompi->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree_nompi->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    pythia_tree_nompi->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    pythia_tree_nompi->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree_nompi->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_nompi->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree_nompi->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree_nompi->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree_nompi->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_nompi->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree_nompi->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree_nompi->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_jpsi_bx","jpsi_eta-dx2_eta");

    pythia_tree_nompi->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree_nompi->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    pythia_tree_nompi->SetAlias("delta_y_jpsi_bx","jpsi_y-dx2_y");

    pythia_tree_nompi->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree_nompi->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    pythia_tree_nompi->SetAlias("delta_R_jpsi_bx","sqrt(delta_eta_jpsi_bx*delta_eta_jpsi_bx+angle_jpsi_bx*angle_jpsi_bx)");

    double sigmaGen_pythia_nompi;
    double sigmaErr_pythia_nompi;
    pythia_tree_nompi->SetBranchAddress("sigmaGen",&sigmaGen_pythia_nompi);
    pythia_tree_nompi->SetBranchAddress("sigmaErr",&sigmaErr_pythia_nompi);
    
    int n_entries_pythia_nompi = pythia_tree_nompi->GetEntries();
    // -------------------------------------------------------------------------
    // Get GenXicc simulation
    // -------------------------------------------------------------------------

    TFile* genxicc_gg_file = TFile::Open("../output/main202output_GenXicc_N_9000_gg.root");
    TTree* genxicc_gg_tree = (TTree*) genxicc_gg_file->Get("events");

    TFile* genxicc_gc_file = TFile::Open("../output/main202output_GenXicc_N_9000_gc.root");
    TTree* genxicc_gc_tree = (TTree*) genxicc_gc_file->Get("events");

    TFile* genxicc_cc_file = TFile::Open("../output/main202output_GenXicc_N_9000.root");
    TTree* genxicc_cc_tree = (TTree*) genxicc_cc_file->Get("events");


    double sigmaGen_genxicc_cc;
    double sigmaErr_genxicc_cc;
    genxicc_cc_tree->SetBranchAddress("sigmaGen",&sigmaGen_genxicc_cc);
    genxicc_cc_tree->SetBranchAddress("sigmaErr",&sigmaErr_genxicc_cc);
    

    genxicc_cc_tree->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    genxicc_cc_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_cc_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    genxicc_cc_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    genxicc_cc_tree->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    genxicc_cc_tree->SetAlias("angle_jpsi_bx","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");

    genxicc_cc_tree->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    genxicc_cc_tree->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    genxicc_cc_tree->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    genxicc_cc_tree->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    genxicc_cc_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    genxicc_cc_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_cc_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    genxicc_cc_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    genxicc_cc_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    genxicc_cc_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    genxicc_cc_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    genxicc_cc_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    genxicc_cc_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    genxicc_cc_tree->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    genxicc_cc_tree->SetAlias("delta_eta_jpsi_bx","jpsi_eta-dx2_eta");

    genxicc_cc_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    genxicc_cc_tree->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    genxicc_cc_tree->SetAlias("delta_y_jpsi_bx","jpsi_y-dx2_y");



    genxicc_cc_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    genxicc_cc_tree->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    genxicc_cc_tree->SetAlias("delta_R_jpsi_bx","sqrt(delta_eta_jpsi_bx*delta_eta_jpsi_bx+angle_jpsi_bx*angle_jpsi_bx)");
    
    int n_entries_genxicc_cc = genxicc_cc_tree->GetEntries();

    double sigmaGen_genxicc_gc;
    double sigmaErr_genxicc_gc;
    genxicc_gc_tree->SetBranchAddress("sigmaGen",&sigmaGen_genxicc_gc);
    genxicc_gc_tree->SetBranchAddress("sigmaErr",&sigmaErr_genxicc_gc);
    

    genxicc_gc_tree->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    genxicc_gc_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_gc_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    genxicc_gc_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    genxicc_gc_tree->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    genxicc_gc_tree->SetAlias("angle_jpsi_bx","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");

    genxicc_gc_tree->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    genxicc_gc_tree->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    genxicc_gc_tree->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    genxicc_gc_tree->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    genxicc_gc_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    genxicc_gc_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_gc_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    genxicc_gc_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    genxicc_gc_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    genxicc_gc_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    genxicc_gc_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    genxicc_gc_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    genxicc_gc_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    genxicc_gc_tree->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    genxicc_gc_tree->SetAlias("delta_eta_jpsi_bx","jpsi_eta-dx2_eta");

    genxicc_gc_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    genxicc_gc_tree->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    genxicc_gc_tree->SetAlias("delta_y_jpsi_bx","jpsi_y-dx2_y");



    genxicc_gc_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    genxicc_gc_tree->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    genxicc_gc_tree->SetAlias("delta_R_jpsi_bx","sqrt(delta_eta_jpsi_bx*delta_eta_jpsi_bx+angle_jpsi_bx*angle_jpsi_bx)");
    
    int n_entries_genxicc_gc = genxicc_gc_tree->GetEntries();


    double sigmaGen_genxicc_gg;
    double sigmaErr_genxicc_gg;
    genxicc_gg_tree->SetBranchAddress("sigmaGen",&sigmaGen_genxicc_gg);
    genxicc_gg_tree->SetBranchAddress("sigmaErr",&sigmaErr_genxicc_gg);
    

    genxicc_gg_tree->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    genxicc_gg_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_gg_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    genxicc_gg_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    genxicc_gg_tree->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    genxicc_gg_tree->SetAlias("angle_jpsi_bx","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");

    genxicc_gg_tree->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    genxicc_gg_tree->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    genxicc_gg_tree->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    genxicc_gg_tree->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    genxicc_gg_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    genxicc_gg_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_gg_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    genxicc_gg_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    genxicc_gg_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    genxicc_gg_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    genxicc_gg_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    genxicc_gg_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    genxicc_gg_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    genxicc_gg_tree->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    genxicc_gg_tree->SetAlias("delta_eta_jpsi_bx","jpsi_eta-dx2_eta");

    genxicc_gg_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    genxicc_gg_tree->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    genxicc_gg_tree->SetAlias("delta_y_jpsi_bx","jpsi_y-dx2_y");



    genxicc_gg_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    genxicc_gg_tree->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    genxicc_gg_tree->SetAlias("delta_R_jpsi_bx","sqrt(delta_eta_jpsi_bx*delta_eta_jpsi_bx+angle_jpsi_bx*angle_jpsi_bx)");
    
    int n_entries_genxicc_gg = genxicc_gg_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    pythia_tree_nompi->GetEntry(pythia_tree_nompi->GetEntries()-1);
    genxicc_cc_tree->GetEntry(genxicc_cc_tree->GetEntries()-1);
    genxicc_gc_tree->GetEntry(genxicc_gc_tree->GetEntries()-1);
    genxicc_gg_tree->GetEntry(genxicc_gg_tree->GetEntries()-1);

    std::cout << "Cross section GenXicc cc: " << sigmaGen_genxicc_cc*microbarn << " +/- " << sigmaErr_genxicc_cc*microbarn << " microbarns"; 
    std::cout << "\t" << genxicc_cc_tree->GetEntries("nXiccpp>0") << "/" << genxicc_cc_tree->GetEntries("")<<std::endl;
   
    std::cout << "Cross section GenXicc gg: " << sigmaGen_genxicc_gg*microbarn << " +/- " << sigmaErr_genxicc_gg*microbarn << " microbarns"; 
    std::cout << "\t" << genxicc_gg_tree->GetEntries("nXiccpp>0") << "/" << genxicc_gg_tree->GetEntries("")<<std::endl;
   
    std::cout << "Cross section GenXicc gc: " << sigmaGen_genxicc_gc*microbarn << " +/- " << sigmaErr_genxicc_gc*microbarn << " microbarns"; 
    std::cout << "\t" << genxicc_gc_tree->GetEntries("nXiccpp>0") << "/" << genxicc_gc_tree->GetEntries("")<<std::endl;
    
    std::cout << "Cross section Pythia:  " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nXiccpp>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
       
    std::cout << "Cross section Pythia nompi:  " << sigmaGen_pythia_nompi*microbarn  << " +/- " << sigmaErr_pythia_nompi*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree_nompi->GetEntries("nXiccpp>0")<< "/" << pythia_tree_nompi->GetEntries("")<< std::endl;
    
    double withtarget_pythia       = pythia_tree->GetEntries("nXiccpp>0");
    double withtarget_pythia_nompi = pythia_tree_nompi->GetEntries("nXiccpp>0");
    double withtarget_genxicc_cc   = genxicc_cc_tree->GetEntries("nXiccpp>0");
    double withtarget_genxicc_gg   = genxicc_gg_tree->GetEntries("nXiccpp>0");
    double withtarget_genxicc_gc   = genxicc_gc_tree->GetEntries("nXiccpp>0");

    // double scalefactor_genxicc_cc = sigmaGen_genxicc_cc*microbarn/n_entries_genxicc_cc * (withtarget_genxicc_cc/n_entries_genxicc_cc);
    // double scalefactor_genxicc_gg = sigmaGen_genxicc_gg*microbarn/n_entries_genxicc_gg * (withtarget_genxicc_gg/n_entries_genxicc_gg);
    // double scalefactor_genxicc_gc = sigmaGen_genxicc_gc*microbarn/n_entries_genxicc_gc * (withtarget_genxicc_gc/n_entries_genxicc_gc);
    // double scalefactor_pythia     = sigmaGen_pythia*microbarn/n_entries_pythia         * (withtarget_pythia    /n_entries_pythia    );

    double scalefactor_genxicc_cc   = sigmaGen_genxicc_cc*microbarn/withtarget_genxicc_cc ;
    double scalefactor_genxicc_gg   = sigmaGen_genxicc_gg*microbarn/withtarget_genxicc_gg ;
    double scalefactor_genxicc_gc   = sigmaGen_genxicc_gc*microbarn/withtarget_genxicc_gc ;
    double scalefactor_pythia       = sigmaGen_pythia*microbarn/withtarget_pythia         ;
    double scalefactor_pythia_nompi = sigmaGen_pythia_nompi*microbarn/withtarget_pythia_nompi;
    
    std::cout << "scalefactor_genxicc_cc   " << scalefactor_genxicc_cc<<std::endl;
    std::cout << "scalefactor_genxicc_gg   " << scalefactor_genxicc_gg<<std::endl;
    std::cout << "scalefactor_genxicc_gc   " << scalefactor_genxicc_gc<<std::endl;
    std::cout << "scalefactor_pythia       " << scalefactor_pythia<<std::endl;
    std::cout << "scalefactor_pythia_nompi " << scalefactor_pythia_nompi<<std::endl;

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    TCut cuts_genxicc_cc   = "ncquark+ncquarkbar==4&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0&&nXiccpp==1";
    TCut cuts_genxicc_gc   = "ncquark+ncquarkbar==3&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0&&nXiccpp==1";
    TCut cuts_genxicc_gg   = "ncquark+ncquarkbar==2&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0&&nXiccpp==1";
    TCut cuts_pythia       = "ncquark+ncquarkbar==4&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0&&nXiccpp==1";
    TCut cuts_pythia_nompi = "ncquark+ncquarkbar==4&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0&&nXiccpp==1";
    
    // TCut cuts_all = "dx2_pt>2 && abs(jpsi_eta)>2 && abs(jpsi_eta)<5";
    TCut cuts_all = "";
    

    // -------------------------------------------------------------------------
    // Plot 1D kinematics integrated over all eta 
    // -------------------------------------------------------------------------
    // TCut temp_cut = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5) && foundHardc&&hascStatus23&&(hascStatus23_type!=4)";
    // pT
    
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia        = new TH1D("h_pt_pythia",       "h_pt_pythia",       50,0,10);
    TH1D* h_pt_pythia_nompi  = new TH1D("h_pt_pythia_nompi", "h_pt_pythia_nompi", 50,0,10);
    TH1D* h_pt_genxicc_cc    = new TH1D("h_pt_genxicc_cc",   "h_pt_genxicc_cc",   50,0,10);
    TH1D* h_pt_genxicc_gc    = new TH1D("h_pt_genxicc_gc",   "h_pt_genxicc_gc",   50,0,10);
    TH1D* h_pt_genxicc_gg    = new TH1D("h_pt_genxicc_gg",   "h_pt_genxicc_gg",   50,0,10);

    pythia_tree->Draw(      "jpsi_pt>>h_pt_pythia",      cuts_pythia);
    pythia_tree_nompi->Draw("jpsi_pt>>h_pt_pythia_nompi",cuts_pythia_nompi);
    genxicc_cc_tree->Draw(  "jpsi_pt>>h_pt_genxicc_cc",  cuts_genxicc_cc);
    genxicc_gc_tree->Draw(  "jpsi_pt>>h_pt_genxicc_gc",  cuts_genxicc_gc);
    genxicc_gg_tree->Draw(  "jpsi_pt>>h_pt_genxicc_gg",  cuts_genxicc_gg);

    h_pt_genxicc_cc->Scale(scalefactor_genxicc_cc);
    h_pt_genxicc_gg->Scale(scalefactor_genxicc_gg);
    h_pt_genxicc_gc->Scale(scalefactor_genxicc_gc);
    h_pt_pythia->Scale(scalefactor_pythia);
    h_pt_pythia_nompi->Scale(scalefactor_pythia_nompi);


    // h_pt_genxicc_cc->Scale(1.0/h_pt_genxicc_cc->Integral());
    // h_pt_genxicc_gg->Scale(1.0/h_pt_genxicc_gg->Integral());
    // h_pt_genxicc_gc->Scale(1.0/h_pt_genxicc_gc->Integral());
    // h_pt_pythia->Scale(1.0/h_pt_pythia->Integral());

    h_pt_pythia->SetMaximum(1.05*max({
        h_pt_genxicc_cc->GetMaximum(),
        h_pt_genxicc_gg->GetMaximum(),
        h_pt_genxicc_gc->GetMaximum(),
        h_pt_pythia->GetMaximum(),
        h_pt_pythia_nompi->GetMaximum()
    }));

    h_pt_pythia->SetLineColor(kRed);
    h_pt_pythia_nompi->SetLineColor(kBlue);
    h_pt_genxicc_gc->SetLineColor(kGreen+2);
    h_pt_genxicc_gc->SetLineStyle(kDashed);
    h_pt_genxicc_gg->SetLineColor(kGreen+2);
    h_pt_genxicc_cc->SetLineColor(kGreen+2);

    double h_pt_pythia_bin_width = h_pt_pythia->GetXaxis()->GetBinWidth(2);
    h_pt_pythia->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_pythia_bin_width));
    h_pt_pythia->Draw("hist");
    h_pt_pythia_nompi->Draw("hist same");
    // h_pt_genxicc_cc->Draw("same hist");
    h_pt_genxicc_gc->Draw("same hist");
    h_pt_genxicc_gg->Draw("same hist");

    TLegend* leg_pt = new TLegend(0.6,0.6,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,"Pythia With MPI","l");
    leg_pt->AddEntry(h_pt_pythia_nompi,"Pythia No MPI","l");
    // leg_pt->AddEntry(h_pt_genxicc_cc,"GenXicc 2.1 cc#rightarrow#Xi_{cc}^{++}+...","l");
    leg_pt->AddEntry(h_pt_genxicc_gc,"GenXicc 2.1 gc#rightarrow#Xi_{cc}^{++}+...","l");
    leg_pt->AddEntry(h_pt_genxicc_gg,"GenXicc 2.1 gg#rightarrow#Xi_{cc}^{++}+...","l");
    leg_pt->SetTextFont(132);
    leg_pt->SetFillStyle(0);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics_Xicc_compare_productions/c_pt.pdf");

    // -------------------------------------------------------------------------
    // eta
    // -------------------------------------------------------------------------

    TCanvas* c_eta = new TCanvas("c_eta","c_eta");
    TH1D* h_eta_pythia  = new TH1D("h_eta_pythia", "h_eta_pythia", 50,-10,10);
    TH1D* h_eta_pythia_nompi  = new TH1D("h_eta_pythia_nompi", "h_eta_pythia_nompi", 50,-10,10);
    TH1D* h_eta_genxicc_cc = new TH1D("h_eta_genxicc_cc","h_eta_genxicc_cc",50,-10,10);
    TH1D* h_eta_genxicc_gg = new TH1D("h_eta_genxicc_gg","h_eta_genxicc_gg",50,-10,10);
    TH1D* h_eta_genxicc_gc = new TH1D("h_eta_genxicc_gc","h_eta_genxicc_gc",50,-10,10);

    pythia_tree->Draw(      "jpsi_eta>>h_eta_pythia",      cuts_pythia);
    pythia_tree_nompi->Draw("jpsi_eta>>h_eta_pythia_nompi",cuts_pythia_nompi);
    genxicc_cc_tree->Draw(  "jpsi_eta>>h_eta_genxicc_cc",  cuts_genxicc_cc);
    genxicc_gg_tree->Draw(  "jpsi_eta>>h_eta_genxicc_gg",  cuts_genxicc_gg);
    genxicc_gc_tree->Draw(  "jpsi_eta>>h_eta_genxicc_gc",  cuts_genxicc_gc);

    h_eta_genxicc_cc->Scale(scalefactor_genxicc_cc);
    h_eta_genxicc_gg->Scale(scalefactor_genxicc_gg);
    h_eta_genxicc_gc->Scale(scalefactor_genxicc_gc);
    h_eta_pythia->Scale(scalefactor_pythia);
    h_eta_pythia_nompi->Scale(scalefactor_pythia_nompi);

    // h_eta_genxicc_cc->Scale(1.0/h_eta_genxicc_cc->Integral());
    // h_eta_genxicc_gg->Scale(1.0/h_eta_genxicc_gg->Integral());
    // h_eta_genxicc_gc->Scale(1.0/h_eta_genxicc_gc->Integral());
    // h_eta_pythia->Scale(1.0/h_eta_pythia->Integral());

    h_eta_pythia->SetMaximum(1.05*max({
        h_eta_genxicc_cc->GetMaximum(),
        h_eta_genxicc_gg->GetMaximum(),
        h_eta_genxicc_gc->GetMaximum(),
        h_eta_pythia->GetMaximum(),
        h_eta_pythia_nompi->GetMaximum()
    }));

    h_eta_pythia->SetLineColor(kRed);
    h_eta_pythia_nompi->SetLineColor(kBlue);
    h_eta_genxicc_gc->SetLineColor(kGreen+2);
    h_eta_genxicc_gc->SetLineStyle(kDashed);
    h_eta_genxicc_gg->SetLineColor(kGreen+2);
    h_eta_genxicc_cc->SetLineColor(kGreen+2);

    double h_eta_pythia_bin_width = h_eta_pythia->GetXaxis()->GetBinWidth(2);
    h_eta_pythia->SetTitle(Form(";#eta; d#sigma/d#eta [%.1f #mub]",h_eta_pythia_bin_width));
    h_eta_pythia->Draw("hist");
    h_eta_pythia_nompi->Draw("same hist");
    // h_eta_genxicc_cc->Draw("same hist");
    h_eta_genxicc_gc->Draw("same hist");
    h_eta_genxicc_gg->Draw("same hist");


    TLegend* leg_eta = new TLegend(0.6,0.6,0.9,0.9);
    leg_eta->AddEntry(h_eta_pythia,"Pythia With MPI","l");
    leg_eta->AddEntry(h_eta_pythia_nompi,"Pythia No MPI","l");
    // leg_eta->AddEntry(h_eta_genxicc_cc,"GenXicc 2.1 cc#rightarrow#Xi_{cc}^{++}+...","l");
    leg_eta->AddEntry(h_eta_genxicc_gc,"GenXicc 2.1 gc#rightarrow#Xi_{cc}^{++}+...","l");
    leg_eta->AddEntry(h_eta_genxicc_gg,"GenXicc 2.1 gg#rightarrow#Xi_{cc}^{++}+...","l");
    leg_eta->SetTextFont(132);
    leg_eta->SetFillStyle(0);
    leg_eta->Draw();
    c_eta->Print("plots_kinematics_Xicc_compare_productions/c_eta.pdf");

    // -------------------------------------------------------------------------
    // Rapidity
    // -------------------------------------------------------------------------
    TCanvas* c_y = new TCanvas("c_y","c_y");
    TH1D* h_y_pythia        = new TH1D("h_y_pythia",       "h_y_pythia",       50,-10,10);
    TH1D* h_y_pythia_nompi  = new TH1D("h_y_pythia_nompi", "h_y_pythia_nompi", 50,-10,10);
    TH1D* h_y_genxicc_cc    = new TH1D("h_y_genxicc_cc",   "h_y_genxicc_cc",   50,-10,10);
    TH1D* h_y_genxicc_gc    = new TH1D("h_y_genxicc_gc",   "h_y_genxicc_gc",   50,-10,10);
    TH1D* h_y_genxicc_gg    = new TH1D("h_y_genxicc_gg",   "h_y_genxicc_gg",   50,-10,10);
   
    pythia_tree->Draw("jpsi_y>>h_y_pythia",cuts_pythia);
    pythia_tree_nompi->Draw("jpsi_y>>h_y_pythia_nompi",cuts_pythia_nompi);
    genxicc_cc_tree->Draw("jpsi_y>>h_y_genxicc_cc",cuts_genxicc_cc);
    genxicc_gc_tree->Draw("jpsi_y>>h_y_genxicc_gc",cuts_genxicc_gc);
    genxicc_gg_tree->Draw("jpsi_y>>h_y_genxicc_gg",cuts_genxicc_gg);


    h_y_genxicc_cc->Scale(scalefactor_genxicc_cc);
    h_y_genxicc_gg->Scale(scalefactor_genxicc_gg);
    h_y_genxicc_gc->Scale(scalefactor_genxicc_gc);
    h_y_pythia->Scale(scalefactor_pythia);
    h_y_pythia_nompi->Scale(scalefactor_pythia_nompi);

    // h_y_genxicc_cc->Scale(1.0/h_y_genxicc_cc->Integral());
    // h_y_genxicc_gg->Scale(1.0/h_y_genxicc_gg->Integral());
    // h_y_genxicc_gc->Scale(1.0/h_y_genxicc_gc->Integral());
    // h_y_pythia->Scale(1.0/h_y_pythia->Integral());

    h_y_pythia->SetMaximum(1.05*max({
        h_y_genxicc_cc->GetMaximum(),
        h_y_genxicc_gg->GetMaximum(),
        h_y_genxicc_gc->GetMaximum(),
        h_y_pythia->GetMaximum(),
        h_y_pythia_nompi->GetMaximum()
    }));

    h_y_pythia->SetLineColor(kRed);
    h_y_pythia_nompi->SetLineColor(kBlue);
    h_y_genxicc_gc->SetLineColor(kGreen+2);
    h_y_genxicc_gc->SetLineStyle(kDashed);
    h_y_genxicc_gg->SetLineColor(kGreen+2);
    h_y_genxicc_cc->SetLineColor(kGreen+2);

    double h_y_pythia_bin_width = h_y_pythia->GetXaxis()->GetBinWidth(2);
    h_y_pythia->SetTitle(Form(";#it{y}; d#sigma/d#it{y} [%.1f #mub]",h_y_pythia_bin_width));

    h_y_pythia->Draw("hist");
    h_y_pythia_nompi->Draw("same hist");
    // h_y_genxicc_cc->Draw("same hist");
    h_y_genxicc_gc->Draw("same hist");
    h_y_genxicc_gg->Draw("same hist");


    TLegend* leg_y = new TLegend(0.6,0.6,0.9,0.9);
    leg_y->AddEntry(h_y_pythia,"Pythia With MPI","l");
    leg_y->AddEntry(h_y_pythia_nompi,"Pythia No MPI","l");
    // leg_y->AddEntry(h_y_genxicc_cc,"GenXicc 2.1 cc#rightarrow#Xi_{cc}^{++}+...","l");
    leg_y->AddEntry(h_y_genxicc_gc,"GenXicc 2.1 gc#rightarrow#Xi_{cc}^{++}+...","l");
    leg_y->AddEntry(h_y_genxicc_gg,"GenXicc 2.1 gg#rightarrow#Xi_{cc}^{++}+...","l");
    leg_y->SetTextFont(132);
    leg_y->SetFillStyle(0);
    leg_y->Draw();

    c_y->Print("plots_kinematics_Xicc_compare_productions/c_y.pdf");




    // -------------------------------------------------------------------------
    // Plot 2D pT vs. y 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_y_pythia = new TCanvas("c_2D_pt_y_pythia","c_2D_pt_y_pythia");
    TH2D* h_2D_pt_y_pythia       = new TH2D("h_2D_pt_y_pythia",      "h_2D_pt_y_pythia",      20,-10,10,20,0,10);
    TH2D* h_2D_pt_y_pythia_nompi = new TH2D("h_2D_pt_y_pythia_nompi","h_2D_pt_y_pythia_nompi",20,-10,10,20,0,10);
    TH2D* h_2D_pt_y_genxicc_cc   = new TH2D("h_2D_pt_y_genxicc_cc",  "h_2D_pt_y_genxicc_cc",  20,-10,10,20,0,10);
    TH2D* h_2D_pt_y_genxicc_gc   = new TH2D("h_2D_pt_y_genxicc_gc",  "h_2D_pt_y_genxicc_gc",  20,-10,10,20,0,10);
    TH2D* h_2D_pt_y_genxicc_gg   = new TH2D("h_2D_pt_y_genxicc_gg",  "h_2D_pt_y_genxicc_gg",  20,-10,10,20,0,10);

    pythia_tree->Draw(    "jpsi_pt:jpsi_y>>h_2D_pt_y_pythia",             cuts_pythia    );
    pythia_tree_nompi->Draw(    "jpsi_pt:jpsi_y>>h_2D_pt_y_pythia_nompi", cuts_pythia_nompi    );
    genxicc_cc_tree->Draw("jpsi_pt:jpsi_y>>h_2D_pt_y_genxicc_cc",         cuts_genxicc_cc);
    genxicc_gc_tree->Draw("jpsi_pt:jpsi_y>>h_2D_pt_y_genxicc_gc",         cuts_genxicc_gc);
    genxicc_gg_tree->Draw("jpsi_pt:jpsi_y>>h_2D_pt_y_genxicc_gg",         cuts_genxicc_gg);


    h_2D_pt_y_genxicc_cc->Scale(scalefactor_genxicc_cc);
    h_2D_pt_y_genxicc_gg->Scale(scalefactor_genxicc_gg);
    h_2D_pt_y_genxicc_gc->Scale(scalefactor_genxicc_gc);
    h_2D_pt_y_pythia->Scale(scalefactor_pythia);
    h_2D_pt_y_pythia_nompi->Scale(scalefactor_pythia_nompi);

    double h_2D_pt_y_max = 1.05*max({
        // h_2D_pt_y_genxicc_cc->GetMaximum(),
        h_2D_pt_y_genxicc_gg->GetMaximum(),
        h_2D_pt_y_genxicc_gc->GetMaximum(),
        h_2D_pt_y_pythia->GetMaximum(),
        h_2D_pt_y_pythia_nompi->GetMaximum()
    });

    h_2D_pt_y_genxicc_cc->SetMaximum(h_2D_pt_y_max);
    h_2D_pt_y_genxicc_gg->SetMaximum(h_2D_pt_y_max);
    h_2D_pt_y_genxicc_gc->SetMaximum(h_2D_pt_y_max);
    h_2D_pt_y_pythia->SetMaximum(h_2D_pt_y_max);
    h_2D_pt_y_pythia_nompi->SetMaximum(h_2D_pt_y_max);

    h_2D_pt_y_pythia->SetTitle(";#it{y};#it{p}_{T} [GeV/c]");
    h_2D_pt_y_pythia->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_pythia->Print("plots_kinematics_Xicc_compare_productions/c_2D_pt_y_pythia.pdf");
    
    TCanvas* c_2D_pt_y_pythia_nompi = new TCanvas("c_2D_pt_y_pythia_nompi","c_2D_pt_y_pythia_nompi");
    h_2D_pt_y_pythia_nompi->SetTitle(";#it{y};#it{p}_{T} [GeV/c]");
    h_2D_pt_y_pythia_nompi->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_pythia_nompi->Print("plots_kinematics_Xicc_compare_productions/c_2D_pt_y_pythia_nompi.pdf");
    
    TCanvas* c_2D_pt_y_genxicc_cc = new TCanvas("c_2D_pt_y_genxicc_cc","c_2D_pt_y_genxicc_cc");
    h_2D_pt_y_genxicc_cc->SetTitle(";#it{y};#it{p}_{T} [GeV/c]");
    h_2D_pt_y_genxicc_cc->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_genxicc_cc->Print("plots_kinematics_Xicc_compare_productions/c_2D_pt_y_genxicc_cc.pdf");
    

    TCanvas* c_2D_pt_y_genxicc_gc = new TCanvas("c_2D_pt_y_genxicc_gc","c_2D_pt_y_genxicc_gc");
    h_2D_pt_y_genxicc_gc->SetTitle(";#it{y};#it{p}_{T} [GeV/c]");
    h_2D_pt_y_genxicc_gc->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_genxicc_gc->Print("plots_kinematics_Xicc_compare_productions/c_2D_pt_y_genxicc_gc.pdf");
    

    TCanvas* c_2D_pt_y_genxicc_gg = new TCanvas("c_2D_pt_y_genxicc_gg","c_2D_pt_y_genxicc_gg");
    h_2D_pt_y_genxicc_gg->SetTitle(";#it{y};#it{p}_{T} [GeV/c]");
    h_2D_pt_y_genxicc_gg->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_genxicc_gg->Print("plots_kinematics_Xicc_compare_productions/c_2D_pt_y_genxicc_gg.pdf");

    // // -------------------------------------------------------------------------
    // // Plot 1D angles integrated over all eta 
    // // -------------------------------------------------------------------------

    // // angles
    // TCanvas* c_delta_phi_jpsi_bx = new TCanvas("c_delta_phi_jpsi_bx","c_delta_phi_jpsi_bx");
    // TH1D* h_delta_phi_jpsi_dx2_pythia  = new TH1D("h_delta_phi_jpsi_dx2_pythia", "h_delta_phi_jpsi_dx2_pythia", 50,0,TMath::Pi());
    // TH1D* h_delta_phi_jpsi_dx2_genxicc = new TH1D("h_delta_phi_jpsi_dx2_genxicc","h_delta_phi_jpsi_dx2_genxicc",50,0,TMath::Pi());

    // pythia_tree->Draw("angle_jpsi_bx>>h_delta_phi_jpsi_dx2_pythia",  cuts_pythia +cuts_all);
    // genxicc_cc_tree->Draw("angle_jpsi_bx>>h_delta_phi_jpsi_dx2_genxicc",cuts_genxicc_cc+cuts_all);

    // h_delta_phi_jpsi_dx2_genxicc->Scale(scalefactor_genxicc);
    // h_delta_phi_jpsi_dx2_pythia->Scale(scalefactor_pythia);

    // h_delta_phi_jpsi_dx2_pythia->SetLineColor(kRed);
    // h_delta_phi_jpsi_dx2_genxicc->SetLineColor(kGreen+2);

    // h_delta_phi_jpsi_dx2_genxicc->SetTitle(";#Delta#phi(#Xi_{cc}, X_{c}) [rad]; d#sigma/d#Delta#phi");
    // h_delta_phi_jpsi_dx2_genxicc->SetMinimum(0.0);
    

    // h_delta_phi_jpsi_dx2_genxicc->Draw("hist");
    // h_delta_phi_jpsi_dx2_pythia->Draw("hist same");
    


    // TLegend* leg_delta_phi_jpsi_bx = new TLegend(0.7,0.2,0.9,0.4);
    // leg_delta_phi_jpsi_bx->AddEntry(h_delta_phi_jpsi_dx2_pythia,"Pythia","l");
    // leg_delta_phi_jpsi_bx->AddEntry(h_delta_phi_jpsi_dx2_genxicc,"GenXicc 2.1","l");
    // leg_delta_phi_jpsi_bx->SetTextFont(132);
    // leg_delta_phi_jpsi_bx->Draw();

    // c_delta_phi_jpsi_bx->Print("plots_kinematics_Xicc_compare_productions/c_delta_phi_jpsi_bx.pdf");


    // // angles
    // TCanvas* c_delta_phi_jpsi_dx = new TCanvas("c_delta_phi_jpsi_dx","c_delta_phi_jpsi_dx");
    // TH1D* h_delta_phi_jpsi_dx_pythia  = new TH1D("h_delta_phi_jpsi_dx_pythia", "h_delta_phi_jpsi_dx_pythia", 50,0,TMath::Pi());
    // TH1D* h_delta_phi_jpsi_dx_genxicc = new TH1D("h_delta_phi_jpsi_dx_genxicc","h_delta_phi_jpsi_dx_genxicc",50,0,TMath::Pi());

    // pythia_tree->Draw("angle_jpsi_dx>>h_delta_phi_jpsi_dx_pythia",  cuts_pythia +cuts_all);
    // genxicc_cc_tree->Draw("angle_jpsi_dx>>h_delta_phi_jpsi_dx_genxicc",cuts_genxicc_cc+cuts_all);

    // h_delta_phi_jpsi_dx_genxicc->Scale(scalefactor_genxicc);
    // h_delta_phi_jpsi_dx_pythia->Scale(scalefactor_pythia);

    // h_delta_phi_jpsi_dx_pythia->SetLineColor(kRed);
    // h_delta_phi_jpsi_dx_genxicc->SetLineColor(kGreen+2);

    // h_delta_phi_jpsi_dx_pythia->SetTitle(";#Delta#phi(#Xi_{cc}, X_{c}) [rad]; d#sigma/d#Delta#phi");
    // h_delta_phi_jpsi_dx_pythia->SetMinimum(0.0);
    // h_delta_phi_jpsi_dx_pythia->Draw("hist");
    // h_delta_phi_jpsi_dx_genxicc->Draw("hist same");


    // TLegend* leg_delta_phi_jpsi_dx = new TLegend(0.7,0.2,0.9,0.4);
    // leg_delta_phi_jpsi_dx->AddEntry(h_delta_phi_jpsi_dx_pythia,"Pythia","l");
    // leg_delta_phi_jpsi_dx->AddEntry(h_delta_phi_jpsi_dx_genxicc,"GenXicc 2.1","l");
    // leg_delta_phi_jpsi_dx->SetTextFont(132);
    // leg_delta_phi_jpsi_dx->Draw();
    // c_delta_phi_jpsi_dx->Print("plots_kinematics_Xicc_compare_productions/c_delta_phi_jpsi_dx.pdf");

    // // -------------------------------------------------------------------------
    // // Plot 2D angles integrated over all eta 
    // // -------------------------------------------------------------------------


    // SetLHCbStyle("cont");

    // // angles
    // TCanvas* c_delta_phi_pythia = new TCanvas("c_delta_phi_pythia","c_delta_phi_pythia");
    // TH2D* h_delta_phi_pythia  = new TH2D("h_delta_phi_pythia", "h_delta_phi_pythia", 20,0,TMath::Pi(),20,0,TMath::Pi());
    // TH2D* h_delta_phi_genxicc = new TH2D("h_delta_phi_genxicc","h_delta_phi_genxicc",20,0,TMath::Pi(),20,0,TMath::Pi());

    // pythia_tree->Draw("angle_jpsi_dx:angle_jpsi_bx>>h_delta_phi_pythia",  cuts_pythia +cuts_all);
    // genxicc_cc_tree->Draw("angle_jpsi_dx:angle_jpsi_bx>>h_delta_phi_genxicc",cuts_genxicc_cc+cuts_all);

    // h_delta_phi_genxicc->Scale(scalefactor_genxicc);
    // h_delta_phi_pythia->Scale(scalefactor_pythia);

    // h_delta_phi_pythia->SetTitle(";#Delta#phi(#Xi_{cc}, X_{c}) [rad]; #Delta#phi(#Xi_{cc}, X_{c}) [rad]");
    // h_delta_phi_pythia->Draw("colz");
    // c_delta_phi_pythia->Print("plots_kinematics_Xicc_compare_productions/c_delta_phi_pythia.pdf");
    
    // TCanvas* c_delta_phi_genxicc = new TCanvas("c_delta_phi_genxicc","c_delta_phi_genxicc");
    // h_delta_phi_genxicc->SetTitle(";#Delta#phi(#Xi_{cc}, X_{c}) [rad]; #Delta#phi(#Xi_{cc}, X_{c}) [rad]");
    // h_delta_phi_genxicc->Draw("colz");

    // c_delta_phi_genxicc->Print("plots_kinematics_Xicc_compare_productions/c_delta_phi_genxicc.pdf");

    // // eta
    // TCanvas* c_delta_eta_pythia = new TCanvas("c_delta_eta_pythia","c_delta_eta_pythia");
    // TH2D* h_delta_eta_pythia  = new TH2D("h_delta_eta_pythia", "h_delta_eta_pythia", 20,-5.0,5.0,20,-5.0,5.0);
    // TH2D* h_delta_eta_genxicc = new TH2D("h_delta_eta_genxicc","h_delta_eta_genxicc",20,-5.0,5.0,20,-5.0,5.0);

    // pythia_tree->Draw("delta_eta_jpsi_dx:delta_eta_jpsi_bx>>h_delta_eta_pythia",  cuts_pythia +cuts_all);
    // genxicc_cc_tree->Draw("delta_eta_jpsi_dx:delta_eta_jpsi_bx>>h_delta_eta_genxicc",cuts_genxicc_cc+cuts_all);

    // h_delta_eta_genxicc->Scale(scalefactor_genxicc);
    // h_delta_eta_pythia->Scale(scalefactor_pythia);

    // h_delta_eta_pythia->SetTitle(";#Delta#eta(#Xi_{cc}, X_{c}) [rad]; #Delta#eta(#Xi_{cc}, X_{c}) [rad]");
    // h_delta_eta_pythia->Draw("colz");
    // c_delta_eta_pythia->Print("plots_kinematics_Xicc_compare_productions/c_delta_eta_pythia.pdf");
    
    // TCanvas* c_delta_eta_genxicc = new TCanvas("c_delta_eta_genxicc","c_delta_eta_genxicc");
    // h_delta_eta_genxicc->SetTitle(";#Delta#eta(#Xi_{cc}, X_{c}) [rad]; #Delta#eta(#Xi_{cc}, X_{c}) [rad]");
    // h_delta_eta_genxicc->Draw("colz");
    // c_delta_eta_genxicc->Print("plots_kinematics_Xicc_compare_productions/c_delta_eta_genxicc.pdf");

    // // Delta R
    // TCanvas* c_delta_R_pythia = new TCanvas("c_delta_R_pythia","c_delta_R_pythia");
    // TH2D* h_delta_R_pythia  = new TH2D("h_delta_R_pythia", "h_delta_R_pythia", 20,0.0,10.0,20,0.0,10.0);
    // TH2D* h_delta_R_genxicc = new TH2D("h_delta_R_genxicc","h_delta_R_genxicc",20,0.0,10.0,20,0.0,10.0);

    // pythia_tree->Draw("delta_R_jpsi_dx:delta_R_jpsi_bx>>h_delta_R_pythia",  cuts_pythia +cuts_all);
    // genxicc_cc_tree->Draw("delta_R_jpsi_dx:delta_R_jpsi_bx>>h_delta_R_genxicc",cuts_genxicc_cc+cuts_all);

    // h_delta_R_genxicc->Scale(scalefactor_genxicc);
    // h_delta_R_pythia->Scale(scalefactor_pythia);

    // h_delta_R_pythia->SetTitle(";#DeltaR(#Xi_{cc}, X_{c}) [rad]; #DeltaR(#Xi_{cc}, X_{c}) [rad]");
    // h_delta_R_pythia->Draw("colz");
    // c_delta_R_pythia->Print("plots_kinematics_Xicc_compare_productions/c_delta_R_pythia.pdf");
    
    // TCanvas* c_delta_R_genxicc = new TCanvas("c_delta_R_genxicc","c_delta_R_genxicc");
    // h_delta_R_genxicc->SetTitle(";#DeltaR(#Xi_{cc}, X_{c}) [rad]; #DeltaR(#Xi_{cc}, X_{c}) [rad]");
    // h_delta_R_genxicc->Draw("colz");
    // c_delta_R_genxicc->Print("plots_kinematics_Xicc_compare_productions/c_delta_R_genxicc.pdf");


    // // ================================
    // // All combinations 
    // // ================================

    // TH1D* h_1d_angle_jpsi_dx2_withMPI  = new TH1D("h_1d_angle_jpsi_dx2_withMPI", "h_1d_angle_jpsi_dx2_withMPI", 10,0,TMath::Pi());
    // TH1D* h_1d_angle_jpsi_dx2_noMPI    = new TH1D("h_1d_angle_jpsi_dx2_noMPI",   "h_1d_angle_jpsi_dx2_noMPI",   10,0,TMath::Pi());

    // h_1d_angle_jpsi_dx2_withMPI->SetLineColor(kRed);
    // h_1d_angle_jpsi_dx2_noMPI->SetLineColor(kGreen+2);
    // TH1D* h_1d_angle_jpsi_dx_withMPI  = new TH1D("h_1d_angle_jpsi_dx_withMPI", "h_1d_angle_jpsi_dx_withMPI", 10,0,TMath::Pi());
    // TH1D* h_1d_angle_jpsi_dx_noMPI    = new TH1D("h_1d_angle_jpsi_dx_noMPI",   "h_1d_angle_jpsi_dx_noMPI",   10,0,TMath::Pi());
    // h_1d_angle_jpsi_dx_withMPI->SetLineColor(kRed);
    // h_1d_angle_jpsi_dx_noMPI->SetLineColor(kGreen+2);

    // std::map<std::string,TCut> b_cuts;
    // std::map<std::string,TCut> c_cuts;

    // b_cuts["Hard_b_bb"] = "foundHardb&&hasbStatus23&&hasbStatus23_type==5";
    // b_cuts["Hard_b_bx"] = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5)";
    // b_cuts["MPI_b_bb"]  = "foundMPIb&&hasbStatus33&&hasbStatus33_type==5";
    // b_cuts["MPI_b_bx"]  = "foundMPIb&&hasbStatus33&&(hasbStatus33_type!=5)";
    // b_cuts["Shower_b"]  = "foundShowerb";


    // c_cuts["Hard_c_cc"] = "foundHardc&&hascStatus23&&hascStatus23_type==4";
    // c_cuts["Hard_c_cx"] = "foundHardc&&hascStatus23&&(hascStatus23_type!=4)";
    // c_cuts["MPI_c_cc"]  = "foundMPIc&&hascStatus33&&hascStatus33_type==4";
    // c_cuts["MPI_c_cx"]  = "foundMPIc&&hascStatus33&&(hascStatus33_type!=4)";
    // c_cuts["Shower_c"]  = "foundShowerc";

    // TH2D* h_2d_withMPI     = new TH2D("h_2d_withMPI",    "", 5,0,TMath::Pi(),5,0,TMath::Pi());
    // TH2D* h_2d_noMPI_pp    = new TH2D("h_2d_noMPI_pp",   "", 5,0,TMath::Pi(),5,0,TMath::Pi());
    // TH2D* h_2d_noMPI_fe    = new TH2D("h_2d_noMPI_fe",   "", 5,0,TMath::Pi(),5,0,TMath::Pi());

    // TH1D* h_1d_pT_withMPI     = new TH1D("h_1d_pT_withMPI",    "", 50,0,10);
    // TH1D* h_1d_pT_noMPI_pp    = new TH1D("h_1d_pT_noMPI_pp",   "", 50,0,10);
    // TH1D* h_1d_pT_noMPI_fe    = new TH1D("h_1d_pT_noMPI_fe",   "", 50,0,10);
    
    // for(const auto& b_cut: b_cuts){
    //     for(const auto& c_cut: c_cuts){
    //         std::string b_name = b_cut.first; 
    //         std::string c_name = c_cut.first; 


    //         TH2D* h_2d  = new TH2D(Form("h_2d_%s_%s",b_name.c_str(),c_name.c_str()), "", 10,0,TMath::Pi(),10,0,TMath::Pi());


    //         TCanvas* can_all = new TCanvas(Form("c_2d_%s_%s",b_name.c_str(),c_name.c_str()),
    //                                        Form("c_2d_%s_%s",b_name.c_str(),c_name.c_str()));

    //         // TCut temp_cuts = cuts_pythia+b_cut.second+c_cut.second+cuts_all;
    //         TCut temp_cuts = cuts_pythia+b_cut.second+c_cut.second;
    //         int n = pythia_tree->Draw(Form("angle_jpsi_dx:angle_jpsi_bx>>h_2d_%s_%s",b_name.c_str(),c_name.c_str()),temp_cuts,"colz");
    //         if( (b_name == "Hard_b_bb" && c_name == "Shower_c"  )||
    //             (b_name == "MPI_b_bb"  && c_name == "Shower_c"  )||
    //             (b_name == "Shower_b"  && c_name == "Hard_c_cc" )||
    //             (b_name == "Shower_b"  && c_name == "MPI_c_cc"  )||
    //             (b_name == "Shower_b"  && c_name == "Shower_c"  )
    //             ){
                
    //             pythia_tree->Draw("angle_jpsi_dx:angle_jpsi_bx>>+h_2d_noMPI_pp", temp_cuts,"colz");
    //             pythia_tree->Draw("jpsi_pt>>+h_1d_pT_noMPI_pp", temp_cuts,"colz");

    //         } else if( 
    //             (b_name == "Hard_b_bx" && c_name == "Shower_c"  )||
    //             (b_name == "MPI_b_bx"  && c_name == "Shower_c"  )||
    //             (b_name == "Shower_b"  && c_name == "Hard_c_cx" )||
    //             (b_name == "Shower_b"  && c_name == "MPI_c_cx"  )||
    //             (b_name == "Hard_b_bx" && c_name == "Hard_c_cx"  )
    //             ){
    //             pythia_tree->Draw("angle_jpsi_dx:angle_jpsi_bx>>+h_2d_noMPI_fe",temp_cuts,"colz");
    //             pythia_tree->Draw("jpsi_pt>>+h_1d_pT_noMPI_fe",temp_cuts,"colz");
    //         } else {
    //             pythia_tree->Draw("angle_jpsi_dx:angle_jpsi_bx>>+h_2d_withMPI",temp_cuts,"colz");
    //             pythia_tree->Draw("jpsi_pt>>+h_1d_pT_withMPI",temp_cuts,"colz");
    //         }

    //         std::cout << b_name <<"\t" << c_name << "\t" << n << std::endl;
    //         h_2d->SetMinimum(0.0);
    //         h_2d->Draw("colz");
    //         h_2d->SetTitle(";#Delta#phi(#Xi_{cc}^{+},X_{c});#Delta#phi(#Xi_{cc}^{+},X_{c})");
    //         can_all->Print(Form("plots_kinematics_Xicc_compare_productions/c_2d_%s_%s.pdf",b_name.c_str(),c_name.c_str()));

    //         // //

        
    //     }
    // }

    // TCanvas* c_2d_withMPI = new TCanvas("c_2d_withMPI","c_2d_withMPI");
    // h_2d_withMPI->SetMinimum(0.0);
    // h_2d_withMPI->Draw("colz");
    // h_2d_withMPI->SetTitle(";#Delta#phi(#Xi_{cc}^{+},X_{c});#Delta#phi(#Xi_{cc}^{+},X_{c})");
    // c_2d_withMPI->Print("plots_kinematics_Xicc_compare_productions/c_2d_withMPI.pdf");
    

    // TCanvas* c_2d_noMPI_pp = new TCanvas("c_2d_noMPI_pp","c_2d_noMPI_pp");
    // h_2d_noMPI_pp->SetMinimum(0.0);
    // h_2d_noMPI_pp->Draw("colz");
    // h_2d_noMPI_pp->SetTitle(";#Delta#phi(#Xi_{cc}^{+},X_{c});#Delta#phi(#Xi_{cc}^{+},X_{c})");
    // c_2d_noMPI_pp->Print("plots_kinematics_Xicc_compare_productions/c_2d_noMPI_pp.pdf");
    
    

    // TCanvas* c_2d_noMPI_fe = new TCanvas("c_2d_noMPI_fe","c_2d_noMPI_fe");
    // h_2d_noMPI_fe->SetMinimum(0.0);
    // h_2d_noMPI_fe->Draw("colz");
    // h_2d_noMPI_fe->SetTitle(";#Delta#phi(#Xi_{cc}^{+},X_{c});#Delta#phi(#Xi_{cc}^{+},X_{c})");
    // c_2d_noMPI_fe->Print("plots_kinematics_Xicc_compare_productions/c_2d_noMPI_fe.pdf");



    // TCanvas* c_1d_pT_withMPI = new TCanvas("c_1d_pT_withMPI","c_1d_pT_withMPI");
    // h_1d_pT_withMPI->SetMinimum(0.0);
    // h_1d_pT_withMPI->Draw("colz");
    // h_1d_pT_withMPI->SetTitle(";#it{p}_{T} [GeV/c];Entries");
    // c_1d_pT_withMPI->Print("plots_kinematics_Xicc_compare_productions/c_1d_pT_withMPI.pdf");
    

    // TCanvas* c_1d_pT_noMPI_pp = new TCanvas("c_1d_pT_noMPI_pp","c_1d_pT_noMPI_pp");
    // h_1d_pT_noMPI_pp->SetMinimum(0.0);
    // h_1d_pT_noMPI_pp->Draw("colz");
    // h_1d_pT_noMPI_pp->SetTitle(";#it{p}_{T} [GeV/c];Entries");
    // c_1d_pT_noMPI_pp->Print("plots_kinematics_Xicc_compare_productions/c_1d_pT_noMPI_pp.pdf");
    
    

    // TCanvas* c_1d_pT_noMPI_fe = new TCanvas("c_1d_pT_noMPI_fe","c_1d_pT_noMPI_fe");
    // h_1d_pT_noMPI_fe->SetMinimum(0.0);
    // h_1d_pT_noMPI_fe->Draw("colz");
    // h_1d_pT_noMPI_fe->SetTitle(";#it{p}_{T} [GeV/c];Entries");
    // c_1d_pT_noMPI_fe->Print("plots_kinematics_Xicc_compare_productions/c_1d_pT_noMPI_fe.pdf");

}