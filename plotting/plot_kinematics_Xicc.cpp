#include "tools.h"

void plot_kinematics_Xicc(){

    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;



    TCut cuts_pythia  = "nXiccpp==1&&ncquark+ncquarkbar==4&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0";

    // std::string pythia_name = "../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_400000.root";
    std::string pythia_name = "../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_9900000_PartMod1.root";
    

    std::string genxicc_name = "../output/main202output_GenXicc_N_9000_gg.root";
    TCut cuts_genxicc = "nXiccpp==1&&ncquark+ncquarkbar==2&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0";    

    // std::string genxicc_name = "../output/main202output_GenXicc_N_9000_gc.root";
    // TCut cuts_genxicc = "nXiccpp==1&&ncquark+ncquarkbar==3&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0";   

    // std::string genxicc_name = "../output/main202output_GenXicc_N_9000_cc.root";
    // TCut cuts_genxicc = "nXiccpp==1&&ncquark+ncquarkbar==4&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0";

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open(pythia_name.c_str());
    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_400000.root");
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");


    pythia_tree->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    pythia_tree->SetAlias("angle_jpsi_dx2","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");


    pythia_tree->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    pythia_tree->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    pythia_tree->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    pythia_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_jpsi_bx","jpsi_eta-dx2_eta");

    pythia_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    pythia_tree->SetAlias("delta_y_jpsi_bx","jpsi_y-dx2_y");

    pythia_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    pythia_tree->SetAlias("delta_R_jpsi_bx","sqrt(delta_eta_jpsi_bx*delta_eta_jpsi_bx+angle_jpsi_dx2*angle_jpsi_dx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get GenXicc simulation
    // -------------------------------------------------------------------------

    TFile* genxicc_file = TFile::Open(genxicc_name.c_str());
    TTree* genxicc_tree = (TTree*) genxicc_file->Get("events");


    double sigmaGen_genxicc;
    double sigmaErr_genxicc;
    genxicc_tree->SetBranchAddress("sigmaGen",&sigmaGen_genxicc);
    genxicc_tree->SetBranchAddress("sigmaErr",&sigmaErr_genxicc);
    

    genxicc_tree->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    genxicc_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    genxicc_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    genxicc_tree->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    genxicc_tree->SetAlias("angle_jpsi_dx2","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");

    genxicc_tree->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    genxicc_tree->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    genxicc_tree->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    genxicc_tree->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    genxicc_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    genxicc_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    genxicc_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    genxicc_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    genxicc_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    genxicc_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    genxicc_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    genxicc_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    genxicc_tree->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    genxicc_tree->SetAlias("delta_eta_jpsi_bx","jpsi_eta-dx2_eta");

    genxicc_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    genxicc_tree->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    genxicc_tree->SetAlias("delta_y_jpsi_bx","jpsi_y-dx2_y");



    genxicc_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    genxicc_tree->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    genxicc_tree->SetAlias("delta_R_jpsi_bx","sqrt(delta_eta_jpsi_bx*delta_eta_jpsi_bx+angle_jpsi_dx2*angle_jpsi_dx2)");
    
    int n_entries_genxicc = genxicc_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    genxicc_tree->GetEntry(genxicc_tree->GetEntries()-1);

    
    double withtarget_pythia  = pythia_tree->GetEntries("nXiccpp>0");
    double withtarget_genxicc = genxicc_tree->GetEntries("nXiccpp>0");

    double corrected_sigmaGen_genxicc = sigmaGen_genxicc * (withtarget_genxicc/n_entries_genxicc);
    double corrected_sigmaGen_pythia  = sigmaGen_pythia  * (withtarget_pythia/n_entries_pythia);
    double corrected_sigmaErr_genxicc = sigmaErr_genxicc * (withtarget_genxicc/n_entries_genxicc);
    double corrected_sigmaErr_pythia  = sigmaErr_pythia  * (withtarget_pythia/n_entries_pythia);


    std::cout << "Cross section GenXicc: " << sigmaGen_genxicc*microbarn << " +/- " << sigmaErr_genxicc*microbarn << " microbarns"; 
    std::cout << "\t" << genxicc_tree->GetEntries("nXiccpp>0") << "/" << genxicc_tree->GetEntries("")<<std::endl;
    
    std::cout << "Cross section Pythia:  " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nXiccpp>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;


    std::cout << "Corrected Cross section GenXicc: " << corrected_sigmaGen_genxicc*microbarn << " +/- " << corrected_sigmaErr_genxicc*microbarn << " microbarns"<<std::endl;
    std::cout << "Corrected Cross section Pythia:  " << corrected_sigmaGen_pythia*microbarn  << " +/- " << corrected_sigmaErr_pythia*microbarn << " microbarns"<< std::endl;
    

    double scalefactor_genxicc = 1e-2*corrected_sigmaGen_genxicc*microbarn/withtarget_genxicc;
    double scalefactor_pythia  =      corrected_sigmaGen_pythia*microbarn/withtarget_pythia;
    
    std::cout << "scalefactor_genxicc " << scalefactor_genxicc<<std::endl;
    std::cout << "scalefactor_pythia " << scalefactor_pythia<<std::endl;

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    
    // TCut cuts_all = "dx2_pt>2 && abs(jpsi_eta)>2 && abs(jpsi_eta)<5";
    TCut cuts_all = "";
    

    // -------------------------------------------------------------------------
    // Plot 1D kinematics integrated over all eta 
    // -------------------------------------------------------------------------
    // TCut temp_cut = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5) && foundHardc&&hascStatus23&&(hascStatus23_type!=4)";
    // pT
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia  = new TH1D("h_pt_pythia", "h_pt_pythia", 50,0,20);
    TH1D* h_pt_genxicc = new TH1D("h_pt_genxicc","h_pt_genxicc",50,0,20);

    pythia_tree->Draw("jpsi_pt>>h_pt_pythia",cuts_pythia);
    genxicc_tree->Draw("jpsi_pt>>h_pt_genxicc",cuts_genxicc);

    h_pt_genxicc->Scale(scalefactor_genxicc);
    h_pt_pythia->Scale(scalefactor_pythia);

    h_pt_pythia->SetLineColor(kRed);
    h_pt_genxicc->SetLineColor(kGreen+2);
    h_pt_pythia->SetMaximum(1.05*max(h_pt_pythia->GetMaximum(),h_pt_genxicc->GetMaximum()));

    h_pt_pythia->SetTitle(";#it{p}_{T} [GeV/c^{2}]; d#sigma/d#it{p}_{T} [2.5 GeV^{-1} c^{2}]");
    h_pt_pythia->Draw("hist");
    h_pt_genxicc->Draw("same hist");

    TLegend* leg_pt = new TLegend(0.7,0.7,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,"Pythia","l");
    leg_pt->AddEntry(h_pt_genxicc,"GenXicc 2.1","l");
    leg_pt->SetTextFont(132);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics_Xicc/c_pt.pdf");


    // eta
    TCanvas* c_eta = new TCanvas("c_eta","c_eta");
    TH1D* h_eta_pythia  = new TH1D("h_eta_pythia", "h_eta_pythia", 50,-10,10);
    TH1D* h_eta_genxicc = new TH1D("h_eta_genxicc","h_eta_genxicc",50,-10,10);

    pythia_tree->Draw("jpsi_eta>>h_eta_pythia",cuts_pythia);
    genxicc_tree->Draw("jpsi_eta>>h_eta_genxicc",cuts_genxicc);

    h_eta_genxicc->Scale(scalefactor_genxicc);
    h_eta_pythia->Scale(scalefactor_pythia);

    h_eta_pythia->SetLineColor(kRed);
    h_eta_genxicc->SetLineColor(kGreen+2);
    h_eta_pythia->SetMaximum(1.05*max(h_eta_pythia->GetMaximum(),h_eta_genxicc->GetMaximum()));

    h_eta_pythia->SetTitle(";#eta ; d#sigma/d#eta [2.5]");
    h_eta_pythia->Draw("hist");
    h_eta_genxicc->Draw("same hist");


    TLegend* leg_eta = new TLegend(0.7,0.7,0.9,0.9);
    leg_eta->AddEntry(h_eta_pythia,"Pythia","l");
    leg_eta->AddEntry(h_eta_genxicc,"GenXicc 2.1","l");
    leg_eta->SetTextFont(132);
    leg_eta->Draw();
    c_eta->Print("plots_kinematics_Xicc/c_eta.pdf");

    // Rapidity
    TCanvas* c_y = new TCanvas("c_y","c_y");
    TH1D* h_y_pythia  = new TH1D("h_y_pythia", "h_y_pythia", 50,-10,10);
    TH1D* h_y_genxicc = new TH1D("h_y_genxicc","h_y_genxicc",50,-10,10);

    pythia_tree->Draw("jpsi_y>>h_y_pythia",cuts_pythia);
    genxicc_tree->Draw("jpsi_y>>h_y_genxicc",cuts_genxicc);

    h_y_genxicc->Scale(scalefactor_genxicc);
    h_y_pythia->Scale(scalefactor_pythia);

    h_y_pythia->SetLineColor(kRed);
    h_y_genxicc->SetLineColor(kGreen+2);
    h_y_pythia->SetMaximum(1.05*max(h_y_pythia->GetMaximum(),h_y_genxicc->GetMaximum()));

    h_y_pythia->SetTitle(";#it{y} ; d#sigma/dy [2.5]");
    h_y_pythia->Draw("hist");
    h_y_genxicc->Draw("same hist");


    TLegend* leg_y = new TLegend(0.7,0.7,0.9,0.9);
    leg_y->AddEntry(h_y_pythia,"Pythia","l");
    leg_y->AddEntry(h_y_genxicc,"GenXicc 2.1","l");
    leg_y->SetTextFont(132);
    leg_y->Draw();

    c_y->Print("plots_kinematics_Xicc/c_y.pdf");

    // -------------------------------------------------------------------------
    // Plot 2D pT vs. eta 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_eta_pythia = new TCanvas("c_2D_pt_eta_pythia","c_2D_pt_eta_pythia");
    TH2D* h_2D_pt_eta_pythia   = new TH2D("h_2D_pt_eta_pythia",  "h_2D_pt_eta_pythia",  50,-10,10,50,0,20);
    TH2D* h_2D_pt_eta_genxicc  = new TH2D("h_2D_pt_eta_genxicc", "h_2D_pt_eta_genxicc", 50,-10,10,50,0,20);

    pythia_tree->Draw("jpsi_pt:jpsi_eta>>h_2D_pt_eta_pythia",cuts_pythia);
    genxicc_tree->Draw("jpsi_pt:jpsi_eta>>h_2D_pt_eta_genxicc",cuts_genxicc);

    h_2D_pt_eta_genxicc->Scale(scalefactor_genxicc);
    h_2D_pt_eta_pythia->Scale(scalefactor_pythia);


    h_2D_pt_eta_pythia->SetTitle(";#it{p}_{T} [GeV/c^{2}]; #eta");
    h_2D_pt_eta_pythia->Draw("colz");
    
    c_2D_pt_eta_pythia->Print("plots_kinematics_Xicc/c_2D_pt_eta_pythia.pdf");
    

    TCanvas* c_2D_pt_eta_genxicc = new TCanvas("c_2D_pt_eta_genxicc","c_2D_pt_eta_genxicc");

    h_2D_pt_eta_genxicc->SetTitle(";#it{p}_{T} [GeV/c^{2}]; #eta");
    h_2D_pt_eta_genxicc->Draw("colz");
    
    c_2D_pt_eta_genxicc->Print("plots_kinematics_Xicc/c_2D_pt_eta_genxicc.pdf");


    // -------------------------------------------------------------------------
    // Plot 2D pT vs. y 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_y_pythia = new TCanvas("c_2D_pt_y_pythia","c_2D_pt_y_pythia");
    TH2D* h_2D_pt_y_pythia   = new TH2D("h_2D_pt_y_pythia",  "h_2D_pt_y_pythia",  50,-10,10,50,0,10);
    TH2D* h_2D_pt_y_genxicc  = new TH2D("h_2D_pt_y_genxicc", "h_2D_pt_y_genxicc", 50,-10,10,50,0,10);

    pythia_tree->Draw("jpsi_pt:jpsi_y>>h_2D_pt_y_pythia",cuts_pythia);
    genxicc_tree->Draw("jpsi_pt:jpsi_y>>h_2D_pt_y_genxicc",cuts_genxicc);

    h_2D_pt_y_genxicc->Scale(scalefactor_genxicc);
    h_2D_pt_y_pythia->Scale(scalefactor_pythia);


    h_2D_pt_y_pythia->SetTitle(";#it{p}_{T} [GeV/c^{2}]; #it{y}");
    h_2D_pt_y_pythia->Draw("colz");
    
    c_2D_pt_y_pythia->Print("plots_kinematics_Xicc/c_2D_pt_y_pythia.pdf");
    

    TCanvas* c_2D_pt_y_genxicc = new TCanvas("c_2D_pt_y_genxicc","c_2D_pt_y_genxicc");

    h_2D_pt_y_genxicc->SetTitle(";#it{p}_{T} [GeV/c^{2}]; #it{y}");
    h_2D_pt_y_genxicc->Draw("colz");
    
    c_2D_pt_y_genxicc->Print("plots_kinematics_Xicc/c_2D_pt_y_genxicc.pdf");

    // -------------------------------------------------------------------------
    // Plot 1D angles integrated over all eta 
    // -------------------------------------------------------------------------
    SetLHCbStyle("oneD");

    // angles
    TCanvas* c_delta_phi_jpsi_bx = new TCanvas("c_delta_phi_jpsi_bx","c_delta_phi_jpsi_bx");
    TH1D* h_delta_phi_jpsi_dx2_pythia  = new TH1D("h_delta_phi_jpsi_dx2_pythia", "h_delta_phi_jpsi_dx2_pythia", 50,0,TMath::Pi());
    TH1D* h_delta_phi_jpsi_dx2_genxicc = new TH1D("h_delta_phi_jpsi_dx2_genxicc","h_delta_phi_jpsi_dx2_genxicc",50,0,TMath::Pi());

    pythia_tree->Draw("angle_jpsi_dx2>>h_delta_phi_jpsi_dx2_pythia",  cuts_pythia +cuts_all);
    genxicc_tree->Draw("angle_jpsi_dx2>>h_delta_phi_jpsi_dx2_genxicc",cuts_genxicc+cuts_all);

    h_delta_phi_jpsi_dx2_genxicc->Scale(scalefactor_genxicc);
    h_delta_phi_jpsi_dx2_pythia->Scale(scalefactor_pythia);

    h_delta_phi_jpsi_dx2_pythia->SetLineColor(kRed);
    h_delta_phi_jpsi_dx2_genxicc->SetLineColor(kGreen+2);

    h_delta_phi_jpsi_dx2_genxicc->SetTitle(";#Delta#phi(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]; d#sigma/d#Delta#phi");
    h_delta_phi_jpsi_dx2_genxicc->SetMinimum(0.0);
    

    h_delta_phi_jpsi_dx2_genxicc->Draw("hist");
    h_delta_phi_jpsi_dx2_pythia->Draw("hist same");
    


    TLegend* leg_delta_phi_jpsi_bx = new TLegend(0.7,0.2,0.9,0.4);
    leg_delta_phi_jpsi_bx->AddEntry(h_delta_phi_jpsi_dx2_pythia,"Pythia","l");
    leg_delta_phi_jpsi_bx->AddEntry(h_delta_phi_jpsi_dx2_genxicc,"GenXicc 2.1","l");
    leg_delta_phi_jpsi_bx->SetTextFont(132);
    leg_delta_phi_jpsi_bx->Draw();

    c_delta_phi_jpsi_bx->Print("plots_kinematics_Xicc/c_delta_phi_jpsi_bx.pdf");


    // angles
    TCanvas* c_delta_phi_jpsi_dx = new TCanvas("c_delta_phi_jpsi_dx","c_delta_phi_jpsi_dx");
    TH1D* h_delta_phi_jpsi_dx_pythia  = new TH1D("h_delta_phi_jpsi_dx_pythia", "h_delta_phi_jpsi_dx_pythia", 50,0,TMath::Pi());
    TH1D* h_delta_phi_jpsi_dx_genxicc = new TH1D("h_delta_phi_jpsi_dx_genxicc","h_delta_phi_jpsi_dx_genxicc",50,0,TMath::Pi());

    pythia_tree->Draw("angle_jpsi_dx>>h_delta_phi_jpsi_dx_pythia",  cuts_pythia +cuts_all);
    genxicc_tree->Draw("angle_jpsi_dx>>h_delta_phi_jpsi_dx_genxicc",cuts_genxicc+cuts_all);

    h_delta_phi_jpsi_dx_genxicc->Scale(scalefactor_genxicc);
    h_delta_phi_jpsi_dx_pythia->Scale(scalefactor_pythia);

    h_delta_phi_jpsi_dx_pythia->SetLineColor(kRed);
    h_delta_phi_jpsi_dx_genxicc->SetLineColor(kGreen+2);

    h_delta_phi_jpsi_dx_pythia->SetTitle(";#Delta#phi(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]; d#sigma/d#Delta#phi");
    h_delta_phi_jpsi_dx_pythia->SetMinimum(0.0);
    h_delta_phi_jpsi_dx_pythia->Draw("hist");
    h_delta_phi_jpsi_dx_genxicc->Draw("hist same");


    TLegend* leg_delta_phi_jpsi_dx = new TLegend(0.7,0.2,0.9,0.4);
    leg_delta_phi_jpsi_dx->AddEntry(h_delta_phi_jpsi_dx_pythia,"Pythia","l");
    leg_delta_phi_jpsi_dx->AddEntry(h_delta_phi_jpsi_dx_genxicc,"GenXicc 2.1","l");
    leg_delta_phi_jpsi_dx->SetTextFont(132);
    leg_delta_phi_jpsi_dx->Draw();
    c_delta_phi_jpsi_dx->Print("plots_kinematics_Xicc/c_delta_phi_jpsi_dx.pdf");

    // -------------------------------------------------------------------------
    // Plot 2D angles integrated over all eta 
    // -------------------------------------------------------------------------


    SetLHCbStyle("cont");

    // angles
    TCanvas* c_delta_phi_pythia = new TCanvas("c_delta_phi_pythia","c_delta_phi_pythia");
    TH2D* h_delta_phi_pythia  = new TH2D("h_delta_phi_pythia", "h_delta_phi_pythia", 20,0,TMath::Pi(),20,0,TMath::Pi());
    TH2D* h_delta_phi_genxicc = new TH2D("h_delta_phi_genxicc","h_delta_phi_genxicc",20,0,TMath::Pi(),20,0,TMath::Pi());

    pythia_tree->Draw("angle_jpsi_dx:angle_jpsi_dx2>>h_delta_phi_pythia",  cuts_pythia +cuts_all);
    genxicc_tree->Draw("angle_jpsi_dx:angle_jpsi_dx2>>h_delta_phi_genxicc",cuts_genxicc+cuts_all);

    h_delta_phi_genxicc->Scale(scalefactor_genxicc);
    h_delta_phi_pythia->Scale(scalefactor_pythia);

    h_delta_phi_pythia->SetTitle(";#Delta#phi(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]; #Delta#phi(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]");
    h_delta_phi_pythia->Draw("colz");
    c_delta_phi_pythia->Print("plots_kinematics_Xicc/c_delta_phi_pythia.pdf");
    
    TCanvas* c_delta_phi_genxicc = new TCanvas("c_delta_phi_genxicc","c_delta_phi_genxicc");
    h_delta_phi_genxicc->SetTitle(";#Delta#phi(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]; #Delta#phi(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]");
    h_delta_phi_genxicc->Draw("colz");

    c_delta_phi_genxicc->Print("plots_kinematics_Xicc/c_delta_phi_genxicc.pdf");

    // eta
    TCanvas* c_delta_eta_pythia = new TCanvas("c_delta_eta_pythia","c_delta_eta_pythia");
    TH2D* h_delta_eta_pythia  = new TH2D("h_delta_eta_pythia", "h_delta_eta_pythia", 20,-5.0,5.0,20,-5.0,5.0);
    TH2D* h_delta_eta_genxicc = new TH2D("h_delta_eta_genxicc","h_delta_eta_genxicc",20,-5.0,5.0,20,-5.0,5.0);

    pythia_tree->Draw("delta_eta_jpsi_dx:delta_eta_jpsi_bx>>h_delta_eta_pythia",  cuts_pythia +cuts_all);
    genxicc_tree->Draw("delta_eta_jpsi_dx:delta_eta_jpsi_bx>>h_delta_eta_genxicc",cuts_genxicc+cuts_all);

    h_delta_eta_genxicc->Scale(scalefactor_genxicc);
    h_delta_eta_pythia->Scale(scalefactor_pythia);

    h_delta_eta_pythia->SetTitle(";#Delta#eta(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]; #Delta#eta(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]");
    h_delta_eta_pythia->Draw("colz");
    c_delta_eta_pythia->Print("plots_kinematics_Xicc/c_delta_eta_pythia.pdf");
    
    TCanvas* c_delta_eta_genxicc = new TCanvas("c_delta_eta_genxicc","c_delta_eta_genxicc");
    h_delta_eta_genxicc->SetTitle(";#Delta#eta(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]; #Delta#eta(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]");
    h_delta_eta_genxicc->Draw("colz");
    c_delta_eta_genxicc->Print("plots_kinematics_Xicc/c_delta_eta_genxicc.pdf");

    // Delta R
    TCanvas* c_delta_R_pythia = new TCanvas("c_delta_R_pythia","c_delta_R_pythia");
    TH2D* h_delta_R_pythia  = new TH2D("h_delta_R_pythia", "h_delta_R_pythia", 20,0.0,10.0,20,0.0,10.0);
    TH2D* h_delta_R_genxicc = new TH2D("h_delta_R_genxicc","h_delta_R_genxicc",20,0.0,10.0,20,0.0,10.0);

    pythia_tree->Draw("delta_R_jpsi_dx:delta_R_jpsi_bx>>h_delta_R_pythia",  cuts_pythia +cuts_all);
    genxicc_tree->Draw("delta_R_jpsi_dx:delta_R_jpsi_bx>>h_delta_R_genxicc",cuts_genxicc+cuts_all);

    h_delta_R_genxicc->Scale(scalefactor_genxicc);
    h_delta_R_pythia->Scale(scalefactor_pythia);

    h_delta_R_pythia->SetTitle(";#DeltaR(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]; #DeltaR(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]");
    h_delta_R_pythia->Draw("colz");
    c_delta_R_pythia->Print("plots_kinematics_Xicc/c_delta_R_pythia.pdf");
    
    TCanvas* c_delta_R_genxicc = new TCanvas("c_delta_R_genxicc","c_delta_R_genxicc");
    h_delta_R_genxicc->SetTitle(";#DeltaR(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]; #DeltaR(#Xi_{cc}^{++}, X_{#bar{c}}) [rad]");
    h_delta_R_genxicc->Draw("colz");
    c_delta_R_genxicc->Print("plots_kinematics_Xicc/c_delta_R_genxicc.pdf");


    // ================================
    // All combinations 
    // ================================

    TH1D* h_1d_angle_jpsi_dx2_withMPI  = new TH1D("h_1d_angle_jpsi_dx2_withMPI", "h_1d_angle_jpsi_dx2_withMPI", 10,0,TMath::Pi());
    TH1D* h_1d_angle_jpsi_dx2_noMPI    = new TH1D("h_1d_angle_jpsi_dx2_noMPI",   "h_1d_angle_jpsi_dx2_noMPI",   10,0,TMath::Pi());

    h_1d_angle_jpsi_dx2_withMPI->SetLineColor(kRed);
    h_1d_angle_jpsi_dx2_noMPI->SetLineColor(kGreen+2);
    TH1D* h_1d_angle_jpsi_dx_withMPI  = new TH1D("h_1d_angle_jpsi_dx_withMPI", "h_1d_angle_jpsi_dx_withMPI", 10,0,TMath::Pi());
    TH1D* h_1d_angle_jpsi_dx_noMPI    = new TH1D("h_1d_angle_jpsi_dx_noMPI",   "h_1d_angle_jpsi_dx_noMPI",   10,0,TMath::Pi());
    h_1d_angle_jpsi_dx_withMPI->SetLineColor(kRed);
    h_1d_angle_jpsi_dx_noMPI->SetLineColor(kGreen+2);


    std::map<std::string,TCut> cc_cuts;
    
    cc_cuts["Hard_c_cx_Hard_c_cx"] = "foundHardc&&hascStatus23&&(hascStatus23_type==4)";
    
    cc_cuts["Hard_c_cc_MPI_c_cc"] = "hascStatus23&&hascStatus23_type==4&&hascStatus33&&hascStatus33_N==1&&hascStatus33_type2==4";
    cc_cuts["Hard_c_cc_MPI_c_cx"] = "hascStatus23&&hascStatus23_type==4&&hascStatus33&&hascStatus33_N==1&&hascStatus33_type2!=4";
    cc_cuts["Hard_c_cc_Shower_c"] = "hascStatus23&&hascStatus23_type==4&&!hascStatus33";
    
    cc_cuts["Hard_c_cx_MPI_c_cc"] = "hascStatus23&&hascStatus23_type!=4&&hascStatus33&&hascStatus33_N==1&&hascStatus33_type2==4";
    cc_cuts["Hard_c_cx_MPI_c_cx"] = "hascStatus23&&hascStatus23_type!=4&&hascStatus33&&hascStatus33_N==1&&hascStatus33_type2!=4";
    cc_cuts["Hard_c_cx_Shower_c"] = "hascStatus23&&hascStatus23_type!=4&&!hascStatus33";

    cc_cuts["MPI_c_cc_MPI_c_cc"] = "!hascStatus23&&hascStatus33&&hascStatus33_N==2&&hascStatus33_type==4&&hascStatus33_type2==4";
    cc_cuts["MPI_c_cc_MPI_c_cx"] = "!hascStatus23&&hascStatus33&&hascStatus33_N==2&&((hascStatus33_type==4&&hascStatus33_type2!=4)||(hascStatus33_type!=4&&hascStatus33_type2==4))";
    cc_cuts["MPI_c_cc_Shower_c"] = "!hascStatus23&&hascStatus33&&hascStatus33_N==1&&hascStatus33_type==4";

    cc_cuts["MPI_c_cx_MPI_c_cx"] = "!hascStatus23&&hascStatus33&&hascStatus33_N==2&&hascStatus33_type!=4&&hascStatus33_type2!=4";
    cc_cuts["MPI_c_cx_Shower_c"] = "!hascStatus23&&hascStatus33&&hascStatus33_N==1&&hascStatus33_type!=4";

    cc_cuts["Shower_c_Shower_c"] = "!hascStatus23&&!hascStatus33";



    TH2D* h_2d_withMPI     = new TH2D("h_2d_withMPI",    "", 5,0,TMath::Pi(),5,0,TMath::Pi());
    TH2D* h_2d_noMPI_pp    = new TH2D("h_2d_noMPI_pp",   "", 5,0,TMath::Pi(),5,0,TMath::Pi());
    TH2D* h_2d_noMPI_fe    = new TH2D("h_2d_noMPI_fe",   "", 5,0,TMath::Pi(),5,0,TMath::Pi());

    TH1D* h_1d_pT_withMPI     = new TH1D("h_1d_pT_withMPI",    "", 50,0,20);
    TH1D* h_1d_pT_noMPI_pp    = new TH1D("h_1d_pT_noMPI_pp",   "", 50,0,20);
    TH1D* h_1d_pT_noMPI_fe    = new TH1D("h_1d_pT_noMPI_fe",   "", 50,0,20);
    
    for(const auto& cc_cut: cc_cuts){ 
        std::string cc_name = cc_cut.first; 


        TH2D* h_2d  = new TH2D(Form("h_2d_%s",cc_name.c_str()), "", 10,0,TMath::Pi(),10,0,TMath::Pi());


        TCanvas* can_all = new TCanvas(Form("c_2d_%s",cc_name.c_str()),
                                       Form("c_2d_%s",cc_name.c_str()));

        TCut temp_cuts = cuts_pythia+cc_cut.second;
        int n = pythia_tree->Draw(Form("angle_jpsi_dx:angle_jpsi_dx2>>h_2d_%s",cc_name.c_str()),temp_cuts,"colz");
        if( ( cc_name == "Hard_c_cc_Shower_c"  )||
            ( cc_name == "MPI_c_cc_Shower_c"   )||
            ( cc_name == "Shower_c_Shower_c"   )
            ){
            
            pythia_tree->Draw("angle_jpsi_dx:angle_jpsi_dx2>>+h_2d_noMPI_pp", temp_cuts,"colz");
            pythia_tree->Draw("jpsi_pt>>+h_1d_pT_noMPI_pp", temp_cuts,"colz");

        } else if( 
            ( cc_name == "Hard_c_cx_Shower_c"  )||
            ( cc_name == "MPI_c_cx_Shower_c"   )||
            ( cc_name == "Hard_c_cx_Hard_c_cx" )
            ){
            pythia_tree->Draw("angle_jpsi_dx:angle_jpsi_dx2>>+h_2d_noMPI_fe",temp_cuts,"colz");
            pythia_tree->Draw("jpsi_pt>>+h_1d_pT_noMPI_fe",temp_cuts,"colz");
        } else if(

            ( cc_name == "Hard_c_cc_MPI_c_cx"  )||
            ( cc_name == "Hard_c_cc_MPI_c_cx"   )||
            ( cc_name == "Hard_c_cx_MPI_c_cc"   )||
            ( cc_name == "Hard_c_cx_MPI_c_cx"   )||
            ( cc_name == "MPI_c_cc_MPI_c_cc"   )||
            ( cc_name == "MPI_c_cc_MPI_c_cx"   )||
            ( cc_name == "MPI_c_cx_MPI_c_cx" )

            ){
            pythia_tree->Draw("angle_jpsi_dx:angle_jpsi_dx2>>+h_2d_withMPI",temp_cuts,"colz");
            pythia_tree->Draw("jpsi_pt>>+h_1d_pT_withMPI",temp_cuts,"colz");
        } else {
            std::cout << "Missing category " << cc_name << std::endl;
        }

        std::cout <<"\t" << cc_name << "\t" << n << std::endl;
        h_2d->SetMinimum(0.0);
        h_2d->Draw("colz");
        h_2d->SetTitle(";#Delta#phi(#Xi_{cc}^{+},X_{#bar{c}});#Delta#phi(#Xi_{cc}^{+},X_{#bar{c}})");
        can_all->Print(Form("plots_kinematics_Xicc/c_2d_%s.pdf",cc_name.c_str()));

        // //

    
    }

    TCanvas* c_2d_withMPI = new TCanvas("c_2d_withMPI","c_2d_withMPI");
    h_2d_withMPI->SetMinimum(0.0);
    h_2d_withMPI->Draw("colz");
    h_2d_withMPI->SetTitle(";#Delta#phi(#Xi_{cc}^{+},X_{#bar{c}});#Delta#phi(#Xi_{cc}^{+},X_{#bar{c}})");
    c_2d_withMPI->Print("plots_kinematics_Xicc/c_2d_withMPI.pdf");
    

    TCanvas* c_2d_noMPI_pp = new TCanvas("c_2d_noMPI_pp","c_2d_noMPI_pp");
    h_2d_noMPI_pp->SetMinimum(0.0);
    h_2d_noMPI_pp->Draw("colz");
    h_2d_noMPI_pp->SetTitle(";#Delta#phi(#Xi_{cc}^{+},X_{#bar{c}});#Delta#phi(#Xi_{cc}^{+},X_{#bar{c}})");
    c_2d_noMPI_pp->Print("plots_kinematics_Xicc/c_2d_noMPI_pp.pdf");
    
    

    TCanvas* c_2d_noMPI_fe = new TCanvas("c_2d_noMPI_fe","c_2d_noMPI_fe");
    h_2d_noMPI_fe->SetMinimum(0.0);
    h_2d_noMPI_fe->Draw("colz");
    h_2d_noMPI_fe->SetTitle(";#Delta#phi(#Xi_{cc}^{+},X_{#bar{c}});#Delta#phi(#Xi_{cc}^{+},X_{#bar{c}})");
    c_2d_noMPI_fe->Print("plots_kinematics_Xicc/c_2d_noMPI_fe.pdf");



    TCanvas* c_1d_pT_withMPI = new TCanvas("c_1d_pT_withMPI","c_1d_pT_withMPI");
    h_1d_pT_withMPI->SetMinimum(0.0);
    h_1d_pT_withMPI->Draw("colz");
    h_1d_pT_withMPI->SetTitle(";#it{p}_{T} [GeV/c^{2}];Entries");
    c_1d_pT_withMPI->Print("plots_kinematics_Xicc/c_1d_pT_withMPI.pdf");
    

    TCanvas* c_1d_pT_noMPI_pp = new TCanvas("c_1d_pT_noMPI_pp","c_1d_pT_noMPI_pp");
    h_1d_pT_noMPI_pp->SetMinimum(0.0);
    h_1d_pT_noMPI_pp->Draw("colz");
    h_1d_pT_noMPI_pp->SetTitle(";#it{p}_{T} [GeV/c^{2}];Entries");
    c_1d_pT_noMPI_pp->Print("plots_kinematics_Xicc/c_1d_pT_noMPI_pp.pdf");
    
    

    TCanvas* c_1d_pT_noMPI_fe = new TCanvas("c_1d_pT_noMPI_fe","c_1d_pT_noMPI_fe");
    h_1d_pT_noMPI_fe->SetMinimum(0.0);
    h_1d_pT_noMPI_fe->Draw("colz");
    h_1d_pT_noMPI_fe->SetTitle(";#it{p}_{T} [GeV/c^{2}];Entries");
    c_1d_pT_noMPI_fe->Print("plots_kinematics_Xicc/c_1d_pT_noMPI_fe.pdf");

}