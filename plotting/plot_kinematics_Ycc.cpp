#include "tools.h"

void plot_kinematics_Ycc(){

    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bbcc_N_1000000.root");
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");


    pythia_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_dx2","acos((upsilon1s_px*dx2_px + upsilon1s_py*dx2_py)/(upsilon1s_pt*dx2_pt))");


    pythia_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_dx2","upsilon1s_eta-dx2_eta");

    pythia_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_dx2","upsilon1s_y-dx2_y");

    pythia_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_dx2","sqrt(delta_eta_upsilon1s_dx2*delta_eta_upsilon1s_dx2+angle_upsilon1s_dx2*angle_upsilon1s_dx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);

    
    std::cout << "Cross section Pythia:  " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nXiccpp>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
    
    double withtarget_pythia = pythia_tree->GetEntries("nUpsilon1S>0");

    double scalefactor_pythia  =      sigmaGen_pythia*microbarn/n_entries_pythia * (withtarget_pythia/n_entries_pythia);
    
    std::cout << "scalefactor_pythia " << scalefactor_pythia<<std::endl;

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    TCut cuts_pythia  = "nUpsilon1S==1&&nXc==2";

    // TCut cuts_all = "dx2_pt>2 && abs(upsilon1s_eta)>2 && abs(upsilon1s_eta)<5";
    TCut cuts_all = "";
    

    // -------------------------------------------------------------------------
    // Plot 1D kinematics integrated over all eta 
    // -------------------------------------------------------------------------
    // TCut temp_cut = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5) && foundHardc&&hascStatus23&&(hascStatus23_type!=4)";
    // pT
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia  = new TH1D("h_pt_pythia", "h_pt_pythia", 50,0,20);

    pythia_tree->Draw("upsilon1s_pt>>h_pt_pythia",cuts_pythia);

    h_pt_pythia->Scale(scalefactor_pythia);

    h_pt_pythia->SetLineColor(kRed);

    h_pt_pythia->SetTitle(";p_{T} [GeV/c^{2}]; d#sigma/dp_{T} [2.5 GeV^{-1} c^{2}]");
    h_pt_pythia->Draw("hist");

    TLegend* leg_pt = new TLegend(0.7,0.7,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,"Pythia","l");
    leg_pt->SetTextFont(132);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics_Ycc/c_pt.pdf");


    // eta
    TCanvas* c_eta = new TCanvas("c_eta","c_eta");
    TH1D* h_eta_pythia  = new TH1D("h_eta_pythia", "h_eta_pythia", 50,-10,10);

    pythia_tree->Draw("upsilon1s_eta>>h_eta_pythia",cuts_pythia);

    h_eta_pythia->Scale(scalefactor_pythia);

    h_eta_pythia->SetLineColor(kRed);

    h_eta_pythia->SetTitle(";#eta ; d#sigma/d#eta [2.5]");
    h_eta_pythia->Draw("hist");


    TLegend* leg_eta = new TLegend(0.7,0.7,0.9,0.9);
    leg_eta->AddEntry(h_eta_pythia,"Pythia","l");
    leg_eta->SetTextFont(132);
    leg_eta->Draw();
    c_eta->Print("plots_kinematics_Ycc/c_eta.pdf");

    // Rapidity
    TCanvas* c_y = new TCanvas("c_y","c_y");
    TH1D* h_y_pythia  = new TH1D("h_y_pythia", "h_y_pythia", 50,-10,10);

    pythia_tree->Draw("upsilon1s_y>>h_y_pythia",cuts_pythia);

    h_y_pythia->Scale(scalefactor_pythia);

    h_y_pythia->SetLineColor(kRed);

    h_y_pythia->SetTitle(";y ; d#sigma/dy [2.5]");
    h_y_pythia->Draw("hist");


    TLegend* leg_y = new TLegend(0.7,0.7,0.9,0.9);
    leg_y->AddEntry(h_y_pythia,"Pythia","l");
    leg_y->SetTextFont(132);
    leg_y->Draw();

    c_y->Print("plots_kinematics_Ycc/c_y.pdf");

    // -------------------------------------------------------------------------
    // Plot 1D angles integrated over all eta 
    // -------------------------------------------------------------------------

    // angles
    TCanvas* c_delta_phi_upsilon1s_dx2 = new TCanvas("c_delta_phi_upsilon1s_dx2","c_delta_phi_upsilon1s_dx2");
    TH1D* h_delta_phi_upsilon1s_dx2_pythia  = new TH1D("h_delta_phi_upsilon1s_dx2_pythia", "h_delta_phi_upsilon1s_dx2_pythia", 10,0,TMath::Pi());

    pythia_tree->Draw("angle_upsilon1s_dx2>>h_delta_phi_upsilon1s_dx2_pythia",  cuts_pythia +cuts_all);

    h_delta_phi_upsilon1s_dx2_pythia->Scale(scalefactor_pythia);
    h_delta_phi_upsilon1s_dx2_pythia->SetLineColor(kRed);
    h_delta_phi_upsilon1s_dx2_pythia->SetMinimum(0.0);
    h_delta_phi_upsilon1s_dx2_pythia->SetTitle(";#Delta#phi(#Upsilon(1S), X_{c}) [rad]; d#sigma/d#Delta#phi");
    h_delta_phi_upsilon1s_dx2_pythia->Draw("hist");
    


    TLegend* leg_delta_phi_upsilon1s_dx2 = new TLegend(0.7,0.2,0.9,0.4);
    leg_delta_phi_upsilon1s_dx2->AddEntry(h_delta_phi_upsilon1s_dx2_pythia,"Pythia","l");
    leg_delta_phi_upsilon1s_dx2->SetTextFont(132);
    leg_delta_phi_upsilon1s_dx2->Draw();

    c_delta_phi_upsilon1s_dx2->Print("plots_kinematics_Ycc/c_delta_phi_upsilon1s_dx2.pdf");


    // angles
    TCanvas* c_delta_phi_upsilon1s_dx = new TCanvas("c_delta_phi_upsilon1s_dx","c_delta_phi_upsilon1s_dx");
    TH1D* h_delta_phi_upsilon1s_dx_pythia  = new TH1D("h_delta_phi_upsilon1s_dx_pythia", "h_delta_phi_upsilon1s_dx_pythia", 10,0,TMath::Pi());

    pythia_tree->Draw("angle_upsilon1s_dx>>h_delta_phi_upsilon1s_dx_pythia",  cuts_pythia +cuts_all);

    h_delta_phi_upsilon1s_dx_pythia->Scale(scalefactor_pythia);

    h_delta_phi_upsilon1s_dx_pythia->SetLineColor(kRed);

    h_delta_phi_upsilon1s_dx_pythia->SetTitle(";#Delta#phi(#Upsilon(1S), X_{c}) [rad]; d#sigma/d#Delta#phi");
    h_delta_phi_upsilon1s_dx_pythia->SetMinimum(0.0);
    h_delta_phi_upsilon1s_dx_pythia->Draw("hist");


    TLegend* leg_delta_phi_upsilon1s_dx = new TLegend(0.7,0.2,0.9,0.4);
    leg_delta_phi_upsilon1s_dx->AddEntry(h_delta_phi_upsilon1s_dx_pythia,"Pythia","l");
    leg_delta_phi_upsilon1s_dx->SetTextFont(132);
    leg_delta_phi_upsilon1s_dx->Draw();
    c_delta_phi_upsilon1s_dx->Print("plots_kinematics_Ycc/c_delta_phi_upsilon1s_dx.pdf");



    // angles
    TCanvas* c_delta_eta_upsilon1s_dx = new TCanvas("c_delta_eta_upsilon1s_dx","c_delta_eta_upsilon1s_dx");
    TH1D* h_delta_eta_upsilon1s_dx_pythia  = new TH1D("h_delta_eta_upsilon1s_dx_pythia", "h_delta_eta_upsilon1s_dx_pythia", 50,-5.0,5.0);

    pythia_tree->Draw("delta_y_upsilon1s_dx>>h_delta_eta_upsilon1s_dx_pythia",  cuts_pythia +cuts_all);

    h_delta_eta_upsilon1s_dx_pythia->Scale(scalefactor_pythia);

    h_delta_eta_upsilon1s_dx_pythia->SetLineColor(kRed);

    h_delta_eta_upsilon1s_dx_pythia->SetTitle(";#Deltay(#Upsilon(1S), X_{c}); d#sigma/d#Delta#phi");
    h_delta_eta_upsilon1s_dx_pythia->Draw("hist same");


    TLegend* leg_delta_eta_upsilon1s_dx = new TLegend(0.7,0.2,0.9,0.4);
    leg_delta_eta_upsilon1s_dx->AddEntry(h_delta_eta_upsilon1s_dx_pythia,"Pythia","l");
    leg_delta_eta_upsilon1s_dx->SetTextFont(132);
    leg_delta_eta_upsilon1s_dx->Draw();
    c_delta_eta_upsilon1s_dx->Print("plots_kinematics_Ycc/c_delta_eta_upsilon1s_dx.pdf");
    // -------------------------------------------------------------------------
    // Plot 2D angles integrated over all eta 
    // -------------------------------------------------------------------------


    SetLHCbStyle("cont");

    // angles
    TCanvas* c_delta_phi_pythia = new TCanvas("c_delta_phi_pythia","c_delta_phi_pythia");
    TH2D* h_delta_phi_pythia  = new TH2D("h_delta_phi_pythia", "h_delta_phi_pythia", 20,0,TMath::Pi(),20,0,TMath::Pi());

    pythia_tree->Draw("angle_upsilon1s_dx:angle_upsilon1s_dx2>>h_delta_phi_pythia",  cuts_pythia +cuts_all);


    h_delta_phi_pythia->Scale(scalefactor_pythia);

    h_delta_phi_pythia->SetTitle(";#Delta#phi(#Upsilon(1S), X_{c}) [rad]; #Delta#phi(#Upsilon(1S), X_{c}) [rad]");
    h_delta_phi_pythia->Draw("colz");
    c_delta_phi_pythia->Print("plots_kinematics_Ycc/c_delta_phi_pythia.pdf");
    

    // eta
    TCanvas* c_delta_eta_pythia = new TCanvas("c_delta_eta_pythia","c_delta_eta_pythia");
    TH2D* h_delta_eta_pythia  = new TH2D("h_delta_eta_pythia", "h_delta_eta_pythia", 20,-5.0,5.0,20,-5.0,5.0);

    pythia_tree->Draw("delta_eta_upsilon1s_dx:delta_eta_upsilon1s_dx2>>h_delta_eta_pythia",  cuts_pythia +cuts_all);

    h_delta_eta_pythia->Scale(scalefactor_pythia);

    h_delta_eta_pythia->SetTitle(";#Delta#eta(#Upsilon(1S), X_{c}) [rad]; #Delta#eta(#Upsilon(1S), X_{c}) [rad]");
    h_delta_eta_pythia->Draw("colz");
    c_delta_eta_pythia->Print("plots_kinematics_Ycc/c_delta_eta_pythia.pdf");

    // Delta R
    TCanvas* c_delta_R_pythia = new TCanvas("c_delta_R_pythia","c_delta_R_pythia");
    TH2D* h_delta_R_pythia  = new TH2D("h_delta_R_pythia", "h_delta_R_pythia", 20,0.0,10.0,20,0.0,10.0);

    pythia_tree->Draw("delta_R_upsilon1s_dx:delta_R_upsilon1s_dx2>>h_delta_R_pythia",  cuts_pythia +cuts_all);


    h_delta_R_pythia->Scale(scalefactor_pythia);

    h_delta_R_pythia->SetTitle(";#DeltaR(#Upsilon(1S), X_{c}) [rad]; #DeltaR(#Upsilon(1S), X_{c}) [rad]");
    h_delta_R_pythia->Draw("colz");
    c_delta_R_pythia->Print("plots_kinematics_Ycc/c_delta_R_pythia.pdf");
    
    // -------------------------------------------------------------------------
    // Plot 2D pT vs. y 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_y_pythia = new TCanvas("c_2D_pt_y_pythia","c_2D_pt_y_pythia");
    TH2D* h_2D_pt_y_pythia      = new TH2D("h_2D_pt_y_pythia",      "h_2D_pt_y_pythia",    20,-10,10,20,0,10);

    pythia_tree->Draw(    "upsilon1s_pt:upsilon1s_y>>h_2D_pt_y_pythia",    cuts_pythia +cuts_all    );


    h_2D_pt_y_pythia->SetTitle(";y;p_{T} [GeV/c^{2}]");
    h_2D_pt_y_pythia->Draw("colz");
    c_2D_pt_y_pythia->Print("plots_kinematics_Ycc/c_2D_pt_y_pythia.pdf");
    
    // ================================
    // All combinations 
    // ================================

    TH1D* h_1d_angle_upsilon1s_dx2_withMPI  = new TH1D("h_1d_angle_upsilon1s_dx2_withMPI", "h_1d_angle_upsilon1s_dx2_withMPI", 10,0,TMath::Pi());
    TH1D* h_1d_angle_upsilon1s_dx2_noMPI    = new TH1D("h_1d_angle_upsilon1s_dx2_noMPI",   "h_1d_angle_upsilon1s_dx2_noMPI",   10,0,TMath::Pi());

    h_1d_angle_upsilon1s_dx2_withMPI->SetLineColor(kRed);
    h_1d_angle_upsilon1s_dx2_noMPI->SetLineColor(kGreen+2);
    TH1D* h_1d_angle_upsilon1s_dx_withMPI  = new TH1D("h_1d_angle_upsilon1s_dx_withMPI", "h_1d_angle_upsilon1s_dx_withMPI", 10,0,TMath::Pi());
    TH1D* h_1d_angle_upsilon1s_dx_noMPI    = new TH1D("h_1d_angle_upsilon1s_dx_noMPI",   "h_1d_angle_upsilon1s_dx_noMPI",   10,0,TMath::Pi());
    h_1d_angle_upsilon1s_dx_withMPI->SetLineColor(kRed);
    h_1d_angle_upsilon1s_dx_noMPI->SetLineColor(kGreen+2);


    std::map<std::string,TCut> b_cuts;
    
    b_cuts["Hard_b_bb"] = "hasbStatus23&&hasbStatus23_type==5";
    b_cuts["Hard_b_bx"] = "hasbStatus23&&hasbStatus23_type!=5";
    
    b_cuts["MPI_b_bb"] = "hasbStatus33&&hasbStatus33_type==5";
    b_cuts["MPI_b_bx"] = "hasbStatus33&&hasbStatus33_type!=5";

    b_cuts["Hard_onia"] = "hasbOniaStatus23";
    b_cuts["MPI_onia"]  = "hasbOniaStatus33";
    
    b_cuts["Shower_b"]  = "!hasbStatus23&&!hasbStatus33&&!hasbOniaStatus23&&!hasbOniaStatus33";
    
    std::map<std::string,TCut> c_cuts;
    c_cuts["Hard_c_cc"] = "hascStatus23&&hascStatus23_type==4";
    c_cuts["Hard_c_cx"] = "hascStatus23&&hascStatus23_type!=4";
    
    c_cuts["MPI_c_cc"] = "hascStatus33&&hascStatus33_type==4";
    c_cuts["MPI_c_cx"] = "hascStatus33&&hascStatus33_type!=4";
    
    c_cuts["Shower_c"]  = "!hascStatus23&&!hascStatus33";


    TH2D* h_2d_withMPI     = new TH2D("h_2d_withMPI",    "", 5,0,TMath::Pi(),5,0,TMath::Pi());
    TH2D* h_2d_noMPI_pp    = new TH2D("h_2d_noMPI_pp",   "", 5,0,TMath::Pi(),5,0,TMath::Pi());
    TH2D* h_2d_noMPI_fe    = new TH2D("h_2d_noMPI_fe",   "", 5,0,TMath::Pi(),5,0,TMath::Pi());


    TH1D* h_1d_dPhi_withMPI     = new TH1D("h_1d_dPhi_withMPI",    "",10,0,TMath::Pi());
    TH1D* h_1d_dPhi_noMPI_pp    = new TH1D("h_1d_dPhi_noMPI_pp",   "",10,0,TMath::Pi());
    TH1D* h_1d_dPhi_noMPI_fe    = new TH1D("h_1d_dPhi_noMPI_fe",   "",10,0,TMath::Pi());

    TH1D* h_1d_pT_withMPI     = new TH1D("h_1d_pT_withMPI",    "", 50,0,20);
    TH1D* h_1d_pT_noMPI_pp    = new TH1D("h_1d_pT_noMPI_pp",   "", 50,0,20);
    TH1D* h_1d_pT_noMPI_fe    = new TH1D("h_1d_pT_noMPI_fe",   "", 50,0,20);
    
    for(const auto& b_cut: b_cuts){ 
        for(const auto& c_cut: c_cuts){ 
            std::string c_name = c_cut.first; 
            std::string b_name = b_cut.first; 


            TH2D* h_2d  = new TH2D(Form("h_2d_%s_%s",b_name.c_str(),c_name.c_str()), "", 10,0,TMath::Pi(),10,0,TMath::Pi());


            TCanvas* can_all = new TCanvas(Form("c_2d_%s_%s",b_name.c_str(),c_name.c_str()),
                                           Form("c_2d_%s_%s",b_name.c_str(),c_name.c_str()));

            TCut temp_cuts = cuts_pythia+c_cut.second+b_cut.second;
            int n = pythia_tree->Draw(Form("angle_upsilon1s_dx:angle_upsilon1s_dx2>>h_2d_%s_%s",b_name.c_str(),c_name.c_str()),temp_cuts,"colz");
            std::cout << b_name << "\t"<<c_name << "\t" << n << std::endl;


            if( ( b_name == "Hard_b_bb" && c_name == "Shower_c"   )||
                ( b_name == "Hard_onia" && c_name == "Shower_c"   )||
                ( b_name == "MPI_onia"  && c_name == "Shower_c"   )||
                ( b_name == "MPI_b_bb"  && c_name == "Shower_c"   )||
                ( b_name == "Shower_b"  && c_name == "Hard_c_cc"  )||
                ( b_name == "Shower_b"  && c_name == "MPI_c_cc"   )||
                ( b_name == "Shower_b"  && c_name == "Shower_c"   )
                ){
                
                pythia_tree->Draw("angle_upsilon1s_dx:angle_upsilon1s_dx2>>+h_2d_noMPI_pp", temp_cuts,"colz");
                pythia_tree->Draw("angle_upsilon1s_dx>>+h_1d_dPhi_noMPI_pp", temp_cuts,"colz");
                pythia_tree->Draw("angle_upsilon1s_dx2>>+h_1d_dPhi_noMPI_pp",temp_cuts,"colz");
                pythia_tree->Draw("upsilon1s_pt>>+h_1d_pT_noMPI_pp", temp_cuts,"colz");

            } else if( 
                ( b_name == "Hard_b_bx" && c_name == "Shower_c"   )||
                ( b_name == "MPI_b_bx"  && c_name == "Shower_c"   )||
                ( b_name == "Shower_b"  && c_name == "Hard_c_cx"  )||
                ( b_name == "Shower_b"  && c_name == "MPI_c_cx"   )||
                ( b_name == "Hard_b_bx" && c_name == "Hard_c_cx"  )
                ){
                pythia_tree->Draw("angle_upsilon1s_dx:angle_upsilon1s_dx2>>+h_2d_noMPI_fe",temp_cuts,"colz");
                pythia_tree->Draw("angle_upsilon1s_dx>>+h_1d_dPhi_noMPI_fe", temp_cuts,"colz");
                pythia_tree->Draw("angle_upsilon1s_dx2>>+h_1d_dPhi_noMPI_fe",temp_cuts,"colz");
                pythia_tree->Draw("upsilon1s_pt>>+h_1d_pT_noMPI_fe",temp_cuts,"colz");
            } else if(

                ( b_name == "Hard_b_bb" && c_name == "MPI_c_cc"   )||
                ( b_name == "Hard_b_bb" && c_name == "MPI_c_cx"   )||
                ( b_name == "Hard_b_bx" && c_name == "MPI_c_cc"   )||
                ( b_name == "Hard_b_bx" && c_name == "MPI_c_cx"   )||
                ( b_name == "MPI_b_bb"  && c_name == "Hard_c_cc"  )||
                ( b_name == "MPI_b_bb"  && c_name == "Hard_c_cx"  )||
                ( b_name == "MPI_b_bb"  && c_name == "MPI_c_cc"   )||
                ( b_name == "MPI_b_bb"  && c_name == "MPI_c_cx"   )||
                ( b_name == "MPI_b_bx"  && c_name == "Hard_c_cc"  )||
                ( b_name == "MPI_b_bx"  && c_name == "Hard_c_cx"  )||
                ( b_name == "MPI_b_bx"  && c_name == "MPI_c_cc"   )||
                ( b_name == "MPI_b_bx"  && c_name == "MPI_c_cx"   )||
                ( b_name == "Hard_onia" && c_name == "MPI_c_cc"   )||
                ( b_name == "Hard_onia" && c_name == "MPI_c_cx"   )||
                ( b_name == "MPI_onia"  && c_name == "Hard_c_cc"  )||
                ( b_name == "MPI_onia"  && c_name == "Hard_c_cx"  )||
                ( b_name == "MPI_onia"  && c_name == "MPI_c_cc"   )||
                ( b_name == "MPI_onia"  && c_name == "MPI_c_cx"   )

                ){
                pythia_tree->Draw("angle_upsilon1s_dx:angle_upsilon1s_dx2>>+h_2d_withMPI",temp_cuts,"colz");
                pythia_tree->Draw("angle_upsilon1s_dx>>+h_1d_dPhi_withMPI", temp_cuts,"colz");
                pythia_tree->Draw("angle_upsilon1s_dx2>>+h_1d_dPhi_withMPI",temp_cuts,"colz");
                pythia_tree->Draw("upsilon1s_pt>>+h_1d_pT_withMPI",temp_cuts,"colz");
            } else {
                std::cout << "    *** Missing category :" << std::endl;
            }

            h_2d->SetMinimum(0.0);
            h_2d->Draw("colz");
            h_2d->SetTitle(";#Delta#phi(#Upsilon(1S),X_{c});#Delta#phi(#Upsilon(1S),X_{c})");
            can_all->Print(Form("plots_kinematics_Ycc/c_2d_%s_%s.pdf",b_name.c_str(),c_name.c_str()));

            // //

        }
    }

    // 2D angles 
    TCanvas* c_2d_withMPI = new TCanvas("c_2d_withMPI","c_2d_withMPI");
    h_2d_withMPI->SetMinimum(0.0);
    h_2d_withMPI->Draw("colz");
    h_2d_withMPI->SetTitle(";#Delta#phi(#Upsilon(1S),X_{c});#Delta#phi(#Upsilon(1S),X_{c})");
    c_2d_withMPI->Print("plots_kinematics_Ycc/c_2d_withMPI.pdf");
    

    TCanvas* c_2d_noMPI_pp = new TCanvas("c_2d_noMPI_pp","c_2d_noMPI_pp");
    h_2d_noMPI_pp->SetMinimum(0.0);
    h_2d_noMPI_pp->Draw("colz");
    h_2d_noMPI_pp->SetTitle(";#Delta#phi(#Upsilon(1S),X_{c});#Delta#phi(#Upsilon(1S),X_{c})");
    c_2d_noMPI_pp->Print("plots_kinematics_Ycc/c_2d_noMPI_pp.pdf");
    
    

    TCanvas* c_2d_noMPI_fe = new TCanvas("c_2d_noMPI_fe","c_2d_noMPI_fe");
    h_2d_noMPI_fe->SetMinimum(0.0);
    h_2d_noMPI_fe->Draw("colz");
    h_2d_noMPI_fe->SetTitle(";#Delta#phi(#Upsilon(1S),X_{c});#Delta#phi(#Upsilon(1S),X_{c})");
    c_2d_noMPI_fe->Print("plots_kinematics_Ycc/c_2d_noMPI_fe.pdf");


    SetLHCbStyle("oneD");

    // 1D angles 
    TCanvas* c_1d_dPhi_withMPI = new TCanvas("c_1d_dPhi_withMPI","c_1d_dPhi_withMPI");
    h_1d_dPhi_withMPI->SetMinimum(0.0);
    h_1d_dPhi_withMPI->Draw("colz");
    h_1d_dPhi_withMPI->SetTitle(";#Delta#phi(#Upsilon(1S),X_{c});Events");
    c_1d_dPhi_withMPI->Print("plots_kinematics_Ycc/c_1d_dPhi_withMPI.pdf");
    

    TCanvas* c_1d_dPhi_noMPI_pp = new TCanvas("c_1d_dPhi_noMPI_pp","c_1d_dPhi_noMPI_pp");
    h_1d_dPhi_noMPI_pp->SetMinimum(0.0);
    h_1d_dPhi_noMPI_pp->Draw("colz");
    h_1d_dPhi_noMPI_pp->SetTitle(";#Delta#phi(#Upsilon(1S),X_{c});Events");
    c_1d_dPhi_noMPI_pp->Print("plots_kinematics_Ycc/c_1d_dPhi_noMPI_pp.pdf");
    
    

    TCanvas* c_1d_dPhi_noMPI_fe = new TCanvas("c_1d_dPhi_noMPI_fe","c_1d_dPhi_noMPI_fe");
    h_1d_dPhi_noMPI_fe->SetMinimum(0.0);
    h_1d_dPhi_noMPI_fe->Draw("colz");
    h_1d_dPhi_noMPI_fe->SetTitle(";#Delta#phi(#Upsilon(1S),X_{c});Events");
    c_1d_dPhi_noMPI_fe->Print("plots_kinematics_Ycc/c_1d_dPhi_noMPI_fe.pdf");




    // 1D pT 

    TCanvas* c_1d_pT_withMPI = new TCanvas("c_1d_pT_withMPI","c_1d_pT_withMPI");
    h_1d_pT_withMPI->SetMinimum(0.0);
    h_1d_pT_withMPI->Draw("colz");
    h_1d_pT_withMPI->SetTitle(";p_{T} [GeV/c^{2}];Entries");
    c_1d_pT_withMPI->Print("plots_kinematics_Ycc/c_1d_pT_withMPI.pdf");
    

    TCanvas* c_1d_pT_noMPI_pp = new TCanvas("c_1d_pT_noMPI_pp","c_1d_pT_noMPI_pp");
    h_1d_pT_noMPI_pp->SetMinimum(0.0);
    h_1d_pT_noMPI_pp->Draw("colz");
    h_1d_pT_noMPI_pp->SetTitle(";p_{T} [GeV/c^{2}];Entries");
    c_1d_pT_noMPI_pp->Print("plots_kinematics_Ycc/c_1d_pT_noMPI_pp.pdf");
    
    

    TCanvas* c_1d_pT_noMPI_fe = new TCanvas("c_1d_pT_noMPI_fe","c_1d_pT_noMPI_fe");
    h_1d_pT_noMPI_fe->SetMinimum(0.0);
    h_1d_pT_noMPI_fe->Draw("colz");
    h_1d_pT_noMPI_fe->SetTitle(";p_{T} [GeV/c^{2}];Entries");
    c_1d_pT_noMPI_fe->Print("plots_kinematics_Ycc/c_1d_pT_noMPI_fe.pdf");

}