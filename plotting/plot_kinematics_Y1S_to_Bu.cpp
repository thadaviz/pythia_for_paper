#include "tools.h"

void plot_kinematics_Y1S_to_Bu(){
    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    std::string filename_pythia_cc       = "../output/from_lxplus/Complete_paper_samples/Sample_bb/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_10000000_PartMod1_noColRec.root";

    // -------------------------------------------------------------------------
    // Fig cuts
    // -------------------------------------------------------------------------

    TCut Upsilon1S_fid = "upsilon1s_y > 2.0 && upsilon1s_y < 4.5";
    // TCut Upsilon1S_fid = "";

    TCut Bu_fid = "Bu_p_y > 2.0 && Bu_p_y < 4.5";
    // TCut Bu_fid = "";

    TCut cuts_Upsilon1S_all    = "nUpsilon1S>0";
    TCut cuts_Bu_all           = "nBu_p>=1";

    TCut cuts_DPS = "Upsilon1S_iscc&&(Upsilon1S_iscc_c_partSys!=Upsilon1S_iscc_cbar_partSys)";
    TCut cuts_SPS = "(Upsilon1S_isHard||Upsilon1S_isMPI||(Upsilon1S_iscc&&(Upsilon1S_iscc_c_partSys==Upsilon1S_iscc_cbar_partSys)))";

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open(filename_pythia_cc.c_str());
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");

    pythia_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("bx2_pt" , "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    pythia_tree->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");


    pythia_tree->SetAlias("angle_bx2_bx","acos((bx2_px*bx_px + bx2_py*bx_py)/(bx2_pt*bx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_bx","acos((upsilon1s_px*bx_px + upsilon1s_py*bx_py)/(upsilon1s_pt*bx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_bx2","acos((upsilon1s_px*bx2_px + upsilon1s_py*bx2_py)/(upsilon1s_pt*bx2_pt))");


    pythia_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree->SetAlias("bx2_p",  "sqrt(bx2_px*bx2_px+bx2_py*bx2_py+bx2_pz*bx2_pz)");
    pythia_tree->SetAlias("bx2_pt", "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    pythia_tree->SetAlias("bx2_eta","(bx2_pz>0?-1:1)*acosh(bx2_p/bx2_pt)");
    pythia_tree->SetAlias("bx2_y","0.5*log( (bx2_pe+bx2_pz) / (bx2_pe-bx2_pz) )");

    pythia_tree->SetAlias("Bu_p_p",  "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py+Bu_p_pz*Bu_p_pz)");
    pythia_tree->SetAlias("Bu_p_pt", "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py)");
    pythia_tree->SetAlias("Bu_p_eta","(Bu_p_pz>0?-1:1)*acosh(Bu_p_p/Bu_p_pt)");
    pythia_tree->SetAlias("Bu_p_y","0.5*log( (Bu_p_pe+Bu_p_pz) / (Bu_p_pe-Bu_p_pz) )");

    pythia_tree->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");

    pythia_tree->SetAlias("delta_eta_bx2_bx","bx2_eta-bx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_bx","upsilon1s_eta-bx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_bx2","upsilon1s_eta-bx2_eta");

    pythia_tree->SetAlias("delta_y_bx2_bx","bx2_y-bx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_bx","upsilon1s_y-bx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_bx2","upsilon1s_y-bx2_y");

    pythia_tree->SetAlias("delta_R_bx2_bx","sqrt(delta_eta_bx2_bx*delta_eta_bx2_bx+angle_bx2_bx*angle_bx2_bx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_bx","sqrt(delta_eta_upsilon1s_bx*delta_eta_upsilon1s_bx+angle_upsilon1s_bx*angle_upsilon1s_bx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_bx2","sqrt(delta_eta_upsilon1s_bx2*delta_eta_upsilon1s_bx2+angle_upsilon1s_bx2*angle_upsilon1s_bx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
 
    std::cout << "Cross section Pythia:        " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nUpsilon1S>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
   
    
    double scalefactor_pythia     = sigmaGen_pythia*microbarn/n_entries_pythia;

    // -------------------------------------------------------------------------
    // Plot pT 
    // -------------------------------------------------------------------------
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia  = new TH1D("h_pt_pythia", "h_pt_pythia", 20,0,20);
    TH1D* h_pt_pythia_Dz  = new TH1D("h_pt_pythia_Dz", "h_pt_pythia_Dz", 20,0,20);


    pythia_tree->Draw("upsilon1s_pt>>h_pt_pythia",cuts_Upsilon1S_all+Upsilon1S_fid);
    pythia_tree->Draw("Bu_p_pt>>h_pt_pythia_Dz",cuts_Bu_all+Bu_fid);

    h_pt_pythia->Scale(scalefactor_pythia);
    h_pt_pythia_Dz->Scale(scalefactor_pythia);


    // Black -> With MPI
    h_pt_pythia->SetLineColor(kBlack);

    // Black -> With MPI
    h_pt_pythia_Dz->SetLineColor(kBlack);
    h_pt_pythia_Dz->SetLineStyle(kDashed);


    h_pt_pythia->SetMaximum(1.05*max({h_pt_pythia->GetMaximum(),
                                      h_pt_pythia_Dz->GetMaximum()
                                  }));
    
    double h_pt_pythia_bin_width = h_pt_pythia->GetXaxis()->GetBinWidth(2);
    h_pt_pythia->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_pythia_bin_width));
    h_pt_pythia->Draw("hist");
    h_pt_pythia_Dz->Draw("hist same");
    gPad->SetLogy();

    TLegend* leg_pt = new TLegend(0.6,0.6,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,"#Upsilon(1S) Pythia - All","l");
    leg_pt->AddEntry(h_pt_pythia_Dz,"B^{+} Pythia - All","l");
    leg_pt->SetTextFont(132);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics_Y1S_to_Bu/c_pt.pdf");

    TCanvas* c_pt_ratio = new TCanvas("c_pt_ratio","c_pt_ratio");

    TH1D* h_pt_pythia_ratio  = new TH1D("h_pt_pythia_ratio", "h_pt_pythia_ratio", 20,0,20);
    h_pt_pythia_ratio->Divide(h_pt_pythia,h_pt_pythia_Dz);

    h_pt_pythia_ratio->Scale(100);
    
    h_pt_pythia_ratio->SetMaximum(1.05*max({
                                                  h_pt_pythia_ratio->GetMaximum()
                                      }));
    h_pt_pythia_ratio->SetTitle(";#it{p}_{T} [GeV/#it{c}];R(#it{p}_{T}) (%)");
    // h_pt_pythia_ratio->SetMinimum(0);
    // h_pt_pythia_ratio->SetMaximum(1.8);
    h_pt_pythia_ratio->Draw();


    TLegend* leg_pt_ratio = new TLegend(0.2,0.7,0.5,0.9);
    leg_pt_ratio->AddEntry(h_pt_pythia_ratio,"Pythia - All","pe");
    leg_pt_ratio->SetTextFont(132);
    leg_pt_ratio->SetFillStyle(0);
    leg_pt_ratio->Draw();

    c_pt_ratio->Print("plots_kinematics_Y1S_to_Bu/c_pt_ratio.pdf");


    // -------------------------------------------------------------------------
    // Plot multiplicity
    // -------------------------------------------------------------------------
    TCanvas* c_multiplicity = new TCanvas("c_multiplicity","c_multiplicity");
    TH1D* h_multiplicity_pythia  = new TH1D("h_multiplicity_pythia", "h_multiplicity_pythia", 20,0,100);
    TH1D* h_multiplicity_pythia_DPS  = new TH1D("h_multiplicity_pythia_DPS", "h_multiplicity_pythia_DPS", 20,0,100);
    TH1D* h_multiplicity_pythia_SPS  = new TH1D("h_multiplicity_pythia_SPS", "h_multiplicity_pythia_SPS", 20,0,100);

    TH1D* h_multiplicity_pythia_Dz  = new TH1D("h_multiplicity_pythia_Dz", "h_multiplicity_pythia_Dz", 20,0,100);

    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia",cuts_Upsilon1S_all+Upsilon1S_fid);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_DPS",cuts_Upsilon1S_all+Upsilon1S_fid+cuts_DPS);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_SPS",cuts_Upsilon1S_all+Upsilon1S_fid+cuts_SPS);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_Dz",cuts_Bu_all+Bu_fid);

    h_multiplicity_pythia->Scale(scalefactor_pythia);
    h_multiplicity_pythia_DPS->Scale(scalefactor_pythia);
    h_multiplicity_pythia_SPS->Scale(scalefactor_pythia);
    h_multiplicity_pythia_Dz->Scale(scalefactor_pythia);

    // Black -> With MPI
    h_multiplicity_pythia->SetLineColor(kBlack);

    h_multiplicity_pythia_Dz->SetLineColor(kBlack);
    h_multiplicity_pythia_Dz->SetLineStyle(kDashed);

    h_multiplicity_pythia_DPS->SetLineColor(kRed);
    h_multiplicity_pythia_SPS->SetLineColor(kBlue);

    h_multiplicity_pythia->SetMaximum(1.05*max({h_multiplicity_pythia->GetMaximum(),
                                                h_multiplicity_pythia_Dz->GetMaximum(),
                                                h_multiplicity_pythia_DPS->GetMaximum(),
                                                h_multiplicity_pythia_SPS->GetMaximum()
                                      }));
    
    double h_multiplicity_pythia_bin_width = h_multiplicity_pythia->GetXaxis()->GetBinWidth(2);
    h_multiplicity_pythia->SetTitle(Form(";N_{Charged in LHCb}; d#sigma/dN [%.1f #mub]",h_multiplicity_pythia_bin_width));
    h_multiplicity_pythia->Draw("hist");
    h_multiplicity_pythia_Dz->Draw("same hist");
    h_multiplicity_pythia_DPS->Draw("same hist");
    h_multiplicity_pythia_SPS->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_multiplicity = new TLegend(0.6,0.6,0.9,0.9);
    leg_multiplicity->AddEntry(h_multiplicity_pythia_Dz,"B^{+} Pythia - All","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia,    "#Upsilon(1S) Pythia - All","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_SPS,"#Upsilon(1S) Pythia - Just DPS","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_DPS,"#Upsilon(1S) Pythia - Just SPS","l");
    leg_multiplicity->SetTextFont(132);
    leg_multiplicity->SetFillStyle(0);
    leg_multiplicity->Draw();
    c_multiplicity->Print("plots_kinematics_Y1S_to_Bu/c_multiplicity.pdf");

    TCanvas* c_multiplicity_ratio = new TCanvas("c_multiplicity_ratio","c_multiplicity_ratio");

    TH1D* h_multiplicity_pythia_ratio  = new TH1D("h_multiplicity_pythia_ratio", "h_multiplicity_pythia_ratio", 20,0,100);
    TH1D* h_multiplicity_pythia_SPS_ratio  = new TH1D("h_multiplicity_pythia_SPS_ratio", "h_multiplicity_pythia_SPS_ratio", 20,0,100);
    TH1D* h_multiplicity_pythia_DPS_ratio  = new TH1D("h_multiplicity_pythia_DPS_ratio", "h_multiplicity_pythia_DPS_ratio", 20,0,100);

    h_multiplicity_pythia_ratio->Divide(h_multiplicity_pythia,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_DPS_ratio->Divide(h_multiplicity_pythia_DPS,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_SPS_ratio->Divide(h_multiplicity_pythia_SPS,h_multiplicity_pythia_Dz);


    h_multiplicity_pythia_ratio->Scale(100);
    h_multiplicity_pythia_DPS_ratio->Scale(100);
    h_multiplicity_pythia_SPS_ratio->Scale(100);
    
            // Black -> With MPI
    h_multiplicity_pythia_ratio->SetLineColor(kBlack);


    h_multiplicity_pythia_DPS_ratio->SetLineColor(kRed);
    h_multiplicity_pythia_DPS_ratio->SetMarkerColor(kRed);
    h_multiplicity_pythia_SPS_ratio->SetLineColor(kBlue);
    h_multiplicity_pythia_SPS_ratio->SetMarkerColor(kBlue);

    h_multiplicity_pythia_ratio->SetMaximum(1.05*max({
                                                  h_multiplicity_pythia_ratio->GetMaximum(),
                                                  h_multiplicity_pythia_DPS_ratio->GetMaximum(),
                                                  h_multiplicity_pythia_SPS_ratio->GetMaximum()
                                      }));
    h_multiplicity_pythia_ratio->SetTitle(";N_{Charged in LHCb};#scale[0.6]{#frac{d#sigma(#Upsilon(1S))}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_multiplicity_pythia_ratio->SetMinimum(0);
    h_multiplicity_pythia_ratio->Draw();
    h_multiplicity_pythia_DPS_ratio->Draw("same");
    h_multiplicity_pythia_SPS_ratio->Draw("same");


    TLegend* leg_multiplicity_ratio = new TLegend(0.5,0.4,0.8,0.7);
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_ratio,"Pythia - All","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_DPS_ratio,"Pythia - Just DPS","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_SPS_ratio,"Pythia - Just SPS","pe");
    leg_multiplicity_ratio->SetTextFont(132);
    leg_multiplicity_ratio->SetFillStyle(0);
    leg_multiplicity_ratio->Draw();

    c_multiplicity_ratio->Print("plots_kinematics_Y1S_to_Bu/c_multiplicity_ratio.pdf");

    // -------------------------------------------------------------------------
    // Plot multiplicity
    // -------------------------------------------------------------------------
    TCanvas* c_nMPI = new TCanvas("c_nMPI","c_nMPI");
    TH1D* h_nMPI_pythia  = new TH1D("h_nMPI_pythia", "h_nMPI_pythia", 30,0,30);
    TH1D* h_nMPI_pythia_DPS  = new TH1D("h_nMPI_pythia_DPS", "h_nMPI_pythia_DPS", 30,0,30);
    TH1D* h_nMPI_pythia_SPS  = new TH1D("h_nMPI_pythia_SPS", "h_nMPI_pythia_SPS", 30,0,30);

    TH1D* h_nMPI_pythia_Dz  = new TH1D("h_nMPI_pythia_Dz", "h_nMPI_pythia_Dz", 30,0,30);

    pythia_tree->Draw("nMPI>>h_nMPI_pythia",cuts_Upsilon1S_all+Upsilon1S_fid);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_DPS",cuts_Upsilon1S_all+Upsilon1S_fid+cuts_DPS);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_SPS",cuts_Upsilon1S_all+Upsilon1S_fid+cuts_SPS);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia",cuts_Upsilon1S_all+Upsilon1S_fid);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_Dz",cuts_Bu_all+Bu_fid);

    h_nMPI_pythia->Scale(scalefactor_pythia);
    h_nMPI_pythia_DPS->Scale(scalefactor_pythia);
    h_nMPI_pythia_SPS->Scale(scalefactor_pythia);
    h_nMPI_pythia_Dz->Scale(scalefactor_pythia);


    // Black -> With MPI
    h_nMPI_pythia->SetLineColor(kBlack);

    h_nMPI_pythia_Dz->SetLineColor(kBlack);
    h_nMPI_pythia_Dz->SetLineStyle(kDashed);


    h_nMPI_pythia_DPS->SetLineColor(kRed);
    h_nMPI_pythia_SPS->SetLineColor(kBlue);

    h_nMPI_pythia->SetMaximum(1.05*max({h_nMPI_pythia->GetMaximum(),
                                      h_nMPI_pythia_Dz->GetMaximum(),
                                      h_nMPI_pythia_DPS->GetMaximum(),
                                      h_nMPI_pythia_SPS->GetMaximum()
                                      }));
    
    double h_nMPI_pythia_bin_width = h_nMPI_pythia->GetXaxis()->GetBinWidth(2);
    h_nMPI_pythia->SetTitle(Form(";N_{MPI}; d#sigma/dN [%.1f #mub]",h_nMPI_pythia_bin_width));
    h_nMPI_pythia->Draw("hist");
    h_nMPI_pythia_Dz->Draw("same hist");
    h_nMPI_pythia_DPS->Draw("same hist");
    h_nMPI_pythia_SPS->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_nMPI = new TLegend(0.6,0.6,0.9,0.9);
    leg_nMPI->AddEntry(h_nMPI_pythia_Dz,"B^{+} Pythia - All","l");
    leg_nMPI->AddEntry(h_nMPI_pythia,    "#Upsilon(1S) Pythia - All","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_DPS,"#Upsilon(1S) Pythia - Just DPS","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_SPS,"#Upsilon(1S) Pythia - Just SPS","l");

    leg_nMPI->SetTextFont(132);
    leg_nMPI->SetFillStyle(0);
    leg_nMPI->Draw();
    c_nMPI->Print("plots_kinematics_Y1S_to_Bu/c_nMPI.pdf");

    TCanvas* c_nMPI_ratio = new TCanvas("c_nMPI_ratio","c_nMPI_ratio");

    TH1D* h_nMPI_pythia_ratio  = new TH1D("h_nMPI_pythia_ratio", "h_nMPI_pythia_ratio", 30,0,30);
    TH1D* h_nMPI_pythia_DPS_ratio  = new TH1D("h_nMPI_pythia_DPS_ratio", "h_nMPI_pythia_DPS_ratio", 30,0,30);
    TH1D* h_nMPI_pythia_SPS_ratio  = new TH1D("h_nMPI_pythia_SPS_ratio", "h_nMPI_pythia_SPS_ratio", 30,0,30);

    h_nMPI_pythia_ratio->Divide(h_nMPI_pythia,h_nMPI_pythia_Dz);
    h_nMPI_pythia_DPS_ratio->Divide(h_nMPI_pythia_DPS,h_nMPI_pythia_Dz);
    h_nMPI_pythia_SPS_ratio->Divide(h_nMPI_pythia_SPS,h_nMPI_pythia_Dz);


    h_nMPI_pythia_ratio->Scale(100);
    h_nMPI_pythia_DPS_ratio->Scale(100);
    h_nMPI_pythia_SPS_ratio->Scale(100);
    
        // Black -> With MPI
    h_nMPI_pythia_ratio->SetLineColor(kBlack);


    h_nMPI_pythia_DPS_ratio->SetLineColor(kRed);
    h_nMPI_pythia_DPS_ratio->SetMarkerColor(kRed);
    h_nMPI_pythia_SPS_ratio->SetLineColor(kBlue);
    h_nMPI_pythia_SPS_ratio->SetMarkerColor(kBlue);

    h_nMPI_pythia_ratio->SetMaximum(1.05*max({h_nMPI_pythia_ratio->GetMaximum(),
                                              h_nMPI_pythia_DPS_ratio->GetMaximum(),
                                              h_nMPI_pythia_SPS_ratio->GetMaximum()
                                      }));
    h_nMPI_pythia_ratio->SetTitle(";N_{MPI};#scale[0.6]{#frac{d#sigma(#Upsilon(1S))}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_nMPI_pythia_ratio->SetMinimum(0);
    h_nMPI_pythia_ratio->Draw();
    h_nMPI_pythia_DPS_ratio->Draw("same");
    h_nMPI_pythia_SPS_ratio->Draw("same");


    TLegend* leg_nMPI_ratio = new TLegend(0.2,0.4,0.5,0.7);
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_ratio,"Pythia - All","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_DPS_ratio,"Pythia - Just DPS","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_SPS_ratio,"Pythia - Just SPS","pe");
    leg_nMPI_ratio->SetTextFont(132);
    leg_nMPI_ratio->SetFillStyle(0);
    leg_nMPI_ratio->Draw();
    c_nMPI_ratio->Print("plots_kinematics_Y1S_to_Bu/c_nMPI_ratio.pdf");



}

