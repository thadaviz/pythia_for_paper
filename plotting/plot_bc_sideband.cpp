#include "tools.h"
void plot_bc_sideband(){
    SetLHCbStyle("oneD");

    TFile* tfile_pythia = TFile::Open("../output/main202output_SoftQCD_nd_UserHook_bc_N_12000_PartMod1.root");
    TTree* pythia_parts = (TTree*)tfile_pythia->Get("parts");
    TFile* tfile_vincia = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bc_N_20000.root");
    TTree* vincia_parts = (TTree*)tfile_vincia->Get("parts");


    TH1D* h_bc_sideband_pythia    = new TH1D("h_bc_sideband_pythia",   ";log_{10}(m(b+c[+g]) [GeV/c^{2}]);Events",100,0,4);
    TH1D* h_bc_sideband_vincia    = new TH1D("h_bc_sideband_vincia",   ";log_{10}(m(b+c[+g]) [GeV/c^{2}]);Events",100,0,4);
    TH1D* h_bc_sideband_pythia_bc = new TH1D("h_bc_sideband_pythia_bc","h_bc_sideband_pythia_bc",100,0,4);
    TH1D* h_bc_sideband_vincia_bc = new TH1D("h_bc_sideband_vincia_bc","h_bc_sideband_vincia_bc",100,0,4);

    pythia_parts->Draw("log10(bc_sideband)>>h_bc_sideband_pythia","abs(id)==5&&con_to_c&&!(isBc||isBcstar)");
    pythia_parts->Draw("log10(bc_sideband)>>h_bc_sideband_pythia_bc","abs(id)==5&&con_to_c&&(isBc||isBcstar)");
    vincia_parts->Draw("log10(bc_sideband)>>h_bc_sideband_vincia","abs(id)==5&&con_to_c&&!(isBc||isBcstar)");
    vincia_parts->Draw("log10(bc_sideband)>>h_bc_sideband_vincia_bc","abs(id)==5&&con_to_c&&(isBc||isBcstar)");


    TCanvas* can_pythia = new TCanvas("can_pythia","can_pythia");


    h_bc_sideband_pythia_bc->SetFillColor(kRed);
    h_bc_sideband_pythia->Draw();

    h_bc_sideband_pythia_bc->Draw("same");

    can_pythia->Print("plots_bc_sideband/can_pythia.pdf");

    TCanvas* can_vincia = new TCanvas("can_vincia","can_vincia");

    h_bc_sideband_vincia_bc->SetFillColor(kRed);

    h_bc_sideband_vincia->Draw();
    h_bc_sideband_vincia_bc->Draw("same");

    can_vincia->Print("plots_bc_sideband/can_vincia.pdf");

    TCanvas* can_bc = new TCanvas("can_bc","can_bc");


    TH1D* h_pythia_bc = new TH1D("h_pythia_bc","h_pythia_bc",20,4,8);
    TH1D* h_vincia_bc = new TH1D("h_vincia_bc","h_vincia_bc",20,4,8);

    vincia_parts->Draw("bc_sideband>>h_vincia_bc","abs(id)==5&&con_to_c&&(isBc||isBcstar)");
    pythia_parts->Draw("bc_sideband>>h_pythia_bc","abs(id)==5&&con_to_c&&(isBc||isBcstar)");

    h_vincia_bc->Scale(1.0/h_vincia_bc->Integral());
    h_pythia_bc->Scale(1.0/h_pythia_bc->Integral());
    h_pythia_bc->Draw("hist");
    h_vincia_bc->Draw("hist same");
    
}