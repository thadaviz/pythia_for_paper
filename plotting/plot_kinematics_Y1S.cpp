#include "tools.h"

void plot_kinematics_Y1S(){

    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_Scale_4.000000_Quark_5_N_184000.root");
    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_Scale_4.000000_Quark_5_N_35000.root");
    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_Scale_4.000000_Quark_5_N_400000_PartMod1.root");
    TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_Scale_4.000000_Quark_5_N_4860000_PartMod1.root");
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");


    pythia_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("bx2_pt" , "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    pythia_tree->SetAlias("bx_pt" , "sqrt(bx_px*bx_px+bx_py*bx_py)");


    pythia_tree->SetAlias("angle_bx2_bx","acos((bx2_px*bx_px + bx2_py*bx_py)/(bx2_pt*bx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_bx","acos((upsilon1s_px*bx_px + upsilon1s_py*bx_py)/(upsilon1s_pt*bx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_bx2","acos((upsilon1s_px*bx2_px + upsilon1s_py*bx2_py)/(upsilon1s_pt*bx2_pt))");


    pythia_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree->SetAlias("bx2_p",  "sqrt(bx2_px*bx2_px+bx2_py*bx2_py+bx2_pz*bx2_pz)");
    pythia_tree->SetAlias("bx2_pt", "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    pythia_tree->SetAlias("bx2_eta","(bx2_pz>0?-1:1)*acosh(bx2_p/bx2_pt)");
    pythia_tree->SetAlias("bx2_y","0.5*log( (bx2_pe+bx2_pz) / (bx2_pe-bx2_pz) )");


    pythia_tree->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    pythia_tree->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    pythia_tree->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");
    pythia_tree->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");


    pythia_tree->SetAlias("delta_eta_bx2_bx","bx2_eta-bx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_bx","upsilon1s_eta-bx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_bx2","upsilon1s_eta-bx2_eta");

    pythia_tree->SetAlias("delta_y_bx2_bx","bx2_y-bx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_bx","upsilon1s_y-bx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_bx2","upsilon1s_y-bx2_y");

    pythia_tree->SetAlias("delta_R_bx2_bx","sqrt(delta_eta_bx2_bx*delta_eta_bx2_bx+angle_bx2_bx*angle_bx2_bx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_bx","sqrt(delta_eta_upsilon1s_bx*delta_eta_upsilon1s_bx+angle_upsilon1s_bx*angle_upsilon1s_bx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_bx2","sqrt(delta_eta_upsilon1s_bx2*delta_eta_upsilon1s_bx2+angle_upsilon1s_bx2*angle_upsilon1s_bx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();


    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    
    std::cout << "Cross section Pythia:  " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nUpsilon1S==1&&nXb==2")<< "/" << pythia_tree->GetEntries("")<< std::endl;
    
    double withtarget_pythia = pythia_tree->GetEntries("nUpsilon1S&&nXb==2");
    double scalefactor_pythia  =      sigmaGen_pythia*microbarn/n_entries_pythia * (withtarget_pythia/n_entries_pythia);
    
    std::cout << "scalefactor_pythia " << scalefactor_pythia<<std::endl;

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    TCut cuts_pythia  = "nUpsilon1S==1&&nXb==2&&bx2_pt!=0&&bx_pt!=0&&upsilon1s_pt!=0";

    // TCut cuts_all = "(upsilon1s_eta)>2 && (upsilon1s_eta)<5 && (bx_eta)>2 && (bx_eta)<5 && (bx2_eta)>2 && (bx2_eta)<5 ";
    TCut cuts_all = "";
    

    // -------------------------------------------------------------------------
    // Plot 1D kinematics integrated over all eta 
    // -------------------------------------------------------------------------
    // TCut temp_cut = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5) && foundHardc&&hasbStatus23&&(hasbStatus23_type!=4)";
    // pT
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia  = new TH1D("h_pt_pythia", "h_pt_pythia", 50,0,20);

    pythia_tree->Draw("upsilon1s_pt>>h_pt_pythia",cuts_pythia);

    h_pt_pythia->Scale(scalefactor_pythia);
    h_pt_pythia->SetLineColor(kRed);
    h_pt_pythia->SetTitle(";p_{T} [GeV/c^{2}]; d#sigma/dp_{T} [2.5 GeV^{-1} c^{2}]");
    h_pt_pythia->Draw("hist");

    TLegend* leg_pt = new TLegend(0.7,0.7,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,"Pythia","l");
    leg_pt->SetTextFont(132);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics_Y1S/c_pt.pdf");


    // eta
    TCanvas* c_eta = new TCanvas("c_eta","c_eta");
    TH1D* h_eta_pythia  = new TH1D("h_eta_pythia", "h_eta_pythia", 50,-10,10);

    pythia_tree->Draw("upsilon1s_eta>>h_eta_pythia",cuts_pythia);

    h_eta_pythia->Scale(scalefactor_pythia);
    h_eta_pythia->SetLineColor(kRed);
    h_eta_pythia->SetTitle(";#eta ; d#sigma/d#eta [2.5]");
    h_eta_pythia->Draw("hist");


    TLegend* leg_eta = new TLegend(0.7,0.7,0.9,0.9);
    leg_eta->AddEntry(h_eta_pythia,"Pythia","l");
    leg_eta->SetTextFont(132);
    leg_eta->Draw();
    c_eta->Print("plots_kinematics_Y1S/c_eta.pdf");

    // Rapidity
    TCanvas* c_y = new TCanvas("c_y","c_y");
    TH1D* h_y_pythia  = new TH1D("h_y_pythia", "h_y_pythia", 50,-10,10);

    pythia_tree->Draw("upsilon1s_y>>h_y_pythia",cuts_pythia);

    h_y_pythia->Scale(scalefactor_pythia);
    h_y_pythia->SetLineColor(kRed);
    h_y_pythia->SetTitle(";y ; d#sigma/dy [2.5]");
    h_y_pythia->Draw("hist");


    TLegend* leg_y = new TLegend(0.7,0.7,0.9,0.9);
    leg_y->AddEntry(h_y_pythia,"Pythia","l");
    leg_y->SetTextFont(132);
    leg_y->Draw();

    c_y->Print("plots_kinematics_Y1S/c_y.pdf");

    // -------------------------------------------------------------------------
    // Plot 1D angles integrated over all eta 
    // -------------------------------------------------------------------------

    // angles
    TCanvas* c_delta_phi_upsilon1s_bx2 = new TCanvas("c_delta_phi_upsilon1s_bx2","c_delta_phi_upsilon1s_bx2");
    TH1D* h_delta_phi_upsilon1s_bx2_pythia  = new TH1D("h_delta_phi_upsilon1s_bx2_pythia", "h_delta_phi_upsilon1s_bx2_pythia", 50,0,TMath::Pi());

    pythia_tree->Draw("angle_upsilon1s_bx2>>h_delta_phi_upsilon1s_bx2_pythia",  cuts_pythia +cuts_all);

    h_delta_phi_upsilon1s_bx2_pythia->Scale(scalefactor_pythia);
    h_delta_phi_upsilon1s_bx2_pythia->SetLineColor(kRed);
    h_delta_phi_upsilon1s_bx2_pythia->Draw("hist");


    TLegend* leg_delta_phi_upsilon1s_bx2 = new TLegend(0.7,0.2,0.9,0.4);
    leg_delta_phi_upsilon1s_bx2->AddEntry(h_delta_phi_upsilon1s_bx2_pythia,"Pythia","l");
    leg_delta_phi_upsilon1s_bx2->SetTextFont(132);
    leg_delta_phi_upsilon1s_bx2->Draw();

    c_delta_phi_upsilon1s_bx2->Print("plots_kinematics_Y1S/c_delta_phi_upsilon1s_bx2.pdf");


    // angles
    TCanvas* c_delta_phi_upsilon1s_bx = new TCanvas("c_delta_phi_upsilon1s_bx","c_delta_phi_upsilon1s_bx");
    TH1D* h_delta_phi_upsilon1s_bx_pythia  = new TH1D("h_delta_phi_upsilon1s_bx_pythia", "h_delta_phi_upsilon1s_bx_pythia", 50,0,TMath::Pi());

    pythia_tree->Draw("angle_upsilon1s_bx>>h_delta_phi_upsilon1s_bx_pythia",  cuts_pythia +cuts_all);

    h_delta_phi_upsilon1s_bx_pythia->Scale(scalefactor_pythia);
    h_delta_phi_upsilon1s_bx_pythia->SetLineColor(kRed);

    h_delta_phi_upsilon1s_bx_pythia->SetTitle(";#Delta#phi(#Upsilon(1S), X_{b}) [rad]; d#sigma/d#Delta#phi");
    h_delta_phi_upsilon1s_bx_pythia->SetMinimum(0.0);
    h_delta_phi_upsilon1s_bx_pythia->Draw("hist");


    TLegend* leg_delta_phi_upsilon1s_bx = new TLegend(0.7,0.2,0.9,0.4);
    leg_delta_phi_upsilon1s_bx->AddEntry(h_delta_phi_upsilon1s_bx_pythia,"Pythia","l");
    leg_delta_phi_upsilon1s_bx->SetTextFont(132);
    leg_delta_phi_upsilon1s_bx->Draw();
    c_delta_phi_upsilon1s_bx->Print("plots_kinematics_Y1S/c_delta_phi_upsilon1s_bx.pdf");



    // angles
    TCanvas* c_delta_y_upsilon1s_bx = new TCanvas("c_delta_y_upsilon1s_bx","c_delta_y_upsilon1s_bx");
    TH1D* h_delta_y_upsilon1s_bx_pythia  = new TH1D("h_delta_y_upsilon1s_bx_pythia", "h_delta_y_upsilon1s_bx_pythia", 50,-5.0,5.0);

    pythia_tree->Draw("delta_y_upsilon1s_bx>>h_delta_y_upsilon1s_bx_pythia",  cuts_pythia +cuts_all);

    h_delta_y_upsilon1s_bx_pythia->Scale(scalefactor_pythia);
    h_delta_y_upsilon1s_bx_pythia->Scale(1.0/h_delta_y_upsilon1s_bx_pythia->Integral());
    h_delta_y_upsilon1s_bx_pythia->SetLineColor(kRed);

    h_delta_y_upsilon1s_bx_pythia->Draw("hist same");


    TLegend* leg_delta_eta_upsilon1s_bx = new TLegend(0.7,0.6,0.9,0.8);
    leg_delta_eta_upsilon1s_bx->AddEntry(h_delta_y_upsilon1s_bx_pythia,"Pythia","l");
    leg_delta_eta_upsilon1s_bx->SetTextFont(132);
    leg_delta_eta_upsilon1s_bx->Draw();
    c_delta_y_upsilon1s_bx->Print("plots_kinematics_Y1S/c_delta_y_upsilon1s_bx.pdf");
    // -------------------------------------------------------------------------
    // Plot 2D angles integrated over all eta 
    // -------------------------------------------------------------------------


    SetLHCbStyle("cont");

    // angles
    TCanvas* c_delta_phi_pythia = new TCanvas("c_delta_phi_pythia","c_delta_phi_pythia");
    TH2D* h_delta_phi_pythia  = new TH2D("h_delta_phi_pythia", "h_delta_phi_pythia", 20,0,TMath::Pi(),20,0,TMath::Pi());

    pythia_tree->Draw("angle_upsilon1s_bx:angle_upsilon1s_bx2>>h_delta_phi_pythia",  cuts_pythia +cuts_all);

    h_delta_phi_pythia->Scale(scalefactor_pythia);
    h_delta_phi_pythia->SetTitle(";#Delta#phi(#Upsilon(1S), X_{b}) [rad]; #Delta#phi(#Upsilon(1S), X_{b}) [rad]");
    h_delta_phi_pythia->Draw("colz");
    c_delta_phi_pythia->Print("plots_kinematics_Y1S/c_delta_phi_pythia.pdf");
    


    // eta
    TCanvas* c_delta_eta_pythia = new TCanvas("c_delta_eta_pythia","c_delta_eta_pythia");
    TH2D* h_delta_eta_pythia  = new TH2D("h_delta_eta_pythia", "h_delta_eta_pythia", 20,-5.0,5.0,20,-5.0,5.0);

    pythia_tree->Draw("delta_eta_upsilon1s_bx:delta_eta_upsilon1s_bx2>>h_delta_eta_pythia",  cuts_pythia +cuts_all);

    h_delta_eta_pythia->Scale(scalefactor_pythia);
    h_delta_eta_pythia->SetTitle(";#Delta#eta(#Upsilon(1S), X_{b}) [rad]; #Delta#eta(#Upsilon(1S), X_{b}) [rad]");
    h_delta_eta_pythia->Draw("colz");
    c_delta_eta_pythia->Print("plots_kinematics_Y1S/c_delta_eta_pythia.pdf");
    

    // Delta R
    TCanvas* c_delta_R_pythia = new TCanvas("c_delta_R_pythia","c_delta_R_pythia");
    TH2D* h_delta_R_pythia  = new TH2D("h_delta_R_pythia", "h_delta_R_pythia", 20,0.0,10.0,20,0.0,10.0);

    pythia_tree->Draw("delta_R_upsilon1s_bx:delta_R_upsilon1s_bx2>>h_delta_R_pythia",  cuts_pythia +cuts_all);

    h_delta_R_pythia->Scale(scalefactor_pythia);
    h_delta_R_pythia->SetTitle(";#DeltaR(#Upsilon(1S), X_{b}) [rad]; #DeltaR(#Upsilon(1S), X_{b}) [rad]");
    h_delta_R_pythia->Draw("colz");
    c_delta_R_pythia->Print("plots_kinematics_Y1S/c_delta_R_pythia.pdf");
    
    
    // -------------------------------------------------------------------------
    // Plot 2D pT vs. y 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_y_pythia = new TCanvas("c_2D_pt_y_pythia","c_2D_pt_y_pythia");
    TH2D* h_2D_pt_y_pythia      = new TH2D("h_2D_pt_y_pythia",      "h_2D_pt_y_pythia",    20,-10,10,20,0,10);

    pythia_tree->Draw(    "upsilon1s_pt:upsilon1s_y>>h_2D_pt_y_pythia",    cuts_pythia +cuts_all    );


    h_2D_pt_y_pythia->SetTitle(";y;p_{T} [GeV/c^{2}]");
    h_2D_pt_y_pythia->Draw("colz");
    c_2D_pt_y_pythia->Print("plots_kinematics_Y1S/c_2D_pt_y_pythia.pdf");
    

    // Loop over the events and categorise them
    bool foundHardc; 
    bool hasbStatus23; 
    bool hasbStatus33; 
    bool hasbOniaStatus23; 
    bool hasbOniaStatus33; 
    int hasbStatus23_type;
    int hasbStatus33_type;
    int hasbStatus33_type2;
    int hasbStatus33_N;
    int hasbOniaStatus33_N;
    int nUpsilon1S;
    int nXb;

    pythia_tree->SetBranchAddress("foundHardc",        &foundHardc); 
    pythia_tree->SetBranchAddress("hasbStatus23",      &hasbStatus23); 
    pythia_tree->SetBranchAddress("hasbStatus33",      &hasbStatus33); 
    pythia_tree->SetBranchAddress("hasbOniaStatus23",  &hasbOniaStatus23); 
    pythia_tree->SetBranchAddress("hasbOniaStatus33",  &hasbOniaStatus33); 
    pythia_tree->SetBranchAddress("hasbStatus23_type", &hasbStatus23_type );
    pythia_tree->SetBranchAddress("hasbStatus33_type", &hasbStatus33_type );
    pythia_tree->SetBranchAddress("hasbStatus33_type2",&hasbStatus33_type2 );
    pythia_tree->SetBranchAddress("hasbStatus33_N",    &hasbStatus33_N );
    pythia_tree->SetBranchAddress("nUpsilon1S",             &nUpsilon1S );
    pythia_tree->SetBranchAddress("nXb",               &nXb );
    pythia_tree->SetBranchAddress("hasbOniaStatus33_N",&hasbOniaStatus33_N);


    // Add kinematic distributions
    double upsilon1s_pe, upsilon1s_px, upsilon1s_py, upsilon1s_pz;
    double bx_pe,   bx_px,   bx_py,   bx_pz;
    double bx2_pe,  bx2_px,  bx2_py,  bx2_pz;

    pythia_tree->SetBranchAddress("upsilon1s_px",        &upsilon1s_px); 
    pythia_tree->SetBranchAddress("upsilon1s_py",        &upsilon1s_py); 
    pythia_tree->SetBranchAddress("upsilon1s_pz",        &upsilon1s_pz); 
    pythia_tree->SetBranchAddress("upsilon1s_pe",        &upsilon1s_pe); 
    pythia_tree->SetBranchAddress("bx_px",          &bx_px); 
    pythia_tree->SetBranchAddress("bx_py",          &bx_py); 
    pythia_tree->SetBranchAddress("bx_pz",          &bx_pz); 
    pythia_tree->SetBranchAddress("bx_pe",          &bx_pe);
    pythia_tree->SetBranchAddress("bx2_px",         &bx2_px); 
    pythia_tree->SetBranchAddress("bx2_py",         &bx2_py); 
    pythia_tree->SetBranchAddress("bx2_pz",         &bx2_pz); 
    pythia_tree->SetBranchAddress("bx2_pe",         &bx2_pe); 
    bool Upsilon1S_isMixedLine;
    pythia_tree->SetBranchAddress("Upsilon1S_isMixedLine",         &Upsilon1S_isMixedLine); 

    std::map<std::string,int> n_processes; 
    std::map<std::string,TH2D*> h_processes; 


    std::map<std::string,TH2D*> h_processes_pt_eta; 

    int n_bins = 10;
    int n_bins_kin = 20;

    for(int i = 0; i< n_entries_pythia; i++){
        pythia_tree->GetEntry(i);
        if(nUpsilon1S==1&&nXb==2){
            // TLotentzVector upsilon1s_vec( upsilon1s_px,upsilon1s_py,upsilon1s_pz,upsilon1s_pe);
            // TLotentzVector bx_vec(   bx_px,  bx_py,  bx_pz,  bx_pe  );
            // TLotentzVector bx2_vec(  bx2_px, bx2_py, bx2_pz, bx2_pe );

            double upsilon1s_pt = sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py);
            double bx2_pt  = sqrt(bx2_px *bx2_px +bx2_py *bx2_py );
            double bx_pt   = sqrt(bx_px  *bx_px  +bx_py  *bx_py  ); 

            double angle_bx2_bx   = acos((bx2_px*bx_px   + bx2_py*bx_py  )/(bx2_pt*bx_pt  ));
            double angle_upsilon1s_bx  = acos((upsilon1s_px*bx_px  + upsilon1s_py*bx_py )/(upsilon1s_pt*bx_pt ));
            double angle_upsilon1s_bx2 = acos((upsilon1s_px*bx2_px + upsilon1s_py*bx2_py)/(upsilon1s_pt*bx2_pt));
    
            double upsilon1s_p = sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz);

            double upsilon1s_eta = (upsilon1s_pz>0.0?-1.0:1.0)*acosh(upsilon1s_p/upsilon1s_pt);

            std::vector<std::string> processes;
            std::string name;

            int n_accounted_for = hasbStatus23 + hasbStatus33_N + hasbOniaStatus23 + hasbOniaStatus33_N;

            if(hasbStatus23){
                
                name = "Hard";
                if(hasbStatus23_type==5){
                    name += "_bb";
                // } else if(hasbStatus23_type==21){
                //     name += " cg";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);
            }

            if(hasbOniaStatus23){
                name = "Hard_Onia";
                processes.push_back(name);
            }

            if(hasbStatus33_N == 1){
                name = "MPI";
                if(hasbStatus33_type==5){
                    name += "_bb";
                // } else if(hasbStatus33_type==21){
                //     name += " cg";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);

            }
            if(hasbStatus33_N == 2){
                name = "MPI1";
                if(hasbStatus33_type==5){
                    name += "_bb";
                // } else if(hasbStatus33_type==21){
                //     name += " cg";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);

                
                name = "MPI2";
                if(hasbStatus33_type2==5){
                    name += "_bb";
                // } else if(hasbStatus33_type2==21){
                //     name += " cg";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);


            }
            if(hasbOniaStatus33_N == 1){
                name =  "MPI_Onia";
                processes.push_back(name);

            }
            if(hasbOniaStatus33_N == 2){
                name =  "MPI1_Onia";
                processes.push_back(name);

                name =  "MPI2_Onia";
                processes.push_back(name);

            }

            if(n_accounted_for==1){
                name =  "Showerb";
                processes.push_back(name);


            }

            if(n_accounted_for==0){
                name =  "Showerb";
                processes.push_back(name);

                name =  "Showerb";
                processes.push_back(name);

            }

            std::string cat_name;
            if(n_accounted_for <3 && processes.size()==2){
                cat_name = processes[0]+"_"+ processes[1];
            } else if(n_accounted_for >=3){
                cat_name = "Bad_3";
            } else {
                cat_name = "Bad_Other";
            }
            
            if(Upsilon1S_isMixedLine){
                cat_name += "_Mix";
            } else {
                cat_name += "_Sep";
            }

            if(h_processes.count(cat_name)==0){
                h_processes[cat_name] = new TH2D(Form("h_%s",cat_name.c_str()), 
                                                 Form("h_%s;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{b}) [rad]",cat_name.c_str()),
                                                 n_bins,0,TMath::Pi(),
                                                 n_bins,0,TMath::Pi() );
                h_processes_pt_eta[cat_name] = new TH2D(Form("h_%s_pt_eta",cat_name.c_str()), 
                                                 Form("h_%s_pt_eta;#eta;p_{T} [GeV/c^2]",cat_name.c_str()),
                                                 n_bins_kin,-10.0,10.0,
                                                 n_bins_kin,0,15.0 );

                
            }
            
            h_processes[cat_name]->Fill(angle_upsilon1s_bx,angle_upsilon1s_bx2);
            h_processes_pt_eta[cat_name]->Fill(upsilon1s_eta,upsilon1s_pt);
            n_processes[cat_name]++;
        }
    }

    for(const auto & n_proc: n_processes){
        std::cout << std::setw(25) << n_proc.first  << " | ";
        std::cout << std::setw(10) << n_proc.second << " | ";
        std::cout << std::endl;
    }   

    // Add plots together
    std::map<std::string,bool> isSPS;

    isSPS["Hard_Onia_Showerb_Mix"]   = true;
    isSPS["Hard_bb_Showerb_Mix"]     = true;
    isSPS["Hard_bx_Showerb_Mix"]     = true;
    isSPS["MPI_Onia_Showerb_Mix"]    = true;
    isSPS["MPI_bb_Showerb_Mix"]      = true;
    isSPS["MPI_bx_Showerb_Mix"]      = true;
    isSPS["Showerb_Showerb_Mix"]     = true;


    isSPS["Hard_Onia_MPI_Onia_Mix"]  = false;
    isSPS["Hard_Onia_MPI_bb_Mix"]    = false;
    isSPS["Hard_Onia_MPI_bx_Mix"]    = false;
    
    isSPS["Hard_bb_MPI_Onia_Mix"]    = false;
    isSPS["Hard_bb_MPI_bb_Mix"]      = false;
    isSPS["Hard_bb_MPI_bx_Mix"]      = false;
    
    isSPS["Hard_bx_Hard_Onia_Mix"]   = false;
    isSPS["Hard_bx_MPI_Onia_Mix"]    = false;
    isSPS["Hard_bx_MPI_bb_Mix"]      = false;
    isSPS["Hard_bx_MPI_bx_Mix"]      = false;
    
    isSPS["MPI1_Onia_MPI2_Onia_Mix"] = false;
    isSPS["MPI1_bb_MPI2_bb_Mix"]     = false;
    isSPS["MPI1_bb_MPI2_bx_Mix"]     = false;
    isSPS["MPI1_bx_MPI2_bb_Mix"]     = false;
    isSPS["MPI1_bx_MPI2_bx_Mix"]     = false;
    
    isSPS["MPI_bb_MPI_Onia_Mix"]     = false;
    isSPS["MPI_bx_MPI_Onia_Mix"]     = false; // possibly if x = onia
    


    isSPS["Hard_Onia_Showerb_Sep"]   = true;
    isSPS["Hard_bb_Showerb_Sep"]     = true;
    isSPS["Hard_bx_Showerb_Sep"]     = true;
    isSPS["MPI_Onia_Showerb_Sep"]    = true;
    isSPS["MPI_bb_Showerb_Sep"]      = true;
    isSPS["MPI_bx_Showerb_Sep"]      = true;
    isSPS["Showerb_Showerb_Sep"]     = true;

    isSPS["Hard_Onia_MPI_Onia_Sep"]  = false;
    isSPS["Hard_Onia_MPI_bb_Sep"]    = false;
    isSPS["Hard_Onia_MPI_bx_Sep"]    = false;
    
    isSPS["Hard_bb_MPI_Onia_Sep"]    = false;
    isSPS["Hard_bb_MPI_bb_Sep"]      = false;
    isSPS["Hard_bb_MPI_bx_Sep"]      = false;
    
    isSPS["Hard_bx_Hard_Onia_Sep"]   = false;
    isSPS["Hard_bx_MPI_Onia_Sep"]    = false;
    isSPS["Hard_bx_MPI_bb_Sep"]      = false;
    isSPS["Hard_bx_MPI_bx_Sep"]      = false;
    
    isSPS["MPI1_Onia_MPI2_Onia_Sep"] = false;
    isSPS["MPI1_bb_MPI2_bb_Sep"]     = false;
    isSPS["MPI1_bb_MPI2_bx_Sep"]     = false;
    isSPS["MPI1_bx_MPI2_bb_Sep"]     = false;
    isSPS["MPI1_bx_MPI2_bx_Sep"]     = false;
    
    isSPS["MPI_bb_MPI_Onia_Sep"]     = false;
    isSPS["MPI_bx_MPI_Onia_Sep"]     = false; // possibly if x = onia
    

    TH2D* h_processes_pt_eta_SPS_Mix = new TH2D("h_SPS_Mix_pt_eta", 
                                     "h_SPS_Mix_pt_eta;#eta;p_{T} [GeV/c^2]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );


    TH2D* h_processes_pt_eta_MPI_Mix = new TH2D("h_MPI_Mix_pt_eta", 
                                     "h_MPI_Mix_pt_eta;#eta;p_{T} [GeV/c^2]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );

    TH2D* h_processes_pt_eta_SPS_Sep = new TH2D("h_SPS_Sep_pt_eta", 
                                     "h_SPS_Sep_pt_eta;#eta;p_{T} [GeV/c^2]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );


    TH2D* h_processes_pt_eta_MPI_Sep = new TH2D("h_MPI_Sep_pt_eta", 
                                     "h_MPI_Sep_pt_eta;#eta;p_{T} [GeV/c^2]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );

    TH2D* h_processes_SPS_Mix = new TH2D("h_SPS_Mix", 
                                    "h_SPS_Mix;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{b}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_MPI_Mix = new TH2D("h_MPI_Mix", 
                                    "h_MPI_Mix;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{b}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_SPS_Sep = new TH2D("h_SPS_Sep", 
                                    "h_SPS_Sep;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{b}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_MPI_Sep = new TH2D("h_MPI_Sep", 
                                    "h_MPI_Sep;#Delta#phi(#Upsilon(1S), X_{b}) [rad];#Delta#phi(#Upsilon(1S), X_{b}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    for(const auto & h_proc: h_processes_pt_eta){
        if(isSPS.count(h_proc.first)){
            if(h_proc.first.find("_Mix")!=std::string::npos){
                if(isSPS[h_proc.first]){
                    h_processes_pt_eta_SPS_Mix->Add(h_processes_pt_eta_SPS_Mix,h_proc.second);
                } else {
                    h_processes_pt_eta_MPI_Mix->Add(h_processes_pt_eta_MPI_Mix,h_proc.second);
                } 
            }else{

                if(isSPS[h_proc.first]){
                    h_processes_pt_eta_SPS_Sep->Add(h_processes_pt_eta_SPS_Sep,h_proc.second);
                } else {
                    h_processes_pt_eta_MPI_Sep->Add(h_processes_pt_eta_MPI_Sep,h_proc.second);
                } 
            }
        } else {
            std::cout << "Cat not SPS or MPI " << h_proc.first << std::endl;
        }
    }
    SetLHCbStyle("cont");


    h_processes_pt_eta_SPS_Mix->SetMinimum(0);
    h_processes_pt_eta_MPI_Mix->SetMinimum(0);
    h_processes_pt_eta_SPS_Sep->SetMinimum(0);
    h_processes_pt_eta_MPI_Sep->SetMinimum(0);


    TCanvas* can_pt_eta_SPS_Mix = new TCanvas("can_pt_eta_SPS_Mix","can_pt_eta_SPS_Mix");
    h_processes_pt_eta_SPS_Mix->Draw("colz");
    can_pt_eta_SPS_Mix->Print("plots_kinematics_Y1S/can_pt_eta_SPS_Mix.pdf");
    
    TCanvas* can_pt_eta_MPI_Mix = new TCanvas("can_pt_eta_MPI_Mix","can_pt_eta_MPI_Mix");
    h_processes_pt_eta_MPI_Mix->Draw("colz");
    can_pt_eta_MPI_Mix->Print("plots_kinematics_Y1S/can_pt_eta_MPI_Mix.pdf");


    TCanvas* can_pt_eta_SPS_Sep = new TCanvas("can_pt_eta_SPS_Sep","can_pt_eta_SPS_Sep");
    h_processes_pt_eta_SPS_Sep->Draw("colz");
    can_pt_eta_SPS_Sep->Print("plots_kinematics_Y1S/can_pt_eta_SPS_Sep.pdf");
    
    TCanvas* can_pt_eta_MPI_Sep = new TCanvas("can_pt_eta_MPI_Sep","can_pt_eta_MPI_Sep");
    h_processes_pt_eta_MPI_Sep->Draw("colz");
    can_pt_eta_MPI_Sep->Print("plots_kinematics_Y1S/can_pt_eta_MPI_Sep.pdf");


    
    for(const auto & h_proc: h_processes){
        if(isSPS.count(h_proc.first)){
            if(h_proc.first.find("_Mix")!=std::string::npos){
                if(isSPS[h_proc.first]){
                    h_processes_SPS_Mix->Add(h_processes_SPS_Mix,h_proc.second);
                } else {
                    h_processes_MPI_Mix->Add(h_processes_MPI_Mix,h_proc.second);
                }  
            } else {
                if(isSPS[h_proc.first]){
                    h_processes_SPS_Sep->Add(h_processes_SPS_Sep,h_proc.second);
                } else {
                    h_processes_MPI_Sep->Add(h_processes_MPI_Sep,h_proc.second);
                }
            } 
        } else {
            std::cout << "Cat not SPS or MPI " << h_proc.first << std::endl;
        }
    }
    SetLHCbStyle("cont");

    h_processes_SPS_Mix->SetMinimum(0);
    h_processes_MPI_Mix->SetMinimum(0);
    h_processes_SPS_Sep->SetMinimum(0);
    h_processes_MPI_Sep->SetMinimum(0);

    TCanvas* can_SPS_Mix = new TCanvas("can_SPS_Mix","can_SPS_Mix");
    h_processes_SPS_Mix->Draw("colz");
    can_SPS_Mix->Print("plots_kinematics_Y1S/can_SPS_Mix.pdf");
    
    TCanvas* can_MPI_Mix = new TCanvas("can_MPI_Mix","can_MPI_Mix");
    h_processes_MPI_Mix->Draw("colz");
    can_MPI_Mix->Print("plots_kinematics_Y1S/can_MPI_Mix.pdf");

    TCanvas* can_SPS_Sep = new TCanvas("can_SPS_Sep","can_SPS_Sep");
    h_processes_SPS_Sep->Draw("colz");
    can_SPS_Sep->Print("plots_kinematics_Y1S/can_SPS_Sep.pdf");
    
    TCanvas* can_MPI_Sep = new TCanvas("can_MPI_Sep","can_MPI_Sep");
    h_processes_MPI_Sep->Draw("colz");
    can_MPI_Sep->Print("plots_kinematics_Y1S/can_MPI_Sep.pdf");


    // Draw angles
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s",name.c_str()),Form("can_%s",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics_Y1S/can_%s.pdf",name.c_str()));

    }

    // Draw pt/eta
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes_pt_eta){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s_pt_eta",name.c_str()),Form("can_%s_pt_eta",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics_Y1S/can_%s_pt_eta.pdf",name.c_str()));

    }
}