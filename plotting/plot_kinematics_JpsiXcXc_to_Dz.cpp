#include "tools.h"

void plot_kinematics_JpsiXcXc_to_Dz(){
    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;


    double BcVegPy_crosssec_val = 113830880.77856530      * nanobarn; // nb
    double BcVegPy_crosssec_err =      4772.3452537976009 * nanobarn; // nb
    
    double theory_Br_Bc2JpsiPi = 0.33*1e-2;
    double theory_Br_Bu2JpsiK  = 0.106*1e-2;


    // std::string filename_pythia_cccc     = "../output/from_lxplus/Complete_paper_samples/Sample_cccc/main202output_SoftQCD_nd_UserHook_cc_Scale_1.500000_Quark_4_N_20000000_PartMod1_noColRec.root";
    // std::string filename_pythia_cc       = "../output/from_lxplus/Complete_paper_samples/Sample_cc/main202output_SoftQCD_nd_UserHook_Minni_Scale_1.500000_Quark_4_N_10000000_PartMod1_noColRec.root";

    std::string filename_pythia_cccc     = "../output/from_lxplus/Complete_paper_samples/Sample_cccc/main202output_SoftQCD_nd_UserHook_cc_Scale_1.500000_Quark_4_N_20000000_PartMod1_withColRec.root";
    std::string filename_pythia_cc       = "../output/from_lxplus/Complete_paper_samples/Sample_cc/main202output_SoftQCD_nd_UserHook_Minni_Scale_1.500000_Quark_4_N_2000000_PartMod1_withColRec.root";


    // -------------------------------------------------------------------------
    // Fig cuts
    // -------------------------------------------------------------------------

    // TCut Jpsi_fid = "jpsi_y > 2.0 && jpsi_y < 4.5";
    TCut Jpsi_fid = "";

    // TCut Dz_fid = "Dp_p_y > 2.0 && Dp_p_y < 4.5";
    TCut Dz_fid = "";

    TCut cuts_JpsiXcXc_all    = "nJpsi>0 && nXc==2 && jpsi_pt>0";
    TCut cuts_Dz_all          = "nDp>=1";


    TCut cut_DPS_Mix  = " Jpsi_isMixedLine && Jpsi_iscc && Jpsi_iscc_c_partSys!=Jpsi_iscc_cbar_partSys ";
    TCut cut_SPS_Mix =  " Jpsi_isMixedLine && Jpsi_iscc && Jpsi_iscc_c_partSys==Jpsi_iscc_cbar_partSys";
  
    TCut cut_DPS_Sep  = "!Jpsi_isMixedLine && (((Jpsi_isHard||Jpsi_isMPI)&&Jpsi_isHard_partSys!=dx_partSys)||(Jpsi_iscc && Jpsi_iscc_c_partSys!=Jpsi_iscc_cbar_partSys&&Jpsi_iscc_c_partSys!=dx_partSys))"; 
    TCut cut_SPS_Sep = " !Jpsi_isMixedLine && (((Jpsi_isHard||Jpsi_isMPI)&&Jpsi_isHard_partSys==dx_partSys)||(Jpsi_iscc && Jpsi_iscc_c_partSys==Jpsi_iscc_cbar_partSys&&Jpsi_iscc_c_partSys==dx_partSys))";

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file_all = TFile::Open(filename_pythia_cc.c_str());
    TTree* pythia_tree_all = (TTree*) pythia_file_all->Get("events");

    pythia_tree_all->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree_all->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_all->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree_all->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree_all->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    pythia_tree_all->SetAlias("angle_jpsi_dx2","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");


    pythia_tree_all->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    pythia_tree_all->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree_all->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    pythia_tree_all->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    pythia_tree_all->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree_all->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_all->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree_all->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree_all->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree_all->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_all->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree_all->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree_all->SetAlias("Dp_p_p",  "sqrt(Dp_p_px*Dp_p_px+Dp_p_py*Dp_p_py+Dp_p_pz*Dp_p_pz)");
    pythia_tree_all->SetAlias("Dp_p_pt", "sqrt(Dp_p_px*Dp_p_px+Dp_p_py*Dp_p_py)");
    pythia_tree_all->SetAlias("Dp_p_eta","(Dp_p_pz>0?-1:1)*acosh(Dp_p_p/Dp_p_pt)");
    pythia_tree_all->SetAlias("Dp_p_y","0.5*log( (Dp_p_pe+Dp_p_pz) / (Dp_p_pe-Dp_p_pz) )");

    pythia_tree_all->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree_all->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    pythia_tree_all->SetAlias("delta_eta_jpsi_dx2","jpsi_eta-dx2_eta");

    pythia_tree_all->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree_all->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    pythia_tree_all->SetAlias("delta_y_jpsi_dx2","jpsi_y-dx2_y");

    pythia_tree_all->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree_all->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    pythia_tree_all->SetAlias("delta_R_jpsi_dx2","sqrt(delta_eta_jpsi_dx2*delta_eta_jpsi_dx2+angle_jpsi_dx2*angle_jpsi_dx2)");

    double sigmaGen_pythia_all;
    double sigmaErr_pythia_all;
    pythia_tree_all->SetBranchAddress("sigmaGen",&sigmaGen_pythia_all);
    pythia_tree_all->SetBranchAddress("sigmaErr",&sigmaErr_pythia_all);
    
    int n_entries_pythia_all = pythia_tree_all->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open(filename_pythia_cccc.c_str());
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");

    pythia_tree->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    pythia_tree->SetAlias("angle_jpsi_dx2","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");


    pythia_tree->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    pythia_tree->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    pythia_tree->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    pythia_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");

    pythia_tree->SetAlias("Dp_p_p",  "sqrt(Dp_p_px*Dp_p_px+Dp_p_py*Dp_p_py+Dp_p_pz*Dp_p_pz)");
    pythia_tree->SetAlias("Dp_p_pt", "sqrt(Dp_p_px*Dp_p_px+Dp_p_py*Dp_p_py)");
    pythia_tree->SetAlias("Dp_p_eta","(Dp_p_pz>0?-1:1)*acosh(Dp_p_p/Dp_p_pt)");
    pythia_tree->SetAlias("Dp_p_y","0.5*log( (Dp_p_pe+Dp_p_pz) / (Dp_p_pe-Dp_p_pz) )");

    pythia_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_jpsi_dx2","jpsi_eta-dx2_eta");

    pythia_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    pythia_tree->SetAlias("delta_y_jpsi_dx2","jpsi_y-dx2_y");

    pythia_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    pythia_tree->SetAlias("delta_R_jpsi_dx2","sqrt(delta_eta_jpsi_dx2*delta_eta_jpsi_dx2+angle_jpsi_dx2*angle_jpsi_dx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    pythia_tree_all->GetEntry(pythia_tree_all->GetEntries()-1);
 
    std::cout << "Cross section Pythia:        " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nJpsi>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
   
    std::cout << "Cross section Pythia all:  " << sigmaGen_pythia_all*microbarn  << " +/- " << sigmaErr_pythia_all*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree_all->GetEntries("nJpsi>0")<< "/" << pythia_tree_all->GetEntries("")<< std::endl;
    

    double scalefactor_pythia     = sigmaGen_pythia*microbarn/n_entries_pythia;
    double scalefactor_pythia_all = sigmaGen_pythia_all*microbarn/n_entries_pythia_all;


    // // -------------------------------------------------------------------------
    // // Plot multiplicity
    // // -------------------------------------------------------------------------
    // TCanvas* c_multiplicity = new TCanvas("c_multiplicity","c_multiplicity");
    // TH1D* h_multiplicity_pythia  = new TH1D("h_multiplicity_pythia", "h_multiplicity_pythia", 20,0,100);
    // TH1D* h_multiplicity_pythia_DPS  = new TH1D("h_multiplicity_pythia_DPS", "h_multiplicity_pythia_DPS", 20,0,100);
    // TH1D* h_multiplicity_pythia_SPS  = new TH1D("h_multiplicity_pythia_SPS", "h_multiplicity_pythia_SPS", 20,0,100);

    // TH1D* h_multiplicity_pythia_Dz  = new TH1D("h_multiplicity_pythia_Dz", "h_multiplicity_pythia_Dz", 20,0,100);

    // pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia",cuts_JpsiXcXc_all+Jpsi_fid);
    // pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_DPS",cuts_JpsiXcXc_all+Jpsi_fid+cuts_DPS);
    // pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_SPS",cuts_JpsiXcXc_all+Jpsi_fid+cuts_SPS);
    // pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_Dz",cuts_Dz_all+Dz_fid);

    // h_multiplicity_pythia->Scale(scalefactor_pythia);
    // h_multiplicity_pythia_DPS->Scale(scalefactor_pythia);
    // h_multiplicity_pythia_SPS->Scale(scalefactor_pythia);
    // h_multiplicity_pythia_Dz->Scale(scalefactor_pythia);

    // // Black -> With MPI
    // h_multiplicity_pythia->SetLineColor(kBlack);

    // h_multiplicity_pythia_Dz->SetLineColor(kBlack);
    // h_multiplicity_pythia_Dz->SetLineStyle(kDashed);

    // h_multiplicity_pythia_DPS->SetLineColor(kRed);
    // h_multiplicity_pythia_SPS->SetLineColor(kBlue);

    // h_multiplicity_pythia->SetMaximum(1.05*max({h_multiplicity_pythia->GetMaximum(),
    //                                             h_multiplicity_pythia_Dz->GetMaximum(),
    //                                             h_multiplicity_pythia_DPS->GetMaximum(),
    //                                             h_multiplicity_pythia_SPS->GetMaximum()
    //                                   }));
    
    // double h_multiplicity_pythia_bin_width = h_multiplicity_pythia->GetXaxis()->GetBinWidth(2);
    // h_multiplicity_pythia->SetTitle(Form(";N_{Charged}^{2.0<#eta<4.5}; d#sigma/dN [%.1f #mub]",h_multiplicity_pythia_bin_width));
    // h_multiplicity_pythia->Draw("hist");
    // h_multiplicity_pythia_Dz->Draw("same hist");
    // h_multiplicity_pythia_DPS->Draw("same hist");
    // h_multiplicity_pythia_SPS->Draw("same hist");
    // gPad->SetLogy();

    // TLegend* leg_multiplicity = new TLegend(0.6,0.6,0.9,0.9);
    // leg_multiplicity->AddEntry(h_multiplicity_pythia_Dz,"D^{+} Pythia With MPI","l");
    // leg_multiplicity->AddEntry(h_multiplicity_pythia,"J/#psi Pythia With MPI","l");
    // leg_multiplicity->AddEntry(h_multiplicity_pythia_SPS,"J/#psi DPS Pythia With MPI","l");
    // leg_multiplicity->AddEntry(h_multiplicity_pythia_DPS,"J/#psi SPS Pythia With MPI","l");
    // leg_multiplicity->SetTextFont(132);
    // leg_multiplicity->SetFillStyle(0);
    // leg_multiplicity->Draw();
    // c_multiplicity->Print("plots_kinematics_JpsiXcXc_to_Dz/c_multiplicity.pdf");

    // TCanvas* c_multiplicity_ratio = new TCanvas("c_multiplicity_ratio","c_multiplicity_ratio");

    // TH1D* h_multiplicity_pythia_ratio  = new TH1D("h_multiplicity_pythia_ratio", "h_multiplicity_pythia_ratio", 20,0,100);
    // TH1D* h_multiplicity_pythia_SPS_ratio  = new TH1D("h_multiplicity_pythia_SPS_ratio", "h_multiplicity_pythia_SPS_ratio", 20,0,100);
    // TH1D* h_multiplicity_pythia_DPS_ratio  = new TH1D("h_multiplicity_pythia_DPS_ratio", "h_multiplicity_pythia_DPS_ratio", 20,0,100);

    // h_multiplicity_pythia_ratio->Divide(h_multiplicity_pythia,h_multiplicity_pythia_Dz);
    // h_multiplicity_pythia_DPS_ratio->Divide(h_multiplicity_pythia_DPS,h_multiplicity_pythia_Dz);
    // h_multiplicity_pythia_SPS_ratio->Divide(h_multiplicity_pythia_SPS,h_multiplicity_pythia_Dz);


    // h_multiplicity_pythia_ratio->Scale(100);
    // h_multiplicity_pythia_DPS_ratio->Scale(100);
    // h_multiplicity_pythia_SPS_ratio->Scale(100);
    
    //         // Black -> With MPI
    // h_multiplicity_pythia_ratio->SetLineColor(kBlack);


    // h_multiplicity_pythia_DPS_ratio->SetLineColor(kRed);
    // h_multiplicity_pythia_DPS_ratio->SetMarkerColor(kRed);
    // h_multiplicity_pythia_SPS_ratio->SetLineColor(kBlue);
    // h_multiplicity_pythia_SPS_ratio->SetMarkerColor(kBlue);

    // h_multiplicity_pythia_ratio->SetMaximum(1.05*max({
    //                                               h_multiplicity_pythia_ratio->GetMaximum(),
    //                                               h_multiplicity_pythia_DPS_ratio->GetMaximum(),
    //                                               h_multiplicity_pythia_SPS_ratio->GetMaximum()
    //                                   }));
    // h_multiplicity_pythia_ratio->SetTitle(";N_{Charged}^{2.0<#eta<4.5};#scale[0.6]{#frac{d#sigma(J/#psi)}{dN}}/#scale[0.6]{#frac{d#sigma(D^{+})}{dN}} (%)");
    // h_multiplicity_pythia_ratio->SetMinimum(0);
    // h_multiplicity_pythia_ratio->Draw();
    // h_multiplicity_pythia_DPS_ratio->Draw("same");
    // h_multiplicity_pythia_SPS_ratio->Draw("same");


    // TLegend* leg_multiplicity_ratio = new TLegend(0.5,0.4,0.8,0.7);
    // leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_ratio,"Pythia With MPI","pe");
    // leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_DPS_ratio,"DPS Pythia With MPI","pe");
    // leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_SPS_ratio,"SPS Pythia With MPI","pe");
    // leg_multiplicity_ratio->SetTextFont(132);
    // leg_multiplicity_ratio->SetFillStyle(0);
    // leg_multiplicity_ratio->Draw();

    // c_multiplicity_ratio->Print("plots_kinematics_JpsiXcXc_to_Dz/c_multiplicity_ratio.pdf");

    // -------------------------------------------------------------------------
    // Plot multiplicity
    // -------------------------------------------------------------------------
    TCanvas* c_nMPI = new TCanvas("c_nMPI","c_nMPI");
    TH1D* h_nMPI_pythia  = new TH1D("h_nMPI_pythia", "h_nMPI_pythia", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Mix  = new TH1D("h_nMPI_pythia_DPS_Mix", "h_nMPI_pythia_DPS_Mix", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Mix  = new TH1D("h_nMPI_pythia_SPS_Mix", "h_nMPI_pythia_SPS_Mix", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Sep  = new TH1D("h_nMPI_pythia_DPS_Sep", "h_nMPI_pythia_DPS_Sep", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Sep  = new TH1D("h_nMPI_pythia_SPS_Sep", "h_nMPI_pythia_SPS_Sep", 13,0,26);

    TH1D* h_nMPI_pythia_Dz  = new TH1D("h_nMPI_pythia_Dz", "h_nMPI_pythia_Dz", 13,0,26);

    pythia_tree->Draw("nMPI>>h_nMPI_pythia",cuts_JpsiXcXc_all+Jpsi_fid);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_DPS_Mix",cuts_JpsiXcXc_all+Jpsi_fid+cut_DPS_Mix);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_SPS_Mix",cuts_JpsiXcXc_all+Jpsi_fid+cut_SPS_Mix);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_DPS_Sep",cuts_JpsiXcXc_all+Jpsi_fid+cut_DPS_Sep);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_SPS_Sep",cuts_JpsiXcXc_all+Jpsi_fid+cut_SPS_Sep);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia",cuts_JpsiXcXc_all+Jpsi_fid);
    pythia_tree_all->Draw("nMPI>>h_nMPI_pythia_Dz",cuts_Dz_all+Dz_fid);

    h_nMPI_pythia->Scale(scalefactor_pythia);
    h_nMPI_pythia_DPS_Mix->Scale(scalefactor_pythia);
    h_nMPI_pythia_SPS_Mix->Scale(scalefactor_pythia);
    h_nMPI_pythia_DPS_Sep->Scale(scalefactor_pythia);
    h_nMPI_pythia_SPS_Sep->Scale(scalefactor_pythia);
    h_nMPI_pythia_Dz->Scale(scalefactor_pythia_all);


    // Black -> With MPI
    h_nMPI_pythia->SetLineColor(kBlack);

    h_nMPI_pythia_Dz->SetLineColor(kBlack);
    h_nMPI_pythia_Dz->SetLineStyle(kDashed);


    h_nMPI_pythia_DPS_Mix->SetLineColor(kBlue);
    h_nMPI_pythia_SPS_Mix->SetLineColor(kRed);

    h_nMPI_pythia_DPS_Sep->SetLineColor(kBlue-9);
    h_nMPI_pythia_SPS_Sep->SetLineColor(kRed-9);

    h_nMPI_pythia_DPS_Sep->SetLineStyle(kDotted);
    h_nMPI_pythia_SPS_Sep->SetLineStyle(kDotted);

    h_nMPI_pythia->SetMaximum(1.05*max({h_nMPI_pythia->GetMaximum(),
                                      h_nMPI_pythia_Dz->GetMaximum(),
                                      h_nMPI_pythia_DPS_Mix->GetMaximum(),
                                      h_nMPI_pythia_SPS_Mix->GetMaximum(),
                                      h_nMPI_pythia_DPS_Sep->GetMaximum(),
                                      h_nMPI_pythia_SPS_Sep->GetMaximum()
                                      }));
    
    double h_nMPI_pythia_bin_width = h_nMPI_pythia->GetXaxis()->GetBinWidth(2);
    h_nMPI_pythia->SetTitle(Form(";N_{MPI}; d#sigma/dN [%.1f #mub]",h_nMPI_pythia_bin_width));
    h_nMPI_pythia->Draw("hist");
    h_nMPI_pythia_Dz->Draw("same hist");
    h_nMPI_pythia_DPS_Mix->Draw("same hist");
    h_nMPI_pythia_SPS_Mix->Draw("same hist");
    h_nMPI_pythia_DPS_Sep->Draw("same hist");
    h_nMPI_pythia_SPS_Sep->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_nMPI = new TLegend(0.6,0.6,0.9,0.9);
    leg_nMPI->AddEntry(h_nMPI_pythia_Dz,     "D^{+} Pythia","l");
    leg_nMPI->AddEntry(h_nMPI_pythia,        "J/#psiX_{c}X_{#bar{c}} Pythia","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_DPS_Mix,"J/#psiX_{c}X_{#bar{c}} DPS Mixed","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_DPS_Sep,"J/#psiX_{c}X_{#bar{c}} DPS Unmixed","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_SPS_Mix,"J/#psiX_{c}X_{#bar{c}} SPS Mixed","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_SPS_Sep,"J/#psiX_{c}X_{#bar{c}} SPS Unmixed","l");

    leg_nMPI->SetTextFont(132);
    leg_nMPI->SetFillStyle(0);
    leg_nMPI->Draw();
    c_nMPI->Print("plots_kinematics_JpsiXcXc_to_Dz/c_nMPI.pdf");

    TCanvas* c_nMPI_ratio = new TCanvas("c_nMPI_ratio","c_nMPI_ratio");

    TH1D* h_nMPI_pythia_ratio  = new TH1D("h_nMPI_pythia_ratio", "h_nMPI_pythia_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Mix_ratio  = new TH1D("h_nMPI_pythia_DPS_Mix_ratio", "h_nMPI_pythia_DPS_Mix_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Mix_ratio  = new TH1D("h_nMPI_pythia_SPS_Mix_ratio", "h_nMPI_pythia_SPS_Mix_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Sep_ratio  = new TH1D("h_nMPI_pythia_DPS_Sep_ratio", "h_nMPI_pythia_DPS_Sep_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Sep_ratio  = new TH1D("h_nMPI_pythia_SPS_Sep_ratio", "h_nMPI_pythia_SPS_Sep_ratio", 13,0,26);

    h_nMPI_pythia_ratio->Divide(h_nMPI_pythia,h_nMPI_pythia_Dz);
    h_nMPI_pythia_DPS_Mix_ratio->Divide(h_nMPI_pythia_DPS_Mix,h_nMPI_pythia_Dz);
    h_nMPI_pythia_SPS_Mix_ratio->Divide(h_nMPI_pythia_SPS_Mix,h_nMPI_pythia_Dz);
    h_nMPI_pythia_DPS_Sep_ratio->Divide(h_nMPI_pythia_DPS_Sep,h_nMPI_pythia_Dz);
    h_nMPI_pythia_SPS_Sep_ratio->Divide(h_nMPI_pythia_SPS_Sep,h_nMPI_pythia_Dz);


    h_nMPI_pythia_ratio->Scale(100);
    h_nMPI_pythia_DPS_Mix_ratio->Scale(100);
    h_nMPI_pythia_SPS_Mix_ratio->Scale(100);
    h_nMPI_pythia_DPS_Sep_ratio->Scale(100);
    h_nMPI_pythia_SPS_Sep_ratio->Scale(100);
    
        // Black -> With MPI
    h_nMPI_pythia_ratio->SetLineColor(kBlack);


    h_nMPI_pythia_DPS_Mix_ratio->SetLineColor(kBlue);
    h_nMPI_pythia_DPS_Mix_ratio->SetMarkerColor(kBlue);

    h_nMPI_pythia_SPS_Mix_ratio->SetLineColor(kRed);
    h_nMPI_pythia_SPS_Mix_ratio->SetMarkerColor(kRed);

    h_nMPI_pythia_DPS_Sep_ratio->SetLineColor(kBlue-9);
    h_nMPI_pythia_DPS_Sep_ratio->SetMarkerColor(kBlue-9);

    h_nMPI_pythia_SPS_Sep_ratio->SetLineColor(kRed-9);
    h_nMPI_pythia_SPS_Sep_ratio->SetMarkerColor(kRed-9);
    h_nMPI_pythia_DPS_Sep_ratio->SetLineStyle(kDotted);
    h_nMPI_pythia_SPS_Sep_ratio->SetLineStyle(kDotted);

    h_nMPI_pythia_ratio->SetMaximum(1.05*max({h_nMPI_pythia_ratio->GetMaximum(),
                                              h_nMPI_pythia_DPS_Mix_ratio->GetMaximum(),
                                              h_nMPI_pythia_SPS_Mix_ratio->GetMaximum(),
                                              h_nMPI_pythia_DPS_Sep_ratio->GetMaximum(),
                                              h_nMPI_pythia_SPS_Sep_ratio->GetMaximum()
                                      }));
    h_nMPI_pythia_ratio->SetTitle(";N_{MPI};#scale[0.6]{#frac{d#sigma(J/#psiX_{c}X_{#bar{c}})}{dN}}/#scale[0.6]{#frac{d#sigma(D^{+})}{dN}} (%)");
    h_nMPI_pythia_ratio->SetMinimum(0);
    h_nMPI_pythia_ratio->SetMaximum(1.1);
    h_nMPI_pythia_ratio->Draw();
    h_nMPI_pythia_DPS_Mix_ratio->Draw("same");
    h_nMPI_pythia_SPS_Mix_ratio->Draw("same");
    h_nMPI_pythia_DPS_Sep_ratio->Draw("same");
    h_nMPI_pythia_SPS_Sep_ratio->Draw("same");


    TLegend* leg_nMPI_ratio = new TLegend(0.13,0.58,0.71,0.93);
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_ratio,        "Pythia - All","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_DPS_Mix_ratio,"Pythia - Just DPS Mixed","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_DPS_Sep_ratio,"Pythia - Just DPS Unmixed","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_SPS_Mix_ratio,"Pythia - Just SPS Mixed","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_SPS_Sep_ratio,"Pythia - Just SPS Unmixed","pe");
    leg_nMPI_ratio->SetTextFont(132);
    leg_nMPI_ratio->SetFillStyle(0);
    leg_nMPI_ratio->Draw();
    c_nMPI_ratio->Print("plots_kinematics_JpsiXcXc_to_Dz/c_nMPI_ratio.pdf");



    // -------------------------------------------------------------------------
    // Plot multiplicity
    // -------------------------------------------------------------------------
    TCanvas* c_multiplicity = new TCanvas("c_multiplicity","c_multiplicity");
    TH1D* h_multiplicity_pythia  = new TH1D("h_multiplicity_pythia", "h_multiplicity_pythia", 20,0,80);
    TH1D* h_multiplicity_pythia_DPS_Mix  = new TH1D("h_multiplicity_pythia_DPS_Mix", "h_multiplicity_pythia_DPS_Mix", 20,0,80);
    TH1D* h_multiplicity_pythia_SPS_Mix  = new TH1D("h_multiplicity_pythia_SPS_Mix", "h_multiplicity_pythia_SPS_Mix", 20,0,80);
    TH1D* h_multiplicity_pythia_DPS_Sep  = new TH1D("h_multiplicity_pythia_DPS_Sep", "h_multiplicity_pythia_DPS_Sep", 20,0,80);
    TH1D* h_multiplicity_pythia_SPS_Sep  = new TH1D("h_multiplicity_pythia_SPS_Sep", "h_multiplicity_pythia_SPS_Sep", 20,0,80);

    TH1D* h_multiplicity_pythia_Dz  = new TH1D("h_multiplicity_pythia_Dz", "h_multiplicity_pythia_Dz", 20,0,80);

    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia",cuts_JpsiXcXc_all+Jpsi_fid);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_DPS_Mix",cuts_JpsiXcXc_all+Jpsi_fid+cut_DPS_Mix);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_SPS_Mix",cuts_JpsiXcXc_all+Jpsi_fid+cut_SPS_Mix);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_DPS_Sep",cuts_JpsiXcXc_all+Jpsi_fid+cut_DPS_Sep);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_SPS_Sep",cuts_JpsiXcXc_all+Jpsi_fid+cut_SPS_Sep);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia",cuts_JpsiXcXc_all+Jpsi_fid);
    pythia_tree_all->Draw("nChargedInLHCb>>h_multiplicity_pythia_Dz",cuts_Dz_all+Dz_fid);

    h_multiplicity_pythia->Scale(scalefactor_pythia);
    h_multiplicity_pythia_DPS_Mix->Scale(scalefactor_pythia);
    h_multiplicity_pythia_SPS_Mix->Scale(scalefactor_pythia);
    h_multiplicity_pythia_DPS_Sep->Scale(scalefactor_pythia);
    h_multiplicity_pythia_SPS_Sep->Scale(scalefactor_pythia);
    h_multiplicity_pythia_Dz->Scale(scalefactor_pythia_all);


    // Black -> With MPI
    h_multiplicity_pythia->SetLineColor(kBlack);

    h_multiplicity_pythia_Dz->SetLineColor(kBlack);
    h_multiplicity_pythia_Dz->SetLineStyle(kDashed);


    h_multiplicity_pythia_DPS_Mix->SetLineColor(kBlue);
    h_multiplicity_pythia_SPS_Mix->SetLineColor(kRed);

    h_multiplicity_pythia_DPS_Sep->SetLineColor(kBlue-9);
    h_multiplicity_pythia_SPS_Sep->SetLineColor(kRed-9);

    h_multiplicity_pythia_DPS_Sep->SetLineStyle(kDotted);
    h_multiplicity_pythia_SPS_Sep->SetLineStyle(kDotted);

    h_multiplicity_pythia->SetMaximum(1.05*max({h_multiplicity_pythia->GetMaximum(),
                                      h_multiplicity_pythia_Dz->GetMaximum(),
                                      h_multiplicity_pythia_DPS_Mix->GetMaximum(),
                                      h_multiplicity_pythia_SPS_Mix->GetMaximum(),
                                      h_multiplicity_pythia_DPS_Sep->GetMaximum(),
                                      h_multiplicity_pythia_SPS_Sep->GetMaximum()
                                      }));
    
    double h_multiplicity_pythia_bin_width = h_multiplicity_pythia->GetXaxis()->GetBinWidth(2);
    h_multiplicity_pythia->SetTitle(Form(";N_{Charged}^{2.0<#eta<4.5}; d#sigma/dN [%.1f #mub]",h_multiplicity_pythia_bin_width));
    h_multiplicity_pythia->Draw("hist");
    h_multiplicity_pythia_Dz->Draw("same hist");
    h_multiplicity_pythia_DPS_Mix->Draw("same hist");
    h_multiplicity_pythia_SPS_Mix->Draw("same hist");
    h_multiplicity_pythia_DPS_Sep->Draw("same hist");
    h_multiplicity_pythia_SPS_Sep->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_multiplicity = new TLegend(0.6,0.6,0.9,0.9);
    leg_multiplicity->AddEntry(h_multiplicity_pythia_Dz,     "D^{+} Pythia","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia,        "J/#psiX_{c}X_{#bar{c}} Pythia","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_DPS_Mix,"J/#psiX_{c}X_{#bar{c}} DPS Mixed","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_DPS_Sep,"J/#psiX_{c}X_{#bar{c}} DPS Unmixed","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_SPS_Mix,"J/#psiX_{c}X_{#bar{c}} SPS Mixed","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_SPS_Sep,"J/#psiX_{c}X_{#bar{c}} SPS Unmixed","l");

    leg_multiplicity->SetTextFont(132);
    leg_multiplicity->SetFillStyle(0);
    leg_multiplicity->Draw();
    c_multiplicity->Print("plots_kinematics_JpsiXcXc_to_Dz/c_multiplicity.pdf");

    TCanvas* c_multiplicity_ratio = new TCanvas("c_multiplicity_ratio","c_multiplicity_ratio");

    TH1D* h_multiplicity_pythia_ratio  = new TH1D("h_multiplicity_pythia_ratio", "h_multiplicity_pythia_ratio", 20,0,80);
    TH1D* h_multiplicity_pythia_DPS_Mix_ratio  = new TH1D("h_multiplicity_pythia_DPS_Mix_ratio", "h_multiplicity_pythia_DPS_Mix_ratio", 20,0,80);
    TH1D* h_multiplicity_pythia_SPS_Mix_ratio  = new TH1D("h_multiplicity_pythia_SPS_Mix_ratio", "h_multiplicity_pythia_SPS_Mix_ratio", 20,0,80);
    TH1D* h_multiplicity_pythia_DPS_Sep_ratio  = new TH1D("h_multiplicity_pythia_DPS_Sep_ratio", "h_multiplicity_pythia_DPS_Sep_ratio", 20,0,80);
    TH1D* h_multiplicity_pythia_SPS_Sep_ratio  = new TH1D("h_multiplicity_pythia_SPS_Sep_ratio", "h_multiplicity_pythia_SPS_Sep_ratio", 20,0,80);

    h_multiplicity_pythia_ratio->Divide(h_multiplicity_pythia,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_DPS_Mix_ratio->Divide(h_multiplicity_pythia_DPS_Mix,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_SPS_Mix_ratio->Divide(h_multiplicity_pythia_SPS_Mix,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_DPS_Sep_ratio->Divide(h_multiplicity_pythia_DPS_Sep,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_SPS_Sep_ratio->Divide(h_multiplicity_pythia_SPS_Sep,h_multiplicity_pythia_Dz);


    h_multiplicity_pythia_ratio->Scale(100);
    h_multiplicity_pythia_DPS_Mix_ratio->Scale(100);
    h_multiplicity_pythia_SPS_Mix_ratio->Scale(100);
    h_multiplicity_pythia_DPS_Sep_ratio->Scale(100);
    h_multiplicity_pythia_SPS_Sep_ratio->Scale(100);
    
        // Black -> With MPI
    h_multiplicity_pythia_ratio->SetLineColor(kBlack);


    h_multiplicity_pythia_DPS_Mix_ratio->SetLineColor(kBlue);
    h_multiplicity_pythia_DPS_Mix_ratio->SetMarkerColor(kBlue);

    h_multiplicity_pythia_SPS_Mix_ratio->SetLineColor(kRed);
    h_multiplicity_pythia_SPS_Mix_ratio->SetMarkerColor(kRed);

    h_multiplicity_pythia_DPS_Sep_ratio->SetLineColor(kBlue-9);
    h_multiplicity_pythia_DPS_Sep_ratio->SetMarkerColor(kBlue-9);

    h_multiplicity_pythia_SPS_Sep_ratio->SetLineColor(kRed-9);
    h_multiplicity_pythia_SPS_Sep_ratio->SetMarkerColor(kRed-9);
    h_multiplicity_pythia_DPS_Sep_ratio->SetLineStyle(kDotted);
    h_multiplicity_pythia_SPS_Sep_ratio->SetLineStyle(kDotted);

    h_multiplicity_pythia_ratio->SetMaximum(1.05*max({h_multiplicity_pythia_ratio->GetMaximum(),
                                              h_multiplicity_pythia_DPS_Mix_ratio->GetMaximum(),
                                              h_multiplicity_pythia_SPS_Mix_ratio->GetMaximum(),
                                              h_multiplicity_pythia_DPS_Sep_ratio->GetMaximum(),
                                              h_multiplicity_pythia_SPS_Sep_ratio->GetMaximum()
                                      }));
    h_multiplicity_pythia_ratio->SetTitle(";N_{Charged}^{2.0<#eta<4.5};#scale[0.6]{#frac{d#sigma(J/#psiX_{c}X_{#bar{c}})}{dN}}/#scale[0.6]{#frac{d#sigma(D^{+})}{dN}} (%)");
    h_multiplicity_pythia_ratio->SetMinimum(0);
    h_multiplicity_pythia_ratio->SetMaximum(1.0);
    h_multiplicity_pythia_ratio->Draw();
    h_multiplicity_pythia_DPS_Mix_ratio->Draw("same");
    h_multiplicity_pythia_SPS_Mix_ratio->Draw("same");
    h_multiplicity_pythia_DPS_Sep_ratio->Draw("same");
    h_multiplicity_pythia_SPS_Sep_ratio->Draw("same");


    TLegend* leg_multiplicity_ratio = new TLegend(0.13,0.58,0.71,0.93);
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_ratio,        "Pythia - All","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_DPS_Mix_ratio,"Pythia - Just DPS Mixed","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_DPS_Sep_ratio,"Pythia - Just DPS Unmixed","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_SPS_Mix_ratio,"Pythia - Just SPS Mixed","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_SPS_Sep_ratio,"Pythia - Just SPS Unmixed","pe");
    leg_multiplicity_ratio->SetTextFont(132);
    leg_multiplicity_ratio->SetFillStyle(0);
    leg_multiplicity_ratio->Draw();
    c_multiplicity_ratio->Print("plots_kinematics_JpsiXcXc_to_Dz/c_multiplicity_ratio.pdf");

    // // angles
    SetLHCbStyle("square");
    int n_bins = 10;

    TH2D* h_processes_delta_phi_2d_pythia_all = new TH2D("h_processes_delta_phi_2d_pythia_all", 
                                    "h_processes_delta_phi_2d_pythia_all;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );
    TH2D* h_processes_delta_phi_2d_pythia_SPS_Mix = new TH2D("h_processes_delta_phi_2d_pythia_SPS_Mix", 
                                    "h_processes_delta_phi_2d_pythia_SPS_Mix;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_delta_phi_2d_pythia_DPS_Mix = new TH2D("h_processes_delta_phi_2d_pythia_DPS_Mix", 
                                    "h_processes_delta_phi_2d_pythia_DPS_Mix;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );
    TH2D* h_processes_delta_phi_2d_pythia_SPS_Sep = new TH2D("h_processes_delta_phi_2d_pythia_SPS_Sep", 
                                    "h_processes_delta_phi_2d_pythia_SPS_Sep;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_delta_phi_2d_pythia_DPS_Sep = new TH2D("h_processes_delta_phi_2d_pythia_DPS_Sep", 
                                    "h_processes_delta_phi_2d_pythia_DPS_Sep;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );



    TCanvas* can_delta_phi_2d_pythia_all = new TCanvas("can_delta_phi_2d_pythia_all","can_delta_phi_2d_pythia_all",550,500);

    pythia_tree->Draw( "angle_jpsi_dx:angle_jpsi_dx2>>h_processes_delta_phi_2d_pythia_all",      cuts_JpsiXcXc_all+Jpsi_fid);
    pythia_tree->Draw( "angle_jpsi_dx:angle_jpsi_dx2>>h_processes_delta_phi_2d_pythia_DPS_Mix",  cuts_JpsiXcXc_all+Jpsi_fid+cut_DPS_Mix);
    pythia_tree->Draw( "angle_jpsi_dx:angle_jpsi_dx2>>h_processes_delta_phi_2d_pythia_SPS_Mix",  cuts_JpsiXcXc_all+Jpsi_fid+cut_SPS_Mix);
    pythia_tree->Draw( "angle_jpsi_dx:angle_jpsi_dx2>>h_processes_delta_phi_2d_pythia_DPS_Sep",  cuts_JpsiXcXc_all+Jpsi_fid+cut_DPS_Sep);
    pythia_tree->Draw( "angle_jpsi_dx:angle_jpsi_dx2>>h_processes_delta_phi_2d_pythia_SPS_Sep",  cuts_JpsiXcXc_all+Jpsi_fid+cut_SPS_Sep);

    h_processes_delta_phi_2d_pythia_all->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_DPS_Mix->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_SPS_Mix->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_DPS_Sep->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_SPS_Sep->SetMinimum(0);
    
    h_processes_delta_phi_2d_pythia_all->Draw("colz");
    can_delta_phi_2d_pythia_all->Print("plots_kinematics_JpsiXcXc_to_Dz/can_delta_phi_2d_pythia_all.pdf");

    TCanvas* can_delta_phi_2d_pythia_SPS_Mix = new TCanvas("can_delta_phi_2d_pythia_SPS_Mix","can_delta_phi_2d_pythia_SPS_Mix",550,500);
    h_processes_delta_phi_2d_pythia_SPS_Mix->Draw("colz");
    can_delta_phi_2d_pythia_SPS_Mix->Print("plots_kinematics_JpsiXcXc_to_Dz/can_delta_phi_2d_pythia_SPS_Mix.pdf");
    
    TCanvas* can_delta_phi_2d_pythia_DPS_Mix = new TCanvas("can_delta_phi_2d_pythia_DPS_Mix","can_delta_phi_2d_pythia_DPS_Mix",550,500);
    h_processes_delta_phi_2d_pythia_DPS_Mix->Draw("colz");
    can_delta_phi_2d_pythia_DPS_Mix->Print("plots_kinematics_JpsiXcXc_to_Dz/can_delta_phi_2d_pythia_DPS_Mix.pdf");

    TCanvas* can_delta_phi_2d_pythia_SPS_Sep = new TCanvas("can_delta_phi_2d_pythia_SPS_Sep","can_delta_phi_2d_pythia_SPS_Sep",550,500);
    h_processes_delta_phi_2d_pythia_SPS_Sep->Draw("colz");
    can_delta_phi_2d_pythia_SPS_Sep->Print("plots_kinematics_JpsiXcXc_to_Dz/can_delta_phi_2d_pythia_SPS_Sep.pdf");
    
    TCanvas* can_delta_phi_2d_pythia_DPS_Sep = new TCanvas("can_delta_phi_2d_pythia_DPS_Sep","can_delta_phi_2d_pythia_DPS_Sep",550,500);
    h_processes_delta_phi_2d_pythia_DPS_Sep->Draw("colz");
    can_delta_phi_2d_pythia_DPS_Sep->Print("plots_kinematics_JpsiXcXc_to_Dz/can_delta_phi_2d_pythia_DPS_Sep.pdf");


}

