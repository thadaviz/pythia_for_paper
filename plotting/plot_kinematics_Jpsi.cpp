#include "tools.h"

void plot_kinematics_Jpsi(){

    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_400000.root");
    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_200000_updateMixed.root");
    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_354000_updateMixed.root");
    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_400000_PartMod1.root");
    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_3920000_PartMod1.root");
    TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_9900000_PartMod1.root");
    // TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_9560000_PartMod1_noColRec.root");
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");


    pythia_tree->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    pythia_tree->SetAlias("angle_jpsi_dx2","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");


    pythia_tree->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    pythia_tree->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    pythia_tree->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    pythia_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_jpsi_bx","jpsi_eta-dx2_eta");

    pythia_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    pythia_tree->SetAlias("delta_y_jpsi_bx","jpsi_y-dx2_y");

    pythia_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    pythia_tree->SetAlias("delta_R_jpsi_bx","sqrt(delta_eta_jpsi_bx*delta_eta_jpsi_bx+angle_jpsi_dx2*angle_jpsi_dx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia nompi simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file_nompi = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_cc_N_10000000_noMPI_PartMod1.root");
    TTree* pythia_tree_nompi = (TTree*) pythia_file_nompi->Get("events");

    pythia_tree_nompi->SetAlias("jpsi_pt" , "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree_nompi->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_nompi->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree_nompi->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree_nompi->SetAlias("angle_jpsi_dx","acos((jpsi_px*dx_px + jpsi_py*dx_py)/(jpsi_pt*dx_pt))");
    pythia_tree_nompi->SetAlias("angle_jpsi_bx","acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt))");


    pythia_tree_nompi->SetAlias("jpsi_p",  "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz)");
    pythia_tree_nompi->SetAlias("jpsi_pt", "sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py)");
    pythia_tree_nompi->SetAlias("jpsi_eta","(jpsi_pz>0?-1:1)*acosh(jpsi_p/jpsi_pt)");
    pythia_tree_nompi->SetAlias("jpsi_y","0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) )");

    pythia_tree_nompi->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree_nompi->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_nompi->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree_nompi->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree_nompi->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree_nompi->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_nompi->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree_nompi->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree_nompi->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_jpsi_dx","jpsi_eta-dx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_jpsi_bx","jpsi_eta-dx2_eta");

    pythia_tree_nompi->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree_nompi->SetAlias("delta_y_jpsi_dx","jpsi_y-dx_y");
    pythia_tree_nompi->SetAlias("delta_y_jpsi_bx","jpsi_y-dx2_y");

    pythia_tree_nompi->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree_nompi->SetAlias("delta_R_jpsi_dx","sqrt(delta_eta_jpsi_dx*delta_eta_jpsi_dx+angle_jpsi_dx*angle_jpsi_dx)");
    pythia_tree_nompi->SetAlias("delta_R_jpsi_bx","sqrt(delta_eta_jpsi_bx*delta_eta_jpsi_bx+angle_jpsi_bx*angle_jpsi_bx)");

    double sigmaGen_pythia_nompi;
    double sigmaErr_pythia_nompi;
    pythia_tree_nompi->SetBranchAddress("sigmaGen",&sigmaGen_pythia_nompi);
    pythia_tree_nompi->SetBranchAddress("sigmaErr",&sigmaErr_pythia_nompi);
    
    int n_entries_pythia_nompi = pythia_tree_nompi->GetEntries();
    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);

    
    std::cout << "Cross section Pythia:  " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nJpsi==1&&nXc==2")<< "/" << pythia_tree->GetEntries("")<< std::endl;
    
    double withtarget_pythia = pythia_tree->GetEntries("nJpsi==1&&nXc==2");

    double scalefactor_pythia  =      sigmaGen_pythia*microbarn/n_entries_pythia * (withtarget_pythia/n_entries_pythia);
    
    std::cout << "scalefactor_pythia " << scalefactor_pythia<<std::endl;

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    TCut cuts_pythia        = "nJpsi==1&&nXc==2&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0";
    TCut cuts_pythia_nompi  = "nJpsi==1&&nXc==2&&dx2_pt!=0&&dx_pt!=0&&jpsi_pt!=0";

    // TCut cuts_all = "(jpsi_eta)>2 && (jpsi_eta)<5 && (dx_eta)>2 && (dx_eta)<5 && (dx2_eta)>2 && (dx2_eta)<5 ";
    TCut cuts_all = "";
    

    // -------------------------------------------------------------------------
    // Plot 1D kinematics integrated over all eta 
    // -------------------------------------------------------------------------
    // TCut temp_cut = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5) && foundHardc&&hascStatus23&&(hascStatus23_type!=4)";
    // pT
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia  = new TH1D("h_pt_pythia", "h_pt_pythia", 50,0,20);

    pythia_tree->Draw("jpsi_pt>>h_pt_pythia",cuts_pythia);

    h_pt_pythia->Scale(scalefactor_pythia);
    h_pt_pythia->SetLineColor(kRed);
    h_pt_pythia->SetTitle(";p_{T} [GeV/c^{2}]; d#sigma/dp_{T} [2.5 GeV^{-1} c^{2}]");
    h_pt_pythia->Draw("hist");

    TLegend* leg_pt = new TLegend(0.7,0.7,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,"Pythia","l");
    leg_pt->SetTextFont(132);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics_Jpsi/c_pt.pdf");


    // eta
    TCanvas* c_eta = new TCanvas("c_eta","c_eta");
    TH1D* h_eta_pythia  = new TH1D("h_eta_pythia", "h_eta_pythia", 50,-10,10);

    pythia_tree->Draw("jpsi_eta>>h_eta_pythia",cuts_pythia);

    h_eta_pythia->Scale(scalefactor_pythia);
    h_eta_pythia->SetLineColor(kRed);
    h_eta_pythia->SetTitle(";#eta ; d#sigma/d#eta [2.5]");
    h_eta_pythia->Draw("hist");


    TLegend* leg_eta = new TLegend(0.7,0.7,0.9,0.9);
    leg_eta->AddEntry(h_eta_pythia,"Pythia","l");
    leg_eta->SetTextFont(132);
    leg_eta->Draw();
    c_eta->Print("plots_kinematics_Jpsi/c_eta.pdf");

    // Rapidity
    TCanvas* c_y = new TCanvas("c_y","c_y");
    TH1D* h_y_pythia  = new TH1D("h_y_pythia", "h_y_pythia", 50,-10,10);

    pythia_tree->Draw("jpsi_y>>h_y_pythia",cuts_pythia);

    h_y_pythia->Scale(scalefactor_pythia);
    h_y_pythia->SetLineColor(kRed);
    h_y_pythia->SetTitle(";y ; d#sigma/dy [2.5]");
    h_y_pythia->Draw("hist");


    TLegend* leg_y = new TLegend(0.7,0.7,0.9,0.9);
    leg_y->AddEntry(h_y_pythia,"Pythia","l");
    leg_y->SetTextFont(132);
    leg_y->Draw();

    c_y->Print("plots_kinematics_Jpsi/c_y.pdf");

    // -------------------------------------------------------------------------
    // Plot 1D angles integrated over all eta 
    // -------------------------------------------------------------------------

    // angles
    TCanvas* c_delta_phi_jpsi_bx = new TCanvas("c_delta_phi_jpsi_bx","c_delta_phi_jpsi_bx");
    TH1D* h_delta_phi_jpsi_dx2_pythia  = new TH1D("h_delta_phi_jpsi_dx2_pythia", "h_delta_phi_jpsi_dx2_pythia", 50,0,TMath::Pi());

    pythia_tree->Draw("angle_jpsi_dx2>>h_delta_phi_jpsi_dx2_pythia",  cuts_pythia +cuts_all);

    h_delta_phi_jpsi_dx2_pythia->Scale(scalefactor_pythia);
    h_delta_phi_jpsi_dx2_pythia->SetLineColor(kRed);
    h_delta_phi_jpsi_dx2_pythia->Draw("hist same");
    
    TLegend* leg_delta_phi_jpsi_bx = new TLegend(0.7,0.2,0.9,0.4);
    leg_delta_phi_jpsi_bx->AddEntry(h_delta_phi_jpsi_dx2_pythia,"Pythia","l");
    leg_delta_phi_jpsi_bx->SetTextFont(132);
    leg_delta_phi_jpsi_bx->Draw();

    c_delta_phi_jpsi_bx->Print("plots_kinematics_Jpsi/c_delta_phi_jpsi_bx.pdf");


    // angles
    TCanvas* c_delta_phi_jpsi_dx = new TCanvas("c_delta_phi_jpsi_dx","c_delta_phi_jpsi_dx");
    TH1D* h_delta_phi_jpsi_dx_pythia  = new TH1D("h_delta_phi_jpsi_dx_pythia", "h_delta_phi_jpsi_dx_pythia", 50,0,TMath::Pi());

    pythia_tree->Draw("angle_jpsi_dx>>h_delta_phi_jpsi_dx_pythia",  cuts_pythia +cuts_all);

    h_delta_phi_jpsi_dx_pythia->Scale(scalefactor_pythia);
    h_delta_phi_jpsi_dx_pythia->SetLineColor(kRed);
    h_delta_phi_jpsi_dx_pythia->SetTitle(";#Delta#phi(J/#psi, X_{c}) [rad]; d#sigma/d#Delta#phi");
    h_delta_phi_jpsi_dx_pythia->SetMinimum(0.0);
    h_delta_phi_jpsi_dx_pythia->Draw("hist");

    TLegend* leg_delta_phi_jpsi_dx = new TLegend(0.7,0.2,0.9,0.4);
    leg_delta_phi_jpsi_dx->AddEntry(h_delta_phi_jpsi_dx_pythia,"Pythia","l");
    leg_delta_phi_jpsi_dx->SetTextFont(132);
    leg_delta_phi_jpsi_dx->Draw();
    c_delta_phi_jpsi_dx->Print("plots_kinematics_Jpsi/c_delta_phi_jpsi_dx.pdf");


    // angles
    TCanvas* c_delta_y_jpsi_dx = new TCanvas("c_delta_y_jpsi_dx","c_delta_y_jpsi_dx");
    TH1D* h_delta_y_jpsi_dx_pythia  = new TH1D("h_delta_y_jpsi_dx_pythia", "h_delta_y_jpsi_dx_pythia", 50,-5.0,5.0);

    pythia_tree->Draw("delta_y_jpsi_dx>>h_delta_y_jpsi_dx_pythia",  cuts_pythia +cuts_all);

    h_delta_y_jpsi_dx_pythia->Scale(scalefactor_pythia);
    h_delta_y_jpsi_dx_pythia->Scale(1.0/h_delta_y_jpsi_dx_pythia->Integral());
    h_delta_y_jpsi_dx_pythia->SetLineColor(kRed);
    h_delta_y_jpsi_dx_pythia->Draw("hist same");

    TLegend* leg_delta_eta_jpsi_dx = new TLegend(0.7,0.6,0.9,0.8);
    leg_delta_eta_jpsi_dx->AddEntry(h_delta_y_jpsi_dx_pythia,"Pythia","l");
    leg_delta_eta_jpsi_dx->SetTextFont(132);
    leg_delta_eta_jpsi_dx->Draw();
    c_delta_y_jpsi_dx->Print("plots_kinematics_Jpsi/c_delta_y_jpsi_dx.pdf");
    // -------------------------------------------------------------------------
    // Plot 2D angles integrated over all eta 
    // -------------------------------------------------------------------------


    SetLHCbStyle("cont");

    // angles
    TCanvas* c_delta_phi_pythia = new TCanvas("c_delta_phi_pythia","c_delta_phi_pythia");
    TH2D* h_delta_phi_pythia  = new TH2D("h_delta_phi_pythia", "h_delta_phi_pythia", 20,0,TMath::Pi(),20,0,TMath::Pi());

    pythia_tree->Draw("angle_jpsi_dx:angle_jpsi_dx2>>h_delta_phi_pythia",  cuts_pythia +cuts_all);

    h_delta_phi_pythia->Scale(scalefactor_pythia);
    h_delta_phi_pythia->SetTitle(";#Delta#phi(J/#psi, X_{c}) [rad]; #Delta#phi(J/#psi, X_{c}) [rad]");
    h_delta_phi_pythia->Draw("colz");
    c_delta_phi_pythia->Print("plots_kinematics_Jpsi/c_delta_phi_pythia.pdf");


    // eta
    TCanvas* c_delta_eta_pythia = new TCanvas("c_delta_eta_pythia","c_delta_eta_pythia");
    TH2D* h_delta_eta_pythia  = new TH2D("h_delta_eta_pythia", "h_delta_eta_pythia", 20,-5.0,5.0,20,-5.0,5.0);

    pythia_tree->Draw("delta_eta_jpsi_dx:delta_eta_jpsi_bx>>h_delta_eta_pythia",  cuts_pythia +cuts_all);

    h_delta_eta_pythia->Scale(scalefactor_pythia);
    h_delta_eta_pythia->SetTitle(";#Delta#eta(J/#psi, X_{c}) [rad]; #Delta#eta(J/#psi, X_{c}) [rad]");
    h_delta_eta_pythia->Draw("colz");
    c_delta_eta_pythia->Print("plots_kinematics_Jpsi/c_delta_eta_pythia.pdf");
    

    // Delta R
    TCanvas* c_delta_R_pythia = new TCanvas("c_delta_R_pythia","c_delta_R_pythia");
    TH2D* h_delta_R_pythia  = new TH2D("h_delta_R_pythia", "h_delta_R_pythia", 20,0.0,10.0,20,0.0,10.0);

    pythia_tree->Draw("delta_R_jpsi_dx:delta_R_jpsi_bx>>h_delta_R_pythia",  cuts_pythia +cuts_all);

    h_delta_R_pythia->Scale(scalefactor_pythia);
    h_delta_R_pythia->SetTitle(";#DeltaR(J/#psi, X_{c}) [rad]; #DeltaR(J/#psi, X_{c}) [rad]");
    h_delta_R_pythia->Draw("colz");
    c_delta_R_pythia->Print("plots_kinematics_Jpsi/c_delta_R_pythia.pdf");
    
    // -------------------------------------------------------------------------
    // Plot 2D pT vs. y 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_y_pythia = new TCanvas("c_2D_pt_y_pythia","c_2D_pt_y_pythia");
    TH2D* h_2D_pt_y_pythia      = new TH2D("h_2D_pt_y_pythia",      "h_2D_pt_y_pythia",    20,-10,10,20,0,10);

    pythia_tree->Draw(    "jpsi_pt:jpsi_y>>h_2D_pt_y_pythia",    cuts_pythia +cuts_all    );

    h_2D_pt_y_pythia->SetTitle(";y;p_{T} [GeV/c^{2}]");
    h_2D_pt_y_pythia->Draw("colz");
    c_2D_pt_y_pythia->Print("plots_kinematics_Jpsi/c_2D_pt_y_pythia.pdf");
     
    
    // -------------------------------------------------------------------------
    // Compare to LHCb measurements
    // -------------------------------------------------------------------------

    SetLHCbStyle("oneD");
    TCanvas* c_lhcb_comp = new TCanvas("c_lhcb_comp","c_lhcb_comp");
    TH1D* h_lhcb_comp        = new TH1D("h_lhcb_comp",       "h_lhcb_comp",       10,0,1.0);
    TH1D* h_lhcb_comp_nompi  = new TH1D("h_lhcb_comp_nompi", "h_lhcb_comp_nompi", 10,0,1.0);
    
    TCut cuts_fid_dx  = "(jpsi_eta)>2 && (jpsi_eta)<5 &&  (dx_eta )>2 && (dx_eta )<5  ";
    TCut cuts_fid_dx2 = "(jpsi_eta)>2 && (jpsi_eta)<5 &&  (dx2_eta)>2 && (dx2_eta)<5  ";

    pythia_tree->Draw("angle_jpsi_dx/TMath::Pi()>>h_lhcb_comp",  cuts_pythia +cuts_all+cuts_fid_dx);
    pythia_tree_nompi->Draw("angle_jpsi_dx/TMath::Pi()>>h_lhcb_comp_nompi",  cuts_pythia_nompi +cuts_all+cuts_fid_dx);

    pythia_tree->Draw("angle_jpsi_dx2/TMath::Pi()>>+h_lhcb_comp",  cuts_pythia +cuts_all+cuts_fid_dx2);
    pythia_tree_nompi->Draw("angle_jpsi_dx2/TMath::Pi()>>+h_lhcb_comp_nompi",  cuts_pythia_nompi +cuts_all+cuts_fid_dx2);

    TH1D *h_lhcb_Dz = new TH1D("h_lhcb_Dz","",10,0,1);
    h_lhcb_Dz->SetBinContent( 1,0.1162); h_lhcb_Dz->SetBinError( 1,0.0075);
    h_lhcb_Dz->SetBinContent( 2,0.099);  h_lhcb_Dz->SetBinError( 2,0.0071);
    h_lhcb_Dz->SetBinContent( 3,0.0868); h_lhcb_Dz->SetBinError( 3,0.0065);
    h_lhcb_Dz->SetBinContent( 4,0.0968); h_lhcb_Dz->SetBinError( 4,0.0069);
    h_lhcb_Dz->SetBinContent( 5,0.0759); h_lhcb_Dz->SetBinError( 5,0.0071);
    h_lhcb_Dz->SetBinContent( 6,0.0965); h_lhcb_Dz->SetBinError( 6,0.0073);
    h_lhcb_Dz->SetBinContent( 7,0.0996); h_lhcb_Dz->SetBinError( 7,0.0078);
    h_lhcb_Dz->SetBinContent( 8,0.1039); h_lhcb_Dz->SetBinError( 8,0.0077);
    h_lhcb_Dz->SetBinContent( 9,0.1152); h_lhcb_Dz->SetBinError( 9,0.0075);
    h_lhcb_Dz->SetBinContent(10,0.1101); h_lhcb_Dz->SetBinError(10,0.0077);

    TH1D *h_lhcb_Dp = new TH1D("h_lhcb_Dp","",10,0,1);
    h_lhcb_Dp->SetLineColor(kGreen+2);
    h_lhcb_Dp->SetMarkerColor(kGreen+2);

    h_lhcb_Dp->SetBinContent( 1,0.0987);  h_lhcb_Dp->SetBinError( 1,0.0092);
    h_lhcb_Dp->SetBinContent( 2,0.0952);  h_lhcb_Dp->SetBinError( 2,0.0088);
    h_lhcb_Dp->SetBinContent( 3,0.0937);  h_lhcb_Dp->SetBinError( 3,0.0086);
    h_lhcb_Dp->SetBinContent( 4,0.1021);  h_lhcb_Dp->SetBinError( 4,0.0091);
    h_lhcb_Dp->SetBinContent( 5,0.0899);  h_lhcb_Dp->SetBinError( 5,0.0093);
    h_lhcb_Dp->SetBinContent( 6,0.0894);  h_lhcb_Dp->SetBinError( 6,0.0081);
    h_lhcb_Dp->SetBinContent( 7,0.0993);  h_lhcb_Dp->SetBinError( 7,0.0088);
    h_lhcb_Dp->SetBinContent( 8,0.105);   h_lhcb_Dp->SetBinError( 8,0.0099);
    h_lhcb_Dp->SetBinContent( 9,0.1241);  h_lhcb_Dp->SetBinError( 9,0.0112);
    h_lhcb_Dp->SetBinContent(10,0.1026);  h_lhcb_Dp->SetBinError(10,0.0107);


    TH1D *h_lhcb_Ds = new TH1D("h_lhcb_Ds","",8,0,1);
    h_lhcb_Ds->SetLineColor(kMagenta);
    h_lhcb_Ds->SetMarkerColor(kMagenta);
    h_lhcb_Ds->SetBinContent(   1  , 0.1267);  h_lhcb_Ds->SetBinError( 1 , 0.03   ); 
    h_lhcb_Ds->SetBinContent(   2  , 0.1224);  h_lhcb_Ds->SetBinError( 2 , 0.0301 ); 
    h_lhcb_Ds->SetBinContent(   3  , 0.1224);  h_lhcb_Ds->SetBinError( 3 , 0.0311 ); 
    h_lhcb_Ds->SetBinContent(   4  , 0.1299);  h_lhcb_Ds->SetBinError( 4 , 0.0279 ); 
    h_lhcb_Ds->SetBinContent(   5  , 0.1049);  h_lhcb_Ds->SetBinError( 5 , 0.0273 ); 
    h_lhcb_Ds->SetBinContent(   6  , 0.1126);  h_lhcb_Ds->SetBinError( 6 , 0.0289 ); 
    h_lhcb_Ds->SetBinContent(   7  , 0.1075);  h_lhcb_Ds->SetBinError( 7 , 0.0321 ); 
    h_lhcb_Ds->SetBinContent(   8  , 0.1736);  h_lhcb_Ds->SetBinError( 8 , 0.0327 );

    std::cout << "Integral: h_lhcb_comp       " << h_lhcb_comp->Integral()       << std::endl;
    std::cout << "Integral: h_lhcb_comp_nompi " << h_lhcb_comp_nompi->Integral() << std::endl;
    std::cout << "Integral: h_lhcb_Dz         " << h_lhcb_Dz->Integral()         << std::endl;
    std::cout << "Integral: h_lhcb_Dp         " << h_lhcb_Dp->Integral()         << std::endl;
    
    std::cout << "Integralw: h_lhcb_comp       " << h_lhcb_comp->Integral("width")       << std::endl;
    std::cout << "Integralw: h_lhcb_comp_nompi " << h_lhcb_comp_nompi->Integral("width") << std::endl;
    std::cout << "Integralw: h_lhcb_Dz         " << h_lhcb_Dz->Integral("width")         << std::endl;
    std::cout << "Integralw: h_lhcb_Dp         " << h_lhcb_Dp->Integral("width")         << std::endl;

    h_lhcb_comp->Scale(1.0/h_lhcb_comp->Integral("width"));
    h_lhcb_comp_nompi->Scale(1.0/h_lhcb_comp_nompi->Integral("width"));
    h_lhcb_Dz->Scale(1.0/h_lhcb_Dz->Integral("width"));
    h_lhcb_Dp->Scale(1.0/h_lhcb_Dp->Integral("width"));
    h_lhcb_Ds->Scale(1.0/h_lhcb_Ds->Integral("width"));


    h_lhcb_comp->SetLineColor(kRed);
    h_lhcb_comp_nompi->SetLineColor(kBlue);
    
    double h_lhcb_comp_bin_width = h_lhcb_comp->GetXaxis()->GetBinWidth(2);
    h_lhcb_comp->SetTitle(";#Delta#phi(J/#psi, X_{c})/#pi; 1/#sigma d#sigma/d#Delta#phi");
    h_lhcb_comp->SetMinimum(0.0);
    h_lhcb_comp->SetMaximum(2.0);

    h_lhcb_comp->Draw("hist ][");
    h_lhcb_comp_nompi->Draw("same hist ][");

    h_lhcb_Dp->Draw("e1 same");
    h_lhcb_Dz->Draw("e1 same");
    h_lhcb_Ds->Draw("e1 same");


    TLegend* leg_lhcb_comp = new TLegend(0.5,0.7,0.9,0.9);
    leg_lhcb_comp->AddEntry(h_lhcb_comp,"Pythia With MPI","l");
    leg_lhcb_comp->AddEntry(h_lhcb_comp_nompi,"Pythia No MPI","l");
    leg_lhcb_comp->AddEntry(h_lhcb_Dp,"LHCb J/#psiD^{+}","le");
    leg_lhcb_comp->AddEntry(h_lhcb_Dz,"LHCb J/#psiD^{0}","le");
    leg_lhcb_comp->AddEntry(h_lhcb_Ds,"LHCb J/#psiD_{s}^{+}","le");
    leg_lhcb_comp->SetTextFont(132);
    leg_lhcb_comp->SetFillStyle(0);
    leg_lhcb_comp->Draw();


    c_lhcb_comp->Print("plots_kinematics_Jpsi/c_lhcb_comp.pdf");



    // Loop over the events and categorise them
    bool foundHardc; 
    bool hascStatus23; 
    bool hascStatus33; 
    bool hascOniaStatus23; 
    bool hascOniaStatus33; 
    int hascStatus23_type;
    int hascStatus33_type;
    int hascStatus33_type2;
    int hascStatus33_N;
    int hascOniaStatus33_N;
    int nJpsi;
    int nXc;

    pythia_tree->SetBranchAddress("foundHardc",        &foundHardc); 
    pythia_tree->SetBranchAddress("hascStatus23",      &hascStatus23); 
    pythia_tree->SetBranchAddress("hascStatus33",      &hascStatus33); 
    pythia_tree->SetBranchAddress("hascOniaStatus23",  &hascOniaStatus23); 
    pythia_tree->SetBranchAddress("hascOniaStatus33",  &hascOniaStatus33); 
    pythia_tree->SetBranchAddress("hascStatus23_type", &hascStatus23_type );
    pythia_tree->SetBranchAddress("hascStatus33_type", &hascStatus33_type );
    pythia_tree->SetBranchAddress("hascStatus33_type2",&hascStatus33_type2 );
    pythia_tree->SetBranchAddress("hascStatus33_N",    &hascStatus33_N );
    pythia_tree->SetBranchAddress("nJpsi",             &nJpsi );
    pythia_tree->SetBranchAddress("nXc",               &nXc );
    pythia_tree->SetBranchAddress("hascOniaStatus33_N",&hascOniaStatus33_N);


    // Add kinematic distributions
    double jpsi_pe, jpsi_px, jpsi_py, jpsi_pz;
    double dx_pe,   dx_px,   dx_py,   dx_pz;
    double dx2_pe,  dx2_px,  dx2_py,  dx2_pz;

    pythia_tree->SetBranchAddress("jpsi_px",        &jpsi_px); 
    pythia_tree->SetBranchAddress("jpsi_py",        &jpsi_py); 
    pythia_tree->SetBranchAddress("jpsi_pz",        &jpsi_pz); 
    pythia_tree->SetBranchAddress("jpsi_pe",        &jpsi_pe); 
    pythia_tree->SetBranchAddress("dx_px",          &dx_px); 
    pythia_tree->SetBranchAddress("dx_py",          &dx_py); 
    pythia_tree->SetBranchAddress("dx_pz",          &dx_pz); 
    pythia_tree->SetBranchAddress("dx_pe",          &dx_pe);
    pythia_tree->SetBranchAddress("dx2_px",         &dx2_px); 
    pythia_tree->SetBranchAddress("dx2_py",         &dx2_py); 
    pythia_tree->SetBranchAddress("dx2_pz",         &dx2_pz); 
    pythia_tree->SetBranchAddress("dx2_pe",         &dx2_pe); 
    bool Jpsi_isMixedLine;
    pythia_tree->SetBranchAddress("Jpsi_isMixedLine",         &Jpsi_isMixedLine); 

    std::map<std::string,int> n_processes; 
    std::map<std::string,TH2D*> h_processes; 
    std::map<std::string,TH1D*> h_processes_delta_y;
    std::map<std::string,TH2D*> h_processes_delta_y_2d; 


    std::map<std::string,TH2D*> h_processes_pt_eta; 

    int n_bins = 10;
    int n_bins_kin = 20;

    for(int i = 0; i< n_entries_pythia; i++){
        pythia_tree->GetEntry(i);
        if(nJpsi==1&&nXc==2){
            // TLotentzVector jpsi_vec( jpsi_px,jpsi_py,jpsi_pz,jpsi_pe);
            // TLotentzVector dx_vec(   dx_px,  dx_py,  dx_pz,  dx_pe  );
            // TLotentzVector dx2_vec(  dx2_px, dx2_py, dx2_pz, dx2_pe );

            double jpsi_pt = sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py);
            double dx2_pt  = sqrt(dx2_px *dx2_px +dx2_py *dx2_py );
            double dx_pt   = sqrt(dx_px  *dx_px  +dx_py  *dx_py  ); 

            double angle_dx2_dx   = acos((dx2_px*dx_px   + dx2_py*dx_py  )/(dx2_pt*dx_pt  ));
            double angle_jpsi_dx  = acos((jpsi_px*dx_px  + jpsi_py*dx_py )/(jpsi_pt*dx_pt ));
            double angle_jpsi_dx2 = acos((jpsi_px*dx2_px + jpsi_py*dx2_py)/(jpsi_pt*dx2_pt));
    
            double jpsi_p = sqrt(jpsi_px*jpsi_px+jpsi_py*jpsi_py+jpsi_pz*jpsi_pz);
            double dx_p = sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz);
            double dx2_p = sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz);

            double jpsi_eta = (jpsi_pz>0.0?-1.0:1.0)*acosh(jpsi_p/jpsi_pt);
            double dx_eta   = (dx_pz>0.0?-1.0:1.0)*acosh(dx_p/dx_pt);
            double dx2_eta  = (dx2_pz>0.0?-1.0:1.0)*acosh(dx2_p/dx2_pt);


            double jpsi_y = 0.5*log( (jpsi_pe+jpsi_pz) / (jpsi_pe-jpsi_pz) );
            double dx2_y  = 0.5*log( (dx2_pe +dx2_pz)  / (dx2_pe -dx2_pz)  );
            double dx_y   = 0.5*log( (dx_pe  +dx_pz)   / (dx_pe  -dx_pz)   );

            double delta_y_jpsi_dx2 = jpsi_y - dx2_y;
            double delta_y_jpsi_dx  = jpsi_y - dx_y;
            double delta_y_dx2_dx   = dx2_y  - dx_y;


            bool inAcc = (jpsi_eta)>2 && (jpsi_eta)<5 && (dx_eta)>2 && (dx_eta)<5 && (dx2_eta)>2 && (dx2_eta)<5;
            if(not inAcc) continue;

            std::vector<std::string> processes;
            std::string name;

            int n_accounted_for = hascStatus23 + hascStatus33_N + hascOniaStatus23 + hascOniaStatus33_N;

            if(hascStatus23){
                
                name = "Hard";
                if(hascStatus23_type==4){
                    name += "_cc";
                // } else if(hascStatus23_type==21){
                //     name += " cg";
                } else {
                    name += "_cx";
                }
                processes.push_back(name);
            }

            if(hascOniaStatus23){
                name = "Hard_Onia";
                processes.push_back(name);
            }

            if(hascStatus33_N == 1){
                name = "MPI";
                if(hascStatus33_type==4){
                    name += "_cc";
                // } else if(hascStatus33_type==21){
                //     name += " cg";
                } else {
                    name += "_cx";
                }
                processes.push_back(name);

            }
            if(hascStatus33_N == 2){
                name = "MPI1";
                if(hascStatus33_type==4){
                    name += "_cc";
                // } else if(hascStatus33_type==21){
                //     name += " cg";
                } else {
                    name += "_cx";
                }
                processes.push_back(name);

                
                name = "MPI2";
                if(hascStatus33_type2==4){
                    name += "_cc";
                // } else if(hascStatus33_type2==21){
                //     name += " cg";
                } else {
                    name += "_cx";
                }
                processes.push_back(name);


            }
            if(hascOniaStatus33_N == 1){
                name =  "MPI_Onia";
                processes.push_back(name);

            }
            if(hascOniaStatus33_N == 2){
                name =  "MPI1_Onia";
                processes.push_back(name);

                name =  "MPI2_Onia";
                processes.push_back(name);

            }

            if(n_accounted_for==1){
                name =  "Showerc";
                processes.push_back(name);


            }

            if(n_accounted_for==0){
                name =  "Showerc";
                processes.push_back(name);

                name =  "Showerc";
                processes.push_back(name);

            }

            std::string cat_name;
            if(n_accounted_for <3 && processes.size()==2){
                cat_name = processes[0]+"_"+ processes[1];
            } else if(n_accounted_for >=3){
                cat_name = "Bad_3";
            } else {
                cat_name = "Bad_Other";
            }
            
            if(Jpsi_isMixedLine){
                cat_name += "_Mix";
            } else {
                cat_name += "_Sep";
            }

            if(h_processes.count(cat_name)==0){
                h_processes[cat_name] = new TH2D(Form("h_%s",cat_name.c_str()), 
                                                 Form("h_%s;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",cat_name.c_str()),
                                                 n_bins,0,TMath::Pi(),
                                                 n_bins,0,TMath::Pi() );
                h_processes_pt_eta[cat_name] = new TH2D(Form("h_%s_pt_eta",cat_name.c_str()), 
                                                 Form("h_%s_pt_eta;#eta;p_{T} [GeV/c^2]",cat_name.c_str()),
                                                 n_bins_kin,-10.0,10.0,
                                                 n_bins_kin,0,15.0 );
                h_processes_delta_y_2d[cat_name] = new TH2D(Form("h_%s_delta_y_2d",cat_name.c_str()), 
                                                           Form("h_%s_delta_y_2d;#Delta#it{y}(B_{c}^{+}, X_{c});#Delta#it{y}(B_{c}^{+}, X_{#bar{c}})",cat_name.c_str()),
                                                            20,0,10.0,
                                                            20,0,10.0 );

                h_processes_delta_y[cat_name] = new TH1D(Form("h_%s_delta_y",cat_name.c_str()),
                                                         Form("h_%s_delta_y; #Delta#it{y}(X_{c},X_{#bar{c}});Events",cat_name.c_str()),
                                                         20,0.0,5.0);

                
            }
            
            h_processes[cat_name]->Fill(angle_jpsi_dx,angle_jpsi_dx2);
            h_processes_pt_eta[cat_name]->Fill(jpsi_eta,jpsi_pt);
            h_processes_delta_y[cat_name]->Fill(delta_y_dx2_dx);

            h_processes_delta_y_2d[cat_name]->Fill(abs(delta_y_jpsi_dx),abs(delta_y_jpsi_dx2));

            n_processes[cat_name]++;
        }
    }

    for(const auto & n_proc: n_processes){
        std::cout << std::setw(25) << n_proc.first  << " | ";
        std::cout << std::setw(10) << n_proc.second << " | ";
        std::cout << std::endl;
    }   

    // Add plots together
    std::map<std::string,bool> isSPS;


    isSPS["Hard_Onia_Showerc_Mix"]   = true;
    isSPS["Hard_cc_Showerc_Mix"]     = true;
    isSPS["MPI_Onia_Showerc_Mix"]    = true;
    isSPS["Hard_cx_Showerc_Mix"]     = true;
    isSPS["MPI_cc_Showerc_Mix"]      = true;
    isSPS["MPI_cx_Showerc_Mix"]      = true;
    isSPS["Showerc_Showerc_Mix"]     = true;

    isSPS["Hard_Onia_MPI_Onia_Mix"]  = false;
    isSPS["Hard_Onia_MPI_cc_Mix"]    = false;
    isSPS["Hard_Onia_MPI_cx_Mix"]    = false;

    isSPS["Hard_cc_MPI_Onia_Mix"]    = false;
    isSPS["Hard_cc_MPI_cc_Mix"]      = false; // M
    isSPS["Hard_cc_MPI_cx_Mix"]      = false; // M

    isSPS["Hard_cx_Hard_Onia_Mix"]   = false;
    isSPS["Hard_cx_MPI_Onia_Mix"]    = false;
    isSPS["Hard_cx_MPI_cc_Mix"]      = false; // M
    isSPS["Hard_cx_MPI_cx_Mix"]      = false; // M

    isSPS["MPI1_Onia_MPI2_Onia_Mix"] = false;
    isSPS["MPI1_cc_MPI2_cc_Mix"]     = false; // M
    isSPS["MPI1_cc_MPI2_cx_Mix"]     = false; // M
    isSPS["MPI1_cx_MPI2_cc_Mix"]     = false; // M Both included 
    isSPS["MPI1_cx_MPI2_cx_Mix"]     = false; // M
    
    isSPS["MPI_cc_MPI_Onia_Mix"]     = false;
    // isSPS["MPI_cx_MPI_Onia_Mix"]     = false; // possibly if x = onia // Turn off!
    

    isSPS["Hard_Onia_Showerc_Sep"]   = true;
    isSPS["Hard_cc_Showerc_Sep"]     = true;
    isSPS["Hard_cx_Showerc_Sep"]     = true;
    isSPS["MPI_Onia_Showerc_Sep"]    = true;
    isSPS["MPI_cc_Showerc_Sep"]      = true;
    isSPS["MPI_cx_Showerc_Sep"]      = true;
    isSPS["Showerc_Showerc_Sep"]     = true;


    isSPS["Hard_Onia_MPI_Onia_Sep"]  = false;
    isSPS["Hard_Onia_MPI_cc_Sep"]    = false;
    isSPS["Hard_Onia_MPI_cx_Sep"]    = false;
    
    isSPS["Hard_cc_MPI_Onia_Sep"]    = false;
    isSPS["Hard_cc_MPI_cc_Sep"]      = false;
    isSPS["Hard_cc_MPI_cx_Sep"]      = false;
    
    isSPS["Hard_cx_Hard_Onia_Sep"]   = false;
    isSPS["Hard_cx_MPI_Onia_Sep"]    = false;
    isSPS["Hard_cx_MPI_cc_Sep"]      = false;
    isSPS["Hard_cx_MPI_cx_Sep"]      = false;
    
    isSPS["MPI1_Onia_MPI2_Onia_Sep"] = false;
    isSPS["MPI1_cc_MPI2_cc_Sep"]     = false;
    isSPS["MPI1_cc_MPI2_cx_Sep"]     = false;
    isSPS["MPI1_cx_MPI2_cc_Sep"]     = false;
    isSPS["MPI1_cx_MPI2_cx_Sep"]     = false;
    
    isSPS["MPI_cc_MPI_Onia_Sep"]     = false;
    isSPS["MPI_cx_MPI_Onia_Sep"]     = false; // possibly if x = onia
    

    TH2D* h_processes_pt_eta_SPS_Mix = new TH2D("h_SPS_Mix_pt_eta", 
                                     "h_SPS_Mix_pt_eta;#eta;p_{T} [GeV/c^2]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );


    TH2D* h_processes_pt_eta_MPI_Mix = new TH2D("h_MPI_Mix_pt_eta", 
                                     "h_MPI_Mix_pt_eta;#eta;p_{T} [GeV/c^2]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );

    TH2D* h_processes_pt_eta_SPS_Sep = new TH2D("h_SPS_Sep_pt_eta", 
                                     "h_SPS_Sep_pt_eta;#eta;p_{T} [GeV/c^2]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );


    TH2D* h_processes_pt_eta_MPI_Sep = new TH2D("h_MPI_Sep_pt_eta", 
                                     "h_MPI_Sep_pt_eta;#eta;p_{T} [GeV/c^2]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );

    TH2D* h_processes_SPS_Mix = new TH2D("h_SPS_Mix", 
                                    "h_SPS_Mix;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_MPI_Mix = new TH2D("h_MPI_Mix", 
                                    "h_MPI_Mix;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_SPS_Sep = new TH2D("h_SPS_Sep", 
                                    "h_SPS_Sep;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_MPI_Sep = new TH2D("h_MPI_Sep", 
                                    "h_MPI_Sep;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );
    

    TH2D* h_processes_delta_y_2d_SPS_Mix = new TH2D("h_SPS_Mix_delta_y_2d", 
                                     "h_SPS_Mix_delta_y_2d;#Delta#it{y}(J/#psi, X_{c}) [rad];#Delta#it{y}(J/#psi, X_{c}) [rad]",
                                     20,0.0,10.0,
                                     20,0.0,10.0 );


    TH2D* h_processes_delta_y_2d_MPI_Mix = new TH2D("h_MPI_Mix_delta_y_2d", 
                                     "h_MPI_Mix_delta_y_2d;#Delta#it{y}(J/#psi, X_{c}) [rad];#Delta#it{y}(J/#psi, X_{c}) [rad]",
                                     20,0.0,10.0,
                                     20,0.0,10.0 );

    TH2D* h_processes_delta_y_2d_SPS_Sep = new TH2D("h_SPS_Sep_delta_y_2d", 
                                     "h_SPS_Sep_delta_y_2d;#Delta#it{y}(J/#psi, X_{c}) [rad];#Delta#it{y}(J/#psi, X_{c}) [rad]",
                                     20,0.0,10.0,
                                     20,0.0,10.0 );


    TH2D* h_processes_delta_y_2d_MPI_Sep = new TH2D("h_MPI_Sep_delta_y_2d", 
                                     "h_MPI_Sep_pt_eta;#Delta#it{y}(J/#psi, X_{c}) [rad];#Delta#it{y}(J/#psi, X_{c}) [rad]",
                                     20,0.0,10.0,
                                     20,0.0,10.0 );
    
    // -------------------------------------------------------------------------
    // Delta y in 2D
    // -------------------------------------------------------------------------

    for(const auto & h_proc: h_processes_delta_y_2d){
        if(isSPS.count(h_proc.first)){
            if(h_proc.first.find("_Mix")!=std::string::npos){
                if(isSPS[h_proc.first]){
                    h_processes_delta_y_2d_SPS_Mix->Add(h_processes_delta_y_2d_SPS_Mix,h_proc.second);
                } else {
                    h_processes_delta_y_2d_MPI_Mix->Add(h_processes_delta_y_2d_MPI_Mix,h_proc.second);
                } 
            }else{

                if(isSPS[h_proc.first]){
                    h_processes_delta_y_2d_SPS_Sep->Add(h_processes_delta_y_2d_SPS_Sep,h_proc.second);
                } else {
                    h_processes_delta_y_2d_MPI_Sep->Add(h_processes_delta_y_2d_MPI_Sep,h_proc.second);
                } 
            }
        } else {
            std::cout << "Cat not SPS or MPI " << h_proc.first << std::endl;
        }
    }
    SetLHCbStyle("cont");


    h_processes_delta_y_2d_SPS_Mix->SetMinimum(0);
    h_processes_delta_y_2d_MPI_Mix->SetMinimum(0);
    h_processes_delta_y_2d_SPS_Sep->SetMinimum(0);
    h_processes_delta_y_2d_MPI_Sep->SetMinimum(0);


    TCanvas* can_delta_y_2d_SPS_Mix = new TCanvas("can_delta_y_2d_SPS_Mix","can_delta_y_2d_SPS_Mix");
    h_processes_delta_y_2d_SPS_Mix->Draw("colz");
    can_delta_y_2d_SPS_Mix->Print("plots_kinematics_Jpsi/can_delta_y_2d_SPS_Mix.pdf");
    
    TCanvas* can_delta_y_2d_MPI_Mix = new TCanvas("can_delta_y_2d_MPI_Mix","can_delta_y_2d_MPI_Mix");
    h_processes_delta_y_2d_MPI_Mix->Draw("colz");
    can_delta_y_2d_MPI_Mix->Print("plots_kinematics_Jpsi/can_delta_y_2d_MPI_Mix.pdf");


    TCanvas* can_delta_y_2d_SPS_Sep = new TCanvas("can_delta_y_2d_SPS_Sep","can_delta_y_2d_SPS_Sep");
    h_processes_delta_y_2d_SPS_Sep->Draw("colz");
    can_delta_y_2d_SPS_Sep->Print("plots_kinematics_Jpsi/can_delta_y_2d_SPS_Sep.pdf");
    
    TCanvas* can_delta_y_2d_MPI_Sep = new TCanvas("can_delta_y_2d_MPI_Sep","can_delta_y_2d_MPI_Sep");
    h_processes_delta_y_2d_MPI_Sep->Draw("colz");
    can_delta_y_2d_MPI_Sep->Print("plots_kinematics_Jpsi/can_delta_y_2d_MPI_Sep.pdf");


    // -------------------------------------------------------------------------
    // pT vs eta 
    // -------------------------------------------------------------------------

    for(const auto & h_proc: h_processes_pt_eta){
        if(isSPS.count(h_proc.first)){
            if(h_proc.first.find("_Mix")!=std::string::npos){
                if(isSPS[h_proc.first]){
                    h_processes_pt_eta_SPS_Mix->Add(h_processes_pt_eta_SPS_Mix,h_proc.second);
                } else {
                    h_processes_pt_eta_MPI_Mix->Add(h_processes_pt_eta_MPI_Mix,h_proc.second);
                } 
            }else{

                if(isSPS[h_proc.first]){
                    h_processes_pt_eta_SPS_Sep->Add(h_processes_pt_eta_SPS_Sep,h_proc.second);
                } else {
                    h_processes_pt_eta_MPI_Sep->Add(h_processes_pt_eta_MPI_Sep,h_proc.second);
                } 
            }
        } else {
            std::cout << "Cat not SPS or MPI " << h_proc.first << std::endl;
        }
    }
    SetLHCbStyle("cont");


    h_processes_pt_eta_SPS_Mix->SetMinimum(0);
    h_processes_pt_eta_MPI_Mix->SetMinimum(0);
    h_processes_pt_eta_SPS_Sep->SetMinimum(0);
    h_processes_pt_eta_MPI_Sep->SetMinimum(0);


    TCanvas* can_pt_eta_SPS_Mix = new TCanvas("can_pt_eta_SPS_Mix","can_pt_eta_SPS_Mix");
    h_processes_pt_eta_SPS_Mix->Draw("colz");
    can_pt_eta_SPS_Mix->Print("plots_kinematics_Jpsi/can_pt_eta_SPS_Mix.pdf");
    
    TCanvas* can_pt_eta_MPI_Mix = new TCanvas("can_pt_eta_MPI_Mix","can_pt_eta_MPI_Mix");
    h_processes_pt_eta_MPI_Mix->Draw("colz");
    can_pt_eta_MPI_Mix->Print("plots_kinematics_Jpsi/can_pt_eta_MPI_Mix.pdf");


    TCanvas* can_pt_eta_SPS_Sep = new TCanvas("can_pt_eta_SPS_Sep","can_pt_eta_SPS_Sep");
    h_processes_pt_eta_SPS_Sep->Draw("colz");
    can_pt_eta_SPS_Sep->Print("plots_kinematics_Jpsi/can_pt_eta_SPS_Sep.pdf");
    
    TCanvas* can_pt_eta_MPI_Sep = new TCanvas("can_pt_eta_MPI_Sep","can_pt_eta_MPI_Sep");
    h_processes_pt_eta_MPI_Sep->Draw("colz");
    can_pt_eta_MPI_Sep->Print("plots_kinematics_Jpsi/can_pt_eta_MPI_Sep.pdf");


    // -------------------------------------------------------------------------
    // Delta phi in 2D
    // -------------------------------------------------------------------------

    for(const auto & h_proc: h_processes){
        if(isSPS.count(h_proc.first)){
            if(h_proc.first.find("_Mix")!=std::string::npos){
                if(isSPS[h_proc.first]){
                    h_processes_SPS_Mix->Add(h_processes_SPS_Mix,h_proc.second);
                } else {
                    h_processes_MPI_Mix->Add(h_processes_MPI_Mix,h_proc.second);
                }  
            } else {
                if(isSPS[h_proc.first]){
                    h_processes_SPS_Sep->Add(h_processes_SPS_Sep,h_proc.second);
                } else {
                    h_processes_MPI_Sep->Add(h_processes_MPI_Sep,h_proc.second);
                }
            } 
        } else {
            std::cout << "Cat not SPS or MPI " << h_proc.first << std::endl;
        }
    }
    SetLHCbStyle("cont");

    h_processes_SPS_Mix->SetMinimum(0);
    h_processes_MPI_Mix->SetMinimum(0);
    h_processes_SPS_Sep->SetMinimum(0);
    h_processes_MPI_Sep->SetMinimum(0);

    TCanvas* can_SPS_Mix = new TCanvas("can_SPS_Mix","can_SPS_Mix");
    h_processes_SPS_Mix->Draw("colz");
    can_SPS_Mix->Print("plots_kinematics_Jpsi/can_SPS_Mix.pdf");
    
    TCanvas* can_MPI_Mix = new TCanvas("can_MPI_Mix","can_MPI_Mix");
    h_processes_MPI_Mix->Draw("colz");
    can_MPI_Mix->Print("plots_kinematics_Jpsi/can_MPI_Mix.pdf");

    TCanvas* can_SPS_Sep = new TCanvas("can_SPS_Sep","can_SPS_Sep");
    h_processes_SPS_Sep->Draw("colz");
    can_SPS_Sep->Print("plots_kinematics_Jpsi/can_SPS_Sep.pdf");
    
    TCanvas* can_MPI_Sep = new TCanvas("can_MPI_Sep","can_MPI_Sep");
    h_processes_MPI_Sep->Draw("colz");
    can_MPI_Sep->Print("plots_kinematics_Jpsi/can_MPI_Sep.pdf");


    // Draw angles
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s",name.c_str()),Form("can_%s",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics_Jpsi/can_%s.pdf",name.c_str()));

    }

    // Draw pt/eta
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes_pt_eta){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s_pt_eta",name.c_str()),Form("can_%s_pt_eta",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics_Jpsi/can_%s_pt_eta.pdf",name.c_str()));

    }


    // Draw pt/eta
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes_delta_y){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s_delta_y",name.c_str()),Form("can_%s_delta_y",name.c_str()));
        h_proc.second->Draw("");
        can->Print(Form("plots_kinematics_Jpsi/can_%s_delta_y.pdf",name.c_str()));

    }
    // Draw pt/eta
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes_delta_y_2d){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s",name.c_str()),Form("can_%s",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics_Jpsi/can_%s.pdf",name.c_str()));

    }



}