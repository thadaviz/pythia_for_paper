#include "tools.h"

void plot_BB_angles(){
    SetLHCbStyle("oneD");

    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    // TFile* tfile = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_50000_noMPI.root");
    // TFile* tfile = TFile::Open("../output/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_50000_PartMod1.root");
    TFile* tfile = TFile::Open("../output/from_lxplus/Complete_paper_samples/Sample_bb/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_10000000_PartMod1_noColRec.root");
    // TFile* tfile = TFile::Open("../output/main202output_SoftQCD_nd_N_100000_noMPI_PartMod1.root");

    TTree* events = (TTree*)tfile->Get("events");

    events->SetAlias("bx_pt" ,  "sqrt(bx_px*bx_px+bx_py*bx_py)");
    events->SetAlias("bx2_pt" , "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");

    events->SetAlias("angle_bx_bx2","acos((bx_px*bx2_px + bx_py*bx2_py)/(bx_pt*bx2_pt))");

    // Plot 2d for  Bc
    TH1D* h_1d_all  = new TH1D("h_1d_all", "h_1d_all", 10,0,TMath::Pi());
    TH1D* h_1d_hard_bb = new TH1D("h_1d_hard_bb","h_1d_hard_bb",10,0,TMath::Pi());
    TH1D* h_1d_hard_bx = new TH1D("h_1d_hard_bx","h_1d_hard_bx",10,0,TMath::Pi());
    TH1D* h_1d_MPI_bb  = new TH1D("h_1d_MPI_bb", "h_1d_MPI_bb", 10,0,TMath::Pi());
    TH1D* h_1d_MPI_bx  = new TH1D("h_1d_MPI_bx", "h_1d_MPI_bx", 10,0,TMath::Pi());
    TH1D* h_1d_All_bb  = new TH1D("h_1d_All_bb", "h_1d_All_bb", 10,0,TMath::Pi());
    TH1D* h_1d_All_bx  = new TH1D("h_1d_All_bx", "h_1d_All_bx", 10,0,TMath::Pi());
    TH1D* h_1d_Show    = new TH1D("h_1d_Show",   "h_1d_Show",   10,0,TMath::Pi());

    events->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    events->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    events->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");

    events->SetAlias("bx2_p",  "sqrt(bx2_px*bx2_px+bx2_py*bx2_py+bx2_pz*bx2_pz)");
    events->SetAlias("bx2_pt", "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    events->SetAlias("bx2_eta","(bx2_pz>0?-1:1)*acosh(bx2_p/bx2_pt)");

    events->SetAlias("delta_eta_bx_bx2","bx_eta-bx2_eta");


    int total    = events->GetEntries();
    int selected = events->GetEntries("nXb==2");
    std::cout << "selected: " << selected << std::endl;
    std::cout << "total:    " << total    << std::endl;
    TCut cuts = "nXb==2";
    // cuts = cuts + "abs(bx_eta)>2.0&&abs(bx_eta)<5 && abs(bx2_eta)>2.0&&abs(bx2_eta)<5";
    
    // --------------------- Get crosssection -----------------------------------

    double sigmaGen;
    double sigmaErr;
    events->SetBranchAddress("sigmaGen",&sigmaGen);
    events->SetBranchAddress("sigmaErr",&sigmaErr);
    events->GetEntry(events->GetEntries()-1);
    std::cout << "SigmaGen: " << sigmaGen*microbarn << " +/- "<<sigmaErr*microbarn <<" microbarns" <<  std::endl;

    int n_entries_pythia = events->GetEntries();
    double scalefactor = sigmaGen*microbarn/n_entries_pythia;

    events->Draw("angle_bx_bx2>>h_1d_all",    "nXb==2","colz");
    events->Draw("angle_bx_bx2>>h_1d_hard_bb",cuts+"hasbStatus23&&hasbStatus23_type==5","colz");
    events->Draw("angle_bx_bx2>>h_1d_hard_bx",cuts+"hasbStatus23&&hasbStatus23_type!=5","colz");
    events->Draw("angle_bx_bx2>>h_1d_MPI_bb", cuts+"hasbStatus33&&hasbStatus33_type==5","colz");
    events->Draw("angle_bx_bx2>>h_1d_MPI_bx", cuts+"hasbStatus33&&hasbStatus33_type!=5","colz");
    events->Draw("angle_bx_bx2>>h_1d_Show",   cuts+"!(hasbStatus33||hasbStatus23)","colz");


    events->Draw("angle_bx_bx2>>h_1d_All_bb",cuts+"hasbStatus23&&hasbStatus23_type==5","colz");
    events->Draw("angle_bx_bx2>>h_1d_All_bx",cuts+"hasbStatus23&&hasbStatus23_type!=5","colz");
    events->Draw("angle_bx_bx2>>+h_1d_All_bb", cuts+"hasbStatus33&&hasbStatus33_type==5","colz");
    events->Draw("angle_bx_bx2>>+h_1d_All_bx", cuts+"hasbStatus33&&hasbStatus33_type!=5","colz");


    h_1d_all->Scale(scalefactor);
    h_1d_hard_bb->Scale(scalefactor);
    h_1d_hard_bx->Scale(scalefactor);
    h_1d_MPI_bb->Scale(scalefactor);
    h_1d_MPI_bx->Scale(scalefactor);
    h_1d_Show->Scale(scalefactor);

    h_1d_All_bb->Scale(scalefactor);
    h_1d_All_bx->Scale(scalefactor);

    h_1d_hard_bb->SetLineColor(kRed);
    h_1d_hard_bx->SetLineColor(kBlue);
    h_1d_Show->SetLineColor(kGreen+2);
    h_1d_hard_bb->SetMinimum(0.001);
    h_1d_hard_bx->SetMinimum(0.001);
    h_1d_Show->SetMinimum(0.001);

    h_1d_All_bb->SetLineColor(kRed);
    h_1d_All_bx->SetLineColor(kBlue);
    h_1d_All_bb->SetMinimum(0.001);
    h_1d_All_bx->SetMinimum(0.001);

    
    // -------------------------------------------------------------------------
    // 1D plots
    // -------------------------------------------------------------------------
    // ================================
    // All
    // ================================
    TCanvas* can_1d_all = new TCanvas("can_1d_all","can_1d_all");


    h_1d_all->SetMinimum(0.0);
    h_1d_all->Draw("hist");

    h_1d_all->SetTitle(";|#Delta#phi(X_{b},X_{#bar{b}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_all->Print("plots_BB_angles/1d_all.pdf");

    // ================================
    // hard bb
    // ================================

    TCanvas* can_1d_hard_bb = new TCanvas("can_1d_hard_bb","can_1d_hard_bb");


    h_1d_hard_bb->SetMinimum(0.0);
    h_1d_hard_bb->Draw("hist");
    h_1d_hard_bb->SetTitle(";|#Delta#phi(X_{b},X_{#bar{b}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_hard_bb->Print("plots_BB_angles/1d_hard_bb.pdf");

    // ================================
    // hard bx
    // ================================

    TCanvas* can_1d_hard_bx = new TCanvas("can_1d_hard_bx","can_1d_hard_bx");


    h_1d_hard_bx->SetMinimum(0.0);
    h_1d_hard_bx->Draw("hist");
    h_1d_hard_bx->SetTitle(";|#Delta#phi(X_{b},X_{#bar{b}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_hard_bx->Print("plots_BB_angles/1d_hard_bx.pdf");

    // ================================
    // hard bb
    // ================================

    TCanvas* can_1d_All_bb = new TCanvas("can_1d_All_bb","can_1d_All_bb");


    h_1d_All_bb->SetMinimum(0.0);
    h_1d_All_bb->Draw("hist");
    h_1d_All_bb->SetTitle(";|#Delta#phi(X_{b},X_{#bar{b}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_All_bb->Print("plots_BB_angles/1d_All_bb.pdf");

    // ================================
    // All bx
    // ================================

    TCanvas* can_1d_All_bx = new TCanvas("can_1d_All_bx","can_1d_All_bx");


    h_1d_All_bx->SetMinimum(0.0);
    h_1d_All_bx->Draw("hist");
    h_1d_All_bx->SetTitle(";|#Delta#phi(X_{b},X_{#bar{b}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_All_bx->Print("plots_BB_angles/1d_All_bx.pdf");

    // ================================
    // MPI bb
    // ================================

    TCanvas* can_1d_MPI_bb = new TCanvas("can_1d_MPI_bb","can_1d_MPI_bb");

    
    h_1d_MPI_bb->SetMinimum(0.0);
    h_1d_MPI_bb->Draw("hist");
    h_1d_MPI_bb->SetTitle(";|#Delta#phi(X_{b},X_{#bar{b}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_MPI_bb->Print("plots_BB_angles/1d_MPI_bb.pdf");


    // ================================
    // MPI bx
    // ================================

    TCanvas* can_1d_MPI_bx = new TCanvas("can_1d_MPI_bx","can_1d_MPI_bx");

    
    h_1d_MPI_bx->SetMinimum(0.0);
    h_1d_MPI_bx->Draw("hist");
    h_1d_MPI_bx->SetTitle(";|#Delta#phi(X_{b},X_{#bar{b}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_MPI_bx->Print("plots_BB_angles/1d_MPI_bx.pdf");

    
    // ================================
    // Show
    // ================================

    TCanvas* can_1d_Show = new TCanvas("can_1d_Show","can_1d_Show");


    h_1d_Show->SetMinimum(0.0);
    h_1d_Show->Draw("hist");
    h_1d_Show->SetTitle(";|#Delta#phi(X_{b},X_{#bar{b}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_Show->Print("plots_BB_angles/1d_Show.pdf");



    TH1D* h_1d_eta_all     = new TH1D("h_1d_eta_all",    "h_1d_eta_all",    10,0,7);
    TH1D* h_1d_eta_hard_bb = new TH1D("h_1d_eta_hard_bb","h_1d_eta_hard_bb",10,0,7);
    TH1D* h_1d_eta_hard_bx = new TH1D("h_1d_eta_hard_bx","h_1d_eta_hard_bx",10,0,7);
    TH1D* h_1d_eta_MPI_bb  = new TH1D("h_1d_eta_MPI_bb", "h_1d_eta_MPI_bb", 10,0,7);
    TH1D* h_1d_eta_MPI_bx  = new TH1D("h_1d_eta_MPI_bx", "h_1d_eta_MPI_bx", 10,0,7);
    TH1D* h_1d_eta_Show    = new TH1D("h_1d_eta_Show",   "h_1d_eta_Show",   10,0,7);


    // -------------------------------------------------------------------------
    // 1D plots
    // -------------------------------------------------------------------------
    // ================================
    // All
    // ================================
    TCanvas* can_1d_eta_all = new TCanvas("can_1d_eta_all","can_1d_eta_all");

    events->Draw("abs(delta_eta_bx_bx2)>>h_1d_eta_all","nXb==2","colz");

    h_1d_eta_all->SetMinimum(0.0);
    h_1d_eta_all->Draw();

    h_1d_eta_all->SetTitle(";|#Delta#phi(X_{b},X_{#bar{b}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_eta_all->Print("plots_BB_angles/eta_1d_all.pdf");

    // ================================
    // hard bb
    // ================================

    TCanvas* can_1d_eta_hard_bb = new TCanvas("can_1d_eta_hard_bb","can_1d_eta_hard_bb");

    events->Draw("abs(delta_eta_bx_bx2)>>h_1d_eta_hard_bb",cuts+"hasbStatus23&&hasbStatus23_type==5","colz");

    h_1d_eta_hard_bb->SetMinimum(0.0);
    h_1d_eta_hard_bb->Draw();
    h_1d_eta_hard_bb->SetTitle(";#Delta#eta(X_{b},X_{#bar{b}});Events");
    can_1d_eta_hard_bb->Print("plots_BB_angles/eta_1d_hard_bb.pdf");

    // ================================
    // hard bx
    // ================================

    TCanvas* can_1d_eta_hard_bx = new TCanvas("can_1d_eta_hard_bx","can_1d_eta_hard_bx");

    events->Draw("abs(delta_eta_bx_bx2)>>h_1d_eta_hard_bx",cuts+"hasbStatus23&&hasbStatus23_type!=5","colz");

    h_1d_eta_hard_bx->SetMinimum(0.0);
    h_1d_eta_hard_bx->Draw();
    h_1d_eta_hard_bx->SetTitle(";#Delta#eta(X_{b},X_{#bar{b}});Events");
    can_1d_eta_hard_bx->Print("plots_BB_angles/eta_1d_hard_bx.pdf");

    // ================================
    // MPI bb
    // ================================

    TCanvas* can_1d_eta_MPI_bb = new TCanvas("can_1d_eta_MPI_bb","can_1d_eta_MPI_bb");

    events->Draw("abs(delta_eta_bx_bx2)>>h_1d_eta_MPI_bb",cuts+"hasbStatus33&&hasbStatus33_type==5","colz");
    
    h_1d_eta_MPI_bb->SetMinimum(0.0);
    h_1d_eta_MPI_bb->Draw();
    h_1d_eta_MPI_bb->SetTitle(";#Delta#eta(X_{b},X_{#bar{b}});Events");
    can_1d_eta_MPI_bb->Print("plots_BB_angles/eta_1d_MPI_bb.pdf");


    // ================================
    // MPI bx
    // ================================

    TCanvas* can_1d_eta_MPI_bx = new TCanvas("can_1d_eta_MPI_bx","can_1d_eta_MPI_bx");

    events->Draw("abs(delta_eta_bx_bx2)>>h_1d_eta_MPI_bx",cuts+"hasbStatus33&&hasbStatus33_type!=5","colz");
    
    h_1d_eta_MPI_bx->SetMinimum(0.0);
    h_1d_eta_MPI_bx->Draw();
    h_1d_eta_MPI_bx->SetTitle(";#Delta#eta(X_{b},X_{#bar{b}});Events");
    can_1d_eta_MPI_bx->Print("plots_BB_angles/eta_1d_MPI_bx.pdf");

    
    // ================================
    // Show
    // ================================

    TCanvas* can_1d_eta_Show = new TCanvas("can_1d_eta_Show","can_1d_eta_Show");

    events->Draw("abs(delta_eta_bx_bx2)>>h_1d_eta_Show",cuts+"!(hasbStatus33||hasbStatus23)","colz");

    h_1d_eta_Show->SetMinimum(0.0);
    h_1d_eta_Show->Draw();
    h_1d_eta_Show->SetTitle(";#Delta#eta(X_{b},X_{#bar{b}});Events");
    can_1d_eta_Show->Print("plots_BB_angles/eta_1d_Show.pdf");


    
    // ================================
    // Plot all 
    // ================================
    TCanvas* can_1d_all_bb = new TCanvas("can_1d_all_bb","can_1d_all_bb");

    // h_1d_hard_bb->Scale(1.0/h_1d_hard_bb->Integral());
    // h_1d_hard_bx->Scale(1.0/h_1d_hard_bx->Integral());
    // h_1d_Show->Scale(1.0/h_1d_Show->Integral());

    double h_1d_hard_bb_bin_width = h_1d_hard_bb->GetXaxis()->GetBinWidth(2);
    h_1d_hard_bb->SetTitle(Form(";#Delta#phi [rad]; #scale[0.6]{#frac{d#sigma(X_{b}X_{#bar{b}})}{d#Delta#phi}} [%.1f #mub rad^{-1}]",h_1d_hard_bb_bin_width));

    h_1d_hard_bb->Draw("][hist");
    h_1d_hard_bx->Draw("][hist same");
    h_1d_Show->Draw("][hist same");
    // gPad->SetLogy();

    TLegend* leg = new TLegend(0.2,0.3,0.6,0.6);
    leg->AddEntry(h_1d_hard_bb,"Pair creation","l");
    leg->AddEntry(h_1d_hard_bx,"Flavour excitation","l");
    leg->AddEntry(h_1d_Show,   "Gluon splitting","l");
    leg->SetTextFont(132);
    leg->SetFillStyle(0);
    leg->Draw(); 
    can_1d_all_bb->Print("plots_BB_angles/1d_all_overlaid_bb.pdf");



}