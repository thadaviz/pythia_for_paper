#include "tools.h"

void plot_CC_angles(){
    SetLHCbStyle("oneD");

    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    // TFile* tfile = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_50000_noMPI.root");
    // TFile* tfile = TFile::Open("../output/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_50000_noMPI_PartMod1.root");
    TFile* tfile = TFile::Open("../output/main202output_SoftQCD_nd_UserHook_Minni_Scale_1.300000_Quark_4_N_50000_noMPI_PartMod1.root");

    TTree* events = (TTree*)tfile->Get("events");

    events->SetAlias("dx_pt" ,  "sqrt(dx_px*dx_px+dx_py*dx_py)");
    events->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");

    events->SetAlias("angle_cx_cx2","acos((dx_px*dx2_px + dx_py*dx2_py)/(dx_pt*dx2_pt))");

    // Plot 2d for  Bc
    TH1D* h_1d_all  = new TH1D("h_1d_all", "h_1d_all", 10,0,TMath::Pi());
    TH1D* h_1d_hard_cc = new TH1D("h_1d_hard_cc","h_1d_hard_cc",10,0,TMath::Pi());
    TH1D* h_1d_hard_cx = new TH1D("h_1d_hard_cx","h_1d_hard_cx",10,0,TMath::Pi());
    TH1D* h_1d_MPI_cc  = new TH1D("h_1d_MPI_cc", "h_1d_MPI_cc", 10,0,TMath::Pi());
    TH1D* h_1d_MPI_cx  = new TH1D("h_1d_MPI_cx", "h_1d_MPI_cx", 10,0,TMath::Pi());
    TH1D* h_1d_Show    = new TH1D("h_1d_Show",   "h_1d_Show",   10,0,TMath::Pi());

    events->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    events->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    events->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");

    events->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    events->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    events->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");

    events->SetAlias("delta_eta_dx_dx2","dx_eta-dx2_eta");


    int total    = events->GetEntries();
    int selected = events->GetEntries("nXc==2");
    std::cout << "selected: " << selected << std::endl;
    std::cout << "total:    " << total    << std::endl;
    TCut cuts = "nXc==2";
    // cuts = cuts + "abs(dx_eta)>2.0&&abs(dx_eta)<5 && abs(dx2_eta)>2.0&&abs(dx2_eta)<5";
    
    // --------------------- Get crosssection -----------------------------------

    double sigmaGen;
    double sigmaErr;
    events->SetBranchAddress("sigmaGen",&sigmaGen);
    events->SetBranchAddress("sigmaErr",&sigmaErr);
    events->GetEntry(events->GetEntries()-1);
    std::cout << "SigmaGen: " << sigmaGen*microbarn << " +/- "<<sigmaErr*microbarn <<" microbarns" <<  std::endl;

    int n_entries_pythia = events->GetEntries();
    double scalefactor = sigmaGen*microbarn/n_entries_pythia;

    events->Draw("angle_cx_cx2>>h_1d_all",    "nXc==2","colz");
    events->Draw("angle_cx_cx2>>h_1d_hard_cc",cuts+"hascStatus23&&hasbStatus23_type==4","colz");
    events->Draw("angle_cx_cx2>>h_1d_hard_cx",cuts+"hascStatus23&&hasbStatus23_type!=4","colz");
    events->Draw("angle_cx_cx2>>h_1d_MPI_cc", cuts+"hascStatus33&&hasbStatus33_type==4","colz");
    events->Draw("angle_cx_cx2>>h_1d_MPI_cx", cuts+"hascStatus33&&hasbStatus33_type!=4","colz");
    events->Draw("angle_cx_cx2>>h_1d_Show",   cuts+"!(hascStatus33||hascStatus23)","colz");

    h_1d_all->Scale(scalefactor);
    h_1d_hard_cc->Scale(scalefactor);
    h_1d_hard_cx->Scale(scalefactor);
    h_1d_MPI_cc->Scale(scalefactor);
    h_1d_MPI_cx->Scale(scalefactor);
    h_1d_Show->Scale(scalefactor);

    h_1d_hard_cc->SetLineColor(kRed);
    h_1d_hard_cx->SetLineColor(kBlue);
    h_1d_Show->SetLineColor(kGreen+2);
    h_1d_hard_cc->SetMinimum(0.001);
    h_1d_hard_cx->SetMinimum(0.001);
    h_1d_Show->SetMinimum(0.001);
    
    // -------------------------------------------------------------------------
    // 1D plots
    // -------------------------------------------------------------------------
    // ================================
    // All
    // ================================
    TCanvas* can_1d_all = new TCanvas("can_1d_all","can_1d_all");


    h_1d_all->SetMinimum(0.0);
    h_1d_all->Draw("hist");

    h_1d_all->SetTitle(";|#Delta#phi(X_{c},X_{#bar{c}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_all->Print("plots_CC_angles/1d_all.pdf");

    // ================================
    // hard bb
    // ================================

    TCanvas* can_1d_hard_cc = new TCanvas("can_1d_hard_cc","can_1d_hard_cc");


    h_1d_hard_cc->SetMinimum(0.0);
    h_1d_hard_cc->Draw("hist");
    h_1d_hard_cc->SetTitle(";|#Delta#phi(X_{c},X_{#bar{c}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_hard_cc->Print("plots_CC_angles/1d_hard_cc.pdf");

    // ================================
    // hard cx
    // ================================

    TCanvas* can_1d_hard_cx = new TCanvas("can_1d_hard_cx","can_1d_hard_cx");


    h_1d_hard_cx->SetMinimum(0.0);
    h_1d_hard_cx->Draw("hist");
    h_1d_hard_cx->SetTitle(";|#Delta#phi(X_{c},X_{#bar{c}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_hard_cx->Print("plots_CC_angles/1d_hard_cx.pdf");

    // ================================
    // MPI bb
    // ================================

    TCanvas* can_1d_MPI_cc = new TCanvas("can_1d_MPI_cc","can_1d_MPI_cc");

    
    h_1d_MPI_cc->SetMinimum(0.0);
    h_1d_MPI_cc->Draw("hist");
    h_1d_MPI_cc->SetTitle(";|#Delta#phi(X_{c},X_{#bar{c}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_MPI_cc->Print("plots_CC_angles/1d_MPI_cc.pdf");


    // ================================
    // MPI cx
    // ================================

    TCanvas* can_1d_MPI_cx = new TCanvas("can_1d_MPI_cx","can_1d_MPI_cx");

    
    h_1d_MPI_cx->SetMinimum(0.0);
    h_1d_MPI_cx->Draw("hist");
    h_1d_MPI_cx->SetTitle(";|#Delta#phi(X_{c},X_{#bar{c}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_MPI_cx->Print("plots_CC_angles/1d_MPI_cx.pdf");

    
    // ================================
    // Show
    // ================================

    TCanvas* can_1d_Show = new TCanvas("can_1d_Show","can_1d_Show");


    h_1d_Show->SetMinimum(0.0);
    h_1d_Show->Draw("hist");
    h_1d_Show->SetTitle(";|#Delta#phi(X_{c},X_{#bar{c}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_Show->Print("plots_CC_angles/1d_Show.pdf");



    TH1D* h_1d_eta_all     = new TH1D("h_1d_eta_all",    "h_1d_eta_all",    10,0,7);
    TH1D* h_1d_eta_hard_cc = new TH1D("h_1d_eta_hard_cc","h_1d_eta_hard_cc",10,0,7);
    TH1D* h_1d_eta_hard_cx = new TH1D("h_1d_eta_hard_cx","h_1d_eta_hard_cx",10,0,7);
    TH1D* h_1d_eta_MPI_cc  = new TH1D("h_1d_eta_MPI_cc", "h_1d_eta_MPI_cc", 10,0,7);
    TH1D* h_1d_eta_MPI_cx  = new TH1D("h_1d_eta_MPI_cx", "h_1d_eta_MPI_cx", 10,0,7);
    TH1D* h_1d_eta_Show    = new TH1D("h_1d_eta_Show",   "h_1d_eta_Show",   10,0,7);


    // -------------------------------------------------------------------------
    // 1D plots
    // -------------------------------------------------------------------------
    // ================================
    // All
    // ================================
    TCanvas* can_1d_eta_all = new TCanvas("can_1d_eta_all","can_1d_eta_all");

    events->Draw("abs(delta_eta_dx_dx2)>>h_1d_eta_all","nXc==2","colz");

    h_1d_eta_all->SetMinimum(0.0);
    h_1d_eta_all->Draw();

    h_1d_eta_all->SetTitle(";|#Delta#phi(X_{c},X_{#bar{c}})| [rad];d#sigma/d#Delta#phi [#pi/10 #mub rad^{-1}]");
    can_1d_eta_all->Print("plots_CC_angles/eta_1d_all.pdf");

    // ================================
    // hard bb
    // ================================

    TCanvas* can_1d_eta_hard_cc = new TCanvas("can_1d_eta_hard_cc","can_1d_eta_hard_cc");

    events->Draw("abs(delta_eta_dx_dx2)>>h_1d_eta_hard_cc",cuts+"hasbStatus23&&hasbStatus23_type==5","colz");

    h_1d_eta_hard_cc->SetMinimum(0.0);
    h_1d_eta_hard_cc->Draw();
    h_1d_eta_hard_cc->SetTitle(";#Delta#eta(X_{c},X_{#bar{c}});Events");
    can_1d_eta_hard_cc->Print("plots_CC_angles/eta_1d_hard_cc.pdf");

    // ================================
    // hard cx
    // ================================

    TCanvas* can_1d_eta_hard_cx = new TCanvas("can_1d_eta_hard_cx","can_1d_eta_hard_cx");

    events->Draw("abs(delta_eta_dx_dx2)>>h_1d_eta_hard_cx",cuts+"hasbStatus23&&hasbStatus23_type!=5","colz");

    h_1d_eta_hard_cx->SetMinimum(0.0);
    h_1d_eta_hard_cx->Draw();
    h_1d_eta_hard_cx->SetTitle(";#Delta#eta(X_{c},X_{#bar{c}});Events");
    can_1d_eta_hard_cx->Print("plots_CC_angles/eta_1d_hard_cx.pdf");

    // ================================
    // MPI bb
    // ================================

    TCanvas* can_1d_eta_MPI_cc = new TCanvas("can_1d_eta_MPI_cc","can_1d_eta_MPI_cc");

    events->Draw("abs(delta_eta_dx_dx2)>>h_1d_eta_MPI_cc",cuts+"hasbStatus33&&hasbStatus33_type==5","colz");
    
    h_1d_eta_MPI_cc->SetMinimum(0.0);
    h_1d_eta_MPI_cc->Draw();
    h_1d_eta_MPI_cc->SetTitle(";#Delta#eta(X_{c},X_{#bar{c}});Events");
    can_1d_eta_MPI_cc->Print("plots_CC_angles/eta_1d_MPI_cc.pdf");


    // ================================
    // MPI cx
    // ================================

    TCanvas* can_1d_eta_MPI_cx = new TCanvas("can_1d_eta_MPI_cx","can_1d_eta_MPI_cx");

    events->Draw("abs(delta_eta_dx_dx2)>>h_1d_eta_MPI_cx",cuts+"hasbStatus33&&hasbStatus33_type!=5","colz");
    
    h_1d_eta_MPI_cx->SetMinimum(0.0);
    h_1d_eta_MPI_cx->Draw();
    h_1d_eta_MPI_cx->SetTitle(";#Delta#eta(X_{c},X_{#bar{c}});Events");
    can_1d_eta_MPI_cx->Print("plots_CC_angles/eta_1d_MPI_cx.pdf");

    
    // ================================
    // Show
    // ================================

    TCanvas* can_1d_eta_Show = new TCanvas("can_1d_eta_Show","can_1d_eta_Show");

    events->Draw("abs(delta_eta_dx_dx2)>>h_1d_eta_Show",cuts+"!(hasbStatus33||hasbStatus23)","colz");

    h_1d_eta_Show->SetMinimum(0.0);
    h_1d_eta_Show->Draw();
    h_1d_eta_Show->SetTitle(";#Delta#eta(X_{c},X_{#bar{c}});Events");
    can_1d_eta_Show->Print("plots_CC_angles/eta_1d_Show.pdf");


    
    // ================================
    // Plot all 
    // ================================
    TCanvas* can_1d_all_cc = new TCanvas("can_1d_all_cc","can_1d_all_cc");

    // h_1d_hard_cc->Scale(1.0/h_1d_hard_cc->Integral());
    // h_1d_hard_cx->Scale(1.0/h_1d_hard_cx->Integral());
    // h_1d_Show->Scale(1.0/h_1d_Show->Integral());


    h_1d_hard_cc->Draw("hist");
    h_1d_hard_cx->Draw("hist same");
    h_1d_Show->Draw("hist same");
    // gPad->SetLogy();
    can_1d_all_cc->Print("plots_CC_angles/1d_all_cc.pdf");



}