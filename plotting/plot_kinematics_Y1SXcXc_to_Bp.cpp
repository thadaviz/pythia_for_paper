#include "tools.h"

void plot_kinematics_Y1SXcXc_to_Bp(){
    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;


    std::string filename_pythia_bbcc     = "../output/from_lxplus/Complete_paper_samples/Sample_bbcc/main202output_SoftQCD_nd_UserHook_bbcc_N_14800000_PartMod1_noColRec.root";
    std::string filename_pythia_bb       = "../output/from_lxplus/Complete_paper_samples/Sample_bb/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_10000000_PartMod1_noColRec.root";


    // -------------------------------------------------------------------------
    // Fig cuts
    // -------------------------------------------------------------------------

    // TCut Upsilon1S_fid = "upsilon1s_y > 2.0 && upsilon1s_y < 4.5";
    TCut Upsilon1S_fid = "";

    // TCut Dz_fid = "Bu_p_y > 2.0 && Bu_p_y < 4.5";
    TCut Dz_fid = "";

    TCut cuts_Y1SXcXc_all    = "nUpsilon1S>0 && nXc==2";
    TCut cuts_Dz_all          = "nBu_p>=1";


    TCut cut_DPS_Sep  = "(((Upsilon1S_isHard||Upsilon1S_isMPI)&&Upsilon1S_isHard_partSys!=dx2_partSys)||(Upsilon1S_iscc && Upsilon1S_iscc_c_partSys!=dx2_partSys))"; 
    TCut cut_SPS_Sep = " (((Upsilon1S_isHard||Upsilon1S_isMPI)&&Upsilon1S_isHard_partSys==dx2_partSys)||(Upsilon1S_iscc && Upsilon1S_iscc_c_partSys==dx2_partSys))";

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file_all = TFile::Open(filename_pythia_bb.c_str());
    TTree* pythia_tree_all = (TTree*) pythia_file_all->Get("events");

    pythia_tree_all->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_all->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_all->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree_all->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree_all->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");
    pythia_tree_all->SetAlias("angle_upsilon1s_dx2","acos((upsilon1s_px*dx2_px + upsilon1s_py*dx2_py)/(upsilon1s_pt*dx2_pt))");


    pythia_tree_all->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree_all->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_all->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree_all->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree_all->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree_all->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_all->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree_all->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree_all->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree_all->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_all->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree_all->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree_all->SetAlias("Bu_p_p",  "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py+Bu_p_pz*Bu_p_pz)");
    pythia_tree_all->SetAlias("Bu_p_pt", "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py)");
    pythia_tree_all->SetAlias("Bu_p_eta","(Bu_p_pz>0?-1:1)*acosh(Bu_p_p/Bu_p_pt)");
    pythia_tree_all->SetAlias("Bu_p_y","0.5*log( (Bu_p_pe+Bu_p_pz) / (Bu_p_pe-Bu_p_pz) )");

    pythia_tree_all->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree_all->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");
    pythia_tree_all->SetAlias("delta_eta_upsilon1s_dx2","upsilon1s_eta-dx2_eta");

    pythia_tree_all->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree_all->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");
    pythia_tree_all->SetAlias("delta_y_upsilon1s_dx2","upsilon1s_y-dx2_y");

    pythia_tree_all->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree_all->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");
    pythia_tree_all->SetAlias("delta_R_upsilon1s_dx2","sqrt(delta_eta_upsilon1s_dx2*delta_eta_upsilon1s_dx2+angle_upsilon1s_dx2*angle_upsilon1s_dx2)");

    double sigmaGen_pythia_all;
    double sigmaErr_pythia_all;
    pythia_tree_all->SetBranchAddress("sigmaGen",&sigmaGen_pythia_all);
    pythia_tree_all->SetBranchAddress("sigmaErr",&sigmaErr_pythia_all);
    
    int n_entries_pythia_all = pythia_tree_all->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open(filename_pythia_bbcc.c_str());
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");

    pythia_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_dx2","acos((upsilon1s_px*dx2_px + upsilon1s_py*dx2_py)/(upsilon1s_pt*dx2_pt))");


    pythia_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");

    pythia_tree->SetAlias("Bu_p_p",  "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py+Bu_p_pz*Bu_p_pz)");
    pythia_tree->SetAlias("Bu_p_pt", "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py)");
    pythia_tree->SetAlias("Bu_p_eta","(Bu_p_pz>0?-1:1)*acosh(Bu_p_p/Bu_p_pt)");
    pythia_tree->SetAlias("Bu_p_y","0.5*log( (Bu_p_pe+Bu_p_pz) / (Bu_p_pe-Bu_p_pz) )");

    pythia_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_dx2","upsilon1s_eta-dx2_eta");

    pythia_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_dx2","upsilon1s_y-dx2_y");

    pythia_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_dx2","sqrt(delta_eta_upsilon1s_dx2*delta_eta_upsilon1s_dx2+angle_upsilon1s_dx2*angle_upsilon1s_dx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    pythia_tree_all->GetEntry(pythia_tree_all->GetEntries()-1);
 
    std::cout << "Cross section Pythia:        " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nUpsilon1S>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
   
    std::cout << "Cross section Pythia all:  " << sigmaGen_pythia_all*microbarn  << " +/- " << sigmaErr_pythia_all*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree_all->GetEntries("nUpsilon1S>0")<< "/" << pythia_tree_all->GetEntries("")<< std::endl;
    

    double scalefactor_pythia     = sigmaGen_pythia*microbarn/n_entries_pythia;
    double scalefactor_pythia_all = sigmaGen_pythia_all*microbarn/n_entries_pythia_all;


    // -------------------------------------------------------------------------
    // Plot multiplicity
    // -------------------------------------------------------------------------
    TCanvas* c_nMPI = new TCanvas("c_nMPI","c_nMPI");
    TH1D* h_nMPI_pythia  = new TH1D("h_nMPI_pythia", "h_nMPI_pythia", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Sep  = new TH1D("h_nMPI_pythia_DPS_Sep", "h_nMPI_pythia_DPS_Sep", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Sep  = new TH1D("h_nMPI_pythia_SPS_Sep", "h_nMPI_pythia_SPS_Sep", 13,0,26);

    TH1D* h_nMPI_pythia_Dz  = new TH1D("h_nMPI_pythia_Dz", "h_nMPI_pythia_Dz", 13,0,26);

    pythia_tree->Draw("nMPI>>h_nMPI_pythia",cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_DPS_Sep",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Sep);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia_SPS_Sep",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Sep);
    pythia_tree->Draw("nMPI>>h_nMPI_pythia",cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree_all->Draw("nMPI>>h_nMPI_pythia_Dz",cuts_Dz_all+Dz_fid);

    h_nMPI_pythia->Scale(scalefactor_pythia);
    h_nMPI_pythia_DPS_Sep->Scale(scalefactor_pythia);
    h_nMPI_pythia_SPS_Sep->Scale(scalefactor_pythia);
    h_nMPI_pythia_Dz->Scale(scalefactor_pythia_all);


    // Black -> With MPI
    h_nMPI_pythia->SetLineColor(kBlack);

    h_nMPI_pythia_Dz->SetLineColor(kBlack);
    h_nMPI_pythia_Dz->SetLineStyle(kDashed);


    h_nMPI_pythia_DPS_Sep->SetLineColor(kBlue);
    h_nMPI_pythia_SPS_Sep->SetLineColor(kRed);


    h_nMPI_pythia->SetMaximum(1.05*max({h_nMPI_pythia->GetMaximum(),
                                      h_nMPI_pythia_Dz->GetMaximum(),
                                      h_nMPI_pythia_DPS_Sep->GetMaximum(),
                                      h_nMPI_pythia_SPS_Sep->GetMaximum()
                                      }));
    
    double h_nMPI_pythia_bin_width = h_nMPI_pythia->GetXaxis()->GetBinWidth(2);
    h_nMPI_pythia->SetTitle(Form(";N_{MPI}; d#sigma/dN [%.1f #mub]",h_nMPI_pythia_bin_width));
    h_nMPI_pythia->Draw("hist");
    h_nMPI_pythia_Dz->Draw("same hist");
    h_nMPI_pythia_DPS_Sep->Draw("same hist");
    h_nMPI_pythia_SPS_Sep->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_nMPI = new TLegend(0.6,0.6,0.9,0.9);
    leg_nMPI->AddEntry(h_nMPI_pythia_Dz,     "B^{+} Pythia","l");
    leg_nMPI->AddEntry(h_nMPI_pythia,        "#Upsilon(1S)X_{c}X_{#bar{c}} Pythia","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_DPS_Sep,"#Upsilon(1S)X_{c}X_{#bar{c}} DPS","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_SPS_Sep,"#Upsilon(1S)X_{c}X_{#bar{c}} SPS","l");

    leg_nMPI->SetTextFont(132);
    leg_nMPI->SetFillStyle(0);
    leg_nMPI->Draw();
    c_nMPI->Print("plots_kinematics_Y1SXcXc_to_Bu/c_nMPI.pdf");

    TCanvas* c_nMPI_ratio = new TCanvas("c_nMPI_ratio","c_nMPI_ratio");

    TH1D* h_nMPI_pythia_ratio  = new TH1D("h_nMPI_pythia_ratio", "h_nMPI_pythia_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_DPS_Sep_ratio  = new TH1D("h_nMPI_pythia_DPS_Sep_ratio", "h_nMPI_pythia_DPS_Sep_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_Sep_ratio  = new TH1D("h_nMPI_pythia_SPS_Sep_ratio", "h_nMPI_pythia_SPS_Sep_ratio", 13,0,26);

    h_nMPI_pythia_ratio->Divide(h_nMPI_pythia,h_nMPI_pythia_Dz);
    h_nMPI_pythia_DPS_Sep_ratio->Divide(h_nMPI_pythia_DPS_Sep,h_nMPI_pythia_Dz);
    h_nMPI_pythia_SPS_Sep_ratio->Divide(h_nMPI_pythia_SPS_Sep,h_nMPI_pythia_Dz);


    h_nMPI_pythia_ratio->Scale(100);
    h_nMPI_pythia_DPS_Sep_ratio->Scale(100);
    h_nMPI_pythia_SPS_Sep_ratio->Scale(100);
    
        // Black -> With MPI
    h_nMPI_pythia_ratio->SetLineColor(kBlack);


    h_nMPI_pythia_DPS_Sep_ratio->SetLineColor(kBlue);
    h_nMPI_pythia_DPS_Sep_ratio->SetMarkerColor(kBlue);

    h_nMPI_pythia_SPS_Sep_ratio->SetLineColor(kRed);
    h_nMPI_pythia_SPS_Sep_ratio->SetMarkerColor(kRed);

    h_nMPI_pythia_ratio->SetMaximum(1.05*max({h_nMPI_pythia_ratio->GetMaximum(),
                                              h_nMPI_pythia_DPS_Sep_ratio->GetMaximum(),
                                              h_nMPI_pythia_SPS_Sep_ratio->GetMaximum()
                                      }));
    h_nMPI_pythia_ratio->SetTitle(";N_{MPI};#scale[0.6]{#frac{d#sigma(#Upsilon(1S)X_{c}X_{#bar{c}})}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_nMPI_pythia_ratio->SetMinimum(0);
    h_nMPI_pythia_ratio->Draw();
    h_nMPI_pythia_DPS_Sep_ratio->Draw("same");
    h_nMPI_pythia_SPS_Sep_ratio->Draw("same");


    TLegend* leg_nMPI_ratio = new TLegend(0.56,0.28,0.92,0.58);
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_ratio,        "Pythia - All","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_DPS_Sep_ratio,"Pythia - Just DPS","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_SPS_Sep_ratio,"Pythia - Just SPS","pe");
    leg_nMPI_ratio->SetTextFont(132);
    leg_nMPI_ratio->SetFillStyle(0);
    leg_nMPI_ratio->Draw();
    c_nMPI_ratio->Print("plots_kinematics_Y1SXcXc_to_Bu/c_nMPI_ratio.pdf");



    // -------------------------------------------------------------------------
    // Plot multiplicity
    // -------------------------------------------------------------------------
    TCanvas* c_multiplicity = new TCanvas("c_multiplicity","c_multiplicity");
    TH1D* h_multiplicity_pythia  = new TH1D("h_multiplicity_pythia", "h_multiplicity_pythia", 15,0,80);
    TH1D* h_multiplicity_pythia_DPS_Sep  = new TH1D("h_multiplicity_pythia_DPS_Sep", "h_multiplicity_pythia_DPS_Sep", 15,0,80);
    TH1D* h_multiplicity_pythia_SPS_Sep  = new TH1D("h_multiplicity_pythia_SPS_Sep", "h_multiplicity_pythia_SPS_Sep", 15,0,80);

    TH1D* h_multiplicity_pythia_Dz  = new TH1D("h_multiplicity_pythia_Dz", "h_multiplicity_pythia_Dz", 15,0,80);

    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia",cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_DPS_Sep",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Sep);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia_SPS_Sep",cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Sep);
    pythia_tree->Draw("nChargedInLHCb>>h_multiplicity_pythia",cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree_all->Draw("nChargedInLHCb>>h_multiplicity_pythia_Dz",cuts_Dz_all+Dz_fid);

    h_multiplicity_pythia->Scale(scalefactor_pythia);
    h_multiplicity_pythia_DPS_Sep->Scale(scalefactor_pythia);
    h_multiplicity_pythia_SPS_Sep->Scale(scalefactor_pythia);
    h_multiplicity_pythia_Dz->Scale(scalefactor_pythia_all);


    // Black -> With MPI
    h_multiplicity_pythia->SetLineColor(kBlack);

    h_multiplicity_pythia_Dz->SetLineColor(kBlack);
    h_multiplicity_pythia_Dz->SetLineStyle(kDashed);



    h_multiplicity_pythia_DPS_Sep->SetLineColor(kBlue);
    h_multiplicity_pythia_SPS_Sep->SetLineColor(kRed);



    h_multiplicity_pythia->SetMaximum(1.05*max({h_multiplicity_pythia->GetMaximum(),
                                      h_multiplicity_pythia_Dz->GetMaximum(),
                                      h_multiplicity_pythia_DPS_Sep->GetMaximum(),
                                      h_multiplicity_pythia_SPS_Sep->GetMaximum()
                                      }));
    
    double h_multiplicity_pythia_bin_width = h_multiplicity_pythia->GetXaxis()->GetBinWidth(2);
    h_multiplicity_pythia->SetTitle(Form(";N_{Charged}^{2.0<#eta<4.5}; d#sigma/dN [%.1f #mub]",h_multiplicity_pythia_bin_width));
    h_multiplicity_pythia->Draw("hist");
    h_multiplicity_pythia_Dz->Draw("same hist");
    h_multiplicity_pythia_DPS_Sep->Draw("same hist");
    h_multiplicity_pythia_SPS_Sep->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_multiplicity = new TLegend(0.6,0.6,0.9,0.9);
    leg_multiplicity->AddEntry(h_multiplicity_pythia_Dz,     "B^{+} Pythia","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia,        "#Upsilon(1S)X_{c}X_{#bar{c}} Pythia","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_DPS_Sep,"#Upsilon(1S)X_{c}X_{#bar{c}} DPS","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_SPS_Sep,"#Upsilon(1S)X_{c}X_{#bar{c}} SPS","l");

    leg_multiplicity->SetTextFont(132);
    leg_multiplicity->SetFillStyle(0);
    leg_multiplicity->Draw();
    c_multiplicity->Print("plots_kinematics_Y1SXcXc_to_Bu/c_multiplicity.pdf");

    TCanvas* c_multiplicity_ratio = new TCanvas("c_multiplicity_ratio","c_multiplicity_ratio");

    TH1D* h_multiplicity_pythia_ratio  = new TH1D("h_multiplicity_pythia_ratio", "h_multiplicity_pythia_ratio", 15,0,80);
    TH1D* h_multiplicity_pythia_DPS_Sep_ratio  = new TH1D("h_multiplicity_pythia_DPS_Sep_ratio", "h_multiplicity_pythia_DPS_Sep_ratio", 15,0,80);
    TH1D* h_multiplicity_pythia_SPS_Sep_ratio  = new TH1D("h_multiplicity_pythia_SPS_Sep_ratio", "h_multiplicity_pythia_SPS_Sep_ratio", 15,0,80);

    h_multiplicity_pythia_ratio->Divide(h_multiplicity_pythia,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_DPS_Sep_ratio->Divide(h_multiplicity_pythia_DPS_Sep,h_multiplicity_pythia_Dz);
    h_multiplicity_pythia_SPS_Sep_ratio->Divide(h_multiplicity_pythia_SPS_Sep,h_multiplicity_pythia_Dz);


    h_multiplicity_pythia_ratio->Scale(100);
    h_multiplicity_pythia_DPS_Sep_ratio->Scale(100);
    h_multiplicity_pythia_SPS_Sep_ratio->Scale(100);
    
        // Black -> With MPI
    h_multiplicity_pythia_ratio->SetLineColor(kBlack);

    h_multiplicity_pythia_DPS_Sep_ratio->SetLineColor(kBlue);
    h_multiplicity_pythia_DPS_Sep_ratio->SetMarkerColor(kBlue);

    h_multiplicity_pythia_SPS_Sep_ratio->SetLineColor(kRed);
    h_multiplicity_pythia_SPS_Sep_ratio->SetMarkerColor(kRed);

    h_multiplicity_pythia_ratio->SetMaximum(1.05*max({h_multiplicity_pythia_ratio->GetMaximum(),
                                              h_multiplicity_pythia_DPS_Sep_ratio->GetMaximum(),
                                              h_multiplicity_pythia_SPS_Sep_ratio->GetMaximum()
                                      }));
    h_multiplicity_pythia_ratio->SetTitle(";N_{Charged}^{2.0<#eta<4.5};#scale[0.6]{#frac{d#sigma(#Upsilon(1S)X_{c}X_{#bar{c}})}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_multiplicity_pythia_ratio->SetMinimum(0);
    h_multiplicity_pythia_ratio->Draw();
    h_multiplicity_pythia_DPS_Sep_ratio->Draw("same");
    h_multiplicity_pythia_SPS_Sep_ratio->Draw("same");


    TLegend* leg_multiplicity_ratio = new TLegend(0.54,0.29,0.91,0.6);
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_ratio,        "Pythia - All","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_DPS_Sep_ratio,"Pythia - Just DPS","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_SPS_Sep_ratio,"Pythia - Just SPS","pe");
    leg_multiplicity_ratio->SetTextFont(132);
    leg_multiplicity_ratio->SetFillStyle(0);
    leg_multiplicity_ratio->Draw();
    c_multiplicity_ratio->Print("plots_kinematics_Y1SXcXc_to_Bu/c_multiplicity_ratio.pdf");



    // -------------------------------------------------------------------------
    // Compare to LHCb measurements
    // -------------------------------------------------------------------------

    SetLHCbStyle("oneD");
    TCanvas* c_lhcb_comp = new TCanvas("c_lhcb_comp","c_lhcb_comp");
    TH1D* h_lhcb_comp_SPS        = new TH1D("h_lhcb_comp_SPS",       "h_lhcb_comp_SPS",       10,0,1.0);
    TH1D* h_lhcb_comp_all  = new TH1D("h_lhcb_comp_all", "h_lhcb_comp_all", 10,0,1.0);
    
    TCut cuts_fid_dx  = "(upsilon1s_eta)>2 && (upsilon1s_eta)<5 &&  (dx_eta )>2 && (dx_eta )<5  ";
    TCut cuts_fid_dx2 = "(upsilon1s_eta)>2 && (upsilon1s_eta)<5 &&  (dx2_eta)>2 && (dx2_eta)<5  ";

    pythia_tree->Draw("angle_upsilon1s_dx/TMath::Pi() >> h_lhcb_comp_SPS",  cuts_Y1SXcXc_all+cuts_fid_dx +cut_SPS_Sep);
    pythia_tree->Draw("angle_upsilon1s_dx/TMath::Pi() >> h_lhcb_comp_all",  cuts_Y1SXcXc_all+cuts_fid_dx );
    pythia_tree->Draw("angle_upsilon1s_dx2/TMath::Pi()>>+h_lhcb_comp_SPS",  cuts_Y1SXcXc_all+cuts_fid_dx2+cut_SPS_Sep);
    pythia_tree->Draw("angle_upsilon1s_dx2/TMath::Pi()>>+h_lhcb_comp_all",  cuts_Y1SXcXc_all+cuts_fid_dx2);

    TH1D *h_lhcb_Dz = new TH1D("h_lhcb_Dz","",10,0,1);
    h_lhcb_Dz->SetLineColor(kBlue);
    h_lhcb_Dz->SetMarkerColor(kBlue);  

    h_lhcb_Dz->SetBinContent(1,0.1703914);
    h_lhcb_Dz->SetBinContent(2,0.1089607);
    h_lhcb_Dz->SetBinContent(3,0.1301497);
    h_lhcb_Dz->SetBinContent(4,0.08425749);
    h_lhcb_Dz->SetBinContent(5,0.1050316);
    h_lhcb_Dz->SetBinContent(6,0.08505122);
    h_lhcb_Dz->SetBinContent(7,0.06646973);
    h_lhcb_Dz->SetBinContent(8,0.06720591);
    h_lhcb_Dz->SetBinContent(9,0.06453125);
    h_lhcb_Dz->SetBinContent(10,0.1179511);
    h_lhcb_Dz->SetBinError(1,0.03345452);
    h_lhcb_Dz->SetBinError(2,0.02542649);
    h_lhcb_Dz->SetBinError(3,0.02568044);
    h_lhcb_Dz->SetBinError(4,0.03549756);
    h_lhcb_Dz->SetBinError(5,0.01887588);
    h_lhcb_Dz->SetBinError(6,0.02268587);
    h_lhcb_Dz->SetBinError(7,0.0205256);
    h_lhcb_Dz->SetBinError(8,0.01586396);
    h_lhcb_Dz->SetBinError(9,0.01794524);
    h_lhcb_Dz->SetBinError(10,0.02540943);

    TH1D *h_lhcb_Dp = new TH1D("h_lhcb_Dp","",5,0,1);
    h_lhcb_Dp->SetLineColor(kGreen+2);
    h_lhcb_Dp->SetMarkerColor(kGreen+2);

    h_lhcb_Dp->SetBinContent(1,0.3048123);
    h_lhcb_Dp->SetBinContent(2,0.2014837);
    h_lhcb_Dp->SetBinContent(3,0.215055);
    h_lhcb_Dp->SetBinContent(4,0.1873196);
    h_lhcb_Dp->SetBinContent(5,0.09132945);
    h_lhcb_Dp->SetBinError(1,0.09617287);
    h_lhcb_Dp->SetBinError(2,0.05734113);
    h_lhcb_Dp->SetBinError(3,0.05072002);
    h_lhcb_Dp->SetBinError(4,0.04659536);
    h_lhcb_Dp->SetBinError(5,0.03919364);


    std::cout << "Integral: h_lhcb_comp_SPS       " << h_lhcb_comp_SPS->Integral()       << std::endl;
    std::cout << "Integral: h_lhcb_comp_all " << h_lhcb_comp_all->Integral() << std::endl;
    std::cout << "Integral: h_lhcb_Dz         " << h_lhcb_Dz->Integral()         << std::endl;
    std::cout << "Integral: h_lhcb_Dp         " << h_lhcb_Dp->Integral()         << std::endl;
    
    std::cout << "Integralw: h_lhcb_comp_SPS       " << h_lhcb_comp_SPS->Integral("width")       << std::endl;
    std::cout << "Integralw: h_lhcb_comp_all " << h_lhcb_comp_all->Integral("width") << std::endl;
    std::cout << "Integralw: h_lhcb_Dz         " << h_lhcb_Dz->Integral("width")         << std::endl;
    std::cout << "Integralw: h_lhcb_Dp         " << h_lhcb_Dp->Integral("width")         << std::endl;

    h_lhcb_comp_SPS->Scale(1.0/h_lhcb_comp_SPS->Integral("width"));
    h_lhcb_comp_all->Scale(1.0/h_lhcb_comp_all->Integral("width"));
    h_lhcb_Dz->Scale(1.0/h_lhcb_Dz->Integral("width"));
    h_lhcb_Dp->Scale(1.0/h_lhcb_Dp->Integral("width"));


    h_lhcb_comp_SPS->SetLineColor(kRed);
    h_lhcb_comp_all->SetLineColor(kBlack);
    
    double h_lhcb_comp_SPS_bin_width = h_lhcb_comp_SPS->GetXaxis()->GetBinWidth(2);
    h_lhcb_comp_SPS->SetTitle(";#Delta#phi(#Upsilon(1S), X_{c})/#pi; 1/#sigma d#sigma/d#Delta#phi");
    h_lhcb_comp_SPS->SetMinimum(0.0);
    h_lhcb_comp_SPS->SetMaximum(3.0);

    h_lhcb_comp_SPS->Draw("hist ][");
    h_lhcb_comp_all->Draw("same hist ][");

    h_lhcb_Dp->Draw("e1 same");
    h_lhcb_Dz->Draw("e1 same");


    TLegend* leg_lhcb_comp = new TLegend(0.37,0.62,0.9,0.9);
    leg_lhcb_comp->AddEntry(h_lhcb_comp_all,"Pythia - All","l");
    leg_lhcb_comp->AddEntry(h_lhcb_comp_SPS,"Pythia - Just SPS","l");
    leg_lhcb_comp->AddEntry(h_lhcb_Dp,"LHCb #Upsilon(1S)D^{+}","pe");
    leg_lhcb_comp->AddEntry(h_lhcb_Dz,"LHCb #Upsilon(1S)D^{0}","pe");
    leg_lhcb_comp->SetTextFont(132);
    leg_lhcb_comp->SetFillStyle(0);
    leg_lhcb_comp->Draw();



    c_lhcb_comp->Print("plots_kinematics_Y1SXcXc_to_Bu/c_lhcb_comp.pdf");


    // // angles
    SetLHCbStyle("square");
    int n_bins = 10;

    TH2D* h_processes_delta_phi_2d_pythia_all = new TH2D("h_processes_delta_phi_2d_pythia_all", 
                                    "h_processes_delta_phi_2d_pythia_all;#Delta#phi(#Upsilon(1S), X_{c}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_delta_phi_2d_pythia_SPS_Sep = new TH2D("h_processes_delta_phi_2d_pythia_SPS_Sep", 
                                    "h_processes_delta_phi_2d_pythia_SPS_Sep;#Delta#phi(#Upsilon(1S), X_{c}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_delta_phi_2d_pythia_DPS_Sep = new TH2D("h_processes_delta_phi_2d_pythia_DPS_Sep", 
                                    "h_processes_delta_phi_2d_pythia_DPS_Sep;#Delta#phi(#Upsilon(1S), X_{c}) [rad];#Delta#phi(#Upsilon(1S), X_{#bar{c}}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );



    TCanvas* can_delta_phi_2d_pythia_all = new TCanvas("can_delta_phi_2d_pythia_all","can_delta_phi_2d_pythia_all",550,500);

    pythia_tree->Draw( "angle_upsilon1s_dx:angle_upsilon1s_dx2>>h_processes_delta_phi_2d_pythia_all",      cuts_Y1SXcXc_all+Upsilon1S_fid);
    pythia_tree->Draw( "angle_upsilon1s_dx:angle_upsilon1s_dx2>>h_processes_delta_phi_2d_pythia_DPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_DPS_Sep);
    pythia_tree->Draw( "angle_upsilon1s_dx:angle_upsilon1s_dx2>>h_processes_delta_phi_2d_pythia_SPS_Sep",  cuts_Y1SXcXc_all+Upsilon1S_fid+cut_SPS_Sep);

    h_processes_delta_phi_2d_pythia_all->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_DPS_Sep->SetMinimum(0);
    h_processes_delta_phi_2d_pythia_SPS_Sep->SetMinimum(0);
    
    h_processes_delta_phi_2d_pythia_all->Draw("colz");
    can_delta_phi_2d_pythia_all->Print("plots_kinematics_Y1SXcXc_to_Bu/can_delta_phi_2d_pythia_all.pdf");

    TCanvas* can_delta_phi_2d_pythia_SPS_Sep = new TCanvas("can_delta_phi_2d_pythia_SPS_Sep","can_delta_phi_2d_pythia_SPS_Sep",550,500);
    h_processes_delta_phi_2d_pythia_SPS_Sep->Draw("colz");
    can_delta_phi_2d_pythia_SPS_Sep->Print("plots_kinematics_Y1SXcXc_to_Bu/can_delta_phi_2d_pythia_SPS_Sep.pdf");
    
    TCanvas* can_delta_phi_2d_pythia_DPS_Sep = new TCanvas("can_delta_phi_2d_pythia_DPS_Sep","can_delta_phi_2d_pythia_DPS_Sep",550,500);
    h_processes_delta_phi_2d_pythia_DPS_Sep->Draw("colz");
    can_delta_phi_2d_pythia_DPS_Sep->Print("plots_kinematics_Y1SXcXc_to_Bu/can_delta_phi_2d_pythia_DPS_Sep.pdf");


}

