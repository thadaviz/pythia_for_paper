#include "tools.h"

void plot_kinematics_Xibc0_to_Bu(){
    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    std::string filename_pythia_cccc     = "../output/from_lxplus/Complete_paper_samples/Sample_bbcc/main202output_SoftQCD_nd_UserHook_bbcc_N_2000000_PartMod1_withColRec.root";
    std::string filename_pythia_cc       = "../output/from_lxplus/Complete_paper_samples/Sample_bb/main202output_SoftQCD_nd_UserHook_Minni_Scale_4.000000_N_2000000_PartMod1_withColRec.root";
    std::string filename_pythia_genxicc  = "../output/from_lxplus/Complete_paper_samples/Sample_Xibc_GenXicc/main202output_GenXicc_N_10000_PartMod1_withColRec.root";
  
    // -------------------------------------------------------------------------
    // Fig cuts
    // -------------------------------------------------------------------------

    // TCut Xiccpp_fid = "upsilon1s_y > 2.0 && upsilon1s_y < 4.5";
    TCut Xiccpp_fid = "";

    // TCut Dp_fid = "Bu_p_y > 2.0 && Bu_p_y < 4.5";
    TCut Dp_fid = "";

    TCut cuts_Xiccpp_all    = "nXibcp_p>0";
    TCut cuts_Dp_all        = "nBu_p>=1";

    TCut cuts_DPS = "upsilon1s_b1_partSys!=upsilon1s_b2_partSys";
    TCut cuts_SPS = "upsilon1s_b1_partSys==upsilon1s_b2_partSys";
    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file_cc = TFile::Open(filename_pythia_cc.c_str());
    TTree* pythia_tree_cc = (TTree*) pythia_file_cc->Get("events");

    pythia_tree_cc->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_cc->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_cc->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree_cc->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree_cc->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");
    pythia_tree_cc->SetAlias("angle_upsilon1s_dx2","acos((upsilon1s_px*dx2_px + upsilon1s_py*dx2_py)/(upsilon1s_pt*dx2_pt))");


    pythia_tree_cc->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree_cc->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_cc->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree_cc->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree_cc->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree_cc->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_cc->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree_cc->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree_cc->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree_cc->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_cc->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree_cc->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");

    pythia_tree_cc->SetAlias("Bu_p_p",  "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py+Bu_p_pz*Bu_p_pz)");
    pythia_tree_cc->SetAlias("Bu_p_pt", "sqrt(Bu_p_px*Bu_p_px+Bu_p_py*Bu_p_py)");
    pythia_tree_cc->SetAlias("Bu_p_eta","(Bu_p_pz>0?-1:1)*acosh(Bu_p_p/Bu_p_pt)");
    pythia_tree_cc->SetAlias("Bu_p_y","0.5*log( (Bu_p_pe+Bu_p_pz) / (Bu_p_pe-Bu_p_pz) )");

    pythia_tree_cc->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree_cc->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");
    pythia_tree_cc->SetAlias("delta_eta_upsilon1s_dx2","upsilon1s_eta-dx2_eta");

    pythia_tree_cc->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree_cc->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");
    pythia_tree_cc->SetAlias("delta_y_upsilon1s_dx2","upsilon1s_y-dx2_y");

    pythia_tree_cc->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree_cc->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");
    pythia_tree_cc->SetAlias("delta_R_upsilon1s_dx2","sqrt(delta_eta_upsilon1s_dx2*delta_eta_upsilon1s_dx2+angle_upsilon1s_dx2*angle_upsilon1s_dx2)");

    double sigmaGen_pythia_cc;
    double sigmaErr_pythia_cc;
    pythia_tree_cc->SetBranchAddress("sigmaGen",&sigmaGen_pythia_cc);
    pythia_tree_cc->SetBranchAddress("sigmaErr",&sigmaErr_pythia_cc);
    
    int n_entries_pythia_cc = pythia_tree_cc->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open(filename_pythia_cccc.c_str());
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");

    pythia_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_dx2","acos((upsilon1s_px*dx2_px + upsilon1s_py*dx2_py)/(upsilon1s_pt*dx2_pt))");


    pythia_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");

    pythia_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_dx2","upsilon1s_eta-dx2_eta");

    pythia_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_dx2","upsilon1s_y-dx2_y");

    pythia_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_dx2","sqrt(delta_eta_upsilon1s_dx2*delta_eta_upsilon1s_dx2+angle_upsilon1s_dx2*angle_upsilon1s_dx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* genxicc_file = TFile::Open(filename_pythia_genxicc.c_str());
    TTree* genxicc_tree = (TTree*) genxicc_file->Get("events");

    genxicc_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    genxicc_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    genxicc_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    genxicc_tree->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");
    genxicc_tree->SetAlias("angle_upsilon1s_dx2","acos((upsilon1s_px*dx2_px + upsilon1s_py*dx2_py)/(upsilon1s_pt*dx2_pt))");


    genxicc_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    genxicc_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    genxicc_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    genxicc_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    genxicc_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    genxicc_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    genxicc_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    genxicc_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    genxicc_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    genxicc_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    genxicc_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    genxicc_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");

    genxicc_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    genxicc_tree->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");
    genxicc_tree->SetAlias("delta_eta_upsilon1s_dx2","upsilon1s_eta-dx2_eta");

    genxicc_tree->SetAlias("delta_y_dx2_dx","dx2_y-dx_y");
    genxicc_tree->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");
    genxicc_tree->SetAlias("delta_y_upsilon1s_dx2","upsilon1s_y-dx2_y");

    genxicc_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    genxicc_tree->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");
    genxicc_tree->SetAlias("delta_R_upsilon1s_dx2","sqrt(delta_eta_upsilon1s_dx2*delta_eta_upsilon1s_dx2+angle_upsilon1s_dx2*angle_upsilon1s_dx2)");

    double sigmaGen_genxicc;
    double sigmaErr_genxicc;
    genxicc_tree->SetBranchAddress("sigmaGen",&sigmaGen_genxicc);
    genxicc_tree->SetBranchAddress("sigmaErr",&sigmaErr_genxicc);
    
    int n_entries_genxicc = genxicc_tree->GetEntries();


    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(   pythia_tree->GetEntries()   -1);
    pythia_tree_cc->GetEntry(pythia_tree_cc->GetEntries()-1);
    genxicc_tree->GetEntry(genxicc_tree->GetEntries()-1);
 
    std::cout << "Cross section Pythia:        " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nXibcp_p>0")<< "/" << pythia_tree->GetEntries("")<< std::endl;
   
    std::cout << "Cross section Pythia all:  " << sigmaGen_pythia_cc*microbarn  << " +/- " << sigmaErr_pythia_cc*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree_cc->GetEntries("nXibcp_p>0")<< "/" << pythia_tree_cc->GetEntries("")<< std::endl;
        
    std::cout << "Cross section GenXicc:  " << sigmaGen_genxicc*microbarn  << " +/- " << sigmaErr_genxicc*microbarn << " microbarns";
    std::cout << "\t" << genxicc_tree->GetEntries("nXibcp_p>0")<< "/" << genxicc_tree->GetEntries("")<< std::endl;
    

    double scalefactor_pythia     =      sigmaGen_pythia   *microbarn/n_entries_pythia;
    double scalefactor_pythia_cc  =      sigmaGen_pythia_cc*microbarn/n_entries_pythia_cc;
    double scalefactor_genxicc    = 1e-3*sigmaGen_genxicc*microbarn/n_entries_genxicc;

    // -------------------------------------------------------------------------
    // Plot pT 
    // -------------------------------------------------------------------------
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia     = new TH1D("h_pt_pythia",    "h_pt_pythia",    20,0,20);
    TH1D* h_pt_pythia_Dp  = new TH1D("h_pt_pythia_Dp", "h_pt_pythia_Dp", 20,0,20);


    pythia_tree->Draw(   "upsilon1s_pt>>h_pt_pythia",cuts_Xiccpp_all+Xiccpp_fid);
    pythia_tree_cc->Draw("Bu_p_pt>>h_pt_pythia_Dp",cuts_Dp_all+Dp_fid);

    h_pt_pythia->Scale(   scalefactor_pythia   );
    h_pt_pythia_Dp->Scale(scalefactor_pythia_cc);


    // Black -> With MPI
    h_pt_pythia->SetLineColor(kBlack);

    // Black -> With MPI
    h_pt_pythia_Dp->SetLineColor(kBlack);
    h_pt_pythia_Dp->SetLineStyle(kDashed);


    h_pt_pythia->SetMaximum(1.05*max({h_pt_pythia->GetMaximum(),
                                      h_pt_pythia_Dp->GetMaximum()
                                  }));
    
    double h_pt_pythia_bin_width = h_pt_pythia->GetXaxis()->GetBinWidth(2);
    h_pt_pythia->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_pythia_bin_width));
    h_pt_pythia->Draw("hist");
    h_pt_pythia_Dp->Draw("hist same");
    gPad->SetLogy();

    TLegend* leg_pt = new TLegend(0.6,0.6,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,   "#Xi_{bc}^{+} Pythia With MPI","l");
    leg_pt->AddEntry(h_pt_pythia_Dp,"B^{+} Pythia With MPI","l");
    leg_pt->SetTextFont(132);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics_Xibc0_to_Bu/c_pt.pdf");

    TCanvas* c_pt_ratio = new TCanvas("c_pt_ratio","c_pt_ratio");

    TH1D* h_pt_pythia_ratio  = new TH1D("h_pt_pythia_ratio", "h_pt_pythia_ratio", 20,0,20);
    h_pt_pythia->Sumw2();
    h_pt_pythia_Dp->Sumw2();
    h_pt_pythia_ratio->Divide(h_pt_pythia,h_pt_pythia_Dp);

    h_pt_pythia_ratio->Scale(100);
    
    h_pt_pythia_ratio->SetMaximum(1.05*max({
                                                  h_pt_pythia_ratio->GetMaximum()
                                      }));
    h_pt_pythia_ratio->SetTitle(";#it{p}_{T} [GeV/#it{c}];R(#it{p}_{T}) (%)");
    // h_pt_pythia_ratio->SetMinimum(0);
    // h_pt_pythia_ratio->SetMaximum(1.8);
    h_pt_pythia_ratio->Draw();


    TLegend* leg_pt_ratio = new TLegend(0.2,0.7,0.5,0.9);
    leg_pt_ratio->AddEntry(h_pt_pythia_ratio,"Pythia With MPI","pe");
    leg_pt_ratio->SetTextFont(132);
    leg_pt_ratio->SetFillStyle(0);
    leg_pt_ratio->Draw();

    c_pt_ratio->Print("plots_kinematics_Xibc0_to_Bu/c_pt_ratio.pdf");


    // -------------------------------------------------------------------------
    // Plot multiplicity
    // -------------------------------------------------------------------------
    TCanvas* c_multiplicity = new TCanvas("c_multiplicity","c_multiplicity");
    TH1D* h_multiplicity_pythia      = new TH1D("h_multiplicity_pythia", "h_multiplicity_pythia", 16,0,80);
    TH1D* h_multiplicity_pythia_DPS  = new TH1D("h_multiplicity_pythia_DPS", "h_multiplicity_pythia_DPS", 16,0,80);
    TH1D* h_multiplicity_pythia_SPS  = new TH1D("h_multiplicity_pythia_SPS", "h_multiplicity_pythia_SPS", 16,0,80);
    TH1D* h_multiplicity_genxicc     = new TH1D("h_multiplicity_genxicc",    "h_multiplicity_genxicc",    16,0,80);

    TH1D* h_multiplicity_pythia_Dp  = new TH1D("h_multiplicity_pythia_Dp", "h_multiplicity_pythia_Dp", 16,0,80);

    pythia_tree->Draw(   "nChargedInLHCb>>h_multiplicity_pythia",    cuts_Xiccpp_all+Xiccpp_fid);
    pythia_tree->Draw(   "nChargedInLHCb>>h_multiplicity_pythia_DPS",cuts_Xiccpp_all+Xiccpp_fid+cuts_DPS);
    pythia_tree->Draw(   "nChargedInLHCb>>h_multiplicity_pythia_SPS",cuts_Xiccpp_all+Xiccpp_fid+cuts_SPS);
    genxicc_tree->Draw(  "nChargedInLHCb>>h_multiplicity_genxicc",   cuts_Xiccpp_all+Xiccpp_fid);
    pythia_tree_cc->Draw("nChargedInLHCb>>h_multiplicity_pythia_Dp", cuts_Dp_all+Dp_fid);

    h_multiplicity_pythia->Scale(    scalefactor_pythia);
    h_multiplicity_pythia_DPS->Scale(scalefactor_pythia);
    h_multiplicity_pythia_SPS->Scale(scalefactor_pythia);
    h_multiplicity_genxicc->Scale(   scalefactor_genxicc);
    h_multiplicity_pythia_Dp->Scale( scalefactor_pythia_cc);

    // Black -> With MPI
    h_multiplicity_pythia->SetLineColor(kBlack);

    h_multiplicity_pythia_Dp->SetLineColor(kBlack);
    h_multiplicity_pythia_Dp->SetLineStyle(kDashed);

    h_multiplicity_pythia_DPS->SetLineColor(kBlue);
    h_multiplicity_pythia_SPS->SetLineColor(kRed);

    // Green GenXicc
    h_multiplicity_genxicc->SetLineColor(kGreen+2);


    h_multiplicity_pythia->SetMaximum(1.05*max({h_multiplicity_pythia->GetMaximum(),
                                                h_multiplicity_pythia_Dp->GetMaximum(),
                                                h_multiplicity_pythia_DPS->GetMaximum(),
                                                h_multiplicity_pythia_SPS->GetMaximum(),
                                                h_multiplicity_genxicc->GetMaximum()
                                      }));
    
    double h_multiplicity_pythia_bin_width = h_multiplicity_pythia->GetXaxis()->GetBinWidth(2);
    h_multiplicity_pythia->SetTitle(Form(";N_{Charged}^{2.0<#eta<4.5}; d#sigma/dN [%.1f #mub]",h_multiplicity_pythia_bin_width));
    h_multiplicity_pythia->Draw("hist");
    h_multiplicity_pythia_Dp->Draw("same hist");
    h_multiplicity_pythia_DPS->Draw("same hist");
    h_multiplicity_pythia_SPS->Draw("same hist");
    h_multiplicity_genxicc->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_multiplicity = new TLegend(0.6,0.6,0.9,0.9);
    leg_multiplicity->AddEntry(h_multiplicity_pythia_Dp, "B^{+} Pythia","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia,    "#Xi_{bc}^{+} Pythia - All","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_SPS,"#Xi_{bc}^{+} Pythia - Just DPS","l");
    leg_multiplicity->AddEntry(h_multiplicity_pythia_DPS,"#Xi_{bc}^{+} Pythia - Just SPS","l");
    leg_multiplicity->AddEntry(h_multiplicity_genxicc,   "#Xi_{bc}^{+} GenXicc","l");
    leg_multiplicity->SetTextFont(132);
    leg_multiplicity->SetFillStyle(0);
    leg_multiplicity->Draw();
    c_multiplicity->Print("plots_kinematics_Xibc0_to_Bu/c_multiplicity.pdf");

    TCanvas* c_multiplicity_ratio = new TCanvas("c_multiplicity_ratio","c_multiplicity_ratio");

    TH1D* h_multiplicity_pythia_ratio      = new TH1D("h_multiplicity_pythia_ratio",     "h_multiplicity_pythia_ratio",     16,0,80);
    TH1D* h_multiplicity_pythia_SPS_ratio  = new TH1D("h_multiplicity_pythia_SPS_ratio", "h_multiplicity_pythia_SPS_ratio", 16,0,80);
    TH1D* h_multiplicity_pythia_DPS_ratio  = new TH1D("h_multiplicity_pythia_DPS_ratio", "h_multiplicity_pythia_DPS_ratio", 16,0,80);
    TH1D* h_multiplicity_genxicc_ratio     = new TH1D("h_multiplicity_genxicc_ratio",    "h_multiplicity_genxicc_ratio",    16,0,80);

    h_multiplicity_pythia->Sumw2();
    h_multiplicity_pythia_DPS->Sumw2();
    h_multiplicity_genxicc->Sumw2();
    h_multiplicity_pythia_SPS->Sumw2();
    h_multiplicity_pythia_Dp->Sumw2();

    h_multiplicity_pythia_ratio->Divide(    h_multiplicity_pythia,    h_multiplicity_pythia_Dp);
    h_multiplicity_pythia_DPS_ratio->Divide(h_multiplicity_pythia_DPS,h_multiplicity_pythia_Dp);
    h_multiplicity_genxicc_ratio->Divide(   h_multiplicity_genxicc,   h_multiplicity_pythia_Dp);
    h_multiplicity_pythia_SPS_ratio->Divide(h_multiplicity_pythia_SPS,h_multiplicity_pythia_Dp);


    h_multiplicity_pythia_ratio->Scale(100);
    h_multiplicity_pythia_DPS_ratio->Scale(100);
    h_multiplicity_genxicc_ratio->Scale(100);
    h_multiplicity_pythia_SPS_ratio->Scale(100);
    
    // Black -> With MPI
    h_multiplicity_pythia_ratio->SetLineColor(kBlack);


    h_multiplicity_pythia_DPS_ratio->SetLineColor(kBlue);
    h_multiplicity_pythia_DPS_ratio->SetMarkerColor(kBlue);


    h_multiplicity_pythia_SPS_ratio->SetLineColor(kRed);
    h_multiplicity_pythia_SPS_ratio->SetMarkerColor(kRed);

    h_multiplicity_genxicc_ratio->SetLineColor(kGreen+2);
    h_multiplicity_genxicc_ratio->SetMarkerColor(kGreen+2);

    h_multiplicity_pythia_ratio->SetMaximum(1.05*max({
                                                  h_multiplicity_pythia_ratio->GetMaximum(),
                                                  h_multiplicity_pythia_DPS_ratio->GetMaximum(),
                                                  h_multiplicity_pythia_SPS_ratio->GetMaximum(),
                                                  h_multiplicity_genxicc_ratio->GetMaximum()
                                      }));
    h_multiplicity_pythia_ratio->SetTitle(";N_{Charged}^{2.0<#eta<4.5};#scale[0.6]{#frac{d#sigma(#Xi_{bc}^{+})}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_multiplicity_pythia_ratio->SetMinimum(0);
    h_multiplicity_pythia_ratio->Draw();
    h_multiplicity_pythia_DPS_ratio->Draw("same");
    h_multiplicity_genxicc_ratio->Draw("same");
    h_multiplicity_pythia_SPS_ratio->Draw("same");


    TLegend* leg_multiplicity_ratio = new TLegend(0.2,0.5,0.6,0.9);
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_ratio,    "Pythia - All","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_DPS_ratio,"Pythia - Just DPS","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_pythia_SPS_ratio,"Pythia - Just SPS","pe");
    leg_multiplicity_ratio->AddEntry(h_multiplicity_genxicc_ratio,   "GenXicc","pe");
    leg_multiplicity_ratio->SetTextFont(132);
    leg_multiplicity_ratio->SetFillStyle(0);
    leg_multiplicity_ratio->Draw();

    c_multiplicity_ratio->Print("plots_kinematics_Xibc0_to_Bu/c_multiplicity_ratio.pdf");

    // -------------------------------------------------------------------------
    // Plot MPI
    // -------------------------------------------------------------------------
    TCanvas* c_nMPI = new TCanvas("c_nMPI","c_nMPI");
    TH1D* h_nMPI_pythia      = new TH1D("h_nMPI_pythia",     "h_nMPI_pythia",     13,0,26);
    TH1D* h_nMPI_pythia_DPS  = new TH1D("h_nMPI_pythia_DPS", "h_nMPI_pythia_DPS", 13,0,26);
    TH1D* h_nMPI_pythia_SPS  = new TH1D("h_nMPI_pythia_SPS", "h_nMPI_pythia_SPS", 13,0,26);
    TH1D* h_nMPI_genxicc     = new TH1D("h_nMPI_genxicc",    "h_nMPI_genxicc",    13,0,26);
    TH1D* h_nMPI_pythia_Dp   = new TH1D("h_nMPI_pythia_Dp",  "h_nMPI_pythia_Dp",  13,0,26);

    pythia_tree->Draw(   "nMPI>>h_nMPI_pythia",    cuts_Xiccpp_all+Xiccpp_fid);
    pythia_tree->Draw(   "nMPI>>h_nMPI_pythia_DPS",cuts_Xiccpp_all+Xiccpp_fid+cuts_DPS);
    pythia_tree->Draw(   "nMPI>>h_nMPI_pythia_SPS",cuts_Xiccpp_all+Xiccpp_fid+cuts_SPS);
    pythia_tree->Draw(   "nMPI>>h_nMPI_pythia",    cuts_Xiccpp_all+Xiccpp_fid);
    genxicc_tree->Draw(  "nMPI>>h_nMPI_genxicc",   cuts_Xiccpp_all+Xiccpp_fid);
    pythia_tree_cc->Draw("nMPI>>h_nMPI_pythia_Dp", cuts_Dp_all+Dp_fid);

    h_nMPI_pythia->Scale(    scalefactor_pythia   );
    h_nMPI_pythia_DPS->Scale(scalefactor_pythia   );
    h_nMPI_pythia_SPS->Scale(scalefactor_pythia   );
    h_nMPI_genxicc->Scale(   scalefactor_genxicc  );
    h_nMPI_pythia_Dp->Scale( scalefactor_pythia_cc);


    // Black -> With MPI
    h_nMPI_pythia->SetLineColor(kBlack);

    h_nMPI_pythia_Dp->SetLineColor(kBlack);
    h_nMPI_pythia_Dp->SetLineStyle(kDashed);


    h_nMPI_pythia_DPS->SetLineColor(kBlue);
    h_nMPI_pythia_SPS->SetLineColor(kRed);

    // Green 
    h_nMPI_genxicc->SetLineColor(kGreen+2);



    h_nMPI_pythia->SetMaximum(1.05*max({h_nMPI_pythia->GetMaximum(),
                                      h_nMPI_pythia_Dp->GetMaximum(),
                                      h_nMPI_pythia_DPS->GetMaximum(),
                                      h_nMPI_pythia_SPS->GetMaximum(),
                                      h_nMPI_genxicc->GetMaximum()
                                      }));
    
    double h_nMPI_pythia_bin_width = h_nMPI_pythia->GetXaxis()->GetBinWidth(2);
    h_nMPI_pythia->SetTitle(Form(";N_{MPI}; d#sigma/dN [%.1f #mub]",h_nMPI_pythia_bin_width));
    h_nMPI_pythia->Draw("hist");
    h_nMPI_pythia_Dp->Draw("same hist");
    h_nMPI_pythia_DPS->Draw("same hist");
    h_nMPI_pythia_SPS->Draw("same hist");
    h_nMPI_genxicc->Draw("same hist");
    gPad->SetLogy();

    TLegend* leg_nMPI = new TLegend(0.6,0.6,0.9,0.9);
    leg_nMPI->AddEntry(h_nMPI_pythia_Dp,"B^{+} Pythia","l");
    leg_nMPI->AddEntry(h_nMPI_pythia,    "#Xi_{bc}^{+} Pythia - All","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_DPS,"#Xi_{bc}^{+} Pythia - Just DPS","l");
    leg_nMPI->AddEntry(h_nMPI_pythia_SPS,"#Xi_{bc}^{+} Pythia - Just SPS","l");
    leg_nMPI->AddEntry(h_nMPI_genxicc,   "#Xi_{bc}^{+} GenXicc","l");

    leg_nMPI->SetTextFont(132);
    leg_nMPI->SetFillStyle(0);
    leg_nMPI->Draw();
    c_nMPI->Print("plots_kinematics_Xibc0_to_Bu/c_nMPI.pdf");

    TCanvas* c_nMPI_ratio = new TCanvas("c_nMPI_ratio","c_nMPI_ratio");

    TH1D* h_nMPI_pythia_ratio      = new TH1D("h_nMPI_pythia_ratio",     "h_nMPI_pythia_ratio",     13,0,26);
    TH1D* h_nMPI_pythia_DPS_ratio  = new TH1D("h_nMPI_pythia_DPS_ratio", "h_nMPI_pythia_DPS_ratio", 13,0,26);
    TH1D* h_nMPI_pythia_SPS_ratio  = new TH1D("h_nMPI_pythia_SPS_ratio", "h_nMPI_pythia_SPS_ratio", 13,0,26);
    TH1D* h_nMPI_genxicc_ratio     = new TH1D("h_nMPI_genxicc_ratio",    "h_nMPI_genxicc_ratio",    13,0,26);

    h_nMPI_pythia->Sumw2();
    h_nMPI_pythia_DPS->Sumw2();
    h_nMPI_pythia_SPS->Sumw2();
    h_nMPI_genxicc->Sumw2();
    h_nMPI_pythia_Dp->Sumw2();
    h_nMPI_pythia_ratio->Divide(    h_nMPI_pythia,    h_nMPI_pythia_Dp);
    h_nMPI_pythia_DPS_ratio->Divide(h_nMPI_pythia_DPS,h_nMPI_pythia_Dp);
    h_nMPI_pythia_SPS_ratio->Divide(h_nMPI_pythia_SPS,h_nMPI_pythia_Dp);
    h_nMPI_genxicc_ratio->Divide(   h_nMPI_genxicc,   h_nMPI_pythia_Dp);


    h_nMPI_pythia_ratio->Scale(100);
    h_nMPI_pythia_DPS_ratio->Scale(100);
    h_nMPI_pythia_SPS_ratio->Scale(100);
    h_nMPI_genxicc_ratio->Scale(100);
    
        // Black -> With MPI
    h_nMPI_pythia_ratio->SetLineColor(kBlack);


    h_nMPI_pythia_DPS_ratio->SetLineColor(kBlue);
    h_nMPI_pythia_DPS_ratio->SetMarkerColor(kBlue);

    h_nMPI_pythia_SPS_ratio->SetLineColor(kRed);
    h_nMPI_pythia_SPS_ratio->SetMarkerColor(kRed);

    h_nMPI_genxicc_ratio->SetLineColor(kGreen+2);
    h_nMPI_genxicc_ratio->SetMarkerColor(kGreen+2);

    h_nMPI_pythia_ratio->SetMaximum(1.05*max({h_nMPI_pythia_ratio->GetMaximum(),
                                              h_nMPI_pythia_DPS_ratio->GetMaximum(),
                                              h_nMPI_pythia_SPS_ratio->GetMaximum(),
                                              h_nMPI_genxicc_ratio->GetMaximum()
                                      }));
    h_nMPI_pythia_ratio->SetTitle(";N_{MPI};#scale[0.6]{#frac{d#sigma(#Xi_{bc}^{+})}{dN}}/#scale[0.6]{#frac{d#sigma(B^{+})}{dN}} (%)");
    h_nMPI_pythia_ratio->SetMinimum(0);
    h_nMPI_pythia_ratio->Draw();
    h_nMPI_pythia_DPS_ratio->Draw("same");
    h_nMPI_pythia_SPS_ratio->Draw("same");
    h_nMPI_genxicc_ratio->Draw("same");


    TLegend* leg_nMPI_ratio = new TLegend(0.2,0.5,0.6,0.9);
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_ratio,    "Pythia - All","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_DPS_ratio,"Pythia - Just DPS","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_pythia_SPS_ratio,"Pythia - Just SPS","pe");
    leg_nMPI_ratio->AddEntry(h_nMPI_genxicc_ratio,   "GenXicc","pe");
    leg_nMPI_ratio->SetTextFont(132);
    leg_nMPI_ratio->SetFillStyle(0);
    leg_nMPI_ratio->Draw();
    c_nMPI_ratio->Print("plots_kinematics_Xibc0_to_Bu/c_nMPI_ratio.pdf");


    // -------------------------------------------------------------------------
    // Plot pT just Xibc
    // -------------------------------------------------------------------------
    TCanvas* c_pt_Xibc = new TCanvas("c_pt_Xibc","c_pt_Xibc");
    TH1D* h_pt_Xibc_pythia_all     = new TH1D("h_pt_Xibc_pythia_all",    "h_pt_Xibc_pythia_all",    20,0,20);
    TH1D* h_pt_Xibc_pythia_SPS     = new TH1D("h_pt_Xibc_pythia_SPS",    "h_pt_Xibc_pythia_SPS",    20,0,20);
    TH1D* h_pt_Xibc_pythia_DPS     = new TH1D("h_pt_Xibc_pythia_DPS",    "h_pt_Xibc_pythia_DPS",    20,0,20);
    TH1D* h_pt_Xibc_genxicc        = new TH1D("h_pt_Xibc_genxicc",       "h_pt_Xibc_genxicc",       20,0,20);


    pythia_tree->Draw(    "upsilon1s_pt>>h_pt_Xibc_pythia_all",cuts_Xiccpp_all+Xiccpp_fid);
    pythia_tree->Draw(    "upsilon1s_pt>>h_pt_Xibc_pythia_SPS",cuts_Xiccpp_all+Xiccpp_fid+cuts_SPS);
    pythia_tree->Draw(    "upsilon1s_pt>>h_pt_Xibc_pythia_DPS",cuts_Xiccpp_all+Xiccpp_fid+cuts_DPS);
    genxicc_tree->Draw(   "upsilon1s_pt>>h_pt_Xibc_genxicc",cuts_Xiccpp_all+Xiccpp_fid);

    h_pt_Xibc_pythia_all->Scale(   scalefactor_pythia   );
    h_pt_Xibc_pythia_DPS->Scale(   scalefactor_pythia   );
    h_pt_Xibc_pythia_SPS->Scale(   scalefactor_pythia   );
    h_pt_Xibc_genxicc->Scale(      scalefactor_genxicc   );

    // Black -> All
    h_pt_Xibc_pythia_all->SetLineColor(kBlack);

    // Red -> just SPS
    h_pt_Xibc_pythia_SPS->SetLineColor(kRed);

    // Blue -> just DPS
    h_pt_Xibc_pythia_DPS->SetLineColor(kBlue);

    // Green -> BcVegPy
    h_pt_Xibc_genxicc->SetLineColor(kGreen+2);
    


    h_pt_Xibc_pythia_all->SetMaximum(1.05*max({h_pt_Xibc_pythia_all->GetMaximum(),
                                      h_pt_Xibc_genxicc->GetMaximum(),
                                      h_pt_Xibc_pythia_SPS->GetMaximum(),
                                      h_pt_Xibc_pythia_DPS->GetMaximum()
                                      }));
    
    double h_pt_Xibc_pythia_all_bin_width = h_pt_Xibc_pythia_all->GetXaxis()->GetBinWidth(2);
    h_pt_Xibc_pythia_all->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma(#Xi^{+}_{bc})/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_Xibc_pythia_all_bin_width));
    h_pt_Xibc_pythia_all->Draw("hist");
    h_pt_Xibc_pythia_SPS->Draw("hist same");
    h_pt_Xibc_pythia_DPS->Draw("hist same");
    h_pt_Xibc_genxicc->Draw("same hist");
    // gPad->SetLogy();

    TLegend* leg_pt_Xibc = new TLegend(0.55,0.6,0.93,0.9);
    leg_pt_Xibc->AddEntry(h_pt_Xibc_pythia_all,         "Pythia - All","l");
    leg_pt_Xibc->AddEntry(h_pt_Xibc_pythia_DPS,         "Pythia - Just DPS","l");
    leg_pt_Xibc->AddEntry(h_pt_Xibc_pythia_SPS,         "Pythia - Just SPS","l");
    leg_pt_Xibc->AddEntry(h_pt_Xibc_genxicc,            "GenXicc","l");
    leg_pt_Xibc->SetTextFont(132);
    leg_pt_Xibc->SetFillStyle(0);
    leg_pt_Xibc->Draw();
    c_pt_Xibc->Print("plots_kinematics_Xibc0_to_Bu/c_pt_Xibc.pdf");

    // -------------------------------------------------------------------------
    // Plot y just Xibc
    // -------------------------------------------------------------------------
    TCanvas* c_y_Xibc = new TCanvas("c_y_Xibc","c_y_Xibc");
    TH1D* h_y_Xibc_pythia_all     = new TH1D("h_y_Xibc_pythia_all",     "h_y_Xibc_pythia_all",   20,-7.5,12.5);
    TH1D* h_y_Xibc_pythia_SPS     = new TH1D("h_y_Xibc_pythia_SPS",    "h_y_Xibc_pythia_SPS",    20,-7.5,12.5);
    TH1D* h_y_Xibc_pythia_DPS     = new TH1D("h_y_Xibc_pythia_DPS",    "h_y_Xibc_pythia_DPS",    20,-7.5,12.5);
    TH1D* h_y_Xibc_genxicc        = new TH1D("h_y_Xibc_genxicc",       "h_y_Xibc_genxicc",       20,-7.5,12.5);


    pythia_tree->Draw(    "upsilon1s_y>>h_y_Xibc_pythia_all",cuts_Xiccpp_all+Xiccpp_fid);
    pythia_tree->Draw(    "upsilon1s_y>>h_y_Xibc_pythia_SPS",cuts_Xiccpp_all+Xiccpp_fid+cuts_SPS);
    pythia_tree->Draw(    "upsilon1s_y>>h_y_Xibc_pythia_DPS",cuts_Xiccpp_all+Xiccpp_fid+cuts_DPS);
    genxicc_tree->Draw(   "upsilon1s_y>>h_y_Xibc_genxicc",   cuts_Xiccpp_all+Xiccpp_fid);

    h_y_Xibc_pythia_all->Scale(   scalefactor_pythia   );
    h_y_Xibc_pythia_DPS->Scale(   scalefactor_pythia   );
    h_y_Xibc_pythia_SPS->Scale(   scalefactor_pythia   );
    h_y_Xibc_genxicc->Scale(   scalefactor_genxicc   );

    // Black -> All
    h_y_Xibc_pythia_all->SetLineColor(kBlack);

    // Red -> just SPS
    h_y_Xibc_pythia_SPS->SetLineColor(kRed);

    // Blue -> just DPS
    h_y_Xibc_pythia_DPS->SetLineColor(kBlue);

    // Green -> BcVegPy
    h_y_Xibc_genxicc->SetLineColor(kGreen+2);
  


    h_y_Xibc_pythia_all->SetMaximum(1.05*max({h_y_Xibc_pythia_all->GetMaximum(),
                                      h_y_Xibc_genxicc->GetMaximum(),
                                      h_y_Xibc_pythia_SPS->GetMaximum(),
                                      h_y_Xibc_pythia_DPS->GetMaximum()
                                      }));
    
    double h_y_Xibc_pythia_all_bin_width = h_y_Xibc_pythia_all->GetXaxis()->GetBinWidth(2);
    h_y_Xibc_pythia_all->SetTitle(Form(";#it{y}; d#sigma(#Xi^{+}_{bc})/d#it{y} [%.1f #mub]",h_y_Xibc_pythia_all_bin_width));
    h_y_Xibc_pythia_all->Draw("hist");
    h_y_Xibc_pythia_SPS->Draw("hist same");
    h_y_Xibc_pythia_DPS->Draw("hist same");
    h_y_Xibc_genxicc->Draw("same hist");
    // gPad->SetLogy();

    TLegend* leg_y_Xibc = new TLegend(0.55,0.6,0.93,0.9);
    leg_y_Xibc->AddEntry(h_y_Xibc_pythia_all,         "Pythia - All","l");
    leg_y_Xibc->AddEntry(h_y_Xibc_pythia_DPS,         "Pythia - Just DPS","l");
    leg_y_Xibc->AddEntry(h_y_Xibc_pythia_SPS,         "Pythia - Just SPS","l");
    leg_y_Xibc->AddEntry(h_y_Xibc_genxicc,            "GenXicc","l");
    leg_y_Xibc->SetTextFont(132);
    leg_y_Xibc->SetFillStyle(0);
    leg_y_Xibc->Draw();
    c_y_Xibc->Print("plots_kinematics_Xibc0_to_Bu/c_y_Xibc.pdf");
}

