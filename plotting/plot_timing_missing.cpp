#include "tools.h"

void plot_timing_missing(){
    SetLHCbStyle("oneD");

    // std::string name = "../output/from_lxplus/Timing/Clean/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_4.000000_N_1000000.root";
    std::string name = "../output/Timing/main202output_SoftQCD_nd_UserHook_Minni_pass_Scale_4.000000_N_1000000_PartMod1.root";

    TFile* file = TFile::Open(name.c_str());
    if(!file){
        std::cout << "Can't fild file: " << name << std::endl;
        exit(1);
    }
    
    TTree* events = (TTree*)file->Get("events");
    if(!events){
        std::cout << "Can't fild tree: events in file " <<name << std::endl;
        exit(1);
    }

    events->SetAlias("bx_p",  "sqrt(bx_px*bx_px+bx_py*bx_py+bx_pz*bx_pz)");
    events->SetAlias("bx_pt", "sqrt(bx_px*bx_px+bx_py*bx_py)");
    events->SetAlias("bx_eta","(bx_pz>0?-1:1)*acosh(bx_p/bx_pt)");

    events->SetAlias("bx2_p",  "sqrt(bx2_px*bx2_px+bx2_py*bx2_py+bx2_pz*bx2_pz)");
    events->SetAlias("bx2_pt", "sqrt(bx2_px*bx2_px+bx2_py*bx2_py)");
    events->SetAlias("bx2_eta","(bx2_pz>0?-1:1)*acosh(bx2_p/bx2_pt)");

    events->SetAlias("delta_eta_bx_bx2","bx_eta-bx2_eta");

    events->SetAlias("bx_y","0.5*log( (bx_pe+bx_pz) / (bx_pe-bx_pz) )");
    events->SetAlias("bx2_y","0.5*log( (bx2_pe+bx2_pz) / (bx2_pe-bx2_pz) )");

    double n_bb_all     = events->GetEntries("(foundb || hasbOnia)");
    double n_bb_failMPI = n_bb_all - events->GetEntries("(foundb || hasbOnia) && !decMinnivetoMPI");
    double n_bb_failPT  = n_bb_all - events->GetEntries("(foundb || hasbOnia) && !decMinnivetoMPI && !decMinnivetoPT");

    TCut twoB = "nXb==2";

    TH1D* h_pt_all     = new TH1D("h_pt_all",    "h_pt_all",    40,0,20);
    TH1D* h_pt_missing = new TH1D("h_pt_missing","h_pt_missing",40,0,20);
    TH1D* h_pt_accepted = new TH1D("h_pt_accepted","h_pt_accepted",40,0,20);
    TH1D* h_pt_mis_shw = new TH1D("h_pt_mis_shw","h_pt_mis_shw",40,0,20);
    TH1D* h_pt_mis_mpi = new TH1D("h_pt_mis_mpi","h_pt_mis_mpi",40,0,20);
    h_pt_missing->SetLineColor(kGreen+2);
    h_pt_accepted->SetLineColor(kRed);
    h_pt_mis_shw->SetLineColor(kGreen+2);
    h_pt_mis_mpi->SetLineColor(kGreen+2);
    h_pt_mis_shw->SetLineStyle(kDashed);
    h_pt_mis_mpi->SetLineStyle(kDotted);
    h_pt_all->SetTitle(";#it{p}_{T} [GeV/c];Events");


    TH1D* h_eta_all     = new TH1D("h_eta_all",    "h_eta_all",    20,-10,10);
    TH1D* h_eta_missing = new TH1D("h_eta_missing","h_eta_missing",20,-10,10);
    TH1D* h_eta_accepted = new TH1D("h_eta_accepted","h_eta_accepted",20,-10,10);
    TH1D* h_eta_mis_shw = new TH1D("h_eta_mis_shw","h_eta_mis_shw",20,-10,10);
    TH1D* h_eta_mis_mpi = new TH1D("h_eta_mis_mpi","h_eta_mis_mpi",20,-10,10);
    h_eta_missing->SetLineColor(kGreen+2);
    h_eta_accepted->SetLineColor(kGreen+2);
    h_eta_mis_shw->SetLineColor(kGreen+2);
    h_eta_mis_mpi->SetLineColor(kGreen+2);
    h_eta_mis_shw->SetLineStyle(kDashed);
    h_eta_mis_mpi->SetLineStyle(kDotted);
    h_eta_all->SetTitle(";#eta;Events");
    
    TH1D* h_y_all     = new TH1D("h_y_all",    "h_y_all",    20,-10,10);
    TH1D* h_y_missing = new TH1D("h_y_missing","h_y_missing",20,-10,10);
    TH1D* h_y_accepted = new TH1D("h_y_accepted","h_y_accepted",20,-10,10);
    TH1D* h_y_mis_shw = new TH1D("h_y_mis_shw","h_y_mis_shw",20,-10,10);
    TH1D* h_y_mis_mpi = new TH1D("h_y_mis_mpi","h_y_mis_mpi",20,-10,10);
    h_y_missing->SetLineColor(kGreen+2);
    h_y_accepted->SetLineColor(kGreen+2);
    h_y_mis_shw->SetLineColor(kGreen+2);
    h_y_mis_mpi->SetLineColor(kGreen+2);
    h_y_mis_shw->SetLineStyle(kDashed);
    h_y_mis_mpi->SetLineStyle(kDotted);
    h_y_all->SetTitle(";#it{y};Events");


    TCut all = "(foundb || hasbOnia)";
    TCut missing = all && "!(!decMinnivetoMPI && !decMinnivetoPT)";
    TCut accepted = all && "(!decMinnivetoMPI && !decMinnivetoPT)";
    
    TCut missing_shower = missing && "!(hasbStatus33||hasbOniaStatus33)";
    TCut missing_MPI    = missing && " (hasbStatus33||hasbOniaStatus33)";
    
    TCanvas* c_pt_missing= new TCanvas("c_pt_missing","c_pt_missing");
    
    events->Draw("bx_pt>>h_pt_missing",missing+twoB);
    events->Draw("bx_pt>>h_pt_accepted",accepted+twoB);
    events->Draw("bx_pt>>h_pt_mis_shw",missing_shower+twoB);
    events->Draw("bx_pt>>h_pt_mis_mpi",missing_MPI+twoB);
    events->Draw("bx_pt>>h_pt_all",all+twoB);

    h_pt_all->SetMinimum(0.1);
    h_pt_all->SetMaximum(1e5);
    h_pt_all->Draw();
    h_pt_missing->Draw("same");
    h_pt_accepted->Draw("same");
    h_pt_all->Draw("same");
    h_pt_mis_shw->Draw("same");
    h_pt_mis_mpi->Draw("same");
    gPad->SetLogy();
    // gPad->SetGrid();
    
    TLegend* leg_pt = new TLegend(0.5,0.6,0.9,0.9);
    leg_pt->AddEntry(h_pt_all,"All #it{b#bar{b}}","l");
    leg_pt->AddEntry(h_pt_accepted, "Accepted #it{b#bar{b}}","l");
    leg_pt->AddEntry(h_pt_missing, "Missed #it{b#bar{b}}","l");
    leg_pt->AddEntry(h_pt_mis_shw, "Missed Shower #it{b#bar{b}}","l");
    leg_pt->AddEntry(h_pt_mis_mpi, "Missed MPI #it{b#bar{b}}","l");
    
    leg_pt->Draw();
    leg_pt->SetTextFont(132);
    leg_pt->SetFillStyle(0);
    c_pt_missing->Print("plots_timing_missing/c_pt_missing.pdf");

    TCanvas* c_eta_missing= new TCanvas("c_eta_missing","c_eta_missing");
    
    events->Draw("bx_eta>>h_eta_missing",missing+twoB);
    events->Draw("bx_eta>>h_eta_mis_shw",missing_shower+twoB);
    events->Draw("bx_eta>>h_eta_mis_mpi",missing_MPI+twoB);
    events->Draw("bx_eta>>h_eta_all",all+twoB);

    h_eta_all->SetMinimum(0.1);
    h_eta_all->Draw();
    h_eta_missing->Draw("same");
    h_eta_mis_shw->Draw("same");
    h_eta_mis_mpi->Draw("same");
    gPad->SetLogy();
    
    TLegend* leg_eta = new TLegend(0.5,0.7,0.9,0.9);
    leg_eta->AddEntry(h_eta_all,"All #it{b#bar{b}}","l");
    leg_eta->AddEntry(h_eta_missing, "Missed #it{b#bar{b}}","l");
    leg_eta->AddEntry(h_eta_mis_shw, "Missed Shower #it{b#bar{b}}","l");
    leg_eta->AddEntry(h_eta_mis_mpi, "Missed MPI #it{b#bar{b}}","l");
    
    leg_eta->Draw();
    leg_eta->SetTextFont(132);
    leg_eta->SetFillStyle(0);
    c_eta_missing->Print("plots_timing_missing/c_eta_missing.pdf");

    TCanvas* c_y_missing= new TCanvas("c_y_missing","c_y_missing");
    
    events->Draw("bx_y>>h_y_missing",missing+twoB);
    events->Draw("bx_y>>h_y_mis_shw",missing_shower+twoB);
    events->Draw("bx_y>>h_y_mis_mpi",missing_MPI+twoB);
    events->Draw("bx_y>>h_y_all",all+twoB);

    h_y_all->SetMinimum(0.1);
    h_y_all->Draw();
    h_y_missing->Draw("same");
    h_y_mis_shw->Draw("same");
    h_y_mis_mpi->Draw("same");
    gPad->SetLogy();
    
    TLegend* leg_y = new TLegend(0.5,0.7,0.9,0.9);
    leg_y->AddEntry(h_y_all,"All #it{b#bar{b}}","l");
    leg_y->AddEntry(h_y_missing, "Missed #it{b#bar{b}}","l");
    leg_y->AddEntry(h_y_mis_shw, "Missed Shower #it{b#bar{b}}","l");
    leg_y->AddEntry(h_y_mis_mpi, "Missed MPI #it{b#bar{b}}","l");
    
    leg_y->Draw();
    leg_y->SetTextFont(132);
    leg_y->SetFillStyle(0);
    c_y_missing->Print("plots_timing_missing/c_y_missing.pdf");

}