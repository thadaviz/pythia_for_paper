#include "tools.h"

void plot_kinematics_Y1SXcXc(){

    SetLHCbStyle("oneD");
    const double millibarn = 1e+0;
    const double microbarn = 1e+3;
    const double nanobarn  = 1e+6;
    const double picobarn  = 1e+9;

    // -------------------------------------------------------------------------
    // Get Pythia simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bbcc_N_2000000_PartMod1_WithColRec.root");
    TTree* pythia_tree = (TTree*) pythia_file->Get("events");


    pythia_tree->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");
    pythia_tree->SetAlias("angle_upsilon1s_dx2","acos((upsilon1s_px*dx2_px + upsilon1s_py*dx2_py)/(upsilon1s_pt*dx2_pt))");
    pythia_tree->SetAlias("angle_dx_dx2","acos((dx_px*dx2_px + dx_py*dx2_py)/(dx_pt*dx2_pt))");


    pythia_tree->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");
    pythia_tree->SetAlias("delta_eta_upsilon1s_dx2","upsilon1s_eta-dx2_eta");

    pythia_tree->SetAlias("delta_y_dx_dx2","dx_y-dx2_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");
    pythia_tree->SetAlias("delta_y_upsilon1s_dx2","upsilon1s_y-dx2_y");

    pythia_tree->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");
    pythia_tree->SetAlias("delta_R_upsilon1s_dx2","sqrt(delta_eta_upsilon1s_dx2*delta_eta_upsilon1s_dx2+angle_upsilon1s_dx2*angle_upsilon1s_dx2)");

    double sigmaGen_pythia;
    double sigmaErr_pythia;
    pythia_tree->SetBranchAddress("sigmaGen",&sigmaGen_pythia);
    pythia_tree->SetBranchAddress("sigmaErr",&sigmaErr_pythia);
    
    int n_entries_pythia = pythia_tree->GetEntries();

    // -------------------------------------------------------------------------
    // Get Pythia noMPI simulation
    // -------------------------------------------------------------------------

    TFile* pythia_file_nompi = TFile::Open("../output/from_lxplus/main202output_SoftQCD_nd_UserHook_bbcc_N_2000000_noMPI_PartMod1_WithColRec.root");
    TTree* pythia_tree_nompi = (TTree*) pythia_file_nompi->Get("events");


    pythia_tree_nompi->SetAlias("upsilon1s_pt" , "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_nompi->SetAlias("dx2_pt" , "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_nompi->SetAlias("dx_pt" , "sqrt(dx_px*dx_px+dx_py*dx_py)");


    pythia_tree_nompi->SetAlias("angle_dx2_dx","acos((dx2_px*dx_px + dx2_py*dx_py)/(dx2_pt*dx_pt))");
    pythia_tree_nompi->SetAlias("angle_upsilon1s_dx","acos((upsilon1s_px*dx_px + upsilon1s_py*dx_py)/(upsilon1s_pt*dx_pt))");
    pythia_tree_nompi->SetAlias("angle_upsilon1s_dx2","acos((upsilon1s_px*dx2_px + upsilon1s_py*dx2_py)/(upsilon1s_pt*dx2_pt))");
    pythia_tree_nompi->SetAlias("angle_dx_dx2","acos((dx_px*dx2_px + dx_py*dx2_py)/(dx_pt*dx2_pt))");


    pythia_tree_nompi->SetAlias("upsilon1s_p",  "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz)");
    pythia_tree_nompi->SetAlias("upsilon1s_pt", "sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py)");
    pythia_tree_nompi->SetAlias("upsilon1s_eta","(upsilon1s_pz>0?-1:1)*acosh(upsilon1s_p/upsilon1s_pt)");
    pythia_tree_nompi->SetAlias("upsilon1s_y","0.5*log( (upsilon1s_pe+upsilon1s_pz) / (upsilon1s_pe-upsilon1s_pz) )");

    pythia_tree_nompi->SetAlias("dx2_p",  "sqrt(dx2_px*dx2_px+dx2_py*dx2_py+dx2_pz*dx2_pz)");
    pythia_tree_nompi->SetAlias("dx2_pt", "sqrt(dx2_px*dx2_px+dx2_py*dx2_py)");
    pythia_tree_nompi->SetAlias("dx2_eta","(dx2_pz>0?-1:1)*acosh(dx2_p/dx2_pt)");
    pythia_tree_nompi->SetAlias("dx2_y","0.5*log( (dx2_pe+dx2_pz) / (dx2_pe-dx2_pz) )");


    pythia_tree_nompi->SetAlias("dx_p",  "sqrt(dx_px*dx_px+dx_py*dx_py+dx_pz*dx_pz)");
    pythia_tree_nompi->SetAlias("dx_pt", "sqrt(dx_px*dx_px+dx_py*dx_py)");
    pythia_tree_nompi->SetAlias("dx_eta","(dx_pz>0?-1:1)*acosh(dx_p/dx_pt)");
    pythia_tree_nompi->SetAlias("dx_y","0.5*log( (dx_pe+dx_pz) / (dx_pe-dx_pz) )");


    pythia_tree_nompi->SetAlias("delta_eta_dx2_dx","dx2_eta-dx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_upsilon1s_dx","upsilon1s_eta-dx_eta");
    pythia_tree_nompi->SetAlias("delta_eta_upsilon1s_dx2","upsilon1s_eta-dx2_eta");

    pythia_tree_nompi->SetAlias("delta_y_dx_dx2","dx_y-dx2_y");
    pythia_tree_nompi->SetAlias("delta_y_upsilon1s_dx","upsilon1s_y-dx_y");
    pythia_tree_nompi->SetAlias("delta_y_upsilon1s_dx2","upsilon1s_y-dx2_y");

    pythia_tree_nompi->SetAlias("delta_R_dx2_dx","sqrt(delta_eta_dx2_dx*delta_eta_dx2_dx+angle_dx2_dx*angle_dx2_dx)");
    pythia_tree_nompi->SetAlias("delta_R_upsilon1s_dx","sqrt(delta_eta_upsilon1s_dx*delta_eta_upsilon1s_dx+angle_upsilon1s_dx*angle_upsilon1s_dx)");
    pythia_tree_nompi->SetAlias("delta_R_upsilon1s_dx2","sqrt(delta_eta_upsilon1s_dx2*delta_eta_upsilon1s_dx2+angle_upsilon1s_dx2*angle_upsilon1s_dx2)");

    double sigmaGen_pythia_nompi;
    double sigmaErr_pythia_nompi;
    pythia_tree_nompi->SetBranchAddress("sigmaGen",&sigmaGen_pythia_nompi);
    pythia_tree_nompi->SetBranchAddress("sigmaErr",&sigmaErr_pythia_nompi);
    
    int n_entries_pythia_nompi = pythia_tree_nompi->GetEntries();
    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------

    pythia_tree->GetEntry(pythia_tree->GetEntries()-1);
    pythia_tree_nompi->GetEntry(pythia_tree_nompi->GetEntries()-1);

  
    std::cout << "Cross section Pythia:        " << sigmaGen_pythia*microbarn  << " +/- " << sigmaErr_pythia*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree->GetEntries("nUpsilon1S==1&&nXc==2")<< "/" << pythia_tree->GetEntries("")<< std::endl;
   
    std::cout << "Cross section Pythia No MPI:  " << sigmaGen_pythia_nompi*microbarn  << " +/- " << sigmaErr_pythia_nompi*microbarn << " microbarns";
    std::cout << "\t" << pythia_tree_nompi->GetEntries("nUpsilon1S==1&&nXc==2")<< "/" << pythia_tree_nompi->GetEntries("")<< std::endl;
    
    double withtarget_pythia       = pythia_tree->GetEntries("nUpsilon1S==1&&nXc==2");
    double withtarget_pythia_nompi = pythia_tree_nompi->GetEntries("nUpsilon1S==1&&nXc==2");

    double scalefactor_pythia        =      sigmaGen_pythia*microbarn/n_entries_pythia * (withtarget_pythia/n_entries_pythia);
    double scalefactor_pythia_nompi  =      sigmaGen_pythia_nompi*microbarn/n_entries_pythia_nompi * (withtarget_pythia_nompi/n_entries_pythia_nompi);
    
    std::cout << "scalefactor_pythia       " << scalefactor_pythia<<std::endl;
    std::cout << "scalefactor_pythia_nompi " << scalefactor_pythia_nompi<<std::endl;

    // -------------------------------------------------------------------------
    // Get Crosssections
    // -------------------------------------------------------------------------
    TCut cuts_pythia        = "nUpsilon1S==1&&nXc==2&&dx2_pt!=0&&dx_pt!=0&&upsilon1s_pt!=0";
    TCut cuts_pythia_nompi  = "nUpsilon1S==1&&nXc==2&&dx2_pt!=0&&dx_pt!=0&&upsilon1s_pt!=0";

    // TCut cuts_fid = "(upsilon1s_eta)>2 && (upsilon1s_eta)<5 && (dx_eta)>2 && (dx_eta)<5 && (dx2_eta)>2 && (dx2_eta)<5 ";
    TCut cuts_all = "";
    

    // -------------------------------------------------------------------------
    // Plot 1D kinematics integrated over all eta 
    // -------------------------------------------------------------------------
    // TCut temp_cut = "foundHardb&&hasbStatus23&&(hasbStatus23_type!=5) && foundHardc&&hascStatus23&&(hascStatus23_type!=4)";
    // pT
    TCanvas* c_pt = new TCanvas("c_pt","c_pt");
    TH1D* h_pt_pythia        = new TH1D("h_pt_pythia", "h_pt_pythia", 50,0,20);
    TH1D* h_pt_pythia_nompi  = new TH1D("h_pt_pythia_nompi", "h_pt_pythia_nompi", 50,0,20);

    pythia_tree->Draw("upsilon1s_pt>>h_pt_pythia",cuts_pythia);
    pythia_tree_nompi->Draw("upsilon1s_pt>>h_pt_pythia_nompi",cuts_pythia_nompi);

    h_pt_pythia->Scale(scalefactor_pythia);
    h_pt_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_pt_pythia->SetLineColor(kRed);
    h_pt_pythia_nompi->SetLineColor(kBlue);
    
    double h_pt_pythia_bin_width = h_pt_pythia->GetXaxis()->GetBinWidth(2);
    h_pt_pythia->SetTitle(Form(";#it{p}_{T} [GeV/#it{c}]; d#sigma/d#it{p}_{T} [%.1f #mub GeV^{-1}#it{c}]",h_pt_pythia_bin_width));
    h_pt_pythia->Draw("hist");
    h_pt_pythia_nompi->Draw("hist same");

    TLegend* leg_pt = new TLegend(0.6,0.6,0.9,0.9);
    leg_pt->AddEntry(h_pt_pythia,"Pythia With MPI","l");
    leg_pt->AddEntry(h_pt_pythia_nompi,"Pythia No MPI","l");
    leg_pt->SetTextFont(132);
    leg_pt->SetFillStyle(0);
    leg_pt->Draw();
    c_pt->Print("plots_kinematics_Y1sXcXc/c_pt.pdf");


    // eta
    TCanvas* c_eta = new TCanvas("c_eta","c_eta");
    TH1D* h_eta_pythia        = new TH1D("h_eta_pythia", "h_eta_pythia", 50,-10,10);
    TH1D* h_eta_pythia_nompi  = new TH1D("h_eta_pythia_nompi", "h_eta_pythia_nompi", 50,-10,10);

    pythia_tree->Draw("upsilon1s_eta>>h_eta_pythia",cuts_pythia);
    pythia_tree_nompi->Draw("upsilon1s_eta>>h_eta_pythia_nompi",cuts_pythia_nompi);

    h_eta_pythia->Scale(scalefactor_pythia);
    h_eta_pythia_nompi->Scale(scalefactor_pythia_nompi);
    h_eta_pythia->SetLineColor(kRed);
    h_eta_pythia_nompi->SetLineColor(kBlue);
    double h_eta_pythia_bin_width = h_eta_pythia->GetXaxis()->GetBinWidth(2);
    h_eta_pythia->SetTitle(Form(";#eta [GeV/#it{c}]; d#sigma/d#eta [%.1f #mub]",h_eta_pythia_bin_width));
    h_eta_pythia->Draw("hist");
    h_eta_pythia_nompi->Draw("same hist");

    TLegend* leg_eta = new TLegend(0.6,0.6,0.9,0.9);
    leg_eta->AddEntry(h_eta_pythia,"Pythia With MPI","l");
    leg_eta->AddEntry(h_eta_pythia_nompi,"Pythia No MPI","l");
    leg_eta->SetTextFont(132);
    leg_eta->SetFillStyle(0);
    leg_eta->Draw();
    c_eta->Print("plots_kinematics_Y1sXcXc/c_eta.pdf");

    // Rapidity
    TCanvas* c_y = new TCanvas("c_y","c_y");
    TH1D* h_y_pythia        = new TH1D("h_y_pythia", "h_y_pythia", 50,-10,10);
    TH1D* h_y_pythia_nompi  = new TH1D("h_y_pythia_nompi", "h_y_pythia_nompi", 50,-10,10);

    pythia_tree->Draw("upsilon1s_y>>h_y_pythia",cuts_pythia);
    pythia_tree_nompi->Draw("upsilon1s_y>>h_y_pythia_nompi",cuts_pythia_nompi);

    h_y_pythia->Scale(scalefactor_pythia);
    h_y_pythia_nompi->Scale(scalefactor_pythia_nompi);
    h_y_pythia->SetLineColor(kRed);
    h_y_pythia_nompi->SetLineColor(kBlue);
    double h_y_pythia_bin_width = h_y_pythia->GetXaxis()->GetBinWidth(2);
    h_y_pythia->SetTitle(Form(";#it{y} [GeV/#it{c}]; d#sigma/d#it{y} [%.1f #mub]",h_y_pythia_bin_width));
    h_y_pythia->Draw("hist");
    h_y_pythia_nompi->Draw("same hist");

    TLegend* leg_y = new TLegend(0.6,0.6,0.9,0.9);
    leg_y->AddEntry(h_y_pythia,"Pythia With MPI","l");
    leg_y->AddEntry(h_y_pythia_nompi,"Pythia No MPI","l");
    leg_y->SetTextFont(132);
    leg_y->SetFillStyle(0);
    leg_y->Draw();

    c_y->Print("plots_kinematics_Y1sXcXc/c_y.pdf");

    // -------------------------------------------------------------------------
    // Plot 1D angles integrated over all eta 
    // -------------------------------------------------------------------------
    int n_bins_delta_phi = 50;
    // angles
    TCanvas* c_delta_phi_upsilon1s_dx2 = new TCanvas("c_delta_phi_upsilon1s_dx2","c_delta_phi_upsilon1s_dx2");
    TH1D* h_delta_phi_upsilon1s_dx2_pythia  = new TH1D("h_delta_phi_upsilon1s_dx2_pythia", "h_delta_phi_upsilon1s_dx2_pythia", n_bins_delta_phi,0,TMath::Pi());
    TH1D* h_delta_phi_upsilon1s_dx2_pythia_nompi  = new TH1D("h_delta_phi_upsilon1s_dx2_pythia_nompi", "h_delta_phi_upsilon1s_dx2_pythia_nompi", n_bins_delta_phi,0,TMath::Pi());

    pythia_tree->Draw("angle_upsilon1s_dx2>>h_delta_phi_upsilon1s_dx2_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("angle_upsilon1s_dx2>>h_delta_phi_upsilon1s_dx2_pythia_nompi",  cuts_pythia_nompi +cuts_all);

    h_delta_phi_upsilon1s_dx2_pythia->Scale(scalefactor_pythia);
    h_delta_phi_upsilon1s_dx2_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    h_delta_phi_upsilon1s_dx2_pythia->SetLineColor(kRed);
    h_delta_phi_upsilon1s_dx2_pythia_nompi->SetLineColor(kBlue);
    
    double h_delta_phi_upsilon1s_dx2_pythia_bin_width = h_delta_phi_upsilon1s_dx2_pythia->GetXaxis()->GetBinWidth(2);
    h_delta_phi_upsilon1s_dx2_pythia->SetTitle(Form(";#Delta#phi(#Upsilon(1S), X_{#bar{c}}) [rad]; d#sigma/d#Delta#phi [%.2f #mub rad^{-1}]",h_delta_phi_upsilon1s_dx2_pythia_bin_width));
    h_delta_phi_upsilon1s_dx2_pythia->SetMinimum(0.0);
    h_delta_phi_upsilon1s_dx2_pythia->Draw("hist");
    h_delta_phi_upsilon1s_dx2_pythia_nompi->Draw("same hist");

    TLegend* leg_delta_phi_upsilon1s_dx2 = new TLegend(0.6,0.3,0.9,0.5);
    leg_delta_phi_upsilon1s_dx2->AddEntry(h_delta_phi_upsilon1s_dx2_pythia,"Pythia With MPI","l");
    leg_delta_phi_upsilon1s_dx2->AddEntry(h_delta_phi_upsilon1s_dx2_pythia_nompi,"Pythia No MPI","l");
    leg_delta_phi_upsilon1s_dx2->SetTextFont(132);
    leg_delta_phi_upsilon1s_dx2->SetFillStyle(0);
    leg_delta_phi_upsilon1s_dx2->Draw();

    c_delta_phi_upsilon1s_dx2->Print("plots_kinematics_Y1sXcXc/c_delta_phi_upsilon1s_dx2.pdf");


    TCanvas* c_delta_phi_upsilon1s_dx = new TCanvas("c_delta_phi_upsilon1s_dx","c_delta_phi_upsilon1s_dx");
    TH1D* h_delta_phi_upsilon1s_dx_pythia  = new TH1D("h_delta_phi_upsilon1s_dx_pythia", "h_delta_phi_upsilon1s_dx_pythia", n_bins_delta_phi,0,TMath::Pi());
    TH1D* h_delta_phi_upsilon1s_dx_pythia_nompi  = new TH1D("h_delta_phi_upsilon1s_dx_pythia_nompi", "h_delta_phi_upsilon1s_dx_pythia_nompi", n_bins_delta_phi,0,TMath::Pi());

    pythia_tree->Draw("angle_upsilon1s_dx>>h_delta_phi_upsilon1s_dx_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("angle_upsilon1s_dx>>h_delta_phi_upsilon1s_dx_pythia_nompi",  cuts_pythia_nompi +cuts_all);

    h_delta_phi_upsilon1s_dx_pythia->Scale(scalefactor_pythia);
    h_delta_phi_upsilon1s_dx_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    h_delta_phi_upsilon1s_dx_pythia->SetLineColor(kRed);
    h_delta_phi_upsilon1s_dx_pythia_nompi->SetLineColor(kBlue);
    
    double h_delta_phi_upsilon1s_dx_pythia_bin_width = h_delta_phi_upsilon1s_dx_pythia->GetXaxis()->GetBinWidth(2);
    h_delta_phi_upsilon1s_dx_pythia->SetTitle(Form(";#Delta#phi(#Upsilon(1S), X_{c}) [rad]; d#sigma/d#Delta#phi [%.2f #mub rad^{-1}]",h_delta_phi_upsilon1s_dx_pythia_bin_width));
    h_delta_phi_upsilon1s_dx_pythia->SetMinimum(0.0);
    h_delta_phi_upsilon1s_dx_pythia->Draw("hist");
    h_delta_phi_upsilon1s_dx_pythia_nompi->Draw("same hist");

    TLegend* leg_delta_phi_upsilon1s_dx = new TLegend(0.6,0.3,0.9,0.5);
    leg_delta_phi_upsilon1s_dx->AddEntry(h_delta_phi_upsilon1s_dx_pythia,"Pythia With MPI","l");
    leg_delta_phi_upsilon1s_dx->AddEntry(h_delta_phi_upsilon1s_dx_pythia_nompi,"Pythia No MPI","l");
    leg_delta_phi_upsilon1s_dx->SetTextFont(132);
    leg_delta_phi_upsilon1s_dx->SetFillStyle(0);
    leg_delta_phi_upsilon1s_dx->Draw();

    c_delta_phi_upsilon1s_dx->Print("plots_kinematics_Y1sXcXc/c_delta_phi_upsilon1s_dx.pdf");


    TCanvas* c_delta_phi_dx_dx2 = new TCanvas("c_delta_phi_dx_dx2","c_delta_phi_dx_dx2");
    TH1D* h_delta_phi_dx_dx2_pythia  = new TH1D("h_delta_phi_dx_dx2_pythia", "h_delta_phi_dx_dx2_pythia", n_bins_delta_phi,0,TMath::Pi());
    TH1D* h_delta_phi_dx_dx2_pythia_nompi  = new TH1D("h_delta_phi_dx_dx2_pythia_nompi", "h_delta_phi_dx_dx2_pythia_nompi", n_bins_delta_phi,0,TMath::Pi());

    pythia_tree->Draw("angle_dx_dx2>>h_delta_phi_dx_dx2_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("angle_dx_dx2>>h_delta_phi_dx_dx2_pythia_nompi",  cuts_pythia_nompi +cuts_all);

    h_delta_phi_dx_dx2_pythia->Scale(scalefactor_pythia);
    h_delta_phi_dx_dx2_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    h_delta_phi_dx_dx2_pythia->SetLineColor(kRed);
    h_delta_phi_dx_dx2_pythia_nompi->SetLineColor(kBlue);
    
    double h_delta_phi_dx_dx2_pythia_bin_width = h_delta_phi_dx_dx2_pythia->GetXaxis()->GetBinWidth(2);
    h_delta_phi_dx_dx2_pythia->SetTitle(Form(";#Delta#phi(X_{c}, X_{#bar{c}}) [rad]; d#sigma/d#Delta#phi [%.2f #mub rad^{-1}]",h_delta_phi_dx_dx2_pythia_bin_width));
    h_delta_phi_dx_dx2_pythia->SetMinimum(0.0);
    h_delta_phi_dx_dx2_pythia->Draw("hist");
    h_delta_phi_dx_dx2_pythia_nompi->Draw("same hist");

    TLegend* leg_delta_phi_dx_dx2 = new TLegend(0.6,0.3,0.9,0.5);
    leg_delta_phi_dx_dx2->AddEntry(h_delta_phi_dx_dx2_pythia,"Pythia With MPI","l");
    leg_delta_phi_dx_dx2->AddEntry(h_delta_phi_dx_dx2_pythia_nompi,"Pythia No MPI","l");
    leg_delta_phi_dx_dx2->SetTextFont(132);
    leg_delta_phi_dx_dx2->SetFillStyle(0);
    leg_delta_phi_dx_dx2->Draw();

    c_delta_phi_dx_dx2->Print("plots_kinematics_Y1sXcXc/c_delta_phi_dx_dx2.pdf");
    

    // rapidity 
    TCanvas* c_delta_y_upsilon1s_dx2 = new TCanvas("c_delta_y_upsilon1s_dx2","c_delta_y_upsilon1s_dx2");
    TH1D* h_delta_y_upsilon1s_dx2_pythia  = new TH1D("h_delta_y_upsilon1s_dx2_pythia", "h_delta_y_upsilon1s_dx2_pythia", 50,-10.0,10.0);
    TH1D* h_delta_y_upsilon1s_dx2_pythia_nompi  = new TH1D("h_delta_y_upsilon1s_dx2_pythia_nompi", "h_delta_y_upsilon1s_dx2_pythia_nompi", 50,-10.0,10.0);

    pythia_tree->Draw("delta_y_upsilon1s_dx2>>h_delta_y_upsilon1s_dx2_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("delta_y_upsilon1s_dx2>>h_delta_y_upsilon1s_dx2_pythia_nompi",  cuts_pythia_nompi +cuts_all);

    h_delta_y_upsilon1s_dx2_pythia->Scale(scalefactor_pythia);
    h_delta_y_upsilon1s_dx2_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    h_delta_y_upsilon1s_dx2_pythia->SetLineColor(kRed);
    h_delta_y_upsilon1s_dx2_pythia_nompi->SetLineColor(kBlue);
    
    double h_delta_y_upsilon1s_dx2_pythia_bin_width = h_delta_y_upsilon1s_dx2_pythia->GetXaxis()->GetBinWidth(2);
    h_delta_y_upsilon1s_dx2_pythia->SetTitle(Form(";#Delta#it{y}(#Upsilon(1S), X_{#bar{c}}); d#sigma/d#Delta#it{y} [%.2f #mub]",h_delta_y_upsilon1s_dx2_pythia_bin_width));
    h_delta_y_upsilon1s_dx2_pythia->SetMinimum(0.0);
    h_delta_y_upsilon1s_dx2_pythia->Draw("hist");
    h_delta_y_upsilon1s_dx2_pythia_nompi->Draw("same hist");

    TLegend* leg_delta_y_upsilon1s_dx2 = new TLegend(0.6,0.3,0.9,0.5);
    leg_delta_y_upsilon1s_dx2->AddEntry(h_delta_y_upsilon1s_dx2_pythia,"Pythia With MPI","l");
    leg_delta_y_upsilon1s_dx2->AddEntry(h_delta_y_upsilon1s_dx2_pythia_nompi,"Pythia No MPI","l");
    leg_delta_y_upsilon1s_dx2->SetTextFont(132);
    leg_delta_y_upsilon1s_dx2->SetFillStyle(0);
    leg_delta_y_upsilon1s_dx2->Draw();

    c_delta_y_upsilon1s_dx2->Print("plots_kinematics_Y1sXcXc/c_delta_y_upsilon1s_dx2.pdf");


    TCanvas* c_delta_y_upsilon1s_dx = new TCanvas("c_delta_y_upsilon1s_dx","c_delta_y_upsilon1s_dx");
    TH1D* h_delta_y_upsilon1s_dx_pythia  = new TH1D("h_delta_y_upsilon1s_dx_pythia", "h_delta_y_upsilon1s_dx_pythia", 50,-10.0,10.0);
    TH1D* h_delta_y_upsilon1s_dx_pythia_nompi  = new TH1D("h_delta_y_upsilon1s_dx_pythia_nompi", "h_delta_y_upsilon1s_dx_pythia_nompi", 50,-10.0,10.0);

    pythia_tree->Draw("delta_y_upsilon1s_dx>>h_delta_y_upsilon1s_dx_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("delta_y_upsilon1s_dx>>h_delta_y_upsilon1s_dx_pythia_nompi",  cuts_pythia_nompi +cuts_all);

    h_delta_y_upsilon1s_dx_pythia->Scale(scalefactor_pythia);
    h_delta_y_upsilon1s_dx_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    h_delta_y_upsilon1s_dx_pythia->SetLineColor(kRed);
    h_delta_y_upsilon1s_dx_pythia_nompi->SetLineColor(kBlue);
    
    double h_delta_y_upsilon1s_dx_pythia_bin_width = h_delta_y_upsilon1s_dx_pythia->GetXaxis()->GetBinWidth(2);
    h_delta_y_upsilon1s_dx_pythia->SetTitle(Form(";#Delta#it{y}(#Upsilon(1S), X_{c}); d#sigma/d#Delta#it{y} [%.2f #mub]",h_delta_y_upsilon1s_dx_pythia_bin_width));
    h_delta_y_upsilon1s_dx_pythia->SetMinimum(0.0);
    h_delta_y_upsilon1s_dx_pythia->Draw("hist");
    h_delta_y_upsilon1s_dx_pythia_nompi->Draw("same hist");

    TLegend* leg_delta_y_upsilon1s_dx = new TLegend(0.6,0.3,0.9,0.5);
    leg_delta_y_upsilon1s_dx->AddEntry(h_delta_y_upsilon1s_dx_pythia,"Pythia With MPI","l");
    leg_delta_y_upsilon1s_dx->AddEntry(h_delta_y_upsilon1s_dx_pythia_nompi,"Pythia No MPI","l");
    leg_delta_y_upsilon1s_dx->SetTextFont(132);
    leg_delta_y_upsilon1s_dx->SetFillStyle(0);
    leg_delta_y_upsilon1s_dx->Draw();

    c_delta_y_upsilon1s_dx->Print("plots_kinematics_Y1sXcXc/c_delta_y_upsilon1s_dx.pdf");


    TCanvas* c_delta_y_dx_dx2 = new TCanvas("c_delta_y_dx_dx2","c_delta_y_dx_dx2");
    TH1D* h_delta_y_dx_dx2_pythia        = new TH1D("h_delta_y_dx_dx2_pythia", "h_delta_y_dx_dx2_pythia", 50,-10.0,10.0);
    TH1D* h_delta_y_dx_dx2_pythia_nompi  = new TH1D("h_delta_y_dx_dx2_pythia_nompi", "h_delta_y_dx_dx2_pythia_nompi", 50,-10.0,10.0);

    pythia_tree->Draw("delta_y_dx_dx2>>h_delta_y_dx_dx2_pythia",              cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("delta_y_dx_dx2>>h_delta_y_dx_dx2_pythia_nompi",  cuts_pythia_nompi +cuts_all);

    h_delta_y_dx_dx2_pythia->Scale(scalefactor_pythia);
    h_delta_y_dx_dx2_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    h_delta_y_dx_dx2_pythia->SetLineColor(kRed);
    h_delta_y_dx_dx2_pythia_nompi->SetLineColor(kBlue);
    
    double h_delta_y_dx_dx2_pythia_bin_width = h_delta_y_dx_dx2_pythia->GetXaxis()->GetBinWidth(2);
    h_delta_y_dx_dx2_pythia->SetTitle(Form(";#Delta#it{y}(X_{c}, X_{#bar{c}}); d#sigma/d#Delta#it{y} [%.2f #mub]",h_delta_y_dx_dx2_pythia_bin_width));
    h_delta_y_dx_dx2_pythia->SetMinimum(0.0);
    h_delta_y_dx_dx2_pythia->Draw("hist");
    h_delta_y_dx_dx2_pythia_nompi->Draw("same hist");

    TLegend* leg_delta_y_dx_dx2 = new TLegend(0.6,0.3,0.9,0.5);
    leg_delta_y_dx_dx2->AddEntry(h_delta_y_dx_dx2_pythia,"Pythia With MPI","l");
    leg_delta_y_dx_dx2->AddEntry(h_delta_y_dx_dx2_pythia_nompi,"Pythia No MPI","l");
    leg_delta_y_dx_dx2->SetTextFont(132);
    leg_delta_y_dx_dx2->SetFillStyle(0);
    leg_delta_y_dx_dx2->Draw();

    c_delta_y_dx_dx2->Print("plots_kinematics_Y1sXcXc/c_delta_y_dx_dx2.pdf");
    // -------------------------------------------------------------------------
    // Plot 2D angles integrated over all eta 
    // -------------------------------------------------------------------------


    SetLHCbStyle("cont");

    // angles
    TCanvas* c_delta_phi_pythia = new TCanvas("c_delta_phi_pythia","c_delta_phi_pythia");
    TH2D* h_delta_phi_pythia        = new TH2D("h_delta_phi_pythia", "h_delta_phi_pythia", 10,0,TMath::Pi(),10,0,TMath::Pi());
    TH2D* h_delta_phi_pythia_nompi  = new TH2D("h_delta_phi_pythia_nompi", "h_delta_phi_pythia_nompi", 10,0,TMath::Pi(),10,0,TMath::Pi());

    pythia_tree->Draw("angle_upsilon1s_dx:angle_upsilon1s_dx2>>h_delta_phi_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("angle_upsilon1s_dx:angle_upsilon1s_dx2>>h_delta_phi_pythia_nompi",  cuts_pythia_nompi +cuts_all);
    h_delta_phi_pythia->Scale(scalefactor_pythia);
    h_delta_phi_pythia_nompi->Scale(scalefactor_pythia_nompi);

    h_delta_phi_pythia_nompi->SetMinimum(0.0);
    h_delta_phi_pythia->SetMinimum(0.0);

    h_delta_phi_pythia->SetTitle(";#Delta#phi(J/#psi, X_{#bar{c}}) [rad]; #Delta#phi(J/#psi, X_{c}) [rad]");
    h_delta_phi_pythia->Draw("colz");
    c_delta_phi_pythia->Print("plots_kinematics_Y1sXcXc/c_delta_phi_pythia.pdf");
    
    TCanvas* c_delta_phi_pythia_nompi = new TCanvas("c_delta_phi_pythia_nompi","c_delta_phi_pythia_nompi");
    h_delta_phi_pythia_nompi->SetTitle(";#Delta#phi(J/#psi, X_{#bar{c}}) [rad]; #Delta#phi(J/#psi, X_{c}) [rad]");
    h_delta_phi_pythia_nompi->Draw("colz");
    c_delta_phi_pythia_nompi->Print("plots_kinematics_Y1sXcXc/c_delta_phi_pythia_nompi.pdf");
    

    // y
    TCanvas* c_delta_y_pythia = new TCanvas("c_delta_y_pythia","c_delta_y_pythia");
    TH2D* h_delta_y_pythia        = new TH2D("h_delta_y_pythia", "h_delta_y_pythia",             20,-5.0,5.0,20,-5.0,5.0);
    TH2D* h_delta_y_pythia_nompi  = new TH2D("h_delta_y_pythia_nompi", "h_delta_y_pythia_nompi", 20,-5.0,5.0,20,-5.0,5.0);

    pythia_tree->Draw("delta_y_upsilon1s_dx:delta_y_upsilon1s_dx2>>h_delta_y_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("delta_y_upsilon1s_dx:delta_y_upsilon1s_dx2>>h_delta_y_pythia_nompi",  cuts_pythia_nompi +cuts_all);

    h_delta_y_pythia->Scale(scalefactor_pythia);
    h_delta_y_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    h_delta_y_pythia_nompi->SetMinimum(0.0);
    h_delta_y_pythia->SetMinimum(0.0);
    
    h_delta_y_pythia->SetTitle(";#Delta#it{y}(J/#psi, X_{#bar{c}}); #Delta#it{y}(J/#psi, X_{c})");
    h_delta_y_pythia->Draw("colz");
    c_delta_y_pythia->Print("plots_kinematics_Y1sXcXc/c_delta_y_pythia.pdf");
    
    TCanvas* c_delta_y_pythia_nompi = new TCanvas("c_delta_y_pythia_nompi","c_delta_y_pythia_nompi");
    h_delta_y_pythia_nompi->SetTitle(";#Delta#it{y}(J/#psi, X_{#bar{c}}); #Delta#it{y}(J/#psi, X_{c})");
    h_delta_y_pythia_nompi->Draw("colz");
    c_delta_y_pythia_nompi->Print("plots_kinematics_Y1sXcXc/c_delta_y_pythia_nompi.pdf");
    
    // Delta R
    TCanvas* c_delta_R_pythia = new TCanvas("c_delta_R_pythia","c_delta_R_pythia");
    TH2D* h_delta_R_pythia  = new TH2D("h_delta_R_pythia", "h_delta_R_pythia", 20,0.0,10.0,20,0.0,10.0);
    TH2D* h_delta_R_pythia_nompi  = new TH2D("h_delta_R_pythia_nompi", "h_delta_R_pythia_nompi", 20,0.0,10.0,20,0.0,10.0);

    pythia_tree->Draw("delta_R_upsilon1s_dx:delta_R_upsilon1s_dx2>>h_delta_R_pythia",  cuts_pythia +cuts_all);
    pythia_tree_nompi->Draw("delta_R_upsilon1s_dx:delta_R_upsilon1s_dx2>>h_delta_R_pythia_nompi",  cuts_pythia_nompi +cuts_all);

    h_delta_R_pythia->Scale(scalefactor_pythia);
    h_delta_R_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    h_delta_R_pythia_nompi->SetMinimum(0.0);
    h_delta_R_pythia->SetMinimum(0.0);

    h_delta_R_pythia->SetTitle(";#DeltaR(J/#psi, X_{c}) [rad]; #DeltaR(J/#psi, X_{c}) [rad]");
    h_delta_R_pythia->Draw("colz");
    c_delta_R_pythia->Print("plots_kinematics_Y1sXcXc/c_delta_R_pythia.pdf");
    
    TCanvas* c_delta_R_pythia_nompi = new TCanvas("c_delta_R_pythia_nompi","c_delta_R_pythia_nompi");
    h_delta_R_pythia_nompi->SetTitle(";#DeltaR(J/#psi, X_{#bar{c}}) [rad]; #DeltaR(J/#psi, X_{c}) [rad]");
    h_delta_R_pythia_nompi->Draw("colz");
    c_delta_R_pythia_nompi->Print("plots_kinematics_Y1sXcXc/c_delta_R_pythia_nompi.pdf");
     
    // -------------------------------------------------------------------------
    // Plot 2D pT vs. y 
    // -------------------------------------------------------------------------

    SetLHCbStyle("cont");
    TCanvas* c_2D_pt_y_pythia = new TCanvas("c_2D_pt_y_pythia","c_2D_pt_y_pythia");
    TH2D* h_2D_pt_y_pythia      = new TH2D("h_2D_pt_y_pythia",      "h_2D_pt_y_pythia",    20,-10,10,20,0,10);
    TH2D* h_2D_pt_y_pythia_nompi      = new TH2D("h_2D_pt_y_pythia_nompi",      "h_2D_pt_y_pythia_nompi",    20,-10,10,20,0,10);

    pythia_tree->Draw(    "upsilon1s_pt:upsilon1s_y>>h_2D_pt_y_pythia",    cuts_pythia +cuts_all    );
    pythia_tree_nompi->Draw(    "upsilon1s_pt:upsilon1s_y>>h_2D_pt_y_pythia_nompi",    cuts_pythia_nompi +cuts_all    );
    h_2D_pt_y_pythia->Scale(scalefactor_pythia);
    h_2D_pt_y_pythia_nompi->Scale(scalefactor_pythia_nompi);
    
    // h_2D_pt_y_pythia_nompi->SetMinimum(0.0);
    // h_2D_pt_y_pythia->SetMinimum(0.0);

    h_2D_pt_y_pythia->SetTitle(";y;#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_pythia->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_pythia->Print("plots_kinematics_Y1sXcXc/c_2D_pt_y_pythia.pdf");
    
    TCanvas* c_2D_pt_y_pythia_nompi = new TCanvas("c_2D_pt_y_pythia_nompi","c_2D_pt_y_pythia_nompi");

    h_2D_pt_y_pythia_nompi->SetTitle(";y;#it{p}_{T} [GeV/#it{c}]");
    h_2D_pt_y_pythia_nompi->Draw("colz");
    gPad->SetLogz();
    c_2D_pt_y_pythia_nompi->Print("plots_kinematics_Y1sXcXc/c_2D_pt_y_pythia_nompi.pdf");


    // -------------------------------------------------------------------------
    // Compare to LHCb measurements
    // -------------------------------------------------------------------------

    SetLHCbStyle("oneD");
    TCanvas* c_lhcb_comp = new TCanvas("c_lhcb_comp","c_lhcb_comp");
    TH1D* h_lhcb_comp        = new TH1D("h_lhcb_comp",       "h_lhcb_comp",       10,0,1.0);
    TH1D* h_lhcb_comp_nompi  = new TH1D("h_lhcb_comp_nompi", "h_lhcb_comp_nompi", 10,0,1.0);
    
    TCut cuts_fid_dx  = "(upsilon1s_eta)>2 && (upsilon1s_eta)<5 &&  (dx_eta )>2 && (dx_eta )<5  ";
    TCut cuts_fid_dx2 = "(upsilon1s_eta)>2 && (upsilon1s_eta)<5 &&  (dx2_eta)>2 && (dx2_eta)<5  ";

    pythia_tree->Draw("angle_upsilon1s_dx/TMath::Pi()>>h_lhcb_comp",  cuts_pythia +cuts_all+cuts_fid_dx);
    pythia_tree_nompi->Draw("angle_upsilon1s_dx/TMath::Pi()>>h_lhcb_comp_nompi",  cuts_pythia_nompi +cuts_all+cuts_fid_dx);

    pythia_tree->Draw("angle_upsilon1s_dx2/TMath::Pi()>>+h_lhcb_comp",  cuts_pythia +cuts_all+cuts_fid_dx2);
    pythia_tree_nompi->Draw("angle_upsilon1s_dx2/TMath::Pi()>>+h_lhcb_comp_nompi",  cuts_pythia_nompi +cuts_all+cuts_fid_dx2);

    TH1D *h_lhcb_Dz = new TH1D("h_lhcb_Dz","",10,0,1);
    h_lhcb_Dz->SetLineColor(kBlack);
    h_lhcb_Dz->SetBinContent(1,0.1703914);
    h_lhcb_Dz->SetBinContent(2,0.1089607);
    h_lhcb_Dz->SetBinContent(3,0.1301497);
    h_lhcb_Dz->SetBinContent(4,0.08425749);
    h_lhcb_Dz->SetBinContent(5,0.1050316);
    h_lhcb_Dz->SetBinContent(6,0.08505122);
    h_lhcb_Dz->SetBinContent(7,0.06646973);
    h_lhcb_Dz->SetBinContent(8,0.06720591);
    h_lhcb_Dz->SetBinContent(9,0.06453125);
    h_lhcb_Dz->SetBinContent(10,0.1179511);
    h_lhcb_Dz->SetBinError(1,0.03345452);
    h_lhcb_Dz->SetBinError(2,0.02542649);
    h_lhcb_Dz->SetBinError(3,0.02568044);
    h_lhcb_Dz->SetBinError(4,0.03549756);
    h_lhcb_Dz->SetBinError(5,0.01887588);
    h_lhcb_Dz->SetBinError(6,0.02268587);
    h_lhcb_Dz->SetBinError(7,0.0205256);
    h_lhcb_Dz->SetBinError(8,0.01586396);
    h_lhcb_Dz->SetBinError(9,0.01794524);
    h_lhcb_Dz->SetBinError(10,0.02540943);

    TH1D *h_lhcb_Dp = new TH1D("h_lhcb_Dp","",5,0,1);
    h_lhcb_Dp->SetLineColor(kGreen+2);
    h_lhcb_Dp->SetMarkerColor(kGreen+2);

    h_lhcb_Dp->SetBinContent(1,0.3048123);
    h_lhcb_Dp->SetBinContent(2,0.2014837);
    h_lhcb_Dp->SetBinContent(3,0.215055);
    h_lhcb_Dp->SetBinContent(4,0.1873196);
    h_lhcb_Dp->SetBinContent(5,0.09132945);
    h_lhcb_Dp->SetBinError(1,0.09617287);
    h_lhcb_Dp->SetBinError(2,0.05734113);
    h_lhcb_Dp->SetBinError(3,0.05072002);
    h_lhcb_Dp->SetBinError(4,0.04659536);
    h_lhcb_Dp->SetBinError(5,0.03919364);


    std::cout << "Integral: h_lhcb_comp       " << h_lhcb_comp->Integral()       << std::endl;
    std::cout << "Integral: h_lhcb_comp_nompi " << h_lhcb_comp_nompi->Integral() << std::endl;
    std::cout << "Integral: h_lhcb_Dz         " << h_lhcb_Dz->Integral()         << std::endl;
    std::cout << "Integral: h_lhcb_Dp         " << h_lhcb_Dp->Integral()         << std::endl;
    
    std::cout << "Integralw: h_lhcb_comp       " << h_lhcb_comp->Integral("width")       << std::endl;
    std::cout << "Integralw: h_lhcb_comp_nompi " << h_lhcb_comp_nompi->Integral("width") << std::endl;
    std::cout << "Integralw: h_lhcb_Dz         " << h_lhcb_Dz->Integral("width")         << std::endl;
    std::cout << "Integralw: h_lhcb_Dp         " << h_lhcb_Dp->Integral("width")         << std::endl;

    h_lhcb_comp->Scale(1.0/h_lhcb_comp->Integral("width"));
    h_lhcb_comp_nompi->Scale(1.0/h_lhcb_comp_nompi->Integral("width"));
    h_lhcb_Dz->Scale(1.0/h_lhcb_Dz->Integral("width"));
    h_lhcb_Dp->Scale(1.0/h_lhcb_Dp->Integral("width"));


    h_lhcb_comp->SetLineColor(kRed);
    h_lhcb_comp_nompi->SetLineColor(kBlue);
    
    double h_lhcb_comp_bin_width = h_lhcb_comp->GetXaxis()->GetBinWidth(2);
    h_lhcb_comp->SetTitle(";#Delta#phi(#Upsilon(1S), X_{c})/#pi; 1/#sigma d#sigma/d#Delta#phi");
    h_lhcb_comp->SetMinimum(0.0);
    h_lhcb_comp->SetMaximum(3.0);

    h_lhcb_comp->Draw("hist ][");
    h_lhcb_comp_nompi->Draw("same hist ][");

    h_lhcb_Dp->Draw("e1 same");
    h_lhcb_Dz->Draw("e1 same");


    TLegend* leg_lhcb_comp = new TLegend(0.5,0.7,0.9,0.9);
    leg_lhcb_comp->AddEntry(h_lhcb_comp,"Pythia With MPI","l");
    leg_lhcb_comp->AddEntry(h_lhcb_comp_nompi,"Pythia No MPI","l");
    leg_lhcb_comp->AddEntry(h_lhcb_Dp,"LHCb #Upsilon(1S)D^{+}","le");
    leg_lhcb_comp->AddEntry(h_lhcb_Dz,"LHCb #Upsilon(1S)D^{0}","le");
    leg_lhcb_comp->SetTextFont(132);
    leg_lhcb_comp->SetFillStyle(0);
    leg_lhcb_comp->Draw();



    c_lhcb_comp->Print("plots_kinematics_Y1sXcXc/c_lhcb_comp.pdf");

    // return;

    // Loop over the events and categorise them
    bool foundHardc; 
    bool hascStatus23; 
    bool hascStatus33; 
    bool hascOniaStatus23; 
    bool hascOniaStatus33; 
    int hascStatus23_type;
    int hascStatus33_type;
    int hascStatus33_type2;
    int hascStatus33_N;
    int hascOniaStatus33_N;
    bool hasbStatus23; 
    bool hasbStatus33; 
    bool hasbOniaStatus23; 
    bool hasbOniaStatus33; 
    int hasbStatus23_type;
    int hasbStatus33_type;
    int hasbStatus33_type2;
    int hasbStatus33_N;
    int hasbOniaStatus33_N;
    int nUpsilon1S;
    int nXc;

    pythia_tree->SetBranchAddress("foundHardc",        &foundHardc); 
    pythia_tree->SetBranchAddress("hascStatus23",      &hascStatus23); 
    pythia_tree->SetBranchAddress("hascStatus33",      &hascStatus33); 
    pythia_tree->SetBranchAddress("hascOniaStatus23",  &hascOniaStatus23); 
    pythia_tree->SetBranchAddress("hascOniaStatus33",  &hascOniaStatus33); 
    pythia_tree->SetBranchAddress("hascStatus23_type", &hascStatus23_type );
    pythia_tree->SetBranchAddress("hascStatus33_type", &hascStatus33_type );
    pythia_tree->SetBranchAddress("hascStatus33_type2",&hascStatus33_type2 );
    pythia_tree->SetBranchAddress("hascStatus33_N",    &hascStatus33_N );
    pythia_tree->SetBranchAddress("hasbStatus23",      &hasbStatus23); 
    pythia_tree->SetBranchAddress("hasbStatus33",      &hasbStatus33); 
    pythia_tree->SetBranchAddress("hasbOniaStatus23",  &hasbOniaStatus23); 
    pythia_tree->SetBranchAddress("hasbOniaStatus33",  &hasbOniaStatus33); 
    pythia_tree->SetBranchAddress("hasbStatus23_type", &hasbStatus23_type );
    pythia_tree->SetBranchAddress("hasbStatus33_type", &hasbStatus33_type );
    pythia_tree->SetBranchAddress("hasbStatus33_type2",&hasbStatus33_type2 );
    pythia_tree->SetBranchAddress("hasbStatus33_N",    &hasbStatus33_N );
    pythia_tree->SetBranchAddress("nUpsilon1S",             &nUpsilon1S );
    pythia_tree->SetBranchAddress("nXc",               &nXc );
    pythia_tree->SetBranchAddress("hascOniaStatus33_N",&hascOniaStatus33_N);
    pythia_tree->SetBranchAddress("hasbOniaStatus33_N",&hasbOniaStatus33_N);


    // Add kinematic distributions
    double upsilon1s_pe, upsilon1s_px, upsilon1s_py, upsilon1s_pz;
    double dx_pe,   dx_px,   dx_py,   dx_pz;
    double dx2_pe,  dx2_px,  dx2_py,  dx2_pz;

    pythia_tree->SetBranchAddress("upsilon1s_px",        &upsilon1s_px); 
    pythia_tree->SetBranchAddress("upsilon1s_py",        &upsilon1s_py); 
    pythia_tree->SetBranchAddress("upsilon1s_pz",        &upsilon1s_pz); 
    pythia_tree->SetBranchAddress("upsilon1s_pe",        &upsilon1s_pe); 
    pythia_tree->SetBranchAddress("dx_px",          &dx_px); 
    pythia_tree->SetBranchAddress("dx_py",          &dx_py); 
    pythia_tree->SetBranchAddress("dx_pz",          &dx_pz); 
    pythia_tree->SetBranchAddress("dx_pe",          &dx_pe);
    pythia_tree->SetBranchAddress("dx2_px",         &dx2_px); 
    pythia_tree->SetBranchAddress("dx2_py",         &dx2_py); 
    pythia_tree->SetBranchAddress("dx2_pz",         &dx2_pz); 
    pythia_tree->SetBranchAddress("dx2_pe",         &dx2_pe); 
    // bool Jpsi_isMixedLine;
    // pythia_tree->SetBranchAddress("Jpsi_isMixedLine",         &Jpsi_isMixedLine); 

    std::map<std::string,int> n_processes; 
    std::map<std::string,TH2D*> h_processes; 


    std::map<std::string,TH2D*> h_processes_pt_eta; 

    int n_bins = 10;
    int n_bins_kin = 10;

    for(int i = 0; i< n_entries_pythia; i++){
        pythia_tree->GetEntry(i);
        if(nUpsilon1S==1&&nXc==2){
            // TLotentzVector upsilon1s_vec( upsilon1s_px,upsilon1s_py,upsilon1s_pz,upsilon1s_pe);
            // TLotentzVector dx_vec(   dx_px,  dx_py,  dx_pz,  dx_pe  );
            // TLotentzVector dx2_vec(  dx2_px, dx2_py, dx2_pz, dx2_pe );

            double upsilon1s_pt = sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py);
            double dx2_pt  = sqrt(dx2_px *dx2_px +dx2_py *dx2_py );
            double dx_pt   = sqrt(dx_px  *dx_px  +dx_py  *dx_py  ); 

            double angle_dx2_dx   = acos((dx2_px*dx_px   + dx2_py*dx_py  )/(dx2_pt*dx_pt  ));
            double angle_upsilon1s_dx  = acos((upsilon1s_px*dx_px  + upsilon1s_py*dx_py )/(upsilon1s_pt*dx_pt ));
            double angle_upsilon1s_dx2 = acos((upsilon1s_px*dx2_px + upsilon1s_py*dx2_py)/(upsilon1s_pt*dx2_pt));
    
            double upsilon1s_p = sqrt(upsilon1s_px*upsilon1s_px+upsilon1s_py*upsilon1s_py+upsilon1s_pz*upsilon1s_pz);

            double upsilon1s_eta = (upsilon1s_pz>0.0?-1.0:1.0)*acosh(upsilon1s_p/upsilon1s_pt);

            std::vector<std::string> processes;
            std::string name;

            int n_c_accounted_for = hascStatus23 + hascStatus33_N + hascOniaStatus23 + hascOniaStatus33_N;
            int n_b_accounted_for = hasbStatus23 + hasbStatus33_N + hasbOniaStatus23 + hasbOniaStatus33_N;

            // std::cout << "n_c_accounted_for: " << n_c_accounted_for;
            // std::cout << "\tn_b_accounted_for: " << n_b_accounted_for;
            // std::cout << std::endl;
            

            // ---------------------- b processes ------------------------------
            if(hasbStatus23){   
                name = "Hard";
                if(hasbStatus23_type==5){
                    name += "_bb";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);
            }

            if(hasbOniaStatus23){
                name = "Hard_bOnia";
                processes.push_back(name);
            }

            if(hasbStatus33_N == 1){
                name = "MPI";
                if(hasbStatus33_type==5){
                    name += "_bb";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);

            }
            if(hasbStatus33_N == 2){
                name = "MPI1";
                if(hasbStatus33_type==5){
                    name += "_bb";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);

                
                name = "MPI2";
                if(hasbStatus33_type2==5){
                    name += "_bb";
                } else {
                    name += "_bx";
                }
                processes.push_back(name);


            }
            if(hasbOniaStatus33_N == 1){
                name =  "MPI_bOnia";
                processes.push_back(name);

            }
            if(hasbOniaStatus33_N == 2){
                name =  "MPI1_bOnia";
                processes.push_back(name);

                name =  "MPI2_bOnia";
                processes.push_back(name);

            }
            
            // ------------------------ b shower -------------------------------

            if(n_b_accounted_for==0){
                name =  "Showerb";
                processes.push_back(name);
            }

            // ---------------------- c processes ------------------------------
            if(hascStatus23){   
                name = "Hard";
                if(hascStatus23_type==4){
                    name += "_cc";
                } else if(hascStatus23_type==21) {
                    name += "_cg";
                } else if(hascStatus23_type>0&&hascStatus23_type<6) {
                    name += "_cq";
                } else {
                    name += "_cX";
                }
                processes.push_back(name);
            }

            if(hascOniaStatus23){
                name = "Hard_cOnia";
                processes.push_back(name);
            }

            if(hascStatus33_N == 1){
                name = "MPI";
                if(hascStatus33_type==4){
                    name += "_cc";
                } else if(hascStatus33_type==21) {
                    name += "_cg";
                } else if(hascStatus33_type>0&&hascStatus33_type<6) {
                    name += "_cq";
                } else {
                    name += "_cX";
                }
                processes.push_back(name);

            }
            if(hascStatus33_N == 2){
                name = "MPI1";
                if(hascStatus33_type==4){
                    name += "_cc";
                } else if(hascStatus33_type==21) {
                    name += "_cg";
                } else if(hascStatus33_type>0&&hascStatus33_type<6) {
                    name += "_cq";
                } else {
                    name += "_cX";
                }
                processes.push_back(name);

                
                name = "MPI2";
                if(hascStatus33_type2==4){
                    name += "_cc";
                } else if(hascStatus33_type==21) {
                    name += "_cg";
                } else if(hascStatus33_type>0&&hascStatus33_type<6) {
                    name += "_cq";
                } else {
                    name += "_cX";
                }
                processes.push_back(name);


            }
            if(hascOniaStatus33_N == 1){
                name =  "MPI_cOnia";
                processes.push_back(name);

            }
            if(hascOniaStatus33_N == 2){
                name =  "MPI1_cOnia";
                processes.push_back(name);

                name =  "MPI2_cOnia";
                processes.push_back(name);

            }


            // ------------------------ c shower -------------------------------

            if(n_c_accounted_for==0){
                name =  "Showerc";
                processes.push_back(name);
            }



            // ---------------------- c shower ------------------------------
            std::string cat_name;
            if(n_c_accounted_for+n_b_accounted_for <3 && processes.size()==2){
                cat_name = processes[0]+"_"+ processes[1];
            } else if(n_c_accounted_for+n_b_accounted_for >=3){
                cat_name = "Bad_3";
            } else {
                cat_name = "Bad_Other";
            }
            

            cat_name += "_Sep";


            if(h_processes.count(cat_name)==0){
                h_processes[cat_name] = new TH2D(Form("h_%s",cat_name.c_str()), 
                                                 Form("h_%s;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",cat_name.c_str()),
                                                 n_bins,0,TMath::Pi(),
                                                 n_bins,0,TMath::Pi() );
                h_processes_pt_eta[cat_name] = new TH2D(Form("h_%s_pt_eta",cat_name.c_str()), 
                                                 Form("h_%s_pt_eta;#eta;#it{p}_{T} [GeV/#it{c}]",cat_name.c_str()),
                                                 n_bins_kin,-10.0,10.0,
                                                 n_bins_kin,0,15.0 );

                
            }
            
            h_processes[cat_name]->Fill(angle_upsilon1s_dx,angle_upsilon1s_dx2);
            h_processes_pt_eta[cat_name]->Fill(upsilon1s_eta,upsilon1s_pt);
            n_processes[cat_name]++;
        }
    }

    for(const auto & n_proc: n_processes){
        std::cout << std::setw(25) << n_proc.first  << " | ";
        std::cout << std::setw(10) << n_proc.second << " | ";
        std::cout << std::endl;
    }   

    // Add plots together
    std::map<std::string,bool> isSPS;


    isSPS["Hard_bOnia_Hard_cX_Sep"]  = true;  
    isSPS["Hard_bOnia_MPI_cX_Sep"]   = false;  
    isSPS["Hard_bOnia_MPI_cc_Sep"]   = false;  
    isSPS["Hard_bOnia_MPI_cg_Sep"]   = false;  
    isSPS["Hard_bOnia_MPI_cq_Sep"]   = false;  
    isSPS["Hard_bOnia_Showerc_Sep"]  = true;  

    isSPS["Hard_bb_MPI_cc_Sep"]      = false;  
    isSPS["Hard_bb_MPI_cg_Sep"]      = false;  
    isSPS["Hard_bb_MPI_cq_Sep"]      = false;  
    isSPS["Hard_bb_Showerc_Sep"]     = true;

    isSPS["Hard_bx_Hard_cq_Sep"]     = true;  
    isSPS["Hard_bx_MPI_cc_Sep"]      = false;  
    isSPS["Hard_bx_MPI_cg_Sep"]      = false;  
    isSPS["Hard_bx_MPI_cq_Sep"]      = false;  
    isSPS["Hard_bx_Showerc_Sep"]     = true;

    isSPS["MPI_bOnia_Hard_cc_Sep"]   = false;  
    isSPS["MPI_bOnia_Hard_cg_Sep"]   = false;  
    isSPS["MPI_bOnia_Hard_cq_Sep"]   = false;  
    isSPS["MPI_bOnia_MPI_cOnia_Sep"] = false;  
    isSPS["MPI_bOnia_MPI_cX_Sep"]    = true;  
    isSPS["MPI_bOnia_MPI_cc_Sep"]    = false;  
    isSPS["MPI_bOnia_MPI_cg_Sep"]    = false;  
    isSPS["MPI_bOnia_MPI_cq_Sep"]    = false;  
    isSPS["MPI_bOnia_Showerc_Sep"]   = true; 

    isSPS["MPI_bb_Hard_cg_Sep"]      = false;  
    isSPS["MPI_bb_Hard_cq_Sep"]      = false;  
    isSPS["MPI_bb_MPI_cOnia_Sep"]    = false;  
    isSPS["MPI_bb_MPI_cc_Sep"]       = false;  
    isSPS["MPI_bb_MPI_cg_Sep"]       = false;  
    isSPS["MPI_bb_MPI_cq_Sep"]       = false;  
    isSPS["MPI_bb_Showerc_Sep"]      = true; 

    isSPS["MPI_bx_Hard_cc_Sep"]      = false;  
    isSPS["MPI_bx_Hard_cg_Sep"]      = false;  
    isSPS["MPI_bx_Hard_cq_Sep"]      = false;  
    isSPS["MPI_bx_MPI_cc_Sep"]       = false;  
    isSPS["MPI_bx_MPI_cg_Sep"]       = false;  
    isSPS["MPI_bx_MPI_cq_Sep"]       = false;  
    isSPS["MPI_bx_Showerc_Sep"]      = true; 

    isSPS["Showerb_Hard_cc_Sep"]     = true;  
    isSPS["Showerb_Hard_cg_Sep"]     = true;  
    isSPS["Showerb_MPI_cOnia_Sep"]   = true;  
    isSPS["Showerb_MPI_cc_Sep"]      = true;  
    isSPS["Showerb_MPI_cg_Sep"]      = true;  
    isSPS["Showerb_MPI_cq_Sep"]      = true;  
    isSPS["Showerb_Showerc_Sep"]     = true; 

    TH2D* h_processes_pt_eta_SPS_Mix = new TH2D("h_SPS_Mix_pt_eta", 
                                     "h_SPS_Mix_pt_eta;#eta;#it{p}_{T} [GeV/#it{c}]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );


    TH2D* h_processes_pt_eta_MPI_Mix = new TH2D("h_MPI_Mix_pt_eta", 
                                     "h_MPI_Mix_pt_eta;#eta;#it{p}_{T} [GeV/#it{c}]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );

    TH2D* h_processes_pt_eta_SPS_Sep = new TH2D("h_SPS_Sep_pt_eta", 
                                     "h_SPS_Sep_pt_eta;#eta;#it{p}_{T} [GeV/#it{c}]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );


    TH2D* h_processes_pt_eta_MPI_Sep = new TH2D("h_MPI_Sep_pt_eta", 
                                     "h_MPI_Sep_pt_eta;#eta;#it{p}_{T} [GeV/#it{c}]",
                                     n_bins_kin,-10.0,10.0,
                                     n_bins_kin,0,15.0 );

    TH2D* h_processes_SPS_Mix = new TH2D("h_SPS_Mix", 
                                    "h_SPS_Mix;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_MPI_Mix = new TH2D("h_MPI_Mix", 
                                    "h_MPI_Mix;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_SPS_Sep = new TH2D("h_SPS_Sep", 
                                    "h_SPS_Sep;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    TH2D* h_processes_MPI_Sep = new TH2D("h_MPI_Sep", 
                                    "h_MPI_Sep;#Delta#phi(J/#psi, X_{c}) [rad];#Delta#phi(J/#psi, X_{c}) [rad]",
                                    n_bins,0,TMath::Pi(),
                                    n_bins,0,TMath::Pi() );

    for(const auto & h_proc: h_processes_pt_eta){
        if(isSPS.count(h_proc.first)){
            if(h_proc.first.find("_Mix")!=std::string::npos){
                if(isSPS[h_proc.first]){
                    h_processes_pt_eta_SPS_Mix->Add(h_processes_pt_eta_SPS_Mix,h_proc.second);
                } else {
                    h_processes_pt_eta_MPI_Mix->Add(h_processes_pt_eta_MPI_Mix,h_proc.second);
                } 
            }else{

                if(isSPS[h_proc.first]){
                    h_processes_pt_eta_SPS_Sep->Add(h_processes_pt_eta_SPS_Sep,h_proc.second);
                } else {
                    h_processes_pt_eta_MPI_Sep->Add(h_processes_pt_eta_MPI_Sep,h_proc.second);
                } 
            }
        } else {
            std::cout << "Cat not SPS or MPI " << h_proc.first << std::endl;
        }
    }
    SetLHCbStyle("cont");


    h_processes_pt_eta_SPS_Mix->SetMinimum(0);
    h_processes_pt_eta_MPI_Mix->SetMinimum(0);
    h_processes_pt_eta_SPS_Sep->SetMinimum(0);
    h_processes_pt_eta_MPI_Sep->SetMinimum(0);


    TCanvas* can_pt_eta_SPS_Mix = new TCanvas("can_pt_eta_SPS_Mix","can_pt_eta_SPS_Mix");
    h_processes_pt_eta_SPS_Mix->Draw("colz");
    can_pt_eta_SPS_Mix->Print("plots_kinematics_Y1sXcXc/can_pt_eta_SPS_Mix.pdf");
    
    TCanvas* can_pt_eta_MPI_Mix = new TCanvas("can_pt_eta_MPI_Mix","can_pt_eta_MPI_Mix");
    h_processes_pt_eta_MPI_Mix->Draw("colz");
    can_pt_eta_MPI_Mix->Print("plots_kinematics_Y1sXcXc/can_pt_eta_MPI_Mix.pdf");


    TCanvas* can_pt_eta_SPS_Sep = new TCanvas("can_pt_eta_SPS_Sep","can_pt_eta_SPS_Sep");
    h_processes_pt_eta_SPS_Sep->Draw("colz");
    can_pt_eta_SPS_Sep->Print("plots_kinematics_Y1sXcXc/can_pt_eta_SPS_Sep.pdf");
    
    TCanvas* can_pt_eta_MPI_Sep = new TCanvas("can_pt_eta_MPI_Sep","can_pt_eta_MPI_Sep");
    h_processes_pt_eta_MPI_Sep->Draw("colz");
    can_pt_eta_MPI_Sep->Print("plots_kinematics_Y1sXcXc/can_pt_eta_MPI_Sep.pdf");


    
    for(const auto & h_proc: h_processes){
        if(isSPS.count(h_proc.first)){
            if(h_proc.first.find("_Mix")!=std::string::npos){
                if(isSPS[h_proc.first]){
                    h_processes_SPS_Mix->Add(h_processes_SPS_Mix,h_proc.second);
                } else {
                    h_processes_MPI_Mix->Add(h_processes_MPI_Mix,h_proc.second);
                }  
            } else {
                if(isSPS[h_proc.first]){
                    h_processes_SPS_Sep->Add(h_processes_SPS_Sep,h_proc.second);
                } else {
                    h_processes_MPI_Sep->Add(h_processes_MPI_Sep,h_proc.second);
                }
            } 
        } else {
            std::cout << "Cat not SPS or MPI " << h_proc.first << std::endl;
        }
    }
    SetLHCbStyle("cont");

    h_processes_SPS_Mix->SetMinimum(0);
    h_processes_MPI_Mix->SetMinimum(0);
    h_processes_SPS_Sep->SetMinimum(0);
    h_processes_MPI_Sep->SetMinimum(0);

    TCanvas* can_SPS_Mix = new TCanvas("can_SPS_Mix","can_SPS_Mix");
    h_processes_SPS_Mix->Draw("colz");
    can_SPS_Mix->Print("plots_kinematics_Y1sXcXc/can_SPS_Mix.pdf");
    
    TCanvas* can_MPI_Mix = new TCanvas("can_MPI_Mix","can_MPI_Mix");
    h_processes_MPI_Mix->Draw("colz");
    can_MPI_Mix->Print("plots_kinematics_Y1sXcXc/can_MPI_Mix.pdf");

    TCanvas* can_SPS_Sep = new TCanvas("can_SPS_Sep","can_SPS_Sep");
    h_processes_SPS_Sep->Draw("colz");
    can_SPS_Sep->Print("plots_kinematics_Y1sXcXc/can_SPS_Sep.pdf");
    
    TCanvas* can_MPI_Sep = new TCanvas("can_MPI_Sep","can_MPI_Sep");
    h_processes_MPI_Sep->Draw("colz");
    can_MPI_Sep->Print("plots_kinematics_Y1sXcXc/can_MPI_Sep.pdf");


    // Draw angles
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s",name.c_str()),Form("can_%s",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics_Y1sXcXc/can_%s.pdf",name.c_str()));

    }

    // Draw pt/eta
    SetLHCbStyle("cont");
    for(const auto & h_proc: h_processes_pt_eta){
        std::string name = h_proc.second->GetName();
        TCanvas* can = new TCanvas(Form("can_%s_pt_eta",name.c_str()),Form("can_%s_pt_eta",name.c_str()));
        h_proc.second->Draw("colz");
        can->Print(Form("plots_kinematics_Y1sXcXc/can_%s_pt_eta.pdf",name.c_str()));

    }
}